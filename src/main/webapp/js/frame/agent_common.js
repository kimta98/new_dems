
agent = {
	getFilePath : function(obj_id){
		var filter = '그림 파일 (*.jpg, *.gif, *.bmp) | *.jpg; *.gif; *.bmp; | 모든 파일 (*.*) | *.*';
		var args = {data:{"type": "openDirExplorer","filter": filter}};
		var data = JSON.stringify(args.data);
		var callUrl = 'http://localhost:57626/service';
		$.ajax({
			type:'POST',
			crossOrigin : true,
			data : data,
			dataType : "json",
			url : callUrl,
			beforeSend: function () {},
			complete:  function () {},
			success : function(data){
				var code = data.code;
				var msg = data.msg;
				var result = data.result;

				if(10000 == code){
					var html = '';
					var alertMsg = '';
					if(result != null && result != ""){
						for(var i=0;i<result.length;i++){
							var validateFileName = true;
							var fileName = result[i].fileName;
							var filePath = result[i].filePath;
							var fileFullPath = result[i].fileFullPath;
							if($('#' + obj_id).find('li').length == 0){
								html += agent.gridLocalFile(fileName , fileFullPath ,'Y');
							}else{
								for(var x=0; x<$('#' + obj_id).find('li').length; x++){
									if($('#' + obj_id).find('li').eq(x).find('span').text() == fileName){
										validateFileName = false;
										break;
									}
								}
								if(validateFileName){
									html += agent.gridLocalFile(fileName , fileFullPath ,'Y');
								}else{
									if(alertMsg != ''){
										alertMsg += ', ';
									}
									alertMsg += fileName;
								}
							}
						}
					}
					if(alertMsg != ""){
						alert(alertMsg +"과 같은 파일이 존재하여 목록에서 제외합니다.");
					}
					$('#' + obj_id).append(html);

				}
			},
			fail:function(data){},
			error : function(xhr, status, error){}
		});
	},
	gridLocalFile : function(fileName , fileFullPath , newFileYn){
		
		var html = '<li>';
		html += '<input name="newFileYn" type="hidden" value="' + newFileYn + '"/>'
		html += '<input name="filePath" type="hidden" value="' + fileFullPath + '"/>'
		html += '<span>' + fileName + '</span>';
		html += '<a href="#" onclick="javascript_:this.parentNode.parentNode.removeChild(this.parentNode);return false;" class="buttonG35">삭제</a>'
		html += '</li>';
		return html;
	},
	gridServerFile : function(fileName , serverPath , idx , newFileYn , regrId , sessionId){
		serverPath = replaceAll(serverPath,"\\","/");
		
		var html = '<li>';
		html += '<input name="newFileYn" type="hidden" value="' + newFileYn + '"/>'
		html += '<input name="filePath" type="hidden" value=""/>'
		html += '<input name="serverPath" type="hidden" value="' + serverPath + '"/>'
		html += '<span><a href="#none" onclick="agent.downloadFile(\'' + serverPath + '\',\'' + idx + '\')" >' + fileName + '</span>';
		if(sessionId == regrId){
			html += '<a href="#" onclick="javascript_:this.parentNode.parentNode.removeChild(this.parentNode);agent.removeFile(\'' + serverPath + '\');return false;" class="buttonG35">삭제</a>'
		}
		html += '</li>';
		return html;
	},
	uploadFile : function(args){
		var obj_id = args.obj_id;
		var target_id = args.target_id;
		var type = args.type;
		var creator_id = args.creator_id;
		var path = "/" + type + "/" +  target_id;
		var obj = $('#' + obj_id).find('li');

		var args = {"type":"ftp_uploadFile","hash":"sha256","src":[],"dst":{"path":path},"user":{"target_id":target_id,"type":type,"place":"test","creator_id":creator_id}};
		var index = 0;
		for(var i=0;i<obj.length;i++){
			var filePath = obj.eq(i).find('input[name=filePath]').val();
			var newFileYn = obj.eq(i).find('input[name=newFileYn]').val();
			if(newFileYn == 'Y'){
				args.src[index++] = filePath;
			}
		}


		var data = JSON.stringify(args);
		var callUrl = 'http://localhost:57626/service';
		$.ajax({
			type:'POST',
			crossOrigin : true,
			data : data,
			dataType : "json",
			url : callUrl,
			beforeSend: function () {},
			complete:  function () {},
			success : function(data){
				var code = data.code;
				var msg = data.msg;

				if(10000 == code){
				}
			},
			fail:function(data){},
			error : function(xhr, status, error){}
		});

	},
	downloadFile : function(path,idx){
		var args = {"type":"ftp_downloadFile","path":path,"idx":idx};
		console.log(args);

		var data = JSON.stringify(args);
		console.log(data);
		var callUrl = 'http://localhost:57626/service';
		$.ajax({
			type:'POST',
			crossOrigin : true,
			data : data,
			dataType : "json",
			url : callUrl,
			beforeSend: function () {},
			complete:  function () {},
			success : function(data){
				var code = data.code;
				var msg = data.msg;

				if(10000 == code){
				}
			},
			fail:function(data){},
			error : function(xhr, status, error){}
		});

	},
	removeFile : function(serverFilePath){
		/*if(!confirm('삭제하시겠습니까?')){
			return;
		}*/

		var args = {"type":"ftp_removeFile","src":[serverFilePath]};
		var data = JSON.stringify(args);
		var callUrl = 'http://localhost:57626/service';
		$.ajax({
			type:'POST',
			crossOrigin : true,
			data : data,
			dataType : "json",
			url : callUrl,
			beforeSend: function () {},
			complete:  function () {},
			success : function(data){
				var code = data.code;
				var msg = data.msg;

				if(10000 == code){
				}
			},
			fail:function(data){},
			error : function(xhr, status, error){}
		});
	}
}

function replaceAll(strTemp, strValue1, strValue2){
    while(1){
        if( strTemp.indexOf(strValue1) != -1 )
            strTemp = strTemp.replace(strValue1, strValue2);
        else
            break;
    }
    return strTemp;
}

