<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 28.
 * 2. 작성자 : sjw7240
 * 3. 화면명 : 공판 진행관리 목록
 * 4. 설명 : 공판 진행관리 목록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp"%>


<script type="text/javaScript" language="javascript">
var tabId;
var pgs = "pgs";

$(document).ready(function() {

	fn_setBtn(session_usergb);//권한별 버튼셋팅

	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

    gfn_calendarConfig("searchRegFromDt", "searchRegToDt", "minDate", "");
    gfn_calendarConfig("searchRegToDt", "searchRegFromDt", "maxDate", "");

    $('#searchRegToDt').val(gfn_dashDate(gfn_getCurDate(), "-"));
    $('#searchRegFromDt').val(gfn_dashDate(gfn_addMonth(gfn_getCurDate(), -1), "-"));

    var codeInfo = [{cdId:'C03', cd:comPgsStat ,selectId:'srcPgsStat',type:'2', callbackNm:'fn_codeCallBack', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});

});

/**
 * 공판요청관리목록 콤보박스 콜백함수
 * @param
 * @returns
 */
function fn_codeCallBack(){
	$('#srcPgsStat').prepend("<option value='"+comPgsStat+"' selected>전체</option>");

	$('#srcPgsStat').on('change',function(){
		fn_tralPgsSearch(1);
	});

	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_tralPgsSearch"});
}
/**
 * 공판요청관리목록 조회
 * @param {string} page 항목에 대한 고유 식별자
 * @returns fn_callBack
 */
function fn_tralPgsSearch(page){
	var callUrl = "<c:url value = "/tral/pgs/queryTralPgsMList.do"/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callBack',page:page,perPageNum:10});
}

 /**
  * 공판요청관리목록 조회 콜백함수
  * @param {object} data 조회한 결과데이터
  * @returns
  */
function fn_callBack(data){

	$("#tralReqList").empty();
	$("#totalcnt").text(data.totalCount);
	var listStr = "";

	if(data.tralPgsList.length < 1){
		$('#tralReqList').append('<tr><td colspan="14">조회된 결과가 없습니다.</td></tr>');
	}else{

		$.each(data.tralPgsList, function(index, item){

			listStr = "<tr><td><input type='checkbox' name='chk' id='chk_"+index+"' value='Y' class='check_agree1'/></td>";

			/*포렌식수사관 또는 포렌식수사과장*/
			listStr +=	"<td>"+gfn_nullRtnSpace(item.tstfReqSno)+"</td>"+
						"<td>"+gfn_nullRtnSpace(item.regDt)+
						"<input type='hidden' name='tstfReqSno' id='tstfReqSno' value='"+gfn_nullRtnSpace(item.tstfReqSno)+"'/></td>"+
						"<td>"+gfn_nullRtnSpace(item.deptNm)+"</td>"+
						"<td>"+gfn_nullRtnSpace(item.incdtNo)+"<input type='hidden' name='incdtSno' data-requireNm='사건일련번호' data-maxLength='10' title='사건일련번호' id='incdtSno' value='"+gfn_nullRtnSpace(item.incdtSno)+"'/></td>"+
						"<td><a href='#' onclick=javascript:fn_insTralPgs('"+gfn_nullRtnSpace(item.tstfReqSno)+"','"+gfn_nullRtnSpace(data.page)+"')><u><strong>"+gfn_nullRtnSpace(item.incdtNm)+"</strong></u></a></td>"+
						"<td>"+gfn_nullRtnSpace(item.regrNm)+"</td>"+
						"<td>"+gfn_nullRtnSpace(item.prosrNm);

			if(gfn_nullRtnSpace(item.prosrAprvDt) === '') {
				listStr += "";
			} else {
				listStr += "<br>("+gfn_nullRtnSpace(item.prosrAprvDt)+")";
			}

			listStr +=	"</td><td>";

			if(gfn_nullRtnSpace(item.rcvDt) === '') {
				listStr += "";
			} else {
				listStr += gfn_getCurDate().substr(0,4) + "-" + gfn_nullRtnSpace(item.rcvNo) + "<br>("+gfn_nullRtnSpace(item.rcvDt)+")";
			}

			listStr +=	"</td><td>"+gfn_nullRtnSpace(item.rcvrNm)+"</td>"+
						"<td>"+gfn_nullRtnSpace(item.aprvrNm);
			if (gfn_nullRtnSpace(item.aprvDt) === '') {
				listStr += '</td>';
			} else {
				listStr += "<br>("+gfn_nullRtnSpace(item.aprvDt) + ")</td>";
			}

			if (gfn_nullRtnSpace(item.pgsStat) === 'C03006') {
				listStr += "<td><a href='javascript:void(0);' class='myButton' onclick=javascript:fn_testiRes('" + gfn_nullRtnSpace(item.tstfReqSno) + "')>증언결과등록</a></td>";
			} else if(gfn_nullRtnSpace(item.tstfDt) !== ''){
				listStr += "<td><a href='javascript:void(0);' class='myButton' onclick=javascript:fn_testiRes('" + gfn_nullRtnSpace(item.tstfReqSno) + "')>증언결과보기</a></td>";
			} else {
				listStr += "<td></td>";
			}

			listStr += "<td>"+gfn_nullRtnSpace(item.pgsStatNm);

			switch (gfn_nullRtnSpace(item.pgsStat)) {
				case "C03001":
					listStr += "<br>(" + gfn_nullRtnSpace(item.regDt) + ")";
					break;
				case "C03002":
					listStr += "<br>(" + gfn_nullRtnSpace(item.updDt) + ")";
					break;
				case "C03003":
					listStr += "<br>(" + gfn_nullRtnSpace(item.prosrAprvDt) + ")";
					break;
				case "C03005":
					listStr += "<br>(" + gfn_nullRtnSpace(item.rcvDt) + ")";
					break;
				case "C03006":
					listStr += "<br>(" + gfn_nullRtnSpace(item.aprvDt) + ")";
					break;
				default:
					listStr += "";
					break;
			}

			listStr += "<input type='hidden' data-requireNm='진행상태' data-maxLength='6' title='진행상태' name='pgsStat' value='"+gfn_nullRtnSpace(item.pgsStat)+"'/></td></tr>";

			$('#tralReqList').append(listStr);
		});
	}

	data.__callFuncName__ ="fn_tralPgsSearch";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
}

  /**
   * 공판요청관리목록 등록화면이동
   * @param
   * @returns
   */
  function fn_insTralPgs(id){
  	var src = "<c:url value = "/tral/pgs/indexTralPgsRDtl.do"/>";

    if(id != null && id != ''){
		src = "<c:url value = "/tral/pgs/indexTralPgsUDtl.do"/>?tstfReqSno="+id+"&pgs=" + pgs;
    }
	requestUtil.setSearchForm("searchForm");
  	parent.$('#'+tabId+' iframe').attr('src', src);
  }

/**
* 공판요청관리 상세화면이동
* @param
* @returns
*/
function fn_detail(tstfReqSno,page){

	$('#tstfReqSno').val(tstfReqSno);
	var searchParam = $('#searchForm').serialize();
	var src = "<c:url value = "/tral/pgs/indexTralPgsUDtl.do"/>?page="+page+"&"+searchParam;
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 공판요청관리 권한별 버튼 셋팅
* @param
* @returns
*/
function fn_setBtn(userGb) {
	var btnStr = "";

	$("tr > td[colspan=3]").parent().attr("id", "trForen");
	$("#trForen").children(":first").text("증언일자");
	$("td[colspan=3]").attr("colspan", "1");
	btnStr += "<th scope='col' class='hcolor'>증언내용</th>"
			+ "<td scope='col'><input class='input20' type='text' name='realTstfCnts' id='realTstfCnts' data-requireNm='실증언내용' data-maxLength='4000' title='실증언내용' /></td>";
	$("#trForen").append(btnStr);
	btnStr = "";

	$("#theadColumn > th:nth-child(3)").text("작성일자");
	$("#theadColumn > th:nth-child(7)").text("요청자");
	$("#theadColumn > th:nth-child(9)").remove();
	//$("#theadColumn > th:nth-child(2)").remove();
	btnStr +=	'<th scope="col">접수번호</th>' +
				'<th scope="col">담당자</th>' +
				'<th scope="col">승인</th>' +
				'<th scope="col">증언결과</th>' +
				'<th scope="col">진행상태</th>';
	$("#theadColumn").append(btnStr);
	btnStr = "";

	if (userGb === 'C01003') {
		btnStr += "<a href='javascript:void(0);' class='myButton'>접수</a>";
		$("#btnRcv").append(btnStr);
		$("#btnRcv > a").attr("onclick", "fn_modPgsStat('rcv'); return false;");
		btnStr = "";
		$("li#btnRcvAprv").remove();

	} else if(userGb === 'C01004'){
		btnStr += "<a href='javascript:void(0);' class='myButton'>승인</a>";
		$("#btnRcvAprv").append(btnStr);
		$("#btnRcvAprv > a").attr("onclick", "fn_modPgsStat('aprv'); return false;");
		btnStr = "";
		$("li#btnRcv").remove();
	}
}

/**
* 공판지원요청 요청관리 진행상태 변경요청
* @param {string} reqType 버튼별 type
* @returns fn_pgsStatCallBack
*/
function fn_modPgsStat(reqType){
	var cnt = 0;
	var tpArray = [];
	var Objtype = {};
	var isChk = false;
	var tstfArray = [];
	var ask = "";

	$('#tralReqList > tr').each(function(index,item){

		var $chkbox = $(item).find('input[type=checkbox]');
		var $tstfReqSno = $(item).find('input[name=tstfReqSno]');
		var $pgsStat = $(item).find('input[name=pgsStat]');
		var $incdtSno = $(item).find('input[name=incdtSno]');

		if($chkbox.is(':checked') == true){
			cnt++;
			switch (reqType) {
			case "rcv":
				if($pgsStat.val() == 'C03003'){
					ask = "접수 하시겠습니까?";
					tpArray.push($tstfReqSno.val());
				}else{
					fn_showUserPage('진행상태가 검사승인만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "rcvCncl":
			case "aprv":
				if($pgsStat.val() == 'C03005'){
					ask = "승인 하시겠습니까?";
					tpArray.push($tstfReqSno.val());
				}else{
					fn_showUserPage('진행상태가 접수만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "aprvCncl":
			case "tstf":
				if($pgsStat.val() == 'C03006' && reqType!== 'tstf'){
					tpArray.push($tstfReqSno.val());
				}else if(reqType === 'tstf'){
					ask = "증언내용을 등록 하시겠습니까?";
				}else{
					fn_showUserPage('진행상태가 승인만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			}

		}

	});

	if(cnt < 1){
		fn_showUserPage('선택해주세요');
		return;
	}

	if(isChk){
		return;
	}

    var callUrl = "";
	if(reqType.slice(-4) == "Cncl"){
		fn_showModalPage("취소 하시겠습니까?", function () {
			Objtype.type = reqType;
			var callUrl = "<c:url value='/tral/req/delTralReqUDtl.do'/>";
			var data = {rowDatas : tpArray, type : Objtype};
			requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_pgsStatCallBack'});
		});
	} else {
		fn_showModalPage(ask, function () {
			Objtype.type = reqType;
			var callUrl = "<c:url value='/tral/req/queryPgsStat.do'/>";
			var data = {rowDatas : tpArray, type : Objtype};
			if(reqType === 'tstf'){
				tstfArray.push($("#tstfDt").val());
				tstfArray.push($("#tstfPlace").val());
				tstfArray.push($("#tstfrId").val());
				tstfArray.push($("#realTstfCnts").val());
				data = {rowDatas : tpArray, type : Objtype, tstfDatas : tstfArray};
			}
			requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_pgsStatCallBack'});
		});
	}


}

/**
* 공판관리 진행상태 변경 콜백
* @param
* @returns
*/
function fn_pgsStatCallBack(){
	fn_tralPgsSearch(1);
}

function fn_testiRes(id) {

	switch(gfn_nullRtnSpace(id)){
		case "":
			break;
		default:
			var src = "<c:url value = "/tral/pgs/indexTralPgsUDtl.do"/>?tstfReqSno="+id+"&tstf='tstf'&pgs="+pgs;
			requestUtil.setSearchForm("searchForm");
		  	parent.$('#'+tabId+' iframe').attr('src', src);
			break;
	}

}

</script>
</head>
<body>
	<div id="con_wrap">
		<div id="contents_info">
			<!--- contnets  적용 ------>
			<div>
				<div class="loca">
					<div class="ttl">공판 진행 관리</div>
					<div class="loca_list"></div>
				</div>

				<div class="sub">
					<!--------------검색------------------>
					<form id="searchForm" name="searchForm" onsubmit="return false;">
						<input type="hidden" class="" id="page" name="page" value="1"/>
						<div class="t_head">
							<table class="tbl_type_hd" border="1" cellspacing="0"
								onkeydown="if(gfn_enterChk())fn_tralPgsSearch(1);">
								<caption>검색</caption>
								<colgroup>
									<col width="10%">
									<col width="25%">
									<col width="10%">
									<col width="25%">
									<col width="10%">
									<col width="20%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="hcolor">사건번호</th>
										<td scope="col"><input class="input20" type="text"name="searchIncdtNo" id="searchIncdtNo" /></td>
										<th scope="col" class="hcolor">사건명</th>
										<td scope="col"><input class="input20" type="text"name="searchIncdtNm" id="searchIncdtNm" /></td>
										<th scope="col" class="hcolor">담당자</th>
										<td scope="col"><input class="input20" type="text"name="searchRegrNm" id="searchRegrNm" /></td>
									</tr>
									<tr>
										<th scope="col" class="hcolor">작성일자</th>
										<td scope="col"><input class="inpw20" type="text"name="searchRegFromDt" id="searchRegFromDt" /> <input class="inpw20" type="text" name="searchRegToDt"id="searchRegToDt" /></td>
										<th scope="col" class="hcolor">진행상태</th>
										<td scope="col" colspan="3"><select name="srcPgsStat"id="srcPgsStat" class="selw6"></select></td>
									</tr>
									<tr>
										<th scope="col" class="hcolor">요청번호</th>
										<td scope="col"><input class="input20" type="text"name="srcTstfReqSno" id="srcTstfReqSno" /></td>
										<th scope="col" class="hcolor">접수번호</th>
										<td scope="col"><input class="input20" type="text"name="rcvNoAcp" id="rcvNoAcp" /></td>
									</tr>
								</thead>
							</table>

						</div>
							<div class="btn_c">
								<ul>
									<li id="btnRcv"></li>
									<li id="btnRcvAprv"></li>
									<li><a href="javascript:void(0);" class='gyButton'onclick="fn_tralPgsSearch(1); return false;">조회</a></li>
								</ul>
							</div>
					</form>

					<!--------------//검색------------------>

					<!--------------결과------------------>
					<div class="r_num"></div>
					<!--------------목록---------------------->
					<div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:450px;">
						<table class="tbl_type" border="1" cellspacing="0">
							<caption>공판 진행 관리 목록</caption>
							<colgroup>
								<col width="2%">
								<col width="5%">
								<col width="5%">
								<col width="4%">
								<col width="10%">
								<col width="16%">
								<col width="7%">
								<col width="10%">
								<col width="9%">
								<col width="8%">
								<col width="6%">
								<col width="8%">
								<col width="9%">
							</colgroup>
							<thead>
								<tr id="theadColumn">
									<th scope="col">선택</th>
									<th scope="col">요청번호</th>
									<th scope="col">등록일자</th>
									<th scope="col">부서명</th>
									<th scope="col">사건번호</th>
									<th scope="col">사건명</th>
									<th scope="col">담당자</th>
									<th scope="col">주임검사<br>(승인일자)
									</th>
									<th scope="col">진행상태</th>
								</tr>
							</thead>
							<tbody id="tralReqList">
							</tbody>
						</table>
					</div>
					<!--------------//목록---------------------->

					<!-----------------------페이징----------------------->
					<div id='page_navi' class="page_wrap"></div>
					<!-----------------------//페이징----------------------->
				</div>

			</div>
		</div>
	</div>
	<script type="text/javaScript">
		var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
		var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
		ifa.attr('height', $("#contents_info").height()+120);
		//$(top.document).find("#container").width($(top.document).find("#container").width() - 5)
	</script>
</body>
</html>