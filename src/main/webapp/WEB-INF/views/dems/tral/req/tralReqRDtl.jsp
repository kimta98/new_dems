<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 5. 3.
 * 2. 작성자 : sjw7240
 * 3. 화면명 : 공판요청 등록/수정
 * 4. 설명 : 공판요청 등록/수정
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>
<link rel="stylesheet" href="<c:url value="/assets/css/style.css" />">
<script type="text/javaScript">
var reqInsttNm = '${reqInsttNm}';
var reqDepNm = '${reqDepNm}';
var reqInsttCd = '${reqInsttCd}';
var tstfReqSno = '${param.tstfReqSno}';
var state = '${param.state}';

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

	fn_btnSetting(tstfReqSno);

	gfn_overMaxLength("relIncdt",1300);
	gfn_overMaxLength("tstfReqCnts",1300);

	//요청기관,요청부서,요청기관코드 세팅
	if(reqInsttNm != ''){
		$('#reqInsttNm').text(reqInsttNm);
	}
	if(reqInsttCd != ''){
		$('#reqInsttCd').val(reqInsttCd);
		$('#insttCd').val(session_insttcd);
	}
	if(reqDepNm != ''){
		$('#reqDepNm').text(reqDepNm);
	}

	if(session_usergb == "C01003"){
		$("#prosrTd>button").hide();
	}

	gfn_calendarConfig("reqDt", "", "", "");

});
/**
 * 공판요청관리 콤보박스 콜백함수
 * @param
 * @returns
 */
function fn_codeCallBack(){
	$('#cfscGoodsDiv_1').prepend('<option value="" selected>선택</option>');
	$('#cfscDiv_1').prepend('<option value="" selected>선택</option>');
}

/**
 * 공판요청 등록 목록페이지이동
 * @param
 * @returns
 */
function fn_moveList(){

	var src = "<c:url value = "/tral/req/indexTralReqMList.do"/>";
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 공판요청 등록 시분 콤보박스생성
* @param
* @returns
*/
function fn_makeSbx(idx){
	var h = document.getElementById('hour_'+idx);
	var m = document.getElementById('minute_'+idx);
	for (var i=0; i < 24; i++) h.options[i] = new Option( f(i) , f(i));
	for (i=0; i < 60; i++) m.options[i] = new Option( f(i) , f(i));
	//h.value = '00';
	//m.value = '00';
	function f(n) {return n < 10 ? '0' + i : i; }
}

/**
* 공판요청 등록
* @param
* @returns
*/
function fn_save(type, status){

	// //validate
	// if(!fn_formValidate()){
	// 	return;
	// }

	//요청정보
	var $formDatas = $('#insForm').find('table').not('.iptTblX2');

	var formObj = new Object();

	$formDatas.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){

			if($(this).is('input[type=checkbox]')){
				if($(this).is(':checked')) {
					formObj[item.name] = item.value;
				}
			}else{
				formObj[item.name] = item.value;
			}
		});

	});

	//증언내용정보
	var $tstfReq = $('#insForm').find('table').not('.iptTblX');

	$tstfReq.each(function(index,item){

		var $el = $(item).find('textarea');

		$el.each(function(index,item){

			formObj[item.name] = item.value;
		});

	});
	//type 넣기
	formObj['type'] = type;

	//공판 관리 상세정보
	var tpArray = [];
	var dataObj = new Object();

	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){

			if($(this).is('input[type=checkbox]')){
				if($(this).is(':checked')) {
					dataObj[item.name] = item.value;
				}
			}else{
				dataObj[item.name] = item.value;
			}
		});

		if((index+1) % 2 == 0){
			tpArray.push(dataObj);
			dataObj = {};
		}

	});

	tpArray.forEach(function(obj){

		var cfscDate;
		var hour;
		var minute;

		$.each(obj,function(index,value){

			if(index == 'cfscDate'){
				cfscDate = value.replace(/-/gi, "");
			}
			if(index == 'hour'){
				hour = value;
			}
			if(index == 'minute'){
				minute = value;
			}

		});

		obj.cfscDt = cfscDate+hour+minute;

	});

	var data = {rowDatas : tpArray, formDatas : formObj};

	if(status == 'update'){
		fn_showModalPage("저장 하시겠습니까?", function() {
			var callUrl = "<c:url value='/tral/req/updTralReqUDtl.do'/>";
			requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_tralReqRDtlCallBack'});
		});

	} else if((status != 'update') && (type === 'save')){
		var chkCtn = 0;

		$formDatas.each(function(index,item){

			var $el = $(item).find('input, select, textarea');

			$el.each(function(index,item){

				if($(this).is('input[type=checkbox]')){
					if($(this).is(':checked')) {
						chkCtn++;
					}
				}else{
					if(!validUtil.checkInputValid({valFormID:item.name})){
						return;
					}
				}
			});

		});
		console.log(data);
		fn_showModalPage("저장 하시겠습니까?", function() {
			var callUrl = "<c:url value='/tral/req/tstReqTralReqRDtl.do'/>";
			requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_tralReqRDtlCallBack'});
		});
	} else if((status != 'update') && (type === 'req')){
		fn_showModalPage("승인요청 하시겠습니까?", function() {
			var callUrl = "<c:url value='/tral/req/tstReqTralReqRDtl.do'/>";
			requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_tralReqRDtlCallBack'});
		});
	}

}

function fn_tralReqRDtlCallBack(data){
	//목록이동
	fn_moveList();
}

function fn_openPop(type){
	var callUrl = "<c:url value = "/com/PageLink.do"/>?type="+type
    var jsp = (type == 'incdt' ? "dems/tral/req/incdtFindPop" : "dems/tral/req/crgrFindPop");
    var divId = (type == 'incdt' ? "divIncdtFindPop" : "divCrgrFindPop");

	requestUtil.mdPop({
		popUrl : callUrl+"&link="+jsp,
		height: 700,
        width: 1000,
        title: (type == 'incdt' ? '사건조회' : '담당자조회'),
        divId : divId
	});
}

/**
 * @!@ 사건관리 조회 팝업
 * @param cdId
 * @returns
 */
function fn_incdtIncdtMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtMListPop",
		height: 700,
        width: 1000,
        title: '사건 조회',
        divId : 'incdtIncdtMListPop'
	});
console.log(callUrl+"?link="+"dems/incdt/incdt/incdtIncdtMListPop");
}

/**
 * 사건관리 등록 팝업
 */
function fn_incdtIncdtRDtlPop() {
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtRDtlPop",
		height: 800,
        width: 1000,
        title: '사건 등록',
        divId : 'incdtIncdtRDtlPop'
	});
}

function fn_popCallBack(data, divId){

	$("#"+divId).dialog( "close" );
	$("#"+divId).empty();

	$.each(data, function(index, value){
		if($('#'+index).is('span')){
			$('#'+index).text(value);
		}else{
			$('#'+index).val(value);
		}
	});
}


/**
* 공판요청 수정콜백
* @param
* @returns
*/
function tralCallBack(data) {
	$(".sub_ttl").empty();
	$(".sub_ttl").text("법정 증언 요청 수정 화면");
	$(".fontred").attr("class", "");

	$("#btnSave").empty();
	var str = "";
	str = "<a href='javascript:void(0);' class='myButton'>저장</a>";
	$("#btnSave").append(str);
	$("#btnSave > a").attr("onclick", "fn_save('','" + state + "'); return false;");
	str = "";

	if(!session_usergb == 'C01003'){
		$("#btnAprv").empty();
		var str = "";
		str = "<a href='javascript:void(0);' class='myButton'>승인요청</a>";
		$("#btnAprv").append(str);
		$("#btnAprv > a").attr("onclick", "fn_save('req','" + state + "'); return false;");
		str = "";
	}else{
		$("#btnAprv").empty();
	}

	//checkbox세팅
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppDisk) == 'Y') $('#suppDisk').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppServer) == 'Y') $('#suppServer').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppMobile) == 'Y') $('#suppMobile').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppData) == 'Y') $('#suppData').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppCallDtl) == 'Y') $('#suppCallDtl').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppDiskRpr) == 'Y') $('#suppDiskRpr').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppEtc) == 'Y') $('#suppEtc').prop("checked", true);

	$('#incdtNm').val(gfn_nullRtnSpace(data.tralReqInfoMap.incdtNo)+"("+gfn_nullRtnSpace(data.tralReqInfoMap.incdtNm)+")");
	$('#reqDt').val(gfn_nullRtnSpace(data.tralReqInfoMap.regDt));
	$('#prosrNm').val(gfn_nullRtnSpace(data.tralReqInfoMap.prosrNm));
	$('#regUserNm').val(gfn_nullRtnSpace(data.tralReqInfoMap.regrNm) + " (" + gfn_nullRtnSpace(data.tralReqInfoMap.telNo) + ", " + gfn_nullRtnSpace(data.tralReqInfoMap.hpTelNo) + ")");
	$('#relIncdt').text(gfn_nullRtnSpace(data.tralReqInfoMap.relIncdt));
	$('#tstfReqCnts').text(gfn_nullRtnSpace(data.tralReqInfoMap.tstfReqCnts));
	$('#incdtSno').val(gfn_nullRtnSpace(data.tralReqInfoMap.incdtSno));
	$('#prosrId').val(gfn_nullRtnSpace(data.tralReqInfoMap.prosrId));
	$('#reqUserId').val(gfn_nullRtnSpace(data.tralReqInfoMap.reqUserId));

}

/**
 * 공판요청 등록 폼유효성체크
 * @param
 * @returns
 */
function fn_formValidate(){

	//상세정보 체크
	if($('#detailTbody > tr').length < 1){
		fn_showUserPage('지원 요청 내용을 넣어주세요');
		return false;
	}

	var $formDatas = $('#insForm').find('table').not('.iptTblX2');
	var flag = false;
	//*표시 null체크

	$formDatas.each(function(index,item){

		$(item).find('th > span.fontred').each(function(index,item){

				var $input = $(this).parent('th').next().find('input');

				if($input.length > 0){
					var chkValue = $(this).parent('th').next().find('input').val();
					var title = $(this).parent('th').next().find('input').attr('title');

					if( chkValue == null || chkValue == ''){
						fn_showUserPage(title + '을 입력해주세요', function() {
						});
						$input.focus();
						flag = true;
						return false;
					}
				}
		});
		if(flag)return false;

	});

	if(flag)return false;

	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input[type=text], select, textarea');

		$el.each(function(index,item){
			//널체크
			if($(this).val() == '' || $(this).val() == null){
				var title = $(this).attr('title');
				fn_showUserPage(title + '을 입력해주세요', function() {
					$(this).focus();
				});
				flag = true;
				return false;
			}else{

				//길이체크
				if($(this).is('input[type=text]') || $(this).is('textarea')){
					var maxlength = $(this).data().maxlength;
				    var elLength = $(this).val().length;
				    var title = $(this).attr('title');

					if(elLength > maxlength){
						fn_showUserPage(title + '는 '  + maxlength + '자 까지 가능합니다.', function(){
							$(this).focus();
						});
						flag = true;
						return false;
					}
				}

			}

		});

		if(flag) return false;

	});

	if(flag)return false;

	return true;
}

function fn_btnSetting(sno) {
	if(sno != null && sno != ''){
		$("#tstfReqSno").val(sno);

		var callUrl = "<c:url value = "/tral/req/queryTralReqInfo.do"/>";
		requestUtil.search({callUrl:callUrl,srhFormNm:'insForm',setFormNm:'modForm',callbackNm:'tralCallBack'});
	} else {
		var appendBtn = "<a href='javascript:void(0);' class='myButton'>저장</a>";
		if(session_usergb == 'C01003'){
			$("#btnAprv").empty();
		}
		$("#reqDt").val(gfn_getDate());
		$("#btnSave").append(appendBtn);
		$("#btnSave > a").attr("onclick", "fn_save('save',''); return false;");
	}
}
</script>
</head>
<body>
<div id="con_wrap1">
	<div class="content">
		<div id="contents_info">
			<div class="sub_ttl">법정 증언 요청 등록 화면</div><!-----타이틀------>
				<form name="insForm" id="insForm" method="post" onsubmit="return false;">
				<div class="sub">
					<div class="t_list">
						<table class="iptTblX">
							<caption>등록하기</caption>
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<tbody>
								<tr>
									<input type="hidden" name="tstfReqSno" id="tstfReqSno" data-requireNm='증언요청일련번호' data-maxLength='10' title='증언요청일련번호' />
									<th scope="row">
										요청기관<span class="fontred">*</span>
									</th>
									<td>
										<span id="reqInsttNm"></span>
										<input type="hidden" name="reqInsttCd" id="reqInsttCd" data-requireNm='요청기관코드' data-maxLength='10' title='요청기관코드' />
										<input type="hidden" name="insttCd" id="insttCd" />
									</td>
									<th scope="row">
										요청부서<span class="fontred">*</span>
									</th>
									<td>
										<span id="reqDepNm"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										요청일시<span class="fontred">*</span>
									</th>
									<td>
										<input class="inpw60" title="요청일시" type="text" name="reqDt" id="reqDt" data-requireNm='요청일자' data-maxLength='8' title='요청일자'/>
									</td>
									<th scope="row">
										사건번호(사건명)<span class="fontred">*</span>
									</th>
									<td><input class="inpw60" title="사건번호(사건명)" type="text" name="incdtNm" id="incdtNm" readonly/>
										<input type="hidden" name="incdtSno" id="incdtSno" data-requireNm='사건일련번호' data-maxLength='10' title='사건일련번호'/>
										<button class="buttonG40" onclick="javascript:fn_incdtIncdtMListPop();return false;">검색</button>
									</td>
								</tr>
								<tr>
									<th scope="row">
										주임군검사<span class="fontred">*</span>
									</th>
									<td id="prosrTd"><input class="inpw60" title="주임군검사" type="text" name="prosrNm" id="prosrNm" readonly/>
									    <button class="buttonG40" onclick="javascript:fn_openPop('prosr');return false;">검색</button>
										<input type="hidden" name="prosrId" id="prosrId" data-requireNm='주임군검사ID' data-maxLength='20' title='주임군검사ID'/>
									</td>
									<th scope="row">
										담당자(연락처,HP)<span class="fontred">*</span>
									</th>
									<td><input class="inpw60" title="담당자연락처" type="text" name="regUserNm" id="regUserNm" readonly/>
										<input type="hidden" name="reqUserId" id="reqUserId" data-requireNm='요청자ID' data-maxLength='20' title='요청자ID'/>
										<button class="buttonG40" onclick="javascript:fn_openPop('regUser');return false;">검색</button>
									</td>
								</tr>
								<tr>
									<th scope="row">지원구분(증언대상)<span class="fontred">*</span></th>
									<td colspan="3">
										<input type="checkbox" name="suppDisk" id="suppDisk" title="디스크" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_디스크' data-maxLength='1' title='지원구분_디스크' /><label for="suppDisk" style="display: inline;">디스크</label>
										<input type="checkbox" name="suppServer" id="suppServer" title="서버DB" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_서버_DB' data-maxLength='1' title='지원구분_서버_DB' /><label for="suppServer" style="display: inline;">서버DB</label>
										<input type="checkbox" name="suppMobile" id="suppMobile" title="모바일" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_모바일' data-maxLength='1' title='지원구분_모바일' /><label for="suppMobile" style="display: inline;">모바일</label>
										<input type="checkbox" name="suppData" id="suppData" title="데이터" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_데이터' data-maxLength='1' title='지원구분_데이터' /><label for="suppData" style="display: inline;">데이터</label>
										<input type="checkbox" name="suppCallDtl" id="suppCallDtl" title="통화내역" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_통화내역' data-maxLength='1' title='지원구분_통화내역' /><label for="suppCallDtl" style="display: inline;">통화내역</label>
										<!-- <input type="checkbox" name="suppDiskRpr" id="suppDiskRpr" title="디스크수리" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_디스크수리' data-maxLength='1' title='지원구분_디스크수리' /><label for="suppDiskRpr" style="display: inline;">디스크수리</label> -->
										<input type="checkbox" name="suppEtc" id="suppEtc" title="기타" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_기타' data-maxLength='1' title='지원구분_기타' /><label for="suppEtc" style="display: inline;">기타</label>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="sub_ttl">지원요청 내용</div>
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:400px;">
						<table class="iptTblX2">
							<caption>공판요청 상세정보 조회</caption>
							<colgroup>
								<col width="15%" />
								<col width="85%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col" rowspan="2">
										관련사건<span class="fontred">*</span>
									</th>
									<td scope="col" rowspan="2">
										<textarea name="relIncdt" id="relIncdt" data-requireNm='관련사건' data-maxLength='4000' title='관련사건' cols="30" rows="2"></textarea>
									</td>
								</tr>
							</thead>
							<tbody id="detailTbody">
								<tr>
									<th scope="col">
										증언요청 내용<span class="fontred">*</span>
									</th>
									<td scope="col">
										<textarea name="tstfReqCnts" id="tstfReqCnts" data-requireNm='증언요청내용' data-maxLength='4000' title='증언요청내용' cols="30" rows="2"></textarea>
									</td>
								</tr>
							</tbody>
						</table>
						<!-- <span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>

					<div class="btn_c">
						<ul>
							<li id="btnSave"></li>
							<li id="btnAprv">
								<a href="javascript:void(0);" class='myButton' onclick="fn_save('req', ''); return false;">승인요청</a>
							</li>
							<li>
								<a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a>
							</li>
						</ul>
					</div>

			</div>
			</form>
		</div>
	</div>
</div>
<div>
	<table class="iptTblX3">
		<tbody id="callTralReqSno"></tbody>
	</table>
</div>
<div id="divIncdtFindPop"></div>
<div id="divCrgrFindPop"></div>
<div id="incdtIncdtMListPop"></div>
<div id="incdtIncdtRDtlPop"></div>
<!-- <script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
var height= ifa.get(0).contentWindow.document.body.scrollHeight;
ifa.attr('height', height > 750 ?  height : 1750);
</script> -->
</body>
</html>