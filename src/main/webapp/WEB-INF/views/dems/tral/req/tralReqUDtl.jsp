<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 20.
 * 2. 작성자 : sjw7240
 * 3. 화면명 : 공판 요청 관리 상세
 * 4. 설명 : 공판 요청 관리 상세
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<script type="text/javaScript">
var tstfReqSno = '${param.tstfReqSno}';
var pgs = '${param.pgs}';
var fileArray = [];
//ftp 전송되는값
var ftpFileArray = [];
var fileSno = '';
var atchFileId = '';
var path = '';
var nodeObj;
var tstf = '${param.tstf}';

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

$(document).ready(function() {
	gfn_calendarConfig("tstfDt", "", "", "");
	fn_tralReqModSearch();
});

function fn_formValidate(){
	
	//상세정보 체크	
	if($('#detailTbody > tr').length < 1){
		fn_showUserPage('분석대상 상세정보를 넣어주세요');
		return false;
	}
	
	var $formDatas = $('#insForm').find('table').not('.iptTblX2');
	var flag = false;
	//*표시 null체크
	
	$formDatas.each(function(index,item){

		$(item).find('th > span.fontred').each(function(index,item){
			
				var $input = $(this).parent('th').next().find('input');
				
				if($input.length > 0){
					var chkValue = $(this).parent('th').next().find('input').val();
					var title = $(this).parent('th').next().find('input').attr('title');

					if( chkValue == null || chkValue == ''){
						fn_showUserPage(title + '을 입력해주세요');
						$input.focus();
						flag = true;
						return false; 
					}
				}
		});
		if(flag)return false;

	});
	
	if(flag)return false;
	
	$('#detailTbody > tr').each(function(index,item){
		
		var $el = $(item).find('input[type=text], select, textarea');
		
		$el.each(function(index,item){
			//널체크
			if($(this).val() == '' || $(this).val() == null){
				
				if($(this).is('textarea')){
					
					if($(this).attr('readonly') == 'readonly'){
						return;
					}
					
				}
				
				var title = $(this).attr('title');
				fn_showUserPage(title + '을 입력해주세요');
				$(this).focus();
				flag = true;
				return false; 
			}else{

				//길이체크				
				if($(this).is('input[type=text]') || $(this).is('textarea')){
					var maxlength = $(this).data().maxlength;
				    var elLength = $(this).val().length;
				    var title = $(this).attr('title');
				    
					if(gfn_checkByte(elLength) > maxlength){
						fn_showUserPage(title + '는 '  + maxlength + '자 까지 가능합니다.');
						$(this).focus();
						flag = true;
						return false; 
					}
				}
				
			}
			
		});
		
		if(flag) return false;

	});
	
	if(flag)return false;
	
	return true;
	
}
/**
 * 공판지원요청수정 콤보박스 콜백함수
 * @param
 * @returns
 */
function fn_codeCallBack(data){

	$.each(data.codeInfo,function(index,value){
		$('#'+value.selectId).prepend('<option value="" selected>선택</option>');									
	});
	
	if(data.codeInfo[0].dataList == undefined){
		return;
	}	
	
	//태그에 값 넣어주기
	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			for(var k in data.codeInfo[0].dataList){				
				
				if(item.name == k){
					if($(this).is('select')){
						$(this).val(data.codeInfo[0].dataList[k]).prop("selected", true);
					}else{
						item.value = data.codeInfo[0].dataList[k];	
					}						
				}
			}				

		});

	});
	
	$('input[name=cfscDt]').val('');
}

/**
 * 공판지원요청등록 목록페이지이동
 * @param
 * @returns
 */
function fn_moveList(){
	//검색조건 세팅
	var src = "<c:url value = "/tral/req/indexTralReqMList.do"/>";

	if (gfn_nullRtnSpace(pgs) === 'pgs') {
		src += "?pgs=" + pgs;
	}

	parent.$('#'+tabId+' iframe').attr('src', src);
}


/**
 * 공판지원요청 수정페이지이동
 * @param
 * @returns
 */
function fn_moveUpdate(id, state) {

  	var src = "<c:url value = "/tral/req/indexTralReqRDtl.do"/>?tstfReqSno=" + id + "&state="+state;
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 공판지원요청등록 시분 콤보박스생성
* @param
* @returns
*/
function fn_makeSbx(idx){
	var h = document.getElementById('hour_'+idx);
	var m = document.getElementById('minute_'+idx);
	for (var i=0; i < 24; i++) h.options[i] = new Option( f(i) , f(i));
	for (i=0; i < 60; i++) m.options[i] = new Option( f(i) , f(i));
	//h.value = '00';
	//m.value = '00';
	function f(n) {return n < 10 ? '0' + i : i; }
}

/**
* 공판지원요청수정 수정
* @param
* @returns
*/
function fn_mod(){
	
	$('#tstfReqSno').val(tstfReqSno);
	//공판지원요청정보
	var $formDatas = $('#modForm').find('table').not('.iptTblX2');

	var formObj = new Object();

	$formDatas.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			
			if($(this).is('input[type=checkbox]')){
				if($(this).is(':checked')) {
					formObj[item.name] = item.value;				
				}
			}else{
				formObj[item.name] = item.value;				
			}
		});

	});

	//공판대상 상세정보
	var tpArray = [];
	var dataObj = new Object();

	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input, select, textarea');
		
		$el.each(function(index,item){
			
			if($(this).is('input[type=checkbox]')){
				if($(this).is(':checked')) {
					dataObj[item.name] = item.value;				
				}
			}else{
				dataObj[item.name] = item.value;				
			}
		});

		if((index+1) % 2 == 0){
			tpArray.push(dataObj);
			dataObj = {};
		}

	});
	
	//cfscDt 세팅
	tpArray.forEach(function(obj){
		
		var cfscDate;
		var hour;
		var minute;
		
		$.each(obj,function(index,value){
			
			if(index == 'cfscDate'){
				cfscDate = value.replace(/-/gi, "");
			}
			if(index == 'hour'){
				hour = value;
			}
			if(index == 'minute'){
				minute = value;
			}
										
		});
		
		obj.cfscDt = cfscDate+hour+minute; 
		
	});

	var data = {rowDatas : tpArray, formDatas : formObj};
    var callUrl = "<c:url value='/tral/req/updTralReqUDtl.do'/>";
}

function fn_modCallBack(){

		fn_moveList();
	
}

function fn_openPop(type){
	var callUrl = "<c:url value = "/com/PageLink.do"/>?type="+type;
    var jsp = '';
    var divId = '';
	var title = '';
	switch (type) {
		case 'incdt':
			jsp = "dems/tral/req/incdtFindPop";
			divId = "divIncdtFindPop";
			title = "사건조회";
			break;
	
		case 'tstf':
			jsp = "dems/tral/req/tstfFindPop";
			divId = "divTstfFindPop";
			title = "증언담당자조회";
			break;

		default:
			jsp = "dems/tral/req/crgrFindPop";
			divId = "divCrgrFindPop";
			title = "담당자조회";
			break;
	}
			
	requestUtil.mdPop({
		popUrl : callUrl+"&link="+jsp,
		height: 600,
        width: 1000,
		title: title,
        divId : divId
	});
}

/**
 * @!@ 사건관리 조회 팝업
 * @param cdId
 * @returns 
 */
function fn_incdtIncdtMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtMListPop&flag=update",
		height: 700,
        width: 1000,
        title: '사건 조회',
        divId : 'incdtIncdtMListPop'
	});

}

/**
 * 사건관리 등록 팝업
 */
function fn_incdtIncdtRDtlPop() {
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtRDtlPop",
		height: 800,
        width: 1000,
        title: '사건 등록',
        divId : 'incdtIncdtRDtlPop'
	});
}

function fn_popCallBack(data, divId){
	
	$("#"+divId).dialog( "close" );
	$("#"+divId).empty();
	
	$.each(data, function(index, value){
		if($('#'+index).is('span')){
			$('#'+index).text(value);
		}else{
			$('#'+index).val(value);
		}
	});

}

/**
* 공판지원요청 수정상세 조회
* @param
* @returns 
*/
function fn_tralReqModSearch(){

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");
	
	var input = document.createElement('input'); 
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "tstfReqSno");
	input.setAttribute("value", tstfReqSno);
	searchForm.appendChild(input);	
	
	document.body.appendChild(searchForm);
	
	var callUrl = "<c:url value = "/tral/req/queryTralReqInfo.do"/>";	
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'modForm',callbackNm:'tralReqModCallBack'});
}

/**
* 공판지원요청 수정상세 콜백함수
* @param
* @returns data
*/
function tralReqModCallBack(data){

	var title = '';

	if (gfn_nullRtnSpace(pgs) === 'pgs') {
		title = '법정 증언 요청';
	} else if(gfn_nullRtnSpace(pgs) === '') {
		title = '법정 증언 요청 상세 화면';
	}

	$('#contents_info').children(":first").text(title);

	var li = $("#moveUpdateBtn");
	var str = "";

	var liAprv = $("#btnAprv");
	liAprv.empty();

	switch (gfn_nullRtnSpace(data.tralReqInfoMap.pgsStat)) {
		case "C03001":
			if(gfn_nullRtnSpace(session_usergb) == "C01001"){
				str = "<a href='javascript:void(0);' class='myButton'>수정</a>";
				li.append(str);
				$("#moveUpdateBtn > a").attr("onclick", "fn_moveUpdate('" + gfn_nullRtnSpace(data.tralReqInfoMap.tstfReqSno) + "', 'update'); return false;");
				
				str = "<a href='javascript:void(0);' class='myButton'>승인요청</a>";
				liAprv.append(str);
				$("#btnAprv > a").attr("onclick", "fn_modPgsStat('req'); return false;");
			}
			break;

		case "C03002":
			switch (gfn_nullRtnSpace(session_usergb)) {
				case "C01001":
					// str = "<a href='javascript:void(0);' class='myButton'>수정</a>";
					// li.append(str);
					// $("#moveUpdateBtn > a").attr("onclick", "fn_moveUpdate('" + gfn_nullRtnSpace(data.tralReqInfoMap.tstfReqSno) + "', 'update'); return false;");
					
					str = "<a href='javascript:void(0);' class='myButton'>승인요청취소</a>";
					liAprv.append(str);
					$("#btnAprv > a").attr("onclick", "fn_modPgsStat('reqCncl'); return false;");
					break;

				case "C01002":
					str = "<a href='javascript:void(0);' class='myButton'>검사승인</a>";
					li.append(str);
					$("#moveUpdateBtn > a").attr("onclick", "fn_modPgsStat('prosrAprv'); return false;");
					
					break;
			}
			break;

		case "C03003":
			switch (gfn_nullRtnSpace(session_usergb)) {
				case "C01002":
					str = "<a href='javascript:void(0);' class='myButton'>검사승인취소</a>";
					liAprv.append(str);
					$("#btnAprv > a").attr("onclick", "fn_modPgsStat('prosrAprvCncl'); return false;");
					break;
			
				case "C01003":
					if($.urlParam('pgs') == 'pgs'){
						str = "<a href='javascript:void(0);' class='myButton'>접수</a>";
						liAprv.append(str);
						$("#btnAprv > a").attr("onclick", "fn_modPgsStat('rcv'); return false;");
					}else{
						str = "<a href='javascript:void(0);' class='myButton'>수정</a>";
						li.append(str);
						$("#moveUpdateBtn > a").attr("onclick", "fn_moveUpdate('" + gfn_nullRtnSpace(data.tralReqInfoMap.tstfReqSno) + "', 'update'); return false;");
					}
					break;
			}
			break;

		case "C03005":
			switch (gfn_nullRtnSpace(session_usergb)) {
				case "C01003":
					str = "<a href='javascript:void(0);' class='myButton'>접수취소</a>";
					liAprv.append(str);
					$("#btnAprv > a").attr("onclick", "fn_modPgsStat('rcvCncl'); return false;");
					break;
			
				case "C01004":
					str = "<a href='javascript:void(0);' class='myButton'>승인</a>";
					liAprv.append(str);
					$("#btnAprv > a").attr("onclick", "fn_modPgsStat('aprv'); return false;");
					break;
			}
			break;

		case "C03006":
			if (gfn_nullRtnSpace(data.tralReqInfoMap.tstfDt) === '' ) {
				switch (gfn_nullRtnSpace(session_usergb)) {
					case "C01004":
						str = "<a href='javascript:void(0);' class='myButton'>승인취소</a>";
						liAprv.append(str);
						$("#btnAprv > a").attr("onclick", "fn_modPgsStat('aprvCncl'); return false;");
						break;
				}
			}
			break;
	}

	//checkbox세팅	
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppDisk) == 'Y') $('#suppDisk').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppServer) == 'Y') $('#suppServer').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppMobile) == 'Y') $('#suppMobile').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppData) == 'Y') $('#suppData').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppCallDtl) == 'Y') $('#suppCallDtl').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppDiskRpr) == 'Y') $('#suppDiskRpr').prop("checked", true);
	if(gfn_nullRtnSpace(data.tralReqInfoChkMap.suppEtc) == 'Y') $('#suppEtc').prop("checked", true);

	if(data != null){
		
		$('#tstfReqSno').text(gfn_nullRtnSpace(data.tralReqInfoMap.tstfReqSno));
		$('.tstfReqSno').val(gfn_nullRtnSpace(data.tralReqInfoMap.tstfReqSno));
		$('#relIncdt').text(gfn_nullRtnSpace(data.tralReqInfoMap.relIncdt));
		$('#tstfReqCnts').text(gfn_nullRtnSpace(data.tralReqInfoMap.tstfReqCnts));
		$('#aprvNm').text(gfn_nullRtnSpace(data.tralReqInfoMap.aprvNm) + " " + gfn_nullRtnSpace(data.tralReqInfoMap.aprvDt));	
		$('#reqInsttNm').text(gfn_nullRtnSpace(data.tralReqInfoMap.insttNm));	
		$('#deptNm').text(gfn_nullRtnSpace(data.tralReqInfoMap.deptNm));	
		$('#regDt').text(gfn_nullRtnSpace(data.tralReqInfoMap.regDt));	
		$('#incdtSno').text(gfn_nullRtnSpace(data.tralReqInfoMap.incdtNo) + "\n(" + gfn_nullRtnSpace(data.tralReqInfoMap.incdtNm) + ")");
		$('.incdtSno').val(gfn_nullRtnSpace(data.tralReqInfoMap.incdtSno));
		$('#prosrNm').text(gfn_nullRtnSpace(data.tralReqInfoMap.prosrNm));
		$('#regrNm').text(gfn_nullRtnSpace(data.tralReqInfoMap.regrNm) + " (" + gfn_nullRtnSpace(data.tralReqInfoMap.telNo) + ", " + gfn_nullRtnSpace(data.tralReqInfoMap.hpTelNo) + ")");
		$('.pgsStat').val(gfn_nullRtnSpace(data.tralReqInfoMap.pgsStat));
		$('#tstfDt').val(gfn_nullRtnSpace(data.tralReqInfoMap.tstfDt));
		$('#tstfPlace').val(gfn_nullRtnSpace(data.tralReqInfoMap.tstfPlace));
		$('#tstfrNm').val(gfn_nullRtnSpace(data.tralReqInfoMap.tstfrNm));
		$('#tstfrId').val(gfn_nullRtnSpace(data.tralReqInfoMap.tstfrId));
		$('#realTstfCnts').val(gfn_nullRtnSpace(data.tralReqInfoMap.realTstfCnts));
		
		if (gfn_nullRtnSpace(data.tralReqInfoMap.prosrAprvDt) !== '') {
			$('#aprvProsrNm').text(gfn_nullRtnSpace(data.tralReqInfoMap.prosrNm) + " (" + gfn_nullRtnSpace(data.tralReqInfoMap.prosrAprvDt) + ")");			
		}
		if (gfn_nullRtnSpace(data.tralReqInfoMap.rcvDt) !== '') {
			$('#rcvrNm').text(gfn_nullRtnSpace(data.tralReqInfoMap.rcvrNm) + " (" + gfn_getCurDate().substr(0,4) + "-" + gfn_nullRtnSpace(data.tralReqInfoMap.rcvNo) + ", " + gfn_nullRtnSpace(data.tralReqInfoMap.rcvDt) + ")");	
		}
		if (gfn_nullRtnSpace(data.tralReqInfoMap.aprvDt)) {
			$('#aprvrNm').text(gfn_nullRtnSpace(data.tralReqInfoMap.aprvrNm) + " (" + gfn_nullRtnSpace(data.tralReqInfoMap.aprvDt) + ")");	
		}
	}
	if(gfn_nullRtnSpace(tstf) !== '' && gfn_nullRtnSpace(session_usergb) === 'C01003'){
		$("#btnAprv").empty();
		$("#btnTstf").append("<a href='javascript:void(0);' class='myButton'>저장</a>");
		$("#btnTstf > a").attr("onclick", "fn_modPgsStat('tstf'); return false;");
		$("input#tstfDt").val(gfn_getDate());
	} else if(gfn_nullRtnSpace(tstf) === ''){
		$("#tstfDiv").empty();
	} else if((gfn_nullRtnSpace(tstf) === 'tstf' && gfn_nullRtnSpace(session_usergb) === 'C01004') || gfn_nullRtnSpace(data.tralReqInfoMap.pgsStat) === 'C03006'){
		$("#tstfPlace").attr("readonly", "");
		$("#realTstfCnts").attr("readonly", "");
		$("#tstfrNm").attr("readonly", "");
		$("#tstfDt").attr("readonly", "");
		$("button[onclick='javascript:fn_openPop(\'tstf\');return false;']").attr('style', 'display:none;');
		$("th > span.fontred").text("");
		
	}

	//상세조회시 쓴 form 지우기
	$('form[name=searchForm]').remove();
	
	//regUserNm세팅
	$('#regUserNm').val(data.tralReqInfoMap.reqUserNm);
	
	fn_setFrameSize();
}

function fn_save(type) {

	var url = "<c:url value='/tral/req/updTralReqUDtl.do'/>";
	var data = {}
	var tpArray = [];
	var dataObj = new Object();
	dataObj[0] = '' + tstfReqSno;
	tpArray.push(dataObj);
	dataObj = {};

	data = {rowDatas : tpArray};

	if(type === 'req'){

		var $formAprv = $('.iptTblX3');

		var formObjAprv = new Object();

		$formAprv.each(function(index,item){

			var $elAprv = $(item).find('input[type=hidden]');

			$elAprv.each(function(index,item){
				
				formObjAprv[item.name] = item.value;
			});

		});

		var aprvStr = "<input type='hidden'  />";
		$(".iptTblX3").append();

		data = {rowDatas : tpArray, aprvDatas : formObjAprv};

	}

	fn_showModalPage("수정 하시겠습니까?", function () {
		requestUtil.saveData({callUrl:url, data:data, callbackNm:'fn_saveCallback'});	
	});

}


function fn_saveCallback(data) {
}


/**
* 공판지원요청 요청관리 진행상태 변경요청
* @param {string} reqType 버튼별 type
* @returns fn_pgsStatCallBack
*/
function fn_modPgsStat(reqType){
	var tpArray = [];
	var Objtype = {};	
	var isChk = false;

	var tstfReqSno = $('.tstfReqSno').val();
	var pgsStat = $('.pgsStat');
	var incdtSno = $('.incdtSno');
	
	switch (reqType) {
	/*승인요청을 누름*/
	case "req":
		if(pgsStat.val() == 'C03001'){
			ask = "검사 승인요청을 하시겠습니까?";
			tpArray.push(tstfReqSno);
		}else{
			fn_showUserPage('진행상태가 임시등록만 가능합니다.');
			isChk = true;
			return false;
		}
		break;
	/*검사승인을 누름*/
	case "prosrAprv":
	case "reqCncl":
		if(pgsStat.val() == 'C03002'){
			ask = "검사 승인을 하시겠습니까?";
			tpArray.push(tstfReqSno);
		}else{
			fn_showUserPage('진행상태가 승인요청만 가능합니다.');
			isChk = true;
			return false;
		}
		break;
	case "prosrAprvCncl":
	case "rcv":
		if(pgsStat.val() == 'C03003'){
			ask = "접수 하시겠습니까?";
			tpArray.push(tstfReqSno);
		}else{
			fn_showUserPage('진행상태가 검사승인만 가능합니다.');
			isChk = true;
			return false;
		}
		break;
	case "rcvCncl":
	case "aprv":
		if(pgsStat.val() == 'C03005'){
			ask = "승인 하시겠습니까?";
			tpArray.push(tstfReqSno);
		}else{
			fn_showUserPage('진행상태가 접수만 가능합니다.');
			isChk = true;
			return false;
		}
		break;
	case "aprvCncl":
	case "tstf":
		
		if(!fn_formValidate()){
			return;
		}
		
		if(pgsStat.val() == 'C03006' && reqType!== 'tstf'){
			tpArray.push(tstfReqSno);
		}else if(reqType === 'tstf'){
			ask = "증언내용을 등록 하시겠습니까?";
			tpArray.push(tstfReqSno);
			Objtype.incdtSno = incdtSno.val();
			Objtype.tstfDt = $("#tstfDt").val();
			Objtype.tstfPlace = $("#tstfPlace").val();
			Objtype.tstfrId = $("#tstfrId").val();
			Objtype.realTstfCnts = $("#realTstfCnts").val();
			Objtype.pgsStat = 'C03006';
		}else{
			fn_showUserPage('진행상태가 승인만 가능합니다.');
			isChk = true;
			return false;
		}
		break;
	}
	
	$('.iptTblX > tr').each(function(index,item){

	});
	
	if(isChk){
		return;
	}

	if(reqType.slice(-4) == "Cncl"){
		fn_showModalPage("취소 하시겠습니까?", function () {
			Objtype.type = reqType;
			var callUrl = "<c:url value='/tral/req/delTralReqUDtl.do'/>";
			var data = {rowDatas : tpArray, type : Objtype};
		    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_pgsStatCallBack'});
		});
	} else {
		fn_showModalPage(ask, function () {
			Objtype.type = reqType;
			var callUrl = "<c:url value='/tral/req/queryPgsStat.do'/>";
			var data = {rowDatas : tpArray, type : Objtype};
		    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_pgsStatCallBack'});
		});
	}
	
}

function fn_pgsStatCallBack() {
	fn_moveList();
}
</script>
</head>
<body>
<div id="con_wrap1">
	<div class="content">
		<div id="contents_info">
			<div class="sub_ttl"></div><!-----타이틀------>
				<form name="insForm" id="insForm" method="post" onsubmit="return false;">
					<div class="sub">
						<div class="t_list">
							<table class="iptTblX">
								<caption>상세</caption>
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<tbody>
									<tr>
										<th scope="row">
											요청번호
										</th>
										<td>
											<span id="tstfReqSno" data-requireNm='증언요청일련번호' data-maxLength='10' title='증언요청일련번호'></span>
											<input type="hidden" name="tstfReqSno" class="tstfReqSno" data-requireNm='증언요청일련번호' data-maxLength='10' title='증언요청일련번호' />
											<input type="hidden" name="pgsStat" class="pgsStat" data-requireNm='진행상태' data-maxLength='6' title='진행상태' />
										</td>
										<th scope="row">
											검사(승인일자)
										</th>
										<td>
											<span id="aprvProsrNm"></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											요청기관
										</th>
										<td>
											<span id="reqInsttNm"></span>
										</td>
										<th scope="row">
											요청부서
										</th>
										<td>
											<span id="deptNm"></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											요청일시
										</th>
										<td>
											<span id="regDt"></span>
										</td>
										<th scope="row">
											사건번호(사건명)
										</th>
										<td>
											<span id="incdtSno" data-requireNm='사건일련번호' data-maxLength='10' title='사건일련번호'></span>
											<input type="hidden" name="incdtSno" class="incdtSno" data-requireNm='사건일련번호' data-maxLength='10' title='사건일련번호' />
										</td>
									</tr>
									<tr>
										<th scope="row">
											주임군검사
										</th>
										<td>
											<span id="prosrNm"></span>
										</td>
										<th scope="row">
											담당자(연락처,HP)
										</th>
										<td>
											<span id="regrNm"></span>
										</td>
									</tr>
									<tr>
										<th scope="row">지원구분(증언대상)</th>
										<td colspan="3" id="chk">
											<input type="checkbox" name="suppDisk" id="suppDisk" title="디스크" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_디스크' data-maxLength='1' title='지원구분_디스크' disabled/><label for="suppDisk" style="display: inline;">디스크</label>
											<input type="checkbox" name="suppServer" id="suppServer" title="서버DB" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_서버_DB' data-maxLength='1' title='지원구분_서버_DB' disabled/><label for="suppServer" style="display: inline;">서버DB</label>
											<input type="checkbox" name="suppMobile" id="suppMobile" title="모바일" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_모바일' data-maxLength='1' title='지원구분_모바일' disabled/><label for="suppMobile" style="display: inline;">모바일</label>
											<input type="checkbox" name="suppData" id="suppData" title="데이터" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_데이터' data-maxLength='1' title='지원구분_데이터' disabled/><label for="suppData" style="display: inline;">데이터</label>
											<input type="checkbox" name="suppCallDtl" id="suppCallDtl" title="통화내역" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_통화내역' data-maxLength='1' title='지원구분_통화내역' disabled/><label for="suppCallDtl" style="display: inline;">통화내역</label>
											<!-- <input type="checkbox" name="suppDiskRpr" id="suppDiskRpr" title="디스크수리" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_디스크수리' data-maxLength='1' title='지원구분_디스크수리' disabled/><label for="suppDiskRpr" style="display: inline;">디스크수리</label> -->
											<input type="checkbox" name="suppEtc" id="suppEtc" title="기타" value="Y" class="check_agree1" style="margin-left: 10px;" data-requireNm='지원구분_기타' data-maxLength='1' title='지원구분_기타' disabled/><label for="suppEtc" style="display: inline;">기타</label>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="sub_ttl">지원요청 내용</div>
						<div class="t_list">
							<table class="iptTblX2">
								<caption>공판요청 상세정보 조회</caption>
								<colgroup>
									<col width="15%" />
									<col width="85%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" rowspan="2">
											관련사건
										</th>
										<td scope="col" rowspan="2">
											<textarea name="relIncdt" id="relIncdt" cols="30" rows="2" data-requireNm='관련사건' data-maxLength='4000' title='관련사건' readonly></textarea>
										</td>
									</tr>
								</thead>
								<tbody id="detailTbody">
									<tr>
										<th scope="col">
											증언요청 내용
										</th>
										<td scope="col">
											<textarea name="tstfReqCnts" id="tstfReqCnts" cols="30" rows="2" data-requireNm='증언요청내용' data-maxLength='4000' title='증언요청내용' readonly></textarea>
										</td>
									</tr>
								</tbody>
							</table>
							<br />
							<table class="iptTblX">
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="row">
											담당자 (접수번호, 접수일자)
										</th>
										<td>
											<span id="rcvrNm"></span>
										</td>
										<th scope="row">
											승인자 (승인일자)
										</th>
										<td>
											<span id="aprvrNm"></span>
										</td>
									</tr>
								</thead>
							</table>
						</div>

						<div id="tstfDiv">
							<div class="sub_ttl">증언결과</div>
							<div class="t_list">
								<table class="iptTblX">
									<caption>증언결과</caption>
									<colgroup>
										<col width="15%" />
										<col width="35%" />
										<col width="15%" />
										<col width="35%" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">
												증언일자<span class="fontred">*</span>
											</th>
											<td scope="row"><input type="text" class="inpw20" name="tstfDt" id="tstfDt" data-requireNm="증언일시" data-maxLength="22" title="증언일시" /></td>
											<th scope="row">
												증언장소<span class="fontred">*</span>
											</th>
											<td scope="row">
												<input type="text" name="tstfPlace" id="tstfPlace" data-requireNm="증언장소" data-maxLength="500" title="증언장소" />
											</td>
										</tr>
										<tr>
											<th scope="row">
												증언담당자<span class="fontred">*</span>
											</th>
											<td scope="row" colspan="3">
												<input type="text" name="tstfrNm" id="tstfrNm" title="증언자" readonly/>
												<input type="hidden" name="tstfrId" id="tstfrId" data-requireNm="증언자ID" data-maxLength="20" title="증언자ID" />
												<button class="buttonG40" onclick="javascript:fn_openPop('tstf');return false;">검색</button>
											</td>
										</tr>
										<tr>
											<th scope="row" rowspan="3">
												증언내용<span class="fontred">*</span>
											</th>
											<td scope="row" colspan="3" rowspan="3">
												<textarea id="realTstfCnts" name="realTstfCnts" data-requireNm="실증언내용" data-maxLength="4000" title="실증언내용"></textarea>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="btn_c">
							<ul>
								<li id="moveUpdateBtn">
								</li>
								<li id="btnAprv">
									<a href="javascript:void(0);" class='myButton' onclick="fn_save('req'); return false;">승인요청</a>
								</li>
								<li id="btnTstf">
								</li>
								<li>
									<a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a>
								</li>
							</ul>
						</div>
						<input type="hidden" name="aprv" id="aprv" />
				</div>
			</form>
		</div>
	</div>
</div>
<div>
	<table class="iptTblX3">
		<tbody id="callTralReqSno"></tbody>
	</table>
</div>
<div id="divIncdtFindPop"></div>
<div id="divCrgrFindPop"></div>
<div id="divTstfFindPop"></div>
<div id="incdtIncdtMListPop"></div>
<div id="incdtIncdtRDtlPop"></div>
<script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
var height= ifa.get(0).contentWindow.document.body.scrollHeight;
ifa.attr('height', height > 750 ?  height : 1750);
</script>
</body>
</html>