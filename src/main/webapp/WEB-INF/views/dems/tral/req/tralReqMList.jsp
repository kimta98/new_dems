<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 28.
 * 2. 작성자 : sjw7240
 * 3. 화면명 : 공판 요청관리 목록
 * 4. 설명 : 공판 요청관리 목록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp"%>


<script type="text/javaScript" language="javascript">
var tabId;

$(document).ready(function() {

 	fn_setBtn(session_usergb);//권한별 버튼셋팅

	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

    gfn_calendarConfig("searchRegFromDt", "searchRegToDt", "minDate", "");
    gfn_calendarConfig("searchRegToDt", "searchRegFromDt", "maxDate", "");

    $('#searchRegToDt').val(gfn_dashDate(gfn_getCurDate(), "-"));
    $('#searchRegFromDt').val(gfn_dashDate(gfn_addMonth(gfn_getCurDate(), -1), "-"));

    var codeInfo = [{cdId:'C03',cd:comPgsStat,selectId:'srcPgsStat',type:'2', callbackNm:'fn_codeCallBack', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
    // fn_ajaxCodeList(codeInfo);
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});

});

/**
 * 공판요청관리목록 콤보박스 콜백함수
 * @param
 * @returns
 */
function fn_codeCallBack(){
	$('#srcPgsStat').prepend("<option value='"+comPgsStat+"' selected>전체</option>");
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_tralReqSearch"});
}
/**
 * 공판요청관리목록 조회
 * @param {string} page 항목에 대한 고유 식별자
 * @returns fn_callBack
 */
function fn_tralReqSearch(page){
	var callUrl = "<c:url value = "/tral/req/queryTralReqMList.do"/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callBack',page:page,perPageNum:10});
}

 /**
  * 공판요청관리목록 조회 콜백함수
  * @param {object} data 조회한 결과데이터
  * @returns
  */
function fn_callBack(data){

	$("#tralReqList").empty();
	$("#totalcnt").text(data.totalCount);
	var listStr = "";

	if(data.tralReqList.length < 1){
		$('#tralReqList').append('<tr><td colspan="10">조회된 결과가 없습니다.</td></tr>');
	}else{

		$.each(data.tralReqList, function(index, item){

			listStr = "<tr><td><input type='checkbox' name='chk' id='chk_"+index+"' value='Y' class='check_agree1'/></td>";

			/*일반수사관 또는 검사일때*/
			listStr +=	"<td>"+gfn_nullRtnSpace(item.tstfReqSno)+"<input type='hidden' data-requireNm='증언요청일련번호' data-maxLength='10' title='증언요청일련번호' name='tstfReqSno' id='tstfReqSno' value='"+gfn_nullRtnSpace(item.tstfReqSno)+"'/></td>"+
						"<td>"+gfn_nullRtnSpace(item.regDt)+"</td>"+
						"<td>"+gfn_nullRtnSpace(item.deptNm)+"</td>"+
						"<td><input type='hidden' data-requireNm='사건일련번호' data-maxLength='10' title='사건일련번호' name='incdtSno' id='incdtSno' value='"+gfn_nullRtnSpace(item.incdtSno)+"'/>"+
						"<a href='#' onclick=javascript:fn_insTralReq('"+gfn_nullRtnSpace(item.tstfReqSno)+"','"+gfn_nullRtnSpace(data.page)+"')><u><strong>" + gfn_nullRtnSpace(item.incdtNo)+"</strong></u></a></td>"+
						"<td><a href='#' onclick=javascript:fn_insTralReq('"+gfn_nullRtnSpace(item.tstfReqSno)+"','"+gfn_nullRtnSpace(data.page)+"')><u><strong>"+gfn_nullRtnSpace(item.incdtNm)+"</strong></u></a></td>"+
						"<td>"+gfn_nullRtnSpace(item.rcvNo)+"</td>"+
						"<td>"+gfn_nullRtnSpace(item.regrNm)+"</td>"+
						"<td>"+gfn_nullRtnSpace(item.prosrNm);

			if(gfn_nullRtnSpace(item.prosrAprvDt) !== '') {
				listStr += "<br>("+gfn_nullRtnSpace(item.prosrAprvDt)+")</td>";
			}
				listStr += "<td>"+gfn_nullRtnSpace(item.pgsStatNm);

			switch (gfn_nullRtnSpace(item.pgsStat)) {
				case 'C03001':
					listStr += "<br>("+gfn_nullRtnSpace(item.regDt);
					break;
				case 'C03002':
					listStr += "<br>("+gfn_nullRtnSpace(item.updDt);
					break;
				case 'C03003':
					listStr += "<br>("+gfn_nullRtnSpace(item.prosrAprvDt);
					break;
				case 'C03005':
					listStr += "<br>("+gfn_nullRtnSpace(item.rcvDt);
					break;
				case 'C03006':
					listStr += "<br>("+gfn_nullRtnSpace(item.aprvDt);
					break;
			}

			listStr += ")<input type='hidden' data-requireNm='진행상태' data-maxLength='6' title='진행상태' name='pgsStat' value='"+gfn_nullRtnSpace(item.pgsStat)+"'/></td></tr>";

			$('#tralReqList').append(listStr);
		});
	}

	data.__callFuncName__ ="fn_tralReqSearch";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
}

  /**
   * 공판요청관리목록 등록화면이동
   * @param
   * @returns
   */
function fn_insTralReq(id){
	var src = "<c:url value = "/tral/req/indexTralReqRDtl.do"/>";

	if(id != null && id != ''){
		src = "<c:url value = "/tral/req/indexTralReqUDtl.do"/>?tstfReqSno="+id;
	}
	requestUtil.setSearchForm("searchForm");
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 공판요청관리 상세화면이동
* @param
* @returns
*/
function fn_detail(tstfReqSno,page){

	$('#tstfReqSno').val(tstfReqSno);
	var searchParam = $('#searchForm').serialize();
	var src = "<c:url value = "/tral/req/indexTralReqUDtl.do"/>?page="+page+"&"+searchParam;
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 공판요청관리 권한별 버튼 셋팅
* @param
* @returns
*/
function fn_setBtn(userGb) {
	var btnStr = "";

  //일반수사관
  if(userGb === 'C01001'){
    $("#btnReq > a").css('display' , 'block');
	$("li#btnProsrAprv").remove();
	$("li#btnProsrAprvCncl").remove();
    $(".RdButton").css('display' , 'block');

  } else if(userGb === 'C01002'){
    $("#btnProsrAprv > a").css('display' , 'block');
    $("#btnProsrAprvCncl > a").css('display' , 'block');
	$("li#btnReq").remove();
	$("li > a.RdButton").parent().remove();

  } else if(userGb === 'C01003'){
	  $(".RdButton").css('display' , 'block');
  }


}

/**
* 공판지원요청 요청관리 진행상태 변경요청
* @param {string} reqType 버튼별 type
* @returns fn_pgsStatCallBack
*/
function fn_modPgsStat(reqType){
	var cnt = 0;
	var tpArray = [];
	var Objtype = {};
	var isChk = false;
	var ask = "";

	var tstfReqSnoStr = "";
	var incdtSnoStr = "";
	var pgsStatStr = "";
	var typeStr = "";
	var userIdStr = "";

	$('#tralReqList > tr').each(function(index,item){

		var $chkbox = $(item).find('input[type=checkbox]');
		var $tstfReqSno = $(item).find('input[name=tstfReqSno]');
		var $pgsStat = $(item).find('input[name=pgsStat]');
		var $incdtSno = $(item).find('input[name=incdtSno]');

		if($chkbox.is(':checked') == true){
			switch (reqType) {
			case "req":
				if($pgsStat.val() == 'C03001'){
				tpArray.push($tstfReqSno.val());
				}else{
					fn_showUserPage('진행상태가 임시등록만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "prosrAprv":
			case "reqCncl":
				if($pgsStat.val() == 'C03002'){
				tpArray.push($tstfReqSno.val());
				}else{
					fn_showUserPage('진행상태가 승인요청만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "prosrAprvCncl":
				if($pgsStat.val() == 'C03003'){
				tpArray.push($tstfReqSno.val());
				}else{
					fn_showUserPage('진행상태가 검사승인만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			}
			cnt++;

		}

	});

	switch (reqType) {
		case "req":
			ask = "검사 승인요청을 하시겠습니까?";
			break;
		case "prosrAprv":
			ask = "검사 승인을 하시겠습니까?";
			break;
	}

	if(cnt < 1){
		fn_showUserPage('선택해주세요');
		return;
	}

	if(isChk){
		return;
	}


	if(reqType.slice(-4) == "Cncl"){
		fn_showModalPage("취소 하시겠습니까?", function () {
			Objtype.type = reqType;
			var callUrl = "<c:url value='/tral/req/delTralReqUDtl.do'/>";
			var data = {rowDatas : tpArray, type : Objtype};
			requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_pgsStatCallBack'});
		});
	} else {
		fn_showModalPage(ask, function () {
			Objtype.type = reqType;
			var callUrl = "<c:url value='/tral/req/queryPgsStat.do'/>";
			var data = {rowDatas : tpArray, type : Objtype};
			requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_pgsStatCallBack'});
		});
	}
}

/**
* 공판관리 진행상태 변경 콜백
* @param
* @returns
*/
function fn_pgsStatCallBack(){
	fn_tralReqSearch(1);
}

function fn_testiRes(id) {
	src = "<c:url value = "/tral/req/indexTralReqUDtl.do"/>?tstfReqSno="+id+"&tstf='tstf'";
	requestUtil.setSearchForm("searchForm");
  	parent.$('#'+tabId+' iframe').attr('src', src);
}

</script>
</head>
<body>
	<div id="con_wrap">
		<div id="contents_info">
			<!--- contnets  적용 ------>
			<div>
				<div class="loca">
					<div class="ttl">공판 요청 관리</div>
					<div class="loca_list"></div>
				</div>

				<div class="sub">
					<!--------------검색------------------>
					<form id="searchForm" name="searchForm" onsubmit="return false;">
						<input type="hidden" name="tstfReqSno" id="tstfReqSno" />
						<input type="hidden" class="" id="page" name="page" value="1"/>
						<div class="t_head">
							<table class="tbl_type_hd" border="1" cellspacing="0"
								onkeydown="if(gfn_enterChk())fn_tralReqSearch(1);">
								<caption>검색</caption>
								<colgroup>
									<col width="10%">
									<col width="25%">
									<col width="10%">
									<col width="25%">
									<col width="10%">
									<col width="20%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="hcolor">사건번호</th>
										<td scope="col"><input class="input20" type="text"name="searchIncdtNo" id="searchIncdtNo" /></td>
										<th scope="col" class="hcolor">사건명</th>
										<td scope="col"><input class="input20" type="text"name="searchIncdtNm" id="searchIncdtNm" /></td>
										<th scope="col" class="hcolor">담당자</th>
										<td scope="col"><input class="input20" type="text"name="searchRegrNm" id="searchRegrNm" /></td>
									</tr>
									<tr>
										<th scope="col" class="hcolor">요청번호</th>
							            <td scope="col" >
							          		<input class="input20" type="text" name="srcTstfReqSno" id="srcTstfReqSno"/>
							            </td>
							            <th scope="col" class="hcolor">접수번호</th>
							            <td scope="col" >
							            	<input class="input20" type="text" name="rcvNoAcp" id="rcvNoAcp"/>
							            </td>
							            <th scope="col" class="hcolor">진행상태</th>
										<td scope="col" colspan="3"><select name="srcPgsStat"id="srcPgsStat" class="selw6" onchange="fn_tralReqSearch(1)"></select></td>
									<tr>
										<th scope="col" class="hcolor">작성일자</th>
										<td scope="col"><input class="inpw20" type="text"name="searchRegFromDt" id="searchRegFromDt" /> <input class="inpw20" type="text" name="searchRegToDt"id="searchRegToDt" /></td>
										<th scope="col" class="hcolor"></th>
							            <td scope="col" ></td>
							            <th scope="col" class="hcolor"></th>
							            <td scope="col" ></td>
									</tr>
								</thead>
							</table>

						</div>
							<div class="btn_c">
								<ul>
									<li id="btnProsrAprv"><a href="javascript:void(0);"class='myButton' style="display: none;" onclick="fn_modPgsStat('prosrAprv');return false;">검사승인</a></li>
									<li id="btnProsrAprvCncl"><a href="javascript:void(0);"class='myButton' style="display: none;" onclick="fn_modPgsStat('prosrAprvCncl');return false;">승인취소</a></li>
									<li id="btnReq"><a href="javascript:void(0);"class='myButton' style="display: none;" onclick="fn_modPgsStat('req');return false;">승인요청</a></li>
									<li><a href="javascript:void(0);" class='RdButton'  style="display: none;" onclick="fn_insTralReq(); return false;">등록</a></li>
									<li><a href="javascript:void(0);" class='gyButton'onclick="fn_tralReqSearch(1); return false;">조회</a></li>
								</ul>
							</div>
					</form>

					<!--------------//검색------------------>

					<!--------------결과------------------>
					<div class="r_num">
						| 결과 <strong id="totalcnt" style="color: #C00"></strong>건
					</div>
					<!--------------목록---------------------->
					<div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:450px;">
						<table class="tbl_type" border="1" cellspacing="0">
							<caption>공판 요청 관리 목록</caption>
							<colgroup>
								<col width="3%">
								<col width="9%">
								<col width="9%">
								<col width="9%">
								<col width="15%">
								<col width="20%">
								<col width="5%">
								<col width="10%">
								<col width="10%">
								<col width="10%">
							</colgroup>
							<thead>
								<tr id="theadColumn">
									<th scope="col">선택</th>
									<th scope="col">요청번호</th>
									<th scope="col">등록일자</th>
									<th scope="col">부서명</th>
									<th scope="col">사건번호</th>
									<th scope="col">사건명</th>
									<th scope="col">접수번호</th>
									<th scope="col">담당자</th>
									<th scope="col">주임검사<br>(승인일자)
									</th>
									<th scope="col">진행상태</th>
								</tr>
							</thead>
							<tbody id="tralReqList">
							</tbody>
						</table>
					</div>
					<!--------------//목록---------------------->

					<!-----------------------페이징----------------------->
					<div id='page_navi' class="page_wrap"></div>
					<!-----------------------//페이징----------------------->
				</div>

			</div>
		</div>
	</div>
	<script type="text/javaScript">
		var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
		var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
		ifa.attr('height', $("#contents_info").height()+120);
		//$(top.document).find("#container").width($(top.document).find("#container").width() - 5)
	</script>
</body>
</html>