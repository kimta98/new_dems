<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 디지털 증거 관리 > 증거 처리 신청 조회
 * 4. 설명 : 디지털 증거 관리 > 증거 처리 신청 조회
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript">
// var flag = true;
// var perPageNum;
// var tabId;
var userGb = "${session_usergb}";
//alert("===userGb====>>>"+userGb);

$(document).ready(function() {
	
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
// 	var codeInfo = [{cdId:'C03',selectId:'schPgsStat',type:'1', callbackNm:'fn_ajaxPgsStatCallback', sqlQueryId:''},{cdId:'C05',selectId:'schCfscGoodsDiv' ,type:'1'}];
// 	fn_ajaxCodeList(codeInfo);
	var codeInfo = [{cdId:'C03',cd:comPgsStat,selectId:'schPgsStat',type:'2', callbackNm:'fn_ajaxPgsStatCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'},{cdId:'C05',selectId:'schCfscGoodsDiv' ,type:'1'}];
	//var codeInfo = [{cdId:'C03',cd:comPgsStat,selectId:'schPgsStat',type:'2', callbackNm:'fn_ajaxPgsStatCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	fn_ajaxCodeList(codeInfo);
	
	gfn_calendarConfig("schStReqDt", "schEdReqDt", "minDate", "");    <%/* 대여시작일자 from */%>
	gfn_calendarConfig("schEdReqDt", "schStReqDt", "maxDate", "");   <%/* 대여시작일자 to */%>
	
	
	
});


function fn_ajaxPgsStatCallback(data){
 	//$('#schPgsStat option:eq(0)').before("<option value='' selected>전체</option>");
 	$('#schPgsStat').prepend("<option value='"+comPgsStat+"' selected>전체</option");
 	$('#schCfscGoodsDiv option:eq(0)').before("<option value='' selected>전체</option>");
 	
 	//requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_queryMListCallback', page:$("#page").val(), perPageNum:10});
 	
 	
 	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_searchEvdcTrtMList"});
 	//fn_searchEvdcTrtMList(1);
 	fn_dispCont();
}

/**
 * 달력 콜백함수(없으면 삭제가능) 
 * @param {string} prgID
 * @returns 
 */
function fn_callBack(){
}


/**
 * @!@ 지털 증거 관리 > 증거 처리 신청 조회 처리
 * @param {int} page
 * @returns 
 */
function fn_searchEvdcTrtMList(page){
// 	debugger;
// 	var callUrl = "<c:url value='/evdc/trt/queryEvdcTrtMList.do'/>";
	
// 	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_searchEvdcTrtMListCallback', page:page, perPageNum:10});
	
// 	var callUrl = "<c:url value='/fbbs/ntc/queryFBbsNtcMList.do'/>";	
// 	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callback',page:page,perPageNum:10});

	var callUrl = "<c:url value='/evdc/trt/queryEvdcTrtMList.do'/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_searchEvdcTrtMListCallback', page:$("#page").val(), perPageNum:10});
	
}

 /**
  * 지털 증거 관리 > 증거 처리 신청 조회 처리 콜백
  * @param {json} data
  * @returns 
  */
 function fn_searchEvdcTrtMListCallback(data){
//  	debugger;
	console.log("=========11111111111111 zzzzzzzzzzzzzzzzzzzz=======>>>");
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listEvdcTrt > colgroup").find("col").length;
	//alert(data.totalCount);
	$("#listEvdcTrt > tbody").empty();
	
 	$("#totalcnt").text(data.totalCount);
	
  	if(listCnt < 1){
  		var append = "";
		append += "<tr>";
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		append += "</tr>";
		$("#listEvdcTrt > tbody").append(append);
	}else{
	 	
		$.each(list,function(idx,row){
	 		var append = "";
	 		append += "<tr>";
			
			
// 	 		<th scope="col">선택</th>
//             <th scope="col">요청번호</th>
//             <th scope="col">등록일자</th>
//             <th scope="col">부서명</th>
//             <th scope="col">사건번호</th>
//             <th scope="col">접수번호</th>
//             <th scope="col">증거구분</th>
//             <th scope="col">모델명</th>
//             <th scope="col">시리얼번호</th>
//             <th scope="col">압수일시</th>
//             <th scope="col">요청구분</th>
//             <th scope="col">요청자</th>
//             <th scope="col">주임검사<br>(승인일자)</th>
//             <th scope="col">접수번호</th>
//             <th scope="col">담당자</th>
//             <th scope="col">승인</th>
//             <th scope="col">진행상태</th>
//             <th scope="col">증거상태</th>
	 		

	 		append +='<td style="text-align:center"><input type="checkbox" name="chk" id="chk" value="Y" class="check_agree1" ></td>';
// 	 		append += "<td>" + row.rnum + "</td>";
			append += "<td>"; 
	 		append +=  row.disuseReqSno+"<input type='hidden' name='selDisuseReqSno' id='selDisuseReqSno' value='"+row.disuseReqSno+"'/>";
//  	 		append +=  "<input type='hidden' name='pgsStat' id='pgsStat' value='"+gfn_nullRtnSpace(row.pgsStat)+"'/>";     incdtNo
	 		append +=  "</td>";
	 		append += "<td>" + gfn_dashDate2(gfn_nullRtnSpace(row.dispRegDt),'-') + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.dispDeptNm) + "</td>";
	 		append += "<td><a href='javascript:void(0)' onclick=javascript:fn_searchDetail('"+row.disuseReqSno+"','"+data.page+"','"+data.pgsStat+"')><u>"+row.incdtNo+"</u></a></td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.rcvNoAcp) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.cfscGoodsDivNm) +"("+gfn_nullRtnSpace(row.cfscDivNm)+ ")</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.mdlNm) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.serlNo) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.dispCfscDt) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.reqDivNm) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.reqUserNm) + "</td>";
	 		
	 		if(!gfn_isNull(gfn_nullRtnSpace(row.prosrAprvDt))){ //주임검사(승인일자)
	 			append += "<td>" + gfn_nullRtnSpace(row.prosrNm) +"<br>("+gfn_dashDate2(gfn_nullRtnSpace(row.prosrAprvDt),'-')+")</td>";
	 		}else{
	 			append += "<td>&nbsp;</td>"
	 		}
	 		
	 		if(!gfn_isNull(gfn_nullRtnSpace(row.rcvNo))){ //접수번호
	 			append += "<td>" + gfn_nullRtnSpace(row.dispRcvNo) +"<br>("+gfn_dashDate2(gfn_nullRtnSpace(row.rcvDt),'-')+ ")</td>";
	 		}else{
	 			append += "<td>&nbsp;</td>"
	 		}
	 		
	 		if(!gfn_isNull(gfn_nullRtnSpace(row.rcvrNm))){ //담당자
	 			append += "<td>" + gfn_nullRtnSpace(row.rcvrNm)+"</td>";
	 		}else{
	 			append += "<td>&nbsp;</td>"
	 		}
	 		
	 		if(!gfn_isNull(gfn_nullRtnSpace(row.aprvDt))){ //승인
	 			append += "<td>"+row.aprvrNm +"<br>("+gfn_dashDate2(row.aprvDt,'-')+")<input type='hidden' name='pgsStat' value='"+row.pgsStat+"'/></td>";  //C03006
	 		}else{
	 			append += "<td>&nbsp;</td>"
	 		}
	 		
	 		if(!gfn_isNull(gfn_nullRtnSpace(row.pgsStatNm))){ //진행상태
	 			append += "<td>"+row.pgsStatNm+"<br>"+row.psgStatDt+"<input type='hidden' name='pgsStat' value='"+row.pgsStat+"'/></td>";  //C03006
	 		}else{
	 			append += "<td>&nbsp;</td>"
	 		}
	 		
	 		if(!gfn_isNull(gfn_nullRtnSpace(row.evdcPgsStat))){ //증거상태
	 			append += "<td>" + gfn_nullRtnSpace(row.evdcPgsStat) +"<br>"+gfn_nullRtnSpace(row.evdcPgsStatDt)+ "</td>"; //evdcPgsStatDt
	 		}else{
	 			append += "<td>&nbsp;</td>"
	 		}
	 		console.log("=========11111111111111 evdcPgsStatDt=======>>>"+row.evdcPgsStatDt);
// 	 		if(row.pgsStat=="C09002"){
// 	 			append += "<td>" + gfn_nullRtnSpace(row.rentCfrmrId) +"<br/>("+gfn_dashDate2(gfn_nullRtnSpace(row.rentCfrmDt),'-')+ ")</td>";
// 	 		}else{
// 	 			append += "<td>&nbsp;</td>"
// 	 		}
	 		
// 	 		if(row.pgsStat=="C09003"){
// 	 			append += "<td>" + gfn_nullRtnSpace(row.rtnCfrmrId) +"<br/>("+gfn_dashDate2(gfn_nullRtnSpace(row.rtnCfrmrDt),'-')+ ")</td>";
// 	 		}else{
// 	 			append += "<td>&nbsp;</td>"
// 	 		}
	 		
	 		append += "</tr>";
	        $("#listEvdcTrt > tbody").append(append);
	  	});
	}
  	
  	data.__callFuncName__ ="fn_searchEvdcTrtMList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
 	
 } 
 
function fn_insEvdcDisuseReq(){
	
	//var flag = "MntList";
	//parent.$('#tabs-M000000702').find("iframe").attr("src", '<c:url value="/eqp/lend/indexEqpLendLendReqRDtl.do"/>?eqpSno='+eqpSno+'&flag='+flag);
// 	console.log("=======fn_insEqpLend ST=======>>>");
// 	parent.$('#tabs-M000000702').find("iframe").attr("src", '<c:url value="/eqp/lend/indexEqpLendLendReqRDtl.do"/>');
// 	console.log("=======fn_insEqpLend ED=======>>>"); <input type="hidden" id="modFlag" name="modFlag" value="insert" />
	
	var modFlag = "insert";
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/evdc/apln/indexEvdcAplnRDtl.do"/>?modFlag='+modFlag);
}


function fn_searchDetail(disuseReqSno, page, pgsStat){
// debugger;
	
// 	parent.$tabId.attr("src", '<c:url value="/eqp/lend/indexEqpLendLendReqUDtl.do"/>?rentAplnSno='+rentAplnSno);
	
	$('#disuseReqSno').val(disuseReqSno);
	
	
	if(session_usergb == "C01003" ){ //수사관
		$('#modFlag').val("rcv");
	}else{							 //수사과장
		$('#modFlag').val("aprv");
	}
	
	console.log("=======disuseReqSno zzzz=====>>>"+$('#disuseReqSno').val());
	var searchParam = $('#searchForm').serialize();	
	console.log("=======disuseReqSno searchParam=====>>>"+searchParam);
	//var src = "<c:url value = "/eqp/lend/indexEqpLendLendReqUDtl.do"/>?page="+page+"&"+searchParam;
	
	var src="";
	
	//(사용자구분 C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)  
// 	if(session_usergb == "C01002" || session_usergb == "C01999"){
// 		src = "<c:url value = "/evdc/apln/indexEvdcAplnVDtl.do"/>?page="+page+"&"+searchParam;	
// 	}else{
// 		if(!gfn_isNull(pgsStat) && pgsStat > 'C03002'){
// 			//상세화면으로 이동
// 			src = "<c:url value = "/evdc/apln/indexEvdcAplnVDtl.do"/>?page="+page+"&"+searchParam;	
// 		}else{
// 			//수정화면으로 이동
// 			src = "<c:url value = "/evdc/apln/indexEvdcAplnUDtl.do"/>?page="+page+"&"+searchParam;	
// 		}	
// 	}
	src = "<c:url value = "/evdc/trt/indexEvdcTrtVDtl.do"/>?page="+page+"&"+searchParam;	
	requestUtil.setSearchForm("searchForm");
	parent.$('#'+tabId+' iframe').attr('src', src);	
	 
}

function fn_rtnEqpLend(reqType){
	var chkCnt = 0;
	$('#listEvdcTrt :input[name=chk]').each(function(index) {
		//console.log("====fn_selMgmt	index========>>>"+index);
		 if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
			 chkCnt++;
			 console.log("========fn_rtnEqpLend	selRentAplnSno222===========>>>>>"+$("input[name^=selRentAplnSno]").eq(index+1).val());
		 }
	});
	
	if(chkCnt < 1){
		fn_showUserPage("반납처리할 내역을 선택하십시요.");
		return;
	}
	
	fn_modifyEqpLend(reqType, chkCnt);
}

function fn_aprvEvdcTrt(reqType){
	var msg = "";
	if(reqType=="frsRcv"){ //접수
		msg = "접수할";
	}else if(reqType=="frsAprv"){ //승인
		msg = "승인할";
	}
	
	var chkCnt = 0; //체크한 건수
	var chkRcvCnt = 0; //접수 가능한 건수
	var chkNotRcvCnt = 0; //접수 불가능한 건수
	var chkAprvCnt = 0; //승인 가능한 건수
	var chkNotAprvCnt = 0; //승인 불가능한 건수
	
	//증거신청목록 진행상태 조회 (진행상태코드 C03001:임시등록, C03002:승인요청, C03003:검사승인, C03004:반려, C03005:문서접수, C03006:과장승인)
	
	$('#listEvdcTrt :input[name=chk]').each(function(index) {
		//console.log("====fn_selMgmt	index========>>>"+index);
		 if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
			 chkCnt++;
			 if($("input[name^=pgsStat]").eq(index).val()=="C03003"){
				 chkRcvCnt++;
			 }else{ 
				 chkNotRcvCnt++;
			 }
			 if($("input[name^=pgsStat]").eq(index).val()=="C03005"){
				 chkAprvCnt++;
			 }else{
				 chkNotAprvCnt++;
			 }
		 }
	});
	
	console.log("====fn_aprvEvdcApln chkRcvCnt====>>>"+chkRcvCnt+"\n====fn_aprvEvdcApln chkNotRcvCnt====>>>"+chkNotRcvCnt);
	console.log("====fn_aprvEvdcApln chkAprvCnt====>>>"+chkAprvCnt+"\n====fn_aprvEvdcApln chkNotAprvCnt====>>>"+chkNotAprvCnt);
	
	if(chkCnt < 1){
		fn_showUserPage(msg+" 내역을 선택하십시요.");
		return;
	}
	
	if(reqType=="frsRcv"){ //검사승인
		if(chkRcvCnt < 1){
			fn_showUserPage("접수 가능한 내역만 선택하세요.\n진행상태가 검사승인일 경우에만 접수가 가능합니다.");
			return;
		}else if(chkRcvCnt > 0 && chkNotRcvCnt > 0){
			fn_showUserPage("접수 가능한 내역만 선택하세요.\n진행상태가 검사승인일 경우에만 접수가 가능합니다.");
			return;
		}
	}else if(reqType=="frsAprv"){
		if(chkAprvCnt < 1){
			fn_showUserPage("승인이 가능한 내역만 선택하세요.\n진행상태가 접수일 경우에만 승인이 가능합니다.");
			return;
		}else if(chkAprvCnt > 0 && chkNotAprvCnt > 0){
			fn_showUserPage("승인이 가능한 내역만 선택하세요.\n진행상태가 접수일 경우에만 승인이 가능합니다.");
			return;
		}
	}
	
	
	fn_modifyEvdcTrt(reqType, chkCnt);
}

/**
* 증거처리내역 접수, 승인
* @param
* @returns
*/
function fn_modifyEvdcTrt(reqType, chkCnt){
	var chkCnt = 0;
	var tpArray = [];
	var Objtype = {};	
	var isChk = false;
	var curRentAplnSno = "";
	var nextRentAplnSno = "";
	var arr = new Array();
	var arr2 = new Array();
	var i = 0;
	var compareDisuseReqSno = "";
	var tpArray = [];
	var tpArray2 = [];
	
	var msg = "";
	if(reqType=="frsRcv"){ //접수
		msg = "접수하시겠습니까?";
	}else if(reqType=="frsAprv"){ //승인
		msg = "승인하시겠습니까?";
	}
	
	//alert(reqType);
	
	$('#listEvdcTrt :input[id=chk]').each(function(index) {
		//console.log("========compareRentAplnSno 1111111===>>>"+i);
		if($("input:checkbox[id=chk]").eq(index).is(":checked")==true){
		//if($("input[name='chk']").prop("checked")==true){
		//if($("input[name='chk']").attr("checked")==true){
		//if($("input[name='chk']").attr("checked")==true){	
			console.log("========99999999999999999=======>>>"+$("input[id^=selDisuseReqSno]").eq(index).val());
			tpArray.push($("input[name^=selDisuseReqSno]").eq(index).val());
		}
	});
	
	for(var i=0;i<tpArray.length;i++){
		console.log("========tpArray["+i+"]=======>>>"+tpArray[i]);
	}
	
	//console.log("========tpArray.length===>>>"+tpArray.length);
	
	
	
	for(var i=0;i<tpArray.length;i++){
		//console.log("========tpArray["+i+"]=======>>>"+tpArray[i]);
		compareDisuseReqSno = tpArray[i];
		console.log("========current compareDisuseReqSno ======>>>"+compareDisuseReqSno);
		
		var chkDupCnt2 = 0;
		for(var y=0;y<tpArray2.length;y++){
			console.log("========tpArray2["+y+"] ======>>>"+tpArray2[y]);
			if(tpArray2[y] == compareDisuseReqSno){
				//tpArray2[i] = compareRentAplnSno
				chkDupCnt2++;
				//console.log("========chkDupCnt2 ======>>>"+chkDupCnt2);
			}
		}
		
		console.log("========chkDupCnt2["+i+"] =======>>>"+chkDupCnt2);
		
		if(chkDupCnt2 < 1){
// 			tpArray2[i] = compareRentAplnSno;
			console.log("========chkDupCnt2 < 1 compareDisuseReqSno=======>>>"+compareDisuseReqSno);
			tpArray2.push(compareDisuseReqSno);
		}
	}
	
	for(var i=0;i<tpArray2.length;i++){
		console.log("========tpArray2["+i+"]=======>>>"+tpArray2[i]);
	}
	
	
	
	fn_showModalPage(msg, function() {
		
		Objtype.type = reqType;
		var data = {rowDatas : tpArray2, type : Objtype};
	    var callUrl = "<c:url value='/evdc/trt/modifySelEvdcTrtsList.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifySelEvdcTrtsListCallBack'});
	    
// 		$("#rentAplnSno").val(rentAplnSno);
// 		var data = {rowDatas : tpArray, formDatas1 : formObj1, formDatas2 : formObj2, formDatas3 : formObj3};
// 	    var callUrl = "<c:url value='/eqp/lend/lendReqEqpLendLendReqRDtl.do'/>";
// 	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyEqpLendCallBack'});
	});
	
	
	
}



/**
* 반납처리 콜백
* @param
* @returns
*/
function fn_modifySelEvdcTrtsListCallBack(){
	fn_searchEvdcTrtMList(1);
}

/**
 * 화면 컨트롤
 * @param  
 * @returns
 */
 function fn_dispCont(){
 	
	 console.log("========사용자구분 session_usergb=====>>>"+session_usergb);
	 //console.log("========대여진행상태2 pgsStat2=====>>>"+pgsStat);
	
	$("#btn_frsRcv").hide(); //접수
	$("#btn_frsAprv").hide(); //승인
	$("#btn_excel").hide(); //엑셀다운
	
	if(session_usergb == "C01003"){ //포렌식수사관
		$("#btn_frsRcv").show(); //접수
		$("#btn_excel").show(); //엑셀다운
	}else if(session_usergb == "C01004"){ //수사과장
		$("#btn_frsAprv").show(); //승인
		$("#btn_excel").show(); //엑셀다운
	}
	
 	
 }

 /**
  * @!@ 디지털증거관리 리스트 엑셀 다운로드
  * @param {string} incdtSno
  * @returns 
  */
function fn_getExeclDownload(){
 	requestUtil.comExcelXlsx({
 		formNm :'searchForm',
 		filename : "디지털증거처리",
 		sheetNm : "디지털증거처리",
 		columnArr : "요청번호,등록일자,부서명,사건번호,접수번호,증거구분,모델명,시리얼번호,압수일시,요청구분,요청자,주임검사,검사승인일자,접수번호,접수일자,담당자,승인자,승인일자,진행상태,증거상태",
 		columnVarArr : "disuseReqSno,dispRegDt,dispDeptNm,incdtNo,rcvNoAcp,dispCfscGubun,mdlNm,serlNo,dispCfscDt,reqDivNm,reqUserNm,prosrNm,dispProsrAprvDt,dispRcvNo,dispRcvDt,rcvrNm,aprvrNm,dispAprvDt,pgsStatInfo,evdcPgsStatInfo",
 		sqlQueryId : "evdcAplnDAO.queryEvdcTrtMListExcel",
 		splitSearch : "schPgsStat"
 	});
 }


</script>
</head>
<body>

<div id="con_wrap">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div>
            <!-- 
                  <div><h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3></div>
             -->
                 <div class="loca">
                  <!--  <h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3>//-->
                    <div class="ttl">디지털 증거 처리 관리</div>
                    <div class="loca_list">Home > 디지털 증거 관리 > 증거 처리 관리</div>
                  </div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
<!--                     <form id="searchForm" name="searchForm" method="post"> -->
					 <form id="searchForm" name="searchForm" method="post" onsubmit="return false;">	
                    <div class="t_head">
				        <input type="hidden" id="boardKind" class="b_put"  name="boardKind"   value="C23001"/>
				        <input type="hidden" id="userGb"   name="userGb"   value="C00000"/>
				        <input type="hidden" id="flag" name="flag" value="" />
				        <!-- <input type="hidden" name="disuseReqSno" id="disuseReqSno" value="" /> -->
				        <input type="hidden" name="modFlag" id="modFlag" value="" />
				        <input type="hidden" class="" id="page" name="page" value="1"/>
				        <input type="hidden" class="" id="schReqUserNm" name="schReqUserNm" value=""/>
				        
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_searchEvdcTrtMList(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="11%">
                                   <col width="22%">
                                   <col width="11%">
                                   <col width="23%">
                                   <col width="11%">
                                   <col width="22%">
                                 </colgroup>
                           <thead>
                              <tr>
					            <th scope="col" class="hcolor">사건번호</th>
					            <td scope="col" >
					               <input class="inpw60" type="text" name="schIncdtNo" id="schIncdtNo"  maxlength="100"/>
					            </td>
					            <th scope="col" class="hcolor">사건명</th>
					            <td scope="col" >
					               <input class="inpw60" type="text" name="schIncdtNm" id="schIncdtNm"  maxlength="100"/>
					            </td>
					            <th scope="col" class="hcolor">진행상태</th>
					          	<td scope="col" >
					                <select class="selw10" id="schPgsStat" name="schPgsStat" onchange="fn_searchEvdcTrtMList(1)"  >
					                </select> 
					            </td>
					            
					            
					          </tr>
					          <tr>
					            <th scope="col" class="hcolor">요청일자</th>
					            <td scope="col" >
					               <input id="schStReqDt" name="schStReqDt"  title="요청일자(from)"  type="text" value="" class="inpw30" maxlength="10"/> 
					               <input id="schEdReqDt" name="schEdReqDt"  title="요청일자(to)"  type="text" value="" class="inpw30" maxlength="10"/>
					            </td>
					          
					            <th scope="col" class="hcolor">증거구분</th>
					            <td scope="col" >
					               <select class="selw10" id="schCfscGoodsDiv" name="schCfscGoodsDiv" onchange="fn_searchEvdcTrtMList(1)"  >
					               </select> 
					            </td>
					          	
					            <th scope="col" class="hcolor">증거상태</th>
					          	<td scope="col" >
					                <select id="schEvdcPgsStat" name="schEvdcPgsStat" onchange="fn_searchEvdcTrtMList(1)"  class="selw10" >
					                	<option value = "">전체</option>
					                	<option value = "C03006">완료</option>
					                	<option value = "C03005">미완료</option>
					                </select> 
					            </td>
					          </tr> 
					          <tr>
					            <th scope="col" class="hcolor">증거번호</th>
					            <td scope="col" >
					               <input class="inpw60" type="text" name="" id=""  maxlength="100"/>
					            </td>
					          
					            <th scope="col" class="hcolor">모델명</th>
					            <td scope="col">
					               <input class="inpw30" type="text" name="schMdlNm" id="schMdlNm"  maxlength="100"/>
					            </td>
   					            <th scope="col" class="hcolor">요청구분</th>
					          	<td scope="col" >
					                <select id="schEvdcReqDiv" name="schEvdcReqDiv" onchange="fn_searchEvdcTrtMList(1)"  class="selw10" >
					                	<option value = "">전체</option>
					                	<option value = "C07002">이송</option>
					                	<option value = "C07001">폐기</option>
					                </select> 
					            </td>
					          </tr>  
					          <tr>
								  <th scope="col" class="hcolor">요청번호</th>
								  <td scope="col" >
								  	<input class="inpw60" type="text" name="disuseReqSno" id="disuseReqSno"  maxlength="100"/>
								  </td>
								  <th scope="col" class="hcolor">접수번호</th>
								  <td scope="col" >
								  	<input class="inpw60" type="text" name="schRcvNoAcp" id="schRcvNoAcp"  maxlength="100"/>
								  </td>
					          </tr> 
                           </thead>
                        </table>
                      
                      </div>
                      <div  class="btn_c">
					       <ul>
                             
<%--                                <c:if test="${loginVO.userGb == 'C01999'}"> --%>
							 <li><a href="javascript:void(0);" class='myButton' onclick="fn_getExeclDownload(); return false;" id="btn_excel" name="btn_excel" style="display: none;">엑셀다운</a></li>	
							 <li><a href="javascript:void(0);" class='myButton' onclick="fn_aprvEvdcTrt('frsRcv'); return false;" id="btn_frsRcv" name="btn_frsRcv" style="display: none;">접수</a></li> 
							 <li><a href="javascript:void(0);" class='myButton' onclick="fn_aprvEvdcTrt('frsAprv'); return false;" id="btn_frsAprv" name="btn_frsAprv" style="display: none;">승인</a></li> 
<!-- 							 <li><a href="javascript:void(0);" class='RdButton' onclick="fn_insEvdcDisuseReq(); return false;" id="btn_evdcIns" name="btn_evdcIns" style="display: none;">등록</a></li> -->
                             <li><a href="javascript:void(0);" class='gyButton' onclick="fn_searchEvdcTrtMList(1); return false;">조회</a></li>
<!--                              <li><a href="javascript:void(0);" class='RdButton' onclick="fn_insAnlsReq(); return false;">등록</a></li> -->
<%--                                </c:if> --%>
                             </ul>   
					  </div>
                     </form>
                       
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
<!--                      <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div> -->
<!--                      <div class="bo_num"> -->
<!--                          <select id="perPageNum" name="perPageNum"> -->
<!-- 			               <option value="5">5개씩</option> -->
<!-- 			               <option value="10" selected="selected">10개씩</option>		                -->
<!-- 			             </select> -->
<!--                      </div> -->
					 <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00">0</strong>건</div>
                     
                     <!--------------목록---------------------->
                     <div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:410px;">  
                          <table id="listEvdcTrt" class="tbl_type" border="1" cellspacing="0" >
                                <caption>사업관리</caption>
                                  <colgroup>
                                  	  <col width="3%">	<%/* 체크박스 */%>
                                      <col width="6%">	<%/* 요청번호 */%> 
                                      <col width="5%">	<%/* 등록일자 */%>
                                      <col width="9%">	<%/* 부서명 */%>
                                      <col />			<%/* 사건번호 */%>
                                      <col width="5%">	<%/* 접수번호 */%>
                                      <col width="6%">	<%/* 증거구분 */%>
                                      <col width="5%">	<%/* 모델명 */%>
                                      <col width="5%">	<%/* 시리얼번호 */%>
                                      <col width="5%">	<%/* 압수일시 */%>
                                      <col width="5%">	<%/* 요청구분 */%>
                                      <col width="5%">	<%/* 요청자 */%>
                                      <col width="5%">	<%/* 주임검사(승인일자) */%>
                                      <col width="5%">	<%/* 접수번호 */%>
                                      <col width="5%">	<%/* 담당자 */%>
                                      <col width="6%">	<%/* 승인 */%>
                                      <col width="5%">	<%/* 진행상태 */%>
                                      <col width="5%">	<%/* 증거상태 */%>
                                      </colgroup>
                                    <thead>
                                      <tr>
                                      	 <th scope="col">선택</th>
<!--                                          <th scope="col">순번</th> -->
                                         <th scope="col">요청번호</th>
                                         <th scope="col">등록일자</th>
                                         <th scope="col">부서명</th>
                                         <th scope="col">사건번호</th>
                                         <th scope="col">접수번호</th>
                                         <th scope="col">증거구분</th>
                                         <th scope="col">모델명</th>
                                         <th scope="col">시리얼번호</th>
                                         <th scope="col">압수일시</th>
                                         <th scope="col">요청구분</th>
                                         <th scope="col">요청자</th>
                                         <th scope="col">주임검사<br>(승인일자)</th>
                                         <th scope="col">접수번호</th>
                                         <th scope="col">담당자</th>
                                         <th scope="col">승인</th>
                                         <th scope="col">진행상태</th>
                                         <th scope="col">증거상태</th>
                                      </tr>
                                    </thead>
                                    <tbody id="lendAprvList">
                                    </tbody>
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                    <div id="page_navi" class="page_wrap"></div>                   
                       <!-----------------------//페이징----------------------->
                 
                  </div>
                 
            </div>
    </div>
</div>

</body>
</html>