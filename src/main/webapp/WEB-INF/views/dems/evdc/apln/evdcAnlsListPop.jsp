<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : ilyong
 * 3. 화면명 : 증거목록 조회 팝업
 * 4. 설명 : 증거목록 조회 팝업
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	var codeInfo2 = [{cdId:'C05',selectId:'schCfscGoodsDiv',type:'1', callbackNm:'fn_ajaxSchCfscGoodsDivCallback', sqlQueryId:''}];
	/* fn_ajaxCodeList(codeInfo2);	 */
	gfn_init({startFnNm:'', param:codeInfo2, codeSet:'Y'});
	
	
	<%/* 전체 checkbox 클릭시 */%>
    $('#chkAll').change(function() {
        var chk = $(this).is(':checked');
        if(chk){
               $('input:checkbox[id=popChk]').each(function() {
                   $(this).prop("checked", true);         
               });
        }else{
               $('input:checkbox[id=popChk]').each(function() {
                   $(this).prop("checked", false);         
               });
        }
    });
});


function fn_ajaxSchCfscGoodsDivCallback(data){
 	//$('#schEqpTyp option:eq(0)').before("<option value='' selected>전체</option>");
	$('#schCfscGoodsDiv').prepend('<option value="" selected>전체</option>');
	
	fn_queryMList(1);
}

/**
 * @!@ 프로그램 관리 리스트 조회
 * @param {int} page
 * @returns 
 */
function fn_queryMList(page){
	var popGubun = "pop";
	var callUrl = "<c:url value='/evdc/apln/queryEvdcAnlsListPop.do'/>?popGubun="+popGubun;
	
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_queryMListCallback', page:page, perPageNum:10});
	
}

/**
 * @!@ 프로그램 관리 리스트 조회 콜백
 * @param {json} data
 * @returns 
 */
function fn_queryMListCallback(data){
	
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listEvdcAnlsFind > colgroup").find("col").length;
	
	$("#listEvdcAnlsFind > tbody").empty();
	$("#totalcnt").text(data.totalCount);
	
	if(listCnt == 0){
		var append = "";
		append += "<tr>";
		
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		
		append += "</tr>";
		$("#listEvdcAnlsFind > tbody").append(append);
	}else{
		$.each(list,function(idx,row){
			var append = "";
			var cfscNm = ""; //증거구분명
			cfscNm = gfn_nullRtnSpace(row.cfscGoodsDivNm)+"("+gfn_nullRtnSpace(row.cfscDivNm)+")";
			
			append += "<tr>";
			
// 			<th scope="col">순번</th>
//             <th scope="col">선택</th>
//             <th scope="col">장비명</th>
//             <th scope="col">장비유형</th>
//             <th scope="col">S/N</th>
//             <th scope="col">모델명</th>
//             <th scope="col">제조사</th>
//             <th scope="col">도입일자</th>
			

	 		

	 		append += '<td style=\"text-align:center\">';
 	 		append +=  "<input type=\"checkbox\" name=\"popChk\" id=\"popChk\" value=\"Y\" class=\"check_agree1\">";	<%/* 선택 */%>
 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  gfn_nullRtnSpace(row.dispDeptNm);	

 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  gfn_nullRtnSpace(row.incdtNo);
 	 		append +=  "<input type=\"hidden\" name=\"popIncdtNo\"  value=\""+gfn_nullRtnSpace(row.incdtNo)+"\" />";	<%/* 사건번호 */%>
 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  gfn_nullRtnSpace(row.incdtNm);

 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  gfn_nullRtnSpace(row.rcvNoAcp);
 	 		append +=  "<input type=\"hidden\" name=\"popRcvNoAcp\"  value=\""+gfn_nullRtnSpace(row.rcvNoAcp)+"\" />";	<%/* 분석요청일련번호 */%>
 	 		append +=  "<input type=\"hidden\" name=\"popAnalReqSno\"  value=\""+gfn_nullRtnSpace(row.analReqSno)+"\" />";	<%/* 분석요청일련번호 */%>
 	 		append +=  "<input type=\"hidden\" name=\"popEvdcSno\"  value=\""+gfn_nullRtnSpace(row.evdcSno)+"\" />";	<%/* 증거일련번호 */%>
 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  cfscNm;
 	 		append +=  "<input type=\"hidden\" name=\"popCfscNm\"  value=\""+cfscNm+"\" />";	<%/* 증거구분 */%>
 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  gfn_nullRtnSpace(row.mdlNm)
 	 		append +=  "<input type=\"hidden\" name=\"popMdlNm\"  value=\""+gfn_nullRtnSpace(row.mdlNm)+"\" />";	<%/* 모델명 */%>
 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  gfn_nullRtnSpace(row.serlNo)
 	 		append +=  "<input type=\"hidden\" name=\"popSerlNo\"  value=\""+gfn_nullRtnSpace(row.serlNo)+"\" />";	<%/* 시리얼번호 */%>
 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  gfn_nullRtnSpace(row.dispCfscDt)
 	 		append +=  "<input type=\"hidden\" name=\"popCfscDt\"  value=\""+gfn_nullRtnSpace(row.dispCfscDt)+"\" />";	<%/* 압수일시 */%>
 	 		append += '</td>';
 	 		
	
			append += "</tr>";
	        $("#listEvdcAnlsFind > tbody").append(append);
	 	});
	}
	
	
	data.__callFuncName__ ="fn_queryMList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
	
	
}

 /**
  * 장비 선택
  * @param
  * @returns 
  */
 function fn_setEqpSno(eqpSno, eqpNm){

	 $("#eqpSno").val(eqpSno);
	 $("#eqpNm").val(eqpNm);
	 fn_dialogClose('eqpMgmtFIndEqupQListPop');
 	
 }

function fn_selEvdcAnls(){ 

	var arr = new Array();
	var chkCnt = 0;
    var i =0;
    var checkNo="";
    var checkNO2="";
    
    $('#listEvdcAnlsFind :input[name=popChk]').each(function(index) {
		//console.log("====fn_selMgmt	index========>>>"+index);
		 if($("input:checkbox[name=popChk]").eq(index).is(":checked")==true){
			 var retVal  = new Object();
			 retVal.popIncdtNo       = $("input[name^=popIncdtNo]").eq(index).val();	<%/* 사건번호 */%>
             retVal.popRcvNoAcp        = $("input[name^=popRcvNoAcp]").eq(index).val();	<%/* 접수번호 */%>
             retVal.popAnalReqSno        = $("input[name^=popAnalReqSno]").eq(index).val();	<%/* 분석요청일련번호 */%>
             retVal.popEvdcSno        = $("input[name^=popEvdcSno]").eq(index).val();	<%/* 증거일련번호 */%>
             retVal.popCfscNm     = $("input[name^=popCfscNm]").eq(index).val();	<%/* 구분 */%>	 
             retVal.popMdlNm         = $("input[name^=popMdlNm]").eq(index).val();	<%/* 모델명 */%> 
             retVal.popSerlNo        = $("input[name^=popSerlNo]").eq(index).val(); 	<%/* 시리얼번호 */%>
             retVal.popCfscDt       = $("input[name^=popCfscDt]").eq(index).val(); 	<%/* 압수일시 */%>
             
             arr[i] = retVal;
             ++i;
             
             chkCnt++;
		 }
    });	
    
    if(chkCnt<1){
    	fn_showUserPage("증거를 선택하세요.", function() {
			
		});
		return;
    }
    
    fn_setItem(arr);
}


function fn_setItem(arrObj){
	var size = arrObj.length;
	var paramValue1="";
	var paramValue2="";
	var compAnalReqSno = "";
	var compEvdcSno = "";
	var	dupRcvNoAcp = "";
	
	for(var i=0; i<size; i++){
        var retVal = arrObj[i];
        var rowIndex = retVal.rowIndex;
        if(i > 0){
            rowIndex = 0;
        }

        compAnalReqSno = retVal.popAnalReqSno; //분석요청일련번호
        compEvdcSno = retVal.popEvdcSno; //증거일련번호
        console.log("======compAnalReqSno======>>>"+compAnalReqSno+"======compEvdcSno======>>>"+compEvdcSno);
        
        var dupCnt=0;
        
        $('#tbEvdcAplnDtl3 :input[name=chk]').each(function(index) {
        	console.log("======tbEvdcAplnDtl3 analReqSno======>>>"+$("input[name^=analReqSno]").eq(index).val()+"======tbEvdcAplnDtl3 evdcSno======>>>"+$("input[name^=evdcSno]").eq(index).val());
    		if($("input[name^=analReqSno]").eq(index).val() == compAnalReqSno && $("input[name^=evdcSno]").eq(index).val() == compEvdcSno){
     			dupRcvNoAcp += "(분석요청일련번호: "+retVal.popAnalReqSno+", 증거일련번호: "+retVal.popEvdcSno+")\n";
     			dupCnt++;	
    	 	}	
    	});
        
        if(dupCnt < 1){
        	var append = "";
			append += "<tr>";
				
 			append += '<td style=\"text-align:center\">';
	 		append +=  "<input type=\"checkbox\" name=\"chk\" id=\"chk\" value=\"Y\" class=\"check_agree1\" checked>";	<%/* 선택 */%>
	 		append += '</td>';
	 		
	 		append += '<td style=\"text-align:center\">';
 	 		append +=  retVal.popIncdtNo;	<%/* 사건번호 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  retVal.popRcvNoAcp;	<%/* 접수번호 */%>
 	 		append +=  "<input type=\"hidden\" name=\"analReqSno\"  value=\""+ retVal.popAnalReqSno+"\" maxlength=\"50\" class=\"inpw70\"/>";	<%/* 분석요청일련번호 */%>
 	 		append +=  "<input type=\"hidden\" name=\"evdcSno\"  value=\""+ retVal.popEvdcSno+"\" maxlength=\"50\" class=\"inpw70\"/>";	<%/* 증거일련번호 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  retVal.popCfscNm;	<%/* 구분 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  retVal.popMdlNm;	<%/* 모델명 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  retVal.popSerlNo;	<%/* 시리얼번호 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  retVal.popCfscDt;	<%/* 압수일시 */%>
 	 		append += '</td>';
 	 		
	
			append += "</tr>";
	        $("#tbEvdcAplnDtl3 > tbody").append(append);	
        }
	} 
	
	if(!gfn_isNull(dupRcvNoAcp)){
   	 	fn_showUserPage("이미 추가된 증거목록은 선택할 수 없습니다.\n "+dupRcvNoAcp);
    }
	
	fn_dialogClose("evdcAnlsListPop");

}

function fn_dupMgmtChk(eqpSno, eqpNm){
	//debugger;
	var dupCnt = 0;
	var message = "";
	var pEqpSno = eqpSno;
	
	$('#tbLendDtl :input[name=chk]').each(function(index) {
		if($("input[name^=eqpSno]").eq(index).val() == pEqpSno){
 			dupCnt++;	
 			message += "(장비명: "+eqpNm+")\n";
	 	}	
	});
	
// 	if(){
// 		fn_showUserPage("이미 추가된 장비는 선택할 수 없습니다.\n "+message+")");
// 	}
	
	
// 	if(dupCnt == 0){
// 		return true;
// 	}else{
// 		fn_showUserPage("이미 추가된 장비는 선택할 수 없습니다.\n "+message+")");
// 		return false;
// 	}
	
	
// 	if(dupCnt>0){
// 		fn_showUserPage( "이미 추가된 장비는 선택할 수 없습니다.(장비명: "+message+")", function() {
// 			return false;	
//         });
// 	}else{
// 		return true;
// 	}
}
 
function fn_dialogClose(divId){
	 $("#"+divId).dialog( "close" );
     $("#"+divId).empty();
}
</script>

</head>
<body>
<div id="con_wrap_pop">
	<div class="contents">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                      <div class="window_popup">
                          <div class="sub_ttl">증거목록 찾기</div>
                         
                          <div class="sub">
                             <!--------------검색------------------>
                             <form name="searchForm" id="searchForm" method="post">
                             <input type="hidden" id="srcUseYn" name="srcUseYn" value="Y"/>
                            <div class="t_head">
                                  <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_queryMList(1);">
                                        <caption>검색</caption>
                                         <colgroup>
                                            <col width="20%">
                                            <col width="30%">
                                            <col width="20%">
                                            <col width="30%">
                                         </colgroup>
                                   <thead>
                                      <tr>
                                           <th scope="row" class="hcolor">사건번호</th>
								           <td>
								               <input type="text" id="schIncdtNo" name="schIncdtNo" title="사건번호" class="inpw80" maxlength="100"/>
								           </td>
								           <th scope="row" class="hcolor">사건명</th>
								           <td >
								           		<input type="text" id="schIncdtNm" name="schIncdtNm" title="사건명" class="inpw80" maxlength="100"/>
								           </td>
                                       </tr>
                                       <tr>
                                           <th scope="row" class="hcolor">증거구분</th>
								           <td>
								               <select class="selw10" id="schCfscGoodsDiv" name="schCfscGoodsDiv" onchange=""  >
					                			</select> 
								           </td>
								           <th scope="row" class="hcolor">증거번호</th>
								           <td >
								           		<input type="text" id="schRcvNoAcp" name="schRcvNoAcp" title="사건명" class="inpw80" maxlength="20"/>
								           </td>
                                       </tr>
                                       <tr>
                                           <th scope="row" class="hcolor">모델명</th>
								           <td colspan="3">
								           		<input type="text" id="schMdlNm" name="schMdlNm" title="모델명" class="inpw40" maxlength="100"/>
								           </td>
                                       </tr>
                                   </thead>
                                </table>
                              </div>
                            <div class="btn_c">
                                  <ul>
<%--                                   	 <p><center><button class="button80" onclick="javascirpt:fn_selMgmt2();return false;">선택적용</button></center></p> --%>
                                  	 <li><a href="javascript:fn_selEvdcAnls();" class="myButton">선택확인</a></li>
                                     <li><a href="javascript:fn_queryMList(1);" class="myButton">조회</a></li>
                                  </ul>
                               </div>
                               </form>
                            <!--------------//검색------------------>
                            
                            <!--------------결과------------------>
                             <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>
                             
                             <!--------------목록---------------------->
                             <div class="t_list">  
                                  <table id="listEvdcAnlsFind" class="tbl_type" border="1" cellspacing="0" >
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="4%">  <!-- 선택 -->
                                              <col />			<!-- 부서명 -->
                                              <col width="11%">	<!-- 사건번호 -->
                                              <col width="14%">	<!-- 사건명 -->
                                              <col width="8%">	<!-- 증거번호(접수번호) -->
                                              <col width="12%">	<!-- 증거구분 -->
                                              <col width="12%">	<!-- 모델명 -->
											  <col width="11%">	<!-- 시리얼번호 -->	
											  <col width="12%">	<!-- 압수일시 -->
                                           </colgroup>
                                            <thead>
                                              <tr>
<!--                                                  <th scope="col">순번</th> -->
                                                 <th scope="col"><input type="checkbox" name="chkAll" id="chkAll" ></th>
                                                 <th scope="col">부서명</th>
                                                 <th scope="col">사건번호</th>
                                                 <th scope="col">사건명</th>
                                                 <th scope="col">증거번호<br>(접수번호)</th>
                                                 <th scope="col">증거구분</th>
                                                 <th scope="col">모델명</th>
                                                 <th scope="col">시리얼번호</th>
                                                 <th scope="col">압수일시</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->
                             
                             <!-----------------------페이징----------------------->
                             <div id="page_navi" class="page_wrap"></div>
                               <!-----------------------//페이징----------------------->
                          		
                          		<p></p>
<%--                           		<p><center><button class="button80" onclick="javascirpt:fn_selMgmt2();return false;">선택적용</button></center></p> --%>
                          		<div class="btn_c">
			                      <ul>
<!-- 			                        <li><button class="button60" onclick="javascirpt:fn_selMgmt();return false;">선택적용</button></li> -->
			                        <li><a href="#" class="gyButton" onclick="fn_dialogClose('evdcAnlsListPop');return false;">닫기</a></li>
			                        <!-- <li><a href="#" class="myButton" onclick="fn_indexFsysProgramMList();return false;">목록</a></li> button60-->
			                      </ul>
			                    </div>
                          </div>
                         
                    </div>
               </div>
                 <!---  //contnets  적용 ------>
       </div>
  </div>
</body>
</html>