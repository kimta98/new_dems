<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비관리 > 장비대여 등록
 * 4. 설명 : 장비관리 > 장비대여 등록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var PW_Error = 0;
var ID_Duple = 1;

var modFlag = '${param.modFlag}'; //매입처명
var disuseReqSno = '${param.disuseReqSno}';
var dispconPgsStat = "";

var reqInsttNm = '${reqInsttNm}';
var reqDepNm = '${reqDepNm}';
var reqInsttCd = '${reqInsttCd}';


$(document).ready(function() {
	fn_init();
});

function fn_init(){
tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');	
	
	var codeInfo2 = [{cdId:'C07',selectId:'reqDiv',type:'1', callbackNm:'fn_ajaxReqDivCallback', sqlQueryId:''}];
	fn_ajaxCodeList(codeInfo2);
	
	//요청기관,요청부서,요청기관코드 세팅
	if(reqInsttNm != ''){
		$('#reqInsttNm').text(reqInsttNm);	
	}
	if(reqInsttCd != ''){
		$('#reqInsttCd').val(reqInsttCd);	
	}
	if(reqDepNm != ''){
		$('#reqDepNm').text(reqDepNm);	
	}
	
	
	<%/* 달력 세팅 */%> 
 	gfn_calendarConfig("reqDt", "", "", "");    <%/* 요청일자 */%> 
 	gfn_calendarConfig("rentStartDt", "rentEndDt", "minDate", "");    <%/* 대여시작일자 from */%>
	gfn_calendarConfig("rentEndDt", "rentStartDt", "maxDate", "");   <%/* 대여종료일자 to */%>
	
	
	<%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
//     gfn_overMaxLength("rentReqInfo",250);
<%-- 	gfn_toNumber("cntrAmt"); <%/* 계약금액 */%> --%>
}

function fn_ajaxReqDivCallback(data){
	$('#reqDiv option:eq(0)').before("<option value='' selected>선택</option");
	//증거폐기요청 정보 조회
 	fn_evdcDisuseReqInfoSearch();
}

/**
* //장비대여신청 정보 조회
* @param {string} page 항목에 대한 고유 식별자 
* @returns fn_callBack
*/
function fn_evdcDisuseReqInfoSearch(){

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");
	
	var input = document.createElement('input'); 
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "disuseReqSno");
	input.setAttribute("value", disuseReqSno);
	searchForm.appendChild(input);	
	
	document.body.appendChild(searchForm);
	
	var callUrl = "<c:url value = "/evdc/apln/queryEvdcDisuseReqInfo.do"/>";	
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'insForm',callbackNm:'fn_evdcDisuseReqInfoSearchCallBack'});
}

function fn_evdcDisuseReqInfoSearchCallBack(data){
	//alert("조회성공");
	//var prosrNm = gfn_nullRtnSpace(data.resultMap.aprvrNm);
	var reqDt = gfn_dashDate2(gfn_nullRtnSpace(data.resultMap.reqDt),'-');
	var regUserNm = gfn_nullRtnSpace(data.resultMap.dispRegUserNm);
	var dispDeptNm = gfn_nullRtnSpace(data.resultMap.dispDeptNm);
	var teamNm = gfn_nullRtnSpace(data.resultMap.teamNm);
	var reqDiv = gfn_nullRtnSpace(data.resultMap.reqDiv);
	var reqTransInstt = gfn_nullRtnSpace(data.resultMap.reqTransInstt);
	var spanDisuseReqSno = gfn_nullRtnSpace(data.resultMap.disuseReqSno);
	dispconPgsStat = gfn_nullRtnSpace(data.resultMap.pgsStat);
	
	//alert("========dispconPgsStat======>>>"+dispconPgsStat);
	
	$("#spanDisuseReqSno").text(spanDisuseReqSno);
	$("#reqInsttNm").text(session_insttnm);
	$("#reqDepNm").text(dispDeptNm);
	//$("#prosrNm").val(prosrNm); reqDiv
	$("#reqDt").val(reqDt);
	$("#regUserNm").val(regUserNm);
	$("#reqDiv").val(reqDiv);
	
	
	if(reqDiv == "C07002"){ //  폐기요청구분: C07001	폐기, C07002 이송
		$("#reqTransInstt").show();
		$("#reqTransInstt").val(reqTransInstt);
	}else{
		$("#reqTransInstt").hide();
		$("#reqTransInstt").val("");
	}
	
	
	$('form[name=searchForm]').remove();
	
	//화면 컨트롤
	fn_dispCont();
	
	//디지털 증거 처리 신청 폐기대상 리스트 조회
	fn_selEvdcAplnDtlList();

}


/**
* 장비 상세 정보 조회 콜백
* @param  
* @returns
*/
function fn_queryEqpLendReqInfoCallBack(data){
	
	//console.log("상세정보 조회 성공");

	var dispRentDeptNm = gfn_nullRtnSpace(data.resultMap.deptNm);
	console.log("=======dispRentDeptNm=======>>>"+dispRentDeptNm);
	
// 	$("#eqpSno").text(eqpSno); 		
// 	$("#eqpNm").text(eqpNm); 		
// 	$("#eqpTypNm").text(eqpTypNm); 	
// 	$("#srNo").text(srNo); 			
	

	
}

/**
 * @!@ 장비 지원 관리 > 대여장비목록 조회
 * @param {int} page
 * @returns 
 */
function fn_searchLendMgmtList(page){
// 	debugger;
	var callUrl = "<c:url value='/eqp/lend/queryEqpLendDtlList.do'/>";
	$("#rentAplnSno").val(rentAplnSno);
	
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'insForm', callbackNm:'fn_searchLendMgmtListCallBack', page:page, perPageNum:1000});
}


/**
 * 대여장비목록 조회 콜백
 * @param  
 * @returns
 */
 function fn_searchLendMgmtListCallBack(data){ 
	
	//debugger;
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#tbLendDtl > colgroup").find("col").length;
	
	$("#tbLendDtl > tbody").empty();
	
//  	$("#totalcnt").text(data.totalCount);
	
  	if(listCnt < 1){
  		var append = "";
		append += "<tr>";
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		append += "</tr>";
		$("#tbLendDtl > tbody").append(append);
	}else{
	 	
		$.each(list,function(idx,row){
	 		
			//console.log("========idx zzzzzzzzzzzzzzzzzzz=====>>>"+idx);
			var append = "";
	 		append += "<tr>";
			
	 		append += '<td style=\"text-align:center\">';
	 		append +=  "<input type=\"checkbox\" name=\"chk\" id=\"chk\" value=\"Y\" class=\"check_agree1\" >";	<%/* 선택 */%>
	 		append += '</td>';
	 		
	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.eqpNm);	<%/* 장비명 */%>
 	 		append +=  "<input type=\"hidden\" name=\"eqpSno\"  value=\""+ row.eqpSno+"\" maxlength=\"50\" class=\"inpw70\"/>";	<%/* 장비일련번호 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.eqpTypNm);	<%/* 장비유형 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.srNo);	<%/* S/N */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.mdlNm);	<%/* 모델명 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.mnftCo);	<%/* 제조사 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_dashDate2(gfn_nullRtnSpace(row.purcDt),'-');	<%/* 도입일자 */%>
 	 		append += '</td>';
 	 		

	 		
	 		append += "</tr>";
	 		console.log("========idx append=====>>>"+append);
			//var append = "";
	        $("#tbLendDtl > tbody").append(append);
	  		
	  	});
	}
  	
//    	data.__callFuncName__ ="fn_searchList";
//  	data.__naviID__ ="page_navi";
//  	pageUtil.setPageNavi(data);
 }	 
 
function gfn_getTelNo(that){
	
	//alert("=========that=====>>>>"+that)
	var returnValue = that.replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-"); 
	
	//alert("=========returnValue=====>>>>"+returnValue)
	//$(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
	return returnValue;
 }



//장비대여신청 임시저장
function fn_modifyEvdcAplnInfo(saveGb){
	
	var conMsg = "";
	if(saveGb=="req"){
		conMsg = "승인요청 하시겠습니까?";
		$("#pgsStat").val("C03002");
	}else if(saveGb=="imsi"){
		conMsg = "임시저장 하시겠습니까?";
		$("#pgsStat").val("");
	}else if(saveGb=="reqCnc"){
		conMsg = "승인요청 취소하시겠습니까?";
		$("#pgsStat").val("");
	}else if(saveGb=="mod"){
		conMsg = "수정 하시겠습니까?";
	}else if(saveGb=="del"){
		conMsg = "삭제 하시겠습니까?";
	}
	
	$("#saveGb").val(saveGb);
	
// 	if(!fn_saveCntChk()) return;
 	if(!fn_valChk()) return; 
	console.log("==========11111111111======>>>");
	//장비상세정보
	var tpFormArry = [];
	var $formDatas1 = $('#insForm').find('#tbEvdcAplnDtl');
	var $formDatas2 = $('#insForm').find('#tbEvdcAplnDtl2');
	console.log("==========222222222222======>>>");
	var formObj1 = new Object();
	var formObj2 = new Object();
	var cnt = 0;
	var cnt2 = 0;
	console.log("==========333333333333======>>>");
	$formDatas1.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			formObj1[item.name] = item.value;
			cnt++;
			console.log("========formDatas1["+index+"]========>>>>"+item.value);
		});
		
// 		dataObj = {};

	});
	
	$formDatas2.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			formObj2[item.name] = item.value;
			cnt2++;
			console.log("========formDatas2["+index+"]========>>>>"+item.value);
		});
		
// 		dataObj = {};

	});
	
	//alert("======cnt======>>>"+cnt);
	//tbMgmtDtl
	//tbMntDtl
	
	//유지보수 상세정보
 	var tpArray = [];
	var dataObj = new Object();
	var cnt3 = 0;

	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			dataObj[item.name] = item.value;
			
		});


			tpArray.push(dataObj);
			dataObj = {};


	});
	

	fn_showModalPage(conMsg, function() {
		
		//$("#rentAplnSno").val(rentAplnSno);
		//alert("=======저장 pgsStat========>>>"+$("#pgsStat").val());
		var data = {rowDatas : tpArray, formDatas1 : formObj1, formDatas2 : formObj2};
	    var callUrl = "<c:url value='/evdc/apln/modifyEvdcAplnRDtl.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyEvdcAplnInfoCallBack'});
	});
	
	
}

function fn_modifyEvdcAplnInfoCallBack(data){
	fn_searchList();
	//alert("임시저장 성공");
// 	console.log("===============fn_modifyEvdcAplnInfoCallBack fn_init() 호출=========================>>>>>>>>>");
// 	fn_init();
}

function fn_searchList(){
	//debugger;
	//alert("====insForm serialize=========>>>>"+$("#insForm").serialize());
	//parent.addNaviTab('관리자 > 사용자관리 > 급식기관관리상세',"<c:url value="/eqp/mnt/indexEqpMntMList.do"/>",'급식기관관리상세','M000000703');
	//var flag = $("#flag").val();
	//alert("====flag=======>>>"+flag);
// 	if(flag == "MgmtList"){
// 		parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mgmt/indexEqpMgmtMList.do"/>');
// 	}else{
// 		parent.$('#tabs-M000000703').find("iframe").attr("src", '<c:url value="/eqp/mnt/indexEqpMntMList.do"/>');
// 	}
	
	//var param = "?"+$("#searchForm").serialize();
	var param = "";
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/evdc/apln/indexEvdcAplnMList.do"/>'+param);	
}


<%/*아이디 중복 체크*/%>
function fn_IDcheck(){
	
	var checkParamId = $("#userId").val(); 
// 	alert(checkParamId.search(/\s/));
	
	if( checkParamId.search(/\s/) > 0 ){
		alert('ID에 공백이 들어갈 수 없습니다.');
		$("#userId").focus();
		return;
	} 
	
	if(checkParamId == ""){
		alert("아이디를 입력해 주세요.");
		$("#userId").focus();
		return;
	}else{
		var callUrl = "<c:url value='/fsys/user/sysUserCheckId.do'/>";
		requestUtil.search({callUrl:callUrl,srhFormNm:'insForm',setFormNm:'insForm',callbackNm:'fn_callbackpop'});
	}
	
}

function fn_callbackpop(data) {
	
	var resultCnt = data.egovMap.resultCnt;
// 	alert("fn_callbackpop 진입 resultCnt===>>> "+resultCnt);
// 	if(resultCnt > 0){
// 		alert("111111");
// 	}else{
// 		alert("2222222");
// 	}
// 	var existsCnt = resultCnt.substring(0,resultCnt.indexOf('-'));
// 	alert("fn_callbackpop 진입 1-1");
	if( resultCnt > 0 ) {
		alert('이미 존재하는 ID입니다.');
		$('#userId').val('');
		ID_Duple=1;
		return;
	} else {
		alert('사용가능합니다.');
		ID_Duple =0;
		return;
	}
	
}

/**
 * @ 장비 관리 삭제
 * @param
 * @returns 
 */
function fn_deleteEqpMgmt() {
	
	if(confirm("삭제 하시겠습니까?")){
		
		//사용자 정보 수정
        var callUrl = "<c:url value='/eqp/biz/delEqpBizUDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_deleteEqpBizCallback'});
	
	}
	
}

/**
 * @!@ 메뉴 관리 삭제 콜백
 * @param
 * @returns 
 */
function fn_deleteEqpBizCallback(data){
	fn_searchList();
}

/**
 * @!@ 사업관리조회 팝업
 * @param cd
 * @returns 
 */
function fn_searchEqpBizMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/mgmt/eqpMgmtFIndEqupQListPop&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 650,
        width: 1000,
        title: '장비조회 팝업',
        divId : 'eqpMgmtFIndEqupQListPop'
        //divId : 'eqpMgmtMListPop'
	});

}

<%/* 시작 처리 function */%>
function fn_addRow() {
// 	$("#addRow").click(function(){
		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length+1;

		var tpTag =     '<tr>'+
			'<td style="text-align:center"><input type="checkbox" name="chk" id="chk" value="Y" class="check_agree1"></td>'+
			'<td><textarea id="box" name="dfecCnts" rows=2 cols=30 maxlength="2000"></textarea></td>'+
			'<td><textarea id="box" name="rprCnts" rows=2 cols=30 maxlength="200"></textarea></td>'+
			'<td "text-align:center"><input type="text" name="rprStartDt" id="rprStartDt_'+idx+'" class="inpw50"></td>'+
			'<td "text-align:center"><input type="text" name="rprEndDt" id="rprEndDt_'+idx+'" class="inpw50"></td>'+
			'<td "text-align:center"><input type="text" name="rprCoNm" class="inpw70" maxlength="50"></td>'+
			'<td "text-align:center"><input type="text" name="coCrgr" class="inpw70" maxlength="50"></td>'+
		'</tr>';
		$("#detailTbody").append(tpTag);

		
		gfn_calendarConfig("rprStartDt_"+idx, "", "", "");
		gfn_calendarConfig("rprEndDt_"+idx, "", "", "");
// 	});
		
}


<%/* 삭제버튼 클릭 function */%>
function fn_delRow() {
	var cnt = 0;
	$('#detailTbody > tr').each(function(index,item){
		var $chkbox = $(item).find('input[type=checkbox]');
		$chkbox.each(function(index,item){

			if($(this).is(':checked') == true){
				cnt++;
				//$(this).parent().parent('tr').next().remove();
				$(this).parent().parent('tr').remove();
			}
		})

	});

	if(cnt < 1){
		//alert('선택해주세요');
		fn_showUserPage( "삭제할 내역을 선택하십시요.", function() {
			return;		
        });
	}


//		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length/2;
	

//		for(var i = 1; i <= idx; i++){
//			if(i == 1){
//				$('#detailTbody > tr').eq(0).find('td:eq(1)').text(i);
//				$('#detailTbody > tr').eq(0).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
//				$('#detailTbody > tr').eq(0).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
//				$('#detailTbody > tr').eq(1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
//			}else{
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(1)').text(i);
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
//				$('#detailTbody > tr').eq(i*2-1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
//			}

//		}
//		fn_makeSbx(idx);
//		gfn_calendarConfig("cfscDate_"+idx, "", "", "");
		
}

<%/* 필수입력체크 */%>
function fn_valChk(){
	
	var reqDt = $("#reqDt").val();
	
	var prosrId = $("#prosrId").val();
	var reqUserId = $("#reqUserId").val();
	var reqDiv = $("#reqDiv").val();
	var reqTransInstt = $("#reqTransInstt").val();
	
	if(reqDt.length < 1){	
		fn_showUserPage("요청일자를 선택하세요.", function() {
			$("#reqDt").focus();
		});
		return false;  
	}else if(prosrId.length < 1){
		fn_showUserPage("주임군검사를 선택하세요.", function() {
			$("#prosrNm").focus();
		});
		return false;
	}else if(reqUserId.length < 1){
		fn_showUserPage("담당자를 선택하세요.", function() {
			$("#regUserNm").focus();
		});
		return false;
	}else if(reqDiv.length < 1){
		fn_showUserPage("요청구분을 선택하세요.", function() {
			$("#reqDiv").focus();
		});
		return false;
	}
	
	if(reqDiv=="C07002"){ //폐기요청구분: C07001 폐기, C07002 이송
		if(reqTransInstt.length < 1){
			fn_showUserPage("이송기관을 입력하세요.", function() {
				$("#reqTransInstt").focus();
			});
			return false;
		}
	}
	
	return true; 
	//console.log("=========chkCnt====>>>"+chkCnt);

}
	
function fn_saveCntChk(){
	var chkCnt = 0;
	$('#tbEvdcAplnDtl3 :input[name=chk]').each(function(index) {
		// 체크여부 확인
		//if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
			chkCnt++;
		//}
	});
	
	if(chkCnt<=0){
		fn_showUserPage( "증거목록을 추가하세요.", function() {
			return false;		
        });
	}else{
		//alert("======chkCnt====>>>"+chkCnt);
		return true;
	}
       
}

/**
 * @!@ 사건관리 조회 팝업
 * @param cdId
 * @returns 
 */
function fn_incdtIncdtMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtMListPop",
		height: 700,
        width: 1000,
        title: '사건관리 조회',
        divId : 'incdtIncdtMListPop'
	});

}

/**
 * @!@ 장비조회 팝업
 * @param cd
 * @returns 
 */
function fn_searchEqpBizMListPop2() {
   
		

//  	fn_showUserPage("이미 추가된 장비는 선택할 수 없습니다. \n ggg \n ggg \n ggg \n ggg");
// 	return;
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/mgmt/eqpMgmtFIndEqupQListPop2&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 650,
        width: 1000,
        title: '장비조회 팝업',
        divId : 'eqpMgmtFIndEqupQListPop2'
        //divId : 'eqpMgmtMListPop'
	});

}

/**
* 분석지원요청 요청관리 팝업호출
* @param {string} type 팝업종류
* @returns
*/
function fn_openPop(type){ 
	var callUrl = "<c:url value = "/com/PageLink.do"/>?type="+type
    var jsp = (type == 'incdt' ? "dems/evdc/apln/evdcIncdtFindPop" : "dems/anls/req/crgrFindPop"); 
    var divId = (type == 'incdt' ? "divIncdtFindPop" : "divCrgrFindPop"); 
			
	requestUtil.mdPop({
		popUrl : callUrl+"&link="+jsp,
		height: 600,
        width: 1000,
        title: (type == 'incdt' ? '사건조회' : '담당자조회'),
        divId : divId
	});
}

/**
* 분석지원요청 요청관리 팝업콜백
* @param {string} divId 팝업id
* @param {object} data 결과데이터
* @returns
*/
function fn_popCallBack(data, divId){
	
	$("#"+divId).dialog( "close" );
	$("#"+divId).empty();
	
	$.each(data, function(index, value){
		//alert("======index======>>>"+index+"\n======value=====>>>"+value);
		if($('#'+index).is('span')){
			$('#'+index).text(value);
		}else{
			$('#'+index).val(value);
		}
	});
	
	fn_selEvdcAplnDtlList();
}

/**
* 디지털 증거 처리 신청 폐기대상 리스트 조회
* @param
* @returns anlsReqInfoCallBack
*/
function fn_selEvdcAplnDtlList(){
	var disuseReqSno = $("#disuseReqSno").val();
	//alert("=====fn_selEvdcAplnDtlList  analReqSno=======>>>"+analReqSno);
	
	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");
	
	var input = document.createElement('input'); 
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "disuseReqSno");
	input.setAttribute("value", disuseReqSno);
	searchForm.appendChild(input);	
	
	document.body.appendChild(searchForm);
	
	var callUrl = "<c:url value = "/evdc/apln/queryEvdcAplnDtlList.do"/>";	
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'infoForm',callbackNm:'fn_selEvdcAplnDtlListCallBack'});
}

/**
* 디지털 증거 처리 신청 폐기대상 리스트 조회 콜백
* @param {object} data 조회한 결과데이터
* @returns
*/
function fn_selEvdcAplnDtlListCallBack2(data){
	//alert("========fn_selEvdcAplnDtlListCallBack data.evdcAplnDtlList.length=====>>>"+data.evdcAplnDtlList.length);
	$('form[name=searchForm]').remove();
	$("#tbEvdcAplnDtl3 > tbody").empty();
	for(var i = 0; i < data.evdcAplnDtlList.length; i++){
		
		var tpTag =     '<tr>'+
							'<td><span id="cfscDiv_'+i+'">'+data.evdcAplnDtlList[i].cfscDivNm+'</span></td>'+
							'<td><span id="serlNo">'+data.evdcAplnDtlList[i].serlNo+'</span></td>'+
							'<td><span id="cfscPlace">'+data.evdcAplnDtlList[i].cfscPlace+'</span></td>'+
							'<td><span id="realUser">'+data.evdcAplnDtlList[i].realUser+'</span></td>'+
							'<td><span id="sealYn">'+(data.evdcAplnDtlList[i].sealYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td><span id="dmgdDtl">'+data.evdcAplnDtlList[i].dmgdDtl+'</span></td>'+
							'<td><span id="drvDtl">'+data.evdcAplnDtlList[i].drvDtl+'</span></td>'+
						'</tr>';
		$("#detailTbody").append(tpTag);	
	}
}	


/**
 * @!@ 프로그램 관리 리스트 조회 콜백
 * @param {json} data
 * @returns 
 */
function fn_selEvdcAplnDtlListCallBack(data){
	
	var list = data.evdcAplnDtlList;
	var listCnt = list.length;
	var tabTdCnt = $("#tbEvdcAplnDtl3 > colgroup").find("col").length;
	
	$("#tbEvdcAplnDtl3 > tbody").empty();
	$("#totalcnt").text(data.totalCount);
	
	if(listCnt == 0){
		var append = "";
		append += "<tr>";
		
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		
		append += "</tr>";
		$("#tbEvdcAplnDtl3 > tbody").append(append);
	}else{
		$.each(list,function(idx,row){
			var append = "";
			var cfscNm = ""; //증거구분명
			cfscNm = gfn_nullRtnSpace(row.cfscGoodsDivNm)+"("+gfn_nullRtnSpace(row.cfscDivNm)+")";
			
			append += "<tr>";
			
// 			<th scope="col">순번</th>
//             <th scope="col">선택</th>
//             <th scope="col">장비명</th>
//             <th scope="col">장비유형</th>
//             <th scope="col">S/N</th>
//             <th scope="col">모델명</th>
//             <th scope="col">제조사</th>
//             <th scope="col">도입일자</th>
			

	 		

	 		append += '<td style=\"text-align:center\">';
 	 		append +=  "<input type=\"checkbox\" name=\"chk\" id=\"chk\" value=\"Y\" class=\"check_agree1\">";	<%/* 선택 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.incdtNo);
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.rcvNoAcp);
 	 		append +=  "<input type=\"hidden\" name=\"analReqSno\"  value=\""+gfn_nullRtnSpace(row.analReqSno)+"\" />";	<%/* 분석요청일련번호 */%>
 	 		append +=  "<input type=\"hidden\" name=\"evdcSno\"  value=\""+gfn_nullRtnSpace(row.evdcSno)+"\" />";	<%/* 증거일련번호 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  cfscNm;
 	 		append +=  "<input type=\"hidden\" name=\"cfscNm\"  value=\""+cfscNm+"\" />";	<%/* 증거구분 */%>
 	 		append += '</td>';
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.mdlNm)
 	 		append +=  "<input type=\"hidden\" name=\"mdlNm\"  value=\""+gfn_nullRtnSpace(row.mdlNm)+"\" />";	<%/* 모델명 */%>
 	 		append += '</td>';
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.serlNo)
 	 		append +=  "<input type=\"hidden\" name=\"serlNo\"  value=\""+gfn_nullRtnSpace(row.serlNo)+"\" />";	<%/* 시리얼번호 */%>
 	 		append += '</td>';
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.dispCfscDt)
 	 		append +=  "<input type=\"hidden\" name=\"cfscDt\"  value=\""+gfn_nullRtnSpace(row.dispCfscDt)+"\" />";	<%/* 압수일시 */%>
 	 		append += '</td>';
 	 		
	
			append += "</tr>";
	        $("#tbEvdcAplnDtl3 > tbody").append(append);
	 	});
	}
	
	
// 	data.__callFuncName__ ="fn_queryMList";
// 	data.__naviID__ ="page_navi";
// 	pageUtil.setPageNavi(data);
	
	
}


function fn_chgReqDiv(obj){
	var reqDiv = obj.value;
	//alert("======reqDiv====>>>"+reqDiv);
	if(reqDiv == "C07002"){ //  폐기요청구분: C07001	폐기, C07002 이송
		$("#reqTransInstt").show();
	}else{
		$("#reqTransInstt").hide();
		$("#reqTransInstt").val("");
	}
}

/**
 * @!@ 증거목록 조회 팝업
 * @param cd
 * @returns 
 */
function fn_schEvdcAnlsListPop() {
   
		

//  	fn_showUserPage("이미 추가된 장비는 선택할 수 없습니다. \n ggg \n ggg \n ggg \n ggg");
// 	return;
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/evdc/apln/evdcAnlsListPop",
		height: 750,
        width: 1000,
        title: '증거목록 조회 팝업',
        divId : 'evdcAnlsListPop'
        //divId : 'eqpMgmtMListPop'
	});

}

/**
 * 화면 컨트롤
 * @param  
 * @returns
 */
function fn_dispCont(){
	//dispconPgsStat
	console.log("========사용자구분 session_usergb=====>>>"+session_usergb+"\n========진행상태 dispconPgsStat=====>>>"+dispconPgsStat);
	
	//사용자구분 (C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)   
	//증거신청목록 진행상태 조회 (진행상태코드 C03001:임시등록, C03002:승인요청, C03003:검사승인, C03004:반려, C03005:문서접수, C03006:과장승인)
	
	$("#btn_evdcImsi").hide(); //임시저장
	$("#btn_evdcReq").hide(); //승인요청
	$("#btn_evdcMod").hide(); //수정
	$("#btn_evdcReqCnc").hide(); //승인요청 취소
	$("#btn_evdcDel").hide(); //삭제

	if(session_usergb == "C01001" && (gfn_isNull(dispconPgsStat) || dispconPgsStat=="C03001")){
		$("#btn_evdcImsi").show(); //임시저장
		$("#btn_evdcReq").show(); //승인요청
		$("#btn_evdcDel").show(); //삭제
	}else if(session_usergb == "C01001" && dispconPgsStat=="C03002"){
		$("#btn_evdcReqCnc").show(); //승인요청 취소
		$("#btn_evdcMod").show(); //수정
		$("#btn_evdcDel").show(); //삭제
	}
 	
}

</script>

</head>

<body>
<div id="con_wrap1">
        <div class="content">
           <!----현재위치----->
             
            <div id="contents_info">
                 <div class="sub_ttl">디지털 증거 폐기/이송 요청 수정</div><!-----타이틀------>
                 
                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">
<%-- 					 <input type="text" id="modFlag" name="modFlag" value="<c:out value="${param.modFlag}" />" /> --%>
					 
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:180px;">
						
						
						<table class="iptTblX2" id="tbEvdcAplnDtl">
							 
							<caption>등록</caption>
			               <colgroup>
				             <col width="15%" />
				             <col width="35%" />
				             <col width="15%" />
				             <col width="*" />
			               </colgroup>
			               <tbody>
			                 	<tr>
			                 		<th scope="row">요청번호</th>
									<td>
										<span id="spanDisuseReqSno"></span>
									</td>
									<th scope="row">검사(승인일자)</th>
									<td>
										&nbsp;
									</td>
			                 	</tr>
			                 	<tr>
									<th scope="row">요청기관<span class="fontred">*</span></th>
									<td>
										<span id="reqInsttNm"></span>
										<input type="hidden" name="reqInsttCd" id="reqInsttCd" data-requireNm="요청기관코드" data-maxLength="10" title="요청기관코드"/>
									</td>
									<th scope="row">요청부서<span class="fontred">*</span></th>
									<td>
										<span id="reqDepNm"></span>
									</td>
									
								</tr>
								<tr>
									<th scope="row">요청일자<span class="fontred">*</span></th>
									<td colspan="3">
										<input class="inpw10" title="요청일시" type="text" name="reqDt" id="reqDt"/>
										<input type="hidden" name="disuseReqSno" id="disuseReqSno" value="<c:out value="${param.disuseReqSno}" />"/>
										<input type="hidden" id="modFlag" name="modFlag" value="<c:out value="${param.modFlag}" />" />
										<input type="hidden" id="pgsStat" name="pgsStat" value="" />
										<input type="hidden" id="saveGb" name="saveGb" value="" />
									</td>
								</tr>
								<tr>
									<th scope="row">주임군검사<span class="fontred">*</span></th>
									<td><input class="inpw40" title="주임군검사" type="text" name="prosrNm" id="prosrNm" readonly/>
									    <button class="buttonG40" onclick="javascript:fn_openPop('prosr');return false;">검색</button>
										<input type="hidden" name="prosrId" id="prosrId"/>
									</td>
									<th scope="row">담당자(연락처,HP)<span class="fontred">*</span></th>
									<td><input class="inpw40" title="담당자연락처" type="text" name="regUserNm" id="regUserNm" readonly/>
										<input type="hidden" name="reqUserId" id="reqUserId"/>
										<button class="buttonG40" onclick="javascript:fn_openPop('regUser');return false;">검색</button>
									</td>
								</tr>
			                </tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
					
					<div class="sub_ttl">폐기대상 상세정보</div><!-----타이틀------>
                 
                  <div class="sub">
                     
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:60px;">
						
						<table class="iptTblX2" id="tbEvdcAplnDtl2">
							<caption>등록</caption>
			               <colgroup>
				             <col width="15%" />
				             <col width="35%" />
				             <col width="15%" />
				             <col width="*" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">요청구분<span class="fontred">*</span></th>
				                 <td >
				                 	<select id="reqDiv" name="reqDiv" onchange="fn_chgReqDiv(this);" class="selw6" >
					                </select> 
				                 </td>
				                 <th scope="row">이송요청기관</th>
				                 <td>
				                 	<input class="inpw60" title="이송요청기관" type="text" name="reqTransInstt" id="reqTransInstt" maxlength="100" style="display: none;"/>
				                 </td>
			                 </tr>
			                 
			                </tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
					
<!-- 					<div class="sub_ttl">장비 유지보수 등록</div>---타이틀---- -->
					<div class="flR"><button class="buttonR60" name="addRow" id="addRow" onclick="fn_schEvdcAnlsListPop();return false;">+ 추가</button><button class="buttonG60" name="delRow" id="delRow" onclick="fn_delRow();return false;">- 삭제</button></div>
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:320px;">
						<table class="iptTblX2" id="tbEvdcAplnDtl3"> 
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="8%">
                                <col />
                                <col width="14%">
                                <col width="14%">
                                <col width="14%">
                                <col width="14%">
                                <col width="14%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">선택</th>
                                    <th scope="col">사건번호</th>
                                    <th scope="col">접수번호</th>
                                    <th scope="col">구분</th>
                                    <th scope="col">모델명</th>
                                    <th scope="col">시리얼번호</th>
                                    <th scope="col">압수일시</th>
								</tr>
							</thead>
							<tbody id="detailTbody">
							</tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
                      
					</form>
                    <div class="btn_c">
                      <ul>
                        <li><a href="#" class="myButton" onclick="fn_modifyEvdcAplnInfo('imsi');return false;" style="display: none;" id="btn_evdcImsi" name="btn_evdcImsi">임시저장</a></li>
                        <li><a href="#" class="myButton" onclick="fn_modifyEvdcAplnInfo('req');return false;" style="display: none;" id="btn_evdcReq" name="btn_evdcReq">승인요청</a></li>
                        <li><a href="#" class="myButton" onclick="fn_modifyEvdcAplnInfo('reqCnc');return false;" style="display: none;" id="btn_evdcReqCnc" name="btn_evdcReqCnc">승인요청 취소</a></li>
                        <li><a href="#" class="RdButton" onclick="fn_modifyEvdcAplnInfo('mod');return false;" style="display: none;" id="btn_evdcMod" name="btn_evdcMod">수정</a></li>
                        <li><a href="#" class="RdButton" onclick="fn_modifyEvdcAplnInfo('del');return false;" style="display: none;" id="btn_evdcDel" name="btn_evdcDel">삭제</a></li>
                        <!-- <li><a href="#" class="myButton">재입력</a></li> -->
                        <li><a href="#" class="myButton" onclick="fn_searchList(1);return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->   
                       
                    
                          
                  </div>
            </div>
        
        </div>
 </div>
<div id="divCrgrFindPop"></div> 
<div id="evdcIncdtFindPop"></div>
<div id="divIncdtFindPop"></div>
<div id="evdcAnlsListPop"></div>
<div id="eqpMgmtFIndEqupQListPop2"></div>
<script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
var height= ifa.get(0).contentWindow.document.body.scrollHeight;
ifa.attr('height', height > 750 ?  height : 1750);
</script>
</body>
</html>