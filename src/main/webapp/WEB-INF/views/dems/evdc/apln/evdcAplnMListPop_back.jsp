<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비관리 > 장비대여 등록
 * 4. 설명 : 장비관리 > 장비대여 등록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var PW_Error = 0;
var ID_Duple = 1;

var modFlag = '${param.modFlag}'; //매입처명
var rentAplnSno = '${param.rentAplnSno}';



$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');	
	
	<%/* 달력 세팅 */%> 
 	gfn_calendarConfig("rentAplnDt", "", "", "");    <%/* 대여신청일자 */%> 
 	gfn_calendarConfig("rentStartDt", "rentEndDt", "minDate", "");    <%/* 대여시작일자 from */%>
	gfn_calendarConfig("rentEndDt", "rentStartDt", "maxDate", "");   <%/* 대여종료일자 to */%>
	
	
	<%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
    gfn_overMaxLength("rentReqInfo",250);
<%-- 	gfn_toNumber("cntrAmt"); <%/* 계약금액 */%> --%>
	
	//장비대여신청 정보 조회
	fn_eqpLendReqInfoSearch();
    
});

function fn_ajaxCntrFormCallback(data){
	$('#cntrForm option:eq(0)').before("<option value='' selected>선택</option");
}

/**
* //장비대여신청 정보 조회
* @param {string} page 항목에 대한 고유 식별자 
* @returns fn_callBack
*/
function fn_eqpLendReqInfoSearch(){

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");
	
	var input = document.createElement('input'); 
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "rentAplnSno");
	input.setAttribute("value", rentAplnSno);
	searchForm.appendChild(input);	
	
	document.body.appendChild(searchForm);
	
	var callUrl = "<c:url value = "/eqp/lend/queryEqpLendReqInfo.do"/>";	
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'insForm',callbackNm:'fn_eqpLendReqInfoSearchCallBack'});
}

function fn_eqpLendReqInfoSearchCallBack(data){
	$('form[name=searchForm]').remove();
	
	var dispRentDeptNm = gfn_nullRtnSpace(data.resultMap.deptNm);
	var dispRentTeamNm = gfn_nullRtnSpace(data.resultMap.teamNm);
	var dispRentAplnDt = gfn_dashDate2(gfn_nullRtnSpace(data.resultMap.rentAplnDt),'-');
	var dispRentUserNm = gfn_nullRtnSpace(data.resultMap.userNm);
	var dispRentUserTelNo = gfn_nullRtnSpace(data.resultMap.telNo);
	var dispRentUserHpTelNo = gfn_nullRtnSpace(data.resultMap.hpTelNo);
	//var dispRentUserInfo = dispRentUserNm+" ("+dispRentUserTelNo+", "+dispRentUserHpTelNo+")";
	var dispRentUserInfo =  !gfn_isNull(dispRentUserHpTelNo) ? dispRentUserNm+" ("+gfn_getTelNo(dispRentUserTelNo)+", "+gfn_getTelNo(dispRentUserHpTelNo)+")" : dispRentUserNm+" ("+gfn_getTelNo(dispRentUserTelNo)+")"; 
	
	var dispRentStartDt = gfn_dashDate2(gfn_nullRtnSpace(data.resultMap.rentStartDt),'-');
	var dispRentEndDt = gfn_dashDate2(gfn_nullRtnSpace(data.resultMap.rentEndDt),'-');
	
	
	console.log("=======dispRentUserInfo=======>>>"+dispRentUserInfo);
	//dispRentDeptNm
	$("#dispRentDeptNm").text(dispRentDeptNm);	//요청기관
	$("#dispRentTeamNm").text(dispRentTeamNm);	//요청부서
	$("#rentAplnDt").val(dispRentAplnDt);	//요청일자
	$("#dispRentUserTelNo").text(dispRentUserInfo); //담당자
	
	$("#rentStartDt").val(dispRentStartDt);	//대여시작일자
	$("#rentEndDt").val(dispRentEndDt);	//대여종료일자
	
	$("#rentReqInfo").trigger("keyup");
	
	//대여장비목록 조회
	fn_searchLendMgmtList(1);

}


/**
* 장비 상세 정보 조회 콜백
* @param  
* @returns
*/
function fn_queryEqpLendReqInfoCallBack(data){
	
	//console.log("상세정보 조회 성공");

	var dispRentDeptNm = gfn_nullRtnSpace(data.resultMap.deptNm);
	console.log("=======dispRentDeptNm=======>>>"+dispRentDeptNm);
	
// 	$("#eqpSno").text(eqpSno); 		
// 	$("#eqpNm").text(eqpNm); 		
// 	$("#eqpTypNm").text(eqpTypNm); 	
// 	$("#srNo").text(srNo); 			
	

	
}

/**
 * @!@ 장비 지원 관리 > 대여장비목록 조회
 * @param {int} page
 * @returns 
 */
function fn_searchLendMgmtList(page){
// 	debugger;
	var callUrl = "<c:url value='/eqp/lend/queryEqpLendDtlList.do'/>";
	$("#rentAplnSno").val(rentAplnSno);
	
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'insForm', callbackNm:'fn_searchLendMgmtListCallBack', page:page, perPageNum:1000});
}


/**
 * 대여장비목록 조회 콜백
 * @param  
 * @returns
 */
 function fn_searchLendMgmtListCallBack(data){ 
	
	//debugger;
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#tbLendDtl > colgroup").find("col").length;
	
	$("#tbLendDtl > tbody").empty();
	
//  	$("#totalcnt").text(data.totalCount);
	
  	if(listCnt < 1){
  		var append = "";
		append += "<tr>";
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		append += "</tr>";
		$("#tbLendDtl > tbody").append(append);
	}else{
	 	
		$.each(list,function(idx,row){
	 		
			//console.log("========idx zzzzzzzzzzzzzzzzzzz=====>>>"+idx);
			var append = "";
	 		append += "<tr>";
			
	 		append += '<td style=\"text-align:center\">';
	 		append +=  "<input type=\"checkbox\" name=\"chk\" id=\"chk\" value=\"Y\" class=\"check_agree1\" >";	<%/* 선택 */%>
	 		append += '</td>';
	 		
	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.eqpNm);	<%/* 장비명 */%>
 	 		append +=  "<input type=\"hidden\" name=\"eqpSno\"  value=\""+ row.eqpSno+"\" maxlength=\"50\" class=\"inpw70\"/>";	<%/* 장비일련번호 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.eqpTypNm);	<%/* 장비유형 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.srNo);	<%/* S/N */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.mdlNm);	<%/* 모델명 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_nullRtnSpace(row.mnftCo);	<%/* 제조사 */%>
 	 		append += '</td>';
 	 		
 	 		append += '<td style=\"text-align:center\">';
 	 		append +=  gfn_dashDate2(gfn_nullRtnSpace(row.purcDt),'-');	<%/* 도입일자 */%>
 	 		append += '</td>';
 	 		

	 		
	 		append += "</tr>";
	 		console.log("========idx append=====>>>"+append);
			//var append = "";
	        $("#tbLendDtl > tbody").append(append);
	  		
	  	});
	}
  	
//    	data.__callFuncName__ ="fn_searchList";
//  	data.__naviID__ ="page_navi";
//  	pageUtil.setPageNavi(data);
 }	 
 
function gfn_getTelNo(that){
	
	//alert("=========that=====>>>>"+that)
	var returnValue = that.replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-"); 
	
	//alert("=========returnValue=====>>>>"+returnValue)
	//$(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
	return returnValue;
 }



//장비대여신청 임시저장
function fn_modifyEqpLend(saveGubun){
	
	var conMsg = "";
	if(saveGubun=="req"){
		conMsg = "장비대여신청 하시겠습니까?";
		$("#pgsStat").val("C09001");
	}else{
		conMsg = "임시저장 하시겠습니까?";
		$("#pgsStat").val("");
	}
	
	
// 	if(!fn_saveCntChk()) return;
// 	if(!fn_valChk()) return;
	console.log("==========11111111111======>>>");
	//장비상세정보
	var tpFormArry = [];
	var $formDatas1 = $('#insForm').find('#tbMgmtDtl');
	var $formDatas2 = $('#insForm').find('#tbMgmtDtl2');
	console.log("==========222222222222======>>>");
	var formObj1 = new Object();
	var formObj2 = new Object();
	var cnt = 0;
	var cnt2 = 0;
	console.log("==========333333333333======>>>");
	$formDatas1.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			formObj1[item.name] = item.value;
			cnt++;
			console.log("========formDatas1["+index+"]========>>>>"+item.value);
		});
		
// 		dataObj = {};

	});
	
	$formDatas2.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			formObj2[item.name] = item.value;
			cnt2++;
			console.log("========formDatas2["+index+"]========>>>>"+item.value);
		});
		
// 		dataObj = {};

	});
	
	//alert("======cnt======>>>"+cnt);
	//tbMgmtDtl
	//tbMntDtl
	
	//유지보수 상세정보
	var tpArray = [];
	var dataObj = new Object();
	var cnt3 = 0;

	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			dataObj[item.name] = item.value;
			//console.log("==========item.name======>>>"+item.name+"\n==========item.value======>>>"+item.value);
			//cnt3++;
		});

// 		if(index % 2 == 0){
			tpArray.push(dataObj);
			dataObj = {};
// 		}

	});
	
	//alert("======cnt2======>>>"+cnt2);
	fn_showModalPage(conMsg, function() {
		
		$("#rentAplnSno").val(rentAplnSno);
		//alert("=======저장 pgsStat========>>>"+$("#pgsStat").val());
		var data = {rowDatas : tpArray, formDatas1 : formObj1, formDatas2 : formObj2};
	    var callUrl = "<c:url value='/eqp/lend/lendReqEqpLendLendReqRDtl.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyEqpLendCallBack'});
	});
	
	
}

function fn_modifyEqpLendCallBack(data){
	fn_searchList();
	//alert("임시저장 성공");
}

function fn_searchList(){
	//debugger;
	//alert("====insForm serialize=========>>>>"+$("#insForm").serialize());
	//parent.addNaviTab('관리자 > 사용자관리 > 급식기관관리상세',"<c:url value="/eqp/mnt/indexEqpMntMList.do"/>",'급식기관관리상세','M000000703');
	//var flag = $("#flag").val();
	//alert("====flag=======>>>"+flag);
// 	if(flag == "MgmtList"){
// 		parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mgmt/indexEqpMgmtMList.do"/>');
// 	}else{
// 		parent.$('#tabs-M000000703').find("iframe").attr("src", '<c:url value="/eqp/mnt/indexEqpMntMList.do"/>');
// 	}
	
	//var param = "?"+$("#searchForm").serialize();
	var param = "";
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/lend/indexEqpLendMList.do"/>'+param);	
}


<%/*아이디 중복 체크*/%>
function fn_IDcheck(){
	
	var checkParamId = $("#userId").val(); 
// 	alert(checkParamId.search(/\s/));
	
	if( checkParamId.search(/\s/) > 0 ){
		alert('ID에 공백이 들어갈 수 없습니다.');
		$("#userId").focus();
		return;
	} 
	
	if(checkParamId == ""){
		alert("아이디를 입력해 주세요.");
		$("#userId").focus();
		return;
	}else{
		var callUrl = "<c:url value='/fsys/user/sysUserCheckId.do'/>";
		requestUtil.search({callUrl:callUrl,srhFormNm:'insForm',setFormNm:'insForm',callbackNm:'fn_callbackpop'});
	}
	
}

function fn_callbackpop(data) {
	
	var resultCnt = data.egovMap.resultCnt;
// 	alert("fn_callbackpop 진입 resultCnt===>>> "+resultCnt);
// 	if(resultCnt > 0){
// 		alert("111111");
// 	}else{
// 		alert("2222222");
// 	}
// 	var existsCnt = resultCnt.substring(0,resultCnt.indexOf('-'));
// 	alert("fn_callbackpop 진입 1-1");
	if( resultCnt > 0 ) {
		alert('이미 존재하는 ID입니다.');
		$('#userId').val('');
		ID_Duple=1;
		return;
	} else {
		alert('사용가능합니다.');
		ID_Duple =0;
		return;
	}
	
}

/**
 * @ 장비 관리 삭제
 * @param
 * @returns 
 */
function fn_deleteEqpMgmt() {
	
	if(confirm("삭제 하시겠습니까?")){
		
		//사용자 정보 수정
        var callUrl = "<c:url value='/eqp/biz/delEqpBizUDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_deleteEqpBizCallback'});
	
	}
	
}

/**
 * @!@ 메뉴 관리 삭제 콜백
 * @param
 * @returns 
 */
function fn_deleteEqpBizCallback(data){
	fn_searchList();
}

/**
 * @!@ 사업관리조회 팝업
 * @param cd
 * @returns 
 */
function fn_searchEqpBizMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/mgmt/eqpMgmtFIndEqupQListPop&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 650,
        width: 1000,
        title: '장비조회 팝업',
        divId : 'eqpMgmtFIndEqupQListPop'
        //divId : 'eqpMgmtMListPop'
	});

}

<%/* 시작 처리 function */%>
function fn_addRow() {
// 	$("#addRow").click(function(){
		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length+1;

		var tpTag =     '<tr>'+
			'<td style="text-align:center"><input type="checkbox" name="chk" id="chk" value="Y" class="check_agree1"></td>'+
			'<td><textarea id="box" name="dfecCnts" rows=2 cols=30 maxlength="2000"></textarea></td>'+
			'<td><textarea id="box" name="rprCnts" rows=2 cols=30 maxlength="200"></textarea></td>'+
			'<td "text-align:center"><input type="text" name="rprStartDt" id="rprStartDt_'+idx+'" class="inpw50"></td>'+
			'<td "text-align:center"><input type="text" name="rprEndDt" id="rprEndDt_'+idx+'" class="inpw50"></td>'+
			'<td "text-align:center"><input type="text" name="rprCoNm" class="inpw70" maxlength="50"></td>'+
			'<td "text-align:center"><input type="text" name="coCrgr" class="inpw70" maxlength="50"></td>'+
		'</tr>';
		$("#detailTbody").append(tpTag);

		
		gfn_calendarConfig("rprStartDt_"+idx, "", "", "");
		gfn_calendarConfig("rprEndDt_"+idx, "", "", "");
// 	});
		
}

<%/* 삭제버튼 클릭 function */%>
function fn_delRow() {
	var cnt = 0;
	$('#detailTbody > tr').each(function(index,item){
		var $chkbox = $(item).find('input[type=checkbox]');
		$chkbox.each(function(index,item){

			if($(this).is(':checked') == true){
				cnt++;
				//$(this).parent().parent('tr').next().remove();
				$(this).parent().parent('tr').remove();
			}
		})

	});

	if(cnt < 1){
		//alert('선택해주세요');
		fn_showUserPage( "삭제할 내역을 선택하십시요.", function() {
			return;		
        });
	}


//		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length/2;
	

//		for(var i = 1; i <= idx; i++){
//			if(i == 1){
//				$('#detailTbody > tr').eq(0).find('td:eq(1)').text(i);
//				$('#detailTbody > tr').eq(0).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
//				$('#detailTbody > tr').eq(0).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
//				$('#detailTbody > tr').eq(1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
//			}else{
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(1)').text(i);
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
//				$('#detailTbody > tr').eq(i*2-1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
//			}

//		}
//		fn_makeSbx(idx);
//		gfn_calendarConfig("cfscDate_"+idx, "", "", "");
		
}

<%/* 필수입력체크 */%>
function fn_valChk(){
	
	var chkCnt = 0;
	
	$('#tbMntDtl :input[name=chk]').each(function(index) {
		// 체크여부 확인
		if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
			
 			if($("textarea[name=dfecCnts]").eq(index).val() == ""){
 				fn_showUserPage( "결함내용은(는) 필수 입력 항목입니다.", function() {
 					$("textarea[name=dfecCnts]").eq(index).focus();
                });
 				return false;
 			}
 			
 			if($("textarea[name=rprCnts]").eq(index).val() == ""){
 				fn_showUserPage( "수리내용은(는) 필수 입력 항목입니다.", function() {
 					$("textarea[name=rprCnts]").eq(index).focus();
                });
 				return false;
 			}

 			
			if($("input[name^=rprStartDt]").eq(index).val() == ""){
				fn_showUserPage( "수리시작일은(는) 필수 입력 항목입니다.", function() {
 					$("input[name^=rprStartDt]").eq(index).focus();
                });
				return false;
			}
			
			if($("input[name^=rprEndDt]").eq(index).val() == ""){
				fn_showUserPage( "수리종료일은(는) 필수 입력 항목입니다.", function() {
 					$("input[name^=rprEndDt]").eq(index).focus();
                });
				return false;
			}
			
			if($("input[name^=rprCoNm]").eq(index).val() == ""){
				fn_showUserPage( "수리업체명은(는) 필수 입력 항목입니다.", function() {
 					$("input[name^=rprCoNm]").eq(index).focus();
                });
				return false;
			}
			
			if($("input[name^=coCrgr]").eq(index).val() == ""){
				fn_showUserPage( "업체담당자명은(는) 필수 입력 항목입니다.", function() {
 					$("input[name^=coCrgr]").eq(index).focus();
                });
				return false;
			}
 			return true;
			//chkCnt++;
		}
	});
	return true;
	//console.log("=========chkCnt====>>>"+chkCnt);

}
	
function fn_saveCntChk(){
	var chkCnt = 0;
	$('#tbMntDtl :input[name=chk]').each(function(index) {
		// 체크여부 확인
		if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
			chkCnt++;
		}
	});
	
	if(chkCnt<=0){
		fn_showUserPage( "저장할 내역을 선택하세요.", function() {
			return false;		
        });
	}else{
		alert("======chkCnt====>>>"+chkCnt);
		return true;
	}
       
}

/**
 * @!@ 사건관리 조회 팝업
 * @param cdId
 * @returns 
 */
function fn_incdtIncdtMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtMListPop",
		height: 700,
        width: 1000,
        title: '사건관리 조회',
        divId : 'incdtIncdtMListPop'
	});

}

/**
 * @!@ 장비조회 팝업
 * @param cd
 * @returns 
 */
function fn_searchEqpBizMListPop2() {
   
		

//  	fn_showUserPage("이미 추가된 장비는 선택할 수 없습니다. \n ggg \n ggg \n ggg \n ggg");
// 	return;
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/mgmt/eqpMgmtFIndEqupQListPop2&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 650,
        width: 1000,
        title: '장비조회 팝업',
        divId : 'eqpMgmtFIndEqupQListPop2'
        //divId : 'eqpMgmtMListPop'
	});

}
</script>

</head>

<body>
<div id="con_wrap1">
        <div class="content">
           <!----현재위치----->
             
            <div id="contents_info">
                 <div class="sub_ttl">장비 대여 신청 수정</div><!-----타이틀------>
                 
                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">
<%-- 					 <input type="text" id="modFlag" name="modFlag" value="<c:out value="${param.modFlag}" />" /> --%>
					 
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:150px;">
						
						
						<table class="iptTblX2" id="tbMgmtDtl">
							 
							<caption>등록</caption>
			               <colgroup>
				             <col width="15%" />
				             <col width="35%" />
				             <col width="15%" />
				             <col width="*" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">요청기관</th>
				                 <td>
				                 	<span id="dispRentDeptNm"></span>
<%-- 				                 	<input type="text" id="bizNm" name="bizNm"  value= "<c:out value='${session_telno}' />" maxlength="100"/> --%>

				                 </td>
				                 <th scope="row">요청부서</th>
				                 <td >
				                 	<span id="dispRentTeamNm"></span>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">요청일자</th>
				                 <td >
				                 	<input id="rentAplnDt" name="rentAplnDt" type="text" value=""  maxlength="10"/>
<%-- 				                 	 <input type="text" id="modFlag" name="modFlag" value="<c:out value="${param.modFlag}" />" /> --%>
				                 </td>
				                 <th scope="row">사건번호(사건명)</th>
				                 <td >
				                 	<input type="text" id="IncdtNm" name="IncdtNm"  value=""  maxlength="100"/>
				                 	<a href="#" class="buttonG60" onclick="fn_incdtIncdtMListPop();return false;">검색</a>
				                 </td>
			                 </tr>
			                 <tr>
								<th scope="row">담당자(연락처, HP)</th>
								<td colspan="3">
									<span id="dispRentUserTelNo"></span>
									<input type="text" id="modFlag" name="modFlag" value="<c:out value="${param.modFlag}" />" />
									<input type="hidden" id="sessionUserId" name="sessionUserId" value="<c:out value="${session_userid}" />" />
									<input type="hidden" id="rentInsttCd" name="rentInsttCd" value="<c:out value="${session_insttcd}" />" />
									<input type="text" id="rentAplnSno" name="rentAplnSno" value="" />
									<input type="text" id="pgsStat" name="pgsStat" value="" />
								</td>
							</tr>
							
			                 
			                </tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
					
					<div class="sub_ttl">대여 정보</div><!-----타이틀------>
                 
                  <div class="sub">
                     
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:150px;">
						
						<table class="iptTblX2" id="tbMgmtDtl2">
							<caption>등록</caption>
			               <colgroup>
				             <col width="15%" />
				             <col  />
				             
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">대여기간</th>
				                 <td>
									<input id="rentStartDt" name="rentStartDt"  title="대여시작일자"  type="text" value="" style='width:100px' maxlength="10"/> 
					                <input id="rentEndDt" name="rentEndDt"  title="대여종료일자"  type="text" value="" style='width:100px' maxlength="10"/>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">요청사항</th>
				                 <td >
				                 	<textarea id="rentReqInfo" name="rentReqInfo" rows=2 cols=20 maxlength="260"></textarea>
				                 	<span class="txt_info" name="rentReqInfoByteChk" id="rentReqInfoByteChk"></span>
				                 </td>
			                 </tr>
			                 
			                </tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
					
<!-- 					<div class="sub_ttl">장비 유지보수 등록</div>---타이틀---- -->
					<div class="flR"><button class="buttonR60" name="addRow" id="addRow" onclick="fn_searchEqpBizMListPop2();return false;">+ 추가</button><button class="buttonG60" name="delRow" id="delRow" onclick="fn_delRow();return false;">- 삭제</button></div>
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:350px;">
						<table class="iptTblX2" id="tbLendDtl">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="5%">
                                <col />
                                <col width="11%">
                                <col width="16%">
                                <col width="16%">
                                <col width="16%">
                                <col width="11%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">선택</th>
                                    <th scope="col">장비명</th>
                                    <th scope="col">장비유형</th>
                                    <th scope="col">S/N</th>
                                    <th scope="col">모델명</th>
                                    <th scope="col">제조사</th>
                                    <th scope="col">도입일자</th>
								</tr>
							</thead>
							<tbody id="detailTbody">
							</tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
                      
					</form>
                    <div class="btn_c">
                      <ul>
                        <li><a href="#" class="myButton" onclick="fn_modifyEqpLend('imsi');return false;">임시저장</a></li>
                        <li><a href="#" class="myButton" onclick="fn_modifyEqpLend('req');return false;">장비대여신청</a></li>
                        <!-- <li><a href="#" class="myButton">재입력</a></li> -->
                        <li><a href="#" class="myButton" onclick="fn_searchList(1);return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->   
                       
                    
                          
                  </div>
            </div>
        
        </div>
 </div>
<div id="incdtIncdtMListPop"></div>
<div id="eqpMgmtFIndEqupQListPop2"></div>
<script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
var height= ifa.get(0).contentWindow.document.body.scrollHeight;
ifa.attr('height', height > 750 ?  height : 1750);
</script>
</body>
</html>