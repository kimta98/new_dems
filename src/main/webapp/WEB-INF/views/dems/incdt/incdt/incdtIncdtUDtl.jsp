<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 사건 관리 수정 화면
 * 4. 설명 : @!@ 사건 관리 수정 화면 
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	<%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
	/* <span class="txt_info" name="ctnByteChk" id="ctnByteChk"></span> */
    gfn_overMaxLength("incdtExpl",4000);
	
    if(session_usergb == "C01001" || session_usergb == "C01003"){
		$("#saveBtn").show();
		$("#delBtn").show();
		if(session_usergb == "C01003"){
			$("#aprvrNm").val("관리자");
			$("#aprvrId").val("Admin");
			$("#aprvrTd>button").hide();
		}
	}
    
    fn_queryMDtl();
    
});

/**
 * @!@ 사건관리 상세 조회
 * @param
 * @returns 
 */
function fn_queryMDtl(){
	
	var callUrl = "<c:url value='/incdt/incdt/queryIncdtIncdtMDtl.do'/>";
	
	requestUtil.search({callUrl:callUrl, srhFormNm:'insForm', callbackNm:'fn_queryMDtlCallback'});
	
}

/**
 * @!@ 사건관리 상세 조회 콜백
 * @param
 * @returns 
 */
function fn_queryMDtlCallback(data){
	$("#incdtExpl").trigger("keyup");
}

/**
 * @!@ 죄명 조회 팝업
 * @param cdId
 * @returns 
 */
function fn_sysCrimeCodeMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/sys/crimecode/sysCrimeCodeMListPop",
		height: 700,
        width: 1000,
        title: '죄명 조회',
        divId : 'sysCrimeCodeMListPop'
	});

}

/**
 * @!@ 담당자 조회 팝업
 * @param cdId
 * @returns 
 */
var findUserGb = "";
var targetTagId = "";
var targetTagNm = "";
function fn_incdtRegUserMListPop(userGb, tagId, tagNm) {

	findUserGb = userGb;
	targetTagId = tagId;
	targetTagNm = tagNm;
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtRegUserMListPop",
		height: 700,
        width: 1000,
        title: '담당자 조회',
        divId : 'incdtRegUserMListPop'
	});

}

/**
* @!@ 사건을 사용중인지 카운트로 조회
* @param cdId
* @returns 
*/
function fn_queryIncdtUsingChkCnt(action){
	var callbackNm = "";
	if(action == "SAVE"){
		callbackNm = "fn_updIncdtIncdtUDtl";
	}else if(action == "DEL"){
		callbackNm = "fn_delIncdtIncdtUDtl";
	}
	var callUrl = "<c:url value='/incdt/incdt/queryIncdtUsingChkCnt.do'/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'insForm',callbackNm:callbackNm});
}

/**
* @!@ 사건 관리 수정
* @param
* @returns 
*/
function fn_updIncdtIncdtUDtl(data){

	if(!validUtil.checkInputValid({valFormID:'insForm'})){
		return;
	}
	
	var msg = "저장 하시겠습니까?";
	if($("#useYn").val() == "N"){
		if(data.chkCnt > 0){
			msg = "현재 사용중인 사건 입니다. 미사용으로 저장 하시겠습니까? (사용중인 목록에서 제외 됩니다)";
		}
	}
	
	fn_showModalPage(msg, function() {
		var callUrl = "<c:url value='/incdt/incdt/updIncdtIncdtUDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_updIncdtIncdtUDtlCallback'});
	});
}

/**
 * @!@ 사건 관리 수정 콜백
 * @param {json} data
 * @returns 
 */
function fn_updIncdtIncdtUDtlCallback(data){
	fn_indexIncdtIncdtMList();
}
 

 
 
/**
* @!@ 사건 관리 삭제
* @param
* @returns 
*/
function fn_delIncdtIncdtUDtl(data){

	var msg = "삭제 하시겠습니까? (사용중인 목록에서 제외 됩니다)";
	if(data.chkCnt > 0){
		msg = "현재 사용중인 사건 입니다. 삭제 하시겠습니까? (사용중인 목록에서 제외 됩니다)";
	}
	
	fn_showModalPage(msg, function() {
		var callUrl = "<c:url value='/incdt/incdt/delIncdtIncdtUDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_delIncdtIncdtUDtlCallback'});
	});
}

/**
 * @!@ 사건 관리 삭제 콜백
 * @param {json} data
 * @returns 
 */
function fn_delIncdtIncdtUDtlCallback(data){
	fn_indexIncdtIncdtMList();
}
 
 

/**
 * @!@ 사건 관리 리스트 화면 이동
 * @param
 * @returns 
 */
function fn_indexIncdtIncdtMList(){
	var param = "?"+$("#insForm").serialize();
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/incdt/incdt/indexIncdtIncdtMList.do"/>'+param);
}
</script>

</head>

<body>
<div id="con_wrap">
        <div class="content">
           <!----현재위치----->
             
            <div id="contents_info">
                 <div class="sub_ttl">사건 수정</div><!-----타이틀------>
                 
                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">
                     	<input id="incdtSno" name="incdtSno"  title="사건일련번호" type="hidden" value="<c:out value='${param.incdtSno}'/>" class="inpw50" />
                      <div class="t_list">
		                 <table class="iptTblX">
			               <caption>수정</caption>
			               <colgroup>
				             <col width="20%" />
				             <col width="30%" />
				             <col width="20%" />
				             <col width="30%" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">사건 번호<span class="fontred">*</span></th>
				                 <td>
				                 	<input id="incdtNo" name="incdtNo"  title="사건번호"  type="text" class="inpw50" data-requireNm="사건번호" data-maxLength="200" />
				                 </td>
				                 <th scope="row">사건명<span class="fontred">*</span></th>
				                 <td>
				                 	<input id="incdtNm" name="incdtNm"  title="사건명"  type="text" class="inpw50" data-requireNm="사건명" data-maxLength="200" />
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">죄명<span class="fontred">*</span></th>
				                 <td>
							 		<input id="crimeNm" name="crimeNm"  title="죄명"  type="text" readonly class="inpw50" data-requireNm="죄명" />
				                 	<input id="crimeCd" name="crimeCd"  title="법위반코드"  type="hidden" class="inpw50" data-maxLength="10"/>
							 		<button class="buttonG40" onclick="fn_sysCrimeCodeMListPop();return false;">검색</button>
				                 </td>
				                 <th scope="row">담당부서</th>
				                 <td>
				                 	<span id="regDeptNm"></span>
				                 	<input id="regInsttCd" name="regInsttCd"  title="담당부서"  type="hidden" value="" readonly class="inpw50" />
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">담당자<span class="fontred">*</span></th>
				                 <td>
							 		<input id="regUserNm" name="regUserNm"  title="담당자"  type="text" value="" readonly class="inpw50" data-requireNm="담당자" />
							 		<input id="regUserId" name="regUserId"  title="담당자ID"  type="hidden" value="" readonly class="inpw50" data-maxLength="20"/>
							 		<button class="buttonG40" onclick="fn_incdtRegUserMListPop('C01001', 'regUserId', 'regUserNm');return false;">검색</button>
				                 </td>
				                 <th scope="row">주임검사(승인자)<span class="fontred">*</span></th>
				                 <td id="aprvrTd">
				                 	<input id="aprvrNm" name="aprvrNm"  title="주임검사(승인자)"  type="text" readonly class="inpw50" data-requireNm="주임검사(승인자)" />
				                 	<input id="aprvrId" name="aprvrId"  title="주임검사(승인자)ID"  type="hidden" readonly class="inpw50" data-maxLength="20"/>
				                 	<button class="buttonG40" onclick="fn_incdtRegUserMListPop('C01002', 'aprvrId', 'aprvrNm');return false;">검색</button>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">사용여부</th>
				                 <td colspan="3">
				                 	<select id="useYn" name="useYn" title="사용여부" class="selw6">
					                   <option value="Y" selected>사용</option>
					                   <option value="N">미사용</option>
							 		</select>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">사건 개요</th>
				                 <td colspan="3">
				                 	<textarea name="incdtExpl" id="incdtExpl" title="사건 개요" rows="35" cols="10"  style="width:99%; height:280px;" data-maxLength="4000" ></textarea>
	                                <span class="txt_info" name="incdtExplByteChk" id="incdtExplByteChk"></span>
				                 </td>
			                 </tr>
			                </tbody>
		                 </table>
	                  </div>
					</form>
                    <div class="btn_c">
                      <ul>
                        <li><a href="javascript:void(0);" class="RdButton" onclick="fn_queryIncdtUsingChkCnt('SAVE');return false;" id="saveBtn" style="display: none;">저장</a></li>
                        <li><a href="javascript:void(0);" class="RdButton" onclick="fn_queryIncdtUsingChkCnt('DEL');return false;" id="delBtn" style="display: none;">삭제</a></li>
                        <!-- <li><a href="javascript:void(0);" class="myButton">재입력</a></li> -->
                        <li><a href="javascript:void(0);" class="myButton" onclick="fn_indexIncdtIncdtMList();return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->   
                       
                    
                          
                  </div>
            </div>
        
        </div>
 </div>
<div id="sysCrimeCodeMListPop"></div>
<div id="incdtRegUserMListPop"></div>
</body>
</html>