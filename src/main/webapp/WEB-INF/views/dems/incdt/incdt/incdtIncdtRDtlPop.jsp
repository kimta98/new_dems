<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 사건 관리 등록 화면
 * 4. 설명 : @!@ 사건 관리 등록 화면 
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>

<head>
<%-- <%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %> --%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>디지털 증거 관리 시스템</title>

<script type="text/javaScript" language="javascript" defer="defer">
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	<%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
	/* <span class="txt_info" name="ctnByteChk" id="ctnByteChk"></span> */
    gfn_overMaxLength("incdtExpl",4000);
    
    if(session_usergb == "C01001" || session_usergb == "C01003"){
		$("#regBtn").show();
		if(session_usergb == "C01003"){
			$("#aprvrNm").val("관리자");
			$("#aprvrId").val("Admin");
			$("#aprvrTd>button").hide();
		}
	}
    
    $("#regUserId").val(session_userid);
    $("#regUserNm").val(session_usernm);
    $("#deptNm").html(session_deptnm);
    $("#regInsttCd").val(session_insttcd);
    
});

/**
 * @!@ 죄명 조회 팝업
 * @param cdId
 * @returns 
 */
function fn_sysCrimeCodeMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/sys/pop/sysCrimeCodeMListPop",
		height: 700,
        width: 1000,
        title: '죄명 조회',
        divId : 'popSysCrimeCodeMListPop'
	});

}

/**
 * @!@ 담당자 조회 팝업
 * @param cdId
 * @returns 
 */
var findUserGb = "";
var targetTagId = "";
var targetTagNm = "";
function fn_incdtRegUserMListPop(userGb, tagId, tagNm) {
	if(session_usergb == "C01003"){
		findUserGb = "C01003";	
	}else{
		findUserGb = userGb;
	}
	targetTagId = tagId;
	targetTagNm = tagNm;
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/pop/incdtRegUserMListPop",
		height: 700,
        width: 1000,
        title: '담당자 조회',
        divId : 'popIncdtRegUserMListPop'
	});

}

/**
* @!@ 사건 관리 등록
* @param
* @returns 
*/
function fn_regIncdtIncdtRDtl(){

	if(!validUtil.checkInputValid({valFormID:'popInsForm'})){
		return;
	}
	
	fn_showModalPage("등록 하시겠습니까?", function() {
		var callUrl = "<c:url value='/incdt/incdt/regIncdtIncdtRDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'popInsForm',callbackNm:'fn_regIncdtIncdtRDtlCallback'});
	});
}

/**
 * @!@ 사건 관리 등록 콜백
 * @param {json} data
 * @returns 
 */
function fn_regIncdtIncdtRDtlCallback(data){
	 fn_incdtIncdtMListPop();
	 fn_dialogClose("incdtIncdtRDtlPop");
}

/**
 * @!@ 사건 관리 리스트 화면 이동
 * @param
 * @returns 
 */
function fn_indexIncdtIncdtMList(){
	var param = "?"+$("#popInsForm").serialize();
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/incdt/incdt/indexIncdtIncdtMList.do"/>'+param);
}

function fn_incdtMListView(){
	fn_incdtIncdtMListPop();
	fn_dialogClose("incdtIncdtRDtlPop");
}
</script>

</head>

<body>
<div id="con_wrap_pop">
	<div class="contents">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                      <div class="window_popup">
                          <div class="sub_ttl">사건 등록</div>
                         
                          <div class="sub">
                             
                             <!--------------목록---------------------->
                             <form name="popInsForm" id="popInsForm" method="post">
		                      <div class="t_list">
				                 <table class="iptTblX">
                                        <caption>등록</caption>
						               <colgroup>
							             <col width="20%" />
							             <col width="30%" />
							             <col width="20%" />
							             <col width="30%" />
						               </colgroup>
						               <tbody>
						                 <tr>
							                 <th scope="row">사건 번호<span class="fontred">*</span></th>
							                 <td>
							                 	<input id="incdtNo" name="incdtNo"  title="사건번호"  type="text" class="inpw50" data-requireNm="사건번호" data-maxLength="200" />
							                 </td>
							                 <th scope="row">사건명<span class="fontred">*</span></th>
							                 <td>
							                 	<input id="incdtNm" name="incdtNm"  title="사건명"  type="text" class="inpw50" data-requireNm="사건명" data-maxLength="200" />
							                 </td>
						                 </tr>
						                 <tr>
							                 <th scope="row">죄명<span class="fontred">*</span></th>
							                 <td>
										 		<input id="crimeNm" name="crimeNm"  title="죄명"  type="text" readonly class="inpw50" data-requireNm="죄명" />
										 		<input id="crimeCd" name="crimeCd"  title="법위반코드"  type="hidden" class="inpw50" data-maxLength="10"/>
										 		<button class="buttonG40" onclick="fn_sysCrimeCodeMListPop();return false;">검색</button>
							                 </td>
							                 <th scope="row">담당부서</th>
							                 <td>
							                 	<span id="deptNm"></span>
							                 	<input id="regInsttCd" name="regInsttCd"  title="담당부서"  type="hidden" class="inpw50" />
							                 </td>
						                 </tr>
						                 <tr>
							                 <th scope="row">담당자<span class="fontred">*</span></th>
							                 <td>
										 		<input id="regUserNm" name="regUserNm"  title="담당자"  type="text" value="" readonly class="inpw50" data-requireNm="담당자" />
										 		<input id="regUserId" name="regUserId"  title="담당자ID"  type="hidden" value="" readonly class="inpw50" data-maxLength="20"/>
										 		<button class="buttonG40" onclick="fn_incdtRegUserMListPop('C01001', 'regUserId', 'regUserNm');return false;">검색</button>
							                 </td>
							                 <th scope="row">주임검사(승인자)<span class="fontred">*</span></th>
							                 <td id="aprvrTd">
							                 	<input id="aprvrNm" name="aprvrNm"  title="주임검사(승인자)"  type="text" readonly class="inpw50" data-requireNm="주임검사(승인자)" />
							                 	<input id="aprvrId" name="aprvrId"  title="주임검사(승인자)ID"  type="hidden" readonly class="inpw50" data-maxLength="20"/>
							                 	<button class="buttonG40" onclick="fn_incdtRegUserMListPop('C01002', 'aprvrId', 'aprvrNm');return false;">검색</button>
							                 </td>
						                 </tr>
						                 <tr>
							                 <th scope="row">사용여부</th>
							                 <td colspan="3">
							                 	<select id="useYn" name="useYn" title="사용여부" class="selw6">
								                   <option value="Y" selected>사용</option>
								                   <option value="N">미사용</option>
										 		</select>
							                 </td>
						                 </tr>
						                 <tr>
							                 <th scope="row">사건 개요</th>
							                 <td colspan="3">
							                 	<textarea name="incdtExpl" id="incdtExpl" title="사건 개요" rows="35" cols="10"  style="width:99%; height:280px;" data-maxLength="4000" ></textarea>
				                                <span class="txt_info" name="incdtExplByteChk" id="incdtExplByteChk"></span>
							                 </td>
						                 </tr>
						                </tbody>
                                     </table>
                             </div>
                            </form>
                            <div class="btn_c">
		                      <ul>
		                        <li><a href="javascript:void(0)" class="RdButton" onclick="fn_regIncdtIncdtRDtl();return false;" id="regBtn" style="display: none;">등록</a></li>
		                        <!-- <li><a href="javascript:void(0)" class="myButton">재입력</a></li> -->
		                        <li><a href="javascript:void(0)" class="myButton" onclick="fn_incdtMListView();return false;">취소</a></li>
		                      </ul>
		                    </div>
                          
                          </div>
                         
                    </div>
               </div>
                 <!---  //contnets  적용 ------>
       </div>
       
  </div>
 <div id="popIncdtRegUserMListPop"></div>
<div id="popSysCrimeCodeMListPop"></div>
</body>
</html>