<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 사건 관리 리스트 팝업
 * 4. 설명 : @!@ 사건 관리 리스트 팝업
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>디지털 증거 관리 시스템</title>

<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
	var codeInfo = [{cdId:'crimeGrp',selectId:'srcCrimeGrp',type:'4', callbackNm:'fn_ajaxCodeListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	//fn_ajaxCodeList(codeInfo);
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});
});

/**
 * @!@ 그룹죄명 코드 리스트 조회 콜백
 * @param {json} data 
 * @returns 
 */
function fn_ajaxCodeListCallback(data){
	$('#srcCrimeGrp').prepend("<option value='' selected>전체</option");
	fn_queryMList(1);
}
 
/**
 * @!@ 사건 관리 리스트 조회
 * @param {int} page
 * @returns 
 */
function fn_queryMList(page){
	var callUrl = "<c:url value='/incdt/incdt/queryIncdtIncdtMList.do'/>";
	
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_queryMListCallback', page:page, perPageNum:10});
	
}

/**
 * @!@ 사건 관리 리스트 조회 콜백
 * @param {json} data
 * @returns 
 */
function fn_queryMListCallback(data){
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;
	
	$("#listTab > tbody").empty();
	
	if(listCnt == 0){
		var append = "";
		append += "<tr>";
		
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		
		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
		$.each(list,function(idx,row){
			var append = "";
			append += "<tr>";
	
			append += "<td>"+row.incdtNo+"</td>";
			append += "<td>" + gfn_nullRtnSpace(row.incdtNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.crimeNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.regUserNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.aprvrId) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.regrNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.regDt) + "</td>";
			append += '<td><button class="byButton" onclick="fn_choiseBtnClick(\''+gfn_nullRtnSpace(row.incdtSno)+'\',\''+gfn_nullRtnSpace(row.incdtNo)+'\' ,\''+gfn_nullRtnSpace(row.incdtNm)+'\',\''+gfn_nullRtnSpace(row.regTelNo)+'\',\''+gfn_nullRtnSpace(row.regHpTelNo)+'\',\''+gfn_nullRtnSpace(row.regUserId)+'\',\''+gfn_nullRtnSpace(row.regUserNm)+'\',\''+gfn_nullRtnSpace(row.aprvTelNo)+'\',\''+gfn_nullRtnSpace(row.aprvHpTelNo)+'\',\''+gfn_nullRtnSpace(row.aprvrId)+'\',\''+gfn_nullRtnSpace(row.aprvrNm)+'\');return false;">선택</button></td>';
	
			append += "</tr>";
	        $("#listTab > tbody").append(append);
	 	});
	}
	
	data.__callFuncName__ ="fn_queryMList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
	$("#totalCount").text(data.totalCount);
	
}

function fn_choiseBtnClick(incdtSno, incdtNo, incdtNm, regTelNo, regHpTelNo, regUserId, regUserNm, aprvTelNo, aprvHpTelNo, aprvrId, aprvrNm){
	incdtNo += "(" + incdtNm + ")";
	$("#incdtSno").val(incdtSno);
	$("#incdtNm").val(incdtNo);
	$("#reqUserNm").val(regUserNm + " (tel:" + regTelNo + ", hp:" + regHpTelNo + ")");
	$("#reqUserId").val(regUserId);
	$("#prosrNm").val(aprvrNm + " (tel:" + aprvTelNo + ", hp:" + aprvHpTelNo + ")");
	$("#prosrId").val(aprvrId);
	fn_dialogClose("incdtIncdtMListPop");
}

function fn_incdtRDtlView(){
	fn_incdtIncdtRDtlPop();
	fn_dialogClose("incdtIncdtMListPop");
}

</script>

</head>
<body>
<div id="con_wrap_pop">
	<div class="contents">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                      <div class="window_popup">
                          <div class="sub_ttl">사건 관리 조회</div>
                         
                          <div class="sub">
                             <!--------------검색------------------>
                             <form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
                             	<input id="userGb" name="userGb"  title="사용자구분"  type="hidden" class="inpw50" />
                             	<input id="srcUseYn" name="srcUseYn"  title="사용여부"  type="hidden" class="inpw50" value="Y"/>
                            <div class="t_head">
                                  <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_queryMList(1);">
                                        <caption>검색</caption>
                                         <colgroup>
                                            <col width="20%">
                                            <col width="30%">
                                            <col width="20%">
                                            <col width="30%">
                                         </colgroup>
                                   <thead>
                                     <tr>
                                         	<th scope="row" class="hcolor">사건번호</th>
								           <td>
								               <input type="text" id="srcIncdtNo" name="srcIncdtNo" title="사건번호" class="inpw80"/>
								           </td>
								           <th scope="row" class="hcolor">죄명</th>
								           <td>
								               <select id="srcCrimeGrp" name="srcCrimeGrp" title="죄명" onchange="fn_queryMList(1)" class="selw6">
								               </select>
								           </td>
								      </tr>
								      <tr>
                                         	<th scope="row" class="hcolor">사건명</th>
								           <td>
								               <input type="text" id="srcIncdtNm" name="srcIncdtNm" title="사건명" class="inpw80"/>
								           </td>
								           <th scope="row" class="hcolor">담당자</th>
								           <td>
								               <input type="text" id="srcRegUserNm" name="srcRegUserNm" title="담당자" class="inpw80"/>
								           </td>
                                      </tr>
                                   </thead>
                                </table>
                              </div>
                            <div class="btn_c">
                                  <ul>
                                     <li><a href="javascript:void(0)" onclick="fn_queryMList(1);return false;" class="gyButton">조회</a></li>
                                     <c:if test="${paramMap.flag ne 'update' }">
                                     	<li><a href="javascript:void(0)" onclick="fn_incdtRDtlView();return false;" class="myButton">등록</a></li>
                                     </c:if>
                                     <!-- <li><a href="javascript:fn_indexSysCrimeCodeRDtl();" class="myButton">등록</a></li> -->
                                     <!-- <li><a href="javascript:void(0);" class="myButton">엑셀</a></li> -->
                                  </ul>
                               </div>
                               </form>
                            <!--------------//검색------------------>
                            
                            <!--------------결과------------------>
                             <div class="r_num">| 결과  <strong style="color:#C00" id="totalCount">0</strong>건</div>
                             
                             <!--------------목록---------------------->
                             <div class="t_list">  
                                  <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="20%">
                                              <col width="20%">
                                              <col width="10%">
                                              <col width="10%">
                                              <col width="10%">
                                              <col width="10%">
                                              <col width="10%">
                                              <col width="10%">
                                          </colgroup>
                                            <thead>
                                              <tr>
                                                 <th scope="col">사건번호</th>
                                                 <th scope="col">사건명</th>
                                                 <th scope="col">죄명</th>
                                                 <th scope="col">담당자</th>
                                                 <th scope="col">주임검사</th>
                                                 <th scope="col">작성자</th>
                                                 <th scope="col">작성일</th>
                                                 <th scope="col">선택</th>
                                              </tr>
                                            </thead>
                                            <tbody>
												<tr><td colspan="8">조회 결과가 없습니다.</td></tr>
                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->
                             
                             <!-----------------------페이징----------------------->
                             <div id="page_navi" class="page_wrap"></div>
                               <!-----------------------//페이징----------------------->
                          
                          </div>
                         
                    </div>
               </div>
                 <!---  //contnets  적용 ------>
       </div>
  </div>
</body>
</html>