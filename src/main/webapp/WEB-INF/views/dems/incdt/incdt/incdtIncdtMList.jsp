<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 사건 관리 리스트 화면
 * 4. 설명 : @!@ 사건 관리 리스트 화면 
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	if(session_usergb == "C01001" || session_usergb == "C01003"){
		$("#regBtn").show();
	}
	var codeInfo = [{cdId:'crimeGrp',selectId:'srcCrimeGrp',type:'4', callbackNm:'fn_ajaxCodeListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	//fn_ajaxCodeList(codeInfo);
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});
});

/**
 * @!@ 그룹죄명 코드 리스트 조회 콜백
 * @param {json} data 
 * @returns 
 */
function fn_ajaxCodeListCallback(data){
	$('#srcCrimeGrp').prepend("<option value='' selected>전체</option");
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_queryMList"});
}
 
/**
 * @!@ 사건 관리 리스트 조회
 * @param {int} page
 * @returns 
 */
function fn_queryMList(page){
	var callUrl = "<c:url value='/incdt/incdt/queryIncdtIncdtMList.do'/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_queryMListCallback', page:$("#page").val(), perPageNum:10});
	
}

/**
 * @!@ 사건 관리 리스트 조회 콜백
 * @param {json} data
 * @returns 
 */
function fn_queryMListCallback(data){
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;
	
	$("#listTab > tbody").empty();
	
	if(listCnt == 0){
		var append = "";
		append += "<tr>";
		
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		
		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
		$.each(list,function(idx,row){
			var append = "";
			append += "<tr>";
	
			append += "<td><a href='javascript:void(0)' onclick='fn_indexIncdtIncdtUDtl(\""+row.incdtSno+"\");return false;'><u>"+row.incdtNo+"</u></a></td>";
			append += "<td>" + gfn_nullRtnSpace(row.incdtNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.crimeNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.regUserNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.aprvrId) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.regrNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.regDt) + "</td>";
	
			append += "</tr>";
	        $("#listTab > tbody").append(append);
	 	});
	}
	
	data.__callFuncName__ ="fn_queryMList";
	data.__naviID__ ="page_navis";
	pageUtil.setPageNavi(data);
	$("#totalCount").text(data.totalCount);
	
}

/**
 * @!@ 사건 관리 등록 화면 이동
 * @param
 * @returns
 */
function fn_indexIncdtIncdtRDtl(){
	requestUtil.setSearchForm("searchForm");
	var param = "?"+$("#searchForm").serialize();
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/incdt/incdt/indexIncdtIncdtRDtl.do"/>'+param);
}

/**
 * @!@ 사건 관리 수정 화면 이동
 * @param {string} incdtSno
 * @returns 
 */
function fn_indexIncdtIncdtUDtl(incdtSno){
	requestUtil.setSearchForm("searchForm");
	$("#searchForm").append('<input type="hidden" class="" id="incdtSno" name="incdtSno" value="'+incdtSno+'"/>');
	var param = "?"+$("#searchForm").serialize();
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/incdt/incdt/indexIncdtIncdtUDtl.do"/>'+param);
}
 

 
function fn_searchDeptFIndListPop() {
//	fn_showUserPage("이미 추가된 장비는 선택할 수 없습니다. \n ggg \n ggg \n ggg \n ggg");
//	return;
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"frame/fsys/user/fsysDeptFIndListPop&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 700,
     width: 1000,
     title: '부서조회 팝업',
     divId : 'fsysDeptFIndListPop'
     //divId : 'eqpMgmtMListPop'
	});

}
 
function fn_deptFindPopCallBack(insttCd, deptNm, teamNm){
	//alert("=======insttCd====>>>"+insttCd+"\n=======deptNm====>>>"+deptNm+"\n=======teamNm====>>>"+teamNm);
	$("#insttCd").val(insttCd);
	$("#insttNm").val(deptNm+" "+teamNm);
}

function fn_searchDeptClear(){
	$("#insttCd").val("");
	$("#insttNm").val("");
}
 
</script>
</head>
<body>
          <div id="con_wrap">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                    <div>
                         <div class="loca">
		                    <div class="ttl">사건 관리</div>
		                    <div class="loca_list">Home > 사건 관리 > 사건 관리</div>
		                  </div>
                          <div class="sub">
                             <!--------------검색------------------>
                             <form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
                             	<input type="hidden" class="" id="page" name="page" value="1"/>
                            <div class="t_head">
                                  <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_queryMList(1);">
                                        <caption>검색</caption>
                                         <colgroup>
                                            <col width="11%">
                                            <col width="22%">
                                            <col width="11%">
                                            <col width="23%">
                                            <col width="11%">
                                            <col width="22%">
                                         </colgroup>
                                   <thead>
                                      <tr>
                                         	<th scope="row" class="hcolor">사건번호</th>
								           <td>
								               <input type="text" id="srcIncdtNo" name="srcIncdtNo" title="사건번호" class="inpw80"/>
								           </td>
								           <th scope="row" class="hcolor">죄명</th>
								           <td>
								               <select id="srcCrimeGrp" name="srcCrimeGrp" title="죄명" onchange="fn_queryMList(1)" class="selw6">
								               </select>
								           </td>
                                         	<th scope="row" class="hcolor">사건명</th>
								           <td>
								               <input type="text" id="srcIncdtNm" name="srcIncdtNm" title="사건명" class="inpw80"/>
								           </td>
								      </tr>
								      <tr>
								           <th scope="row" class="hcolor">담당자</th>
								           <td>
								               <input type="text" id="srcRegUserNm" name="srcRegUserNm" title="담당자" class="inpw80"/>
								           </td>
								           <th scope="row" class="hcolor">사용여부</th>
								           <td>
								               <select id="srcUseYn" name="srcUseYn" title="사용여부" onchange="fn_queryMList(1)" class="selw6">
								                   <option value="">전체</option>
								                   <option value="Y" selected>사용</option>
								                   <option value="N">미사용</option>
								               </select>							           
								           </td>
								           <c:choose>
									           <c:when test="${sessionScope.loginVO.userGb eq 'C01999' }">
										           <th scope="row" class="hcolor">부서명</th>
										           <td>
										           	<input type="text" id="insttNm" name="insttNm"  value="" onClick="fn_searchDeptClear();"  class="inpw50" maxlength="25" readonly/>
								                 	<input type="hidden" id="insttCd" name="insttCd"  value=""  class="" maxlength="25" data-requireNm="소속기관코드" data-maxLength="10" title="소속기관코드" /> &nbsp;
														<a href="#" class="buttonG80S" onclick="fn_searchDeptFIndListPop();return false;">부서검색</a>
										           </td>
									           </c:when>
									           <c:otherwise>
									           		<th scope="row" class="hcolor"></th>
									           		<td></td>
											    </c:otherwise>
										    </c:choose>
                                      </tr>
                                   </thead>
                                </table>
                              </div>
                            <div class="btn_c">
                                  <ul>
                                  	 <li><a href="javascript:void(0)" onclick="fn_indexIncdtIncdtRDtl();return false;" class="RdButton" id="regBtn" style="display: none;">등록</a></li>
                                     <li><a href="javascript:void(0)" onclick="fn_queryMList(1);return false;" class="gyButton">조회</a></li>
                                     <!-- <li><a href="javascript:void(0);" class="myButton">엑셀</a></li> -->
                                  </ul>
                               </div>
                               </form>
                            <!--------------//검색------------------>
                            
                            <!--------------결과------------------>
                             <div class="r_num">| 결과  <strong style="color:#C00" id="totalCount">0</strong>건</div>
                             
                             <!--------------목록---------------------->
                             <div class="t_list">  
                                  <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="15%">
                                              <col width="20%">
                                              <col width="15%">
                                              <col width="10%">
                                              <col width="15%">
                                              <col width="10%">
                                              <col width="15%">
                                          </colgroup>
                                            <thead>
                                              <tr>
                                                 <th scope="col">사건번호</th>
                                                 <th scope="col">사건명</th>
                                                 <th scope="col">죄명</th>
                                                 <th scope="col">담당자</th>
                                                 <th scope="col">주임검사(승인자)</th>
                                                 <th scope="col">작성자</th>
                                                 <th scope="col">작성일</th>
                                              </tr>
                                            </thead>
                                            <tbody>
												<tr><td colspan="7">조회 결과가 없습니다.</td></tr>
                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->
                             
                             <!-----------------------페이징----------------------->
                             <div id="page_navis" class="page_wrap"></div>
                               <!-----------------------//페이징----------------------->
                          
                          </div>
                          
                    </div>
            </div>
                 <!---  //contnets  적용 ------>
  </div>
  
  <div id="fsysCodeRDtlPop"></div>
  <div id="fsysCodeUDtlPop"></div>
  <div id="fsysCodeDtlRDtlPop"></div>
  <div id="fsysCodeDtlUDtlPop"></div>
  <div id="fsysDeptFIndListPop"></div>
</body>
</html>