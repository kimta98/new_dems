<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 매체별 분석 진행 현황 화면
 * 4. 설명 : @!@ 매체별 분석 진행 현황 화면 
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	$("#srcYear").yearselect({
		/* start: 2000,
		end: 2100,
		step:1,
		order: 'asc',
		selected: "current" */
		order: 'DESC'
	});
	
	gfn_init({startFnNm:'fn_queryMDtl', codeSet:'N'});
	
});

/**
 * @!@ 매체별 분석 진행 현황 조회
 * @param
 * @returns 
 */
function fn_queryMDtl(){
	
	var callUrl = "<c:url value='/stts/medanlspgsstate/querySttsMedAnlsPgsStateQList.do'/>";
	
	requestUtil.search({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_queryMDtlCallback'});
	
}

/**
 * @!@ 현장지원 관리 상세 조회 콜백
 * @param
 * @returns 
 */
function fn_queryMDtlCallback(data){
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;
	var thisWeekRcvTotCnt = 0;
	var yearRcvTotCnt = 0;
	var thisWeekPgsTotCnt = 0;
	var thisWeekFinTotCnt = 0;
	var yearFinTotCnt = 0;

	$("#listTab > tbody").empty();
	
	if(listCnt == 0){
		var append = "";
		append += "<tr>";
		
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		
		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
		$.each(list,function(idx,row){
			var append = "";
			
			append += "<tr>";
			
			append += "<td>" + gfn_nullRtnSpace(row.cdNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.thisWeekRcv) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.yearRcv) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.yearPgs) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.thisWeekFin) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.yearFin) + "</td>";
			
			append += "</tr>";
			
	        $("#listTab > tbody").append(append);
	 
			thisWeekRcvTotCnt += row.thisWeekRcv;
			yearRcvTotCnt += row.yearRcv;
			thisWeekPgsTotCnt += row.thisWeekPgs;
			thisWeekFinTotCnt += row.thisWeekFin;
			yearFinTotCnt += row.yearFin;
	 	});
		
		var append = "";
		
		append += "<tr style='font-weight: bold; background-color: beige;'>";
		
		append += "<td>합계</td>";
		append += "<td>" + thisWeekRcvTotCnt + "</td>";
		append += "<td>" + yearRcvTotCnt + "</td>";
		append += "<td>" + thisWeekPgsTotCnt + "</td>";
		append += "<td>" + thisWeekFinTotCnt + "</td>";
		append += "<td>" + yearFinTotCnt + "</td>";
		
		append += "</tr>";
		
        $("#listTab > tbody").append(append);
	}
}

/**
 * @!@ 현장지원 관리 리스트 엑셀 다운로드
 * @param {string} incdtSno
 * @returns 
 */
function fn_getExeclDownload(){
	 fn_tableToExport('#listTab', {
			type: 'excel',
			htmlHyperlink: 'content',
			fileName: '매체별 분석 진행 현황',
			excelstyles: ['background-color', 'font-size', 'border', 'color'],
			mso: {
					styles: ['background-color', 'font-size', 'border', 'color'],
					worksheetName: '매체별 분석 진행 현황',
					pageFormat: 'a4'
					//fileFormat: 'xlsx'
				}
			/* type: 'pdf',
	        jspdf: {orientation: 'p',
	        margins: {right: 20, left: 20, top: 30, bottom: 30},
	        autotable: {styles: {fillColor: 'inherit',
	                               textColor: 'inherit',
	                               fontStyle: 'inherit'},
	                      tableWidth: 'wrap'}} */
	});
}
 
</script>
</head>
<body>
          <div id="con_wrap">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                    <div>
                         <div class="loca">
		                    <div class="ttl">매체별 분석 진행 현황</div>
		                    <div class="loca_list"></div>
		                  </div>
                          <div class="sub">
                             <!--------------검색------------------>
                             <form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
                             <input type="hidden" class="" id="page" name="page" value="1"/>
                            <div class="t_head">
                                  <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_queryMDtl();">
                                        <caption>검색</caption>
                                         <colgroup>
                                            <col width="20%">
                                            <col width="30%">
                                            <col width="20%">
                                            <col width="30%">
                                         </colgroup>
                                   <thead>
								      <tr>
								           <th scope="row" class="hcolor">검색년도</th>
								           <td>
								               <select id="srcYear" name="srcYear" title="검색년도" onchange="fn_queryMDtl()" class="selw6">
								               </select>
								           </td>
								           <th scope="row" class="hcolor"></th>
								           <td>
								           </td>
                                      </tr>
                                   </thead>
                                </table>
                              </div>
                            <div class="btn_c">
                                  <ul>
                                  	 <li><a href="javascript:void(0);" class="myButton" onclick="fn_getExeclDownload();return false;">엑셀</a></li>
                                     <li><a href="javascript:void(0)" onclick="fn_queryMDtl();return false;" class="gyButton">조회</a></li>
                                  </ul>
                               </div>
                               </form>
                            <!--------------//검색------------------>
                            
                            <!--------------결과------------------>
                             <!-- <div class="r_num">| 결과  <strong style="color:#C00" id="totalCount">0</strong>건</div> -->
                             
                             <!--------------목록---------------------->
                             <div class="t_list">  
                                  <table id="listTab" class="iptTblX2" border="1" cellspacing="0" >
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="20%">
                                              <col width="16%">
                                              <col width="16%">
                                              <col width="16%">
                                              <col width="16%">
                                              <col width="16%">
                                          </colgroup>
                                            <thead>
                                              <tr>
                                                 <th scope="col" rowspan="2">구분</th>
                                                 <th scope="col" colspan="2">접수</th>
                                                 <th scope="col">진행중</th>
                                                 <th scope="col" colspan="2">완료</th>
                                              </tr>
                                              <tr>
                                                 <th scope="col">금주</th>
                                                 <th scope="col">누적</th>
                                                 <th scope="col">누적</th>
                                                 <th scope="col">금주</th>
                                                 <th scope="col">누적</th>
                                              </tr>
                                            </thead>
                                            <tbody>
												<tr><td colspan="6">조회 결과가 없습니다.</td></tr>
                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->
                             
                             <!-----------------------페이징----------------------->
                             <!-- <div id="page_navi" class="page_wrap"></div> -->
                               <!-----------------------//페이징----------------------->
                          
                          </div>
                          
                    </div>
            </div>
                 <!---  //contnets  적용 ------>
  </div>
<!-- <script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
ifa.attr('height', $("#contents_info").height()+120);
//$(top.document).find("#container").width($(top.document).find("#container").width() - 5)
</script> -->
</body>
</html>