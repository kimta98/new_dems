<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 매체별 범죄유형별 현황
 * 4. 설명 : 매체별 범죄유형별 현황
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript">
// var flag = true;
// var perPageNum;
var tabId;
var printCnt = 0;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	// var codeInfo2 = [{cdId:'C14',selectId:'schEqpTyp',type:'1', callbackNm:'fn_ajaxEqpTypCallback', sqlQueryId:''}];
	// fn_ajaxCodeList(codeInfo2);
	
	gfn_calendarConfig("schStRcvDtAcp", "schEdRcvDtAcp", "minDate", "");    <%/* 접수일자 from */%>
	gfn_calendarConfig("schEdRcvDtAcp", "schStRcvDtAcp", "maxDate", "");   <%/* 접수일자 to */%>
	
	var today = gfn_getDate();
	$("#schStRcvDtAcp").val(today.substr(0,4)+"-01-01");
	$("#schEdRcvDtAcp").val(today);
	
	fn_searchList();
});


function fn_ajaxEqpTypCallback(data){
// 	$('#schEqpTyp option:eq(0)').before("<option value='' selected>전체</option>");
// 	$("select[name='schEqpTyp']").prepend('<option value="" selected>전체</option>');
// 	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_searchList"});
}


/**
 * @!@ 매체별 디지털증거 보존현황 조회
 * @param {int} page
 * @returns 
 */
function fn_searchList(page){
	 
	if(!fn_valChk()) return; 
	var callUrl = "<c:url value='/stts/medcrmtypestate/querySttsMedCrmTypeStateQList.do'/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_schListCallback', page:page, perPageNum:10});
}

<%/* 필수입력체크 */%>
function fn_valChk(){
	
 	var schStRcvDtAcp = $("#schStRcvDtAcp").val(); //접수일자(from)
 	var schEdRcvDtAcp = $("#schEdRcvDtAcp").val(); //접수일자(to)
 	
 	var table = document.getElementById('listTab');
 	var totalRowCnt = table.rows.length;
 	
 	if(schStRcvDtAcp.length < 1){	
 		fn_showUserPage("접수시작일자를 입력하세요.", function() {
 			$("#schStRcvDtAcp").focus();
 		});
 		return false;  
 	}else if(schEdRcvDtAcp.length < 1){
 		fn_showUserPage("접수종료일자를 입력하세요.", function() {
 			$("#schEdRcvDtAcp").focus();
 		});
 		return false;
 	}
 	
 	
 	
 	return true; 
 	//console.log("=========chkCnt====>>>"+chkCnt);

 }
 
<%/* 필수입력체크 */%>
function fn_excelDownChk(){
	
//  	var table = document.getElementById('listTab');
//  	var totalRowCnt = table.rows.length;
 	
 	if(printCnt < 1){	
 		fn_showUserPage("조회 후 다운로드하세요.", function() {
 			
 		});
 		return false;  
 	}
 	
 	return true; 
 	//console.log("=========chkCnt====>>>"+chkCnt);

 } 
 

 
function fn_schListCallback(data){
	
	var list = data.list;
	var listCnt = list.length;
	printCnt = listCnt;
	
	$("#listTab").empty();
	
	
	
	var arr = new Array();
	var k =0;
	var i =0;
	
	$.each(list,function(idx,row2){
		 var retVal  = new Object();
		 retVal.cntInfo = row2.crimeGrpNm+"|"+row2.mobTotCnt +"|"+row2.diskTotCnt;
		 arr[i] = retVal;
		 ++i;
	});
	
	var objTbl 	= document.getElementById('listTab');
    var totCnt	= listCnt+1;

    var row 	= "";
    var cols 	= "";
    var unitAmt = "";
    var wth = Math.round(93/listCnt);
    
    var append = "";
    
    
    if(listCnt < 1){
    	append += "<colgroup>";
    	append += "<col width='7%'>";
    	append += "<col />";
    	append += "</colgroup>";
    	append += "<thead>";
	    append += "<tr>";
	    append += "<th scope='col'>구분</th>";
	    append += "<th scope='col'>범죄유형</th>";
	    append += "</tr>";
	    append += "</thead>";	
		append += "<tbody>";
	    append += "<tr>";
	    append += "<td>모바일</td>";
	    append += "<td rowspan='2'>"+nullListMsg+"</td>";
	    append += "</tr>";
	    append += "<tr>";
	    append += "<td>디스크</td>";
	    append += "</tr>";
		
		$("#listTab").append(append);
		
		console.log("=======append=======>>>"+append);
	}else{
		append += "<colgroup>";
	    append += "<col width='7%'>";
	    for(var i=0;i<list.length;i++){
	    	append += "<col width='"+wth+"%'>";	
	    }
	    append += "</colgroup>";
	    
	    append += "<thead>";
	    append += "<tr>";
	    append += "<th scope='col'>구분</th>";
	    
	    $.each(list,function(idx,row){
	    	append += "<th scope='col'>"+row.crimeGrpNm+"</th>";
	  	});
	    
	    append += "</tr>";
	    append += "</thead>";	
		append += "<tbody>";
	    append += "<tr>";
	    append += "<td>모바일</td>";
	    append += "</tr>";
	    append += "<tr>";
	    append += "<td>디스크</td>";
	    append += "</tr>";
	    append += "</tbody>";
	    
	    $("#listTab").append(append);
	    
	    var tt;
	    
		for(var i=0;i<arr.length;i++){
			 var retVal = arr[i];
			 tt = retVal.cntInfo.split('|'); 
			 for(var j=0;j<2;j++){
				 var firstRow=objTbl.rows[j+1];
				 var x=firstRow.insertCell(-1);
				 x.innerHTML=tt[j+1];
			 }
		}
	}
    
    
} 
 

/**
* 엑셀 다운로드
* @param fileName 엑셀파일명 (ex. excel.xls)
* @param sheetName 시트명
* @param headers 시트내용(html - table)
*/
function fn_excelDown2(fileName, sheetName, sheetHtml) {
    var html = '';
    html += '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    html += '    <head>';
    html += '        <meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">';
    html += '        <xml>';
    html += '            <x:ExcelWorkbook>';
    html += '                <x:ExcelWorksheets>';
    html += '                    <x:ExcelWorksheet>'
    html += '                        <x:Name>' + sheetName + '</x:Name>';   // 시트이름
    html += '                        <x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions>';
    html += '                    </x:ExcelWorksheet>';
    html += '                </x:ExcelWorksheets>';
    html += '            </x:ExcelWorkbook>';
    html += '        </xml>';
    html += '    </head>';
    html += '    <body>';
    
    // ----------------- 시트 내용 부분 -----------------
    html += sheetHtml;
    // ----------------- //시트 내용 부분 -----------------
    
    html += '    </body>';
    html += '</html>';
    
    // 데이터 타입
    var data_type = 'data:application/vnd.ms-excel';
    var ua = window.navigator.userAgent;
    var blob = new Blob([html], {type: "application/csv;charset=utf-8;"});
    
    if ((ua.indexOf("MSIE ") > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) && window.navigator.msSaveBlob) {
        // ie이고 msSaveBlob 기능을 지원하는 경우
        navigator.msSaveBlob(blob, fileName);
    } else {
        // ie가 아닌 경우 (바로 다운이 되지 않기 때문에 클릭 버튼을 만들어 클릭을 임의로 수행하도록 처리)
        var anchor = window.document.createElement('a');
        anchor.href = window.URL.createObjectURL(blob);
        anchor.download = fileName;
        document.body.appendChild(anchor);
        anchor.click();
        
        // 클릭(다운) 후 요소 제거
        document.body.removeChild(anchor);
    }
}

function fn_excelDown(){
    
	if(!fn_excelDownChk()) return;
	// 대상 테이블을 가져옴
    var table = document.getElementById("listTab");
    
    if(table){
        // CASE 대상 테이블이 존재하는 경우
        
        // 엑셀다운 (엑셀파일명, 시트명, 내부데이터HTML)
        fn_excelDown2("매체별 디지털증거 보존현황.xls", "증거보존현황", table.outerHTML)
    }
}

/**
 * @!@ 현장지원 관리 리스트 엑셀 다운로드
 * @param {string} incdtSno
 * @returns 
 */
function fn_getExeclDownload(){
	 if(!fn_excelDownChk()) return;
	 
	 fn_tableToExport('#listTab', {
			type: 'excel',
			htmlHyperlink: 'content',
			fileName: '매체별 범죄유형별 현황',
			excelstyles: ['background-color', 'font-size', 'border', 'color'],
			mso: {
					styles: ['background-color', 'font-size', 'border', 'color'],
					worksheetName: '매체별 범죄유형별 현황',
					pageFormat: 'a4'
					//fileFormat: 'xlsx'
				}
			/* type: 'pdf',
	        jspdf: {orientation: 'p',
	        margins: {right: 20, left: 20, top: 30, bottom: 30},
	        autotable: {styles: {fillColor: 'inherit',
	                               textColor: 'inherit',
	                               fontStyle: 'inherit'},
	                      tableWidth: 'wrap'}} */
	});
}

</script>
</head>
<body>

<div id="con_wrap">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div>
            <!-- 
                  <div><h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3></div>
             -->
                 <div class="loca">
                  <!--  <h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3>//-->
                    <div class="ttl">매체별 범죄유형별 현황</div>
                    <div class="loca_list">Home > 통계 관리 > 매체별 범죄유형별 현황</div>
                  </div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
<!--                     <form id="searchForm" name="searchForm" method="post"> -->
					 <form id="searchForm" name="searchForm" onsubmit="return false;">	
					<input type="hidden" class="" id="page" name="page" value="1"/>
                    <div class="t_head">
				        <input type="hidden" id="boardKind" class="b_put"  name="boardKind"   value="C23001"/>
				        <input type="hidden" id="userGb"   name="userGb"   value="C00000"/>
				        
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_searchList(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="15%">
                                   <col width="35%">
                                   <col width="15%">
                                   <col />
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">접수일자</th>
					            <td scope="col" >
					               <input id="schStRcvDtAcp" name="schStRcvDtAcp"  title="접수일자(from)"  type="text" value=""  maxlength="10" class="inpw20"/> 
					               <input id="schEdRcvDtAcp" name="schEdRcvDtAcp"  title="접수일자(to)"  type="text" value=""  maxlength="10" class="inpw20"/>
					            </td>
					            <th scope="col" class="hcolor">조회기준</th>
					            <td scope="col" >
					                <select class="selw10" id="schSearchBas" name="schSearchBas" onchange=""  >
					                	<option value="rcv" checked>접수</option>
					                	<option value="end" >완료</option>
					                </select>
					            </td>
					          </tr>
                           </thead>
                        </table>
                      
                      </div>
                      <div  class="btn_c">
					       <ul>
                             <li><a href="javascript:void(0);" class="myButton" onclick="fn_getExeclDownload();return false;">엑셀다운로드</a></li>
                             <li><a href="javascript:void(0);" class='gyButton' onclick="fn_searchList(1); return false;">조회</a></li>
                           </ul>   
					  </div>
                     </form>
                       
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
<!--                      <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div> -->
<!--                      <div class="bo_num"> -->
<!--                          <select id="perPageNum" name="perPageNum"> -->
<!-- 			               <option value="5">5개씩</option> -->
<!-- 			               <option value="10" selected="selected">10개씩</option>		                -->
<!-- 			             </select> -->
<!--                      </div> -->
<!-- 					 <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div> -->
                     
                     <!--------------목록---------------------->
                     <div class="t_list">  
<!--                           <table id="listTab" class="tbl_type" border="1" cellspacing="0" > -->
<!--                                 <caption>공지사항관리</caption> -->
<!--                                   <colgroup> -->
<!--                                     <col width="8%"> --> 
<!--                                       <col width="10%"> -->
<!--                                       <col /> -->
<!--                                       <col width="10%"> -->
<!--                                       <col width="10%"> -->
<!--                                       <col width="10%"> -->
<!--                                       <col width="10%"> -->
<!--                                       <col width="10%"> -->
<!--                                       <col width="10%"> -->
<!--                                       <col width="10%"> -->
<!--                                       </colgroup> -->
<!--                                     <thead> -->
<!--                                       <tr> -->
<!--                                           <th scope="col">순번</th> --> 
<!--                                          <th scope="col">장비번호</th> -->
<!--                                          <th scope="col">장비명</th> -->
<!--                                          <th scope="col">장비유형</th> -->
<!--                                          <th scope="col">S/N</th> -->
<!--                                          <th scope="col">내용연수</th> -->
<!--                                          <th scope="col">RFID TAG</th> -->
<!--                                          <th scope="col">도입일</th> -->
<!--                                          <th scope="col">만료일</th> -->
<!--                                          <th scope="col">유지보수</th> -->
<!--                                       </tr> -->
<!--                                     </thead> -->
<!--                                     <tbody> -->
<!--                                     </tbody> -->
<!--                              </table> -->
<!--                              <br><br><br> -->
                             <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
<!--                     <div id="page_navi" class="page_wrap"></div>                    -->
                       <!-----------------------//페이징----------------------->
                 
                  </div>
                 
            </div>
    </div>
</div>

</body>
</html>