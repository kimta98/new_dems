<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 5. 26.
 * 2. 작성자 : sjw
 * 3. 화면명 : 매체별 처리기간 평균일수
 * 4. 설명 : 매체별 처리기간 평균일수
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp"%>


<script type="text/javaScript" language="javascript" defer="defer">
var tabId;

$(document).ready(function() {
	
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	$("#srcYear").yearselect({
		/* start: 2000,
		end: 2100,
		step:1,
		order: 'asc',
		selected: "current" */
		order: 'DESC'
	});
	
	$('#perPageNum').on('change',function(){
		fn_querySttsAvgQList(1);
	});
	
	gfn_init({startFnNm:'fn_querySttsAvgQList', param:1, codeSet:"N"});

});

function fn_getExeclDownload(state){
	
	if(gfn_nullRtnSpace(state) === 'empty'){
		fn_showUserPage('조회 후 다운로드 하세요.');
		return false;
	} else {
		requestUtil.comExcelXlsx({
			formNm :'searchForm',
			filename : "매체별 처리기간 평균 일수",
			sheetNm : "매체별 처리기간 평균 일수",
			columnArr : "구분,디스크,서버(DB),모바일,데이터,기타",
			columnVarArr : "mediaYear,diskAvg,dbAvg,mobileAvg,dataAvg,elseAvg",
			sqlQueryId : "sttsMedTrtTrmAvgDayDAO.selectMedTrtTrmAvgDayList"
		});
	}
}

/**
 * @로그 리스트 조회
 * @param {int} page
 * @returns 
 */
function fn_querySttsAvgQList(page){
	var callUrl = "<c:url value="/stts/medTrtTrmAvgDay/querySttsMedTrtTrmAvgDayQList.do"/>";
	
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_queryQListCallback', page:page});
	
}

/**
  * @로그 리스트 조회 콜백
  * @param {json} data
  * @returns 
  */
function fn_queryQListCallback(data){

		$("#sttsMedTrtTrmAvgDayList").empty();
		
		if(data.selectMedTrtTrmAvgDayList.length < 1){
			$('#sttsMedTrtTrmAvgDayList').append('<tr><td colspan="6">조회된 결과가 없습니다.</td></tr>');
			$('#pagination-div').twbsPagination('destroy');
			$('.myButton').attr('onclick','fn_getExeclDownload(\'empty\'); return false;');
		}else{
			var append = "";
			$('.myButton').attr('onclick','fn_getExeclDownload(); return false;');
			$.each(data.selectMedTrtTrmAvgDayList, function(index, item){
				append += "<tr>";
				append += "<td>" + gfn_nullRtnSpace(item.mediaYear) + "</td>";
				append += "<td><span id='"+gfn_nullRtnSpace(item.mediaYear) +"diskAvg'>"+gfn_nullRtnSpace(item.diskAvg)+"</span></td>";
				append += "<td><span id='"+gfn_nullRtnSpace(item.mediaYear) +"dbAvg'>"+gfn_nullRtnSpace(item.dbAvg)+"</span></td>";
				append += "<td><span id='"+gfn_nullRtnSpace(item.mediaYear) +"mobileAvg'>"+gfn_nullRtnSpace(item.mobileAvg)+"</span></td>";
				append += "<td><span id='"+gfn_nullRtnSpace(item.mediaYear) +"dataAvg'>"+gfn_nullRtnSpace(item.dataAvg)+"</span></td>";
				append += "<td><span id='"+gfn_nullRtnSpace(item.mediaYear) +"elseAvg'>"+gfn_nullRtnSpace(item.elseAvg)+"</span></td>";
				append += "</tr>";
			});
			$("#sttsMedTrtTrmAvgDayList").append(append);

		}//else
		
		data.__callFuncName__ ="fn_querySttsAvgQList";
		data.__naviID__ ="page_navi";
		pageUtil.setPageNavi(data);
}
</script>
</head>
<body>
	<div id="con_wrap">
		<div id="contents_info">
			<!--- contnets  적용 ------>
			<div>
				<div class="loca">
					<div class="ttl">매체별 처리기간 평균 일수</div>
					<div class="loca_list"></div>
				</div>

				<div class="sub">
					<!--------------검색------------------>
					<form id="searchForm" name="searchForm" onsubmit="return false;">
						<div class="t_head">

							<table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_querySttsAvgQList(1);">
								<caption>검색</caption>
								<colgroup>
									<col width="20%">
									<col width="80%">
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="hcolor">년도</th>
										<th scope="col">
								               <select id="srcYear" name="srcYear" title="검색년도" onchange="fn_querySttsAvgQList(1)" class="selw6">
								               </select>
                    					</th>
									</tr>
								</thead>
							</table>

						</div>
						<div class="btn_c">
 							<ul>
								<li><a href="javascript:void(0);" class='myButton' onclick="fn_getExeclDownload(); return false;">엑셀다운로드</a></li>
								<li><a href="javascript:void(0);" class='gyButton' onclick="fn_querySttsAvgQList(1); return false;">조회</a></li>
							</ul> 
						</div>

						<!--------------//검색------------------>

						<!--------------결과------------------>
						<div class="r_num">
							
						</div>
						<div class="bo_num">
							<select id="perPageNum" name="perPageNum" class="selw6" style="visibility: hidden;">
								<option value="5">5개씩</option>
								<option value="10" selected="selected">10개씩</option>
							</select>
						</div>
				</div>

				<!--------------목록---------------------->
				<div class="t_list">
					<table class="tbl_type" border="1" cellspacing="0">
						<caption>로그 조회</caption>
						<colgroup>
							<col width="16.66%">
							<col width="16.66%">
							<col width="16.66%">
							<col width="16.66%">
							<col width="16.66%">
							<col width="16.66%">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">구분</th>
								<th scope="col">디스크</th>
								<th scope="col">서버(DB)</th>
								<th scope="col">모바일</th>
								<th scope="col">데이터</th>
								<th scope="col">기타</th>
							</tr>
						</thead>
						<tbody id="sttsMedTrtTrmAvgDayList">
						</tbody>
					</table>
				</div>
				<!--------------//목록---------------------->

				<!-----------------------페이징----------------------->
				<div id='page_navi' class="page_wrap"></div>
				<!-----------------------//페이징----------------------->
				</form>
			</div>

		</div>
	</div>
	</div>


	<div id="sttsAvgDtlPop"></div>
</body>
</html>