<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 16.
 * 2. 작성자 : leeji
 * 3. 화면명 : 연구자료관리 수정
 * 4. 설명 : 연구자료관리 수정
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<script type="text/javaScript">
var tabId;
var fileSeq;

$(document).ready(function() {


	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

	var callUrl = "<c:url value = "/bbs/rsch/goBbsRschUDtl.do"/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'modForm',callbackNm:'fn_callBack'});


});

/**
* 연구자료관리 수정 콜백함수
* @param {object} data 조회한 결과데이터
* @returns
*/
function fn_callBack(data){

	fileSeq = data.bbsRschMap.atchFileId;

    <%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
    gfn_overMaxLengthText("title",100);
    gfn_overMaxLength("cnts",4000);

    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileList.length; i++){
		var id = data.atchFileList[i].id;
		var name = data.atchFileList[i].name;
		var path = data.atchFileList[i].path;
		var upload_yn = data.atchFileList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list').append(html);
	}
}

/**
 * 연구자료관리 수정
 * @param form
 * @returns fn_moveList
 */
function fn_modify(){

    gfn_overMaxLength("cnts",4000);

    if( $("#title").val()==''){
    	fn_showUserPage("제목을 입력하세요.");
        $("#title").focus();
        return false;
    }

    if( $("#cnts").val()==''){
    	fn_showUserPage("내용을 입력하세요.");
        $("#cnts").focus();
        return false;
    } else {
        //4000바이트 까지만 저장 가능
        if(gfn_checkByte($("#cnts").val()) > 4000){
        	fn_showUserPage("내용이 너무 길어 저장할 수 없습니다.");
            $("#cnts").focus();
            return false;
        }
    }

    if(confirm("수정하시겠습니까?")){
        var callUrl = "<c:url value = "/bbs/rsch/updBbsRschUDtl.do"/>";
    	requestUtil.save({callUrl:callUrl,srhFormNm:'modForm',callbackNm:'fn_updateCallback'});
    }
}

/**
 * 연구자료관리 삭제
 * @param {String} putupSno 페이지게시판순번
 * @returns fn_moveList
 */
function fn_delete(putupSno) {
    $("#putupSno").val(putupSno);
    if(confirm("삭제하시겠습니까?")){

    	var callUrl = "<c:url value = "/bbs/rsch/delBbsRschUDtl.do"/>";
    	requestUtil.search({callUrl:callUrl,srhFormNm:'modForm',callbackNm:'fn_moveList'});

    }
}

/**
 * 연구자료관리 파일다운로드
 * @param {String} filePath 파일경로
 * @param {String} fileNm   파일이름
 * @param {String} atchFileNm 실제파일이름
 * @returns
 */
function fn_fileDown(filePath,fileNm,atchFileNm) {
    gfn_fileNmDownload(filePath,fileNm,encodeURI(atchFileNm));
}

 function fn_updateCallback(){
		agent.uploadFile({
			obj_id : "file_list",
			target_id : fileSeq,
			type : "rsch",
			creator_id : session_userid
		});
		fn_moveList();
	}

 /**
  * 연구자료관리 목록페이지이동
  * @param
  * @returns
  */
function fn_moveList(){
	var src = "<c:url value = "/bbs/rsch/indexBbsRschMList.do"/>";
	parent.$('#'+tabId+' iframe').attr('src', src);
}
</script>
</head>
<body>
<div id="con_wrap">
        <div class="content">
            <div id="contents_info">
                 <div class="sub_ttl">연구 자료 수정</div><!-----타이틀------>

                  <div class="sub">

      <div class="t_list">
           <form name="modForm" id="modForm" method="post">
                <input id="putupSno" name="putupSno" type="hidden" value="<c:out value="${param.putupSno}" />"/>
                    <table class="iptTblX">
                        <caption></caption>
                        <colgroup>
                            <col width="12%" />
                            <col/>
                            <col width="12%" />
                            <col width="25%" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope="row">제목<span class="fontred">*</span></th>
                                <td colspan="3"><input class="f_form" title="제목 입력" type="text" name="title" id="title" style="width:97%"/>

                                </td>
                            </tr>
                            <tr>
                                <th scope="row">등록일<span class="fontred">*</span></th>
                                    <td><input class="f_form" id="putupDt" name="putupDt" title="등록일 입력" type="text" style="width:20%" readonly="true"/></td>
                                <th scope="row">작성자</th>
                                <td><span id="regrNm"></span></td>
                            </tr>
                            <tr>
                                <th scope="row">내용<span class="fontred">*</span></th>
                                <td colspan="3" id="editerArea" height="300px" >
                                    <textarea name="cnts" id="cnts" class="f_form" title="내용 입력" rows="35" cols="10" style="width:99%; height:280px;"></textarea>
                                    <span class="txt_info" name="cntsByteChk" id="cntsByteChk"></span>
                                </td>
                            </tr>
                            <tr id="divFile">
                                <th scope="row">첨부파일</th>
                                <td scope="row" class="t_left1 bdrn" colspan="3">
                                    <button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list');" >찾아보기</button>
                                    <ul id="file_list" class="file_list" style="margin-left: 18px;"></ul>
                                </td>
                            </tr>
                         </tbody>
                    </table>
                </form>
             </div>
             <div class="btn_c">
               <ul>
                 <li>
                 	<a href="javascript:void(0);" class='RdButton' onclick="fn_modify(); return false;">수정</a>
                 </li>
                 <li id="delBtn">
                 	<a href="javascript:void(0);" class='RdButton' onclick="fn_delete('<c:out value="${param.putupSno}"/>'); return false;">삭제</a>
                 </li>
                 <li>
                 	<a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a>
               	</li>
               </ul>
             </div>
</div>


</body>
</html>