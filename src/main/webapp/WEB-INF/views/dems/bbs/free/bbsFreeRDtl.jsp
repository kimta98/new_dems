<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 16.
 * 2. 작성자 : leeji
 * 3. 화면명 : 자유게시판 등록
 * 4. 설명 : 자유게시판 등록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<script type="text/javaScript">
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

	$("#putupDt").val(gfn_getDate());
    <%/* 달력 세팅 */%>


    <%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
    gfn_overMaxLengthText("title",100);
    gfn_overMaxLength("cnts",4000);
});

/**
 * 자유게시판 등록
 * @param form
 * @returns fn_bbsFreeRDtlCallBack
 */
function fn_addView(){

    gfn_overMaxLength("cnts",4000);

    var putupDt = $("#putupDt").val();
    if( $("#title").val()==''){
    	fn_showUserPage("제목을 입력하세요.");
        $("#title").focus();
        return false;
    }
    if( $("#cnts").val()==''){
    	fn_showUserPage("내용을 입력하세요.");
        $("#cnts").focus();
        return false;
    } else {
        //4000바이트 까지만 저장 가능
        if(gfn_checkByte($("#cnts").val()) > 4000){
        	fn_showUserPage("내용이 너무 길어 저장할 수 없습니다.");
            $("#cnts").focus();
            return false;
        }
    }

    if(confirm("등록하시겠습니까?")){

    	$("#putupDt").val(putupDt);
    	var runningHead = $("#runningHead").val();
        var callUrl = "<c:url value='/bbs/free/regBbsFreeRDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_bbsFreeRDtlCallBack'});

    }

}

/**
 * 자유게시판 목록화면이동
 * @param
 * @returns
 */
function fn_moveList(){
	var src = "<c:url value = "/bbs/free/indexBbsFreeMList.do"/>";
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 자유게시판 등록 콜백함수
* @param
* @returns
*/
function fn_bbsFreeRDtlCallBack(result){
	agent.uploadFile({
		obj_id : "file_list",
		target_id : result.fileSeq,
		type : "free",
		creator_id : session_userid
	});
	fn_moveList();
}

</script>

</head>
<body>
<div id="con_wrap">
        <div class="content">
            <div id="contents_info">
            <div class="sub_ttl">자유 게시판 등록</div><!-----타이틀------>

          	<div class="sub">

	    		<div class="t_list">
	            <form name="insForm" id="insForm" method="post">
	                    <table class="iptTblX">
	                        <caption>자유게시판등록</caption>
	                        <colgroup>
	                            <col width="12%" />
	                            <col/>
	                            <col width="12%" />
	                            <col width="25%" />
	                        </colgroup>
	                        <tbody>
	                            <tr>
	                                <th scope="row">제목<span class="fontred">*</span></th>
	                                <td><input class="ipt_form" title="제목 입력" type="text" name="title" id="title" style="width:97%" /></td>
	                                <th scope="row">머리말</th>
	                                <td>
	                                	<select name="runningHead" id="runningHead" style="width:100px">
	                                		<option value="freeWr">자유글</option>
	                                		<option value="suggest">건의</option>
	                                		<option value="quest">질의</option>
	                                		<option value="refer">참고자료</option>
	                                		<option value="frPaper">포렌식 논문</option>
	                                		<option value="workMn">업무 메뉴얼</option>
	                                	</select>
	                                </td>
	                            </tr>
	                            <tr>
	                                <th scope="row">등록일<span class="fontred">*</span></th>
	                                <td><input id="putupDt" name="putupDt" title="등록일 입력" type="text" style="width: 100px; border : white;" class="ipt_form" disabled="true"/></td>
	                                <th scope="row">작성자</th>
	                                <td><c:out value="${regNm}"/></td>
	                            </tr>
	                            <tr>
	                                <th scope="row">내용<span class="fontred">*</span></th>
	                                <td id="editerArea" height="300px" colspan="3">
	                                    <textarea name="cnts" id="cnts" title="내용 입력" data-maxlen="100" rows="35" cols="10"  style="width:99%; height:280px;" ></textarea>
	                                    <span class="txt_info" name="cntsByteChk" id="cntsByteChk"></span>
	                                </td>
	                            </tr>
	                            <tr id="divFile">
	                                <th scope="row">첨부파일</th>
	                                <td colspan="3">
	                                    <button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list');" >찾아보기</button>
	                                    <ul id="file_list" class="file_list" style="margin-left: 18px;"></ul>
	                                </td>
	                            </tr>
	                        </tbody>
	                     </table>
	                 </form>
	              </div>
	              <div class="btn_c">
	                <ul>
                        <li>
                        	<a href="javascript:void(0);" class='RdButton' onclick="fn_addView(); return false;">등록</a>
                        </li>
                        <li>
                        	<a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a>
                       	</li>
                      </ul>
	              </div>

           	</div>
           </div>
     </div>
</div>


</body>
</html>