<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 16.
 * 2. 작성자 : Leeji
 * 3. 화면명 : 자유게시판 목록
 * 4. 설명 : 자유게시판 목록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript">
var tabId;

$(document).ready(function() {

	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');	

 	/* var page = '${param.page}';
	var searchType = '${param.searchType}';
	var searchValue = '${param.searchValue}';
	var mode = '${param.mode}';
	
	if(mode == ''){
		page = 1;
	}else{
		page = (mode == 'del' ? 1 : page);		
		$('#searchType').val(searchType).prop('selected',true);
		$('#searchValue').val(searchValue);	
	} */
	
	gfn_init({startFnNm:'', param:{targetFormId:"searchForm", callbackNm:"fn_bbsFreeSearch"}, codeSet:"N"});
	
	//requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_bbsFreeSearch"}); 
	//fn_bbsFreeSearch(page);

});

/**
 * 자유게시판목록 조회
 * @param {string} page 항목에 대한 고유 식별자 
 * @returns fn_callBack
 */
function fn_bbsFreeSearch(page){
	var callUrl = "<c:url value = "/bbs/free/queryBbsFreeMList.do"/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callBack',page:$("#page").val(),perPageNum:10});
}

 /**
  * 자유게시판목록 조회 콜백함수
  * @param {object} data 조회한 결과데이터
  * @returns
  */
function fn_callBack(data){

	$("#bbsFreeList").empty();
	$("#totalcnt").text(data.totalCount);
	
	if(data.bbsFreeList.length < 1){
		$('#bbsFreeList').append('<tr><td colspan="7">조회된 결과가 없습니다.</td></tr>');
	}else{
		$.each(data.bbsFreeList, function(index, item){
			var runHead = item.runningHead;

			if (item.runningHead == "freeWr") {
				runHead = "자유글";
			} else if(item.runningHead == "suggest") {
				runHead = "건의";
			} else if(item.runningHead == "quest") {
				runHead = "질의";
			} else if(item.runningHead == "refer") {
				runHead = "참고자료";
			} else if(item.runningHead == "frPaper") {
				runHead = "포렌식 논문";
			} else if(item.runningHead == "workMn") {
				runHead = "업무 메뉴얼";
			} else {
				runHead = "";
			}
			$('#bbsFreeList').append("<tr><td>"+(data.page == 1 ? index+1 : ((data.page-1)*data.perPageNum)+index+1)
			+"<td>"+ runHead +"</td>"		
			+"<td style='max-width: 200px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis'>"
			+"<a href='#' onclick=javascript:fn_detail("+item.putupSno+","+data.page+")><u><strong><font color=green>"+item.title+"["+item.commentCount+"]"+"</u></a></td><td>"
			+item.fileCount+"</td><td>"+item.putupDt+"</td><td>"+item.regrNm+"</td><td>"+item.selectNum+"</td></tr>");
		 });
	}
	
	data.__callFuncName__ ="fn_bbsFreeSearch";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
}

/**
 * 자유게시판 등록화면이동
 * @param
 * @returns
 */
function fn_insBbsFree(){
	requestUtil.setSearchForm("searchForm");
	var src = "<c:url value = "/bbs/free/indexBbsFreeRDtl.do"/>";	
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 자유게시판 상세화면이동
* @param
* @returns
*/
function fn_detail(putupSno,page){
	requestUtil.setSearchForm("searchForm");
	$('#putupSno').val(putupSno);
	var data = JSON.stringify($('#searchForm').serializeArray());
	var searchParam = {data : data};
	var src = "<c:url value = "/bbs/free/indexBbsFreeUDtl.do"/>?page="+page+"&putupSno="+putupSno+"&"+$.param(searchParam);
	parent.$('#'+tabId+' iframe').attr('src', src);	
}
</script>
<body>
<div id="con_wrap">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div>
                  <div class="loca">
                    <div class="ttl">자유 게시판</div>
                    <div class="loca_list"></div>
                  </div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
                    <form id="searchForm" name="searchForm" onsubmit="return false;">
                    	<input type="hidden" class="" id="page" name="page" value="1"/>
                    <div class="t_head">
				        <input type="hidden" id="boardKind" class="b_put"  name="boardKind" value="C23009"/>
				        <input type="hidden" id="putupSno" class="b_put" name="putupSno" value=""/>
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_bbsFreeSearch(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="15%">
                                   <col width="80%">                                 
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">
						            <select id="searchType" name="searchType" style=width:80px;vertical-align:center;>
						                <option value="all" selected="selected">전체</option>
						                <option value="searchTitle">제목</option>
						                <option value="searchCnts">내용</option>
						                <option value="searchRegerNm">작성자</option>
						            </select>
					            </th>
					            <td scope="col" colspan="3">
					               <input class="b_put" type="text" name="searchValue" id="searchValue" style="width:80%;"/>
					            </td>
					          </tr> 
                           </thead>
                        </table>
                        <div  class="btn_c">
				       	  <ul>
                            <li><a href="javascript:void(0);" class='RdButton' onclick="fn_insBbsFree(); return false;">등록</a></li>
                            <li><a href="javascript:void(0);" class='myButton' onclick="fn_bbsFreeSearch(1); return false;">조회</a></li>
                          </ul>   
					    </div>
                      </div>
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
                     <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>
                     
                     <div class="bo_num" style = "width : 100px">
                         <select id="runningHead" name="runningHead" title ="머리말" class="inpw80" style = "width : 100%">
	       					 <option value="" selected="selected">전체</option>	
				             <option value="freeWr">자유글</option>  
				             <option value="suggest">건의</option>
				             <option value="quest">질의</option>
				             <option value="refer">참고자료</option>
				             <option value="frPaper">포렌식 논문</option>     
				             <option value="workMn">업무 메뉴얼</option>
			             </select>
                     </div>
                      
                     </div>
                     
                     <!--------------목록---------------------->
                    <div class="t_list">  
                          <table class="tbl_type" border="1" cellspacing="0" >
                                <caption>자유게시판</caption>
                                  <colgroup>
                                      <col width="3%">
                                      <col width="8%">                                      
                                      <col width="37%">
                                      <col width="13%">
                                      <col width="13%">
                                      <col width="13%">
                                      <col width="13%">
                                      </colgroup>
                                    <thead>
                                      <tr>
                                         <th scope="col"></th>
                                         <th scope="col">머리말</th>
                                         <th scope="col">제목</th>
                                         <th scope="col">첨부파일</th>
                                         <th scope="col">등록일</th>
                                         <th scope="col">작성자</th>
                                         <th scope="col">조회수</th>
                                      </tr>
                                    </thead>
                                    <tbody id="bbsFreeList">
                                    </tbody>
                             </table>
                    </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                     <div id='page_navi' class="page_wrap"></div>                            
                       <!-----------------------//페이징----------------------->
                  </form>
                  </div>
                 
            </div>
    </div>
</body>
</html>