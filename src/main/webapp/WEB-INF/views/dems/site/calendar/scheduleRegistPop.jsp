<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>디지털 증거 관리 시스템</title>

<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {

	gfn_calendarConfig("scheduleYmd", "", "", "");
	var scheduleYmd = "${paramVO.scheduleYmd}"
	if(scheduleYmd == ''){
		scheduleYmd = gfn_dashDate(gfn_getCurDate(), "-")	;
	}
	$('#scheduleYmd').val(scheduleYmd);

});

function scheduleRegist(){

	var form = document.scheduleRegistForm;

	if(form.scheduleYmd.value == null || form.scheduleYmd.value == ''){
		alert("날짜를 입력하세요.");
		return;
	}

	if(form.scheduleSubject.value == null || form.scheduleSubject.value == ''){
		alert("제목을 입력하세요.");
		return;
	}

	if(form.scheduleContent.value == null || form.scheduleContent.value == ''){
		alert("내용을 입력하세요.");
		return;
	}

	if(confirm("일정을 등록하시겠습니까?")){
		$.ajax({
			type:'POST',
			data : $("form[name=scheduleRegistForm]").serialize(),
			datatype:'json',
			url : "/schedule/scheduleRegistProc.do",
			success : function(data){
				moveCalendar();
				fn_dialogClose("scheduleRegistPop");
			},
			error : function(xhr, status, error){
			}
		});

	}
}

</script>

</head>
<body>
	<div id="con_wrap_pop">
		<div class="contents">
			<div id="contents_info">
				<!--- contnets  적용 ------>
				<div class="window_popup">
					<div class="sub_ttl">일정 등록</div>
					<div class="sub">
						<form name="scheduleRegistForm" id="scheduleRegistForm" method="post">
							<input type="hidden" name="scheduleType" id="scheduleType" value=""/>
							<input type="hidden" name="scheduleUrl" id="scheduleUrl" value=""/>
							<input type="hidden" name="insttCd" id="insttCd" value=""/>
							<div class="t_list">
								<table class="iptTblX">
									<caption>수정</caption>
									<colgroup>
										<col width="20%" />
										<col width="*" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">날짜</th>
											<td><input class="inpw20" title="요청일시" type="text"  name="scheduleYmd" id="scheduleYmd"/></td>
										</tr>
										<tr>
											<th scope="row">권한</th>
											<td>
												<select class="accessCd" id="accessCd" name="accessCd" data-maxLength="6"style=" width:80px"  title="권한" >
													<option value="All" >전체</option>
													<option value="Team" >부서</option>
													<option value="My" >나만</option>
												</select>
											</td>
										</tr>
										<tr>
											<th scope="row">제목</th>
											<td><input id="scheduleSubject" name="scheduleSubject" title="제목" type="text" class="inpw80" data-maxLength="100" /></td>
										</tr>
										<tr>
											<th scope="row">상세내용<span class="fontred">*</span></th>
											<td><textarea name="scheduleContent" id="scheduleContent" title="내용 입력" data-maxlen="100" rows="35" cols="10" style="width:99%; height:280px;"></textarea></td>
										</tr>
									</tbody>
								</table>
							</div>
						</form>
						<div class="btn_c">
							<ul>
								<li>
									<a href="javascript:void(0);" class="RdButton" onclick="scheduleRegist();return false;">등록</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>