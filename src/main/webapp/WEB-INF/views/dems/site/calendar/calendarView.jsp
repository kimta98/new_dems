<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<style type="text/css">
.calWrap {position: relative;}
.calHead {overflow: hidden;margin-bottom: 15px;padding: 15px;border: 1px solid #ddd;background-color: #fbfbfb;}
.calMonth { text-align: center;}
.group {float: right;}
.clb {clear: both !important;}
.calendar {table-layout: fixed;width: 100%;border-collapse: separate;border-spacing: 0px;border-right: 1px solid #e2e2e2;empty-cells: show;}

.calendar thead th:first-child {
    border-left: 1px solid #e2e2e2;
}
.calendar thead th {width: 14.28571428571429%;font-size: 1.071em;line-height: 40px;color: #000;border-top: 1px solid #555;border-left: 1px solid #e2e2e2;border-bottom: 1px solid #555;}

@media screen and (min-width: 1000px)
.calendar td {padding: 10px;}
.calendar td {width: 14%;height: 110px;padding: 5px;vertical-align: top;border-bottom: 1px solid #e2e2e2;border-left: 1px solid #e2e2e2;empty-cells: show;}


.calendar td .day:hover, .calendar td .day:focus {text-decoration: underline;}
@media screen and (min-width: 1000px)
.calendar td .day {margin-bottom: 12px;}
.calendar td .day {font-size: 1.1rem !important;color: inherit;cursor: pointer;font-size: inherit;word-break: normal;border-collapse: separate;border-spacing: 0px;text-decoration: none;display: inline-block;margin-bottom: 5px;padding: 3px;line-height: 1;vertical-align: top;}
.calendar td .schedule {margin-bottom:3px;padding-left:5px;background-color: #666;color: #fff !important;font-size: 0.8rem !important;color: inherit;cursor: pointer;font-size: inherit;word-break: normal;border-collapse: separate;border-spacing: 0px;text-decoration: none;}
.calendar td .schedule.All{}
.calendar td .schedule.Team{background-color:green !important;}
.calendar td .schedule.My{background-color:red !important;}

.calendar td .schedule em{
position: relative;
    background-size: 100% 100% !important;
    width: 16px;
    height: 17px;
    margin-top: 1px !important;
    line-height: 16px;
    float: left;
    padding: 0px;
    display: inline-block;
    font-size: 0px;
    margin: 0;
    border: none;
    margin-right: 9px;
    vertical-align: middle;
background: #00000000 url('/css/dems/images/common/private.png') center center no-repeat;}
.calendar td.today .today {font-style: normal;empty-cells: show;word-break: normal;display: inline-block;margin-left: 10px;padding: 0 5px;font-size: 0.875em;line-height: 1.2;color: #fff;vertical-align: top;border-radius: 10px;background-color: #555;font-style: normal;}

.calWrap .calMonth a span{empty-cells: show;word-break: normal;display: inline-block;padding: 0 5px; font-size: 1.275em;line-height: 2.2;color: #fff;vertical-align: top;border-radius: 10px; background-color: #555;font-style: normal;}
.calWrap .calMonth .month{font-size: 1.5rem;margin-left: 13px;margin-right: 13px;}

.calWrap .calMonth .group .on.all span{background-color: black;}
.calWrap .calMonth .group .on.team span{background-color: green;}
.calWrap .calMonth .group .on.my span{background-color: red;}

</style>
</head>

<script type="text/javascript">
	function fnSearchMonth(searchMonth , searchGroup) {
		var form = document.searchForm;
		form.searchMonth.value= searchMonth;
		form.searchGroup.value= searchGroup;
		form.action = "/calendar/calendarView.do";
		form.submit();
	}


	function fn_scheduleRegistPop(scheduleYmd) {
		requestUtil.mdPop({
			popUrl : "/schedule/scheduleRegistPop.do?scheduleYmd=" + scheduleYmd + "&searchGroup=${paramVO.searchGroup}",
			height : 700,
			width : 1000,
			title : '일정등록 팝업',
			divId : 'scheduleRegistPop'
		});
	}

	function fn_scheduleUpdatePop(scheduleIdx){
		requestUtil.mdPop({
			popUrl : "/schedule/scheduleUpdatePop.do?scheduleIdx=" + scheduleIdx,
			height : 700,
			width : 1000,
			title : '일정수정 팝업',
			divId : 'scheduleUpdatePop'
		});
	}

	function moveCalendar(){
		fnSearchMonth('${currentMonth}','${paramVO.searchGroup}');
	}

</script>

<form name="searchForm" id="searchForm" method="get">
	<input type="hidden" name="searchMonth">
	<input type="hidden" name="searchYmd">
	<input type="hidden" name="searchGroup">
</form>

<div class="calWrap">
	<div class="calHead clearfix">
		<p class="calMonth">
			<a href="#prev" onclick="fnSearchMonth('${prevMonth}','${paramVO.searchGroup}');" class="prev"><span class="blind">이전 달</span></a>
			<c:set var="currentMonthArray" value="${fn:split(currentMonth,'-')}"/>
			<span class="month">${currentMonthArray[0]}. ${currentMonthArray[1]}</span>
			<a href="#next" onclick="fnSearchMonth('${nextMonth}','${paramVO.searchGroup}');" class="next"><span class="blind">다음 달</span></a>
			<span class="group">
				<a href="#none" onclick="fnSearchMonth('${currentMonth}','All');" class="all ${paramVO.searchGroup eq 'All' ? 'on' : ''}"><span class="blind">전체</span></a>
				<a href="#none" onclick="fnSearchMonth('${currentMonth}','Team');" class="team ${paramVO.searchGroup eq 'Team' ? 'on' : ''}"><span class="blind">부서</span></a>
				<a href="#none" onclick="fnSearchMonth('${currentMonth}','My');" class="my ${paramVO.searchGroup eq 'My' ? 'on' : ''}"><span class="blind">나의</span></a>
			</span>
		</p>
	</div>
	<div class="boardWrap clb">
		<table id="caltext" class="calendar">
			<caption></caption>
			<thead>
				<tr>
					<th scope="col" class="sun">일</th>
					<th scope="col">월</th>
					<th scope="col">화</th>
					<th scope="col">수</th>
					<th scope="col">목</th>
					<th scope="col">금</th>
					<th scope="col" class="sat">토</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<c:set var="colCnt" value="0" />
					<%--앞 공백 --%>
					<c:forEach  var="i" begin="1" end="${weekOfMonth%7}" step="1">
						<td>&nbsp;</td>
						<c:set var="colCnt" value="${colCnt+1}" />
					</c:forEach>
					<%--실제 날짜 --%>
					<c:forEach var="day" begin="1" end="${lastDay}" step="1">
						<c:set var="fullDate" value="${currentMonth}-${day < 10 ? '0':''}${day}"/>
						<c:set var="dayOfWeek" value="${colCnt % 7}" />
						<c:choose>
							<c:when test="${dayOfWeek == 0}">
								<c:set var="tdClass" value="sun" />
							</c:when>
							<c:when test="${dayOfWeek == 6}">
								<c:set var="tdClass" value="sat" />
							</c:when>
							<c:otherwise><c:set var="tdClass" value="" /></c:otherwise>
						</c:choose>
						<c:if test="${fullDate eq today}">
							<c:set var="tdClass" value="${tdClass} today" />
						</c:if>
						<c:if test="${fullDate eq paramVO.searchYmd}">
							<c:set var="tdClass" value="${tdClass} choice themeBG" />
						</c:if>

						<td class="${tdClass}">
							<a href="#days" class="day" onclick="javascript:fn_scheduleRegistPop('${fullDate}'); return false;">
								${day}
								<c:if test="${fullDate eq today}"><em class="today">TODAY</em></c:if>
							</a>
							<c:choose>
								<c:when test="${not empty scheduleMap[fullDate]}">
									<c:forEach var="schedule" items="${scheduleMap[fullDate]}" varStatus="status">
										<p class="schedule emp1 ${schedule.accessCd}" onclick="javascript:fn_scheduleUpdatePop('${schedule.scheduleIdx}'); return false;">
											<!--<em></em>-->
											<a><c:out value="${schedule.scheduleSubject}"/></a>
										</p>
									</c:forEach>
								</c:when>
							</c:choose>
						</td>
						<c:if test="${dayOfWeek == 6 && day ne lastDay}">
							<c:out value="</tr>" escapeXml="false"/>
							<c:out value="<tr>" escapeXml="false"/>
						</c:if>
						<c:set var="colCnt" value="${colCnt+1}" />
					</c:forEach>
					<%--뒷 공백 --%>
					<c:if test="${colCnt % 7 > 0}">
						<c:forEach  var="i" begin="1" end="${7 - colCnt % 7}" step="1">
							<td><span class="day">&nbsp;</span></td>
						</c:forEach>
					</c:if>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div id="scheduleRegistPop"></div>
<div id="scheduleUpdatePop"></div>