<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>디지털 증거 관리 시스템</title>

<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {

	gfn_calendarConfig("scheduleYmd", "", "", "");
	var scheduleYmd = "${scheduleVO.scheduleYmd}"
	if(scheduleYmd == ''){
		scheduleYmd = gfn_dashDate(gfn_getCurDate(), "-")	;
	}
	$('#scheduleYmd').val(scheduleYmd);

});
function scheduleDelete(){
	if(confirm("일정을 삭제하시겠습니까?")){
		$.ajax({
			type:'POST',
			data : $("form[name=scheduleUpdateForm]").serialize(),
			datatype:'json',
			url : "/schedule/scheduleDeleteProc.do",
			success : function(data){
				moveCalendar();
				fn_dialogClose("scheduleUpdatePop");
			},
			error : function(xhr, status, error){
			}
		});

	}
}

function scheduleUpdate(){

	var form = document.scheduleUpdateForm;

	if(form.scheduleYmd.value == null || form.scheduleYmd.value == ''){
		alert("날짜를 입력하세요.");
		return;
	}

	if(form.scheduleSubject.value == null || form.scheduleSubject.value == ''){
		alert("제목을 입력하세요.");
		return;
	}

	if(form.scheduleContent.value == null || form.scheduleContent.value == ''){
		alert("내용을 입력하세요.");
		return;
	}

	if(confirm("일정을 수정하시겠습니까?")){
		$.ajax({
			type:'POST',
			data : $("form[name=scheduleUpdateForm]").serialize(),
			datatype:'json',
			url : "/schedule/scheduleUpdateProc.do",
			success : function(data){
				moveCalendar();
				fn_dialogClose("scheduleUpdatePop");
			},
			error : function(xhr, status, error){
			}
		});

	}
}

</script>

</head>
<body>
	<div id="con_wrap_pop">
		<div class="contents">
			<div id="contents_info">
				<!--- contnets  적용 ------>
				<div class="window_popup">
					<div class="sub_ttl">일정 등록</div>
					<div class="sub">
						<form name="scheduleUpdateForm" id="scheduleUpdateForm" method="post">
							<input type="hidden" name="scheduleIdx" id="scheduleIdx" value="${scheduleVO.scheduleIdx}"/>
							<input type="hidden" name="scheduleType" id="scheduleType" value="${scheduleVO.scheduleType}"/>
							<input type="hidden" name="scheduleUrl" id="scheduleUrl" value="${scheduleVO.scheduleUrl}"/>
							<input type="hidden" name="insttCd" id="insttCd" value="${scheduleVO.insttCd}"/>
							<div class="t_list">
								<table class="iptTblX">
									<caption>수정</caption>
									<colgroup>
										<col width="20%" />
										<col width="*" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">날짜</th>
											<td><input class="inpw20" title="요청일시" type="text" name="scheduleYmd" id="scheduleYmd"/></td>
										</tr>
										<tr>
											<th scope="row">권한${scheduleVO.accessCd}</th>
											<td>
												<select class="accessCd" id="accessCd" name="accessCd" data-maxLength="6"style=" width:80px"  title="권한" >
													<option value="All" ${scheduleVO.accessCd eq 'All' ? 'selected="selected"' : ''}>전체</option>
													<option value="Team" ${scheduleVO.accessCd eq 'Team' ? 'selected="selected"' : ''}>부서</option>
													<option value="My" ${scheduleVO.accessCd eq 'My' ? 'selected="selected"' : ''}>나만</option>
												</select>
											</td>
										</tr>
										<tr>
											<th scope="row">제목</th>
											<td><input id="scheduleSubject" name="scheduleSubject" value="${scheduleVO.scheduleSubject}" title="제목" type="text" class="inpw80" data-maxLength="100" /></td>
										</tr>
										<tr>
											<th scope="row">상세내용<span class="fontred">*</span></th>
											<td><textarea name="scheduleContent" id="scheduleContent" value="" title="내용 입력" data-maxlen="100" rows="35" cols="10" style="width:99%; height:280px;">${scheduleVO.scheduleContent}</textarea></td>
										</tr>
									</tbody>
								</table>
							</div>
						</form>
						<div class="btn_c">
							<ul>
								<li>
									<a href="javascript:void(0);" class="RdButton" onclick="scheduleDelete();return false;">삭제</a>
								</li>
								<li>
									<a href="javascript:void(0);" class="RdButton" onclick="scheduleUpdate();return false;">수정</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>