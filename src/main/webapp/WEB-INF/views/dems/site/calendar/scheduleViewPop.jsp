<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>디지털 증거 관리 시스템</title>

<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {

});

function scheduleUpdate(){

	var form = document.scheduleUpdateForm;

	if(form.scheduleYmd.value == null || form.scheduleYmd.value == ''){
		alert("날짜를 입력하세요.");
		return;
	}

	if(form.scheduleSubject.value == null || form.scheduleSubject.value == ''){
		alert("제목을 입력하세요.");
		return;
	}

	if(form.scheduleContent.value == null || form.scheduleContent.value == ''){
		alert("내용을 입력하세요.");
		return;
	}

	if(confirm("일정을 수정하시겠습니까?")){
		$.ajax({
			type:'POST',
			data : $("form[name=scheduleUpdateForm]").serialize(),
			datatype:'json',
			url : "/schedule/scheduleUpdateProc.do",
			success : function(data){
				moveCalendar();
				fn_dialogClose("scheduleUpdatePop");
			},
			error : function(xhr, status, error){
			}
		});

	}
}

</script>

</head>
<body>
	<div id="con_wrap_pop">
		<div class="contents">
			<div id="contents_info">
				<!--- contnets  적용 ------>
				<div class="window_popup">
					<div class="sub_ttl">일정 등록</div>
					<div class="sub">
						<form name="scheduleUpdateForm" id="scheduleUpdateForm" method="post">
							<input type="hidden" name="scheduleIdx" id="scheduleIdx" value="${scheduleVO.scheduleIdx}"/>
							<input type="hidden" name="scheduleType" id="scheduleType" value="${scheduleVO.scheduleType}"/>
							<input type="hidden" name="scheduleUrl" id="scheduleUrl" value="${scheduleVO.scheduleUrl}"/>
							<input type="hidden" name="insttCd" id="insttCd" value="${scheduleVO.insttCd}"/>
							<div class="t_list">
								<table class="iptTblX">
									<caption>수정</caption>
									<colgroup>
										<col width="20%" />
										<col width="*" />
									</colgroup>
									<tbody>
										<tr>
											<th scope="row">작성자</th>
											<td>${scheduleVO.regNm}</td>
										</tr>
										<tr>
											<th scope="row">날짜</th>
											<td>${scheduleVO.scheduleYmd}</td>
										</tr>
										<tr>
											<th scope="row">제목</th>
											<td>${scheduleVO.scheduleSubject}</td>
										</tr>
										<tr>
											<th scope="row">상세내용<span class="fontred">*</span></th>
											<td>${scheduleVO.scheduleContent}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>