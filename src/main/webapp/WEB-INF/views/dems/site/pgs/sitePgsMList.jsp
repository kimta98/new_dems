<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 현장지원 관리 리스트 화면
 * 4. 설명 : @!@ 현장지원 관리 리스트 화면
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var tabId;
var req = "";
var cncl = "";
var reqStr = "";
var cnclStr = "";
switch (session_usergb) {
	case 'C01001':
		req = "C03002";
		cncl = "C03001";
		reqStr = "검사승인요청";
		cnclStr = "검사승인요청취소";
	    break;
	case 'C01002':
		req = "C03003";
		cncl = "C03002";
		reqStr = "검사승인";
		cnclStr = "검사승인취소";
	    break;
	case 'C01003':
		req = "C03005";
		cncl = "C03003";
		reqStr = "접수";
		cnclStr = "접수취소";
	    break;
	case 'C01004':
		req = "C03006";
		cncl = "C03005";
		reqStr = "과장승인";
		cnclStr = "과장승인취소";
	    break;
	default:
		break;
}

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

	gfn_calendarConfig("srcRegDtFrom", "srcRegDtTo", "minDate", "");
    gfn_calendarConfig("srcRegDtTo", "srcRegDtFrom", "maxDate", "");
    $('#srcRegDtFrom').val(gfn_dashDate(gfn_addMonth(gfn_getCurDate(), -1), "-"));
    $('#srcRegDtTo').val(gfn_dashDate(gfn_getCurDate(), "-"));

    gfn_calendarConfig("srcCfscSechPlndDtFrom", "srcCfscSechPlndDtTo", "minDate", "");
    gfn_calendarConfig("srcCfscSechPlndDtTo", "srcCfscSechPlndDtFrom", "maxDate", "");
    /* $('#srcCfscSechPlndDtFrom').val(gfn_dashDate(gfn_getCurDate(), "-"));
    $('#srcCfscSechPlndDtTo').val(gfn_dashDate(gfn_addMonth(gfn_getCurDate(), +1), "-")); */

    $("#reqBtn > a").text(reqStr);
    $("#reqCnclBtn > a").text(cnclStr);

    $("#chkAll").click(function(){ //만약 전체 선택 체크박스가 체크된상태일경우
		if($("#chkAll").prop("checked")) { //해당화면에 전체 checkbox들을 체크해준다
			$("input[name=chk]").prop("checked",true); // 전체선택 체크박스가 해제된 경우
		} else { //해당화면에 모든 checkbox들의 체크를해제시킨다.
			$("input[name=chk]").prop("checked",false);
		}
	})

	/* var codeInfo = [{cdId:'C03',selectId:'srcPgsStat',type:'1', callbackNm:'fn_ajaxCodeListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}]; */
	var codeInfo = [{cdId:'C03',cd:comPgsStat,selectId:'srcPgsStat',type:'2', callbackNm:'fn_ajaxCodeCrimeGrpList', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	//fn_ajaxCodeList(codeInfo);
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});

	if(session_usergb == "C01002" || session_usergb == "C01003" || session_usergb == "C01004"){
		$("#reqBtn").show();
		$("#reqCnclBtn").show();
	}

});

function fn_ajaxCodeCrimeGrpList(){
	var codeInfo = [{cdId:'crimeGrp',selectId:'srcCrimeGrp',type:'4', callbackNm:'fn_ajaxCodeCrimeGrpListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_queryMList"});
}

function fn_ajaxCodeCrimeGrpListCallback() {
	$('#srcCrimeGrp').prepend("<option value='' selected>전체</option");
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_ajaxCodeListCallback"});
}

/**
 * @!@ 그룹죄명 코드 리스트 조회 콜백
 * @param {json} data
 * @returns
 */
function fn_ajaxCodeListCallback(data){
	 $('#srcPgsStat').prepend("<option value='"+comPgsStat+"' selected>전체</option");
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_queryMList"});
}

/**
 * @!@ 현장지원 관리 리스트 조회
 * @param {int} page
 * @returns
 */
function fn_queryMList(page){
	var callUrl = "<c:url value='/site/rcv/querySiteRcvMList.do'/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_queryMListCallback', page:$("#page").val(), perPageNum:10});

}

/**
 * @!@ 현장지원 관리 리스트 조회 콜백
 * @param {json} data
 * @returns
 */
function fn_queryMListCallback(data){
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;

	$("#listTab > tbody").empty();

	if(listCnt == 0){
		var append = "";
		append += "<tr>";

		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";

		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
		$.each(list,function(idx,row){
			var append = "";
			append += "<tr>";
			if(row.pgsStat == req || row.pgsStat == cncl){
				append += "<td><input type='checkbox' name='chk' id='' title='' value='"+ row.suppReqSno + "," + row.pgsStat +"' class='check_agree1'/></td>";
			}else{
				append += "<td></td>";
			}
			append += "<td>" + gfn_nullRtnSpace(row.suppReqSno) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.regDt) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.reqInsttNm) + "</td>";
			append += "<td><a href='javascript:void(0)' onclick='fn_indexUDtl(\""+row.suppReqSno+"\");return false;'><u>"+row.incdtNo+"</u></a></td>";
			append += "<td><a href='javascript:void(0)' onclick='fn_indexUDtl(\""+row.suppReqSno+"\");return false;'><u>"+row.incdtNm+"</u></a></td>";
			append += "<td>" + gfn_nullRtnSpace(row.cfscSechPlndDt) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.reqUserNm) + "</td>";
			if(gfn_nullRtnSpace(row.prosrAprvDt) == ""){
				append += "<td>" + gfn_nullRtnSpace(row.prosrNm) + "</td>";
			}else{
				append += "<td>" + gfn_nullRtnSpace(row.prosrNm) + "<br/>(" + gfn_nullRtnSpace(row.prosrAprvDt) + ")</td>";
			}
			if(gfn_nullRtnSpace(row.rcvDt) == ""){
				append += "<td>" + gfn_nullRtnSpace(row.rcvNo) + "</td>";
			}else{
				append += "<td>" + gfn_nullRtnSpace(row.rcvNo) + "<br/>(" + gfn_nullRtnSpace(row.rcvDt) + ")</td>";
			}
			append += "<td>" + gfn_nullRtnSpace(row.rcvrNm) + "</td>";
			if(gfn_nullRtnSpace(row.aprvDt) == ""){
				append += "<td>" + gfn_nullRtnSpace(row.aprvrNm) + "</td>";
			}else{
				append += "<td>" + gfn_nullRtnSpace(row.aprvrNm) + "<br/>(" + gfn_nullRtnSpace(row.aprvDt) + ")</td>";
			}
			var str = gfn_nullRtnSpace(row.pgsStatNm);
			switch (gfn_nullRtnSpace(row.pgsStat)) {
				case 'C03001':
					str += "<br/>(" + gfn_nullRtnSpace(row.regDt) + ")";
				    break;
				case 'C03002':
					str += "<br/>(" + gfn_nullRtnSpace(row.reqDt) + ")";
				    break;
				case 'C03003':
					str += "<br/>(" + gfn_nullRtnSpace(row.prosrAprvDt) + ")";
				    break;
				case 'C03005':
					str += "<br/>(" + gfn_nullRtnSpace(row.rcvDt) + ")";
				    break;
				case 'C03006':
					str += "<br/>(" + gfn_nullRtnSpace(row.aprvDt) + ")";
				    break;
				default:
					break;
			}
			append += "<td>" + str + "</td>";

			append += "</tr>";
	        $("#listTab > tbody").append(append);
	 	});
	}

	data.__callFuncName__ ="fn_queryMList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
	$("#totalCount").text(data.totalCount);

	$('input:checkbox[name="chkAll"]').prop("checked", false);
}

/**
 * @!@ 현장지원 관리 승인
 * @param {json} data
 * @returns
 */
function fn_reqForAprv(){
	if($('input:checkbox[name="chk"]:checked').length == 0){
		fn_showUserPage(reqStr + " 대상을 선택해주세요.");
		return false;
	}
	var suppReqSnoArr = [];

	$('input:checkbox[name="chk"]:checked').each(function() {
		var snoStat = $(this).val().split(",");
		if(snoStat[1] == cncl){
			suppReqSnoArr.push(snoStat[0]);
		}
	});

	if(suppReqSnoArr.length == 0){
		fn_showUserPage(reqStr + " 가능 대상이 없습니다.", function() {
		});
		return false;
	}

	var Objtype = {};
	Objtype.pgsStat = req;

	var data = {rowDatas : suppReqSnoArr, obj : Objtype};

	fn_showModalPage(reqStr + "을(를) 하시겠습니까?", function() {
		var callUrl = "<c:url value='/site/rcv/reqSiteRcvUDtl.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_reqForAprvCallback'});
	});
}

/**
 * @!@ 현장지원 관리 승인 콜백
 * @param {json} data
 * @returns
 */
function fn_reqForAprvCallback(data){
	$("#page").val("1");
	fn_queryMList(1);
}

/**
 * @!@ 현장지원 관리 요청취소
 * @param {json} data
 * @returns
 */
function fn_reqCncl(){
	if($('input:checkbox[name="chk"]:checked').length == 0){
		fn_showUserPage(cnclStr + " 대상을 선택해주세요.", function() {
		});
		return false;
	}
	var suppReqSnoArr = [];

	$('input:checkbox[name="chk"]:checked').each(function() {
		var snoStat = $(this).val().split(",");
		if(snoStat[1] == req){
			suppReqSnoArr.push(snoStat[0]);
		}
	});

	if(suppReqSnoArr.length == 0){
		fn_showUserPage(cnclStr + " 가능 대상이 없습니다.", function() {
		});
		return false;
	}

	var Objtype = {};
	Objtype.pgsStat = cncl;

	var data = {rowDatas : suppReqSnoArr, obj : Objtype};

	fn_showModalPage(reqStr + "을(를) 취소 하시겠습니까?", function() {
		var callUrl = "<c:url value='/site/rcv/reqCnclSiteRcvUDtl.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_reqCnclCallback'});
	});
}

/**
 * @!@ 현장지원 관리 요청취소 콜백
 * @param {json} data
 * @returns
 */
function fn_reqCnclCallback(data){
	$("#page").val("1");
	fn_queryMList(1);
}

/**
 * @!@ 현장지원 관리 수정 화면 이동
 * @param {string} incdtSno
 * @returns
 */
function fn_indexUDtl(suppReqSno){

	requestUtil.setSearchForm("searchForm");

	$("#searchForm").append('<input type="hidden" class="" id="suppReqSno" name="suppReqSno" value="'+suppReqSno+'"/>');
	var param = "?suppReqSno="+suppReqSno;
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/site/pgs/indexSitePgsMDtl.do"/>'+param);
}

/**
 * @!@ 현장지원 관리 리스트 엑셀 다운로드
 * @param {string} incdtSno
 * @returns
 */
function fn_getExeclDownload(){
	requestUtil.comExcelXlsx({
		formNm :'searchForm',
		filename : "현장지원",
		sheetNm : "현장지원",
		columnArr : "요청번호,등록일자,부서명,사건번호,사건명,지원예정일,담당자,주임검사(승인일자),진행상태",
		columnVarArr : "suppReqSno,regDt,reqInsttNm,incdtNo,incdtNm,cfscSechPlndDt,reqUserNm,prosrNm,pgsStatNm",
		sqlQueryId : "siteRcvDAO.querySiteRcvMListExcel",
		splitSearch : "srcPgsStat"
	});
}

</script>
</head>
<body>
          <div id="con_wrap">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                    <div>
                         <div class="loca">
		                    <div class="ttl">지원 진행 관리</div>
		                    <div class="loca_list"></div>
		                  </div>
                          <div class="sub">
                             <!--------------검색------------------>
                             <form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
                             	<input type="hidden" class="" id="page" name="page" value="1"/>
                            <div class="t_head">
                                  <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_queryMList(1);">
                                        <caption>검색</caption>
                                         <colgroup>
                                            <col width="11%">
                                            <col width="22%">
                                            <col width="11%">
                                            <col width="23%">
                                            <col width="11%">
                                            <col width="22%">
                                         </colgroup>
                                   <thead>
                                      <tr>
                                         	<th scope="row" class="hcolor">사건번호</th>
								           <td>
								               <input type="text" id="srcIncdtNo" name="srcIncdtNo" title="사건번호" class="inpw80"/>
								           </td>
								           <th scope="row" class="hcolor">사건명</th>
								           <td>
								           		<input type="text" id="srcIncdtNm" name="srcIncdtNm" title="사건명" class="inpw80"/>
								           </td>
                                         	<th scope="row" class="hcolor">등록일자</th>
								           <td>
								               <input type="text" id="srcRegDtFrom" name="srcRegDtFrom" title="등록일자From" readonly class="inpw40"/>
								               <input type="text" id="srcRegDtTo" name="srcRegDtTo" title="등록일자TO" readonly class="inpw40"/>
								           </td>
								      </tr>
								      <tr>
								           <th scope="row" class="hcolor">지원예정일</th>
								           <td>
								               <input type="text" id="srcCfscSechPlndDtFrom" name="srcCfscSechPlndDtFrom" title="지원예정일From" class="inpw40"/>
								               <input type="text" id="srcCfscSechPlndDtTo" name="srcCfscSechPlndDtTo" title="지원예정일To" class="inpw40"/>
								           </td>
                                         	<th scope="row" class="hcolor">담당자</th>
								           <td>
								               <input type="text" id="srcReqUserNm" name="srcReqUserNm" title="담당자" class="inpw80"/>
								           </td>
								           <th scope="row" class="hcolor">진행상태</th>
								           <td>
								               <select id="srcPgsStat" name="srcPgsStat" title="진행상태" onchange="fn_queryMList(1)" class="selw6">
								               <!-- <select id="srcPgsStat" name="srcPgsStat" title="진행상태" class="selw6"> -->
								               </select>
								           </td>
								      </tr>
								      <tr>
								           <th scope="row" class="hcolor">요청번호</th>
								           <td>
								           		<input type="text" id="suppReqSno" name="suppReqSno" title="요청번호" class="inpw80"/>
								           </td>
								           <th scope="row" class="hcolor">접수번호</th>
								           <td>
								           		<input type="text" id="rcvNo" name="rcvNo" title="접수번호" class="inpw80"/>
								           </td>
								           <th scope="row" class="hcolor">사용여부</th>
								           <td>
								               <select id="srcUseYn" name="srcUseYn" title="사용여부" onchange="fn_queryMList(1)" class="selw6">
								               <!-- <select id="srcUseYn" name="srcUseYn" title="사용여부" class="selw6"> -->
								                   <option value="" >전체</option>
								                   <option value="Y" selected>사용</option>
								                   <option value="N">미사용</option>
								               </select>
								           </td>
                                      </tr>
                                      <tr>
                                      	   <th scope="row" class="hcolor">의뢰부서</th>
								           <td>
								           		<input type="text" id="reqInsttNm" name="reqInsttNm" title="의뢰부서" class="inpw80"/>
								           </td>
                                      	   <th scope="row" class="hcolor">죄명</th>
								           <td>
								           		<select id="srcCrimeGrp" name="srcCrimeGrp" title="죄명" onchange="fn_queryMList(1)" class="selw6">
								               </select>
								           </td>
								           <th scope="row" class="hcolor"></th>
								           <td>
								           </td>
                                      </tr>
                                   </thead>
                                </table>
                              </div>
                            <div class="btn_c">
                                  <ul>
                                     <li><a href="javascript:void(0);" class="myButton" onclick="fn_getExeclDownload();return false;">엑셀</a></li>
                                  	 <li id="reqBtn" name="reqBtn" style="display: none;"><a href="javascript:void(0);" onclick="fn_reqForAprv();return false;" class="myButton"></a></li>
                                  	 <li id="reqCnclBtn" style="display: none;"><a href="javascript:void(0);" onclick="fn_reqCncl();return false;" class="myButton"></a></li>
                                     <!-- <li><a href="javascript:fn_indexRDtl();" class="RdButton">등록</a></li> -->
                                     <li><a href="javascript:void(0)" onclick="fn_queryMList(1);return false;" class="gyButton">조회</a></li>
                                  </ul>
                               </div>
                               </form>
                            <!--------------//검색------------------>

                            <!--------------결과------------------>
                             <div class="r_num">| 결과  <strong style="color:#C00" id="totalCount">0</strong>건</div>

                             <!--------------목록---------------------->
                             <div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:450px;">
                                  <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="2%">
                                              <col width="7%">
                                              <col width="7%">
                                              <col width="11%">
                                              <col width="12%">
                                              <col width="12%">
                                              <col width="7%">
                                              <col width="7%">
                                              <col width="7%">
                                              <col width="7%">
                                              <col width="7%">
                                              <col width="7%">
                                              <col width="7%">
                                          </colgroup>
                                            <thead>
                                              <tr>
                                                 <th scope="col"><input type='checkbox' name='chkAll' id='chkAll' title='' class='check_agree1'/></th>
                                                 <th scope="col">요청번호</th>
                                                 <th scope="col">작성일자</th>
                                                 <th scope="col">부서명</th>
                                                 <th scope="col">사건번호</th>
                                                 <th scope="col">사건명</th>
                                                 <th scope="col">지원예정일</th>
                                                 <th scope="col">요청자</th>
                                                 <th scope="col">주임검사</th>
                                                 <th scope="col">접수번호</th>
                                                 <th scope="col">담당자</th>
                                                 <th scope="col">승인</th>
                                                 <th scope="col">진행상태</th>
                                              </tr>
                                            </thead>
                                            <tbody>
												<tr><td colspan="12">조회 결과가 없습니다.</td></tr>
                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->

                             <!-----------------------페이징----------------------->
                             <div id="page_navi" class="page_wrap"></div>
                               <!-----------------------//페이징----------------------->

                          </div>

                    </div>
            </div>
                 <!---  //contnets  적용 ------>
  </div>
<script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
ifa.attr('height', $("#contents_info").height()+120);
</script>
</body>
</html>