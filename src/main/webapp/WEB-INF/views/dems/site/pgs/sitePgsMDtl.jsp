<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 현장지원 관리 상세 화면
 * 4. 설명 : @!@ 현장지원 관리 상세 화면 
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var tabId;
var req = "";
var cncl = "";
var reqStr = "";
var cnclStr = "";
switch (session_usergb) {
	case 'C01001':
		req = "C03002";
		cncl = "C03001";
		reqStr = "검사승인요청";
		cnclStr = "검사승인요청취소";
	    break;
	case 'C01002':
		req = "C03003";
		cncl = "C03002";
		reqStr = "검사승인";
		cnclStr = "검사승인취소";
	    break;
	case 'C01003':
		req = "C03005";
		cncl = "C03003";
		reqStr = "접수";
		cnclStr = "접수취소";
	    break;
	case 'C01004':
		req = "C03006";
		cncl = "C03005";
		reqStr = "과장승인";
		cnclStr = "과장승인취소";
	    break;
	default:
		break;
}

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	/* $("#suppDgtlMchn, #suppData, #suppServer, #suppEtc").on("change", function(){
		if($(this).is(":checked")){
            $(this).val("Y");
        }else{
        	$(this).val("");
        }
	}); */
	/* gfn_calendarConfig("cfscSechPlndDt", "", "", ""); */
	<%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
	/* <span class="txt_info" name="ctnByteChk" id="ctnByteChk"></span> */
    gfn_overMaxLength("beforRead",4000);
    gfn_overMaxLength("suppReqCnts",4000);
    
    fn_queryMDtl();
    
    $("#reqBtn > a").text(reqStr);
    $("#reqCnclBtn > a").text(cnclStr);
    
    /* var comSearchDiv = parent.$("#comSearchDiv")
    console.log(comSearchDiv.find("#srcIncdtNo").val()); */
});

/**
 * @!@ 현장지원 관리 상세 조회
 * @param
 * @returns 
 */
function fn_queryMDtl(){
	
	var callUrl = "<c:url value='/site/rcv/querySiteRcvMDtl.do'/>";
	
	requestUtil.search({callUrl:callUrl, srhFormNm:'insForm', callbackNm:'fn_queryMDtlCallback'});
	
}

/**
 * @!@ 현장지원 관리 상세 조회 콜백
 * @param
 * @returns 
 */
function fn_queryMDtlCallback(data){
	/* $("#beforRead").trigger("keyup");
	$("#suppReqCnts").trigger("keyup"); */

	var rtnMap = data.rtnMap;
	if(rtnMap.reqDt == undefined){
		$("#spanSuppReqSno").text(gfn_nullRtnSpace(rtnMap.suppReqSno) + " (임시저장)");
	}else{
		$("#spanSuppReqSno").text(gfn_nullRtnSpace(rtnMap.suppReqSno) + " (" + gfn_nullRtnSpace(rtnMap.reqDt) + ")");
	}
	
	if(rtnMap.prosrAprvDt == undefined){
		$("#spanProsrNm").text(gfn_nullRtnSpace(rtnMap.prosrNm) + " (미승인)");
	}else{
		$("#spanProsrNm").text(gfn_nullRtnSpace(rtnMap.prosrNm) + " (" + gfn_nullRtnSpace(rtnMap.prosrAprvDt) + ")");
	}
	
	$("#reqUserNm").val(gfn_nullRtnSpace(rtnMap.reqUserNm) + " (tel:" + gfn_nullRtnSpace(rtnMap.reqUserTelNo) + ", hp:" + gfn_nullRtnSpace(rtnMap.reqUserHpTelNo) + ")");
	
	if(rtnMap.suppDgtlMchn == "Y"){
		$("#suppDgtlMchn").prop("checked", true);
	}
	if(rtnMap.suppData == "Y"){
		$("#suppData").prop("checked", true);
	}
	if(rtnMap.suppServer == "Y"){
		$("#suppServer").prop("checked", true);
	}
	if(rtnMap.suppEtc == "Y"){
		$("#suppEtc").prop("checked", true);
	}
	
	if(rtnMap.rcvDt != undefined){
		$("#rcvrNm").text(gfn_nullRtnSpace(rtnMap.rcvrNm) + " (" + gfn_nullRtnSpace(rtnMap.rcvNo) + ", " + gfn_nullRtnSpace(rtnMap.rcvDt) + ")");
	}
	
	if(rtnMap.aprvDt != undefined){
		$("#aprvrNm").text(gfn_nullRtnSpace(rtnMap.aprvrNm) + " (" + gfn_nullRtnSpace(rtnMap.aprvDt) + ")");
	}
	
	/* if(session_usergb == "C01002" || session_usergb == "C01003" || session_usergb == "C01004"){ */
		switch (gfn_nullRtnSpace(rtnMap.pgsStat)) {
			case 'C03001':
			    break;
			case 'C03002':
				if(session_usergb == "C01002"){
					$("#reqBtn").show();
				}
			    break;
			case 'C03003':
				if(session_usergb == "C01002"){
					$("#reqCnclBtn").show();
				}else if(session_usergb == "C01003"){
					$("#reqBtn").show();
				}
			    break;
			case 'C03005':
				if(session_usergb == "C01003"){
					$("#reqCnclBtn").show();
				}else if(session_usergb == "C01004"){
					$("#reqBtn").show();
				}
			    break;
			case 'C03006':
				if(session_usergb == "C01004"){
					$("#reqCnclBtn").show();
				}
			    break;
			default:
				break;
		}
	/* } */
	
}

/**
 * @!@ 현장지원 관리 승인
 * @param {json} data
 * @returns 
 */
function fn_reqForAprv(){
	var suppReqSnoArr = [];
	suppReqSnoArr.push($("#suppReqSno").val());
	
	var Objtype = {};
	Objtype.pgsStat = req;
	
	var data = {rowDatas : suppReqSnoArr, obj : Objtype};
	
	fn_showModalPage(reqStr + "을(를) 하시겠습니까?", function() {
		var callUrl = "<c:url value='/site/rcv/reqSiteRcvUDtl.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_reqForAprvCallback'});
	});
}

/**
 * @!@ 현장지원 관리 승인 콜백
 * @param {json} data
 * @returns 
 */
function fn_reqForAprvCallback(data){
	//requestUtil.setSearchFormPage({targetFormId:"searchForm", page:"1"});
	fn_indexMList();
}
 
/**
 * @!@ 현장지원 관리 승인 취소
 * @param {json} data
 * @returns 
 */
function fn_reqCncl(){
	
	var suppReqSnoArr = [];
	suppReqSnoArr.push($("#suppReqSno").val());
	
	var Objtype = {};
	Objtype.pgsStat = cncl;
	
	var data = {rowDatas : suppReqSnoArr, obj : Objtype};
	
	fn_showModalPage(reqStr + "을 취소 하시겠습니까?", function() {
		var callUrl = "<c:url value='/site/rcv/reqCnclSiteRcvUDtl.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_reqCnclCallback'});
	});
}
 
/**
 * @!@ 현장지원 관리 승인 취소 콜백
 * @param {json} data
 * @returns 
 */
function fn_reqCnclCallback(data){
	//requestUtil.setSearchFormPage({targetFormId:"searchForm", page:"1"});
	fn_indexMList();
}
 
/**
 * @!@ 현장지원 관리 리스트 화면 이동
 * @param
 * @returns 
 */
function fn_indexMList(){
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/site/pgs/indexSitePgsMList.do"/>');
}
</script>

</head>

<body>
<div id="con_wrap">
        <div class="content">
           <!----현재위치----->
             
            <div id="contents_info">
                 
                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">
                     	<input id="pgsStat" name="pgsStat"  title="진행상태" type="hidden" class="inpw50" />
                     	<input id="suppReqSno" name="suppReqSno"  title="요청번호" type="hidden" value="<c:out value='${param.suppReqSno}'/>" class="inpw50" />
                      <div class="t_list">
                      	<div class="sub_ttl">지원 요청 내용</div><!-----타이틀------>
		                 <table class="iptTblX">
			               <caption>상세</caption>
			               <colgroup>
				             <col width="20%" />
				             <col width="30%" />
				             <col width="20%" />
				             <col width="30%" />
			               </colgroup>
			               <tbody>
			               	 <tr>
				                 <th scope="row">요청번호(요청일자)</th>
				                 <td>
				                 	<span id="spanSuppReqSno"></span>
				                 </td>
				                 <th scope="row">검사(승인일자)</th>
				                 <td>
				                 	<span id="spanProsrNm"></span>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">요청기관</th>
				                 <td>
				                 	<span id="insttNm"></span>
				                 </td>
				                 <th scope="row">요청부서</th>
				                 <td>
				                 	<span id="reqInsttNm"></span>
				                 	<input id="reqInsttCd" name="reqInsttCd"  title="요청부서"  type="hidden" class="inpw50" />
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">사건번호(사건명)</th>
				                 <td>
				                 	<!-- <input id="incdtNm" name="incdtNm"  title="사건번호(사건명)"  type="text" readonly class="inpw80" data-requireNm="사건번호(사건명)" /> -->
				                 	<span id="incdtNm"></span>
				                 	<input id="incdtSno" name="incdtSno"  title="사건SNO"  type="hidden" class="inpw50" />
				                 	<!-- <button class="buttonG40" onclick="fn_incdtIncdtMListPop();return false;">검색</button> -->
				                 </td>
				                 <th scope="row">주임군검사</th>
				                 <td>
							 		<!-- <input id="prosrNm" name="prosrNm"  title="주임군검사"  type="text" readonly class="inpw80" data-requireNm="주임군검사" /> -->
							 		<span id="prosrNm"></span>
							 		<input id="prosrId" name="prosrId"  title="주임군검사ID"  type="hidden" class="inpw50" />
							 		<!-- <button class="buttonG40" onclick="fn_incdtRegUserMListPop('C01002', 'prosrId', 'prosrNm');return false;">검색</button> -->
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">담당자(연락처, HP)</th>
				                 <td>
							 		<!-- <input id="reqUserNm" name="reqUserNm"  title="담당자"  type="text" value="" readonly class="inpw80" data-requireNm="담당자" /> -->
							 		<span id="reqUserNm"></span>
							 		<input id="reqUserId" name="reqUserId"  title="담당자ID"  type="hidden" value="" class="inpw50" />
							 		<!-- <button class="buttonG40" onclick="fn_incdtRegUserMListPop('C01001', 'reqUserId', 'reqUserNm');return false;">검색</button> -->
				                 </td>
				                 <th scope="row">지원구분(압수대상)</th>
				                 <td>
				                 	<input type="checkbox" name="suppDgtlMchn" id="suppDgtlMchn" title="디지털기기" value="Y" class="check_agree1" style=""/><label for="suppDgtlMchn" style="display: inline;">디지털기기</label>
				                 	<input type="checkbox" name="suppData" id="suppData" title="데이터" value="Y" class="check_agree1" style="margin-left: 10px;"/><label for="suppData" style="display: inline;">데이터</label>
				                 	<input type="checkbox" name="suppServer" id="suppServer" title="서버DB" value="Y" class="check_agree1" style="margin-left: 10px;"/><label for="suppServer" style="display: inline;">서버DB</label>
				                 	<input type="checkbox" name="suppEtc" id="suppEtc" title="기타" value="Y" class="check_agree1" style="margin-left: 10px;"/><label for="suppEtc" style="display: inline;">기타</label>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">사용여부</th>
				                 <td colspan="3">
				                 	<!-- <select id="useYn" name="useYn" title="사용여부" class="selw6">
					                   <option value="Y" selected>사용</option>
					                   <option value="N">미사용</option>
							 		</select> -->
							 		<span id="useYnNm"></span>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">사전준비</th>
				                 <td colspan="3">
				                 	<textarea name="beforRead" id="beforRead" title="사전준비" rows="4" cols="10"  style="width:99%;" data-requireNm="사전준비"  data-maxLength="4000" readonly></textarea>
	                                <!-- <span class="txt_info" name="beforReadByteChk" id="beforReadByteChk"></span> -->
				                 </td>
			                 </tr>
			                 
			                </tbody>
		                 </table>
		                 
		                 <div class="sub_ttl">세부 요청 내용</div><!-----타이틀------>
		                 <table class="iptTblX">
			               <caption>상세</caption>
			               <colgroup>
				             <col width="20%" />
				             <col width="30%" />
				             <col width="20%" />
				             <col width="30%" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">압수, 수색 예정일</th>
				                 <td colspan="3">
				                 	<!-- <input id="cfscSechPlndDt" name="cfscSechPlndDt"  title="압수, 수색 예정일"  type="text" readonly class="inpw10" /> -->
				                 	<span id="cfscSechPlndDt"></span>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">압수, 수색 장소</th>
				                 <td>
				                 	<!-- <input id="cfscSechPlace" name="cfscSechPlace"  title="압수, 수색 장소"  type="text" class="inpw50" data-requireNm="압수, 수색 장소" data-maxLength="300" /> -->
				                 	<span id="cfscSechPlace"></span>
				                 </td>
				                 <th scope="row">요청인원</th>
				                 <td>
							 		<!-- <input id="reqNum" name="reqNum"  title="요청인원"  type="text" class="inpw50" data-requireNm="요청인원" data-maxLength="200" /> -->
							 		<span id="reqNum"></span>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">지원요청내용</th>
				                 <td colspan="3">
				                 	<textarea name="suppReqCnts" id="suppReqCnts" title="지원요청내용" rows="4" cols="10"  style="width:99%;" data-requireNm="지원요청내용" data-maxLength="4000" readonly></textarea>
	                                <!-- <span class="txt_info" name="suppReqCntsByteChk" id="suppReqCntsByteChk"></span> -->
				                 </td>
			                 </tr>
			                 
			                </tbody>
		                 </table>
		                 <br />
		                 <table class="iptTblX">
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="row">
										담당자 (접수번호, 접수일자)
									</th>
									<td>
										<span id="rcvrNm"></span>
									</td>
									<th scope="row">
										승인자 (승인일자)
									</th>
									<td>
										<span id="aprvrNm"></span>
									</td>
								</tr>
							</thead>
						</table>
	                  </div>
					</form>
                    <div class="btn_c">
                      <ul>
                      	<li id="reqBtn" name="reqBtn" style="display: none;"><a href="javascript:void(0);" onclick="fn_reqForAprv();return false;" class="myButton"></a></li>
						<li id="reqCnclBtn" name="reqCnclBtn" style="display: none;"><a href="javascript:void(0);" onclick="fn_reqCncl();return false;" class="myButton"></a></li>
                        <!-- <li><a href="javascript:void(0);" class="RdButton" onclick="fn_regRDtl('C03001');return false;" style="display: none;" id="saveBtn" name="saveBtn">저장</a></li>
                        <li><a href="javascript:void(0);" class="RdButton" onclick="fn_delUDtl();return false;" style="display: none;" id="delBtn" name="delBtn">삭제</a></li> -->
                        <!-- <li><a href="javascript:void(0);" class="myButton">재입력</a></li> -->
                        <li><a href="javascript:void(0)" class="myButton" onclick="fn_indexMList();return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->   
                       
                    
                          
                  </div>
            </div>
        
        </div>
 </div>
<div id="incdtRegUserMListPop"></div>
<div id="incdtIncdtMListPop"></div>

<script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
ifa.attr('height', $("#contents_info").height()+50);
//$(top.document).find("#container").width($(top.document).find("#container").width() - 5)
</script>

</body>
</html>