<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp"%>

<style type="text/css">
	.pagingWrap{display: block;position: relative;overflow: hidden; margin-top:20px; margin-bottom:80px;}
	.pagingWrap .paging{text-align: center;margin: 0;overflow: hidden;line-height: 1.8em;word-break: keep-all;word-wrap: break-word;}
	.pagingWrap .paging span{margin: 0 1px;text-align: center;color: #fff;font-weight: bold;text-decoration: underline;border: 0 none;line-height: 35px;width: 40px;display: inline-block;border: 1px solid #dfdfdf;}
	.pagingWrap .paging strong{margin: 0 1px;text-align: center;color: #fff; font-weight: bold; text-decoration: underline;line-height: 35px; font-size: 13px;display: inline-block;width: 40px;border: 1px solid #dfdfdf;background-color: #42454c !important;overflow: hidden;vertical-align: middle;}
	.pagingWrap .paging a{color: #666666;font-size: 11px;font-weight: normal;text-decoration: none;display: inline-block;width: 40px;line-height: 35px;text-align: center;border: 1px solid #dfdfdf;overflow: hidden;vertical-align: middle;}
	.sub tr{    cursor: pointer;}

</style>

<script type="text/javaScript" language="javascript" defer="defer">
$(document).ready(function() {
	gfn_calendarConfig("startDtFormat", "startDtFormat", "minDate", "");
	gfn_calendarConfig("endDtFormat", "endDtFormat", "maxDate", "");
    $('#startDtFormat').val('${paramVO.startDtFormat}');
    $('#endDtFormat').val('${paramVO.endDtFormat}');
});

function fnList(page){
	var form = document.paramForm;
	form.currentPageNo.value= page;
	form.action = "/requested/requestedList.do";
	form.submit();
}

function fnSearch(){
	var form = document.searchForm;
	form.action = "/requested/requestedList.do";
	form.submit();
}

function fnDetail(borderNm , postSeq){
	var menuNo = "";
	var menuUrl = "";
	var keyParamNm = "";
	var param = "";

	var menuFindUrl = "";



	 if(borderNm == '지원요청'){
		 keyParamNm = "suppReqSno";
		if(session_usergb == 'C01001'){
			menuUrl = "/site/rcv/indexSiteRcvUDtl.do";
			menuFindUrl = "/site/rcv/indexSiteRcvMList.do";
		} else if(session_usergb == 'C01002' || session_usergb == 'C01003' || session_usergb == 'C01004'){
			menuUrl = "/site/pgs/indexSitePgsMDtl.do";
			menuFindUrl = "/site/pgs/indexSitePgsMList.do";
		}
	} else if(borderNm == '분석요청'){
		keyParamNm = "analReqSno";
		if(session_usergb == 'C01001' || session_usergb == 'C01002'){
			menuUrl = "/anls/req/indexAnlsReqInfo.do";
			menuFindUrl = "/anls/req/indexAnlsReqMList.do";
		} else if(session_usergb == 'C01003'){
			menuUrl = "/anls/pgs/indexAnlsPgsRDtl.do";
			menuFindUrl = "/anls/pgs/indexAnlsPgsMList.do";
		}else if( session_usergb == 'C01004'){
			menuUrl = "/anls/pgs/indexAnlsPgsInfo.do";
			menuFindUrl = "/anls/pgs/indexAnlsPgsMList.do";
		}
	} else if(borderNm == '공판요청'){
		keyParamNm = "tstfReqSno";
		if(session_usergb == 'C01001' || session_usergb == 'C01002'){
			menuUrl = "/tral/req/indexTralReqUDtl.do";
			menuFindUrl = "/tral/req/indexTralReqMList.do";
		} else if( session_usergb == 'C01003' || session_usergb == 'C01004'){
			menuUrl = "/tral/pgs/indexTralPgsUDtl.do";
			menuFindUrl = "/tral/pgs/indexTralPgsMList.do";
		}
	} else if(borderNm == '장비대여'){
		keyParamNm = "rentAplnSno";
		if(session_usergb == 'C01001' || session_usergb == 'C01002'){
			menuUrl = "/eqp/lend/indexEqpLendLendReqUDtl.do";
			menuFindUrl = "/eqp/lend/indexEqpLendMList.do";
			param = "&modFlag=modify";
		} else if( session_usergb == 'C01003' || session_usergb == 'C01004'){
			menuUrl = "/eqp/lend/indexEqpLendAprvRDtl.do";
			menuFindUrl = "/eqp/lend/indexEqpLendAprvMList.do";
			param = "&modFlag=aprv&menuGb=aprv";
		}
	} else if(borderNm == '증거관리'){
		keyParamNm = "disuseReqSno";
		if(session_usergb == 'C01001' || session_usergb == 'C01002' ){
			menuUrl = "/evdc/apln/indexEvdcAplnUDtl.do";
			menuFindUrl = "/evdc/apln/indexEvdcAplnMList.do";
		} else if(session_usergb == 'C01003' || session_usergb == 'C01004'){
			menuUrl = "/evdc/trt/indexEvdcTrtVDtl.do";
			menuFindUrl = "/evdc/trt/indexEvdcTrtMList.do";
		}

		if(session_usergb == 'C01001'){
			param = "&modFlag=modify";
		} else if ( session_usergb == 'C01002'){
			param = "&modFlag=aprv";
		} else if ( session_usergb == 'C01003'){
			param = "&modFlag=rcv";
		} else if ( session_usergb == 'C01004'){
			param = "&modFlag=aprv";
		}
	}

	var result = parent.fn_getMenuDate(menuFindUrl);

	parent.fn_openMenu(result.depthFullname , result.url , result.menuNm , result.menuno);
	parent.fn_frameMoveUrl(result.menuno , menuUrl + "?" + keyParamNm + "=" + postSeq + param)
}

</script>
</head>
<body>
<form name="paramForm" id="paramForm" method="get">
	<input type="hidden" name="searchIncdtNo" 	value="${paramVO.searchIncdtNo}">
	<input type="hidden" name="searchIncdtNm"	value="${paramVO.searchIncdtNm}">
	<input type="hidden" name="searchBorder" 	value="${paramVO.searchBorder}">
	<input type="hidden" name="searchRegNm" 	value="${paramVO.searchRegNm}">
	<input type="hidden" name="startDtFormat" 	value="${paramVO.startDtFormat}">
	<input type="hidden" name="endDtFormat" 	value="${paramVO.endDtFormat}">
	<input type="hidden" name="currentPageNo" 	value="${paramVO.currentPageNo}">
</form>


	<div id="con_wrap">
		<div id="contents_info">
			<!--- contnets  적용 ------>
			<div>
				<div class="loca">
					<div class="ttl">지원 요청 관리</div>
					<div class="loca_list"></div>
				</div>
				<div class="sub">
					<!--------------검색------------------>
					<form name="searchForm" id="searchForm" method="post"
						onsubmit="return false;">
						<input type="hidden" class="" id="page" name="page" value="1" />
						<div class="t_head">
							<table class="tbl_type_hd" border="1" cellspacing="0" >
								<caption>검색</caption>
								<colgroup>
									<col width="11%">
									<col width="22%">
									<col width="11%">
									<col width="23%">
									<col width="11%">
									<col width="22%">
								</colgroup>
								<thead>
									<tr>
										<th scope="row" class="hcolor">사건번호</th>
										<td>
											<input type="text" id="searchIncdtNo" name="searchIncdtNo"title="사건번호" class="inpw80" value="${paramVO.searchIncdtNo}" />
										</td>
										<th scope="row" class="hcolor">사건명</th>
										<td>
											<input type="text" id="searchIncdtNm" name="searchIncdtNm"title="사건명" class="inpw80" value="${paramVO.searchIncdtNm}"/>
										</td>
										<th scope="row" class="hcolor">등록일자</th>
										<td>
											<input type="text" id="startDtFormat"name="startDtFormat" title="등록일자From" readonly class="inpw40" />
											<input type="text" id="endDtFormat" name="endDtFormat"title="등록일자TO" readonly class="inpw40" />
										</td>
									</tr>
									<tr>
										<th scope="row" class="hcolor">게시판</th>
										<td>
											<input type="text" id="searchBorder" name="searchBorder"title="사건명" class="inpw80" value="${paramVO.searchBorder}"/>
										</td>
										<th scope="row" class="hcolor">담당자</th>
										<td>
											<input type="text" id="searchRegNm"name="searchRegNm" title="담당자" class="inpw80" value="${paramVO.searchRegNm}"/>
										</td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="btn_c">
							<ul>
								<li><a href="javascript:void(0)" onclick="fnSearch(1);return false;" class="gyButton">조회</a></li>
							</ul>
						</div>
					</form>
					<!--------------//검색------------------>

					<!--------------결과------------------>
					<div class="r_num"> | 결과 <strong style="color: #C00" id="totalCount">${totalCount}</strong>건
					</div>

					<!--------------목록---------------------->
					<div class="t_list"
						style="OVERFLOW-Y: auto; overflow-x: hidden; width: 100%; height: 450px;">
						<table id="listTab" class="tbl_type" border="1" cellspacing="0">
							<caption>목록</caption>
							<colgroup>
								<col width="8%">
								<col width="8%">
								<col width="12%">
								<col width="15%">
								<col width="15%">
								<col width="8%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">번호</th>
									<th scope="col">사건번호</th>
									<th scope="col">사건명</th>
									<th scope="col">게시판</th>
									<th scope="col">부서명</th>
									<th scope="col">담당자</th>
									<th scope="col">등록일</th>
									<th scope="col">상태</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${not empty requestedList}">
										<c:forEach var="requestedVO" items="${requestedList}" varStatus="status">
											<tr onclick="fnDetail('${requestedVO.reqBorderNm}','${requestedVO.reqSeq}');">
												<td>${requestedVO.reqSeq}</td>
												<td>${requestedVO.incdtNo}</td>
												<td>${requestedVO.incdtNm}</td>
												<td>${requestedVO.reqBorderNm}</td>
												<td>${requestedVO.reqInsttNm}</td>
												<td>${requestedVO.reqUserNm}</td>
												<td>${requestedVO.reqDt}</td>
												<td>${requestedVO.reqStatus}</td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<tr>
											<td colspan="8">조회 결과가 없습니다.</td>
										</tr>
									</c:otherwise>
								</c:choose>

							</tbody>
						</table>
					</div>
					<div class="pagingWrap">
						<p class="paging">
							<ui:pagination paginationInfo="${paramVO}" type="user" jsFunction="fnList"/>
						</p>
					</div>
				</div>

			</div>
		</div>
		<!---  //contnets  적용 ------>
	</div>

<script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
ifa.attr('height', $("#contents_info").height()+120);
</script>
</body>
</html>