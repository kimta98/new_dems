<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp"%>

<style type="text/css">
	.pagingWrap{display: block;position: relative;overflow: hidden; margin-top:20px; margin-bottom:80px;}
	.pagingWrap .paging{text-align: center;margin: 0;overflow: hidden;line-height: 1.8em;word-break: keep-all;word-wrap: break-word;}
	.pagingWrap .paging span{margin: 0 1px;text-align: center;color: #fff;font-weight: bold;text-decoration: underline;border: 0 none;line-height: 35px;width: 40px;display: inline-block;border: 1px solid #dfdfdf;}
	.pagingWrap .paging strong{margin: 0 1px;text-align: center;color: #fff; font-weight: bold; text-decoration: underline;line-height: 35px; font-size: 13px;display: inline-block;width: 40px;border: 1px solid #dfdfdf;background-color: #42454c !important;overflow: hidden;vertical-align: middle;}
	.pagingWrap .paging a{color: #666666;font-size: 11px;font-weight: normal;text-decoration: none;display: inline-block;width: 40px;line-height: 35px;text-align: center;border: 1px solid #dfdfdf;overflow: hidden;vertical-align: middle;}
	.sub tr{    cursor: pointer;}

</style>

<script type="text/javaScript" language="javascript" defer="defer">
$(document).ready(function() {
	gfn_calendarConfig("startDtFormat", "startDtFormat", "minDate", "");
	gfn_calendarConfig("endDtFormat", "endDtFormat", "maxDate", "");
    $('#startDtFormat').val(gfn_dashDate(gfn_addMonth(gfn_getCurDate(), -1), "-"));
    $('#endDtFormat').val(gfn_dashDate(gfn_getCurDate(), "-"));
});


function fn_getExeclDownload() {
	//로딩바 출력

	var callUrl = "/requested/registerExcel.do";

	var startDtFormat = $("#startDtFormat").val();
	var endDtFormat = $("#endDtFormat").val();
	var checkList = [];

	if($("input[name=cbx]:checked").length < 1){
		fn_showUserPage("하나 이상의 대장 종류가 선택되어야 합니다.");
		return false;
	}
	
	$(".modal").show();
	
	$("input[name=cbx]:checked").each(function() {
		checkList.push($(this).val());
	});
	$.fileDownload(callUrl, {
		httpMethod: "post",
		data: "startDt="+startDtFormat+"&endDt="+endDtFormat+"&checkList="+checkList,
		successCallback: function() {
			$(".modal").hide();
      	},
      	failCallback: function() {
        	return false;
      	}
	});
}

</script>
<form name="paramForm" id="paramForm" method="get">
	<%-- <input type="hidden" name="searchIncdtNo" 	value="${paramVO.searchIncdtNo}">
	<input type="hidden" name="searchIncdtNm"	value="${paramVO.searchIncdtNm}">
	<input type="hidden" name="searchBorder" 	value="${paramVO.searchBorder}">
	<input type="hidden" name="searchRegNm" 	value="${paramVO.searchRegNm}">
	<input type="hidden" name="startDtFormat" 	value="${paramVO.startDtFormat}">
	<input type="hidden" name="endDtFormat" 	value="${paramVO.endDtFormat}">
	<input type="hidden" name="currentPageNo" 	value="${paramVO.currentPageNo}"> --%>
</form>

	<div id="con_wrap">
		<div id="contents_info">
			<!--- contnets  적용 ------>
			<div>
				<div class="loca">
					<div class="ttl">대장 관리</div>
					<div class="loca_list"></div>
				</div>
				<div class="sub">
					<!--------------검색------------------>
					<form name="searchForm" id="searchForm" method="post"
						onsubmit="return false;">
						<input type="hidden" class="" id="page" name="page" value="1" />
						<div class="t_head">
							<table class="tbl_type_hd" border="1" cellspacing="0" >
								<caption>검색</caption>
								<colgroup>
									<col width="11%">
									<col width="22%">
									<col width="11%">
									<col width="23%">
									<col width="11%">
									<col width="22%">
								</colgroup>
								<thead>
									<tr>
										<th scope="row" class="hcolor">대장범위</th>
										<td>
											<input type="text" id="startDtFormat"name="startDtFormat" title="등록일자From" readonly class="inpw40" />
											<input type="text" id="endDtFormat" name="endDtFormat"title="등록일자TO" readonly class="inpw40" />
										</td>
										<th scope="row" class="hcolor">대장종류</th>
										<td colspan="3">
											<input type="checkbox" name="cbx" id="cbx" title="현장관리" value="site" class="check_agree1" style="margin-left: 10px;"/><label  style="display: inline;">현장관리</label>
						                 	<input type="checkbox" name="cbx" id="cbx" title="분석관리" value="analysis" class="check_agree1" style="margin-left: 10px;"/><label  style="display: inline;">분석관리</label>
						                 	<input type="checkbox" name="cbx" id="cbx" title="공판관리" value="tral" class="check_agree1" style="margin-left: 10px;"/><label  style="display: inline;">공판관리</label>
						                 	<input type="checkbox" name="cbx" id="cbx" title="디지털증거관리" value="evdc" class="check_agree1" style="margin-left: 10px;"/><label  style="display: inline;">디지털증거관리</label>
						                 	<input type="checkbox" name="cbx" id="cbx" title="장비관리" value="equip" class="check_agree1" style="margin-left: 10px;"/><label  style="display: inline;">장비관리</label>
										</td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="btn_c">
							<ul>
								<li><a href="javascript:void(0);" class="myButton" onclick="fn_getExeclDownload();return false;">엑셀 다운로드</a></li>
							</ul>
						</div>
					</form>
				</div>

			</div>
		</div>
		<!---  //contnets  적용 ------>
	</div>

<script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
ifa.attr('height', $("#contents_info").height()+120);
</script>
</body>
</html>


</head>
<body>