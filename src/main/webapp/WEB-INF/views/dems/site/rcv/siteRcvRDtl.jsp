<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 현장지원 관리 등록 화면
 * 4. 설명 : @!@ 현장지원 관리 등록 화면 
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<%-- <script type="text/javascript" src="<c:url value='/js/jquery.ajax-cross-origin.min.js' />" ></script> --%>
<script type="text/javaScript" language="javascript" defer="defer">
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	/* $("#suppDgtlMchn, #suppData, #suppServer, #suppEtc").on("change", function(){
		if($(this).is(":checked")){
            $(this).val("Y");
        }else{
        	$(this).val("");
        }
	}); */
	gfn_calendarConfig("cfscSechPlndDt", "", "", "");
    $('#cfscSechPlndDt').val(gfn_dashDate(gfn_getCurDate(), "-"));
	<%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
	/* <span class="txt_info" name="ctnByteChk" id="ctnByteChk"></span> */
    gfn_overMaxLength("beforRead",4000);
    gfn_overMaxLength("suppReqCnts",4000);
    
    $("#reqUserId").val(session_userid);
    $("#reqUserNm").val(session_usernm);
    $("#insttNm").html(session_insttnm);
    $("#deptNm").html(session_deptnm);
    $("#reqInsttCd").val(session_insttcd);
    
    if(session_usergb == "C01001"){
		$("#reqBtn").show();
		$("#saveBtn").show();
	}
    if(session_usergb == "C01003"){
    	$("#saveBtn").show();	
    	$("#prosrTd>button").hide();
    	//$("#prosrNm").val("관리자");
    	//$("#prosrId").val("Admin");
    }
});

/**
 * @!@ 담당자 조회 팝업
 * @param cdId
 * @returns 
 */
var findUserGb = "";
var targetTagId = "";
var targetTagNm = "";
function fn_incdtRegUserMListPop(userGb, tagId, tagNm) {

	findUserGb = userGb;
	if(session_usergb == "C01003"){
		findUserGb = "C01003";
	}
	targetTagId = tagId;
	targetTagNm = tagNm;
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtRegUserMListPop",
		height: 700,
        width: 1000,
        title: '담당자 조회',
        divId : 'incdtRegUserMListPop'
	});

}

/**
 * @!@ 사건관리 조회 팝업
 * @param cdId
 * @returns 
 */
function fn_incdtIncdtMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtMListPop",
		height: 700,
        width: 1000,
        title: '사건 조회',
        divId : 'incdtIncdtMListPop'
	});

}

/**
 * 사건관리 등록 팝업
 */
function fn_incdtIncdtRDtlPop() {
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtRDtlPop",
		height: 800,
        width: 1000,
        title: '사건 등록',
        divId : 'incdtIncdtRDtlPop'
	});
}

/**
* @!@ 현장지원 관리 등록
* @param
* @returns 
*/
function fn_regRDtl(pgsStat){
	/* $.ajax({
		//type:'post',
		url:'http://192.168.0.41:8080/spms/test/service.do',
dataType: 'jsonp',
//crossOrigin : true,
        data: {
       "type": "ftp_uploadFile",
       "hash": "sha256",
       "src": "D:/DEMS/tip1.exe,D:/DEMS/tip2.exe,D:/DEMS/tip3.exe",
       "dst": "path:/case1/ev1",
    	"path": "/case1/ev1"
    },
    jsonp: "callback",
        success:function(data){
            console.log("success :: \n"+JSON.stringify(data))
        },
        fail:function(data){
            console.log("fail :: \n"+JSON.stringify(data))
        }

	}) 
	*/
	/* $.ajax({
		//type:'post',
		url:'http://192.168.0.41:8080/spms/test/service.do',
dataType: 'jsonp',
//crossOrigin : true,
        data: {
            "type": "ftp_uploadFile",
            "hash": "sha256",
            "src": ["C:/Users/admin/Desktop/c.exe"],
            "dst": {"path": "/case1/ev1"}
         },
    jsonp: "callback",
        success:function(data){
            console.log("success :: \n"+JSON.stringify(data))
        },
        fail:function(data){
            console.log("fail :: \n"+JSON.stringify(data))
        }

	}) 
	return false; */
	
	if(!validUtil.checkInputValid({valFormID:'insForm'})){
		return;
	}
	
	if($("#suppDgtlMchn:checked, #suppData:checked, #suppServer:checked, #suppEtc:checked").length == 0){
		fn_showUserPage("지원구분(압수대상)을 한개 이상 선택해주세요.", function() {
		});
		return false;
	}
	
	var msg = "";
	if(pgsStat == "C03001"){
		msg = "저장 하시겠습니까?";
	}else if(pgsStat == "C03002"){
		msg = "작성된 내용으로 요청 하시겠습니까?";
	}
	
	$("#pgsStat").val(pgsStat);
	
	fn_showModalPage(msg, function() {
		var callUrl = "<c:url value='/site/rcv/reqSiteRcvRDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_regRDtlCallback'});
	});

}

/**
 * @!@ 현장지원 관리 등록 콜백
 * @param {json} data
 * @returns 
 */
function fn_regRDtlCallback(data){
	fn_indexMList();
}

/**
 * @!@ 현장지원 관리 리스트 화면 이동
 * @param
 * @returns 
 */
function fn_indexMList(){
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/site/rcv/indexSiteRcvMList.do"/>');
}
</script>

</head>

<body>
<div id="con_wrap">
        <div class="content">
           <!----현재위치----->
             
            <div id="contents_info">
                 
                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">
                     	<input id="pgsStat" name="pgsStat"  title="진행상태"  type="hidden" class="inpw50" />
                      <div class="t_list">
                      	<div class="sub_ttl">지원 요청 내용</div><!-----타이틀------>
		                 <table class="iptTblX">
			               <caption>등록</caption>
			               <colgroup>
				             <col width="20%" />
				             <col width="30%" />
				             <col width="20%" />
				             <col width="30%" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">요청기관</th>
				                 <td>
				                 	<span id="insttNm"></span>
				                 </td>
				                 <th scope="row">요청부서</th>
				                 <td>
				                 	<span id="deptNm"></span>
				                 	<input id="reqInsttCd" name="reqInsttCd"  title="요청부서"  type="hidden" class="inpw50"  data-requireNm="요청부서"	data-maxLength="10"/>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">사건번호(사건명)<span class="fontred">*</span></th>
				                 <td>
				                 	<input id="incdtNm" name="incdtNm"  title="사건번호(사건명)"  type="text" readonly class="inpw80" data-requireNm="사건번호(사건명)" />
				                 	<input id="incdtSno" name="incdtSno"  title="사건SNO"  type="hidden" class="inpw50" data-maxLength="10"/>
				                 	<button class="buttonG40" onclick="fn_incdtIncdtMListPop();return false;">검색</button>
				                 </td>
				                 <th scope="row">주임군검사<span class="fontred">*</span></th>
				                 <td id="prosrTd">
							 		<input id="prosrNm" name="prosrNm"  title="주임군검사"  type="text" readonly class="inpw80" data-requireNm="주임군검사" />
							 		<input id="prosrId" name="prosrId"  title="주임군검사ID"  type="hidden" class="inpw50" data-maxLength="20"/>
							 		<button class="buttonG40" onclick="fn_incdtRegUserMListPop('C01002', 'prosrId', 'prosrNm');return false;">검색</button>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">담당자(연락처, HP)<span class="fontred">*</span></th>
				                 <td>
							 		<input id="reqUserNm" name="reqUserNm"  title="담당자"  type="text" value="" readonly class="inpw80" data-requireNm="담당자" />
							 		<input id="reqUserId" name="reqUserId"  title="담당자ID"  type="hidden" value="" class="inpw50" data-maxLength="20"/>
							 		<button class="buttonG40" onclick="fn_incdtRegUserMListPop('C01001', 'reqUserId', 'reqUserNm');return false;">검색</button>
				                 </td>
				                 <th scope="row">지원구분(압수대상)<span class="fontred">*</span></th>
				                 <td>
				                 	<input type="checkbox" name="suppDgtlMchn" id="suppDgtlMchn" title="디지털기기" value="Y" class="check_agree1" style=""/><label for="suppDgtlMchn" style="display: inline;">디지털기기</label>
				                 	<input type="checkbox" name="suppData" id="suppData" title="데이터" value="Y" class="check_agree1" style="margin-left: 10px;"/><label for="suppData" style="display: inline;">데이터</label>
				                 	<input type="checkbox" name="suppServer" id="suppServer" title="서버DB" value="Y" class="check_agree1" style="margin-left: 10px;"/><label for="suppServer" style="display: inline;">서버DB</label>
				                 	<input type="checkbox" name="suppEtc" id="suppEtc" title="기타" value="Y" class="check_agree1" style="margin-left: 10px;"/><label for="suppEtc" style="display: inline;">기타</label>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">사용여부</th>
				                 <td colspan="3">
				                 	<select id="useYn" name="useYn" title="사용여부" class="selw6">
					                   <option value="Y" selected>사용</option>
					                   <option value="N">미사용</option>
							 		</select>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">사전준비<span class="fontred">*</span></th>
				                 <td colspan="3">
				                 	<textarea name="beforRead" id="beforRead" title="사전준비" rows="4" cols="10"  style="width:99%; " data-requireNm="사전준비"  data-maxLength="4000"></textarea>
	                                <span class="txt_info" name="beforReadByteChk" id="beforReadByteChk"></span>
				                 </td>
			                 </tr>
			                 
			                </tbody>
		                 </table>
		                 
		                 <div class="sub_ttl">세부 요청 내용</div><!-----타이틀------>
		                 <table class="iptTblX">
			               <caption>등록</caption>
			               <colgroup>
				             <col width="20%" />
				             <col width="30%" />
				             <col width="20%" />
				             <col width="30%" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">압수, 수색 예정일<span class="fontred">*</span></th>
				                 <td colspan="3">
				                 	<input id="cfscSechPlndDt" name="cfscSechPlndDt"  title="압수, 수색 예정일"  type="text" readonly class="inpw10" data-requireNm="압수, 수색 예정일"	data-maxLength="10"/>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">압수, 수색 장소<span class="fontred">*</span></th>
				                 <td>
				                 	<input id="cfscSechPlace" name="cfscSechPlace"  title="압수, 수색 장소"  type="text" class="inpw50" data-requireNm="압수, 수색 장소" data-maxLength="300" />
				                 </td>
				                 <th scope="row">요청인원<span class="fontred">*</span></th>
				                 <td>
							 		<input id="reqNum" name="reqNum"  title="요청인원"  type="text" class="inpw50" data-requireNm="요청인원" data-maxLength="200" />
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">지원요청내용<span class="fontred">*</span></th>
				                 <td colspan="3">
				                 	<textarea name="suppReqCnts" id="suppReqCnts" title="지원요청내용" rows="4" cols="10"  style="width:99%;" data-requireNm="지원요청내용"  data-maxLength="4000"></textarea>
	                                <span class="txt_info" name="suppReqCntsByteChk" id="suppReqCntsByteChk"></span>
				                 </td>
			                 </tr>
			                 
			                </tbody>
		                 </table>
	                  </div>
					</form>
                    <div class="btn_c">
                      <ul>
                      	<li><a href="javascript:void(0);" class="myButton" onclick="fn_regRDtl('C03002');return false;" style="display: none;" id="reqBtn" name="reqBtn">승인요청</a></li>
                        <li><a href="javascript:void(0);" class="myButton" onclick="fn_regRDtl('C03001');return false;" style="display: none;" id="saveBtn" name="saveBtn">저장</a></li>
                        <!-- <li><a href="javascript:void(0);" class="myButton">재입력</a></li> -->
                        <li><a href="javascript:void(0);" class="myButton" onclick="fn_indexMList();return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->   
                       
                    
                          
                  </div>
            </div>
        
        </div>
 </div>
<div id="incdtRegUserMListPop"></div>
<div id="incdtIncdtMListPop"></div>
<div id="incdtIncdtRDtlPop"></div>

<script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
ifa.attr('height', $("#contents_info").height()+50);
//$(top.document).find("#container").width($(top.document).find("#container").width() - 5)
</script>

</body>
</html>