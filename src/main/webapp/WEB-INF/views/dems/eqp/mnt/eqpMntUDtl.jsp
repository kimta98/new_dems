<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비관리 > 유지보수관리 수정
 * 4. 설명 : 장비관리 > 유지보수관리 수정
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var PW_Error = 0;
var ID_Duple = 1;
var tabId;
//alert("=====flag444444=========>>>"+$("#flag").val());

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');	
	
	<%/* 파일업로드 세팅 */%>
    gfn_fileUpload("atchFile", "fileList4","file", 4);
	
	var codeInfo = [{cdId:'C12',selectId:'cntrForm',type:'1', callbackNm:'fn_ajaxCntrFormCallback', sqlQueryId:''}];
	// fn_ajaxCodeList(codeInfo);
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});
	
	<%/* 달력 세팅 */%> 
	gfn_calendarConfig("bizStartDt", "bizEndDt", "minDate", "");    <%/* 사업시작일 from */%>
	gfn_calendarConfig("bizEndDt", "bizStartDt", "maxDate", "");   <%/* 사업종료일 to */%>
	
	gfn_toNumber("cntrAmt"); <%/* 계약금액 */%>
	
	
	fn_dispCont();
	
	
// 	$("#addRow").on("click", function(event) {
	
	
	
	
	//장비 상세 정보 조회
	requestUtil.search({callUrl:"<c:url value='/eqp/mgmt/queryEqpMgmtUDtl.do'/>", srhFormNm:'insForm', callbackNm:'fn_queryEqpMgmtCallBack'}); 
	
	
	<%/* 전체 checkbox 클릭시 */%>
    $('#chkAll').change(function() {
        var chk = $(this).is(':checked');
        if(chk){
               $('input:checkbox[id=chk]').each(function() {
                   $(this).prop("checked", true);         
               });
        }else{
               $('input:checkbox[id=chk]').each(function() {
                   $(this).prop("checked", false);         
               });
        }
    });
    
    //fn_addRow();
    
    
    
});

function fn_ajaxCntrFormCallback(data){
	$('#cntrForm option:eq(0)').before("<option value='' selected>선택</option");
}

/**
* 장비 상세 정보 조회 콜백
* @param  
* @returns
*/
function fn_queryEqpMgmtCallBack(data){
	
	var eqpSno = data.resultMap.eqpSno;	
	var eqpNm = data.resultMap.eqpNm;
	var eqpTypNm = data.resultMap.eqpTypNm;
	var srNo = gfn_nullRtnSpace(data.resultMap.srNo);
	var deprPrid = gfn_nullRtnSpace(data.resultMap.deprPrid);
	var purcDt = gfn_dashDate2(gfn_nullRtnSpace(data.resultMap.purcDt),'-');
	var exprDt = gfn_dashDate2(gfn_nullRtnSpace(data.resultMap.exprDt),'-');
	var mnftCo = gfn_nullRtnSpace(data.resultMap.mnftCo);
	
	$("#eqpSno").text(eqpSno); 		<%/* 장비번호 */%>
	$("#eqpNm").text(eqpNm); 		<%/* 장비명 */%>
	$("#eqpTypNm").text(eqpTypNm); 	<%/* 장비유형 */%>
	$("#srNo").text(srNo); 			<%/* S/N */%>
	
	$("#deprPrid").text(deprPrid); 	<%/* 내용연수 */%>
	$("#purcDt").text(purcDt); 		<%/* 도입일 */%>
	$("#exprDt").text(exprDt); 		<%/* 만료일 */%>
	$("#mnftCo").text(mnftCo); 		<%/* 제조사 */%>
	
	fn_searchMntList(1);
}

/**
 * @!@ 장비 지원 관리 > 유지보수 상세내역 조회
 * @param {int} page
 * @returns 
 */
function fn_searchMntList(page){
// 	debugger;
	var callUrl = "<c:url value='/eqp/mnt/queryEqpMntMList.do'/>";
	
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'insForm', callbackNm:'fn_searchMntListCallBack', page:page, perPageNum:1000});
	
// 	var callUrl = "<c:url value='/fbbs/ntc/queryFBbsNtcMList.do'/>";	
// 	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callback',page:page,perPageNum:10});
	
}


 /**
 * 유지보수 상세내역 콜백
 * @param  
 * @returns
 */
 function fn_searchMntListCallBack(data){ 
	
	//debugger;
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#tbMntDtl > colgroup").find("col").length;
	
	$("#tbMntDtl > tbody").empty();
	
//  	$("#totalcnt").text(data.totalCount);
	
  	if(listCnt < 1){
  		var append = "";
		append += "<tr>";
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		append += "</tr>";
		$("#tbMntDtl > tbody").append(append);
	}else{
	 	
		$.each(list,function(idx,row){
	 		
			console.log("========idx zzzzzzzzzzzzzzzzzzz=====>>>"+idx);
			var append = "";
	 		append += "<tr>";
			
	 		append += '<td style="text-align:center"><input type="checkbox" name="chk" id="chk" value="Y" class="check_agree1"></td>';
	 		//append += '<td><textarea id="box" name="dfecCnts" rows=2 cols=30 maxlength="2000">' + row.dfecCnts + '</textarea></td>';
	 		
	 		append += '<td>';
 	 		append +=  "<textarea name=\"dfecCnts\" id=\"dfecCnts\" rows=\"2\" cols=\"30\"  maxlength=\"2000\" data-requireNm=\"결함내용\"	data-maxLength=\"4000\"	title=\"결함내용\">"+row.dfecCnts+"</textarea>";
 	 		append += '</td>';
	 		append += '<td>';
 	 		append +=  "<textarea name=\"rprCnts\" id=\"rprCnts\"  rows=\"2\" cols=\"30\"  maxlength=\"200\" data-requireNm=\"수리내용\"	data-maxLength=\"400\"	title=\"수리내용\">"+row.rprCnts+"</textarea>";
 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  "<input type=\"text\" name=\"rprStartDt\" id=\"rprStartDt_"+idx+"\" value=\""+row.rprStartDt+"\" maxlength=\"6\" class=\"inpw80\" data-requireNm=\"수리시작일\"	data-maxLength=\"8\"	title=\"수리시작일\"/>";
 	 		append += '</td>';
 	 		append += '<td >';
 	 		append +=  "<input type=\"text\" name=\"rprEndDt\" id=\"rprEndDt_"+idx+"\" value=\""+row.rprEndDt+"\" maxlength=\"6\" class=\"inpw80\" data-requireNm=\"수리종료일\"	data-maxLength=\"8\"	title=\"수리종료일\"/>";
 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  "<input type=\"text\" name=\"rprCoNm\"  id=\"rprCoNm\" value=\""+row.rprCoNm+"\" maxlength=\"50\" class=\"inpw100\" data-requireNm=\"수리업체명\"	data-maxLength=\"100\"	title=\"수리업체명\"/>";
 	 		append += '</td>';
 	 		append += '<td>';
 	 		append +=  "<input type=\"text\" name=\"coCrgr\"  id=\"coCrgr\" value=\""+row.coCrgr+"\" maxlength=\"50\" class=\"inpw100\" data-requireNm=\"업체담당자\"	data-maxLength=\"100\"	title=\"업체담당자\"/>";
 	 		append += '</td>';
 	 		

	 		
	 		append += "</tr>";
	 		console.log("========idx append=====>>>"+append);
			//var append = "";
	        $("#tbMntDtl > tbody").append(append);
	  		
	  		gfn_calendarConfig("rprStartDt_"+idx, "", "", "");
	  		gfn_calendarConfig("rprEndDt_"+idx, "", "", "");
	  	});
	}
  	
//    	data.__callFuncName__ ="fn_searchList";
//  	data.__naviID__ ="page_navi";
//  	pageUtil.setPageNavi(data);
 }	 
/**
* 첨부파일갯수 체크
* @param  
* @returns
*/
function fn_fileCntChk(){
    var numCnt=0;
    $('#divFile :input[id^=fileList4FileSno]').each(function(index) {
        numCnt++;
    });

    if(numCnt > 4){
        $("#atchFile").hide();
    }else{
        $("#atchFile").show();
    }
}

/**
 * 자유게시판 파일다운로드
 * @param {String} filePath 파일경로
 * @param {String} fileNm   파일이름
 * @param {String} atchFileNm 실제파일이름
 * @returns
 */
function fn_fileDown(filePath,fileNm,atchFileNm) {
    gfn_fileNmDownload(filePath,fileNm,encodeURI(atchFileNm));
}

function fn_prgdetail() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"frame/fsys/menu/fsysMenuSchPrgmPop",
		height: 700,
        width: 1000,
        title: '프로그램 목록',
        divId : 'divPrgPopup'
	});

}

//장비 등록
function fn_modifyEqpMnt(){

	//if(!fn_saveCntChk()) return;
	console.log("==========aaaaaaaaaa======>>>");
	/* if(!fn_valChk()) return; */
	console.log("==========bbbbbbbbbb======>>>");
	//console.log("==========111111111111======>>>");
	//장비상세정보
	var tpFormArry = [];
	var $formDatas = $('#insForm').find('#tbMgmtDtl');
	//console.log("==========2222222222222======>>>");
	
	var formObj = new Object();
	var cnt = 0;
	
	$formDatas.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			formObj[item.name] = item.value;
			cnt++;
		});
		
		dataObj = {};

	});
	
	//alert("======cnt======>>>"+cnt);
	//tbMgmtDtl
	//tbMntDtl
	
	//유지보수 상세정보
	var tpArray = [];
	var dataObj = new Object();
	var cnt2 = 0;

	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input, select, textarea');
		//console.log("==========detailTbody	 index======>>>"+index);

		$el.each(function(index,item){
			dataObj[item.name] = item.value;
			//console.log("==========item.name======>>>"+item.name+"\n==========item.value======>>>"+item.value);
			cnt2++;
		});

// 		if(index % 2 == 0){
			tpArray.push(dataObj);
			dataObj = {};
// 		}

	});
	
	//alert("======cnt2======>>>"+cnt2);
	fn_showModalPage("저장 하시겠습니까?", function() {
		var data = {rowDatas : tpArray, formDatas : formObj};
	    var callUrl = "<c:url value='/eqp/mnt/regEqpMntRDtl.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyEqpMntCallBack'});
	});
	
	
}

function fn_modifyEqpMntCallBack(data){
	fn_searchList();
	//alert("등록 성공");
}

//이메일 체크
function emailCheck(email) {
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
    if ( !email.match(regExp) ) {	return false;    } else {		return true;    }
}

//비밀번호 비교
function fn_saveComparePW(){
	var pw = $('input[id=pwd]').val();
	var pw2 = $('input[id=pwdConfirm]').val();
	if(pw != pw2)
	{
		$("#comparePw").attr("style","visibility: visible;");
		PW_Error = 1;	
	}
	else
	{
		$("#comparePw").attr("style","visibility: hidden;");
		PW_Error = 0;	
	}
}


// /fsys/user/indexFsysUserMList.do

function fn_searchList(){
	//debugger;
	
	//parent.addNaviTab('관리자 > 사용자관리 > 급식기관관리상세',"<c:url value="/eqp/mnt/indexEqpMntMList.do"/>",'급식기관관리상세','M000000703');
	var flag = $("#flag").val();
	//alert("====flag=======>>>"+flag);
	if(flag == "MgmtList"){
		//parent.addNaviTab('관리자 > 사용자관리 > 급식기관관리상세',"<c:url value="/eqp/mgmt/indexEqpMgmtMList.do"/>",'급식기관관리상세','M000000701');
// 		parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mgmt/indexEqpMgmtMList.do"/>');
		parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/mgmt/indexEqpMgmtMList.do"/>');
		
	}else{
// 		parent.$('#tabs-M000000703').find("iframe").attr("src", '<c:url value="/eqp/mnt/indexEqpMntMList.do"/>');
		parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/mnt/indexEqpMntMList.do"/>');
	}
	//parent.$('#tabs-M000000703').find("iframe").attr("src", '<c:url value="/eqp/mnt/indexEqpMntMList.do"/>');/eqp/mgmt/indexEqpMgmtMList.do

	
}


/**
 * @ 메뉴 관리 등록 콜백
 * @param {json} data
 * @returns 
 */
function fn_insEqpMgmtCallback(data){
	 fn_searchList();
}

<%/*아이디 중복 체크*/%>
function fn_IDcheck(){
	
	var checkParamId = $("#userId").val(); 
// 	alert(checkParamId.search(/\s/));
	
	if( checkParamId.search(/\s/) > 0 ){
		alert('ID에 공백이 들어갈 수 없습니다.');
		$("#userId").focus();
		return;
	} 
	
	if(checkParamId == ""){
		alert("아이디를 입력해 주세요.");
		$("#userId").focus();
		return;
	}else{
		var callUrl = "<c:url value='/fsys/user/sysUserCheckId.do'/>";
		requestUtil.search({callUrl:callUrl,srhFormNm:'insForm',setFormNm:'insForm',callbackNm:'fn_callbackpop'});
	}
	
}

function fn_callbackpop(data) {
	
	var resultCnt = data.egovMap.resultCnt;
// 	alert("fn_callbackpop 진입 resultCnt===>>> "+resultCnt);
// 	if(resultCnt > 0){
// 		alert("111111");
// 	}else{
// 		alert("2222222");
// 	}
// 	var existsCnt = resultCnt.substring(0,resultCnt.indexOf('-'));
// 	alert("fn_callbackpop 진입 1-1");
	if( resultCnt > 0 ) {
		alert('이미 존재하는 ID입니다.');
		$('#userId').val('');
		ID_Duple=1;
		return;
	} else {
		alert('사용가능합니다.');
		ID_Duple =0;
		return;
	}
	
}

/**
 * @ 장비 관리 삭제
 * @param
 * @returns 
 */
function fn_deleteEqpMgmt() {
	
	if(confirm("삭제 하시겠습니까?")){
		
		//사용자 정보 수정
        var callUrl = "<c:url value='/eqp/biz/delEqpBizUDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_deleteEqpBizCallback'});
	
	}
	
}

/**
 * @!@ 메뉴 관리 삭제 콜백
 * @param
 * @returns 
 */
function fn_deleteEqpBizCallback(data){
	fn_searchList();
}

/**
 * @!@ 사업관리조회 팝업
 * @param cd
 * @returns 
 */
function fn_searchEqpBizMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/mgmt/eqpMgmtFIndEqupQListPop&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 650,
        width: 1000,
        title: '장비조회 팝업',
        divId : 'eqpMgmtFIndEqupQListPop'
        //divId : 'eqpMgmtMListPop'
	});

}

<%/* 시작 처리 function */%>
function fn_addRow() {
// 	$("#addRow").click(function(){
		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length+1;

		var tpTag =     '<tr>'+
			'<td style="text-align:center"><input type="checkbox" name="chk" id="chk" value="Y" class="check_agree1"></td>'+
			'<td><textarea id="dfecCnts" name="dfecCnts" rows=2 cols=30 maxlength="2000" data-requireNm="결함내용"	data-maxLength="4000"	title="결함내용"></textarea></td>'+
			'<td><textarea id="rprCnts" name="rprCnts" rows=2 cols=30 maxlength="200" data-requireNm="수리내용"	data-maxLength="400"	title="수리내용"></textarea></td>'+
			'<td "text-align:center"><input type="text" name="rprStartDt" id="rprStartDt_'+idx+'" class="inpw80" data-requireNm="수리시작일"	data-maxLength="8"	title="수리시작일"></td>'+
			'<td "text-align:center"><input type="text" name="rprEndDt" id="rprEndDt_'+idx+'" class="inpw80" data-requireNm=""	data-maxLength="8"	title="수리종료일"></td>'+
			'<td "text-align:center"><input type="text" name="rprCoNm" id="rprCoNm" class="inpw100" maxlength="50" data-requireNm="수리업체명"	data-maxLength="100"	title="수리업체명"></td>'+
			'<td "text-align:center"><input type="text" name="coCrgr" id="coCrgr" class="inpw100" maxlength="50" data-requireNm="업체담당자"	data-maxLength="100"	title="업체담당자"></td>'+
		'</tr>';
		$("#detailTbody").append(tpTag);

		
		gfn_calendarConfig("rprStartDt_"+idx, "", "", "");
		gfn_calendarConfig("rprEndDt_"+idx, "", "", "");
// 	});
		
}

<%/* 삭제버튼 클릭 function */%>
function fn_delRow() {
	var cnt = 0;
	$('#detailTbody > tr').each(function(index,item){
		var $chkbox = $(item).find('input[type=checkbox]');
		$chkbox.each(function(index,item){

			if($(this).is(':checked') == true){
				cnt++;
				//$(this).parent().parent('tr').next().remove();
				$(this).parent().parent('tr').remove();
			}
		})

	});

	if(cnt < 1){
		//alert('선택해주세요');
		fn_showUserPage( "삭제할 내역을 선택하십시요.", function() {
			return;		
        });
	}


//		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length/2;
	

//		for(var i = 1; i <= idx; i++){
//			if(i == 1){
//				$('#detailTbody > tr').eq(0).find('td:eq(1)').text(i);
//				$('#detailTbody > tr').eq(0).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
//				$('#detailTbody > tr').eq(0).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
//				$('#detailTbody > tr').eq(1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
//			}else{
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(1)').text(i);
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
//				$('#detailTbody > tr').eq(i*2-1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
//			}

//		}
//		fn_makeSbx(idx);
//		gfn_calendarConfig("cfscDate_"+idx, "", "", "");
		
}

<%/* 필수입력체크 */%>
function fn_valChk(){
	
	var chkCnt = 0;
	var isValid = true;
	var aaa;
	
// 	if(!gfn_isNull(aaa)){
// 		fn_showUserPage( "babo", function() {
			
// 		});
// 		return false;
// 	}
// 	return true;
	
	$('#tbMntDtl :input[name=chk]').each(function(index) {
		// 체크여부 확인
// 		if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
			
			if($("textarea[name=dfecCnts]").eq(index).val() ==  ""){
 				fn_showUserPage( "결함내용은(는) 필수 입력 항목입니다.", function() {
 					$("textarea[name=dfecCnts]").eq(index).focus();
 			    });
 				isValid = false;
 				return false;
 			}
			
			if($("textarea[name=rprCnts]").eq(index).val() == ""){
 				fn_showUserPage( "수리내용은(는) 필수 입력 항목입니다.", function() {
 					$("textarea[name=rprCnts]").eq(index).focus();
 	            });
 				isValid = false;
 				return false;
 			}

 	
			if($("input[name^=rprStartDt]").eq(index).val() == ""){
				fn_showUserPage( "수리시작일은(는) 필수 입력 항목입니다.", function() {
					$("input[name^=rprStartDt]").eq(index).focus();
                });
				isValid = false;
 				return false;
			}
			
			if($("input[name^=rprEndDt]").eq(index).val() == ""){
				fn_showUserPage( "수리종료일은(는) 필수 입력 항목입니다.", function() {
					$("input[name^=rprEndDt]").eq(index).focus();
                });
				isValid = false;
 				return false;
			}
			
			if($("input[name^=rprCoNm]").eq(index).val() == ""){
				fn_showUserPage( "수리업체명은(는) 필수 입력 항목입니다.", function() {
					$("input[name^=rprCoNm]").eq(index).focus();
                });
				isValid = false;
 				return false;
			}
			
			if($("input[name^=coCrgr]").eq(index).val() == ""){
				fn_showUserPage( "업체담당자명은(는) 필수 입력 항목입니다.", function() {
					$("input[name^=coCrgr]").eq(index).focus();
                });
				isValid = false;
 				return false;
			}
			return isValid;
// 		}
	});
	
	return isValid;

}
	
function fn_saveCntChk(){
	var chkCnt = 0;
	$('#tbMntDtl :input[name=chk]').each(function(index) {
		// 체크여부 확인
		//if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
			chkCnt++;
		//}
	});
	
	if(chkCnt<=0){
		fn_showUserPage( "저장할 내역을 입력하세요.", function() {
			return false;		
        });
	}else{
		//alert("======chkCnt====>>>"+chkCnt);
		return true;
	}
       
}

 /**
 * 화면 컨트롤
 * @param  
 * @returns
 */
 function fn_dispCont(){
 	
	 console.log("========사용자구분 session_usergb=====>>>"+session_usergb);
	 //console.log("========대여진행상태2 pgsStat2=====>>>"+pgsStat);
	
	$("#btn_insMnt").hide(); //등록
	
	//사용자구분 (C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)   
	if(session_usergb == "C01003"){ //수사관
		$("#btn_insMnt").show(); //저장
	}
 	
 }

</script>

</head>

<body>
<div id="con_wrap1">
        <div class="content">
           <!----현재위치----->
             
            <div id="contents_info">
                 <div class="sub_ttl">장비 유지보수 수정</div><!-----타이틀------>
                 
                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">
                     
                     
					 <input type="hidden" id="atchFileId" name="atchFileId" value="<c:out value="${param.atchFileId}" />" />
					 <input type="hidden" id="flag" name="flag" value="<c:out value="${param.flag}" />" />
					 
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:80px;">
						<table class="iptTblX2" id="tbMgmtDtl">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="12%" />
								<col />
								<col width="12%" />
								<col width="12%" />
								<col width="12%" />
								<col width="12%" />
								<col width="12%" />
								<col width="12%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">장비번호</th>
									<th scope="col">장비명</th>
									<th scope="col">장비유형</th>
									<th scope="col">S/N</th>
									<th scope="col">내용연수</th>
									<th scope="col">도입일</th>
									<th scope="col">만료일</th>
									<th scope="col">제조사</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align:center">
										<span id="eqpSno"></span>
										<input type="hidden" id="eqpSno" name="eqpSno" value="<c:out value="${param.eqpSno}" />" />
										<input type="hidden" id="modFlag" name="modFlag" value="modify" />
									</td>
									<td style="text-align:center">
										<span id="eqpNm"></span>
									</td>
									<td style="text-align:center">
										<span id="eqpTypNm"></span>
									</td>
									<td style="text-align:center">
										<span id="srNo"></span>
									</td>
									<td style="text-align:center">
										<span id="deprPrid"></span>
									</td>
									<td style="text-align:center">
										<span id="purcDt"></span>
									</td>
									<td style="text-align:center">
										<span id="exprDt"></span>
									</td>
									<td style="text-align:center">
										<span id="mnftCo"></span>
									</td>
									
								</tr>
							</tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
					
					<div class="flR"><button class="buttonR60" name="addRow" id="addRow" onclick="fn_addRow();return false;">+ 추가</button><button class="buttonG60" name="delRow" id="delRow" onclick="fn_delRow();return false;">- 삭제</button></div>
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:500px;">
						<table class="iptTblX2" id="tbMntDtl">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="5%" />
								<col width="23%" />
								<col />
								<col width="12%" />
								<col width="12%" />
								<col width="12%" />
								<col width="12%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col"><input type="checkbox" name="chkAll" id="chkAll" ></th>
									<th scope="col">결함내용<span class="fontred">*</span></th>
									<th scope="col">수리내용<span class="fontred">*</span></th>
									<th scope="col">수리시작일<span class="fontred">*</span></th>
									<th scope="col">수리종료일<span class="fontred">*</span></th>
									<th scope="col">수리업체명<span class="fontred">*</span></th>
									<th scope="col">업체담당자<span class="fontred">*</span></th>
								</tr>
							</thead>
							<tbody id="detailTbody">
							</tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
                      
					</form>
                    <div class="btn_c">
                      <ul>
                        <li><a href="#" class="RdButton" onclick="fn_modifyEqpMnt();return false;" id="btn_insMnt" name="btn_insMnt" style="display: none;">저장</a></li>
<!--                         <li><a href="#" class="myButton" onclick="fn_deleteEqpMgmt();return false;">삭제</a></li> -->
                        <!-- <li><a href="#" class="myButton">재입력</a></li> -->
                        <li><a href="#" class="myButton" onclick="fn_searchList(1);return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->   
                       
                    
                          
                  </div>
            </div>
        
        </div>
 </div>
<div id="divPrgPopup"></div>
</body>
</html>