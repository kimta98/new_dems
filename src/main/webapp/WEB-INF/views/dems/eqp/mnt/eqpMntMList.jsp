<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비 지원 관리 > 유지보수관리 조회
 * 4. 설명 : 장비 지원 관리 > 유지보수관리 조회
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript">
// var flag = true;
// var perPageNum;
 var tabId;

$(document).ready(function() {
	
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	gfn_calendarConfig("schStRprStartDt", "schEdRprStartDt", "minDate", "");    <%/* 수리시작일 from */%>
	gfn_calendarConfig("schEdRprStartDt", "schStRprStartDt", "maxDate", "");   <%/* 수리시작일 to */%>
	
	gfn_calendarConfig("schStRprEndDt", "schEdRprEndDt", "minDate", "");    <%/* 수리완료일 from */%>
	gfn_calendarConfig("schEdRprEndDt", "schStRprEndDt", "maxDate", "");   <%/* 수리완료일 to */%>

	gfn_init({startFnNm:'fn_searchList', param:1, codeSet:"N"});
});


function fn_ajaxEqpTypCallback(data){
// 	$('#schEqpTyp option:eq(0)').before("<option value='' selected>전체</option");
}

/**
 * 달력 콜백함수(없으면 삭제가능) 
 * @param {string} prgID
 * @returns 
 */
function fn_callBack(){
}


/**
 * @!@ 
 * @param {string} prgID
 * @returns 
 */
function fn_queryCodeList(param) {
	$.each(param.paramRow,function(idx,row){
        $("#"+row.target).append("<option value=''>전체</option>");
    });
	
	$.ajax({
		url:  "<c:url value='/fcom/queryAjaxCodeList.do'/>",
		type : "POST",
		data: JSON.stringify(param),
		dataType: 'json',
		contentType:"application/json",
		success: function(data) {
		    $.each(data, function(idx, dataRow) {
		    	$.each(dataRow, function(idx, rowRow) {
			    	$("#"+rowRow.target).append("<option value='"+rowRow.cd+"'>"+rowRow.cdNm+"</option>");
			    });
		    });
		    
		}
	});
}


/**
 * @!@ 장비 지원 관리 > 유지보수관리 조회
 * @param {int} page
 * @returns 
 */
function fn_searchList(page){
// 	debugger;
	var callUrl = "<c:url value='/eqp/mnt/queryEqpMntMList.do'/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_searchListCallback', page:page, perPageNum:10});
	
// 	var callUrl = "<c:url value='/fbbs/ntc/queryFBbsNtcMList.do'/>";	
// 	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callback',page:page,perPageNum:10});
	
}

 /**
  * 장비 지원 관리 > 유지보수관리 조회 콜백
  * @param {json} data
  * @returns 
  */
 function fn_searchListCallback(data){
//  	debugger;
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;
	
	$("#listTab > tbody").empty();
	
 	$("#totalcnt").text(data.totalCount);
	
  	if(listCnt < 1){
  		var append = "";
		append += "<tr>";
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
	 	
		$.each(list,function(idx,row){
	 		var append = "";
	 		append += "<tr>";
			
// 	 		append += "<td>" + row.rnum + "</td>";
	 		append += "<td>" + row.eqpSno + "</td>";
	 		append += "<td><a href='javascript:void(0)' onclick=javascript:fn_searchDetail('"+row.eqpSno+"')><u>"+row.eqpNm+"</u></a></td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.skipDfecCnts) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.skipRprCnts) + "</td>";
	 		append += "<td>" + gfn_dashDate2(gfn_nullRtnSpace(row.rprStartDt),'-') + "</td>";
	 		append += "<td>" + gfn_dashDate2(gfn_nullRtnSpace(row.rprEndDt),'-') + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.rprCoNm) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.coCrgr) + "</td>";
	 		
	 		append += "</tr>";
	        $("#listTab > tbody").append(append);
	  	});
	}
  	
  	data.__callFuncName__ ="fn_searchList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
 	
 	
 	
 	
 } 
 
function fn_insEqpBiz(){
	
	parent.$('#tabs-M000000704').find("iframe").attr("src", '<c:url value="/eqp/biz/indexEqpBizRDtl.do"/>');
}


function fn_searchDetail(eqpSno, atchFileId){
// debugger;
	var flag = "MntList";
	//parent.$('#tabs-M000000703').find("iframe").attr("src", '<c:url value="/eqp/mnt/indexEqpMntUDtl.do"/>?eqpSno='+eqpSno+'&flag='+flag);
	requestUtil.setSearchForm("searchForm");
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/mnt/indexEqpMntUDtl.do"/>?eqpSno='+eqpSno+'&flag='+flag);
	
	//parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mnt/indexEqpMntRDtl.do"/>?flag='+flag);
	 
}

</script>
</head>
<body>

<div id="con_wrap">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div>
            <!-- 
                  <div><h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3></div>
             -->
                 <div class="loca">
                  <!--  <h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3>//-->
                    <div class="ttl">유지 보수 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></div>
                    <div class="loca_list">Home > 장비 지원 관리 > 유지 보수 관리</div>
                  </div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
<!--                     <form id="searchForm" name="searchForm" method="post"> -->
					 <form id="searchForm" name="searchForm" onsubmit="return false;">	
						<input type="hidden" class="" id="page" name="page" value="1"/>
                    <div class="t_head">
				        <input type="hidden" id="boardKind" class="b_put"  name="boardKind"   value="C23001"/>
				        <input type="hidden" id="userGb"   name="userGb"   value="C00000"/>
				        
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_searchList(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="11%">
                                   <col width="22%">
                                   <col width="11%">
                                   <col width="23%">
                                   <col width="11%">
                                   <col width="22%">
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">장비명</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schEqpNm" id="schEqpNm" style="width:300px;" maxlength="100"/>
					            </td>
					            <th scope="col" class="hcolor">결함내용</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schDfecCnts" id="schDfecCnts" style="width:300px;" maxlength="100"/>
					            </td>
					            <th scope="col" class="hcolor">수리내용</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schRprCnts" id="schRprCnts" style="width:300px;" maxlength="100"/>
					            </td>
					            
					          </tr>
					          <tr>
					            <th scope="col" class="hcolor">수리시작일</th>
					            <td scope="col" >
					               <input id="schStRprStartDt" name="schStRprStartDt"  title="수리시작일(from)"  type="text" value="" style='width:100px' maxlength="10"/> 
					               <input id="schEdRprStartDt" name="schEdRprStartDt"  title="수리시작일(to)"  type="text" value="" style='width:100px' maxlength="10"/>
					            </td>
					          	<th scope="col" class="hcolor">수리완료일</th>
					          	<td scope="col" >
					               <input id="schStRprEndDt" name="schStRprEndDt"  title="수리완료일(from)"  type="text" value="" style='width:100px' maxlength="10"/> 
					               <input id="schEdRprEndDt" name="schEdRprEndDt"  title="수리완료일(to)"  type="text" value="" style='width:100px' maxlength="10"/>
					            </td>
					            <th scope="col" class="hcolor">업체명</th>
					          	<td scope="col" >
					               <input class="b_put" type="text" name="schRprCoNm" id="schRprCoNm" style="width:300px;" maxlength="50"/>
					            </td>
					          </tr>    
                           </thead>
                        </table>
                      
                      </div>
                      <div  class="btn_c">
					       <ul>
                             
<%--                                <c:if test="${loginVO.userGb == 'C01999'}"> --%>
<!-- 							 <li><a href="javascript:void(0);" class='myButton' onclick="fn_insEqpBiz(); return false;">등록</a></li> -->
                             <li><a href="javascript:void(0);" class='gyButton' onclick="fn_searchList(1); return false;">조회</a></li>
<!--                              <li><a href="javascript:void(0);" class='RdButton' onclick="fn_insAnlsReq(); return false;">등록</a></li> -->
<%--                                </c:if> --%>
                             </ul>   
					  </div>
                     </form>
                       
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
<!--                      <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div> -->
<!--                      <div class="bo_num"> -->
<!--                          <select id="perPageNum" name="perPageNum"> -->
<!-- 			               <option value="5">5개씩</option> -->
<!-- 			               <option value="10" selected="selected">10개씩</option>		                -->
<!-- 			             </select> -->
<!--                      </div> -->
					 <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>
                     
                     <!--------------목록---------------------->
                     <div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:450px;">  
                          <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                <caption>사업관리</caption>
                                  <colgroup>
<%--                                       <col width="7%">	<%/* 순번 */%> --%>
                                      <col width="9%">	<%/* 장비일련번호 */%>
                                      <col />		  	<%/* 장비명 */%> 
                                      <col width="16%">	<%/* 결함내용 */%>
                                      <col width="19%">	<%/* 수리내용 */%>
                                      <col width="9%">	<%/* 수리시작일 */%>
                                      <col width="9%">	<%/* 수리완료일 */%>
                                      <col width="10%">	<%/* 수리업체명 */%>
                                      <col width="8%">	<%/* 업체담당자명 */%>
                                      </colgroup>
                                    <thead>
                                      <tr>
<!--                                          <th scope="col">순번</th> -->
                                         <th scope="col">장비일련번호</th>
                                         <th scope="col">장비명</th>
                                         <th scope="col">결함내용</th>
                                         <th scope="col">수리내용</th>
                                         <th scope="col">수리시작일</th>
                                         <th scope="col">수리완료일</th>
                                         <th scope="col">수리업체명</th>
                                         <th scope="col">업체담당자명</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                    <div id="page_navi" class="page_wrap"></div>                   
                       <!-----------------------//페이징----------------------->
                 
                  </div>
                 
            </div>
    </div>
</div>

</body>
</html>