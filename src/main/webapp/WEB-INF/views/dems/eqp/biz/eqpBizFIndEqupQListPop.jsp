<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : ilyong
 * 3. 화면명 : 사업관리 조회 팝업
 * 4. 설명 : 사업관리 조회 팝업
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>공공급식 식단관리 시스템</title>

<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
	fn_queryMList(1);	
});

/**
 * @!@ 프로그램 관리 리스트 조회
 * @param {int} page
 * @returns 
 */
function fn_queryMList(page){
	
	var callUrl = "<c:url value='/eqp/biz/queryEqpBizFIndEqupQListPop.do'/>";
	
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_queryMListCallback', page:page, perPageNum:10});
	
}

/**
 * @!@ 프로그램 관리 리스트 조회 콜백
 * @param {json} data
 * @returns 
 */
function fn_queryMListCallback(data){
	
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;
	
	$("#listTab > tbody").empty();
	
	if(listCnt == 0){
		var append = "";
		append += "<tr>";
		
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		
		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
		$.each(list,function(idx,row){
			var append = "";
			append += "<tr>";
	
			append += "<td>" + row.rnum + "</td>";
	 		append += "<td>" + row.bizSno + "</td>";
	 		append += "<td><a href='javascript:void(0)' onclick=javascript:fn_searchDetail('"+row.bizSno+"')><u>"+row.bizNm+"</u></a></td>";
	 		append += "<td>" + gfn_dashDate2(gfn_nullRtnSpace(row.bizStartDt),'-') + "</td>";
	 		append += "<td>" + gfn_dashDate2(gfn_nullRtnSpace(row.bizEndDt),'-') + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.coNm) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.coCrgr) + "</td>";
	 		append += "<td><a href='javascript:fn_setBizSno(\""+row.bizSno+"\", \""+row.bizNm+"\");' class='byButton'>선택</a></td>";
// 	 		append += "<td>" + "선택" + "</td>";
	
			append += "</tr>";
	        $("#listTab > tbody").append(append);
	 	});
	}
	
	
	data.__callFuncName__ ="fn_queryMList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
	
	
}

 /**
  * 사업 선택
  * @param
  * @returns 
  */
 function fn_setBizSno(bizSno, bizNm){

 	 $("#bizSno").val(bizSno);
 	 $("#bizNm").val(bizNm);
	 fn_dialogClose('eqpBizFIndEqupQListPop');
 	
 }
</script>

</head>
<body>
<div id="con_wrap_pop">
	<div class="contents">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                      <div class="window_popup">
                          <div class="sub_ttl">사업 찾기</div>
                         
                          <div class="sub">
                             <!--------------검색------------------>
                             <form name="searchForm" id="searchForm" method="post">
                             <input type="hidden" id="srcUseYn" name="srcUseYn" value="Y"/>
                            <div class="t_head">
                                  <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_queryMList(1);">
                                        <caption>검색</caption>
                                         <colgroup>
                                            <col width="20%">
                                            <col width="30%">
                                            <col width="20%">
                                            <col width="30%">
                                         </colgroup>
                                   <thead>
                                      <tr>
                                           <th scope="row" class="hcolor">사업명</th>
								           <td>
								               <input type="text" id="schBizNm" name="schBizNm" title="사업명" style="width:220px;" maxlength="100"/>
								           </td>
								           <th scope="row" class="hcolor">계약번호</th>
								           <td colspan="3">
								           		<input type="text" id="schcntrNo" name="schcntrNo" title="계약번호" style="width:220px;" maxlength="50"/>
								           </td>
                                       </tr>
                                   </thead>
                                </table>
                              </div>
                            <div class="btn_c">
                                  <ul>
                                     <li><a href="javascript:fn_queryMList(1);" class="myButton">조회</a></li>
                                  </ul>
                               </div>
                               </form>
                            <!--------------//검색------------------>
                            
                            <!--------------결과------------------>
                             <div class="r_num">| 결과  <strong style="color:#C00">40</strong>건</div>
                             
                             <!--------------목록---------------------->
                             <div class="t_list">  
                                  <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="7%">
                                              <col width="13%">
                                              <col />
                                              <col width="16%">
                                              <col width="11%">
                                              <col width="11%">
                                              <col width="12%">
                                              <col width="11%">
                                           </colgroup>
                                            <thead>
                                              <tr>
                                                 <th scope="col">순번</th>
                                                 <th scope="col">사업일련번호</th>
                                                 <th scope="col">사업명</th>
                                                 <th scope="col">사업시작일</th>
                                                 <th scope="col">사업종료일</th>
                                                 <th scope="col">업체명</th>
                                                 <th scope="col">담당자명</th>
                                                 <th scope="col">선택</th>
                                              </tr>
                                            </thead>
                                            <tbody>
												<tr><td colspan="7">조회 결과가 없습니다.</td></tr>
                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->
                             
                             <!-----------------------페이징----------------------->
                             <div id="page_navi" class="page_wrap"></div>
                               <!-----------------------//페이징----------------------->
                          
                          		<div class="btn_c">
			                      <ul>
			                        <li><a href="#" class="myButton" onclick="fn_dialogClose('eqpBizFIndEqupQListPop');return false;">닫기</a></li>
			                        <!-- <li><a href="#" class="myButton" onclick="fn_indexFsysProgramMList();return false;">목록</a></li> -->
			                      </ul>
			                    </div>
                          </div>
                         
                    </div>
               </div>
                 <!---  //contnets  적용 ------>
       </div>
  </div>
</body>
</html>