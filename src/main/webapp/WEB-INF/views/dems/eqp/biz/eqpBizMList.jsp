<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비 지원 관리 > 사업관리 조회
 * 4. 설명 : 장비 지원 관리 > 사업관리 조회
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<%-- <link rel="stylesheet" href="<c:url value="/css/com/bootstrap.css" />"> --%>
<script type="text/javaScript" language="javascript">
// var flag = true;
// var perPageNum;
 var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');	
// 	var codeInfo2 = [{cdId:'C14',selectId:'schEqpTyp',type:'1', callbackNm:'fn_ajaxEqpTypCallback', sqlQueryId:''}];
// 	fn_ajaxCodeList(codeInfo2);
	
	gfn_calendarConfig("schStbizStartDt", "schEdbizStartDt", "minDate", "");    <%/* 사업시작일 from */%>
	gfn_calendarConfig("schEdbizStartDt", "schStbizStartDt", "maxDate", "");   <%/* 사업시작일 to */%>
	
	gfn_calendarConfig("schStbizEndDt", "schEdbizEndDt", "minDate", "");    <%/* 사업종료일 from */%>
	gfn_calendarConfig("schEdbizEndDt", "schStbizEndDt", "maxDate", "");   <%/* 사업종료일 to */%>
	
	fn_dispCont();
	
	gfn_init({startFnNm:'', param:{targetFormId:"searchForm", callbackNm:"fn_searchList"}, codeSet:"N"});
	
});


function fn_ajaxEqpTypCallback(data){
	$('#schEqpTyp option:eq(0)').before("<option value='' selected>전체</option");
}

/**
 * 달력 콜백함수(없으면 삭제가능) 
 * @param {string} prgID
 * @returns 
 */
function fn_callBack(){
}


/**
 * @!@ 
 * @param {string} prgID
 * @returns 
 */
function fn_queryCodeList(param) {
	$.each(param.paramRow,function(idx,row){
        $("#"+row.target).append("<option value=''>전체</option>");
    });
	
	$.ajax({
		url:  "<c:url value='/fcom/queryAjaxCodeList.do'/>",
		type : "POST",
		data: JSON.stringify(param),
		dataType: 'json',
		contentType:"application/json",
		success: function(data) {
		    $.each(data, function(idx, dataRow) {
		    	$.each(dataRow, function(idx, rowRow) {
			    	$("#"+rowRow.target).append("<option value='"+rowRow.cd+"'>"+rowRow.cdNm+"</option>");
			    });
		    });
		    
		}
	});
}


/**
 * @!@ 사용자 관리 리스트 조회
 * @param {int} page
 * @returns 
 */
function fn_searchList(page){
// 	debugger;
	var callUrl = "<c:url value='/eqp/biz/queryEqpBizMList.do'/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_searchListCallback', page:$("#page").val(), perPageNum:10});
	
// 	var callUrl = "<c:url value='/fbbs/ntc/queryFBbsNtcMList.do'/>";	
// 	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callback',page:page,perPageNum:10});
	
}

 /**
  * 장비관리 리스트 조회 콜백
  * @param {json} data
  * @returns 
  */
 function fn_searchListCallback(data){
//  	debugger;
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;
	
	$("#listTab > tbody").empty();
	
 	$("#totalcnt").text(data.totalCount);
	
  	if(listCnt < 1){
  		var append = "";
		append += "<tr>";
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
	 	
		$.each(list,function(idx,row){
	 		var append = "";
	 		append += "<tr>";
			
// 	 		append += "<td>" + row.rnum + "</td>";
	 		append += "<td>" + row.bizSno + "</td>";
	 		append += "<td><a href='javascript:void(0)' onclick=javascript:fn_searchDetail('"+row.bizSno+"','"+row.atchFileId+"')><u>"+row.bizSno+"</u></a></td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.cntrNo) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.cntrAmt) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.cntrFormNm) + "</td>";
	 		append += "<td>" + gfn_dashDate2(gfn_nullRtnSpace(row.bizStartDt),'-') + "</td>";
	 		append += "<td>" + gfn_dashDate2(gfn_nullRtnSpace(row.bizEndDt),'-') + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.coNm) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.coCrgr) + "</td>";
	 		
	 		append += "</tr>";
	        $("#listTab > tbody").append(append);
	  	});
	}
  	
  	data.__callFuncName__ ="fn_searchList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
 	
 	
 	
 	
 } 
 
function fn_insEqpBiz(){

	requestUtil.setSearchForm("searchForm");
	
// 	parent.$('#tabs-M000000704').find("iframe").attr("src", '<c:url value="/eqp/biz/indexEqpBizRDtl.do"/>');
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/biz/indexEqpBizRDtl.do"/>');
}


function fn_searchDetail(bizSno, atchFileId){
	
	requestUtil.setSearchForm("searchForm");
	//alert("===bizSno=====>>>"+bizSno+"\n===bizSno=====>>>"+atchFileId);
// debugger;
	//parent.$('#tabs-M000000704').find("iframe").attr("src", '<c:url value="/eqp/biz/indexEqpBizUDtl.do"/>?bizSno='+bizSno+'&atchFileId='+atchFileId);
	
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/biz/indexEqpBizUDtl.do"/>?bizSno='+bizSno+'&atchFileId='+gfn_nullRtnSpace(atchFileId));
	
	 
}

/**
 * 화면 컨트롤
 * @param  
 * @returns
 */
 function fn_dispCont(){
 	
	 console.log("========사용자구분 session_usergb=====>>>"+session_usergb);
	 //console.log("========대여진행상태2 pgsStat2=====>>>"+pgsStat);
	
	$("#btn_insBiz").hide(); //등록
	
	//사용자구분 (C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)   
	if(session_usergb == "C01003"){ //수사관
		$("#btn_insBiz").show(); //저장
	}
 	
 }

</script>
</head>
<body>

<div id="con_wrap">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div>
            <!-- 
                  <div><h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3></div>
             -->
                 <div class="loca">
                  <!--  <h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3>//-->
                    <div class="ttl">사업 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></div>
                    <div class="loca_list">Home > 장비 지원 관리 > 사업 관리</div>
                  </div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
<!--                     <form id="searchForm" name="searchForm" method="post"> -->
					<form id="searchForm" name="searchForm" onsubmit="return false;">
						<input type="hidden" class="" id="page" name="page" value="1"/>
                    <div class="t_head">
				        <input type="hidden" id="boardKind" class="b_put"  name="boardKind"   value="C23001"/>
				        <input type="hidden" id="userGb"   name="userGb"   value="C00000"/>
				        
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_searchList(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="11%">
                                   <col width="22%">
                                   <col width="11%">
                                   <col width="23%">
                                   <col width="11%">
                                   <col width="22%">
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">사업명</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schBizNm" id="schBizNm" style="width:300px;" maxlength="100"/>
					            </td>
					            <th scope="col" class="hcolor">계약번호</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schcntrNo" id="schcntrNo" style="width:300px;" maxlength="50"/>
					            </td>
					            <th scope="col" class="hcolor">사업시작일</th>
					            <td scope="col" >
					               <input id="schStbizStartDt" name="schStbizStartDt"  title="만료시작일자"  type="text" value="" style='width:100px' maxlength="10"/> 
					               <input id="schEdbizStartDt" name="schEdbizStartDt"  title="만료종료일자"  type="text" value="" style='width:100px' maxlength="10"/>
					            </td>
					          </tr>
					          <tr>
					          	<th scope="col" class="hcolor">사업종료일</th>
					          	<td scope="col" >
					               <input id="schStbizEndDt" name="schStbizEndDt"  title="도입시작일자"  type="text" value="" style='width:100px' maxlength="10"/> 
					               <input id="schEdbizEndDt" name="schEdbizEndDt"  title="도입종료일자"  type="text" value="" style='width:100px' maxlength="10"/>
					            </td>
					            <th scope="col" class="hcolor">업체명</th>
					          	<td scope="col" >
					               <input class="b_put" type="text" name="schCoNm" id="schCoNm" style="width:300px;" maxlength="50"/>
					            </td>
					            <th scope="col" class="hcolor">업체담당자</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schCoCrgr" id="schCoCrgr" style="width:300px;" maxlength="50"/>
					            </td>
					          </tr>    
                           </thead>
                        </table>
                      
                      </div>
                      <div  class="btn_c">
					       <ul>
                             
<%--                                <c:if test="${loginVO.userGb == 'C01999'}"> --%>
							 <li><a href="javascript:void(0);" class='RdButton' onclick="fn_insEqpBiz(); return false;" id="btn_insBiz" name="btn_insBiz" style="display: none;">등록</a></li>
                             <li><a href="javascript:void(0);" class='myButton' onclick="fn_searchList(1); return false;">조회</a></li>
<%--                                </c:if> --%>
                             </ul>   
					  </div>
                     </form>
                       
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
<!--                      <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div> -->
<!--                      <div class="bo_num"> -->
<!--                          <select id="perPageNum" name="perPageNum"> -->
<!-- 			               <option value="5">5개씩</option> -->
<!-- 			               <option value="10" selected="selected">10개씩</option>		                -->
<!-- 			             </select> -->
<!--                      </div> -->
					 <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>
                     
                     <!--------------목록---------------------->
                     <div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:450px;">  
                          <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                <caption>사업관리</caption>
                                  <colgroup>
<!--                                       <col width="8%"> -->
                                      <col width="10%">
                                      <col />
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      </colgroup>
                                    <thead>
                                      <tr>
<!--                                          <th scope="col">순번</th> -->
                                         <th scope="col">사업일련번호</th>
                                         <th scope="col">사업명</th>
                                         <th scope="col">계약번호</th>
                                         <th scope="col">계약금액</th>
                                         <th scope="col">계약형태</th>
                                         <th scope="col">사업시작일</th>
                                         <th scope="col">사업종료일</th>
                                         <th scope="col">업체명</th>
                                         <th scope="col">업체담당자명</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                    <div id="page_navi" class="page_wrap"></div>                   
                       <!-----------------------//페이징----------------------->
                 
                  </div>
                 
            </div>
    </div>
</div>

</body>
</html>