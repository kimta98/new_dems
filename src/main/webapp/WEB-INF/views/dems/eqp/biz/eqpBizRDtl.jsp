<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비 지원 관리 > 사업관리 
 * 4. 설명 : 사업 등록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var PW_Error = 0;
var ID_Duple = 1;
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	var codeInfo = [{cdId:'C12',selectId:'cntrForm',type:'1', callbackNm:'fn_ajaxCntrFormCallback', sqlQueryId:''}];
	fn_ajaxCodeList(codeInfo);
	
	<%/* 달력 세팅 */%> 
	gfn_calendarConfig("bizStartDt", "bizEndDt", "minDate", "");    <%/* 사업시작일 from */%>
	gfn_calendarConfig("bizEndDt", "bizStartDt", "maxDate", "");   <%/* 사업종료일 to */%>
	
	gfn_toNumber("cntrAmt"); <%/* 계약금액 */%>
	
	<%/* 파일업로드 세팅 */%>
    gfn_fileUpload("atchFile", "fileList4","file", 4);
    fn_fileCntChk();
    
    fn_dispCont();
});

function fn_ajaxCntrFormCallback(data){
	$('#cntrForm option:eq(0)').before("<option value='' selected>선택</option");
}

/**
 * 첨부파일갯수 체크
 * @param  
 * @returns
 */
function fn_fileCntChk(){
    var numCnt=0;
    $('#divFile :input[id^=fileList4FileSno]').each(function(index) {
        numCnt++;
    });

    if(numCnt > 4){
        $("#atchFile").hide();
    }else{
        $("#atchFile").show();
    }
}


function fn_prgdetail() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"frame/fsys/menu/fsysMenuSchPrgmPop",
		height: 700,
        width: 1000,
        title: '프로그램 목록',
        divId : 'divPrgPopup'
	});

}

//사업 등록
function fn_insEqpBiz(){
	var bizNm = $("#bizNm").val(); 
	var cntrNo = $("#cntrNo").val(); 
	var cntrAmt = $("#cntrAmt").val(); 
	var cntrForm = $("#cntrForm").val(); 
	var bizStartDt = $("#bizStartDt").val(); 
	var bizEndDt = $("#bizEndDt").val(); 
	var useYn = $("#useYn").val(); 
	var coNm = $("#coNm").val(); 
	var coCrgr = $("#coCrgr").val();
	
	if(bizNm.length < 1){	
		fn_showUserPage( "사업명을 입력하세요.", function() {
			$("#bizNm").focus();
        });
		return;  
	}else if(cntrNo.length < 1){	
		fn_showUserPage( "계약번호를 입력하세요.", function() {
			$("#cntrNo").focus();
        });
		return;  
	}else if(cntrAmt.length < 1){	
		fn_showUserPage( "계약금액을 입력하세요.", function() {
			$("#cntrAmt").focus();
        });
		return;  
	}else if(cntrForm.length < 1){	
		fn_showUserPage( "계약형태를 선택하세요.", function() {
			$("#cntrForm").focus();
        });
		return;  
	}else if(bizStartDt.length < 1 || bizEndDt.length < 1){	
		fn_showUserPage( "사업기간을 입력하세요.", function() {
			$("#bizStartDt").focus();
        });
		return;  
	}else if(useYn.length < 1){	
		fn_showUserPage( "사용여부를 선택하세요.", function() {
			$("#useYn").focus();
        });
		return;  
	}else if(coNm.length < 1){	
		fn_showUserPage( "업체명을 입력하세요.", function() {
			$("#coNm").focus();
        });
		return;  
	}else if(coCrgr.length < 1){	
		fn_showUserPage( "업체담당자명을 입력하세요.", function() {
			$("#coCrgr").focus();
        });
		return;  
	}
	
	fn_showModalPage("등록 하시겠습니까?", function() {
		var callUrl = "<c:url value='/eqp/biz/regEqpBizRDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_insEqpBizCallback'});
	});
	
}

function fn_insEqpBizCallback(data){
	fn_searchList();
}

//이메일 체크
function emailCheck(email) {
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
    if ( !email.match(regExp) ) {	return false;    } else {		return true;    }
}

//비밀번호 비교
function fn_saveComparePW(){
	var pw = $('input[id=pwd]').val();
	var pw2 = $('input[id=pwdConfirm]').val();
	if(pw != pw2)
	{
		$("#comparePw").attr("style","visibility: visible;");
		PW_Error = 1;	
	}
	else
	{
		$("#comparePw").attr("style","visibility: hidden;");
		PW_Error = 0;	
	}
}


// /fsys/user/indexFsysUserMList.do

function fn_searchList(){
// 	parent.$('#tabs-M000000704').find("iframe").attr("src", '<c:url value="/eqp/biz/indexEqpBizMList.do"/>');
	
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/biz/indexEqpBizMList.do"/>');
	
}


<%/*아이디 중복 체크*/%>
function fn_IDcheck(){
	
	var checkParamId = $("#userId").val(); 
// 	alert(checkParamId.search(/\s/));
	
	if( checkParamId.search(/\s/) > 0 ){
		alert('ID에 공백이 들어갈 수 없습니다.');
		$("#userId").focus();
		return;
	} 
	
	if(checkParamId == ""){
		alert("아이디를 입력해 주세요.");
		$("#userId").focus();
		return;
	}else{
		var callUrl = "<c:url value='/fsys/user/sysUserCheckId.do'/>";
		requestUtil.search({callUrl:callUrl,srhFormNm:'insForm',setFormNm:'insForm',callbackNm:'fn_callbackpop'});
	}
	
}

function fn_callbackpop(data) {
	
	var resultCnt = data.egovMap.resultCnt;
// 	alert("fn_callbackpop 진입 resultCnt===>>> "+resultCnt);
// 	if(resultCnt > 0){
// 		alert("111111");
// 	}else{
// 		alert("2222222");
// 	}
// 	var existsCnt = resultCnt.substring(0,resultCnt.indexOf('-'));
// 	alert("fn_callbackpop 진입 1-1");
	if( resultCnt > 0 ) {
		alert('이미 존재하는 ID입니다.');
		$('#userId').val('');
		ID_Duple=1;
		return;
	} else {
		alert('사용가능합니다.');
		ID_Duple =0;
		return;
	}
	
}

/**
 * @!@ 사업관리조회 팝업
 * @param cd
 * @returns 
 */
function fn_searchEqpBizMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/biz/eqpBizFIndEqupQListPop&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 650,
        width: 1000,
        title: '사업관리조회 팝업',
        divId : 'eqpBizFIndEqupQListPop'
        //divId : 'eqpMgmtMListPop'
	});

}

/**
 * 화면 컨트롤
 * @param  
 * @returns
 */
 function fn_dispCont(){
 	
	 console.log("========사용자구분 session_usergb=====>>>"+session_usergb);
	 //console.log("========대여진행상태2 pgsStat2=====>>>"+pgsStat);
	
	$("#btn_insBiz").hide(); //등록
	
	//사용자구분 (C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)   
	if(session_usergb == "C01003"){ //수사관
		$("#btn_insBiz").show(); //등록
	}
 	
 }

</script>

</head>

<body>
<div id="con_wrap">
        <div class="content">
           <!----현재위치----->
             
            <div id="contents_info">
                 <div class="sub_ttl">사업 등록</div><!-----타이틀------>
                 
                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">
                     
                     <input type="hidden" id="menuNo" name="menuNo" value="<c:out value="${param.menuNo}" />" />
					 <input type="hidden" id="sysGrp" name="sysGrp" value="<c:out value="${param.sysGrp}" />" />
					 <input type="hidden" id="topMenuNo" name="topMenuNo" value="" />
					 
                      <div class="t_list">
		                 <table class="iptTblX">
			               <caption>등록</caption>
			               <colgroup>
				             <col width="15%" />
				             <col width="35%" />
				             <col width="15%" />
				             <col width="*" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">사업명<span class="fontred">*</span></th>
				                 <td >
				                 	<input type="text" id="bizNm" name="bizNm"  value=""  maxlength="100" class="inpw40"  data-requireNm="사업명"	data-maxLength="200"	title="사업명"/>
				                 </td>
				                 <th scope="row">계약번호<span class="fontred">*</span></th>
				                 <td >
				                 	<input type="text" id="cntrNo" name="cntrNo"  value=""  maxlength="50" class="inpw40" data-requireNm="계약번호"	data-maxLength="50"	title="계약번호"/>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">계약금액(원)<span class="fontred">*</span></th>
				                 <td >
				                 	<input type="text" id="cntrAmt" name="cntrAmt"  value=""  maxlength="12" class="inpw40" data-requireNm="계약금액"	data-maxLength="12,0"	title="계약금액"/>
				                 </td>
				                 <th scope="row">계약형태<span class="fontred">*</span></th>
				                 <td >
				                 	<select class="selw15" id="cntrForm" name="cntrForm" onchange="" data-requireNm="계약형태"	data-maxLength="6"	title="계약형태"  >
				                 	</select>
				                 </td>
			                 </tr>
			                 <tr>
								<th scope="row">사업기간<span class="fontred">*</span></th>
								<td>
									<input type="text" id="bizStartDt" name="bizStartDt"  value=""  maxlength="8" class="inpw30" data-requireNm="사업시작일"	data-maxLength="8"	title="사업시작일"/> 
									<input type="text" id="bizEndDt" name="bizEndDt"  value=""  maxlength="8" class="inpw30" data-requireNm="사업종료일"	data-maxLength="8"	title="사업종료일"/>
								</td>
								<th scope="row">사용여부<span class="fontred">*</span></th>
								<td>
									<select class="selw10" id="useYn" name="useYn" onchange="" data-requireNm="사용여부"	data-maxLength="1"	title="사용여부"  >
										<option value=""> 선택 </option>
										<option value="Y"> 사용 </option>
										<option value="N"> 미사용 </option>
				                 	</select>
								</td>
							</tr>
			                 <tr>
			                 	  <th scope="row">업체명<span class="fontred">*</span></th>
				                  <td >
				                 	<input id="coNm" name="coNm" type="text" value=""  maxlength="50" class="inpw40" data-requireNm="업체명"	data-maxLength="100"	title="업체명"/>
				                  </td>	
				                  <th scope="row">업체담당자명<span class="fontred">*</span></th>
				                  <td >
			                  		<input id="coCrgr" name="coCrgr" type="text" value="" maxlength="50" class="inpw40" data-requireNm="업체담당자"	data-maxLength="100"	title="업체담당자"/>
<!-- 			                  		<a href="#" class="buttonG40" onclick="fn_searchEqpBizMListPop();return false;">장비조회</a> -->
				                  </td>
				                  
			                 </tr>
			                 <tr id="divFile">
				                  <th scope="row">파일첨부</th>
				                  <td colspan="3">
			                  		<button id="atchFile" name="atchFile" type="button" class="btn_sty3" >찾아보기</button>
	                                    <div id="fileList4" name="fileList4">
	                                    </div>
				                  </td>
			                 </tr>
			                 
			                </tbody>
		                 </table>
	                  </div>
					</form>
                    <div class="btn_c">
                      <ul>
                        <li><a href="#" class="RdButton" onclick="fn_insEqpBiz();return false;" id="btn_insBiz" name="btn_insBiz" style="display: none;">등록</a></li>
                        <!-- <li><a href="#" class="myButton">재입력</a></li> -->
                        <li><a href="#" class="myButton" onclick="fn_searchList(1);return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->   
                       
                    
                          
                  </div>
            </div>
        
        </div>
 </div>
<div id="divPrgPopup"></div>
<div id="eqpBizFIndEqupQListPop"></div>
</body>
</html>