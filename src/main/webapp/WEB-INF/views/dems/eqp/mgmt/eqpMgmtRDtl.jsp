<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비관리 > 장비등록
 * 4. 설명 : 장비등록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var PW_Error = 0;
var ID_Duple = 1;
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

	var codeInfo = [{cdId:'C10',selectId:'eqpBuyDiv' ,type:'1', callbackNm:'fn_ajaxEqpBuyDivCallback'},{cdId:'C14',selectId:'eqpTyp' ,type:'1'}];
	fn_ajaxCodeList(codeInfo);

	<%/* 달력 세팅 */%>
	gfn_calendarConfig("purcDt", "", "", "");   <%/* 도입일자 */%>
	gfn_calendarConfig("exprDt", "", "", "");   <%/* 만료일자 */%>

	gfn_toNumber("unitAmt"); <%/* 단가 */%>
	gfn_toNumber("purcQty"); <%/* 도입수량 */%>
	gfn_toNumber("sumAmt");  <%/* 합계금액 */%>
	gfn_toNumber("dstbQty"); <%/* 분배수량 */%>

	fn_dispCont();

	wsUri = "ws://127.0.0.1:21259/",
	websocket = new WebSocket(wsUri);
	websocket.onopen = function (e) {
		console.log("CONNECTED");
	};

	websocket.onclose = function (e) {
		console.log("DISCONNECTED");
	};

	websocket.onmessage = function (e) {
		console.log("RFID TAG : " + e.data);
		$("#rfidTag").val(e.data);
	};

	websocket.onerror = function (e) {
		console.log("ERROR : " + e.data);
	};
});

function fn_ajaxEqpBuyDivCallback(data){
	$('#eqpBuyDiv option:eq(0)').before("<option value='' selected>선택</option");
	$('#eqpTyp option:eq(0)').before("<option value='' selected>선택</option");
}

function fn_ajaxEqpTypCallback(data){
	//$('#eqpTyp option:eq(0)').before("<option value='' selected>선택</option");
}

function fn_callback(data){
	$("#topMenuNo").val(data.topMenuNo);
	if(data.sysGrpList.length > 0){
		$("#menuLvl").append('<option value="lvl2">Level2</option>');

		$.each(data.sysGrpList, function(idx, row) {
	    	$("#upperMenuNo").append("<option value='"+row.sysGrp+"'>"+row.sysGrpNm+"</option>");
	    });
	}

}

function fn_prgdetail() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"frame/fsys/menu/fsysMenuSchPrgmPop",
		height: 700,
        width: 1000,
        title: '프로그램 목록',
        divId : 'divPrgPopup'
	});

}

//장비 등록
function fn_insEqpMgmt(){
	var eqpBuyDiv = $("#eqpBuyDiv").val();
	var eqpNm = $("#eqpNm").val();
	var srNo = $("#srNo").val();
	var eqpTyp = $("#eqpTyp").val();
	var purcDt = $("#purcDt").val();
	var exprDt = $("#exprDt").val();
	var deprPrid = $("#deprPrid").val();
	var rfidTag = $("#rfidTag").val();

	if(eqpBuyDiv.length < 1){
		fn_showUserPage( "장비도입구분을 선택하세요.", function() {
			$("#eqpBuyDiv").focus();
        });
		return;
	}else if(eqpNm.length < 1){
		fn_showUserPage( "장비명을 입력하세요.", function() {
			$("#eqpNm").focus();
        });
		return;
	}else if(srNo.length < 1){
		fn_showUserPage( "시리얼번호를 입력하세요.", function() {
			$("#srNo").focus();
        });
		return;
	}else if(eqpTyp.length < 1){
		fn_showUserPage( "장비유형을 선택하세요.", function() {
			$("#eqpTyp").focus();
        });
		return;
	}else if(purcDt.length < 1){
		fn_showUserPage( "도입일자를 입력하세요.", function() {
			$("#purcDt").focus();
        });
		return;
	}else if(exprDt.length < 1){
		fn_showUserPage( "만료일자를 입력하세요.", function() {
			$("#exprDt").focus();
        });
		return;
	}else if(deprPrid.length < 1){
		fn_showUserPage( "내용년수를 입력하세요.", function() {
			$("#deprPrid").focus();
        });
		return;
	}else if(rfidTag.length < 1){
		fn_showUserPage( "RFID TAG를 입력하세요.", function() {
			$("#rfidTag").focus();
        });
		return;
	}

	fn_showModalPage("등록 하시겠습니까?", function() {
 		//장비 정보 등록
		var callUrl = "<c:url value='/eqp/mgmt/regEqpMgmtRDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_insEqpMgmtCallback'});
	});
}

//이메일 체크
function emailCheck(email) {
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
    if ( !email.match(regExp) ) {	return false;    } else {		return true;    }
}

//비밀번호 비교
function fn_saveComparePW(){
	var pw = $('input[id=pwd]').val();
	var pw2 = $('input[id=pwdConfirm]').val();
	if(pw != pw2)
	{
		$("#comparePw").attr("style","visibility: visible;");
		PW_Error = 1;
	}
	else
	{
		$("#comparePw").attr("style","visibility: hidden;");
		PW_Error = 0;
	}
}

function fn_regFsysUserRDtlCallback(data){
	fn_indexFsysUserMList();
}
// /fsys/user/indexFsysUserMList.do

function fn_searchList(){
// 	parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mgmt/indexEqpMgmtMList.do"/>');

	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/mgmt/indexEqpMgmtMList.do"/>');
}


/**
 * @ 메뉴 관리 등록 콜백
 * @param {json} data
 * @returns
 */
function fn_insEqpMgmtCallback(data){
	 fn_searchList();
}

<%/*아이디 중복 체크*/%>
function fn_IDcheck(){

	var checkParamId = $("#userId").val();
// 	alert(checkParamId.search(/\s/));

	if( checkParamId.search(/\s/) > 0 ){
		alert('ID에 공백이 들어갈 수 없습니다.');
		$("#userId").focus();
		return;
	}

	if(checkParamId == ""){
		alert("아이디를 입력해 주세요.");
		$("#userId").focus();
		return;
	}else{
		var callUrl = "<c:url value='/fsys/user/sysUserCheckId.do'/>";
		requestUtil.search({callUrl:callUrl,srhFormNm:'insForm',setFormNm:'insForm',callbackNm:'fn_callbackpop'});
	}

}

function fn_callbackpop(data) {

	var resultCnt = data.egovMap.resultCnt;
// 	alert("fn_callbackpop 진입 resultCnt===>>> "+resultCnt);
// 	if(resultCnt > 0){
// 		alert("111111");
// 	}else{
// 		alert("2222222");
// 	}
// 	var existsCnt = resultCnt.substring(0,resultCnt.indexOf('-'));
// 	alert("fn_callbackpop 진입 1-1");
	if( resultCnt > 0 ) {
		alert('이미 존재하는 ID입니다.');
		$('#userId').val('');
		ID_Duple=1;
		return;
	} else {
		alert('사용가능합니다.');
		ID_Duple =0;
		return;
	}

}

/**
 * @!@ 장비조회 팝업
 * @param cd
 * @returns
 */
function fn_searchEqpMgmtMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/mgmt/eqpMgmtFIndEqupQListPop&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 650,
        width: 1000,
        title: '장비조회 팝업',
        divId : 'eqpMgmtFIndEqupQListPop'
        //divId : 'eqpMgmtMListPop'
	});

}

/**
 * @!@ 사업관리조회 팝업
 * @param cd
 * @returns
 */
function fn_searchEqpBizMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/biz/eqpBizFIndEqupQListPop&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 700,
        width: 1000,
        title: '사업관리조회 팝업',
        divId : 'eqpBizFIndEqupQListPop'
        //divId : 'eqpMgmtMListPop'
	});

}

/**
 * 화면 컨트롤
 * @param
 * @returns
 */
 function fn_dispCont(){

	 console.log("========사용자구분 session_usergb=====>>>"+session_usergb);
	 //console.log("========대여진행상태2 pgsStat2=====>>>"+pgsStat);

	$("#btn_insMgmt").hide(); //등록

	//사용자구분 (C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)
	if(session_usergb == "C01003"){ //수사관
		$("#btn_insMgmt").show(); //등록
	}

 }

</script>

</head>

<body>
<div id="con_wrap">
        <div class="content">
           <!----현재위치----->

            <div id="contents_info">
                 <div class="sub_ttl">장비 등록</div><!-----타이틀------>

                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">

                     <input type="hidden" id="menuNo" name="menuNo" value="<c:out value="${param.menuNo}" />" />
					 <input type="hidden" id="sysGrp" name="sysGrp" value="<c:out value="${param.sysGrp}" />" />
					 <input type="hidden" id="topMenuNo" name="topMenuNo" value="" />

                      <div class="t_list">
		                 <table class="iptTblX">
			               <caption>등록</caption>
			               <colgroup>
				             <col width="15%" />
				             <col width="35%" />
				             <col width="15%" />
				             <col width="*" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">장비도입구분<span class="fontred">*</span></th>
				                 <td >
				                 	<select class="" id="eqpBuyDiv" name="eqpBuyDiv" onchange="" style=width:80px;" data-requireNm="장비도입구분" data-maxLength="6" title="장비도입구분" >
							 			<option value="001">구입</option>
									</select>
				                 </td>
				                 <th scope="row">사업일련번호</th>
				                 <td >
				                 	<input type="text" id="bizNm" name="bizNm"  value=""  maxlength="20" class="inpw40"/>
				                 	<input type="hidden" id="bizSno" name="bizSno"  value=""  maxlength="25"/> &nbsp;
				                 	<a href="#" class="buttonG80" onclick="fn_searchEqpBizMListPop();return false;">사업검색</a>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">장비명<span class="fontred">*</span></th>
				                 <td >
				                 	<input id="eqpNm" name="eqpNm" type="text" value=""  maxlength="100" class="inpw40" data-requireNm="장비명" data-maxLength="200" title="장비명"/>
				                 </td>
				                 <th scope="row">시리얼번호<span class="fontred">*</span></th>
				                 <td >
				                 	<input id="srNo" name="srNo" type="text" value=""  maxlength="50" data-requireNm="시리얼번호" data-maxLength="50" title="시리얼번호" class="inpw40"/>
				                 </td>
			                 </tr>
			                 <tr>
								<th scope="row">장비유형<span class="fontred">*</span></th>
								<td >
									<select class="" id="eqpTyp" name="eqpTyp" onchange="" style=width:80px;" data-requireNm="장비유형" data-maxLength="6" title="장비유형" >
									</select>
								</td>
								<th scope="row">도입일자<span class="fontred">*</span></th>
				                 <td >
				                 	<input id="purcDt" name="purcDt" type="text" value=""  maxlength="10" data-requireNm="도입일자" data-maxLength="8" title="도입일자" />
				                 </td>
							</tr>
			                 <tr>
				                  <th scope="row">만료일자<span class="fontred">*</span></th>
				                  <td >
			                  		<input id="exprDt" name="exprDt" type="text" value="" maxlength="10" data-requireNm="만료일자" data-maxLength="8" title="만료일자"/>
				                  </td>
				                  <th scope="row">보증기간</th>
				                  <td >
				                 	<input id="guarTrm" name="guarTrm" type="text" value=""  maxlength="8" class="inpw40"/>
				                  </td>
<!-- 				                  &nbsp;&nbsp;<font color="blue">문자,숫자,특수문자 혼용, 10자리 이상입력.</font>	 -->
			                 </tr>
			                 <tr>
				                 <th scope="row">장비계약번호</th>
				                 <td >
				                 	<input id="eqpCntrNo" name="eqpCntrNo" type="text" value=""  maxlength="25" class="inpw40"/>
				                 </td>
				                 <th scope="row">제조사</th>
				                 <td >
				                 	<input id="mnftCo" name="mnftCo" type="text" value=""  maxlength="100" class="inpw40"/> &nbsp;
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">모델명</th>
				                 <td >
<!-- 				                 	<input id="mdlNm" name="mdlNm" type="text" value=""  maxlength="100" data-requireNm="모델명" data-maxLength="200" title="모델명" class="inpw40"/> -->
									<input id="mdlNm" name="mdlNm" type="text" value=""  maxlength="100"  title="모델명" class="inpw40"/>
				                 </td>
				                 <th scope="row">제조국가</th>
				                 <td >
				                 	<input id="mnftNat" name="mnftNat" type="text" value=""  maxlength="100" class="inpw40"/> &nbsp;
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">단가</th>
				                 <td >
				                 	<input id="unitAmt" name="unitAmt" type="text" value=""  maxlength="15" class="inpw40"/>
				                 </td>
				                 <th scope="row">도입수량</th>
				                 <td >
				                 	<input id="purcQty" name="purcQty" type="text" value=""  maxlength="5" class="inpw40"/>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">합계금액</th>
				                 <td >
				                 	<input id="sumAmt" name="sumAmt" type="text" value=""  maxlength="15" class="inpw40"/>
				                 </td>
				                 <th scope="row">분배수량</th>
				                 <td >
				                 	<input id="dstbQty" name="dstbQty" type="text" value=""  maxlength="6" class="inpw40"/> &nbsp;
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">내용년수<span class="fontred">*</span></th>
				                 <td >
				                 	<input id="deprPrid" name="deprPrid" type="text" value=""  maxlength="20" class="inpw40" data-requireNm="내용년수" data-maxLength="20" title="내용년수"/>
				                 </td>
				                 <th scope="row">품종명</th>
				                 <td >
				                 	<input id="kindNm" name="kindNm" type="text" value=""  maxlength="100" class="inpw40"/> &nbsp;
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">유상/무상 구분</th>
				                 <td >
				                 	<input id="payDiv" name="payDiv" type="text" value=""  maxlength="100" class="inpw40"/> &nbsp;
				                 </td>
				                 <th scope="row">RFID TAG<span class="fontred">*</span></th>
				                 <td >
				                 	<input id="rfidTag" name="rfidTag" type="text" value=""  maxlength="20" class="inpw40"  data-requireNm="RFID TAG" data-maxLength="20" title="RFID TAG"/> &nbsp;
				                 </td>
			                 </tr>
			                </tbody>
		                 </table>
	                  </div>
					</form>
                    <div class="btn_c">
                      <ul>
                        <li><a href="#" class="RdButton" onclick="fn_insEqpMgmt();return false;" id="btn_insMgmt" name="btn_insMgmt" style="display: none;">등록</a></li>
                        <!-- <li><a href="#" class="myButton">재입력</a></li> -->
                        <li><a href="#" class="myButton" onclick="fn_searchList(1);return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->



                  </div>
            </div>

        </div>
 </div>
<div id="divPrgPopup"></div>
<div id="eqpMgmtFIndEqupQListPop"></div>
<div id="eqpBizFIndEqupQListPop"></div>
</body>
</html>