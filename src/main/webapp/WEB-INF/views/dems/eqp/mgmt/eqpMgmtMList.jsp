<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비관리 조회
 * 4. 설명 : 장비관리 조회
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<%-- <link rel="stylesheet" href="<c:url value="/css/com/bootstrap.css" />"> --%>
<script type="text/javaScript" language="javascript">
// var flag = true;
// var perPageNum;
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	var codeInfo2 = [{cdId:'C14',selectId:'schEqpTyp',type:'1', callbackNm:'fn_ajaxEqpTypCallback', sqlQueryId:''}];
	// fn_ajaxCodeList(codeInfo2);
	gfn_init({startFnNm:'', param:codeInfo2, codeSet:'Y'});
	
	gfn_calendarConfig("schStPurcDt", "schEdPurcDt", "minDate", "");    <%/* 도입일자 from */%>
	gfn_calendarConfig("schEdPurcDt", "schStPurcDt", "maxDate", "");   <%/* 도입일자 to */%>
	
	gfn_calendarConfig("schStExprDt", "schEdExprDt", "minDate", "");    <%/* 만료일자 from */%>
	gfn_calendarConfig("schEdExprDt", "schStExprDt", "maxDate", "");   <%/* 만료일자 to */%>
	
	fn_searchList(1);
	fn_dispCont();
});


function fn_ajaxEqpTypCallback(data){
// 	$('#schEqpTyp option:eq(0)').before("<option value='' selected>전체</option>");
	$("select[name='schEqpTyp']").prepend('<option value="" selected>전체</option>');
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_searchList"});
}

/**
 * 달력 콜백함수(없으면 삭제가능) 
 * @param {string} prgID
 * @returns 
 */
function fn_callBack(){
}


/**
 * @!@ 
 * @param {string} prgID
 * @returns 
 */
function fn_queryCodeList(param) {
	$.each(param.paramRow,function(idx,row){
        $("#"+row.target).append("<option value=''>전체</option>");
    });
	
	$.ajax({
		url:  "<c:url value='/fcom/queryAjaxCodeList.do'/>",
		type : "POST",
		data: JSON.stringify(param),
		dataType: 'json',
		contentType:"application/json",
		success: function(data) {
		    $.each(data, function(idx, dataRow) {
		    	$.each(dataRow, function(idx, rowRow) {
			    	$("#"+rowRow.target).append("<option value='"+rowRow.cd+"'>"+rowRow.cdNm+"</option>");
			    });
		    });
		    
		}
	});
}


/**
 * @!@ 사용자 관리 리스트 조회
 * @param {int} page
 * @returns 
 */
function fn_searchList(page){
// 	debugger;
	var callUrl = "<c:url value='/eqp/mgmt/queryEqpMgmtMList.do'/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_searchListCallback', page:page, perPageNum:10});
	
// 	var callUrl = "<c:url value='/fbbs/ntc/queryFBbsNtcMList.do'/>";	
// 	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callback',page:page,perPageNum:10});
	
}

 /**
  * 장비관리 리스트 조회 콜백
  * @param {json} data
  * @returns 
  */
 function fn_searchListCallback(data){
//  	debugger;
	var list = data.eqpMgmtMList;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;
	
	$("#listTab > tbody").empty();
	
 	$("#totalcnt").text(data.totalCount);
	
  	if(data.eqpMgmtMList.length < 1){
  		var append = "";
		append += "<tr>";
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
		
		$.each(list,function(idx,row){
	 		var append = "";
	 		append += "<tr>";
			
// 	 		append += "<td>" + row.rnum + "</td>";
	 		append += "<td>" + row.eqpSno + "</td>";
	 		append += "<td><a href='javascript:void(0)' onclick=javascript:fn_searchDetail('"+row.eqpSno+"')><u>"+gfn_nullRtnSpace(row.eqpNm)+"</u></a></td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.eqpTypNm) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.srNo) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.deprPrid) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.rfidTag) + "</td>";
	 		append += "<td>" + gfn_dashDate(gfn_nullRtnSpace(row.purcDt),'-') + "</td>";
	 		append += "<td>" + gfn_dashDate(gfn_nullRtnSpace(row.exprDt),'-') + "</td>";
// 	 		if(row.mtCnt > 0){
// 	 			append += "<td><a href='javascript:fn_modifyMntInfo(\""+row.insttCd+"\", \""+row.insttNm+"\");' class='byButton'>유지보수 수정</a></td>";	
// 	 		}else{
// 	 			append += "<td><a href='javascript:fn_insMntInfo(\""+row.eqpSno+"\");' class='byButton'>유지보수 등록</a></td>";
// 	 		}
	 		if(row.mtCnt > 0){
	 			append += "<td><a href='javascript:fn_modifyMntInfo(\""+row.eqpSno+"\");' class='byButton' style='height:22px;'>유지보수 수정</a></td>";	
	 		}else{
	 			append += "<td><a href='javascript:fn_insMntInfo(\""+row.eqpSno+"\");' class='byButton' style='height:22px;'>유지보수 등록</a></td>";
	 		}
	 		
	 		append += "</tr>";
	        $("#listTab > tbody").append(append);
	  
	  		
	  		
	  	});
	}
  	
  	data.__callFuncName__ ="fn_searchList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
	
	
 } 
 
function fn_insEqpMgmt(){
	
// 	parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mgmt/indexEqpMgmtRDtl.do"/>');
	
	requestUtil.setSearchForm("searchForm");
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/mgmt/indexEqpMgmtRDtl.do"/>');
}


function fn_searchDetail(eqpSno){
// debugger;
	//parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mgmt/indexEqpMgmtUDtl.do"/>?eqpSno='+eqpSno);
	
	requestUtil.setSearchForm("searchForm");
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/mgmt/indexEqpMgmtUDtl.do"/>?eqpSno='+eqpSno);
	 
}

/**
 * 유지보수관리 > 유지보수 등록
 * @param {json} data
 * @returns 
 */
function fn_insMntInfo(eqpSno){
	// debugger;
	var flag = "MgmtList"
	//parent.addNaviTab('Home > 장비 지원 관리 > 유지 보수 관리',"<c:url value="/eqp/mnt/indexEqpMntRDtl.do"/>",'유지 보수 관리','M000000703');	
// 	parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mnt/indexEqpMntRDtl.do"/>?flag='+flag+'&eqpSno='+eqpSno);
	
	requestUtil.setSearchForm("searchForm");
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/mnt/indexEqpMntRDtl.do"/>?flag='+flag+'&eqpSno='+eqpSno);
		 
}
 
/**
 * 유지보수관리 > 유지보수 수정
 * @param {json} data
 * @returns 
 */
function fn_modifyMntInfo(eqpSno){
	// debugger;
	var flag = "MgmtList"
	//parent.addNaviTab('Home > 장비 지원 관리 > 유지 보수 관리',"<c:url value="/eqp/mnt/indexEqpMntRDtl.do"/>",'유지 보수 관리','M000000703');	
// 	parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mnt/indexEqpMntUDtl.do"/>?flag='+flag+'&eqpSno='+eqpSno);
	
	requestUtil.setSearchForm("searchForm");
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/mnt/indexEqpMntUDtl.do"/>?flag='+flag+'&eqpSno='+eqpSno);
		 
} 

 /**
  * @!@ 장비 엑셀 업로드
  * @param {string} crimeCd, crimeGrp
  * @returns 
  */
function fn_updExcEqpMgmtMList() {
 	if($("#excelFile").val() == ""){
 		fn_showUserPage("엑셀파일을 선택해주세요.", function() {
 		});
 		return false;
 	}
 	requestUtil.updExcMList({
 		fileNm:"excelFile"												// file 태그 id
 		, sheetNum:0													// 시트번호
 		, strartRowNum:1												// 읽어드릴 행(0부터 시작) 
 		, startCelNum:0													// 읽어드릴 셀(0부터 시작)
 		, celCnt:15														// 총 셀 갯수
 		, culmnNmArr:"eqpSno,eqpNm,eqpBuyDiv,eqpCntrNo,mnftCo,mdlNm,mnftNat,unitAmt,purcQty,sumAmt,dstbQty,deprPrid,kindNm,payDiv,rfidTag"	// 셀 컬럼
 		, sqlQueryId:"eqpMgmtDAO.regExcelEqpMgmtRDtl"				// insert 쿼리 id
 		, callbackNm: "updExcMListCallback"								// 콜백함수
 	});
} 

 /**
  * @!@ 장비 엑셀 업로드
  * @param {string} crimeCd, crimeGrp
  * @returns 
  */
function updExcMListCallback() {
   //fn_showUserPage("정상적으로 등록되었습니다.");
   fn_searchList(1);
}
  
/**
 * 화면 컨트롤
 * @param  
 * @returns
 */
 function fn_dispCont(){
 	
	 console.log("========사용자구분 session_usergb=====>>>"+session_usergb);
	 //console.log("========대여진행상태2 pgsStat2=====>>>"+pgsStat);
	
	$("#btn_findExcel").hide(); //파일찾기
	$("#btn_excelUpload").hide(); //엑셀업로드
	$("#btn_excelDown").hide(); //엑셀양식다운로드
	$("#btn_insMgmt").hide(); //등록
	
	//사용자구분 (C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)   
	if(session_usergb == "C01003"){ //수사관
		$("#btn_findExcel").show(); //파일찾기
		$("#btn_excelUpload").show(); //엑셀업로드
		$("#btn_excelDown").show(); //엑셀양식다운로드
		$("#btn_insMgmt").show(); //등록
	}
 	
 }  
</script>
</head>
<body>

<div id="con_wrap">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div>
            <!-- 
                  <div><h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3></div>
             -->
                 <div class="loca">
                  <!--  <h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3>//-->
                    <div class="ttl">장비 관리</div>
                    <div class="loca_list">Home > 장비 지원 관리 > 장비 관리</div>
                  </div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
<!--                     <form id="searchForm" name="searchForm" method="post"> -->
					 <form id="searchForm" name="searchForm" onsubmit="return false;">	
					<input type="hidden" class="" id="page" name="page" value="1"/>
                    <div class="t_head">
				        <input type="hidden" id="boardKind" class="b_put"  name="boardKind"   value="C23001"/>
				        <input type="hidden" id="userGb"   name="userGb"   value="C00000"/>
				        
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_searchList(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="11%">
                                   <col width="22%">
                                   <col width="11%">
                                   <col width="23%">
                                   <col width="11%">
                                   <col width="22%">
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">장비번호</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schEqpSno" id="schEqpSno" style="width:300px;" maxlength="10"/>
					            </td>
					            <th scope="col" class="hcolor">장비명</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schEqpNm" id="schEqpNm" style="width:300px;" maxlength="100"/>
					            </td>
					            <th scope="col" class="hcolor">장비유형</th>
					            <td scope="col" >
					               <select class="" id="schEqpTyp" name="schEqpTyp" onchange="fn_searchList(1)" style=width:80px;" >
							 			<%-- <option value="" <c:out value="${sysGrpInfo.cd==''?\"selected\":\"\"}"/> >전체</option>
										<c:forEach items="${sysGrpList}" var="sysGrpInfo" varStatus="status">
										<option value="<c:out value="${sysGrpInfo.cd}"/>"><c:out value="${sysGrpInfo.cdNm}"/></option>
										</c:forEach> --%>
								   </select>
					            </td>
					          </tr>
					          <tr>
					          	<th scope="col" class="hcolor">도입일자</th>
					          	<td scope="col" >
					               <input id="schStPurcDt" name="schStPurcDt"  title="도입시작일자"  type="text" value="" style='width:100px' maxlength="10"/> 
					               <input id="schEdPurcDt" name="schEdPurcDt"  title="도입종료일자"  type="text" value="" style='width:100px' maxlength="10"/>
					            </td>
					            <th scope="col" class="hcolor">만료일</th>
					          	<td scope="col" >
					               <input id="schStExprDt" name="schStExprDt"  title="만료시작일자"  type="text" value="" style='width:100px' maxlength="10"/> 
					               <input id="schEdExprDt" name="schEdExprDt"  title="만료종료일자"  type="text" value="" style='width:100px' maxlength="10"/>
					            </td>
					            <th scope="col" class="hcolor">RFID TAG</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schRfidTag" id="schRfidTag" style="width:300px;" maxlength="20"/>
					            </td>
					          </tr>    
                           </thead>
                        </table>
                      
                      </div>
                      <div  class="btn_c">
					       <ul>
                             
<%--                                <c:if test="${loginVO.userGb == 'C01999'}"> --%>
<!-- 							 <li><a href="javascript:void(0);" class='myButton' onclick="fn_aaaaa(); return false;">엑셀양식 다운로드</a></li> -->

							 <li><div id="btn_findExcel" name="btn_findExcel" style="display: none;"><input name="excelFile" id="excelFile" type="file" ></div></li>
                             <li><a href="javascript:void(0);" onclick="fn_updExcEqpMgmtMList();return false;" class="myButton" id="btn_excelUpload" name="btn_excelUpload" style="display: none;">엑셀업로드</a></li>
                             <li><a href="<c:url value='/resources/sample/장비엑셀업로드.xls'/>" download class="myButton" id="btn_excelDown" name="btn_excelDown" style="display: none;">엑셀양식다운로드</a></li>
                             <li><a href="javascript:void(0);" class='RdButton' onclick="fn_insEqpMgmt(); return false;" id="btn_insMgmt" name="btn_insMgmt" style="display: none;">등록</a></li>
                             <li><a href="javascript:void(0);" class='gyButton' onclick="fn_searchList(1); return false;">조회</a></li>
<%--                                </c:if> --%>
                             </ul>   
					  </div>
                     </form>
                       
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
<!--                      <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div> -->
<!--                      <div class="bo_num"> -->
<!--                          <select id="perPageNum" name="perPageNum"> -->
<!-- 			               <option value="5">5개씩</option> -->
<!-- 			               <option value="10" selected="selected">10개씩</option>		                -->
<!-- 			             </select> -->
<!--                      </div> -->
					 <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>
                     
                     <!--------------목록---------------------->
                     <div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:450px;">  
                          <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                <caption>공지사항관리</caption>
                                  <colgroup>
<!--                                       <col width="8%"> -->
                                      <col width="10%">
                                      <col />
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      </colgroup>
                                    <thead>
                                      <tr>
<!--                                          <th scope="col">순번</th> -->
                                         <th scope="col">장비번호</th>
                                         <th scope="col">장비명</th>
                                         <th scope="col">장비유형</th>
                                         <th scope="col">S/N</th>
                                         <th scope="col">내용연수</th>
                                         <th scope="col">RFID TAG</th>
                                         <th scope="col">도입일</th>
                                         <th scope="col">만료일</th>
                                         <th scope="col">유지보수</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                    <div id="page_navi" class="page_wrap"></div>                   
                       <!-----------------------//페이징----------------------->
                 
                  </div>
                 
            </div>
    </div>
</div>

</body>
</html>