<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : ilyong
 * 3. 화면명 : 부서관리 > 부서별 사용자 등록
 * 4. 설명 : 부서별 사용자 등록 
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>공공급식 식단관리 시스템</title>

<script type="text/javaScript" language="javascript" defer="defer">
var spanCd = "${paramMap.paramInsttCd}";
var spanText = "${paramMap.paramInsttCdNm}";

$(document).ready(function() {
	//alert("===tt====>>>"+tt);
// 	$("#sortOrd").on("keyup",function(){
// 		gfn_new_number("sortOrd");
// 	})
	
	$("#insttCdNm").text(spanText);
	gfn_calendarConfig("aprvDt", "", "", "");   <%/* 승인일자 */%>
	gfn_calendarConfig("rtirDt", "", "", "");   <%/* rtirDt */%>
});


/**
 * @!@ 장비조회
 * @param {int} page
 * @returns 
 */
function fn_searchList(page){
// 	debugger;
	var callUrl = "<c:url value='/eqp/mgmt/queryEqpMgmtMListPop.do'/>";
	
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_searchListCallback', page:page, perPageNum:10});
	
// 	var callUrl = "<c:url value='/fbbs/ntc/queryFBbsNtcMList.do'/>";	
// 	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callback',page:page,perPageNum:10});
	
}

 
 /**
  * @!@ 메뉴 관리 리스트 조회 콜백
  * @param {json} data
  * @returns 
  */
 function fn_searchListCallback(data){
//  	debugger;
 	$("#eqpMgmtMList").empty();
	$("#totalcnt").text(data.totalCount);
	
  	if(data.eqpMgmtMList.length < 1){
  		$('#listTab > tbody').append('<tr><td colspan="7">조회된 결과가 없습니다.</td></tr>');
	}else{
// 		$.each(data.commNotcMList, function(index, item){

// 			$('#commNotcMList').append("<tr><td>"+(data.page == 1 ? index+1 : ((data.page-1)*data.perPageNum)+index+1)+"</td><td>"+item.putupSno+"</td><td style='max-width: 200px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis'>"
// 					+"<a href='#' onclick=javascript:fn_detail("+item.putupSno+")><u><strong><font color=green>"+item.title+"</font></strong></u></a>"+"</td><td>"
// 					+item.fileCount+"</td><td>"+item.putupDt+"</td><td>"+item.regrNm+"</td><td>"+item.selectNum+"</td></tr>");
// 		});
		var list = data.eqpMgmtMList;
	 	$.each(list,function(idx,row){
	 		var append = "";
	 		append += "<tr>";
			
	 		append += "<td>" + row.rnum + "</td>";
	 		append += "<td>" + row.eqpSno + "</td>";
	 		append += "<td><a href='javascript:void(0)' onclick=javascript:fn_searchDetail('"+row.eqpSno+"')><u>"+row.eqpNm+"</u></a></td>";
	 		append += "<td>" + row.eqpTyp + "</td>";
// 	 		append += "<td>" + row.srNo + "</td>";
// 	 		append += "<td>" + '내용연수' + "</td>";
	 		append += "<td>" + row.purcDt + "</td>";
	 		append += "<td>" + row.exprDt + "</td>";
	 		append += "<td><a href='javascript:fn_setEqpSno(\""+row.eqpSno+"\", \""+row.eqpNm+"\");' class='myButton'>선택</a></td>";
// 	 		append += "<td>" + "선택" + "</td>";
	 		
	 		append += "</tr>";
	        $("#listTab > tbody").append(append);
	  	});
	}
  	
  	data.__callFuncName__ ="fn_searchList";
	data.__naviID__ ="page_navi";
	
	//pageUtil.setCalFuc("fn_queryMList2");
	//pageUtil.setNaviID("page_navi2");
	pageUtil.setPageNavi(data);
 	
 	
 } 

 /**
 * 장비 선택
 * @param
 * @returns 
 */
 function fn_setEqpSno(eqpSno, eqpNm){

	 $("#eqpSno").val(eqpSno);
	 $("#eqpNm").val(eqpNm);
	 fn_dialogClose('eqpMgmtMListPop');
 	
 }
  
  
/**
* 부서별 사용자 등록
* @param
* @returns 
*/
function fn_insDeptUser(){

	if(confirm("등록 하시겠습니까?")){

        var callUrl = "<c:url value='/fsys/dept/regFsysDeptUserRDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_fn_insDeptUserCallback'});
	}
	
}

/**
 * 부서별 사용자 등록 콜백
 * @param {string} data
 * @returns 
 */
function fn_fn_insDeptUserCallback(data){
	 //fn_queryMList(1);
	 fn_setSearchInsttCd(spanCd, spanText);
	 fn_dialogClose('fsysDeptUserRDtlPop');
}

 function fn_chgMainCrgrYn(){
	 if($("input:checkbox[name=mainCrgrYn]").is(":checked") ){
		 $("#aprvAuthYn").prop("checked", false);
	 }else{
		 $("#aprvAuthYn").prop("checked", true);
	 }
 }  
 
function fn_chgAprvAuthYn(){
	 if($("input:checkbox[name=aprvAuthYn]").is(":checked") ){
		 $("#mainCrgrYn").prop("checked", false);
	 }else{
		 $("#mainCrgrYn").prop("checked", true);
	 }
}  

function fn_closePop(){
	fn_dialogClose('fsysDeptUserRDtlPop');
}

function fn_selData(){
	$("#eqpNm").val("바보");
}
</script>

</head>
<body>
<div id="con_wrap">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                    <div>
                         <div class="loca">
		                    <div class="ttl">장비조회</div>
		                    <div class="loca_list">Home > 시스템 관리 > 부서관리</div>
		                  </div>
                          <div class="sub" style="float: left; width: 100%;">
                             <!--------------검색------------------>
                             <form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
                            <div class="t_head">
                                  <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_queryMList(1);">
                                        <caption>검색</caption>
                                         <colgroup>
                                            <col width="20%">
                                            <col width="40%">
                                            <col width="20%">
                                            <col width="20%">
                                         </colgroup>
                                   <thead>
                                      <tr>
                                         	<th scope="row" class="hcolor">장비번호</th>
								           <td>
								               <input type="text" id="searchInsttNm" name="searchInsttNm" title="기관명" style="width:220px;"/>
								           </td>
								           <th scope="row" class="hcolor">장비명</th>
								           <td colspan="3">
								           		<input type="text" id="searchInsttNm" name="searchInsttNm" title="기관명" style="width:220px;"/>
<!-- 								               <select id="searchUseYn" name="searchUseYn" onchange="fn_queryMList(1)" style="width:100px;"> -->
<!-- 								                   <option value="" selected>전체</option> -->
<!-- 								                   <option value="Y">사용</option> -->
<!-- 								                   <option value="N">미사용</option> -->
<!-- 								               </select> -->
								           </td>
                                      </tr>
                                   </thead>
                                </table>
                              </div>
                            <div class="btn_c">
                                  <ul>
                                     <li><a href="javascript:fn_searchList(1);" class="gyButton">조회</a></li>
                                     <li><a href="javascript:fn_indexFsysDeptRDtl();" class="myButton">등록</a></li>
                                     <li><a href="javascript:fn_selData();" class="myButton">선택</a></li>
                                     <!-- <li><a href="javascript:void(0);" class="myButton">엑셀</a></li> -->
                                  </ul>
                               </div>
                               </form>
                            <!--------------//검색------------------>
                            
                            <!--------------결과------------------>
                             <div class="r_num">| 결과  <strong style="color:#C00">40</strong>건</div>
                             
                             <!--------------목록---------------------->
                             <div class="t_list">  
                                  <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="16%">
                                              <col width="16%">
                                              <col />
                                              <col width="16%">
                                              <col width="12%">
                                              <col width="18%">
                                              <col width="12%">
                                          </colgroup>
                                            <thead>
                                              <tr>
                                              	 <th scope="col">순번</th>
                                                 <th scope="col">장비번호</th>
                                                 <th scope="col">장비명</th>
                                                 <th scope="col">장비유형</th>
                                                 <th scope="col">도입일</th>
                                                 <th scope="col">만료일</th>
                                                 <th scope="col">선택</th>
                                              </tr>
                                            </thead>
                                            <tbody>

                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->
                             
                             <!-----------------------페이징----------------------->
                             <div id="page_navi" class="page_wrap"></div>
                               <!-----------------------//페이징----------------------->
                          
                          </div>
                          
                          
                          
                         
                    </div>
            </div>
                 <!---  //contnets  적용 ------>
  </div>
</body>
</html>