<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 22. 오후 5:21:45
 * 2. 작성자 : 이종인
 * 3. 화면명 : 사건목록조회 팝업
 * 4. 설명 : 사건목록조회 팝업
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javaScript" language="javascript">
$(document).ready(function() {	
	var codeInfo = [{cdId:'crimeGrp',selectId:'searchCrimeCd',type:'4', callbackNm:'fn_codeCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	fn_ajaxCodeList(codeInfo);
});

function fn_codeCallback(){
	$('#searchCrimeCd').prepend('<option value="" selected>전체</option>');
	fn_incdtSearch(1);
}

/**
 * 사건조회화면목록 조회
 * @param {string} page 항목에 대한 고유 식별자 
 * @returns fn_callBack
 */
function fn_incdtSearch(page){
	var callUrl = "<c:url value = "/eqp/lend/queryIncidentPop.do"/>?type=incdt";
	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callBack',page:page,perPageNum:10});
}

/**
 * 사건조회화면목록 조회 콜백함수
 * @param {object} data 조회한 결과데이터
 * @returns
 */
function fn_callBack(data){
	var list = data.popList;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;
	
	$("#listTab > tbody").empty();
	$("#totalcnt").text(data.totalCount);
	
	
	if(data.popList.length < 1){
		var append = "";
		append += "<tr>";
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
	
		$.each(data.popList, function(index, item){
			var append = "";
	 		append += "<tr>";
	 		
		    append += "<td>" + item.rnum + "</td>";		
			append += "<td>" + gfn_nullRtnSpace(item.incdtNo) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(item.incdtNm) + "</td>";	
			append += "<td>" + gfn_nullRtnSpace(item.crimeNm) + "</td>";	
			append += "<td>" + gfn_nullRtnSpace(item.suppReqSno) + "</td>";	
			append += "<td>" + gfn_nullRtnSpace(item.cfscSechPlndDt) + "</td>";	
			append += "<td>" + gfn_nullRtnSpace(item.userNm) + "</td>";	
			append += "<td>" + gfn_nullRtnSpace(item.aprvrNm) + "</td>";	
			append += "<td>" + gfn_nullRtnSpace(item.regDt) + "</td>";	
			//append += "<td><button class=gyButton onclick=javascript:fn_setIncdt('"+encodeURIComponent(item.incdtNo)+"','"+item.incdtSno+"','"+encodeURIComponent(item.incdtNm)+"','"+item.suppReqSno+"','','','');>선택</button></td></tr>";	
// 			append += "<td><a href='javascript:fn_setIncdt(\""+encodeURIComponent(item.incdtNo)+"\", \""+item.incdtSno+"\", \""+encodeURIComponent(item.incdtNm)+"\", \""+item.suppReqSno+"\");' class='byButton'>선택</a></td>";
			append += "<td><a href='javascript:fn_setIncdt(\""+item.incdtNo+"\", \""+item.incdtSno+"\", \""+item.incdtNm+"\", \""+item.suppReqSno+"\");' class='byButton'>선택</a></td>";
			
			append += "</tr>";
			$("#listTab > tbody").append(append);
			
// 			"<td>"+gfn_nullRtnSpace(item.incdtNo)+"</td>"+
// 			"<td>"+gfn_nullRtnSpace(item.incdtNm)+"</td>"+
// 			"<td>"+gfn_nullRtnSpace(item.crimeNm)+"</td>"+
// 			"<td>"+gfn_nullRtnSpace(item.suppReqSno)+"</td>"+
// 			"<td>"+gfn_nullRtnSpace(item.cfscSechPlndDt)+"</td>"+
// 			"<td>"+gfn_nullRtnSpace(item.userNm)+"</td>"+
// 			"<td>"+gfn_nullRtnSpace(item.aprvrNm)+"</td>"+
// 			"<td>"+gfn_nullRtnSpace(item.regDt)+"</td>"+
// 			"<td><button class=button35 onclick=javascript:fn_setIncdt('"+encodeURIComponent(item.incdtNo)+"','"+item.incdtSno+"','"+encodeURIComponent(item.incdtNm)+"','"+item.suppReqSno+"','','','');>선택</button></td></tr>");
// 			"<td><button class=button35 onclick=javascript:fn_setIncdt('"+encodeURIComponent(item.incdtNo)+"','"+item.incdtSno+"','"+encodeURIComponent(item.incdtNm)+"','"+item.regUserId+"','"+item.aprvrId+"','"+encodeURIComponent(item.aprvrNm)+"','"+encodeURIComponent(item.regUserNm)+"');>선택</button></td></tr>");
		 });
	}

	data.__callFuncName__ ="fn_incdtSearch";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
}
 
/**
 * 사건선택 
 * @param {String} incdtNo
 * @param {String} incdtSno
 * @param {String} incdtNm
 * @param {String} regUserId
 * @param {String} aprvrId
 * @param {String} aprvrNm
 * @param {String} regUserNm
 * @returns {Object} paramObj
 */ 
function fn_setIncdt(incdtNo,incdtSno,incdtNm,suppReqSno){
	//alert(incdtNm); 
	var divId = 'incdtFindPop';
	var paramObj = new Object();
	//paramObj.incdtNm = decodeURIComponent(incdtNo) + " (" + decodeURIComponent(incdtNm) + ")";     //사건번호(사건명)
	paramObj.incdtNm = incdtNo + " (" + incdtNm + ")";     //사건번호(사건명)
	paramObj.incdtSno = incdtSno;                       //사건일련번호
	paramObj.suppReqSno = suppReqSno;                     //지원요청일련번호
// 	paramObj.prosrId = aprvrId;                         //주안군검사ID
// 	paramObj.prosrNm = decodeURIComponent(aprvrNm);     //주안군검사이름
// 	paramObj.regUserNm = decodeURIComponent(regUserNm); //담당자(연락처,HP)

	fn_popCallBack(paramObj, divId);
	
}
 
</script>
</head>
<body>
<div id="con_wrap_pop">
<div class="contents">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div class="window_popup">
                  <div class="sub_ttl">사건 조회</div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
                    <form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
                    <div class="t_head">
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_incdtSearch(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="15%">
                                   <col width="35%">
                                   <col width="15%">
                                   <col width="35%">                         
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">사건번호</th>
					            <td scope="col">
					               <input class="input20" type="text" name="searchIncdtNo" id="searchIncdtNo"/>
					            </td>
					            <th scope="col" class="hcolor">사건명</th>
					            <td scope="col">
					               <input class="input20" type="text" name="searchIncdtNm" id="searchIncdtNm"/>
					            </td>
					          </tr>
					          <tr>
					            <th scope="col" class="hcolor">범위반명</th>
					            <td scope="col">
					               <select name="searchCrimeCd" id="searchCrimeCd" class="selw6"></select>
					            </td>
					            <th scope="col" class="hcolor">담당자</th>
					            <td scope="col" colspan="4">
					               <input class="input20" type="text" name="searchRegrNm" id="searchRegrNm"/>
					            </td>
					          </tr>
                           </thead>
                        </table>
                        <div  class="btn_c">
				       	<ul>
                          <li><a href="javascript:void(0);" class='gyButton' onclick="fn_incdtSearch(1); return false;">조회</a></li>
                        </ul>   
					  </div>
                      
                      </div>
                     </form>
                      
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
                     <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>                     
                     <!--------------목록---------------------->
                     <div class="t_list">  
                          <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                <caption>사건조회</caption>
                                  <colgroup>
                                      <col width="4%">                                      
                                      <col width="12%">
                                      <col />
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                   </colgroup>
                                    <thead>
                                      <tr>
                                         <th scope="col">순번</th>
                                         <th scope="col">사건번호</th>
                                         <th scope="col">사건명</th>
                                         <th scope="col">죄명</th>
                                         <th scope="col">요청번호</th>
                                         <th scope="col">지원예정일</th>
                                         <th scope="col">담당자</th>
                                         <th scope="col">주임검사</th>
                                         <th scope="col">작성일</th>
                                         <th scope="col">선택</th>                                         
                                      </tr>
                                    </thead>
                                    <tbody id="popList">
                                    </tbody>
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                     <div id='page_navi' class="page_wrap"></div>                            
                       <!-----------------------//페이징----------------------->   
                     <div class="btn_c">
                      <ul>
                        <li><a href="#" class="gyButton" onclick="fn_dialogClose('incdtFindPop');return false;">닫기</a></li>
                        <!-- <li><a href="#" class="myButton" onclick="fn_indexFsysProgramMList();return false;">목록</a></li> -->
                      </ul>
                    </div>                 
                  </div>
                 
            </div>
    </div>
    </div>
</div>
</body>
</html>