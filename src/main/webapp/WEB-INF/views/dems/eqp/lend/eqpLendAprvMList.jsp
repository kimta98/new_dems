<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비 지원 관리 > 장비대여관리 조회
 * 4. 설명 : 장비 지원 관리 > 장비대여관리 조회
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
// var flag = true;
// var perPageNum;
 var tabId;
var userGb = "${session_usergb}";
//alert("===userGb====>>>"+userGb);

$(document).ready(function() {
	
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	var codeInfo2 = [{cdId:'C09',selectId:'schPgsStat',type:'1', callbackNm:'fn_ajaxPgsStatCallback', sqlQueryId:''}];
	/* fn_ajaxCodeList(codeInfo2); */
	gfn_init({startFnNm:'', param:codeInfo2, codeSet:'Y'});
	
	gfn_calendarConfig("schStRentStartDt", "schEdRentStartDt", "minDate", "");    <%/* 대여시작일자 from */%>
	gfn_calendarConfig("schEdRentStartDt", "schStRentStartDt", "maxDate", "");   <%/* 대여시작일자 to */%>
	
	gfn_calendarConfig("schStRentEndDt", "schEdRentEndDt", "minDate", "");    <%/* 대여종료일자 from */%>
	gfn_calendarConfig("schEdRentEndDt", "schStRentEndDt", "maxDate", "");   <%/* 대여종료일자 to */%>
	
	fn_dispCont();
});


function fn_ajaxPgsStatCallback(data){
 	$('#schPgsStat option:eq(0)').before("<option value='' selected>전체</option>");
 	$('#schPgsStat option:eq(1)').before("<option value='reserv'>예약중</option>");
	$('#schPgsStat').on({"change":function(){
		fn_searchEqpLendMList(1);
	}});
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_searchEqpLendMList"});
}

/**
 * 달력 콜백함수(없으면 삭제가능) 
 * @param {string} prgID
 * @returns 
 */
function fn_callBack(){
}


/**
 * @!@ 
 * @param {string} prgID
 * @returns 
 */
function fn_queryCodeList(param) {
	$.each(param.paramRow,function(idx,row){
        $("#"+row.target).append("<option value=''>전체</option>");
    });
	
	$.ajax({
		url:  "<c:url value='/fcom/queryAjaxCodeList.do'/>",
		type : "POST",
		data: JSON.stringify(param),
		dataType: 'json',
		contentType:"application/json",
		success: function(data) {
		    $.each(data, function(idx, dataRow) {
		    	$.each(dataRow, function(idx, rowRow) {
			    	$("#"+rowRow.target).append("<option value='"+rowRow.cd+"'>"+rowRow.cdNm+"</option>");
			    });
		    });
		    
		}
	});
}


/**
 * @!@ 장비 지원 관리 > 장비대여관리 조회
 * @param {int} page
 * @returns 
 */
function fn_searchEqpLendMList(page){
// 	debugger;
	var callUrl = "<c:url value='/eqp/lend/queryEqpLendMList.do'/>";
	
	$("#menuGb").val("aprv");
	$("#page").val(page);
	
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_searchEqpLendMListCallback', page:page, perPageNum:10});
	
// 	var callUrl = "<c:url value='/fbbs/ntc/queryFBbsNtcMList.do'/>";	
// 	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callback',page:page,perPageNum:10});
	
}

 /**
  * 장비 지원 관리 > 장비대여관리 조회 콜백
  * @param {json} data
  * @returns 
  */
 function fn_searchEqpLendMListCallback(data){
//  	debugger;
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listLend > colgroup").find("col").length;
	
	$("#listLend > tbody").empty();
	
 	$("#totalcnt").text(data.totalCount);
	
  	if(listCnt < 1){
  		var append = "";
		append += "<tr>";
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		append += "</tr>";
		$("#listLend > tbody").append(append);
	}else{
	 	
		$.each(list,function(idx,row){
	 		var append = "";
	 		append += "<tr>";
			
	 		//var selValue = $("#dataGubun"+paramIndex).val();
	 		
	 		
	 		if(row.pgsStat=="C09002"){
	 			append +='<td style="text-align:center"><input type="checkbox" name="chk" id="chk" value="Y" class="check_agree1"></td>'; //checked disabled
	 		}else{
	 			append +='<td style="text-align:center"><input type="checkbox" name="chk" id="chk" value="Y" class="check_agree1" disabled></td>';
	 		}
	 		//append +='<td style="text-align:center"><input type="checkbox" name="chk" id="chk" value="Y" class="check_agree1"></td>';
// 	 		append += "<td>" + row.rnum + "</td>";
	 		append += "<td>";
	 		append +=  row.rentAplnSno+"<input type='hidden' name='selRentAplnSno' id='selRentAplnSno' value='"+row.rentAplnSno+"'/>";
	 		append +=  "<input type='hidden' name='rtnCfrmrDt' id='rtnCfrmrDt' value='"+gfn_dashDate2(gfn_getCurDate(),'-')+"'/>";
	 		append +=  "</td>";
// 			append += "<td>" + row.rentAplnSno+"</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.dispRegDt) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.insttCdNm) + "</td>";
	 		append += "<td><a href='javascript:void(0)' onclick=javascript:fn_searchDetail('"+row.rentAplnSno+"','"+data.page+"','"+row.regrId+"','"+row.pgsStat+"')><u>"+row.eqpNm+"</u></a></td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.eqpTypNm) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.srNo) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.incdtNo) + "</td>";
	 		append += "<td>" + gfn_nullRtnSpace(row.rentUserNm) + "</td>";
	 		append += "<td>" + gfn_dashDate2(gfn_nullRtnSpace(row.rentStartDt),'-') + "</td>";
	 		append += "<td>" + gfn_dashDate2(gfn_nullRtnSpace(row.rentEndDt),'-') + "</td>";
	 		
	 		if(row.pgsStat =="C09002" || row.pgsStat =="C09003"){
	 			append += "<td>" + gfn_nullRtnSpace(row.rentCfrmrId) +"<br/>("+gfn_dashDate2(gfn_nullRtnSpace(row.rentCfrmDt),'-')+ ")</td>";
	 		}else{
	 			append += "<td>&nbsp;</td>"
	 		}
	 		
	 		if(row.pgsStat=="C09003"){
	 			append += "<td>" + gfn_nullRtnSpace(row.rtnCfrmrId) +"<br/>("+gfn_dashDate2(gfn_nullRtnSpace(row.rtnCfrmrDt),'-')+ ")</td>";
	 		}else{
	 			append += "<td>&nbsp;</td>"
	 		}
	 		
	 		append += "<td>" + gfn_nullRtnSpace(row.pgsStatNm) + "</td>";
	 		
	 		append += "</tr>";
	        $("#listLend > tbody").append(append);
	  	});
	}
  	
  	data.__callFuncName__ ="fn_searchEqpLendMList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
 	
 } 
 
function fn_insEqpLend(){
	
	//var flag = "MntList";
	//parent.$('#tabs-M000000702').find("iframe").attr("src", '<c:url value="/eqp/lend/indexEqpLendLendReqRDtl.do"/>?eqpSno='+eqpSno+'&flag='+flag);
// 	console.log("=======fn_insEqpLend ST=======>>>");
// 	parent.$('#tabs-M000000702').find("iframe").attr("src", '<c:url value="/eqp/lend/indexEqpLendLendReqRDtl.do"/>');
// 	console.log("=======fn_insEqpLend ED=======>>>"); <input type="hidden" id="modFlag" name="modFlag" value="insert" />
	
	var modFlag = "insert";
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/eqp/lend/indexEqpLendLendReqRDtl.do"/>?modFlag='+modFlag);
}


function fn_searchDetail(rentAplnSno, page, regrId, pgsStat){

	//alert("=========session_userid======>>>"+session_userid+"\n=========regrId======>>>"+regrId+"\n=========pgsStat======>>>"+pgsStat+"\n=========rentAplnSno======>>>"+rentAplnSno);
	var src="";
	var searchParam;
	
	$('#rentAplnSno').val(rentAplnSno);
	
 	
	
	if((session_userid == regrId) && (gfn_isNull(pgsStat) || pgsStat == 'C09001')){
		$('#modFlag').val("modify");
		searchParam = $('#searchForm').serialize();
		$('#rentAplnSno').val("");
		src = "<c:url value = "/eqp/lend/indexEqpLendAprvUDtl.do"/>?page="+page+"&"+searchParam;
	}else{
		$('#modFlag').val("aprv");
		searchParam = $('#searchForm').serialize();	
		$('#rentAplnSno').val("");
		src = "<c:url value = "/eqp/lend/indexEqpLendAprvRDtl.do"/>?page="+page+"&"+searchParam;
	}
	
// 	var src = "<c:url value = "/eqp/lend/indexEqpLendAprvRDtl.do"/>?page="+page+"&"+searchParam;
	
	requestUtil.setSearchForm("searchForm");
 	parent.$('#'+tabId+' iframe').attr('src', src);	
}

function fn_rtnEqpLend(reqType){
	var chkCnt = 0;
	$('#listLend :input[name=chk]').each(function(index) {
		//console.log("====fn_selMgmt	index========>>>"+index);
		 if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
			 chkCnt++;
			 console.log("========fn_rtnEqpLend	selRentAplnSno222===========>>>>>"+$("input[name^=selRentAplnSno]").eq(index+1).val());
		 }
	});
	
	if(chkCnt < 1){
		fn_showUserPage("반납처리할 내역을 선택하십시요.");
		return;
	}
	
	fn_modifyEqpLend(reqType, chkCnt);
}

/**
* 분석지원요청 진행상태 변경요청
* @param
* @returns
*/
function fn_modifyEqpLend(reqType, chkCnt){
	var chkCnt = 0;
	var tpArray = [];
	var Objtype = {};	
	var isChk = false;
	var curRentAplnSno = "";
	var nextRentAplnSno = "";
	var arr = new Array();
	var arr2 = new Array();
	var i = 0;
	var compareRentAplnSno = "";
	var tpArray = [];
	var tpArray2 = [];
	
	//alert(reqType);
	
	$('#listLend :input[id=chk]').each(function(index) {
		//console.log("========compareRentAplnSno 1111111===>>>"+i);
		if($("input:checkbox[id=chk]").eq(index).is(":checked")==true){
		//if($("input[name='chk']").prop("checked")==true){
		//if($("input[name='chk']").attr("checked")==true){
		//if($("input[name='chk']").attr("checked")==true){	
			console.log("========99999999999999999=======>>>"+$("input[id^=selRentAplnSno]").eq(index).val());
			tpArray.push($("input[name^=selRentAplnSno]").eq(index).val());
		}
	});
	
	for(var i=0;i<tpArray.length;i++){
		console.log("========tpArray["+i+"]=======>>>"+tpArray[i]);
	}
	
	//console.log("========tpArray.length===>>>"+tpArray.length);
	
	
	
	for(var i=0;i<tpArray.length;i++){
		//console.log("========tpArray["+i+"]=======>>>"+tpArray[i]);
		compareRentAplnSno = tpArray[i];
		console.log("========current compareRentAplnSno ======>>>"+compareRentAplnSno);
		
		var chkDupCnt2 = 0;
		for(var y=0;y<tpArray2.length;y++){
			console.log("========tpArray2["+y+"] ======>>>"+tpArray2[y]);
			if(tpArray2[y] == compareRentAplnSno){
				//tpArray2[i] = compareRentAplnSno
				chkDupCnt2++;
				//console.log("========chkDupCnt2 ======>>>"+chkDupCnt2);
			}
		}
		
		console.log("========chkDupCnt2["+i+"] =======>>>"+chkDupCnt2);
		
		if(chkDupCnt2 < 1){
// 			tpArray2[i] = compareRentAplnSno;
			console.log("========chkDupCnt2 < 1 compareRentAplnSno=======>>>"+compareRentAplnSno);
			tpArray2.push(compareRentAplnSno);
		}
	}
	
	for(var i=0;i<tpArray2.length;i++){
		console.log("========tpArray2["+i+"]=======>>>"+tpArray2[i]);
	}
	
	
	
	fn_showModalPage("반납처리 하시겠습니까?", function() {
		
		Objtype.type = reqType;
		var data = {rowDatas : tpArray2, type : Objtype};
	    var callUrl = "<c:url value='/eqp/lend/ModifySelEqpLendRequest.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyEqpLendCallBack'});
	    
// 		$("#rentAplnSno").val(rentAplnSno);
// 		var data = {rowDatas : tpArray, formDatas1 : formObj1, formDatas2 : formObj2, formDatas3 : formObj3};
// 	    var callUrl = "<c:url value='/eqp/lend/lendReqEqpLendLendReqRDtl.do'/>";
// 	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyEqpLendCallBack'});
	});
	
	
	
}

/**
* 분석지원요청 진행상태 변경요청
* @param
* @returns
*/
// function fn_modifyEqpLend(reqType, chkCnt){
// 	var chkCnt = 0;
// 	var tpArray = [];
// 	var Objtype = {};	
// 	var isChk = false;
// 	var beforeRentAplnSno = "";
// 	var afterRentAplnSno = "";
// 	//alert(reqType);
	
// 	$('#listLend :input[name=chk]').each(function(index) {
// 		//console.log("====fn_selMgmt	index========>>>"+index);
// 		if(chkCnt > 1){
// 			console.log("==== chkCnt>1========>>>"+chkCnt);
// 			if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
// 				beforeRentAplnSno = $("input[name^=selRentAplnSno]").eq(index).val();  
// 				tpArray.push(afterRentAplnSno);
				
// 				$('#listLend :input[name=chk]').each(function(index) {
// 					 afterRentAplnSno = $("input[name^=selRentAplnSno]").eq(index+1).val();  
// 					 if(beforeRentAplnSno != afterRentAplnSno){
// 						 tpArray.push(afterRentAplnSno);
// 					 }
// 				 });
// 			}
// 		}else{
// 			console.log("==== chkCnt==0 ========>>>"+chkCnt);	
// 			if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
// 				afterRentAplnSno = $("input[name^=selRentAplnSno]").eq(index).val();
// 				tpArray.push(afterRentAplnSno);
// 			}
// 		}
// 	});
	
// 	console.log("========chkCnt=======>>>"+chkCnt);
// 	for(var i=0;i<tpArray.length;i++){
// 		console.log("========tpArray["+i+"]=======>>>"+tpArray[i]);
// 	}
	
	
// 	fn_showModalPage("반납처리 하시겠습니까?", function() {
		
// 		Objtype.type = reqType;
// 		var data = {rowDatas : tpArray, type : Objtype};
// 	    var callUrl = "<c:url value='/eqp/lend/ModifySelEqpLendRequest.do'/>";
// 	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyEqpLendCallBack'});
	    
// // 		$("#rentAplnSno").val(rentAplnSno);
// // 		var data = {rowDatas : tpArray, formDatas1 : formObj1, formDatas2 : formObj2, formDatas3 : formObj3};
// // 	    var callUrl = "<c:url value='/eqp/lend/lendReqEqpLendLendReqRDtl.do'/>";
// // 	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyEqpLendCallBack'});
// 	});
	
	
	
// }

/**
* 반납처리 콜백
* @param
* @returns
*/
function fn_modifyEqpLendCallBack(){
	fn_searchEqpLendMList(1);
}

/**
 * 화면 컨트롤
 * @param  
 * @returns
 */
 function fn_dispCont(){
 	
	 console.log("========사용자구분 session_usergb=====>>>"+session_usergb);
	 //console.log("========대여진행상태2 pgsStat2=====>>>"+pgsStat);
	
	$("#btn_lenIsn").hide(); //등록
	$("#btn_lenRtn").hide(); 
	
	//사용자구분 (C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)  
	if(session_usergb == "C01003"){ //포렌식수사관
		$("#btn_lenIsn").show(); //등록
		$("#btn_lenRtn").show(); //반납
	}
	
 	
 }

</script>
</head>
<body>

<div id="con_wrap">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div>
            <!-- 
                  <div><h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3></div>
             -->
                 <div class="loca">
                  <!--  <h3>공지사항 <c:if test="${loginVO.userGb == 'C01999'}">관리</c:if></h3>//-->
                    <div class="ttl"></div>
                    <div class="loca_list">Home > 장비 지원 관리 > 장비 대여 관리</div>
                  </div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
<!--                     <form id="searchForm" name="searchForm" method="post"> -->
					 <form id="searchForm" name="searchForm" onsubmit="return false;">	
					 <input type="hidden" class="" id="page" name="page" value="1"/>
                    <div class="t_head">
				        <input type="hidden" id="userGb"   name="userGb"   value="C00000"/>
				        <input type="hidden" id="flag" name="flag" value="" />
				        <!-- <input type="hidden" name="rentAplnSno" id="rentAplnSno" value="" /> -->
				        <input type="hidden" name="modFlag" id="modFlag" value="" />
				        <input type="hidden" name="menuGb" id="menuGb" value="" />
				        
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_searchEqpLendMList(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="11%">
                                   <col width="22%">
                                   <col width="11%">
                                   <col width="23%">
                                   <col width="11%">
                                   <col width="22%">
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">장비명</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schEqpNm" id="schEqpNm" style="width:300px;" maxlength="100"/>
					            </td>
					            <th scope="col" class="hcolor">사건명</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schIncdtNm" id="schIncdtNm" style="width:300px;" maxlength="100"/>
					            </td>
					            <th scope="col" class="hcolor">담당자명</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="schRentUserNm" id="schRentUserNm" style="width:300px;" maxlength="25"/>
					            </td>
					            
					          </tr>
					          <tr>
					            <th scope="col" class="hcolor">시작일자</th>
					            <td scope="col" >
					               <input id="schStRentStartDt" name="schStRentStartDt"  title="대여시작일자(from)"  type="text" value="" style='width:100px' maxlength="10"/> 
					               <input id="schEdRentStartDt" name="schEdRentStartDt"  title="대여시작일자(to)"  type="text" value="" style='width:100px' maxlength="10"/>
					            </td>
					          	<th scope="col" class="hcolor">종료일자</th>
					          	<td scope="col" >
					               <input id="schStRentEndDt" name="schStRentEndDt"  title="대여종료일자(from)"  type="text" value="" style='width:100px' maxlength="10"/> 
					               <input id="schEdRentEndDt" name="schEdRentEndDt"  title="대여종료일자(to)"  type="text" value="" style='width:100px' maxlength="10"/>
					            </td>
					            <th scope="col" class="hcolor">대여상태</th>
					          	<td scope="col" >
					                <select class="" id="schPgsStat" name="schPgsStat" onchange="fn_searchEqpLendMList(1);" style=width:80px;" >
					                </select> 
					            </td>
					          </tr>  
					          <tr>
					          	<th scope="col" class="hcolor">요청번호</th>
					            <td scope="col" >
					               <input class="b_put" type="text" name="rentAplnSno" id="rentAplnSno" style="width:300px;" maxlength="100"/>
					            </td>
					            <th scope="col" class="hcolor"></th>
					            <td scope="col" >
					            </td>
					            <th scope="col" class="hcolor"></th>
					            <td scope="col" >
					            </td>
					          </tr>  
                           </thead>
                        </table>
                      
                      </div>
                      <div  class="btn_c">
					       <ul>
                             
<%--                                <c:if test="${loginVO.userGb == 'C01999'}"> --%>
							 <li><a href="javascript:void(0);" class='myButton' onclick="fn_rtnEqpLend('rtnCfrm'); return false;" id="btn_lenRtn" name="btn_lenRtn" style="display: none;">반납처리</a></li> 
							 <li><a href="javascript:void(0);" class='RdButton' onclick="fn_insEqpLend(); return false;" id="btn_lenIsn" name="btn_lenIsn" style="display: none;">등록</a></li>
                             <li><a href="javascript:void(0);" class='gyButton' onclick="fn_searchEqpLendMList(1); return false;">조회</a></li>
<!--                              <li><a href="javascript:void(0);" class='RdButton' onclick="fn_insAnlsReq(); return false;">등록</a></li> -->
<%--                                </c:if> --%>
                             </ul>   
					  </div>
                     </form>
                       
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
<!--                      <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div> -->
<!--                      <div class="bo_num"> -->
<!--                          <select id="perPageNum" name="perPageNum"> -->
<!-- 			               <option value="5">5개씩</option> -->
<!-- 			               <option value="10" selected="selected">10개씩</option>		                -->
<!-- 			             </select> -->
<!--                      </div> -->
					 <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>
                     
                     <!--------------목록---------------------->
                     <div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:450px;">  
                          <table id="listLend" class="tbl_type" border="1" cellspacing="0" >
                                <caption>사업관리</caption>
                                  <colgroup>
                                  	  <col width="5%">	<%/* 체크박스 */%>
<%--                                       <col width="5%">	<%/* 순번 */%> --%>
                                      <col width="7%">		  	<%/* 요청번호 */%> 
                                      <col width="7%">	<%/* 등록일자 */%>
                                      <col width="11%">	<%/* 부서명 */%>
                                      <col />	<%/* 장비명 */%>
                                      <col width="7%">	<%/* 장비유형 */%>
                                      <col width="7%">	<%/* S/N */%>
                                      <col width="7%">	<%/* 사건번호 */%>
                                      <col width="7%">	<%/* 담당자 */%>
                                      <col width="6%">	<%/* 시작일 */%>
                                      <col width="6%">	<%/* 종료일 */%>
                                      <col width="6%">	<%/* 대여승인 */%>
                                      <col width="6%">	<%/* 반납확인 */%>
                                      <col width="6%">	<%/* 상태 */%>
                                      </colgroup>
                                    <thead>
                                      <tr>
                                      	 <th scope="col">선택</th>
<!--                                          <th scope="col">순번</th> -->
                                         <th scope="col">요청번호</th>
                                         <th scope="col">등록일자</th>
                                         <th scope="col">부서명</th>
                                         <th scope="col">장비명</th>
                                         <th scope="col">장비유형</th>
                                         <th scope="col">S/N</th>
                                         <th scope="col">사건번호</th>
                                         <th scope="col">담당자</th>
                                         <th scope="col">시작일</th>
                                         <th scope="col">종료일</th>
                                         <th scope="col">대여승인</th>
                                         <th scope="col">반납확인</th>
                                         <th scope="col">상태</th>
                                      </tr>
                                    </thead>
                                    <tbody id="lendAprvList">
                                    </tbody>
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                    <div id="page_navi" class="page_wrap"></div>                   
                       <!-----------------------//페이징----------------------->
                 
                  </div>
                 
            </div>
    </div>
</div>

</body>
</html>