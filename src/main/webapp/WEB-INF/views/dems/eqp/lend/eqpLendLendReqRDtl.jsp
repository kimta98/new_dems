<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:49:11
 * 2. 작성자 : ilyong
 * 3. 화면명 : 장비관리 > 장비대여 등록
 * 4. 설명 : 장비관리 > 장비대여 등록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var PW_Error = 0;
var ID_Duple = 1;

// console.log("=====session_userid=========>>>"+session_userid+"\n=====session_insttcd=========>>>"+session_insttcd);
// console.log("=====session_usernm=========>>>"+session_usernm+"\n=====session_usergb=========>>>"+session_usergb);
// console.log("=====session_usergbnm=========>>>"+session_usergbnm+"\n=====session_insttnm=========>>>"+session_insttnm);
// console.log("=====session_deptnm=========>>>"+session_deptnm+"\n=====session_telno=========>>>"+session_telno);
// console.log("=====session_hptelno=========>>>"+session_hptelno);

var modFlag = '${param.modFlag}'; //매입처명
//var userGb = "${session_usergb}";
//alert("=======modFlag======>>>"+modFlag);

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');	
	
	<%/* 파일업로드 세팅 */%>
    gfn_fileUpload("atchFile", "fileList4","file", 4);
	
	var codeInfo = [{cdId:'C12',selectId:'cntrForm',type:'1', callbackNm:'fn_ajaxCntrFormCallback', sqlQueryId:''}];
	fn_ajaxCodeList(codeInfo);
	
	<%/* 달력 세팅 */%> 
 	gfn_calendarConfig("rentAplnDt", "", "", "");    <%/* 대여신청일자 */%> 
 	gfn_calendarConfig("rentStartDt", "rentEndDt", "minDate", "");    <%/* 대여시작일자 from */%>
	gfn_calendarConfig("rentEndDt", "rentStartDt", "maxDate", "");   <%/* 대여종료일자 to */%>
	
	
	<%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
    gfn_overMaxLength("rentReqInfo",250);
<%-- 	gfn_toNumber("cntrAmt"); <%/* 계약금액 */%> --%>
	
	$("#rentAplnDt").val(gfn_getDate());
	
	
	
// 	$("#addRow").on("click", function(event) {
	
	
	
	
	//장비 상세 정보 조회
// 	requestUtil.search({callUrl:"<c:url value='/eqp/mgmt/queryEqpMgmtUDtl.do'/>", srhFormNm:'insForm', callbackNm:'fn_queryEqpMgmtCallBack'}); 
	
<%-- 	<%/* 전체 checkbox 클릭시 */%> --%>
//     $('#chkAll').change(function() {
//         var chk = $(this).is(':checked');
//         if(chk){
//                $('input:checkbox[id=chk]').each(function() {
//                    $(this).prop("checked", true);         
//                });
//         }else{
//                $('input:checkbox[id=chk]').each(function() {
//                    $(this).prop("checked", false);         
//                });
//         }
//     });
    
	
//     fn_addRow();
    
	fn_dispDefaultInfo();
    
});

function fn_ajaxCntrFormCallback(data){
	$('#cntrForm option:eq(0)').before("<option value='' selected>선택</option");
}

/**
* 장비 상세 정보 조회 콜백
* @param  
* @returns
*/
function fn_queryEqpMgmtCallBack(data){
	
	var eqpSno = data.resultMap.eqpSno;	
	var eqpNm = data.resultMap.eqpNm;
	var eqpTypNm = data.resultMap.eqpTypNm;
	var srNo = gfn_nullRtnSpace(data.resultMap.srNo);
	var deprPrid = gfn_nullRtnSpace(data.resultMap.deprPrid);
	var purcDt = gfn_dashDate2(gfn_nullRtnSpace(data.resultMap.purcDt));
	var exprDt = gfn_dashDate2(gfn_nullRtnSpace(data.resultMap.exprDt));
	var mnftCo = gfn_nullRtnSpace(data.resultMap.mnftCo);
	
	$("#eqpSno").text(eqpSno); 		<%/* 장비번호 */%>
	$("#eqpNm").text(eqpNm); 		<%/* 장비명 */%>
	$("#eqpTypNm").text(eqpTypNm); 	<%/* 장비유형 */%>
	$("#srNo").text(srNo); 			<%/* S/N */%>
	
	$("#deprPrid").text(deprPrid); 	<%/* 내용연수 */%>
	$("#purcDt").text(purcDt); 		<%/* 도입일 */%>
	$("#exprDt").text(exprDt); 		<%/* 만료일 */%>
	$("#mnftCo").text(mnftCo); 		<%/* 제조사 */%>
	
}


/**
* 첨부파일갯수 체크
* @param  
* @returns
*/
function fn_dispDefaultInfo(){
	console.log("=====1session_userid=========>>>"+session_userid+"\n=====session_insttcd=========>>>"+session_insttcd);
	console.log("=====2session_usernm=========>>>"+session_usernm+"\n=====session_usergb=========>>>"+session_usergb);
	console.log("=====3session_usergbnm=========>>>"+session_usergbnm+"\n=====session_insttnm=========>>>"+session_insttnm);
	console.log("=====4session_deptnm=========>>>"+session_deptnm+"\n=====session_telno=========>>>"+session_telno);
	console.log("=====5session_hptelno=========>>>"+session_hptelno);
	
	var dataArray = new Array();
	dataArray = session_deptnm.split(" ");
    
	var deptNm = "";
	var teamNm = "";
    
	if(dataArray.length > 0){
		deptNm = dataArray[0]; <%/* 과명 */%>
		teamNm = dataArray[1]; <%/* 팀명 */%>
	}
		
	//console.log("=====과명=========>>>"+deptNm+"\n=====팀명=========>>>"+teamNm);
	$("#dispRentDeptNm").text(deptNm);
	$("#dispRentTeamNm").text(teamNm);
	$("#dispRentUserTelNo").text(session_hptelno !=null ?  session_usernm+" ("+gfn_getTelNo(session_telno)+", "+gfn_getTelNo(session_hptelno)+")": session_usernm+" ("+gfn_getTelNo(session_telno)+")");
}

function gfn_getTelNo(that){
	
	//alert("=========that=====>>>>"+that)
	var returnValue = that.replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-"); 
	
	//alert("=========returnValue=====>>>>"+returnValue)
	//$(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
	return returnValue;
 }

/**
* 첨부파일갯수 체크
* @param  
* @returns
*/
function fn_fileCntChk(){
    var numCnt=0;
    $('#divFile :input[id^=fileList4FileSno]').each(function(index) {
        numCnt++;
    });

    if(numCnt > 4){
        $("#atchFile").hide();
    }else{
        $("#atchFile").show();
    }
}

/**
 * 자유게시판 파일다운로드
 * @param {String} filePath 파일경로
 * @param {String} fileNm   파일이름
 * @param {String} atchFileNm 실제파일이름
 * @returns
 */
function fn_fileDown(filePath,fileNm,atchFileNm) {
    gfn_fileNmDownload(filePath,fileNm,encodeURI(atchFileNm));
}

function fn_prgdetail() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"frame/fsys/menu/fsysMenuSchPrgmPop",
		height: 700,
        width: 1000,
        title: '프로그램 목록',
        divId : 'divPrgPopup'
	});

}

//장비대여신청 임시저장
function fn_modifyEqpLend(saveGb){
	var conMsg = "";
	if(saveGb=="req"){
		conMsg = "장비대여신청 하시겠습니까?";
		$("#pgsStat").val("C09001");
	}else{
		conMsg = "임시저장 하시겠습니까?";
		$("#pgsStat").val("");
	}
	
	$("#saveGb").val(saveGb);
	
	if(!fn_valChk()) return;
	if(!fn_saveCntChk()) return;
	
	console.log("==========11111111111======>>>");
	//장비상세정보
	var tpFormArry = [];
	var $formDatas1 = $('#insForm').find('#tbMgmtDtl');
	var $formDatas2 = $('#insForm').find('#tbMgmtDtl2');
	console.log("==========222222222222======>>>");
	var formObj1 = new Object();
	var formObj2 = new Object();
	var cnt = 0;
	var cnt2 = 0;
	console.log("==========333333333333======>>>");
	$formDatas1.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			formObj1[item.name] = item.value;
			cnt++;
			console.log("========formDatas1["+index+"]========>>>>"+item.value);
		});
		
// 		dataObj = {};

	});
	
	$formDatas2.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			formObj2[item.name] = item.value;
			cnt2++;
			console.log("========formDatas2["+index+"]========>>>>"+item.value);
		});
		
// 		dataObj = {};

	});
	
	//alert("======cnt======>>>"+cnt);
	//tbMgmtDtl
	//tbMntDtl
	
	//유지보수 상세정보
	var tpArray = [];
	var dataObj = new Object();
	var cnt3 = 0;

	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){
			dataObj[item.name] = item.value;
			//console.log("==========item.name======>>>"+item.name+"\n==========item.value======>>>"+item.value);
			//cnt3++;
		});

// 		if(index % 2 == 0){
			tpArray.push(dataObj);
			dataObj = {};
// 		}

	});
	
	//alert("======cnt2======>>>"+cnt2);
	fn_showModalPage(conMsg, function() {
		var data = {rowDatas : tpArray, formDatas1 : formObj1, formDatas2 : formObj2};
	    var callUrl = "<c:url value='/eqp/lend/lendReqEqpLendLendReqRDtl.do'/>";
	    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyEqpLendCallBack'});
	});
	
	
}

function fn_modifyEqpLendCallBack(data){
	fn_searchList();
	//alert("임시저장 성공");
}

//이메일 체크
function emailCheck(email) {
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
    if ( !email.match(regExp) ) {	return false;    } else {		return true;    }
}

//비밀번호 비교
function fn_saveComparePW(){
	var pw = $('input[id=pwd]').val();
	var pw2 = $('input[id=pwdConfirm]').val();
	if(pw != pw2)
	{
		$("#comparePw").attr("style","visibility: visible;");
		PW_Error = 1;	
	}
	else
	{
		$("#comparePw").attr("style","visibility: hidden;");
		PW_Error = 0;	
	}
}


// /fsys/user/indexFsysUserMList.do

function fn_searchList(){
	//debugger;
	//alert("====insForm serialize=========>>>>"+$("#insForm").serialize());
	//parent.addNaviTab('관리자 > 사용자관리 > 급식기관관리상세',"<c:url value="/eqp/mnt/indexEqpMntMList.do"/>",'급식기관관리상세','M000000703');
	//var flag = $("#flag").val();
	//alert("====flag=======>>>"+flag);
// 	if(flag == "MgmtList"){
// 		parent.$('#tabs-M000000701').find("iframe").attr("src", '<c:url value="/eqp/mgmt/indexEqpMgmtMList.do"/>');
// 	}else{
// 		parent.$('#tabs-M000000703').find("iframe").attr("src", '<c:url value="/eqp/mnt/indexEqpMntMList.do"/>');
// 	}
	
	//var param = "?"+$("#searchForm").serialize();
	var param = "";
	var gotoUrl = "";
	
	if(session_usergb == "C01003"){ //포렌식수사관
		gotoUrl = '<c:url value="/eqp/lend/indexEqpLendAprvMList.do"/>';
	}else{
		gotoUrl = '<c:url value="/eqp/lend/indexEqpLendMList.do"/>';
	}
	parent.$('#'+tabId+' iframe').attr('src', gotoUrl+param);	
}


/**
 * @ 메뉴 관리 등록 콜백
 * @param {json} data
 * @returns 
 */
function fn_insEqpMgmtCallback(data){
	 fn_searchList();
}

<%/*아이디 중복 체크*/%>
function fn_IDcheck(){
	
	var checkParamId = $("#userId").val(); 
// 	alert(checkParamId.search(/\s/));
	
	if( checkParamId.search(/\s/) > 0 ){
		alert('ID에 공백이 들어갈 수 없습니다.');
		$("#userId").focus();
		return;
	} 
	
	if(checkParamId == ""){
		alert("아이디를 입력해 주세요.");
		$("#userId").focus();
		return;
	}else{
		var callUrl = "<c:url value='/fsys/user/sysUserCheckId.do'/>";
		requestUtil.search({callUrl:callUrl,srhFormNm:'insForm',setFormNm:'insForm',callbackNm:'fn_callbackpop'});
	}
	
}

function fn_callbackpop(data) {
	
	var resultCnt = data.egovMap.resultCnt;
// 	alert("fn_callbackpop 진입 resultCnt===>>> "+resultCnt);
// 	if(resultCnt > 0){
// 		alert("111111");
// 	}else{
// 		alert("2222222");
// 	}
// 	var existsCnt = resultCnt.substring(0,resultCnt.indexOf('-'));
// 	alert("fn_callbackpop 진입 1-1");
	if( resultCnt > 0 ) {
		alert('이미 존재하는 ID입니다.');
		$('#userId').val('');
		ID_Duple=1;
		return;
	} else {
		alert('사용가능합니다.');
		ID_Duple =0;
		return;
	}
	
}

/**
 * @ 장비 관리 삭제
 * @param
 * @returns 
 */
function fn_deleteEqpMgmt() {
	
	if(confirm("삭제 하시겠습니까?")){
		
		//사용자 정보 수정
        var callUrl = "<c:url value='/eqp/biz/delEqpBizUDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_deleteEqpBizCallback'});
	
	}
	
}

/**
 * @!@ 메뉴 관리 삭제 콜백
 * @param
 * @returns 
 */
function fn_deleteEqpBizCallback(data){
	fn_searchList();
}

/**
 * @!@ 사업관리조회 팝업
 * @param cd
 * @returns 
 */
function fn_searchEqpBizMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/mgmt/eqpMgmtFIndEqupQListPop&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 650,
        width: 1000,
        title: '장비조회 팝업',
        divId : 'eqpMgmtFIndEqupQListPop'
        //divId : 'eqpMgmtMListPop'
	});

}

<%/* 시작 처리 function */%>
function fn_addRow() {
// 	$("#addRow").click(function(){
		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length+1;

		var tpTag =     '<tr>'+
			'<td style="text-align:center"><input type="checkbox" name="chk" id="chk" value="Y" class="check_agree1"></td>'+
			'<td><textarea id="box" name="dfecCnts" rows=2 cols=30 maxlength="2000"></textarea></td>'+
			'<td><textarea id="box" name="rprCnts" rows=2 cols=30 maxlength="200"></textarea></td>'+
			'<td "text-align:center"><input type="text" name="rprStartDt" id="rprStartDt_'+idx+'" class="inpw50"></td>'+
			'<td "text-align:center"><input type="text" name="rprEndDt" id="rprEndDt_'+idx+'" class="inpw50"></td>'+
			'<td "text-align:center"><input type="text" name="rprCoNm" class="inpw70" maxlength="50"></td>'+
			'<td "text-align:center"><input type="text" name="coCrgr" class="inpw70" maxlength="50"></td>'+
		'</tr>';
		$("#detailTbody").append(tpTag);

		
		gfn_calendarConfig("rprStartDt_"+idx, "", "", "");
		gfn_calendarConfig("rprEndDt_"+idx, "", "", "");
// 	});
		
}

<%/* 삭제버튼 클릭 function */%>
function fn_delRow() {
	var cnt = 0;
	$('#detailTbody > tr').each(function(index,item){
		var $chkbox = $(item).find('input[type=checkbox]');
		$chkbox.each(function(index,item){

			if($(this).is(':checked') == true){
				cnt++;
				//$(this).parent().parent('tr').next().remove();
				$(this).parent().parent('tr').remove();
			}
		})

	});

	if(cnt < 1){
		//alert('선택해주세요');
		fn_showUserPage( "삭제할 내역을 선택하십시요.", function() {
			return;		
        });
	}


//		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length/2;
	

//		for(var i = 1; i <= idx; i++){
//			if(i == 1){
//				$('#detailTbody > tr').eq(0).find('td:eq(1)').text(i);
//				$('#detailTbody > tr').eq(0).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
//				$('#detailTbody > tr').eq(0).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
//				$('#detailTbody > tr').eq(1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
//			}else{
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(1)').text(i);
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
//				$('#detailTbody > tr').eq(i*2-2).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
//				$('#detailTbody > tr').eq(i*2-1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
//			}

//		}
//		fn_makeSbx(idx);
//		gfn_calendarConfig("cfscDate_"+idx, "", "", "");
		
}

<%/* 필수입력체크 */%>
function fn_valChk(){
	
	var chkCnt = 0;
	var isValid = true;
	
	var rentAplnDt = $("#rentAplnDt").val();
	var rentStartDt = $("#rentStartDt").val();
	var rentEndDt = $("#rentEndDt").val();
	var rentReqInfo = $("#rentReqInfo").val();
	
	if(rentAplnDt.length < 1){	
		fn_showUserPage("요청일자를 선택하세요.", function() {
			$("#rentAplnDt").focus();
		});
		return false;  
	}else if(rentStartDt.length < 1){	
		fn_showUserPage("대여시작일자를 선택하세요.", function() {
			$("#rentStartDt").focus();
		});
		return false;  
	}else if(rentEndDt.length < 1){
		fn_showUserPage("대여종료일자를 선택하세요.", function() {
			$("#rentEndDt").focus();
		});
		return false;
	}else if(rentReqInfo.length < 1){
		fn_showUserPage("요청사항을 입력하세요.", function() {
			$("#rentReqInfo").focus();
		});
		return false;
	}
	return true;

}
	
function fn_saveCntChk(){
	var chkCnt = 0; 
	$('#tbLendDtl :input[name=chk]').each(function(index) {
		// 체크여부 확인
		//if($("input:checkbox[name=chk]").eq(index).is(":checked")==true){
			chkCnt++;
		//}
	});
	
	if(chkCnt<=0){
		fn_showUserPage( "장비를 선택하세요.", function() {
				
        });
		return false;	
	}	
	return true;
	
       
}

/**
 * @!@ 사건관리 조회 팝업
 * @param cdId
 * @returns 
 */
function fn_incdtIncdtMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtMListPop",
		height: 700,
        width: 1000,
        title: '사건관리 조회',
        divId : 'incdtIncdtMListPop'
	});

}

/**
 * @!@ 장비조회 팝업
 * @param cd
 * @returns 
 */
function fn_searchEqpBizMListPop2() {
   
	 var rentStartDt = $("#rentStartDt").val();
	 var rentEndDt = $("#rentEndDt").val();
	 
	 if(gfn_isNull(rentStartDt)||gfn_isNull(rentEndDt)){
		 fn_showUserPage( "대여기간을 선택하신 후 장비를 추가하세요.", function() {
				
	     });
		 return;	 
	 }
	
//  	fn_showUserPage("이미 추가된 장비는 선택할 수 없습니다. \n ggg \n ggg \n ggg \n ggg");
// 	return;
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		//popUrl : callUrl+"?link="+"dems/eqp/mgmt/eqpMgmtFIndEqupQListPop2&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		popUrl : callUrl+"?link="+"dems/eqp/lend/eqpMgmtFIndEqupQListPop3&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 650,
        width: 1000,
        title: '장비조회 팝업',
        //divId : 'eqpMgmtFIndEqupQListPop2'
        divId : 'eqpMgmtFIndEqupQListPop3'
        //divId : 'eqpMgmtMListPop'
	});

}


/**
 * @!@ 사건조회 팝업
 * @param cd
 * @returns 
 */
function fn_searchIncdtListPop() {
   
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/eqp/lend/incdtFindPop&paramInsttCd="+$("#searchInsttCd2").val()+"&paramInsttCdNm="+$("#searchInsttCdNm2").val(),
		height: 700,
        width: 1000,
        title: '사건조회 팝업',
        divId : 'incdtFindPop'
        //divId : 'eqpMgmtMListPop'
	});

}

/**
* 분석지원요청 진행관리 팝업 콜백함수
* @param {object} data 조회한 결과데이터
* @param {string} divId 팝업div아이디
* @returns
*/
function fn_popCallBack(data, divId){
    //alert("99999999999999");
	$("#"+divId).dialog( "close" );
	$("#"+divId).empty();
	
	$.each(data, function(index, value){
		if($('#'+index).is('span')){
			$('#'+index).text(value);
		}else{
			$('#'+index).val(value);
		}
	});
	
}

</script>

</head>

<body>
<div id="con_wrap1">
        <div class="content">
           <!----현재위치----->
             
            <div id="contents_info">
                 <div class="sub_ttl">장비 대여 신청</div><!-----타이틀------>
                 
                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">
<%-- 					 <input type="text" id="modFlag" name="modFlag" value="<c:out value="${param.modFlag}" />" /> --%>
					 
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:130px;">
						
						
						<table class="iptTblX2" id="tbMgmtDtl">
							 
							<caption>등록</caption>
			               <colgroup>
				             <col width="15%" />
				             <col width="35%" />
				             <col width="15%" />
				             <col width="*" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">요청기관</th>
				                 <td>
				                 	<span id="dispRentDeptNm"></span>
<%-- 				                 	<input type="text" id="bizNm" name="bizNm"  value= "<c:out value='${session_telno}' />" maxlength="100"/> --%>

				                 </td>
				                 <th scope="row">요청부서</th>
				                 <td >
				                 	<span id="dispRentTeamNm"></span>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">요청일자<span class="fontred">*</span></th>
				                 <td >
				                 	<input id="rentAplnDt" name="rentAplnDt" type="text" value=""  maxlength="10" class="inpw20" data-requireNm="대여신청일자"	data-maxLength="8"	title="대여신청일자"/>
<%-- 				                 	 <input type="text" id="modFlag" name="modFlag" value="<c:out value="${param.modFlag}" />" /> --%>
				                 </td>
				                 <th scope="row">사건번호(사건명)</th>
				                 <td >
				                 	<input type="text" id="incdtNm" name="incdtNm"  value=""  maxlength="100" class="inpw50"/>
				                 	<input type="hidden" id="incdtSno" name="incdtSno"  value=""  maxlength="100" class="inpw30"/>
				                 	<input type="hidden" id="suppReqSno" name="suppReqSno"  value=""  maxlength="100" class="inpw30"/>
				                 	<a href="#" class="buttonG60" onclick="fn_searchIncdtListPop();return false;">검색</a>
				                 </td>
			                 </tr>
			                 <tr>
								<th scope="row">담당자(연락처, HP)</th>
								<td colspan="3">
									<span id="dispRentUserTelNo"></span>
									<input type="hidden" id="modFlag" name="modFlag" value="<c:out value="${param.modFlag}" />" />
									<input type="hidden" id="sessionUserId" name="sessionUserId" value="<c:out value="${session_userid}" />" />
<%-- 									<input type="text" id="rentInsttCd" name="rentInsttCd" value="<c:out value="${session_insttcd}" />" /> --%>
									<input type="hidden" id="pgsStat" name="pgsStat" value="" />
									<input type="hidden" id="saveGb" name="saveGb" value="" />
								</td>
							</tr>
							
			                 
			                </tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
					
					<div class="sub_ttl">대여 정보</div><!-----타이틀------>
                 
                  <div class="sub">
                     
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:150px;">
						
						<table class="iptTblX2" id="tbMgmtDtl2">
							<caption>등록</caption>
			               <colgroup>
				             <col width="15%" />
				             <col  />
				             
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">대여기간<span class="fontred">*</span></th>
				                 <td>
									<input id="rentStartDt" name="rentStartDt"  title="대여시작일자"  type="text" value=""  maxlength="10" class="inpw10" data-requireNm="대여시작일자"	data-maxLength="8"/> 
					                <input id="rentEndDt" name="rentEndDt"  title="대여종료일자"  type="text" value=""  maxlength="10" class="inpw10" data-requireNm="대여종료일자"	data-maxLength="8"/>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">요청사항<span class="fontred">*</span></th>
				                 <td >
				                 	<textarea id="rentReqInfo" name="rentReqInfo" rows=2 cols=20 maxlength="260" data-requireNm="대여요청사항"	data-maxLength="500"	title="대여요청사항"></textarea>
				                 	<span class="txt_info" name="rentReqInfoByteChk" id="rentReqInfoByteChk"></span>
				                 </td>
			                 </tr>
			                 
			                </tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
					
<!-- 					<div class="sub_ttl">장비 유지보수 등록</div>---타이틀---- -->
					<div class="flR"><button class="buttonR60" name="addRow" id="addRow" onclick="fn_searchEqpBizMListPop2();return false;">+ 추가</button><button class="buttonG60" name="delRow" id="delRow" onclick="fn_delRow();return false;">- 삭제</button></div>
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:250px;">
						<table class="iptTblX2" id="tbLendDtl">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="5%">
                                <col />
                                <col width="11%">
                                <col width="16%">
                                <col width="16%">
                                <col width="16%">
                                <col width="11%">
							</colgroup>
							<thead>
								<tr>
									<th scope="col">선택</th>
                                    <th scope="col">장비명</th>
                                    <th scope="col">장비유형</th>
                                    <th scope="col">S/N</th>
                                    <th scope="col">모델명</th>
                                    <th scope="col">제조사</th>
                                    <th scope="col">도입일자</th>
								</tr>
							</thead>
							<tbody id="detailTbody">
							</tbody>
						</table>
<!-- 						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span> -->
					</div>
                      
					</form>
                    <div class="btn_c">
                      <ul>
                        <li><a href="#" class="myButton" onclick="fn_modifyEqpLend('imsi');return false;">임시저장</a></li>
                        <li><a href="#" class="myButton" onclick="fn_modifyEqpLend('req');return false;">장비대여신청</a></li>
                        <!-- <li><a href="#" class="myButton">재입력</a></li> -->
                        <li><a href="#" class="myButton" onclick="fn_searchList(1);return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->   
                       
                    
                          
                  </div>
            </div>
        
        </div>
 </div>
<div id="incdtFindPop"></div>
<div id="eqpMgmtFIndEqupQListPop3"></div>
<script type="text/javaScript">
var tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
var ifa = $(top.document).find('div#'+tabId+' > p > iframe');
var height= ifa.get(0).contentWindow.document.body.scrollHeight;
ifa.attr('height', height > 750 ?  height : 1750);
</script>
</body>
</html>