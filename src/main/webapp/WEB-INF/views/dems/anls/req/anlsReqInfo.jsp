<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 20.
 * 2. 작성자 : leeji
 * 3. 화면명 : 분석지원요청 요청관리 상세화면
 * 4. 설명 : 분석지원요청 요청관리 상세화면
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<script type="text/javaScript">

var analReqSno = '${param.analReqSno}';
var tabId;
var selReqCrgrId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	fn_anlsReqInfoSearch();
});

/**
 * 분석지원요청 요청관리 목록페이지이동
 * @param
 * @returns
 */
function fn_moveList(){
	var src = "<c:url value = "/anls/req/indexAnlsReqMList.do"/>";
	parent.$('#'+tabId+' iframe').attr('height', 730);
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 분석지원요청 요청관리 상세화면 조회
* @param
* @returns anlsReqInfoCallBack
*/
function fn_anlsReqInfoSearch(){

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");

	var input = document.createElement('input');
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "analReqSno");
	input.setAttribute("value", analReqSno);
	searchForm.appendChild(input);

	document.body.appendChild(searchForm);

	var callUrl = "<c:url value = "/anls/req/queryAnlsReqInfo.do"/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'infoForm',callbackNm:'anlsReqInfoCallBack'});
}

/**
* 분석지원요청 요청관리 상세화면 조회
* @param {object} data 조회한 결과데이터
* @returns
*/
function anlsReqInfoCallBack(data){
	$('form[name=searchForm]').remove();

	var pgsStat = data.anlsReqInfoMap.pgsStat;

	//버튼 제어
	var $li = $('div.btn_c').find('li').not(':last');
    $li.each(function(index,item){
    	$(item).hide();
    });
    //버튼 권한별 보이기
    if(session_usergb == 'C01001') { //일반수사관

    	//임시저장상태면 승인요청,삭제,수정
    	if(pgsStat == 'C03001'){
    		$li.filter('[class=aprv]').show();
	    	$li.filter('[class=moveMod]').show();
	    	$li.filter('[class=ftpDelFile]').show();
    	}

    	//승인요청상태면 승인요청취소
    	if(pgsStat == 'C03002'){
	    	$li.filter('[class=aprvCncl]').show();
    	}

    	//선별요청상태면 선별확인
    	if(pgsStat == 'C03007'){
	    	$li.filter('[class=selCfrm]').show();
	    	$li.filter('[class=selCfrmRemove]').show();
    	}

    	//선별확인상태면 선별확인취소
    	if(pgsStat == 'C03008'){
	    	$li.filter('[class=selCfrmCncl]').show();
    	}

	} else if(session_usergb == 'C01002') { //군검사

		//승인요청상태면 승인
		if(pgsStat == 'C03002'){
	    	$li.filter('[class=prosrAprv]').show();
    	}

		//승인상태면 승인취소
		if(pgsStat == 'C03003'){
	    	$li.filter('[class=prosrAprvCncl]').show();
    	}

	} else if(session_usergb == 'C01003') { //포렌식수사관
		//승인상태면 삭제 - 접수이전이므로
		if(pgsStat == 'C03003'){
			$li.filter('[class=moveMod]').show();
			$li.filter('[class=ftpDelFile]').show();
		}
		
		//선별요청상태면 선별확인
    	if(pgsStat == 'C03007'){
	    	$li.filter('[class=selCfrm]').show();
	    	$li.filter('[class=selCfrmRemove]').show();
    	}

    	//선별확인상태면 선별확인취소
    	if(pgsStat == 'C03008'){
	    	$li.filter('[class=selCfrmCncl]').show();
    	}
	
	}

	var rcvrIdAcpNm = data.anlsReqInfoMap.rcvrIdAcpNm //접수담당자
	var rcvNoAcp = data.anlsReqInfoMap.rcvNoAcp //접수번호
	var rcvDtAcp = data.anlsReqInfoMap.rcvDtAcp //접수일자
	var aprvrIdNm = data.anlsReqInfoMap.aprvrIdNm //승인자
	var prosrAprvDt = data.anlsReqInfoMap.prosrAprvDt //승인일자
    selReqCrgrId = data.anlsReqInfoMap.selReqCrgrId
	var selReqCrgrNm = data.anlsReqInfoMap.selReqCrgrNm //선별요청담당자명
	var selReqDt = data.anlsReqInfoMap.selReqDt //선별요청일자
	var selCfrmCrgrNm = data.anlsReqInfoMap.selCfrmCrgrNm //선별확인담당자명
	var selCfrmDt = data.anlsReqInfoMap.selCfrmDt //선별확인일자

	//담당자(접수번호,접수일자) 승인자(승인일자) 선별요청담당자(선별요청일자) 선별확인담당자(선별확인일자)
	var rcvrIdAcp = rcvrIdAcpNm != '' ? rcvrIdAcpNm + '(' + rcvNoAcp + ',' + rcvDtAcp + ')' : '';
	var aprvrId = aprvrIdNm != '' ? aprvrIdNm + '(' + prosrAprvDt + ')' : '';
	var selReqCrgr = selReqCrgrNm != '' ? selReqCrgrNm + '(' + selReqDt + ')' : '';
	var selCfrmCrgr = selCfrmCrgrNm != '' ? selCfrmCrgrNm + '(' + selCfrmDt + ')' : '';

	$('span#rcvrIdAcp').text(rcvrIdAcp);
	$('span#aprvrId').text(aprvrId);
	$('span#selReqCrgr').text(selReqCrgr);
	$('span#selCfrmCrgr').text(selCfrmCrgr);

	for(var i = 0; i < data.analysisEvdcDtlList.length; i++){

		var tpTag =     '<tr>'+
							'<td rowspan="2">'+(i+1)+'</td>'+
							'<td><span id="cfscGoodsDiv_'+i+'">'+data.analysisEvdcDtlList[i].cfscGoodsDivNm+'</span></td>'+
							'<td><span id="mdlNm">'+data.analysisEvdcDtlList[i].mdlNm+'</span></td>'+
							'<td><span id="cfscDt">'+data.analysisEvdcDtlList[i].cfscDt+'</span></td>'+
							'<td><span id="psvCfscrSmitr">'+data.analysisEvdcDtlList[i].psvCfscrSmitr+'</span></td>'+
							'<td><span id="cfscCrgr">'+data.analysisEvdcDtlList[i].cfscCrgr+'</span></td>'+
							'<td><span id="trgtDmgdYn">'+(data.analysisEvdcDtlList[i].trgtDmgdYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td><span id="reqBefDrvYn">'+(data.analysisEvdcDtlList[i].reqBefDrvYn == 'Y' ? 'O' : 'X')+'</span></td>'+
						'</tr>'+
						'<tr>'+
							'<td><span id="cfscDiv_'+i+'">'+data.analysisEvdcDtlList[i].cfscDivNm+'</span></td>'+
							'<td><span id="serlNo">'+data.analysisEvdcDtlList[i].serlNo+'</span></td>'+
							'<td><span id="cfscPlace">'+data.analysisEvdcDtlList[i].cfscPlace+'</span></td>'+
							'<td><span id="realUser">'+data.analysisEvdcDtlList[i].realUser+'</span></td>'+
							'<td><span id="sealYn">'+(data.analysisEvdcDtlList[i].sealYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td><span id="dmgdDtl">'+data.analysisEvdcDtlList[i].dmgdDtl+'</span></td>'+
							'<td><span id="drvDtl">'+data.analysisEvdcDtlList[i].drvDtl+'</span></td>'+
						'</tr>';
		$("#detailTbody").append(tpTag);
	}
/*
	for(var i = 0; i < data.fileList.length; i++){
		var path = (data.fileList[i].filePth+'\\'+data.fileList[i].fileNm).replace(/\\/gi, "//");
		var atchFileId = data.fileList[i].atchFileId;
		$('input#atchFileId').val(atchFileId);
		var spanTag = '<p><span>'+data.fileList[i].fileNm+'</span><button class=button60 onclick=javascript:fn_viewFile(\''+encodeURIComponent(path)+'\')>다운로드</button>'+
		              '<input type="hidden" name="path" value="'+path+'"></p>';
		$("div#"+data.fileList[i].atchFileDiv).append(spanTag);
	}*/

    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileList.length; i++){
		var id = data.atchFileList[i].id;
		var name = data.atchFileList[i].name;
		var path = data.atchFileList[i].path;
		var upload_yn = data.atchFileList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list_1').append(html);
	}

    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileImgList.length; i++){
		var id = data.atchFileImgList[i].id;
		var name = data.atchFileImgList[i].name;
		var path = data.atchFileImgList[i].path;
		var upload_yn = data.atchFileImgList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list_2').append(html);
	}


	fn_setFrameSize();

}

/**
* 분석지원요청 요청관리 진행상태 변경요청
* @param {string} reqType 버튼별 type
* @returns fn_moveList
*/
function fn_modPgsStat(reqType){
	var tpArray = [];
	var Objtype = {};
	tpArray.push(analReqSno);

	Objtype.type = reqType;
	var data = {rowDatas : tpArray, type : Objtype};
    var callUrl = "<c:url value='/anls/req/queryPgsStat.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_moveList'});

}

/**
 * 분석지원 원격요청관리
 */
function fn_procUserIpInfo() {
    console.log(selReqCrgrId);
    var data = {"selReqCrgrId" : selReqCrgrId};
    var callUrl = "<c:url value='/anls/req/jetcoUserIpInfo.do'/>";
    $.ajax({
        type:'POST',
        crossOrigin : true,
        data : data,
        dataType : "json",
        url : callUrl,
        success : function(data){
            console.log(data.userIpInfo);
            fn_Remote(data.userIpInfo.userIp);
        },
        fail:function(data){},
        error : function(xhr, status, error){}
    });
    
    var args = {"type":"remoteRequest", "ip":"192.168.51.64"};
}

function fn_Remote(ip) {
    console.log("remote");
    var args = {data:{"type": "remoteRequest","ip": ip}};
    var data = JSON.stringify(args.data);
    $.ajax({
        type:'POST',
        crossOrigin : true,
        data : data,
        dataType : "json",
        url : "http://localhost:57626/service",
        beforeSend: function () {},
        complete:  function () {},
        success : function(data){
            var code = data.code;
            var msg = data.msg;
            var result = data.result;
            if(10000 == code){
                console.log("remote success");
            }
        },
        fail:function(data){},
        error : function(xhr, status, error){}
    });
}


/**
* 분석지원요청 요청관리 ftp파일삭제(모든)
* @param
* @returns fn_del
*/
function fn_ftpDelFile(){

	//파일삭제 리스트 가져오기
	var path = [];

	$('div#gen, div#img').find('input[type="hidden"]').each(function(index,value){
		//console.log("index :" + index + " value : " +value.value);
		path.push(value.value);
	});

	if(path.length > 0){
		//ftp파일을 지우고 테이블을 삭제함
		requestUtil.ftpFileAjax({data:{"type": "ftp_removeFile","src": path},callbackNm:'fn_del',skip:'Y'});
	} else {
		fn_del();
	}

}

/**
* 분석지원요청 요청관리 상세삭제
* @param
* @returns fn_moveList
*/
function fn_del(){
	var callUrl = "<c:url value='/anls/req/delAnlsReqUDtl.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:{'analReqSno':analReqSno},callbackNm:'fn_moveList'});
}

/**
* 분석지원요청 요청관리 수정페이지 이동
* @param
* @returns
*/
function fn_moveMod(){
	var src = "<c:url value = "/anls/req/indexAnlsReqUDtl.do"/>?analReqSno="+analReqSno;
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 분석지원요청 요청관리 첨부파일 다운로드
* @param {string} path 다운로드할 파일 경로
* @returns
*/
function fn_viewFile(path){
	requestUtil.ftpFileAjax({data:{"type": "ftp_downloadFile","path": decodeURIComponent(path)},skip:'Y'});
}

</script>
</head>
<body>
<div id="con_wrap1">
	<div class="content">
		<div id="contents_info">
			<div class="sub_ttl">분석 요청 상세</div><!-----타이틀------>
				<div class="sub">

					<form name="infoForm" id="infoForm" onsubmit="return false;">
					<div class="t_list">
						<table class="iptTblX">
							<caption>상세화면</caption>
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">요청번호</th>
									<td><span id="analReqSno"></span></td>
									<th scope="row">검사(승인일자)</th>
									<td><span id="prosr"></span></td>
								</tr>
								<tr>
									<th scope="row">요청일시</th>
									<td><span id="reqDt"></span></td>
									<th scope="row">사건번호(사건명)</th>
									<td><span id="incdtNo"></span><p>(<span id="incdtNm"></span>)</p></td>
								</tr>
								<tr>
									<th scope="row">요청기관</th>
									<td>
										<span id="reqInsttNm"></span>
									</td>
									<th scope="row">요청부서</th>
									<td>
										<span id="reqDepNm"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">주임군검사</th>
									<td><span id="prosrNm"></span>
									</td>
									<th scope="row">담당자(연락처,HP)</th>
									<td><span id="reqUserNm"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="sub_ttl">분석 대상 상세정보</div>
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:350px;">
						<table class="iptTblX2">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="5%" />
								<col width="5%" />
								<col width="10%" />
								<col width="15%" />
								<col width="15%" />
								<col width="15%" />
								<col width="15%" />
								<col width="15%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col" rowspan="2">번호</th>
									<th scope="col" rowspan="2">구분</th>
									<th scope="col">모델명</th>
									<th scope="col">압수일시<br>(제출일시)</th>
									<th scope="col">피압수자<br>(제출자)</th>
									<th scope="col">압수자</th>
									<th scope="col">대상물<br>훼손 유무</th>
									<th scope="col">요청 전<br>구동유무</th>
								</tr>
								<tr>
									<th scope="col">시리얼번호</th>
									<th scope="col">압수장소<br>(제출장소)</th>
									<th scope="col">실사용자</th>
									<th scope="col">압수 시<br>봉인유부</th>
									<th scope="col">훼손<br>상세내역</th>
									<th scope="col">구동<br>상세내역</th>
								</tr>
							</thead>
							<tbody id="detailTbody">
							</tbody>
						</table>
					</div>
					<div class="sub_ttl">분석 대상 상세정보</div>
					<div class="t_list">
						<table class="iptTblX">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="15%" />
								<col width="85%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">피의사실</th>
									<td><span id="analSuptFact"></span></td>
								</tr>
								<tr>
									<th scope="row">주요 분석요청 사항</th>
									<td><span id="analReqInfo"></span></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="t_list">
						<table class="iptTblX">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="15%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="45%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">구분</th>
									<th scope="row">문서파일<br>분석</th>
									<th scope="row">메일/메신저<br>분석</th>
									<th scope="row">웹히스토리<br>분석</th>
									<th scope="row">사진/동영상<br>분석</th>
									<th scope="row">키워드</th>
								</tr>
								<tr>
									<td><span>내용</span></td>
									<td><span id="analDocFile"></span></td>
									<td><span id="analEmailMsgr"></span></td>
									<td><span id="analWebHist"></span></td>
									<td><span id="analPhotoVideo"></span></td>
									<td><span id="analKwrd"></span></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="t_list">
						<table class="iptTblX">
						<input type="hidden" name="atchFileId" id="atchFileId">
							<caption>파일첨부</caption>
							<colgroup>
								<col width="15%" />
								<col width="85%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">첨부파일</th>
									<td>
										<div id="gen" style="OVERFLOW-Y:auto; width:100%; height:50px;">
											<ul id="file_list_1" class="file_list_1" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row">이미지파일(선별파일)</th>
									<td>
										<div id="img" style="OVERFLOW-Y:auto; width:100%; height:50px;">
											<ul id="file_list_2" class="file_list_2" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<!-- <div class="t_list">
						<table class="iptTblX">
							<caption>상세화면</caption>
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">담당자(접수번호,접수일자)</th>
									<td><span id="rcvrIdAcp"></span></td>
									<th scope="row">승인자(승인일자)</th>
									<td><span id="aprvrId"></span></td>
								</tr>
								<tr>
									<th scope="row">선별요청</th>
									<td><span id="selReqCrgr"></span></td>
									<th scope="row">선별확인</th>
									<td><span id="selCfrmCrgr"></span></td>
								</tr>
							</tbody>
						</table>
					</div> -->
					</form>

					<div class="btn_c">
						<ul>
							<li class="aprv">
								<a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('aprv'); return false;">승인요청</a>
							</li>
							<li class="aprvCncl">
								<a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('aprvCncl'); return false;">승인요청취소</a>
							</li>
							<li class="selCfrm">
								<a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selCfrm'); return false;">선별확인</a>
							</li>
							<li class="selCfrmRemove">
								<a href="javascript:void(0);" class='myButton' onclick="fn_procUserIpInfo(); return false;">원격요청</a>
							</li>
							<li class="selCfrmCncl">
								<a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selCfrmCncl'); return false;">선별확인취소</a>
							</li>
							<li class="moveMod">
								<a href="javascript:void(0);" class='myButton' onclick="fn_moveMod(); return false;">수정</a>
							</li>
							<li class="ftpDelFile">
								<a href="javascript:void(0);" class='myButton' onclick="fn_ftpDelFile(); return false;">삭제</a>
							</li>
							<li class="prosrAprv">
								<a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('prosrAprv'); return false;">승인</a>
							</li>
							<li class="prosrAprvCncl">
								<a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('prosrAprvCncl'); return false;">승인취소</a>
							</li>
							<li>
								<a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a>
							</li>
						</ul>
					</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>