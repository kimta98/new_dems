<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 22. 오후 5:21:45
 * 2. 작성자 : 이종인
 * 3. 화면명 : 담당자 조회 팝업
 * 4. 설명 : 담당자 조회 팝업
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javaScript" language="javascript">
var type = '${param.type}';
var idx = '${param.idx}';
$(document).ready(function() {	
	//var codeInfo = [{cdId:'crimeGrp',selectId:'searchCrimeCd',type:'4', callbackNm:'fn_codeCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	//fn_ajaxCodeList(codeInfo);
	gfn_init({startFnNm:'fn_crgrSearch', param:1, codeSet:'N'});
});

function fn_codeCallback(){
	$('#searchCrimeCd').prepend('<option value="" selected>전체</option>');
}

/**
 * 담당자 리스트 조회
 * @param {string} page 항목에 대한 고유 식별자 
 * @returns fn_callBack
 */
function fn_crgrSearch(page){
	var callUrl = "<c:url value = "/anls/req/queryAnlsReqPop.do"/>?type="+type;
	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callBack',page:page,perPageNum:10});
}

/**
 * 사건조회화면목록 조회 콜백함수
 * @param {object} data 조회한 결과데이터
 * @returns
 */
function fn_callBack(data){

	$("#popList").empty();
	$("#totalcnt").text(data.totalCount);

	if(data.popList.length < 1){
		$('#popList').append('<tr><td colspan="8">조회된 결과가 없습니다.</td></tr>');
	}else{
	
		$.each(data.popList, function(index, item){
			
			$('#popList').append("<tr><td>"+item.rn+"</td>"+
			"<td>"+item.deptNm+"</td>"+
			"<td>"+item.userNm+"</td>"+
			"<td>"+item.telNo+"</td>"+
			"<td><button class=button35 onclick=javascript:fn_setCrgr('"+item.regUserId+"','"+item.userNm.trim()+"','"+encodeURIComponent(item.regUserNm)+"');>선택</button></td></tr>");
		 });
	}

	data.__callFuncName__ ="fn_crgrSearch";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
}
 
/**
 * 사건선택 
 * @param {String} regUserId
 * @param {String} userNm
 * @param {String} regUserNm
 * @returns {function} fn_popCallBack(paramObj,divId)
 */ 
function fn_setCrgr(regUserId,userNm,regUserNm){
	var paramObj = new Object();
	var divId = 'divCrgrFindPopByPop';
	if(type == 'prosr'){
		paramObj.prosrId = regUserId;                           //주인검사ID
		paramObj.prosrNm = userNm;                              //주인군검사명		
	}else if(type == 'analCrgr'){
		paramObj.analCrgrIdPop = regUserId;                           //분석담당자ID
		paramObj.analCrgrNmPop = userNm;                              //분석담당자명		
	}else if(type == 'analRcvr'){
		paramObj.idx = idx;
		paramObj.rcvrIdAcp = regUserId;
		paramObj.rcvrIdAcpNm = userNm;
	}else{
		paramObj.reqUserId = regUserId;                         //담당자ID	
		paramObj.regUserNm = decodeURIComponent(regUserNm);     //담당자(연락처,HP)		
	}	

	fn_popCallBackByPop(paramObj,divId);
}
 
</script>
</head>
<body>
<div id="con_wrap_pop">
<div class="contents">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div class="window_popup">
                  <div><h2>담당자 조회</h2></div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
                    <form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
                    <div class="t_head">
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_crgrSearch(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="35%">
                                   <col width="65%">                      
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">담당자명</th>
					            <td scope="col">
					               <input class="input20" type="text" name="searchUserNm" id="searchUserNm"/>
					            </td>
					          </tr>
                           </thead>
                        </table>
                        <div  class="btn_c">
				       	<ul>
                          <li><a href="javascript:void(0);" class='gyButton' onclick="fn_crgrSearch(1); return false;">조회</a></li>
                        </ul>   
					  </div>
                      
                      </div>
                     </form>
                      
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
                     <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>                     
                     <!--------------목록---------------------->
                     <div class="t_list">  
                          <table class="tbl_type" border="1" cellspacing="0" >
                                <caption>사건조회</caption>
                                  <colgroup>
                                      <col width="5%">                                      
                                      <col width="15%">
                                      <col width="25%">
                                      <col width="25%">
                                      <col width="25%">
                                   </colgroup>
                                    <thead>
                                      <tr>
                                         <th scope="col">순번</th>
                                         <th scope="col">부서명</th>
                                         <th scope="col">담당자명</th>
                                         <th scope="col">전화번호</th>
                                         <th scope="col">선택</th>                                       
                                      </tr>
                                    </thead>
                                    <tbody id="popList">
                                    </tbody>
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                     <div id='page_navi' class="page_wrap"></div>                            
                       <!-----------------------//페이징----------------------->                  
                  </div>
                 
            </div>
    </div>
    </div>
</div>
</body>
</html>