<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 20.
 * 2. 작성자 : leeji
 * 3. 화면명 : 분석지원요청 요청관리 등록
 * 4. 설명 : 분석지원요청 요청관리 등록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/jquery/jquery.ajax-cross-origin.min.js' />" ></script>
<script type="text/javaScript">

var reqInsttNm = '${reqInsttNm}';
var reqDepNm = '${reqDepNm}';
var reqInsttCd = '${reqInsttCd}';

var fileArray = [];
//ftp 전송되는값
var ftpFileArray = [];
var analReqSno = '';
var tabId;


$(document).ready(function() {

	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	gfn_overMaxLength("analSuptFact",1300);
	gfn_overMaxLength("analReqInfo",1300);

	//요청기관,요청부서,요청기관코드 세팅
	if(reqInsttNm != ''){
		$('#reqInsttNm').text(reqInsttNm);
	}
	if(reqInsttCd != ''){
		$('#reqInsttCd').val(reqInsttCd);
	}
	if(reqDepNm != ''){
		$('#reqDepNm').text(reqDepNm);
	}
	
	// 포렌식수사관일 경우 승인요청 버튼 hide
	if(session_usergb == 'C01003'){
		$(".btn_c li").eq(0).hide();
		$("#prosrTd>button").hide();
		//$("#prosrNm").val("관리자");
		//$("#prosrId").val("Admin");
	}

	gfn_calendarConfig("reqDt", "", "", "");
	$('#reqDt').val(gfn_dashDate(gfn_getCurDate(), "-"));

	$("#addRow").on("click", function(event) {

		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length/2+1;

		var tpTag =     '<tr>'+
			'<td rowspan="2"><input type="checkbox" name="chk" value="Y" class="check_agree1"></td>'+
			'<td rowspan="2">'+idx+'</td>'+
			'<td><select name="cfscGoodsDiv" id="cfscGoodsDiv_'+idx+'" class="selw15" title="압수품구분"></select></td>'+
			'<td><input type="text" name="mdlNm" class="inpw70" title="모델명" data-maxlength="60"></td>'+
			'<td>'+
				'<p><input type="text" name="cfscDate" id="cfscDate_'+idx+'" class="inpw40" title="압수일시_제출일시" readonly></p>'+
				'<p><select name="hour" id="hour_'+idx+'" class="selw3"></select>'+
				'<select name="minute" id="minute_'+idx+'" class="selw3"></select></p>'+
				'<input type="hidden" name="cfscDt">'+
			'</td>'+
			'<td><input type="text" name="psvCfscrSmitr" class="inpw70" title="피압수자_제출자" data-maxlength="7"></td>'+
			'<td><input type="text" name="cfscCrgr" class="inpw70" title="압수자" data-maxlength="7"></td>'+
			'<td>'+
				'<select name="trgtDmgdYn" class="selw15" title="대상물훼손유무" onchange="fn_dtlYn(\'dmgdDtl\',this)">'+
				  	'<option value="" selected>선택</option>'+
				  	'<option value="Y">Y</option>'+
				  	'<option value="N">N</option>'+
			  	'</select>'+
			'</td>'+
			'<td>'+
				'<select name="reqBefDrvYn" class="selw15" title="요청전_구동유무" onchange="fn_dtlYn(\'drvDtl\',this)">'+
				  	'<option value="" selected>선택</option>'+
				  	'<option value="Y">Y</option>'+
				  	'<option value="N">N</option>'+
			  	'</select>'+
			'</td>'+
		'</tr>'+
		'<tr>'+
			'<td><select name="cfscDiv" id="cfscDiv_'+idx+'" class="selw15" title="압수구분"></select></td>'+
			'<td><input type="text" name="serlNo" class="inpw70" title="시리얼번호" data-maxlength="15"></td>'+
			'<td><input type="text" name="cfscPlace" class="inpw70" title="압수장소_제출장소"></td>'+
			'<td><input type="text" name="realUser" class="inpw70" title="실사용자"></td>'+
			'<td>'+
				'<select name="sealYn" class="selw15" title="봉인유무">'+
				  	'<option value="" selected>선택</option>'+
				  	'<option value="Y">Y</option>'+
				  	'<option value="N">N</option>'+
			  	'</select>'+
			'</td>'+
			'<td><textarea id="dmgdDtl_'+idx+'" name="dmgdDtl" rows=1 cols=30 title="훼손상세내역" data-maxlength="160"></textarea></td>'+
			'<td><textarea id="drvDtl_'+idx+'" name="drvDtl" rows=1 cols=30 title="구동상세내역" data-maxlength="160"></textarea></td>'+
		'</tr>';
		$("#detailTbody").append(tpTag);

		if($('#detailTbody > tr').length > 2){
			//1행에 있는 select박스 복사
			$('select#cfscGoodsDiv_'+idx).html($('select#cfscGoodsDiv_1').html());
			$('select#cfscDiv_'+idx).html($('select#cfscDiv_1').html());



		}else{
			//select박스 가져오기
			var codeInfo = [{cdId:'C05',selectId:'cfscGoodsDiv_'+idx ,type:'1', callbackNm:'fn_codeCallBack'},{cdId:'C04',selectId:'cfscDiv_'+idx ,type:'1'}];
			fn_ajaxCodeList(codeInfo);
		}

		fn_makeSbx(idx);
		gfn_calendarConfig("cfscDate_"+idx, "", "", "");
	});

	//삭제 버튼
	$("#delRow").on("click", function(event) {
		var cnt = 0;
		$('#detailTbody > tr').each(function(index,item){
			var $chkbox = $(item).find('input[type=checkbox]');
			$chkbox.each(function(index,item){

				if($(this).is(':checked') == true){
					cnt++;
					$(this).parent().parent('tr').next().remove();
					$(this).parent().parent('tr').remove();
				}
			})

		});

		if(cnt < 1){
			fn_showUserPage('선택해주세요');
			return;
		}


		var idx = $('#detailTbody > tr').length == 0 ? 1 : $('#detailTbody > tr').length/2;
		//tr 2개가 한개


		for(var i = 1; i <= idx; i++){
			if(i == 1){
				$('#detailTbody > tr').eq(0).find('td:eq(1)').text(i);
				$('#detailTbody > tr').eq(0).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
				$('#detailTbody > tr').eq(0).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
				$('#detailTbody > tr').eq(1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
			}else{
				$('#detailTbody > tr').eq(i*2-2).find('td:eq(1)').text(i);
				$('#detailTbody > tr').eq(i*2-2).find('td:eq(4) > input').attr('id', 'cfscDate_'+i);
				$('#detailTbody > tr').eq(i*2-2).find('td:eq(2) > select').attr('id', 'cfscGoodsDiv_'+i);
				$('#detailTbody > tr').eq(i*2-1).find('td:eq(0) > select').attr('id', 'cfscDiv_'+i);
			}

		}
		fn_makeSbx(idx);
		gfn_calendarConfig("cfscDate_"+idx, "", "", "");

	});

	//추가버튼 trigger
	$("#addRow").trigger('click');

});
/**
 * 분석지원요청 요청관리 콤보박스 콜백함수
 * @param
 * @returns
 */
function fn_codeCallBack(){
	$('#cfscGoodsDiv_1').prepend('<option value="" selected>선택</option>');
	$('#cfscDiv_1').prepend('<option value="" selected>선택</option>');
}

/**
 * 분석지원요청 요청관리 목록페이지이동
 * @param
 * @returns
 */
function fn_moveList(){

	var src = "<c:url value = "/anls/req/indexAnlsReqMList.do"/>";
	parent.$('#'+tabId+' iframe').attr('height', 730);
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 분석지원요청 요청관리 시분 콤보박스생성
* @param
* @returns
*/
function fn_makeSbx(idx){
	var h = document.getElementById('hour_'+idx);
	var m = document.getElementById('minute_'+idx);
	for (var i=0; i < 24; i++) h.options[i] = new Option( f(i) , f(i));
	for (i=0; i < 60; i++) m.options[i] = new Option( f(i) , f(i));
	//h.value = '00';
	//m.value = '00';
	function f(n) {return n < 10 ? '0' + i : i; }
}

/**
* 분석지원요청 요청관리 등록
* @param {string} type 버튼별 type
* @returns fn_anlsReqRDtlCallBack
*/
function fn_save(type){

	//validate
	if(!fn_formValidate()){
		return;
	}

	//분석지원요청정보
	var $formDatas = $('#insForm').find('table').not('.iptTblX2');

	var formObj = new Object();

	$formDatas.each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){

			if($(this).is('input[type=checkbox]')){
				if($(this).is(':checked')) {
					formObj[item.name] = item.value;
				}
			}else{
				formObj[item.name] = item.value;
			}
		});

	});
	//type 넣기
	formObj['type'] = type;

	//분석대상 상세정보
	var tpArray = [];
	var dataObj = new Object();

	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input, select, textarea');

		$el.each(function(index,item){

			if($(this).is('input[type=checkbox]')){
				if($(this).is(':checked')) {
					dataObj[item.name] = item.value;
				}
			}else{
				dataObj[item.name] = item.value;
			}
		});

		if((index+1) % 2 == 0){
			tpArray.push(dataObj);
			dataObj = {};
		}

	});

	tpArray.forEach(function(obj){

		var cfscDate;
		var hour;
		var minute;

		$.each(obj,function(index,value){

			if(index == 'cfscDate'){
				cfscDate = value.replace(/-/gi, "");
			}
			if(index == 'hour'){
				hour = value;
			}
			if(index == 'minute'){
				minute = value;
			}

		});

		obj.cfscDt = cfscDate+hour+minute;

	});

	var data = {rowDatas : tpArray, formDatas : formObj};
    var callUrl = "<c:url value='/anls/req/reqAnlsReqRDtl.do'/>";
   	var flag = (ftpFileArray.length > 0 ? 'Y' : 'N');
    requestUtil.saveData({callUrl:callUrl,data:data,skip:flag,callbackNm:'ftp_uploadFileCallback'});
}


/**
* 분석지원요청 요청관리 팝업호출
* @param {string} type 팝업종류
* @returns
*/
function fn_openPop(type){
	var callUrl = "<c:url value = "/com/PageLink.do"/>?type="+type
    var jsp = (type == 'incdt' ? "dems/incdt/incdt/incdtIncdtMListPop" : "dems/anls/req/crgrFindPop");
    var divId = (type == 'incdt' ? "incdtIncdtMListPop" : "divCrgrFindPop");

	requestUtil.mdPop({
		popUrl : callUrl+"&link="+jsp,
		height: 700,
        width: 1000,
        title: (type == 'incdt' ? '사건 조회' : '담당자조회'),
        divId : divId
	});
}

function fn_incdtIncdtMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtMListPop",
		height: 700,
        width: 1000,
        title: '사건 조회',
        divId : 'incdtIncdtMListPop'
	});

}

/**
 * 사건관리 등록 팝업
 */
function fn_incdtIncdtRDtlPop() {
	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/incdt/incdt/incdtIncdtRDtlPop",
		height: 800,
        width: 1000,
        title: '사건 등록',
        divId : 'incdtIncdtRDtlPop'
	});
}

/**
* 분석지원요청 요청관리 팝업콜백
* @param {string} divId 팝업id
* @param {object} data 결과데이터
* @returns
*/
function fn_popCallBack(data, divId){

	$("#"+divId).dialog( "close" );
	$("#"+divId).empty();

	$.each(data, function(index, value){
		if($('#'+index).is('span')){
			$('#'+index).text(value);
		}else{
			$('#'+index).val(value);
		}
	});
}

/**
 * 분석지원요청 요청관리 수정화면이동
 * @param
 * @returns
 */
function fn_insAnlsReq(){
	var src = "<c:url value = "/anls/req/indexAnlsReqRDtl.do"/>";
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
 * 분석지원요청 요청관리 등록 폼유효성체크
 * @param
 * @returns
 */
function fn_formValidate(){

	//상세정보 체크
	if($('#detailTbody > tr').length < 1){
		fn_showUserPage('분석대상 상세정보를 넣어주세요');
		return false;
	}

	var $formDatas = $('#insForm').find('table').not('.iptTblX2');
	var flag = false;
	//*표시 null체크

	$formDatas.each(function(index,item){

		$(item).find('th > span.fontred').each(function(index,item){

				var $input = $(this).parent('th').next().find('input');

				if($input.length > 0){
					var chkValue = $(this).parent('th').next().find('input').val();
					var title = $(this).parent('th').next().find('input').attr('title');

					if( chkValue == null || chkValue == ''){
						fn_showUserPage(title + '을 입력해주세요');
						$input.focus();
						flag = true;
						return false;
					}
				}
		});
		if(flag)return false;

	});

	if(flag)return false;

	$('#detailTbody > tr').each(function(index,item){

		var $el = $(item).find('input[type=text], select, textarea');

		$el.each(function(index,item){
			//널체크
			if($(this).val() == '' || $(this).val() == null){

				if($(this).is('textarea')){

					if($(this).attr('readonly') == 'readonly'){
						return;
					}

				}

				var title = $(this).attr('title');
				fn_showUserPage(title + '을 입력해주세요');
				$(this).focus();
				flag = true;
				return false;
			}else{

				//길이체크
				if($(this).is('input[type=text]') || $(this).is('textarea')){
					var maxlength = $(this).data().maxlength;
				    var elLength = $(this).val().length;
				    var title = $(this).attr('title');

					if(gfn_checkByte(elLength) > maxlength){
						fn_showUserPage(title + '는 '  + maxlength + '자 까지 가능합니다.');
						$(this).focus();
						flag = true;
						return false;
					}
				}

			}

		});

		if(flag) return false;

	});

	if(flag)return false;

	return true;

}

/**
* 분석지원요청 요청관리 첨부파일(첨부한파일경로와 파일명을 넣어준다)
* @param {obj} obj 선택한 노드
* @returns
*/
function fn_fileChk(obj,type){

	var fileObj = {};
	//경로+파일명
	var fileInfo = obj.value;
	//var fileNm = fileInfo.substring(fileInfo.lastIndexOf('\\')+1, fileInfo.lastIndexOf('.'));
	var fileNm = fileInfo.substring(fileInfo.lastIndexOf('\\')+1);
	var filePath = fileInfo.substring(0, fileInfo.lastIndexOf('\\'));

	//같은경로+파일명 체크
	if(ftpFileArray.length > 0){
		var flag = false;
		ftpFileArray.forEach(function(file){
			if(file == fileInfo) {
				fn_showUserPage('이미 첨부된 파일입니다');
				fn_fileValueClear(obj);
				flag = true;
				return false;
			}
		});
		if(flag) return;

	}

	fileObj.type = type;
	fileObj.fileNm = fileNm;
	fileObj.filePath = filePath;
	fileObj.fileInfo = fileInfo;

	fileArray.push(fileObj);
	ftpFileArray.push(fileInfo);

	//화면에 표시해주기
	$(obj).parent().append('<p><span>'+fileNm+'</span><button class="button35" onclick="javascript:fn_delFile(this);return false;">삭제</button></p>');

	//file 초기화
	fn_fileValueClear(obj);

}

/**
* 분석지원요청 요청관리 첨부파일(input file을 초기화한다)
* @param {obj} obj 선택한 노드
* @returns
*/
function fn_fileValueClear(obj){

	var agent = navigator.userAgent.toLowerCase();

	if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
		$(obj).replaceWith( $(obj).clone(true) );
	} else {
		$(obj).val("");
	}
}

/**
* 분석지원요청 요청관리 첨부파일 ftp파일업로드
* @param {object} result 등록한 분석요청일련번호
* @returns ftp_uploadFileCallback
*/
function ftp_uploadFile(result){

	var path = {"path" : "/" + "req" + "/" + result.analReqSno};
 	//var ftpFileArray = ["C:/Java/jdk1.6.0_45/THIRDPARTYLICENSEREADME.txt","D:\패키지 스크립트.txt","D:\펑션 스크립트.txt","공공급식식재표표준체계구축_WBS_1.2_작업중.xlsx"];
	//var path = {"path": "/2/1"};

	var data = {"type": "ftp_uploadFile",
			    "hash": "sha256",
			    "src" : ftpFileArray,
			    "dst" : path};

	requestUtil.ftpFileAjax({data:data,callbackNm:'ftp_uploadFileCallback',skip:'Y'});
}

/**
* 분석지원요청 진행관리 첨부파일 ftp파일업로드 콜백함수
* @param {object} data ftp업로드한후 받은 데이터 path,hash
* @returns fn_moveList
*/
function ftp_uploadFileCallback(result){
	agent.uploadFile({
		obj_id : "file_list_1",
		target_id : result.analReqSno,
		type : "anlsReq_File",
		creator_id : session_userid
	});

	agent.uploadFile({
		obj_id : "file_list_2",
		target_id : result.analReqSno,
		type : "anlsReq_Img",
		creator_id : session_userid
	});

	fn_moveList();
/*
	//[{"path":"\\case1\\ev1\\THIRDPARTYLICENSEREADME.txt","hash":"0b9d59a6f41990a62c03f26beb0cc5df992d08d50155a3c6690465dfc4b0b4d2"}]
	var insFileArray = [];

	//ftp에서 받아온 파일리스트와 화면에서 가지고있는 리스트 비교해서 합친다...(type을넣기위해)
	data.result.forEach(function(v, i) {
		fileArray.forEach(function(v1, i1) {
			console.log("data.reuslt path : " + v.path + " hash : " + v.hash);
			var tpObj = {};
			if(v.path.indexOf(v1.fileNm) > 0){
				console.log("fileArray fileNm : " + v1.fileNm + " fileInfo : " + v1.fileInfo+ " type : " + v1.type);
				tpObj.fileHashVal = v.hash;
				tpObj.atchFileDiv = v1.type;
				//tpObj.fileNm = v.path.substring(v.path.lastIndexOf('\\')+1, v.path.lastIndexOf('.'));
				tpObj.fileNm = v.path.substring(v.path.lastIndexOf('\\')+1);
				tpObj.oriFileNm = v.path.substring(v.path.lastIndexOf('\\')+1);
				tpObj.filePth = v.path.substring(0, v.path.lastIndexOf('\\'));
				insFileArray.push(tpObj);
				console.log("tpObj.fileNm : " + tpObj.fileNm +"tpObj.filePath : " + tpObj.filePth +"tpObj.atchFileDiv : " + tpObj.atchFileDiv);
			}
		});
	});


	//분석요청일련번호
	var formObj = new Object();
	formObj["analReqSno"] = analReqSno;
	formObj["type"] = 'ins';

	//삭제할 파일리스트 없어도 빈것으로 보내야함
	var delFileArray = [];

	//파일정보 update
	var data = {insFileArray : insFileArray, delFileArray : delFileArray, anlsReqInfo: formObj};
    var callUrl = "<c:url value='/anls/req/queryAnlsReqFile.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_moveList'});*/

}

/**
* 분석지원요청 요청관리 첨부파일 삭제
* @param {object} obj 현재노드
* @returns
*/
function fn_delFile(obj){

	var delFile = $(obj).prev().text();

	//fileArray ftpFileArray에서 찾아서 지우기
	var idx = ftpFileArray.indexOf(delFile);
	ftpFileArray.splice(idx,1);

	var findDelFile = fileArray.find(function(item){
		return item.fileInfo === delFile;
	});
	idx = fileArray.indexOf(findDelFile);
	fileArray.splice(idx,1);
/*
 	ftpFileArray.forEach(function(value,index,file){
		console.log("ftpFileArray file : " + file);
	});

	fileArray.forEach(function(value,index,file){
		$.each(file,function(type,value){
			console.log("type : " + type +   "  fileInfo : " + value.fileInfo+   "  filePath : " + value.filePath+   "  fileNm : " + value.fileNm+   "  type : " + value.type);
		});
	});
	 */
	$(obj).parent('p').remove();

}

/**
* 분석지원요청 요청관리 상세내역 화면제어
* @param {string} taId textarea 아이디
* @param {obj} obj 현재 노드
* @returns
*/
function fn_dtlYn(taId,obj){

	//바로 밑에 줄에있는 각 상세내역을 찾기
	$textarea = $(obj).parent().parent('tr').next().find('textarea[name='+taId+']');

	if(obj.value == 'Y'){
		$textarea.attr("readonly", false);
		$textarea.css({'backgroundColor':''});
	}else{
		$textarea.val('');
		$textarea.css({'backgroundColor':'gray'});
		$textarea.attr("readonly", true);
	}
}

</script>
</head>
<body>
<div id="con_wrap1">
	<div class="content">
		<div id="contents_info">
			<div class="sub_ttl">분석 요청 등록</div><!-----타이틀------>
				<form name="insForm" id="insForm" method="post" onsubmit="return false;">
				<div class="sub">
					<div class="t_list">
						<table class="iptTblX">
							<caption>등록하기</caption>
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">요청일시<span class="fontred">*</span></th>
									<td><input class="inpw60" title="요청일시" type="text" name="reqDt" id="reqDt"/></td>
									<th scope="row">사건번호(사건명)<span class="fontred">*</span></th>
									<td><input class="inpw60" title="사건번호(사건명)" type="text" name="incdtNm" id="incdtNm" readonly/>
										<input type="hidden" name="incdtSno" id="incdtSno"/>
										<button class="buttonG40" onclick="javascript:fn_incdtIncdtMListPop();return false;">검색</button>
									</td>
								</tr>
								<tr>
									<th scope="row">요청기관<span class="fontred">*</span></th>
									<td>
										<span id="reqInsttNm"></span>
										<input type="hidden" name="reqInsttCd" id="reqInsttCd"/>
									</td>
									<th scope="row">요청부서<span class="fontred">*</span></th>
									<td>
										<span id="reqDepNm"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">주임군검사<span class="fontred">*</span></th>
									<td id="prosrTd"><input class="inpw60" title="주임군검사" type="text" name="prosrNm" id="prosrNm" readonly/>
									    <button class="buttonG40" onclick="javascript:fn_openPop('prosr');return false;">검색</button>
										<input type="hidden" name="prosrId" id="prosrId"/>
									</td>
									<th scope="row">담당자(연락처,HP)<span class="fontred">*</span></th>
									<td><input class="inpw60" title="담당자연락처" type="text" name="regUserNm" id="regUserNm" readonly/>
										<input type="hidden" name="reqUserId" id="reqUserId"/>
										<button class="buttonG40" onclick="javascript:fn_openPop('regUser');return false;">검색</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="sub_ttl">분석 대상 상세정보</div>
					<div class="flR"><button class="buttonR60" name="addRow" id="addRow">+ 추가</button><button class="buttonG60" name="delRow" id="delRow">- 삭제</button></div>
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:350px;">
						<table class="iptTblX2">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="5%" />
								<col width="5%" />
								<col width="5%" />
								<col width="10%" />
								<col width="15%" />
								<col width="15%" />
								<col width="15%" />
								<col width="15%" />
								<col width="15%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col" rowspan="2"></th>
									<th scope="col" rowspan="2">번호</th>
									<th scope="col" rowspan="2">구분</th>
									<th scope="col">모델명</th>
									<th scope="col">압수일시<br>(제출일시)</th>
									<th scope="col">피압수자<br>(제출자)</th>
									<th scope="col">압수자</th>
									<th scope="col">대상물<br>훼손 유무</th>
									<th scope="col">요청 전<br>구동유무</th>
								</tr>
								<tr>
									<th scope="col">시리얼번호</th>
									<th scope="col">압수장소<br>(제출장소)</th>
									<th scope="col">실사용자</th>
									<th scope="col">압수 시<br>봉인유부</th>
									<th scope="col">훼손<br>상세내역</th>
									<th scope="col">구동<br>상세내역</th>
								</tr>
							</thead>
							<tbody id="detailTbody">
							</tbody>
						</table>
						<span>※ 구분: 디스크, 서버(DB), 모바일, 데이터, 기타로 압수물, 임의제출물을 구별하여 기제</span>
					</div>
					<div class="sub_ttl">분석 대상 상세정보</div>
					<div class="t_list">
						<table class="iptTblX">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="15%" />
								<col width="85%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">피의사실</th>
									<td>
										<textarea id="analSuptFact" name="analSuptFact" rows=1 cols=30></textarea>
										<span class="txt_info" name="analSuptFactByteChk" id="analSuptFactByteChk"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">주요 분석요청 사항</th>
									<td>
										<textarea id="analReqInfo" name="analReqInfo" rows=1 cols=30></textarea>
										<span class="txt_info" name="analReqInfoByteChk" id="analReqInfoByteChk"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="t_list">
						<table class="iptTblX">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="15%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="45%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">구분</th>
									<th scope="row">문서파일<br>분석</th>
									<th scope="row">메일/메신저<br>분석</th>
									<th scope="row">웹히스토리<br>분석</th>
									<th scope="row">사진/동영상<br>분석</th>
									<th scope="row">키워드</th>
								</tr>
								<tr>
									<td><span>내용</span></td>
									<td><input type="checkbox" name="analDocFile" value="Y" class="check_agree1"/></td>
									<td><input type="checkbox" name="analEmailMsgr" value="Y" class="check_agree1"/></td>
									<td><input type="checkbox" name="analWebHist" value="Y" class="check_agree1"/></td>
									<td><input type="checkbox" name="analPhotoVideo" value="Y" class="check_agree1"/></td>
									<td><input type="text" name="analKwrd" class="inpw80"/></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="t_list">
						<table class="iptTblX">
							<caption>파일첨부</caption>
							<colgroup>
								<col width="15%" />
								<col width="85%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">첨부파일</th>
									<td>
										<div style="OVERFLOW-Y:auto; width:100%; height:50px;">
											<button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list_1');" >찾아보기</button>
                                    		<ul id="file_list_1" class="file_list_1" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row">이미지파일(선별파일)</th>
									<td>
										<div style="OVERFLOW-Y:auto; width:100%; height:50px;">
											<button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list_2');" >찾아보기</button>
                                    		<ul id="file_list_2" class="file_list_2" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="btn_c">
						<ul>
							<li>
								<a href="javascript:void(0);" class='myButton' onclick="fn_save('aprv'); return false;">승인요청</a>
							</li>
							<li>
								<a href="javascript:void(0);" class='myButton' onclick="fn_save('save'); return false;">저장</a>
							</li>
							<li>
								<a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a>
							</li>
						</ul>
					</div>

			</div>
			</form>
		</div>
	</div>
</div>
<div id="incdtIncdtMListPop"></div>
<div id="divCrgrFindPop"></div>
<div id="incdtIncdtRDtlPop"></div>
</body>
</html>