<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 20.
 * 2. 작성자 : Leeji
 * 3. 화면명 : 분석지원요청 요청관리 목록
 * 4. 설명 : 분석지원요청 요청관리 목록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript">
var tabId;

var plusPgsStat = ",C03009"; 

$(document).ready(function() {
	
	$("#chkAll").click(function(){
		if($("#chkAll").prop("checked")) {
			$("input[id^=chk_]").prop("checked",true);
		} else {
			$("input[id^=chk_]").prop("checked",false); 
		} 
	});

	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
    gfn_calendarConfig("searchRegFromDt", "searchRegToDt", "minDate", "");
    gfn_calendarConfig("searchRegToDt", "searchRegFromDt", "maxDate", "");

    $('#searchRegToDt').val(gfn_dashDate(gfn_getCurDate(), "-"));
    $('#searchRegFromDt').val(gfn_dashDate(gfn_addMonth(gfn_getCurDate(), -1), "-"));
    
    var codeInfo = [{cdId:'C03',cd:comPgsStat+plusPgsStat,selectId:'searchPgsStat',type:'2', callbackNm:'fn_ajaxCodeCrimeGrpList'}];
    //fn_ajaxCodeList(codeInfo);
    gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});

    var $li = $('div.btn_c').find('li').not(':last');
    $li.each(function(index,item){
    	$(item).hide();
    });
    
    //버튼 권한별 보이기
    if(session_usergb == 'C01001') { //일반수사관
	
    	//승인요청,승인요청취소,선별확인,선별확인취소,등록
    	$li.filter('[class^=aprv]').show();
    	$li.filter('[class^=selCfrm]').show();
    	$li.filter('[class^=insAnlsReq]').show();
    	
	} else if(session_usergb == 'C01002') { //군검사
    
		//승인,승인취소
		$li.filter('[class^=prosr]').show();
		
	} else if(session_usergb == 'C01003') { //포렌식 수사관
		$li.filter('[class^=insAnlsReq]').show();	
	}
    
    
});

function fn_ajaxCodeCrimeGrpList(){
	var codeInfo = [{cdId:'crimeGrp',selectId:'srcCrimeGrp',type:'4', callbackNm:'fn_ajaxCodeCrimeGrpListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});
}

function fn_ajaxCodeCrimeGrpListCallback() {
	$('#srcCrimeGrp').prepend("<option value='' selected>전체</option");
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_codeCallBack"});
}

/**
 * 분석지원요청 요청관리 콤보박스 콜백함수
 * @param
 * @returns fn_anlsReqSearch
 */
function fn_codeCallBack(){
	$('#searchPgsStat').prepend('<option value="'+(comPgsStat+plusPgsStat)+'" selected>전체</option>');
	$('#searchPgsStat').on({"change":function(){
		fn_anlsReqSearch(1);
	}});
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_anlsReqSearch"});
}

/**
 * 분석지원요청 요청관리 조회
 * @param {string} page 항목에 대한 고유 식별자 
 * @returns fn_callBack
 */
function fn_anlsReqSearch(page){
	var callUrl = "<c:url value = "/anls/req/queryAnlsReqMList.do"/>";	
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callBack',page:$("#page").val(),perPageNum:10});
}

 /**
  * 분석지원요청 요청관리 조회 콜백함수
  * @param {object} data 조회한 결과데이터
  * @returns
  */
function fn_callBack(data){
	  
  	if($("#chkAll").prop("checked")) {
		$("#chkAll").prop("checked", false);
	}
	
	$("#anlsReqList").empty();
	$("#totalcnt").text(data.totalCount);
	
	if(data.anlsReqList.length < 1){
		$('#anlsReqList').append('<tr><td colspan="12">조회된 결과가 없습니다.</td></tr>');
	}else{
	
		$.each(data.anlsReqList, function(index, item){
			$('#anlsReqList').append("<tr><td><input type='checkbox' name='chk' id='chk_"+index+"' value='Y' class='check_agree1'/></td>"+
			"<td>"+item.analReqSno+"<input type='hidden' name='analReqSno' value='"+item.analReqSno+"'/></td>"+
			"<td>"+item.regDt+"</td>"+
			"<td>"+item.deptNm+"</td>"+
			"<td><a href='#' onclick=javascript:fn_detail('"+item.analReqSno+"','"+data.page+"')><u><strong>"+item.incdtNo+"</strong></u></a></td>"+
			"<td>"+item.incdtNm+"</td>"+
			"<td>"+item.reqCfsc+"외 "+item.reqCnt+"건</td>"+
			"<td>"+item.regrNm+"</td>"+
			"<td>"+item.prosrNm+"<br>"+item.prosrAprvDt+"</td>"+
			"<td>"+item.pgsStatNm+"<br>"+item.psgStatDt+"<input type='hidden' name='pgsStat' value='"+item.pgsStat+"'/></td>"+
			"<td>"+item.selReqCrgrNm+"<br>"+item.selReqDt+"</td>"+
			"<td>"+item.selCfrmCrgrNm+"<br>"+item.selCfrmDt+"</td></tr>");
		 });
	}
	
	data.__callFuncName__ ="fn_anlsReqSearch";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
	fn_setFrameSize();
	
}

/**
 * 분석지원요청 요청관리 등록화면이동
 * @param
 * @returns
 */
function fn_insAnlsReq(){
	var src = "<c:url value = "/anls/req/indexAnlsReqRDtl.do"/>";	
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 분석지원요청 요청관리 상세화면이동
* @param {string} analReqSno 분석요청일련번호
* @param {string} page 현재페이지 번호
* @returns
*/
function fn_detail(analReqSno,page){
	
	$('#analReqSno').val(analReqSno);
	requestUtil.setSearchForm("searchForm");
	
	var searchParam = $('#searchForm').serialize();	
	var src = "<c:url value = "/anls/req/indexAnlsReqInfo.do"/>?"+searchParam;
	parent.$('#'+tabId+' iframe').attr('src', src);	
}

/**
* 분석지원요청 요청관리 진행상태 변경요청
* @param {string} reqType 버튼별 type
* @returns fn_pgsStatCallBack
*/
function fn_modPgsStat(reqType){
	var cnt = 0;
	var tpArray = [];
	var Objtype = {};	
	var isChk = false;
	
	$('#anlsReqList > tr').each(function(index,item){

		var $chkbox = $(item).find('input[type=checkbox]');
		var $analReqSno = $(item).find('input[name=analReqSno]');
		var $pgsStat = $(item).find('input[name=pgsStat]');

		if($chkbox.is(':checked') == true){
			cnt++;
			switch (reqType) {
			case "aprv":
				if($pgsStat.val() == 'C03001'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 임시등록만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "prosrAprv":
			case "aprvCncl":
				if($pgsStat.val() == 'C03002'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 승인요청만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "prosrAprvCncl":
				if($pgsStat.val() == 'C03003'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 검사승인만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "selCfrm":
				if($pgsStat.val() == 'C03007'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 선별요청만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "selCfrmCncl":
				if($pgsStat.val() == 'C03008'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 선별확인만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			}

		}

	});
	
	if(cnt < 1){
		fn_showUserPage('선택해주세요');
		return;
	}
	
	if(isChk){
		return;
	}
	
	
	Objtype.type = reqType;
	var data = {rowDatas : tpArray, type : Objtype};
    var callUrl = "<c:url value='/anls/req/queryPgsStat.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_pgsStatCallBack'});
	
}

/**
* 분석지원요청 요청관리 진행상태 변경 콜백
* @param
* @returns
*/
function fn_pgsStatCallBack(){
	fn_anlsReqSearch(1);
}

</script>
<body>
<div id="con_wrap1">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div>
                  <div class="loca">
                    <div class="ttl">분석 요청 관리</div>
                    <div class="loca_list"></div>
                  </div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
                    <form id="searchForm" name="searchForm" onsubmit="return false;"> 
                    <input type="hidden" name="analReqSno" id="analReqSno"/>
                    <input type="hidden" id="page" name="page" value="1"/>
                    <div class="t_head">
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_anlsReqSearch(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="10%">
                                   <col width="25%">
                                   <col width="10%">
                                   <col width="25%">
                                   <col width="10%">
                                   <col width="20%">                                 
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">사건번호</th>
					            <td scope="col">
					               <input class="inpw80" type="text" name="searchIncdtNo" id="searchIncdtNo"/>
					            </td>
					            <th scope="col" class="hcolor">사건명</th>
					            <td scope="col">
					               <input class="inpw80" type="text" name="searchIncdtNm" id="searchIncdtNm"/>
					            </td>
					            <th scope="col" class="hcolor">작성일자</th>
					            <td scope="col">
					               <input class="inpw30" type="text" name="searchRegFromDt" id="searchRegFromDt"/>
					               <input class="inpw30" type="text" name="searchRegToDt" id="searchRegToDt"/>
					            </td>
					          </tr>
					          <tr>
					            <th scope="col" class="hcolor">담당자</th>
					            <td scope="col">
					               <input class="inpw80" type="text" name="searchRegrNm" id="searchRegrNm"/>
					            </td>
					            <th scope="col" class="hcolor">진행상태</th>
					            <td scope="col" >
					               <select name="searchPgsStat" id="searchPgsStat" class="selw6">
					               </select>
					            </td>
					            <th scope="col" class="hcolor">피의사실</th>
					            <td scope="col" >
					            	<input class="inpw80" type="text" name="analSuptFact" id="analSuptFact"/>
					            </td>
					          </tr>
					          <tr>
					          	<th scope="col" class="hcolor">요청번호</th>
					            <td scope="col" >
					          		<input class="inpw80" type="text" name="srcAnalReqSno" id="srcAnalReqSno"/>
					            </td>
					            <th scope="col" class="hcolor">접수번호</th>
					            <td scope="col" >
					            	<input class="inpw80" type="text" name="rcvNoAcp" id="rcvNoAcp"/>
					            </td>
					            <th scope="col" class="hcolor">분석요청사항</th>
					            <td scope="col" >
					            	<input class="inpw80" type="text" name="analReqInfo" id="analReqInfo"/>
					            </td>
					          </tr>
					          <tr>
					          	<th scope="col" class="hcolor">죄명</th>
					            <td scope="col" >
					            	<select id="srcCrimeGrp" name="srcCrimeGrp" title="죄명" onchange="fn_anlsReqSearch(1);" class="selw6">
								               </select>
					            </td>
					            <th scope="col" class="hcolor"></th>
					            <td scope="col" >
					            </td>
					            <th scope="col" class="hcolor"></th>
					            <td scope="col" >
					            </td>
					          </tr>
                           </thead>
                        </table>
                        <div class="btn_c">
				       	<ul>
                          <li class="prosrAprv"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('prosrAprv');return false;">승인</a></li>
                          <li class="prosrAprvCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('prosrAprvCncl');return false;">승인취소</a></li>
                          <li class="aprv"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('aprv');return false;">승인요청</a></li>
                          <li class="aprvCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('aprvCncl');return false;">승인요청취소</a></li>
                          <li class="selCfrm"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selCfrm');return false;">선별확인</a></li>
                          <li class="selCfrmCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selCfrmCncl');return false;">선별확인취소</a></li>
                          <li class="insAnlsReq"><a href="javascript:void(0);" class='RdButton' onclick="fn_insAnlsReq(); return false;">등록</a></li>
                          <li><a href="javascript:void(0);" class='gyButton' onclick="fn_anlsReqSearch(1); return false;">조회</a></li>
                        </ul>   
					  </div>
                      
                      </div>
                     </form>
                      
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
                     <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>                     
                     <!--------------목록---------------------->
                     <div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:450px;"> 
                          <table class="tbl_type" border="1" cellspacing="0" >
                                <caption>분석지원요청</caption>
                                  <colgroup>
                                      <col width="2%">                                      
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="10%">
                                      <col width="15%">
                                      <col width="15%">
                                      <col width="10%">
                                      <col width="5%">
                                      <col width="5%">
                                      <col width="6%">
                                      <col width="6%">
                                      <col width="6%">
                                   </colgroup>
                                    <thead>
                                      <tr>
                                         <th scope="col"><input type='checkbox' name='chkAll' id='chkAll' title='' class='check_agree1'/></th>
                                         <th scope="col">요청번호</th>
                                         <th scope="col">등록일자</th>
                                         <th scope="col">부서명</th>
                                         <th scope="col">사건번호</th>
                                         <th scope="col">사건명</th>
                                         <th scope="col">분석요청건수</th>
                                         <th scope="col">담당자</th>
                                         <th scope="col">주임검사<br>(승인일자)</th>
                                         <th scope="col">진행상태</th>
                                         <th scope="col">선별요청</th>
                                         <th scope="col">선별확인</th>
                                      </tr>
                                    </thead>
                                    <tbody id="anlsReqList">
                                    </tbody>
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                     <div id='page_navi' class="page_wrap"></div>                            
                       <!-----------------------//페이징----------------------->                  
                  </div>
                 
            </div>
    </div>
</div>
</body>
</html>