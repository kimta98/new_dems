<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 20.
 * 2. 작성자 : leeji
 * 3. 화면명 : 분석지원요청 진행관리 분석결과 상세
 * 4. 설명 : 분석지원요청 진행관리 분석결과 상세
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<script type="text/javaScript">

var analReqSno = '${param.analReqSno}';
var pgsStat = "";
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	fn_anlsReqInfoSearch();
});

/**
 * 분석지원요청 진행관리 목록페이지이동
 * @param
 * @returns
 */
function fn_moveList(){
	var src = "<c:url value = "/anls/pgs/indexAnlsPgsMList.do"/>";
	parent.$('#'+tabId+' iframe').attr('height', 730);
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 분석지원요청 진행관리 상세화면 조회
* @param
* @returns anlsReqInfoCallBack
*/
function fn_anlsReqInfoSearch(){

	fn_rsltClose();

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");

	var input = document.createElement('input');
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "analReqSno");
	input.setAttribute("value", analReqSno);
	searchForm.appendChild(input);

	document.body.appendChild(searchForm);

	var callUrl = "<c:url value = "/anls/pgs/queryAnlsPgsInfo.do"/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'infoForm',callbackNm:'anlsReqInfoCallBack'});
}

/**
* 분석지원요청 진행관리 상세화면 조회 콜백함수
* @param {object} data 조회한 결과데이터
* @returns
*/
function anlsReqInfoCallBack(data){

	$('form[name=searchForm]').remove();

	var pgsStat = data.anlsReqInfoMap.pgsStat;

	//버튼 제어
	var $li = $('div#btn_c').find('li').not(':last');
    $li.each(function(index,item){
    	$(item).hide();
    });

    //버튼 권한별 보이기
    if(session_usergb == 'C01003') { //포렌식수사관

    	//검사승인상태면 접수
    	if(pgsStat == 'C03003'){
    		$li.filter('[class=rcv]').show();
    	}

    	//접수상태면 접수취소
    	if(pgsStat == 'C03005'){
	    	$li.filter('[class=rcvCncl]').show();
    	}

    	//선별요청상태면 선별요청취소
    	if(pgsStat == 'C03007'){
	    	$li.filter('[class=selReqCncl]').show();
    	}

    	//선별요청취소상태면 선별요청
    	if(pgsStat == 'C03008'){
	    	$li.filter('[class=selReq]').show();
    	}

	} else if(session_usergb == 'C01004') { //포렌식과장

		//문서접수상태면 과장승인
		if(pgsStat == 'C03005'){
	    	$li.filter('[class=aprv]').show();
    	}

		//과장승인상태면 승인취소
		if(pgsStat == 'C03006'){
	    	$li.filter('[class=aprvCncl]').show();
    	}

	}


	$("#detailTbody").empty();
	$('#rsltAnalReqSno').val('');
	$('#rsltEvdcSno').val('');
	$('#atchFileId').val('');

	var rcvrIdAcpNm = data.anlsReqInfoMap.rcvrIdAcpNm //접수담당자
	var rcvNoAcp = data.anlsReqInfoMap.rcvNoAcp //접수번호
	var rcvDtAcp = data.anlsReqInfoMap.rcvDtAcp //접수일자
	var aprvrIdNm = data.anlsReqInfoMap.aprvrIdNm //승인자
	var prosrAprvDt = data.anlsReqInfoMap.prosrAprvDt //승인일자
	var selReqCrgrNm = data.anlsReqInfoMap.selReqCrgrNm //선별요청담당자명
	var selReqDt = data.anlsReqInfoMap.selReqDt //선별요청일자
	var selCfrmCrgrNm = data.anlsReqInfoMap.selCfrmCrgrNm //선별확인담당자명
	var selCfrmDt = data.anlsReqInfoMap.selCfrmDt //선별확인일자

	//담당자(접수번호,접수일자) 승인자(승인일자) 선별요청담당자(선별요청일자) 선별확인담당자(선별확인일자)
	var rcvrIdAcp = rcvrIdAcpNm != '' ? rcvrIdAcpNm + '(' + rcvNoAcp + ',' + rcvDtAcp + ')' : '';
	var aprvrId = aprvrIdNm != '' ? aprvrIdNm + '(' + prosrAprvDt + ')' : '';
	var selReqCrgr = selReqCrgrNm != '' ? selReqCrgrNm + '(' + selReqDt + ')' : '';
	var selCfrmCrgr = selCfrmCrgrNm != '' ? selCfrmCrgrNm + '(' + selCfrmDt + ')' : '';

	$('span#rcvrIdAcp').text(rcvrIdAcp);
	$('span#aprvrId').text(aprvrId);
	$('span#selReqCrgr').text(selReqCrgr);
	$('span#selCfrmCrgr').text(selCfrmCrgr);

	$("span#analDocFile").text(data.anlsReqInfoMap.analDocFile);
	$("span#analEmailMsgr").text(data.anlsReqInfoMap.analEmailMsgr);
	$("span#analWebHist").text(data.anlsReqInfoMap.analWebHist);
	$("span#analPhotoVideo").text(data.anlsReqInfoMap.analPhotoVideo);
	$("span#analKwrd").text(data.anlsReqInfoMap.analKwrd);


	this.pgsStat = data.anlsReqInfoMap.pgsStat;
	var analStatNm = '';

	for(var i = 0; i < data.analysisEvdcDtlList.length; i++){

		if(data.analysisEvdcDtlList[i].rsltCnt > 0){
			analStatNm = "("+data.analysisEvdcDtlList[i].analStatNm+")";
		}else{
			analStatNm = "(미진행)";
		}
		
		// 접수상태에서 수사과장일 경우
		if(pgsStat == 'C03005' && session_usergb == 'C01004'){
			rcvrIdAcpSearch = '<input class="inpw70" title="담당자" type="text" name="rcvrIdAcpNm" id="rcvrIdAcpNm_'+i+'" readonly/>'+
								'<input type="hidden" name="rcvrIdAcp" id="rcvrIdAcp_'+i+'"/>'+
								'<button class="buttonG40" onclick="javascript:fn_openPop(\'analRcvr\', '+i+');return false;">검색</button>';
		}else{
			rcvrIdAcpSearch = data.analysisEvdcDtlList[i].rcvrIdAcpNm;
		}

		var tpTag =     '<tr>'+
							'<td rowspan="2">'+(i+1)+
							'<input type="hidden" name="analReqSno" id="analReqSno_'+i+'" value="'+data.analysisEvdcDtlList[i].analReqSno+'"/>'+
							'<input type="hidden" name="evdcSno" id="evdcSno_'+i+'" value="'+data.analysisEvdcDtlList[i].evdcSno+'"/></td>'+
							'<td><p><span id="rcvNoAcp'+i+'">'+data.analysisEvdcDtlList[i].rcvNoAcp+'</span> '+analStatNm+'</p></td>'+
							'<td><span id="cfscGoodsDiv_'+i+'">'+data.analysisEvdcDtlList[i].cfscGoodsDivNm+'</span></td>'+
							'<td><span id="mdlNm">'+data.analysisEvdcDtlList[i].mdlNm+'</span></td>'+
							'<td><span id="cfscDt">'+data.analysisEvdcDtlList[i].cfscDt+'</span></td>'+
							'<td><span id="psvCfscrSmitr">'+data.analysisEvdcDtlList[i].psvCfscrSmitr+'</span></td>'+
							'<td><span id="cfscCrgr">'+data.analysisEvdcDtlList[i].cfscCrgr+'</span></td>'+
							'<td><span id="trgtDmgdYn">'+(data.analysisEvdcDtlList[i].trgtDmgdYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td><span id="reqBefDrvYn">'+(data.analysisEvdcDtlList[i].reqBefDrvYn == 'Y' ? 'O' : 'X')+'</span></td>'+
						'</tr>'+
						'<tr>'+
							'<td><span id="cfscDiv_'+i+'">'+rcvrIdAcpSearch+'</span></td>'+
							'<td><span id="cfscDiv_'+i+'">'+data.analysisEvdcDtlList[i].cfscDivNm+'</span></td>'+
							'<td><span id="serlNo">'+data.analysisEvdcDtlList[i].serlNo+'</span></td>'+
							'<td><span id="cfscPlace">'+data.analysisEvdcDtlList[i].cfscPlace+'</span></td>'+
							'<td><span id="realUser">'+data.analysisEvdcDtlList[i].realUser+'</span></td>'+
							'<td><span id="sealYn">'+(data.analysisEvdcDtlList[i].sealYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td><span id="dmgdDtl">'+data.analysisEvdcDtlList[i].dmgdDtl+'</span></td>'+
							'<td><span id="drvDtl">'+data.analysisEvdcDtlList[i].drvDtl+'</span></td>'+
						'</tr>';
		$("#detailTbody").append(tpTag);
	}

    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileList.length; i++){
		var id = data.atchFileList[i].id;
		var name = data.atchFileList[i].name;
		var path = data.atchFileList[i].path;
		var upload_yn = data.atchFileList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list_1').append(html);
	}

    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileImgList.length; i++){
		var id = data.atchFileImgList[i].id;
		var name = data.atchFileImgList[i].name;
		var path = data.atchFileImgList[i].path;
		var upload_yn = data.atchFileImgList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list_2').append(html);
	}

	//분석기관 넣기
	$('#analInsttNm').text(data.session_deptnm);
}

/**
* 분석지원요청 진행관리 결과보기
* @param {object} obj 선택한 노드
* @returns
*/
function fn_viewRslt(obj){

	var rcvNoAcp = $(obj).parents('tr:eq(0)').find('span[id^=rcvNoAcp]').text();
	var mdlNm = $(obj).parents('tr:eq(0)').find('span[id^=mdlNm]').text();
	var evdcSno = $(obj).parents('tr:eq(0)').find('input[id^=evdcSno]').val();


	$('#rsltInfo').find('span#rcvNoAcp').text(rcvNoAcp);
	$('#rsltInfo').find('span#mdlNm').text(mdlNm);


	$('#rsltInfo').show();
	$('#btnSave').hide();
	$('#btnDel').show();
	fn_setFrameSize();

	$('#rsltInfo').find('tbody').each(function(index,item){

		var $el = $(item).find('input, textarea');

		$el.each(function(index,item){
			$('#'+item.id).val('');
		});
	});

	$('#file_list_3').html("");

	//분석결과 조회
	fn_anlsRsltSearch(analReqSno,evdcSno);


}

/**
* 분석지원요청 진행관리 팝업
* @param {string} 팝업종류별 type
* @returns
*/
function fn_openPop(type, idx){
	var callUrl = "<c:url value = "/com/PageLink.do"/>?type="+type
	requestUtil.mdPop({
		popUrl : callUrl+"&idx="+idx+"&link=dems/anls/req/crgrFindPop",
		height: 700,
        width: 1000,
        title: '담당자조회',
        divId : 'divCrgrFindPop'
	});
}

function fn_popCallBack(data, divId){

	$("#"+divId).dialog( "close" );
	$("#"+divId).empty();
	var idx = 0;
	$.each(data, function(index, value){
		if(index == 'idx'){
			idx = value;
		}else{
			$('#'+index+"_"+idx).val(value);	
		}
	});
}

/**
* 분석지원요청 진행관리 분석결과 조회
* @param {string} analReqSno 조회할 분석요청일련번호
* @param {string} evdcSno 조회할 증거일련번호
* @returns fn_anlsRsltSearchCallBack
*/
function fn_anlsRsltSearch(analReqSno,evdcSno){

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");

	var input = document.createElement('input');
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "analReqSno");
	input.setAttribute("value", analReqSno);
	searchForm.appendChild(input);

	var input = document.createElement('input');
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "evdcSno");
	input.setAttribute("value", evdcSno);
	searchForm.appendChild(input);

	document.body.appendChild(searchForm);

	var callUrl = "<c:url value = "/anls/pgs/goAnlsPsgUDtl.do"/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'insForm',callbackNm:'fn_anlsRsltSearchCallBack'});

}

/**
* 분석지원요청 진행관리 분석결과 조회 콜백함수
* @param {object} data 조회한 결과 데이터
* @returns
*/
function fn_anlsRsltSearchCallBack(data){
	$('form[name=searchForm]').remove();

	$('#file_list_3').html("");
    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileList.length; i++){
		var id = data.atchFileList[i].id;
		var name = data.atchFileList[i].name;
		var path = data.atchFileList[i].path;
		var upload_yn = data.atchFileList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list_3').append(html);
	}
}

/**
* 분석지원요청 진행관리 분석결과 화면닫기
* @param
* @returns
*/
function fn_rsltClose(){
	$('#rsltInfo').hide();
	fn_setFrameSize($('#contents_info').height());
}

/**
* 분석지원요청 진행관리 분석결과 첨부파일 다운로드
* @param {string} path 다운로드할 파일의 경로
* @returns
*/
function fn_viewFile(path){
	requestUtil.ftpFileAjax({data:{"type": "ftp_downloadFile","path": decodeURIComponent(path)},skip:'Y'});
}

/**
* 분석지원요청 진행상태 변경요청
* @param {string} reqType 버튼별 type
* @returns fn_moveList
*/
function fn_modPgsStat(reqType){

	var tpArray = [];
	var Objtype = {};
	var evdcArray = [];

	switch (reqType) {
	case "aprvCncl":
		// 승인취소 부분 수정 필요 - 담당자가 설정되고 분석을 분석중으로 변경 시 취소 불가
		// 승인취소 시 담당자 배정 혹은 접수담당자가 분석대기 상태로 존재할 경우 해당 분석결과 글 삭제와 동시에 상태 변경 필요. 
		if(pgsStat == 'C03006'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 과장승인만 가능합니다.');
			return false;
		}
		break;
	case "aprv":
		tpArray.push(analReqSno);
		if($("input[name=rcvrIdAcp]").length > 0){
			$("input[name=rcvrIdAcp]").each(function(index, value){
				var evdcSnoStr = $("input[name=evdcSno]:eq("+index+")").val();
				var rcvrIdAcpStr = $("input[name=rcvrIdAcp]:eq("+index+")").val();
				var tempMap = {};
				tempMap[evdcSnoStr] = rcvrIdAcpStr;
				evdcArray.push(tempMap);
			})
		};
		break;
	case "rcvCncl":
		if(pgsStat == 'C03005'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 문서접수만 가능합니다.');
			return false;
		}
		break;
	case "rcv":
		if(pgsStat == 'C03003'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 검사승인만 가능합니다.');
			return false;
		}
		break;
	case "selReq":
		if(pgsStat == 'C03006'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 과장승인만 가능합니다.');
			return false;
		}
		break;
	case "selReqCncl":
		if(pgsStat == 'C03007'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 선별요청만 가능합니다.');
			return false;
		}
		break;
	}

	Objtype.type = reqType;
	var data = {rowDatas : tpArray, type : Objtype, evdcDatas : evdcArray};
    var callUrl = "<c:url value='/anls/pgs/queryPgsStat.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_moveList'});

}
</script>
</head>
<body>
<div id="con_wrap1">
	<div class="content">
		<div id="contents_info">
			<div class="sub_ttl">분석 요청 결과 상세</div><!-----타이틀------>
				<div class="sub">
				<form name="infoForm" id="infoForm" method="POST" onsubmit="return false;">
					<div class="t_list">
						<table class="iptTblX">
							<caption>분석결과 상세화면</caption>
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">요청번호</th>
									<td><span id="analReqSno"></span></td>
									<th scope="row">검사(승인일자)</th>
									<td><span id="prosr"></span></td>
								</tr>
								<tr>
									<th scope="row">요청일시</th>
									<td><span id="reqDt"></span></td>
									<th scope="row">사건번호(사건명)</th>
									<td><span id="incdtNo"></span><p>(<span id="incdtNm"></span>)</p></td>
								</tr>
								<tr>
									<th scope="row">요청기관</th>
									<td>
										<span id="reqInsttNm"></span>
									</td>
									<th scope="row">요청부서</th>
									<td>
										<span id="reqDepNm"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">주임군검사</th>
									<td><span id="prosrNm"></span>
									</td>
									<th scope="row">담당자(연락처,HP)</th>
									<td><span id="reqUserNm"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					<div class="sub_ttl">분석 대상 상세정보</div>
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:350px;">
						<table class="iptTblX2">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="5%" />
								<col width="15%" />
								<col width="5%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="15%" />
								<col width="15%" />
								<col width="15%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col" rowspan="2">번호</th>
									<th scope="col">접수번호 (진행상태)</th>
									<th scope="col" rowspan="2">구분</th>
									<th scope="col">모델명</th>
									<th scope="col">압수일시<br>(제출일시)</th>
									<th scope="col">피압수자<br>(제출자)</th>
									<th scope="col">압수자</th>
									<th scope="col">대상물<br>훼손 유무</th>
									<th scope="col">요청 전<br>구동유무</th>
								</tr>
								<tr>
									<th scope="col">담당자</th>
									<th scope="col">시리얼번호</th>
									<th scope="col">압수장소<br>(제출장소)</th>
									<th scope="col">실사용자</th>
									<th scope="col">압수 시<br>봉인유부</th>
									<th scope="col">훼손<br>상세내역</th>
									<th scope="col">구동<br>상세내역</th>
								</tr>
							</thead>
							<tbody id="detailTbody">
							</tbody>
						</table>
					</div>
					<div id="rsltInfo" style="display:none">
					<div class="sub_ttl">분석결과</div>
					<form name="insForm" id="insForm" method="POST" onsubmit="return false;">
					<div class="btn_c">
						<ul>
							<li>
								<a href="javascript:void(0);" class='myButton' onclick="fn_rsltClose();return false;">닫기</a>
							</li>
						</ul>
					</div>
					<div class="t_list">
						<table class="iptTblX">
							<caption>분석결과 등록</caption>
							<colgroup>
								<col width="10%" />
								<col width="40%" />
								<col width="10%" />
								<col width="40%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">분석기관</th>
									<td><span id="analInsttNm"></span></td>
									<th scope="row">접수번호</th>
									<td><span id="rcvNoAcp"></span></td>
								</tr>
								<tr>
									<th scope="row">구분</th>
									<td><span id="mdlNm"></span></td>
									<th scope="row">분석담당자</th>
									<td>
										<span id="analCrgrNm"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">분석시작일</th>
									<td><span id="analStartDt"></span></td>
									<th scope="row">분석종료일</th>
									<td><span id="analEndDt"></span></td>
								</tr>
								<tr>
									<th scope="row">분석도구</th>
									<td>
										<span id="analToolNm"></span>
									</td>
									<th scope="row">분석상태</th>
									<td>
										<span id="analStatNm"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">분석내용</th>
									<td colspan="3">
										<textarea id="analRsltCnts" name="analRsltCnts" rows=3 cols=30 title="분석내용" readonly="readonly"></textarea>
									</td>
								</tr>
								<tr>
									<th scope="row">첨부파일</th>
									<td colspan="3">
										<div id="rsltFileList" style="OVERFLOW-Y:auto; width:100%; height:100px;">
		                                    <button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list_3');" >찾아보기</button>
		                                    <ul id="file_list_3" class="file_list_3" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					</div>
					<div class="t_list">
						<table class="iptTblX">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="15%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="45%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">구분</th>
									<th scope="row">문서파일<br>분석</th>
									<th scope="row">메일/메신저<br>분석</th>
									<th scope="row">웹히스토리<br>분석</th>
									<th scope="row">사진/동영상<br>분석</th>
									<th scope="row">키워드</th>
								</tr>
								<tr>
									<td><span>내용</span></td>
									<td><span id="analDocFile"></span></td>
									<td><span id="analEmailMsgr"></span></td>
									<td><span id="analWebHist"></span></td>
									<td><span id="analPhotoVideo"></span></td>
									<td><span id="analKwrd"></span></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="t_list">
						<table class="iptTblX">
							<caption>파일첨부</caption>
							<colgroup>
								<col width="15%" />
								<col width="85%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">첨부파일</th>
									<td>
										<div id="gen" style="OVERFLOW-Y:auto; width:100%; height:50px;">
											<ul id="file_list_1" class="file_list_1" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row">이미지파일(선별파일)</th>
									<td>
										<div id="img" style="OVERFLOW-Y:auto; width:100%; height:50px;">
											<ul id="file_list_2" class="file_list_2" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<!-- <div class="t_list">
						<table class="iptTblX">
							<caption>상세화면</caption>
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">담당자(접수번호,접수일자)</th>
									<td><span id="rcvrIdAcp"></span></td>
									<th scope="row">승인자(승인일자)</th>
									<td><span id="aprvrId"></span></td>
								</tr>
								<tr>
									<th scope="row">선별요청</th>
									<td><span id="selReqCrgr"></span></td>
									<th scope="row">선별확인</th>
									<td><span id="selCfrmCrgr"></span></td>
								</tr>
							</tbody>
						</table>
					</div> -->

					<div class="btn_c" id="btn_c">
						<ul>
							<li class="rcv"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('rcv');return false;">접수</a></li>
                          	<li class="rcvCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('rcvCncl');return false;">접수취소</a></li>
                          	<li class="aprv"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('aprv');return false;">승인</a></li>
                          	<li class="aprvCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('aprvCncl');return false;">승인취소</a></li>
                          	<li class="selReq"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selReq');return false;">선별요청</a></li>
                          	<li class="selReqCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selReqCncl');return false;">선별요청취소</a></li>
							<li><a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a></li>
						</ul>
					</div>
			</div>
		</div>
	</div>
</div>
<div id="divCrgrFindPop"></div>
</body>
</html>