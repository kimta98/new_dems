<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 20.
 * 2. 작성자 : leeji
 * 3. 화면명 : 분석지원요청 진행관리 분석결과 상세
 * 4. 설명 : 분석지원요청 진행관리 분석결과 상세
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<script type="text/javaScript">

var analReqSno = '${param.analReqSno}';
var pgsStat = "";
var tabId;

$(document).ready(function() {
	
	gfn_calendarConfig("histRegDt", "", "", "");
    $('#histRegDt').val(gfn_dashDate(gfn_getCurDate(), "-"));
    gfn_overMaxLength("trtCnts",2000);
    <%/* 파일업로드 세팅 */%>
    gfn_fileUpload("atchFile", "fileList4","file", 4);
    fn_fileCntChk();
    
	var codeInfo = [{cdId:'C20',selectId:'trtStatCd',type:'1', callbackNm:'fn_ajaxCodeListCallback'/* , sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList' */}];
	fn_ajaxCodeList(codeInfo);
	
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
});

/**
 * @!@ 증거분석이력 상태 코드 리스트 조회 콜백
 * @param {json} data 
 * @returns 
 */
function fn_ajaxCodeListCallback(data){
	$('#trtStatCd').prepend("<option value='' selected>선택</option");
	fn_anlsReqInfoSearch();
}
 
/**
 * 첨부파일갯수 체크
 * @param  
 * @returns
 */
function fn_fileCntChk(){
    var numCnt=0;
    $('#divFile :input[id^=fileList4FileSno]').each(function(index) {
        numCnt++;
    });

    if(numCnt > 4){
        $("#atchFile").hide();
    }else{
        $("#atchFile").show();
    }
}

/**
 * 분석지원요청 진행관리 목록페이지이동
 * @param
 * @returns
 */
function fn_moveList(){
	var src = "<c:url value = "/anls/pgs/indexAnlsPgsMList.do"/>";
	parent.$('#'+tabId+' iframe').attr('height', 730);
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 분석지원요청 진행관리 상세화면 조회
* @param
* @returns anlsReqInfoCallBack
*/
function fn_anlsReqInfoSearch(){
	
	//fn_rsltClose();

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");
	
	var input = document.createElement('input'); 
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "analReqSno");
	input.setAttribute("value", analReqSno);
	searchForm.appendChild(input);	
	
	document.body.appendChild(searchForm);
	
	var callUrl = "<c:url value = "/anls/pgs/queryAnlsPgsInfo.do"/>";	
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'infoForm',callbackNm:'anlsReqInfoCallBack'});
}

/**
* 분석지원요청 진행관리 상세화면 조회 콜백함수
* @param {object} data 조회한 결과데이터
* @returns
*/
function anlsReqInfoCallBack(data){

	$('form[name=searchForm]').remove();
	
	var pgsStat = data.anlsReqInfoMap.pgsStat;
	
    if(pgsStat != 'C03001' && pgsStat != 'C03002' && (session_usergb == 'C01003' || session_usergb == 'C01004')){
    	$('#regBtn').show();
    	$('#updBtn').show();
    	$('#delBtn').show();
    }
	
	$("#detailTbody").empty();	
	/* $('#rsltAnalReqSno').val('');
	$('#rsltEvdcSno').val('');
	$('#atchFileId').val(''); */	
	
	var rcvrIdAcpNm = data.anlsReqInfoMap.rcvrIdAcpNm //접수담당자
	var rcvNoAcp = data.anlsReqInfoMap.rcvNoAcp //접수번호
	var rcvDtAcp = data.anlsReqInfoMap.rcvDtAcp //접수일자
	var aprvrIdNm = data.anlsReqInfoMap.aprvrIdNm //승인자
	var prosrAprvDt = data.anlsReqInfoMap.prosrAprvDt //승인일자
	var selReqCrgrNm = data.anlsReqInfoMap.selReqCrgrNm //선별요청담당자명
	var selReqDt = data.anlsReqInfoMap.selReqDt //선별요청일자
	var selCfrmCrgrNm = data.anlsReqInfoMap.selCfrmCrgrNm //선별확인담당자명
	var selCfrmDt = data.anlsReqInfoMap.selCfrmDt //선별확인일자
	
	//담당자(접수번호,접수일자) 승인자(승인일자) 선별요청담당자(선별요청일자) 선별확인담당자(선별확인일자)
	var rcvrIdAcp = rcvrIdAcpNm != '' ? rcvrIdAcpNm + '(' + rcvNoAcp + ',' + rcvDtAcp + ')' : '';
	var aprvrId = aprvrIdNm != '' ? aprvrIdNm + '(' + prosrAprvDt + ')' : '';
	var selReqCrgr = selReqCrgrNm != '' ? selReqCrgrNm + '(' + selReqDt + ')' : '';
	var selCfrmCrgr = selCfrmCrgrNm != '' ? selCfrmCrgrNm + '(' + selCfrmDt + ')' : '';
	
	$('span#rcvrIdAcp').text(rcvrIdAcp);
	$('span#aprvrId').text(aprvrId);
	$('span#selReqCrgr').text(selReqCrgr);
	$('span#selCfrmCrgr').text(selCfrmCrgr);	
	
	$("span#analDocFile").text(data.anlsReqInfoMap.analDocFile);
	$("span#analEmailMsgr").text(data.anlsReqInfoMap.analEmailMsgr);
	$("span#analWebHist").text(data.anlsReqInfoMap.analWebHist);
	$("span#analPhotoVideo").text(data.anlsReqInfoMap.analPhotoVideo);
	$("span#analKwrd").text(data.anlsReqInfoMap.analKwrd);


	this.pgsStat = data.anlsReqInfoMap.pgsStat;
	var button = '';
	
	for(var i = 0; i < data.analysisEvdcDtlList.length; i++){	
		
		var evdcHistBtn = '';
		if(pgsStat != 'C03001' && pgsStat != 'C03002' && (session_usergb == 'C01003' || session_usergb == 'C01004' || session_usergb == 'C01999')){
			evdcHistBtn ='<button id="evdcHistBtn" name="evdcHistBtn" class="byButton" onclick="fn_queryAnlsPgsEvdcHistUList(\''+data.analysisEvdcDtlList[i].analReqSno+'\', \''+data.analysisEvdcDtlList[i].evdcSno+'\');return false;">증거분석이력관리</button>';
		}
		
		var tpTag =     '<tr>'+
							'<td rowspan="2">'+(i+1)+
							'<input type="hidden" name="analReqSno" id="analReqSno_'+i+'" value="'+data.analysisEvdcDtlList[i].analReqSno+'"/>'+
							'<input type="hidden" name="evdcSno" id="evdcSno_'+i+'" value="'+data.analysisEvdcDtlList[i].evdcSno+'"/></td>'+
							'<td rowspan="2"><p><span id="rcvNoAcp'+i+'">'+data.analysisEvdcDtlList[i].rcvNoAcp+'</span></p></td>'+
							'<td><span id="cfscGoodsDiv_'+i+'">'+data.analysisEvdcDtlList[i].cfscGoodsDivNm+'</span></td>'+
							'<td><span id="mdlNm">'+data.analysisEvdcDtlList[i].mdlNm+'</span></td>'+
							'<td><span id="cfscDt">'+data.analysisEvdcDtlList[i].cfscDt+'</span></td>'+
							'<td><span id="psvCfscrSmitr">'+data.analysisEvdcDtlList[i].psvCfscrSmitr+'</span></td>'+
							'<td><span id="cfscCrgr">'+data.analysisEvdcDtlList[i].cfscCrgr+'</span></td>'+
							'<td><span id="trgtDmgdYn">'+(data.analysisEvdcDtlList[i].trgtDmgdYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td><span id="reqBefDrvYn">'+(data.analysisEvdcDtlList[i].reqBefDrvYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td rowspan="2" style="text-align-last: center;">'+evdcHistBtn+'</td>'+
						'</tr>'+
						'<tr>'+
							'<td><span id="cfscDiv_'+i+'">'+data.analysisEvdcDtlList[i].cfscDivNm+'</span></td>'+
							'<td><span id="serlNo">'+data.analysisEvdcDtlList[i].serlNo+'</span></td>'+
							'<td><span id="cfscPlace">'+data.analysisEvdcDtlList[i].cfscPlace+'</span></td>'+
							'<td><span id="realUser">'+data.analysisEvdcDtlList[i].realUser+'</span></td>'+
							'<td><span id="sealYn">'+(data.analysisEvdcDtlList[i].sealYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td><span id="dmgdDtl">'+data.analysisEvdcDtlList[i].dmgdDtl+'</span></td>'+
							'<td><span id="drvDtl">'+data.analysisEvdcDtlList[i].drvDtl+'</span></td>'+
						'</tr>';
		$("#detailTbody").append(tpTag);	
	}
	
	//일반,선별파일리스트
	if(data.fileList != undefined){
		for(var i = 0; i < data.fileList.length; i++){		
			var path = (data.fileList[i].filePth+'\\'+data.fileList[i].fileNm).replace(/\\/gi, "//");
			var spanTag = '<p><span>'+data.fileList[i].fileNm+'</span><button class=button60 onclick=javascript:fn_viewFile(\''+encodeURIComponent(path)+'\')>다운로드</button></p>';
			$("div#"+data.fileList[i].atchFileDiv).append(spanTag);
		}		
	}
	
	//분석기관 넣기
	$('#analInsttNm').text(data.session_deptnm);
}

/**
* 분석지원요청 진행관리 결과보기
* @param {object} obj 선택한 노드
* @returns
*/
function fn_viewRslt(obj){	
	
	var rcvNoAcp = $(obj).parents('tr:eq(0)').find('span[id^=rcvNoAcp]').text();
	var mdlNm = $(obj).parents('tr:eq(0)').find('span[id^=mdlNm]').text();
	var evdcSno = $(obj).parents('tr:eq(0)').find('input[id^=evdcSno]').val();
	
	
	$('#rsltInfo').find('span#rcvNoAcp').text(rcvNoAcp);
	$('#rsltInfo').find('span#mdlNm').text(mdlNm);

		
	$('#rsltInfo').show();
	$('#btnSave').hide();
	$('#btnDel').show();
	fn_setFrameSize();

	$('#rsltInfo').find('tbody').each(function(index,item){
		
		var $el = $(item).find('input, textarea');

		$el.each(function(index,item){								
			$('#'+item.id).val('');
		});
	});
	
	//첨부파일 초기화
	$('#rsltFileList').empty();
	
	//분석결과 조회
	fn_anlsRsltSearch(analReqSno,evdcSno);

	
}

/**
* 분석지원요청 진행관리 분석결과 조회
* @param {string} analReqSno 조회할 분석요청일련번호
* @param {string} evdcSno 조회할 증거일련번호
* @returns fn_anlsRsltSearchCallBack
*/
function fn_anlsRsltSearch(analReqSno,evdcSno){

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");
	
	var input = document.createElement('input'); 
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "analReqSno");
	input.setAttribute("value", analReqSno);
	searchForm.appendChild(input);	
	
	var input = document.createElement('input'); 
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "evdcSno");
	input.setAttribute("value", evdcSno);
	searchForm.appendChild(input);	
	
	document.body.appendChild(searchForm);
	
	var callUrl = "<c:url value = "/anls/pgs/goAnlsPsgUDtl.do"/>";	
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'insForm',callbackNm:'fn_anlsRsltSearchCallBack'});
		
}

/**
* 분석지원요청 진행관리 분석결과 조회 콜백함수
* @param {object} data 조회한 결과 데이터
* @returns
*/
function fn_anlsRsltSearchCallBack(data){
	$('form[name=searchForm]').remove();
	
	//분석결과 첨부파일리스트
	if(data.fileList != undefined){
		for(var i = 0; i < data.fileList.length; i++){
			var path = (data.fileList[i].filePth+'\\'+data.fileList[i].fileNm).replace(/\\/gi, "//");
			var spanTag = '<p><span>'+data.fileList[i].fileNm+'</span><button class=button60 onclick=javascript:fn_viewFile(\''+encodeURIComponent(path)+'\')>다운로드</button></p>';
			$("div#rsltFileList").append(spanTag);
		}	
	}
}

/**
* 분석지원요청 진행관리 분석결과 화면닫기
* @param
* @returns
*/
/* function fn_rsltClose(){
	$('#rsltInfo').hide();
	fn_setFrameSize($('#contents_info').height());
} */

/**
* 분석지원요청 진행관리 분석결과 첨부파일 다운로드
* @param {string} path 다운로드할 파일의 경로
* @returns
*/
function fn_viewFile(path){
	requestUtil.ftpFileAjax({data:{"type": "ftp_downloadFile","path": decodeURIComponent(path)},skip:'Y'});
}

/**
* 분석지원요청 진행상태 변경요청
* @param {string} reqType 버튼별 type
* @returns fn_moveList
*/
function fn_modPgsStat(reqType){
	
	var tpArray = [];
	var Objtype = {};

	switch (reqType) {
	case "aprvCncl":
		if(pgsStat == 'C03006'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage("진행상태가 과장승인만 가능합니다.");
			return false;
		}
		break;
	case "aprv":
	case "rcvCncl":
		if(pgsStat == 'C03005'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 문서접수만 가능합니다.');
			return false;
		}
		break;
	case "rcv":
		if(pgsStat == 'C03003'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 검사승인만 가능합니다.');
			return false;
		}
		break;
	case "selReq":
		if(pgsStat == 'C03006'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 과장승인만 가능합니다.');
			return false;
		}
		break;
	case "selReqCncl":
		if(pgsStat == 'C03007'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 선별요청만 가능합니다.');
			return false;
		}
		break;
	}
	
	Objtype.type = reqType;
	var data = {rowDatas : tpArray, type : Objtype};
    var callUrl = "<c:url value='/anls/pgs/queryPgsStat.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_moveList'});
	
}

/**
* 분석지원요청 증거분석이력관리 리스트 조회 세팅
* @param analReqSno, evdcSno
* @returns
*/
function fn_queryAnlsPgsEvdcHistUList(analReqSno, evdcSno){
	$("#evdcHistForm").find("#analReqSno").val(analReqSno);
	$("#evdcHistForm").find("#evdcSno").val(evdcSno);
	$("#evdcHistForm").find("#histSno").val("");
	fn_evdcHistFormReset();
	fn_queryMList(1);
}

/**
* 분석지원요청 증거분석이력관리 상세 조회 세팅
* @param analReqSno, evdcSno, histSno
* @returns
*/
function fn_queryAnlsPgsEvdcHistUDtl(analReqSno, evdcSno, histSno, histRegDt, trtStatCd, trtCnts, atchFileId){
	$("#evdcHistForm").find("#analReqSno").val(analReqSno);
	$("#evdcHistForm").find("#evdcSno").val(evdcSno);
	$("#evdcHistForm").find("#histSno").val(histSno);
	
	$("#evdcHistForm").find("#histRegDt").val(histRegDt);
	$("#evdcHistForm").find("#trtStatCd").val(trtStatCd);
	$("#evdcHistForm").find("#trtCnts").val(trtCnts);
	$("#fileList4").empty();
	
	if(atchFileId != ""){
		var searchFileForm = document.createElement('form');
		searchFileForm.setAttribute("name","searchFileForm");
		searchFileForm.setAttribute("id","searchFileForm");
		
		var input = document.createElement('input'); 
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "atchFileId");
		input.setAttribute("value", atchFileId);
		searchFileForm.appendChild(input);	
		
		document.body.appendChild(searchFileForm);
		var callUrl = "/com/util/queryAtchFileList.do";
		requestUtil.search({callUrl:callUrl,srhFormNm:'searchFileForm',callbackNm:'queryAnlsPgsEvdcHistUDtlCallBack'});
	}
}

/**
* 파일 검색 콜백
* @param analReqSno, evdcSno, histSno
* @returns
*/
function queryAnlsPgsEvdcHistUDtlCallBack(data){
	$("#searchFileForm").remove();
	//파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileList.length; i++){
		var div =  $('<div class="fileList4Class" style="padding: 5px;">');
		//div.append(data.atchFileList[i].oriFileNm);
		div.append("<a href=javascript:fn_fileDown('"+data.atchFileList[i].filePth+"','"+data.atchFileList[i].fileNm+"','"+data.atchFileList[i].oriFileNm+"')>"+data.atchFileList[i].oriFileNm+"</a>");
		/* div.append('<a href="#" onclick="javascript_:this.parentNode.parentNode.removeChild(this.parentNode); fn_fileCntChk(); fn_del(this);return false;"><button type="button" style="width:53px;height:23px;background:url(<c:url value='/images/btn_board_02.gif' />);vertical-align:middle;" ></button></a><br/>'); */
		div.append('<a href="#" onclick="javascript_:this.parentNode.parentNode.removeChild(this.parentNode); fn_fileCntChk();return false;" class="buttonG35">삭제</a><br/>');
		div.append('<input id="fileList4FileSno" name="fileList4FileSno" type="hidden" value="'+data.atchFileList[i].fileSno +'"/>');
		div.append('<input id="fileList4FilePth" name="fileList4FilePth" type="hidden" value="'+ data.atchFileList[i].filePth +'"/>');
		div.append('<input id="fileList4FileNm" name="fileList4FileNm" type="hidden" value="'+data.atchFileList[i].fileNm +'"/>');
		div.append('<input id="fileList4FileSize" name="fileList4FileSize" type="hidden" value="'+data.atchFileList[i].fileSize +'"/>');
		div.append('<input id="fileList4OriFileNm" name="fileList4OriFileNm" type="hidden" value="'+data.atchFileList[i].oriFileNm+'"/>');
		div.append('</div>');
		$('#fileList4').append(div);
	}
}

/**
* @!@ 파일 다운로드
* @param {int} page
* @returns 
*/
function fn_fileDown(filePath,fileNm,atchFileNm) {
    gfn_fileNmDownload(filePath,fileNm,encodeURI(atchFileNm));
}

/**
* @!@ 파일 삭제
* @param {int} page
* @returns 
*/
/* function fn_del(arg){
	var filePth = $(arg).parents('div').find('#fileList4FilePth').val();
	var fileNm = $(arg).parents('div').find('#fileList4FileNm').val();
	fn_fileDel(filePth,fileNm);	
} */

/**
 * @!@ 증거분석이력 관리 리스트 조회
 * @param {int} page
 * @returns 
 */
function fn_queryMList(page){
	var callUrl = "<c:url value='/anls/pgs/queryAnlsPgsEvdcHistMList.do'/>";
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'evdcHistForm', callbackNm:'fn_queryMListCallback', page:page, perPageNum:10});
}
 
/**
 * @!@ 증거분석이력 관리 리스트 조회 콜백
 * @param {json} data
 * @returns 
 */
function fn_queryMListCallback(data){
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;
	
	$("#listTab > tbody").empty();
	
	if(listCnt == 0){
		var append = "";
		append += "<tr>";
		
		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";
		
		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
		$.each(list,function(idx,row){
			var append = "";
			append += "<tr>";
	
			append += "<td>" + gfn_nullRtnSpace(row.evdcSno) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.rcvNoAcp) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.histRegDt) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.trtStatNm) + "</td>";
			append += '<td><a href="javascript:void(0);" onclick="fn_queryAnlsPgsEvdcHistUDtl(\''+gfn_nullRtnSpace(row.analReqSno)+'\' ,\''+gfn_nullRtnSpace(row.evdcSno)+'\',\''+gfn_nullRtnSpace(row.histSno)+'\',\''+gfn_nullRtnSpace(row.histRegDt)+'\',\''+gfn_nullRtnSpace(row.trtStatCd)+'\',\''+gfn_nullRtnSpace(row.trtCnts)+'\',\''+gfn_nullRtnSpace(row.atchFileId)+'\');return false;">'+gfn_nullRtnSpace(row.trtCnts)+'</a></td>';
			append += "<td>" + gfn_nullRtnSpace(row.crgrId) + "</td>";
			append += "<td>" + "파일파일" + "</td>";
	
			append += "</tr>";
	        $("#listTab > tbody").append(append);
	 	});
	}
	
	data.__callFuncName__ ="fn_queryMList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
	$("#totalCount").text(data.totalCount);
	
	fn_evdcHistFormReset();
}
 
 /**
  * @!@ 증거분석이력 등록 폼 초기화
  * @param {int} page
  * @returns 
  */
function fn_evdcHistFormReset(){
	gfn_calendarConfig("histRegDt", "", "", "");
	$("#trtStatCd").val("");
	$("#trtCnts").val("");
	$("#action").val("R");
	$("#histSno").val("");
	$("#fileList4").empty();
}
 
/**
 * @!@ 증거분석이력 등록, 저장
 * @param
 * @returns 
 */
function fn_regRDtl(action){
	if(action == "R"){
		if($("#evdcHistForm").find("#analReqSno").val() == "" || $("#evdcHistForm").find("#evdcSno").val() == ""){
			fn_showUserPage("증거분석이력관리 버튼을 클릭 해주세요.", function() {
			});
			return false;
		}
	}else if(action == "U"){
		if($("#evdcHistForm").find("#histSno").val() == ""){
			fn_showUserPage("수정하실 내용을 클릭 해주세요.", function() {
			});
			return false;
		}
	}
	
	if(!validUtil.checkInputValid({valFormID:'evdcHistForm'})){
		return;
	}
	
	$("#evdcHistForm").find("#action").val(action);
	
	var mode = "";
	if(action == "R"){
		mode = "등록"
	}else if(action == "U"){
		mode = "저장";
	}
	fn_showModalPage(mode + " 하시겠습니까?", function() {
		var callUrl = "<c:url value='/anls/pgs/regAnlsPgsEvdcHistUDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'evdcHistForm',callbackNm:'fn_regRDtlCallback'});
	});
}

/**
 * @!@ 증거분석이력 등록 콜백
 * @param {json} data
 * @returns 
 */
function fn_regRDtlCallback(data){
	fn_queryMList(1);
}
 
/**
 * @!@ 증거분석이력 삭제
 * @param
 * @returns 
 */
function fn_delRDtl(){
	if($("#evdcHistForm").find("#histSno").val() == ""){
		fn_showUserPage("삭제하실 내용을 클릭 해주세요.", function() {
		});
		return false;
	}
	
	fn_showModalPage("삭제 하시겠습니까?", function() {
		var callUrl = "<c:url value='/anls/pgs/delAnlsPgsEvdcHistUDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'evdcHistForm',callbackNm:'fn_delRDtlCallback'});
	});
}
 
/**
 * @!@ 증거분석이력 삭제
 * @param {json} data
 * @returns 
 */
function fn_delRDtlCallback(data){
	fn_queryMList(1);
}
 
</script>
</head>
<body>
<div id="con_wrap1">
	<div class="content">
		<div id="contents_info">
			<div class="sub_ttl">분석 요청 결과 상세</div><!-----타이틀------>
				<div class="sub">						
				<form name="infoForm" id="infoForm" method="POST" onsubmit="return false;">		
					<div class="t_list">
						<table class="iptTblX">
							<caption>분석결과 상세화면</caption>
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">요청번호</th>
									<td><span id="analReqSno"></span></td>
									<th scope="row">검사(승인일자)</th>
									<td><span id="prosr"></span></td>									
								</tr>
								<tr>
									<th scope="row">요청일시</th>
									<td><span id="reqDt"></span></td>									
									<th scope="row">사건번호(사건명)</th>
									<td><span id="incdtNo"></span><p>(<span id="incdtNm"></span>)</p></td>
								</tr>								
								<tr>
									<th scope="row">요청기관</th>
									<td>
										<span id="reqInsttNm"></span>
									</td>
									<th scope="row">요청부서</th>
									<td>
										<span id="reqDepNm"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">주임군검사</th>
									<td><span id="prosrNm"></span>
									</td>
									<th scope="row">담당자(연락처,HP)</th>
									<td><span id="reqUserNm"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					<div class="sub_ttl">분석 대상 상세정보</div>
					<div class="t_list" style="OVERFLOW-Y:scroll; width:100%; height:300px;">
						<table class="iptTblX2">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="5%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col" rowspan="2">번호</th>
									<th scope="col" rowspan="2">접수번호</th>
									<th scope="col" rowspan="2">구분</th>
									<th scope="col">모델명</th>
									<th scope="col">압수일시<br>(제출일시)</th>
									<th scope="col">피압수자<br>(제출자)</th>
									<th scope="col">압수자</th>
									<th scope="col">대상물<br>훼손 유무</th>
									<th scope="col">요청 전<br>구동유무</th>
									<th scope="col" rowspan="2">증거분석이력</th>
								</tr>
								<tr>
									<th scope="col">시리얼번호</th>
									<th scope="col">압수장소<br>(제출장소)</th>
									<th scope="col">실사용자</th>
									<th scope="col">압수 시<br>봉인유부</th>
									<th scope="col">훼손<br>상세내역</th>
									<th scope="col">구동<br>상세내역</th>
								</tr>
							</thead>
							<tbody id="detailTbody">
							</tbody>
						</table>
					</div>
					
					<div class="sub_ttl" style="float:none;">증거 분석 이력</div>
					<div class="r_num">| 결과  <strong style="color:#C00" id="totalCount">0</strong>건</div>
                             
                             <!--------------목록---------------------->
                             <div class="t_list" style="height: 400px;">  
                                  <table id="listTab" class="tbl_type" border="1" cellspacing="0">
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="5%">
                                              <col width="10%">
                                              <col width="10%">
                                              <col width="10%">
                                              <col width="45%">
                                              <col width="10%">
                                              <col width="10%">
                                          </colgroup>
                                            <thead>
                                              <tr>
                                                 <th scope="col">증거번호</th>
                                                 <th scope="col">접수번호</th>
                                                 <th scope="col">등록일자</th>
                                                 <th scope="col">상태</th>
                                                 <th scope="col">처리내용</th>
                                                 <th scope="col">담당자</th>
                                                 <th scope="col">파일</th>
                                              </tr>
                                            </thead>
                                            <tbody>
												<tr><td colspan="7">조회 결과가 없습니다.</td></tr>
                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->
                             
                             <!-----------------------페이징----------------------->
                             <div id="page_navi" class="page_wrap" style="height: 40px;" ></div>
                             
					<form name="evdcHistForm" id="evdcHistForm" method="post">
                          	<input id="analReqSno" name="analReqSno"  title="분석요청일련번호"  type="hidden" class="inpw50" value=""/>
                           	<input id="evdcSno" name="evdcSno"  title="증거일련번호"  type="hidden" class="inpw50" value=""/>
                           	<input id="histSno" name="histSno"  title="이력일련번호"  type="hidden" class="inpw50" value=""/>
                           	<input id="action" name="action"  title="모드"  type="hidden" class="inpw50" value=""/>
                      <div class="t_list">
                          <div class="sub_ttl">증거 분석 이력 등록</div><!-----타이틀------>
		                 <table class="iptTblX">
			               <caption>등록</caption>
			               <colgroup>
				             <col width="20%" />
				             <col width="30%" />
				             <col width="20%" />
				             <col width="30%" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">등록일자<span class="fontred">*</span></th>
				                 <td>
				                 	<input id="histRegDt" name="histRegDt"  title="등록일자"  type="text" readonly class="inpw40" data-requireNm="등록일자" data-maxLength="10"/>
				                 </td>
				                 <th scope="row">처리상태<span class="fontred">*</span></th>
				                 <td>
				                 	<select name="trtStatCd" id="trtStatCd" class="selw15" title="처리상태" data-requireNm="처리상태" data-maxLength="6">
				                 	</select>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">처리내용<span class="fontred">*</span></th>
				                 <td colspan="3">
				                 	<textarea name="trtCnts" id="trtCnts" rows="3" title="처리내용" data-requireNm="처리내용" data-maxLength="2000"></textarea>
	                                <span class="txt_info" name="trtCntsByteChk" id="trtCntsByteChk"></span>
				                 </td>
			                 </tr>
			                 <tr id="divFile">
				                 <th scope="row">첨부파일<button id="atchFile" name="atchFile" type="button" class="btn_sty3" style="float:right;" >찾아보기</button></th>
				                 <td colspan="3">
                                    <div id="fileList4" name="fileList4" style="height: 100px; OVERFLOW-Y:auto; width: 100%;">
                                    </div>
				                 </td>
			                 </tr>
			                </tbody>
		                 </table>
	                  </div>
					</form>
					
					<div class="btn_c" id="btn_c">
						<ul>
							<li><a href="javascript:void(0);" class="RdButton" onclick="fn_regRDtl('R');return false;" id="regBtn" style="display: none;">등록</a></li>
							<li><a href="javascript:void(0);" class="RdButton" onclick="fn_regRDtl('U');return false;" id="updBtn" style="display: none;">저장</a></li>
							<li><a href="javascript:void(0);" class="RdButton" onclick="fn_delRDtl();return false;" id="delBtn" style="display: none;">삭제</a></li>
							<li><a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a></li>
						</ul>
					</div>
			</div>			
		</div>
	</div>
</div>
</body>
</html>