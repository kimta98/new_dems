<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 22. 오후 5:21:45
 * 2. 작성자 : 이종인
 * 3. 화면명 : 담당자 조회 팝업
 * 4. 설명 : 담당자 조회 팝업
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javaScript" language="javascript">
var tabId;
var analReqSno = '${param.analReqSno}';
var evdcSno = '${param.evdcSno}';
var rcvrIdAcp = '${param.rcvrIdAcp}';
var rcvNoAcp = '${param.rcvNoAcp}';
var analStat = "";

$(document).ready(function() {
	//선별이미지파일 업로드 hide
	$(".checkFileList").hide();
	
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	$("#analReqSnoPop").val(analReqSno);
	$("#evdcSnoPop").val(evdcSno);
	
	$('#analStartDtPop').datepicker("destroy");
	$('#analEndDtPop').datepicker("destroy");
	gfn_calendarConfig("analStartDtPop", "analEndDtPop", "minDate", "");
	gfn_calendarConfig("analEndDtPop", "analStartDtPop", "maxDate", "");
	
	$("#rcvNoAcp").text(rcvNoAcp);
	
	//select박스 가져오기
	var codeInfo = [{cdId:'C06',selectId:'analStatPop' ,type:'1', callbackNm:'fn_codeCallBack'},{cdId:'C13',selectId:'analToolPop' ,type:'1'}];
	fn_ajaxCodeList(codeInfo);
	
	//fn_checkType(analReqSno, evdcSno, rcvrIdAcp);
});
 

/**
 * 수정 / 등록 상태 체크
 */
function fn_checkType(analReqSno, evdcSno, rcvrIdAcp){
	
	if(rcvrIdAcp != "" && rcvrIdAcp == session_userid){
		//수정
		$("#regBtn").hide();
		//$("#delBtn").show();
		$("#modifyBtn").show();
		$(".sub_ttl").html("");
		$(".sub_ttl").html("분석 결과 상세");
		fn_getEvdcRslt(analReqSno, evdcSno, rcvrIdAcp);
	}else if(rcvrIdAcp == ""){
		//등록
		$("#regBtn").show();
		//$("#delBtn").hide();
		$("#modifyBtn").hide();
		$(".sub_ttl").html("");
		$(".sub_ttl").html("분석 결과 등록");
	}else{
		//잘못 된 접근
		fn_popCallBack({},"divEvdcRsltPop");
	}
}

function fn_getEvdcRslt(analReqSno, evdcSno, rcvrIdAcp) {
	var data = $("#popInsFormPop").serialize();
	$.ajax({
        type : "POST",
        url : "<c:url value = "/anls/pgs/goAnlsPsgUDtl.do"/>",
        data : data,
        async: false,
        dataType : "json",
        success : function(data){
        	
      		$("#analCrgrIdPop").val(data.infoMap.analCrgrId);
      		$("#analCrgrNmPop").val(data.infoMap.analCrgrNm);
      		$("#analStatPop").val(data.infoMap.analStat).prop("selected", true);
      		// 상태 변경 시 이미지파일 remove 처리 조건을 위한 전역변수 처리
      		analStat = data.infoMap.analStat;
      		$("#analToolPop").val(data.infoMap.analTool).prop("selected", true);
      		$("#analStartDtPop").val(data.infoMap.analStartDt);
      		$("#analEndDtPop").val(data.infoMap.analEndDt);
      		$("#analRsltCntsPop").val(data.infoMap.analRsltCnts);
      		if(data.infoMap.analStat == "C06002" || data.infoMap.analStat == "C06003"){
	      		$(".checkFileList").show();
	      		//파일첨부리스트 만들기
	      		for(var i = 0; i < data.anlsCheckFileList.length; i++){
	      			var id = data.anlsCheckFileList[i].id;
	      			var name = data.anlsCheckFileList[i].name;
	      			var path = data.anlsCheckFileList[i].path;
	      			var upload_yn = data.anlsCheckFileList[i].upload_yn;
	      			var html = "";
	      			if(data.infoMap.analStat == "C06003"){
	      				// 1, 2 session_id / user_id 다르게 보내줘서 삭제가 안뜨도록 하드코딩
	      				html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N', '1', '2') : agent.gridLocalFile(name , '' , 'N');
	      			}else{
		      			html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridLocalFile(name , '' , 'N');
	      			}
	      			$('#file_list_4').append(html);
	      		}
	      		$('#file_list_4').prevAll("#fileSpan").text("총 " + data.anlsCheckFileList.length + "건");
      		}else{
      			$(".checkFileList").find('li').remove();
      			$(".checkFileList").hide();
      		}
      		
      		if(data.infoMap.analStat == "C06003"){
      			$("#modifyBtn").hide();
      		}
      		
      	//파일첨부리스트 만들기
      		for(var i = 0; i < data.anlsTotalFileList.length; i++){
      			var id = data.anlsTotalFileList[i].id;
      			var name = data.anlsTotalFileList[i].name;
      			var path = data.anlsTotalFileList[i].path;
      			var upload_yn = data.anlsTotalFileList[i].upload_yn;
      			if(data.infoMap.analStat == "C06003"){
      				// 1, 2 session_id / user_id 다르게 보내줘서 삭제가 안뜨도록 하드코딩
      				html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N', '1', '2') : agent.gridLocalFile(name , '' , 'N');
      			}else{
	      			html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridLocalFile(name , '' , 'N');
      			}
      			$('#file_list_3').append(html);
      		}
      		$('#file_list_3').prevAll("#fileSpan").text("총 " + data.anlsTotalFileList.length + "건");
      	    
      		//파일첨부리스트 만들기
      		for(var i = 0; i < data.anlsFileList.length; i++){
      			var id = data.anlsFileList[i].id;
      			var name = data.anlsFileList[i].name;
      			var path = data.anlsFileList[i].path;
      			var upload_yn = data.anlsFileList[i].upload_yn;
      			if(data.infoMap.analStat == "C06003"){
      				// 1, 2 session_id / user_id 다르게 보내줘서 삭제가 안뜨도록 하드코딩
      				html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N', '1', '2') : agent.gridLocalFile(name , '' , 'N');
      			}else{
	      			html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridLocalFile(name , '' , 'N');
      			}
      			$('#file_list_5').append(html);
      		}
      		$('#file_list_5').prevAll("#fileSpan").text("총 " + data.anlsFileList.length + "건");
      		
        }
    });
}

function fn_openPopByPop(type){
	var callUrl = "<c:url value = "/com/PageLink.do"/>?type="+type

	requestUtil.mdPop({
		popUrl : callUrl+"&link=dems/anls/req/crgrFindPopByPop",
		height: 700,
        width: 1000,
        title: '담당자조회',
        divId : 'divCrgrFindPopByPop'
	});
}


function fn_popCallBackByPop(data, divId){
	$("#"+divId).dialog( "close" );
	$("#"+divId).empty();

	$.each(data, function(index, value){
		if($('#'+index).is('span')){
			$('#'+index).text(value);
		}else{
			$('#'+index).val(value);
		}
	});
}

function fn_codeCallBack(){
	$('#analStatPop').prepend('<option value="" selected>선택</option>');
	$('#analToolPop').prepend('<option value="" selected>선택</option>');
	fn_checkType(analReqSno, evdcSno, rcvrIdAcp);
}

function fn_dateClear(obj) {
	$("#"+obj.id).val("");
	if(obj.id == "analStartDtPop"){
		gfn_calendarConfig("analStartDtPop", "analEndDtPop", "minDate", "");
		gfn_calendarConfig("analEndDtPop", "analStartDtPop", "maxDate", "");
	}
}

/**
 * 분석 결과 등록
 */
function fn_setEvdcRslt() {
	
	if(!fn_checkRequired()){
		return false;	
	}
	
	fn_checkComplete('insert');
}

/**
 * 분석 결과 수정
 */
function fn_modEvdcRslt() {

	if(!fn_checkRequired()){
		return false;	
	}
	
	fn_checkComplete('update');
}

function fn_requestEvdcRslt(callUrlType, callBackType) {
	
	var $formDatas = $('#popInsFormPop');
	var tpArray =[];

	var formObj = new Object();

	$formDatas.each(function(index,item){

		var $el = $(item).find('input, select, textarea').not('input[type=file]');

		$el.each(function(index,item){
			formObj[item.name] = item.value;
		});

	});
	
	var data = {fileDatas : tpArray, formDatas : formObj};
	var callUrl = "";
	if(callUrlType == 'insert'){
		callUrl = "<c:url value='/anls/pgs/reqAnlsPgsRDtl.do'/>";	
	}else{
		callUrl = "<c:url value='/anls/pgs/updAnlsPgsUDtl.do'/>";
	}
	// skip : Y 콜백 함수 바로 실행 - agent 업로드 요청 보내도 신규 파일 아니면 등록 안되므로 Y 로 고정
	// 콜백 받은 데이터 SEQ 번호 알기 위해서라도 필요.
	if(callBackType == 'remove'){
		requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_removeCallback',skip:'Y'});
	}else{		
		requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_uploadCallback',skip:'Y'});
	}
	
}

/**
 * 분석 결과 삭제
 */
function fn_delEvdcRslt() {
	
}

function fn_checkRequired() {
	
	if($('#analCrgrNmPop').val() == ""){
		fn_showUserPage("분석 담당자가 선택되지 않았습니다.");
		return false;
	}
	
	if($('#analToolPop').val() == ""){
		fn_showUserPage("분석 도구가 선택되지 않았습니다.");
		return false;
	}
	
	if($('#analStatPop').val() == ""){
		fn_showUserPage("분석 상태가 선택되지 않았습니다.");
		return false;
	}
	
	
	if($('#analStatPop').val() == "C06002" && $('#analStartDtPop').val() == ""){
		//분석상태 : 분석중
		fn_showUserPage("분석중 저장 시 분석시작일을 선택하세요.");
		return false;
	}else if($('#analStatPop').val() == "C06003" && $('#analStartDtPop').val() == "" || $('#analStatPop').val() == "C06003" && $('#analEndDtPop').val() == ""){
		//분석상태 : 분석완료
		fn_showUserPage("분석완료 저장 시 분석시작 및 분석종료일을 모두 선택하세요.");
		return false;
	}
	
	return true;
}

/**
 * 분석완료 등록 / 수정 시 체크
 */
function fn_checkComplete(callUrlType) {
	
	if($('#analStatPop').val() == "C06003"){
		fn_showConfirmPage("분석완료 저장 시 수정이 불가능합니다.\n 진행하시겠습니까?").then(res => {
			if(res){
				//분석완료 진행
				fn_showConfirmPage("분석완료 저장을 진행합니다. 전체이미지파일을 파기하시겠습니까?\n 확인 선택 시 파일서버에서 삭제됩니다.").then(res => {
					if(res){
						//파기 진행
						fn_requestEvdcRslt(callUrlType, 'remove');
					}else{
						fn_requestEvdcRslt(callUrlType);
					}
				});
			}else{
				return false;
			}
		}); 
	}else{
		fn_requestEvdcRslt(callUrlType);
	}
}

function fn_uploadCallback(result) {
	
	agent.uploadFile({
		obj_id : "file_list_3",
		target_id : result.analReqSno + "-" + result.evdcSno,
		type : "anlsPgsTotal",
		creator_id : session_userid
	});
	
	if($("#analStatPop").val() == 'C06002' || $("#analStatPop").val() == 'C06003'){
		agent.uploadFile({
			obj_id : "file_list_4",
			target_id : result.analReqSno + "-" + result.evdcSno,
			type : "anlsPgsCheck",
			creator_id : session_userid
		});
	}
	
	agent.uploadFile({
		obj_id : "file_list_5",
		target_id : result.analReqSno + "-" + result.evdcSno,
		type : "anlsPgs",
		creator_id : session_userid
	});
	
	fn_showUserPage("요청처리가 성공적으로 수행되었습니다.", function() {
		window.location.reload();
	});	
}

function fn_removeCallback(result) {
	
	var obj = $("#file_list_3").find("li");
	var srcStr = '';
	for(var i=0;i<obj.length;i++){
		var serverPath = obj.eq(i).find('input[name=serverPath]').val();
		if(i > 0){
			srcStr += ",";
		}
		srcStr += serverPath;
	}
	agent.removeFile(srcStr);
	
	fn_showUserPage("요청처리가 성공적으로 수행되었습니다.", function() {
		window.location.reload();
	});	
}

function fn_changeAnalStat() {
	if($("#analStatPop").val() == 'C06002' || $("#analStatPop").val() == 'C06003'){
		//현재 변경한 상태가 분석중 / 분석완료 이면 보여주기
		$(".checkFileList").show();
	}else{
		//변경상태가 분석중이 아니면 숨기기
		if(analStat != 'C06002' && analStat != 'C06003'){
			//실제 가져온 상태 값이 분석중이 아니였다면 존재하던 파일목록 삭제
			$(".checkFileList").find('li').remove();
		}
		$(".checkFileList").hide();
	}
}

</script>
</head>
<body>
<div id="con_wrap_pop">
<div class="contents">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div class="window_popup">
                  <div class="sub_ttl"></div>
                         
                  <div class="sub">
                             
                             <!--------------목록---------------------->
                    <form name="popInsForm" id="popInsFormPop" method="post">
                    	<input type="hidden" name="analReqSno" id="analReqSnoPop"/>
                    	<input type="hidden" name="evdcSno" id="evdcSnoPop"/>
		            	<div class="t_list">
		                 	<table class="iptTblX">
                            	<caption>등록</caption>
				               	<colgroup>
					             	<col width="20%" />
					             	<col width="30%" />
					             	<col width="20%" />
					             	<col width="30%" />
				               	</colgroup>
				               	<tbody>
						        	<tr>
						        		<th scope="row">접수번호</th>
						        		<td><span id="rcvNoAcp"></span></td>
						        		<th scope="row">분석담당자<span class="fontred">*</span></th>
										<td>
											<input required class="inpw80" title="분석담당자" type="text" name="analCrgrNm" id="analCrgrNmPop" readonly/>
											<input type="hidden" name="analCrgrId" id="analCrgrIdPop"/>
											<button class="buttonG40" onclick="javascript:fn_openPopByPop('analCrgr');return false;">검색</button>
										</td>
						        	</tr>
						        	<tr>
										<th scope="row">분석시작일</th>
										<td><input class="inpw80" title="요청일시" type="text" name="analStartDt" id="analStartDtPop" onclick="javascript:fn_dateClear(this);" readonly/></td>
										<th scope="row">분석종료일</th>
										<td><input class="inpw80" title="요청일시" type="text" name="analEndDt" id="analEndDtPop" onclick="javascript:fn_dateClear(this);" readonly/></td>
									</tr>
									<tr>
										<th scope="row">분석도구<span class="fontred">*</span></th>
										<td>
											<select name="analTool" id="analToolPop" class="selw15" title="분석도구"></select>
										</td>
										<th scope="row">분석상태<span class="fontred">*</span></th>
										<td>
											<select onchange="javascript:fn_changeAnalStat();" name="analStat" id="analStatPop" class="selw15" title="분석상태"></select>
										</td>
									</tr>
									<tr>
									<th scope="row">분석내용<span class="fontred">*</span></th>
										<td colspan="3">
											<textarea id="analRsltCntsPop" name="analRsltCnts" rows=2 cols=30 title="분석내용"></textarea>
											<span class="txt_info" name="analRsltCntsByteChk" id="analRsltCntsByteChk"></span>
										</td>
									</tr>
									<tr>
										<th scope="row">전체이미지파일</th>
										<td colspan="3">
											<div id="rsltFileListPop" style="OVERFLOW-Y:auto; width:100%; height:50px;">
			                                    <button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list_3');" >찾아보기</button>
			                                    <span id="fileSpan"></span>
			                                    <ul id="file_list_3" class="file_list_3" style="margin-left: 18px;"></ul>
											</div>
										</td>
									</tr>
									<tr class="checkFileList">
										<th scope="row">선별이미지파일</th>
										<td colspan="3">
											<div id="rsltFileListPop" style="OVERFLOW-Y:auto; width:100%; height:50px;">
			                                    <button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list_4');" >찾아보기</button>
			                                    <span id="fileSpan"></span>
			                                    <ul id="file_list_4" class="file_list_4" style="margin-left: 18px;"></ul>
											</div>
										</td>
									</tr>
									<tr>
										<th scope="row">첨부파일</th>
										<td colspan="3">
											<div id="rsltFileListPop" style="OVERFLOW-Y:auto; width:100%; height:50px;">
			                                    <button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list_5');" >찾아보기</button>
			                                    <span id="fileSpan"></span>
			                                    <ul id="file_list_5" class="file_list_5" style="margin-left: 18px;"></ul>
											</div>
										</td>
									</tr>
				               	</tbody>
                        	</table>
                    	</div>
                    </form>
                  	<div class="btn_c">
                    	<ul>
                       		<li><a href="javascript:void(0)" class="RdButton" onclick="fn_setEvdcRslt();return false;" id="regBtn" >등록</a></li>
                       		<!-- <li><a href="javascript:void(0)" class="RdButton" onclick="fn_delEvdcRslt();return false;" id="delBtn" >삭제</a></li> -->
                       		<li><a href="javascript:void(0)" class="RdButton" onclick="fn_modEvdcRslt();return false;" id="modifyBtn">수정</a></li>
                       		<li><a href="javascript:void(0)" class="myButton" onclick="fn_popCallBack({},'divEvdcRsltPop');return false;">취소</a></li>
                    	</ul>
                  	</div>
                          
                  </div>
                 
                  
                 
            </div>
    </div>
    </div>
</div>
<div id="divCrgrFindPopByPop"></div>
</body>
</html>