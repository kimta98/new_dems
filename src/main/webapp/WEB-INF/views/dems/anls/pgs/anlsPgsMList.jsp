<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 20.
 * 2. 작성자 : Leeji
 * 3. 화면명 : 분석지원요청 진행관리 목록
 * 4. 설명 : 분석지원요청 진행관리 목록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript">
var tabId;

var plusPgsStat = ",C03009"; 

$(document).ready(function() {
	
	$("#chkAll").click(function(){
		if($("#chkAll").prop("checked")) {
			$("input[id^=chk_]").prop("checked",true);
		} else {
			$("input[id^=chk_]").prop("checked",false); 
		} 
	});

	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

    gfn_calendarConfig("searchRegFromDt", "searchRegToDt", "minDate", "");
    gfn_calendarConfig("searchRegToDt", "searchRegFromDt", "maxDate", "");

    $('#searchRegToDt').val(gfn_dashDate(gfn_getCurDate(), "-"));
    $('#searchRegFromDt').val(gfn_dashDate(gfn_addMonth(gfn_getCurDate(), -1), "-"));
    
    var codeInfo = [{cdId:'C03',cd:comPgsStat+plusPgsStat,selectId:'searchPgsStat',type:'2', callbackNm:'fn_ajaxCodeCrimeGrpList'}];
    //fn_ajaxCodeList(codeInfo);
    gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});
    
    var $li = $('div.btn_c').find('li').not(':last').not(':last').not(':last');
    $li.each(function(index,item){
    	$(item).hide();
    });
    
    //버튼 권한별 보이기
    if(session_usergb == 'C01003') { //포렌식수사관
	
    	//접수,접수취소,선별요청,선별요청취소
    	$li.filter('[class^=rcv]').show();
    	$li.filter('[class^=selReq]').show();
    	
	} else if(session_usergb == 'C01004') { //포렌식과장
    
		//승인,승인취소
		$li.filter('[class^=aprv]').show();
		
	}
    
});

function fn_ajaxCodeCrimeGrpList(){
	var codeInfo = [{cdId:'crimeGrp',selectId:'srcCrimeGrp',type:'4', callbackNm:'fn_ajaxCodeCrimeGrpListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});
}

function fn_ajaxCodeCrimeGrpListCallback() {
	$('#srcCrimeGrp').prepend("<option value='' selected>전체</option");
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_codeCallBack"});
}

/**
 * 분석지원요청 진행관리 목록 콤보박스 콜백함수
 * @param
 * @returns fn_anlsPsgSearch
 */
function fn_codeCallBack(){
	$('#searchPgsStat').prepend('<option value="'+(comPgsStat+plusPgsStat)+'" selected>전체</option>');
	
	$('#searchPgsStat').on({"change":function(){
		fn_anlsPsgSearch(1);
	}});
	
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_anlsPsgSearch"});
}
/**
 * 분석지원요청 진행관리 목록 조회
 * @param {string} page 항목에 대한 고유 식별자 
 * @returns fn_callBack
 */
function fn_anlsPsgSearch(page){
	var callUrl = "<c:url value = "/anls/pgs/queryAnlsPgsMList.do"/>";	
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_callBack',page:$("#page").val(),perPageNum:10});
}

 /**
  * 분석지원요청 진행관리 목록 조회 콜백함수
  * @param {object} data 조회한 결과데이터
  * @returns
  */
function fn_callBack(data){
	  
  	if($("#chkAll").prop("checked")) {
		$("#chkAll").prop("checked", false);
	}
	
	$("#anlsPgsList").empty();
	$("#totalcnt").text(data.totalCount);
	
	if(data.anlsPgsList.length < 1){
		$('#anlsPgsList').append('<tr><td colspan="14">조회된 결과가 없습니다.</td></tr>');
	}else{
	
		$.each(data.anlsPgsList, function(index, item){
			$('#anlsPgsList').append("<tr><td><input type='checkbox' name='chk' id='chk_"+index+"' value='Y' class='check_agree1'/></td>"+
			"<td>"+item.analReqSno+"</td>"+
			"<td>"+item.regDt+"<input type='hidden' name='analReqSno' value='"+item.analReqSno+"'/></td>"+
			"<td>"+item.deptNm+"</td>"+
			"<td><a href='#' onclick=javascript:fn_detail('"+item.analReqSno+"','"+data.page+"')><u><strong>"+item.incdtNo+"</strong></u></a></td>"+
			"<td>"+item.incdtNm+"</td>"+
			"<td>"+item.reqCfsc+"외 "+item.reqCnt+"건</td>"+
			"<td>"+item.regrNm+"</td>"+
			"<td>"+item.prosrNm+"<br>"+item.prosrAprvDt+"</td>"+
			"<td>"+item.rcvNoAcp+"<br>"+item.rcvDtAcp+"</td>"+
			"<td>"+item.rcvrIdAcpNm+"</td>"+
			"<td>"+item.aprvrNm+"<br>"+item.aprvDt+"</td>"+
			"<td>"+item.pgsStatNm+"<br>"+item.psgStatDt+"<input type='hidden' name='pgsStat' value='"+item.pgsStat+"'/></td>");
			
		 });
	}
	
	data.__callFuncName__ ="fn_anlsPsgSearch";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
}

/**
* 분석지원요청 진행관리 상세화면이동
* @param {string} analReqSno 조회할 분석요청일련번호
* @param {string} page 현재페이지번호
* @returns
*/
function fn_detail(analReqSno,page){	
	
	$('#analReqSno').val(analReqSno);
	requestUtil.setSearchForm("searchForm");
	
	var searchParam = $("#searchForm").serialize();
	var callUrl = session_usergb == 'C01003' ? "<c:url value = "/anls/pgs/indexAnlsPgsRDtl.do"/>?"+searchParam : "<c:url value = "/anls/pgs/indexAnlsPgsInfo.do"/>?"+searchParam;
	parent.$('#'+tabId+' iframe').attr('src', callUrl);	
}

/**
* 분석지원요청 진행관리 진행상태 변경요청
* @param {string} reqType 버튼별 type명
* @returns
*/
function fn_modPgsStat(reqType){
	var cnt = 0;
	var tpArray = [];
	var Objtype = {};	
	var isChk = false;
	
	$('#anlsPgsList > tr').each(function(index,item){

		var $chkbox = $(item).find('input[type=checkbox]');
		var $analReqSno = $(item).find('input[name=analReqSno]');
		var $pgsStat = $(item).find('input[name=pgsStat]');

		if($chkbox.is(':checked') == true){
			cnt++;
			switch (reqType) {
			case "aprvCncl":
				if($pgsStat.val() == 'C03006'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 과장승인만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "aprv":
			case "rcvCncl":
				if($pgsStat.val() == 'C03005'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 문서접수만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "rcv":
				if($pgsStat.val() == 'C03003'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 검사승인만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "selReq":
				if($pgsStat.val() == 'C03006'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 과장승인만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			case "selReqCncl":
				if($pgsStat.val() == 'C03007'){
					tpArray.push($analReqSno.val());
				}else{
					fn_showUserPage('진행상태가 선별요청만 가능합니다.');
					isChk = true;
					return false;
				}
				break;
			}

		}

	});
	
	if(cnt < 1){
		fn_showUserPage('선택해주세요');
		return;
	}
	
	if(isChk){
		return;
	}
	
	
	Objtype.type = reqType;
	var data = {rowDatas : tpArray, type : Objtype};
    var callUrl = "<c:url value='/anls/pgs/queryPgsStat.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_pgsStatCallBack'});
	
}

/**
* 분석지원요청 진행관리 진행상태 변경 콜백
* @param
* @returns
*/
function fn_pgsStatCallBack(){
	fn_anlsPsgSearch(1);
}

/**
* 분석지원요청 증거분석이력관리 화면 이동
* @param
* @returns
*/
function fn_indexAnlsPgsEvdcHistUDtl(){	
	var checkedCnt = $("input:checkbox[name=chk]:checked").length;
	
	if(checkedCnt == 0){
		fn_showUserPage("대상을 선택해주세요.", function() {
		});
		return false;
	}
	if(checkedCnt > 1){
		fn_showUserPage("대상을 한개만 선택해주세요.", function() {
		});
		return false;
	}
	
	var index = "";
	$("input:checkbox[name=chk]:checked").each(function(i,elements){
	    index = $(elements).index("input:checkbox[name=chk]");
	});
	
	var checkedpgsStat = $("#anlsPgsList").find("[name=pgsStat]").eq(index).val();
	if(checkedpgsStat == 'C03001' || checkedpgsStat == 'C03002'){
		fn_showUserPage("증거분석이력관리 대상이 아닙니다.", function() {
		});
		return false;
	}
	
	requestUtil.setSearchForm("searchForm");
	
	$('#analReqSno').val($("#anlsPgsList").find("[name=analReqSno]").eq(index).val());
	
	var searchParam = $("#searchForm").serialize();
	var callUrl = "<c:url value = "/anls/pgs/indexAnlsPgsEvdcHistUDtl.do"/>?"+searchParam;
	parent.$('#'+tabId+' iframe').attr('src', callUrl);	
}
	
function fn_excelDown(){
	debugger;
requestUtil.comExcelXlsx({
	formNm :'searchForm',
	filename : "분석지원관리",
	sheetNm : "분석지원관리",
	columnArr : "작성일자,부서명,사건번호,사건명,분석요청건수,요청자,주임검사(승인일자),접수번호,담당자,승인,진행상태,선별요청,선별확인",
	columnVarArr : "regDt,deptNm,incdtNo,incdtNm,reqCfsc,regrNm,prosrNm,rcvNoAcp,rcvrIdAcpNm,aprvrNm,pgsStatNm,selReqCrgrNm,selCfrmCrgrNm",
	sqlQueryId : "anlsReqDAO.selectAnlsReqExcelList",
	splitSearch : "searchPgsStat"
	});
		
	}
</script>
<body>
<div id="con_wrap">
       <div id="contents_info">
              <!--- contnets  적용 ------>
            <div>
                  <div class="loca">
                    <div class="ttl">분석 진행 관리</div>
                    <div class="loca_list"></div>
                  </div>
                 
                  <div class="sub">
                     <!--------------검색------------------>
                    <form id="searchForm" name="searchForm" onsubmit="return false;"> 
                    <input type="hidden" name="analReqSno" id="analReqSno"/>
                    <input type="hidden" id="page" name="page" value="1"/>
                    <div class="t_head">
                          <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_anlsPsgSearch(1);">
                                <caption>검색</caption>
                                 <colgroup>
                                   <col width="10%">
                                   <col width="25%">
                                   <col width="10%">
                                   <col width="25%">
                                   <col width="10%">
                                   <col width="20%">                                 
                                 </colgroup>
                           <thead>                           
                           	  <tr>
					            <th scope="col" class="hcolor">사건번호</th>
					            <td scope="col">
					               <input class="inpw80" type="text" name="searchIncdtNo" id="searchIncdtNo"/>
					            </td>
					            <th scope="col" class="hcolor">사건명</th>
					            <td scope="col">
					               <input class="inpw80" type="text" name="searchIncdtNm" id="searchIncdtNm"/>
					            </td>
					            <th scope="col" class="hcolor">작성일자</th>
					            <td scope="col">
					               <input class="inpw30" type="text" name="searchRegFromDt" id="searchRegFromDt"/>
					               <input class="inpw30" type="text" name="searchRegToDt" id="searchRegToDt"/>
					            </td>
					          </tr>
					          <tr>
					            <th scope="col" class="hcolor">담당자</th>
					            <td scope="col">
					               <input class="inpw80" type="text" name="searchRcvNm" id="searchRcvNm"/>
					            </td>
					            <th scope="col" class="hcolor">진행상태</th>
					            <td scope="col" >
					               <select name="searchPgsStat" id="searchPgsStat" class="selw6">
					               </select>
					            </td>
					            <th scope="col" class="hcolor">피의사실</th>
					            <td scope="col" >
					            	<input class="inpw80" type="text" name="analSuptFact" id="analSuptFact"/>
					            </td>
					          </tr>
					          <tr>
					          	<th scope="col" class="hcolor">요청번호</th>
					            <td scope="col" >
					          		<input class="inpw80" type="text" name="analReqSno" id="analReqSno"/>
					            </td>
					            <th scope="col" class="hcolor">접수번호</th>
					            <td scope="col" >
					            	<input class="inpw80" type="text" name="rcvNoAcp" id="rcvNoAcp"/>
					            </td>
					            <th scope="col" class="hcolor">분석요청사항</th>
					            <td scope="col" >
					            	<input class="inpw80" type="text" name="analReqInfo" id="analReqInfo"/>
					            </td>
					          </tr>
					          <tr>
					          	<th scope="col" class="hcolor">죄명</th>
					            <td scope="col" >
					            	<select id="srcCrimeGrp" name="srcCrimeGrp" title="죄명" onchange="fn_anlsPsgSearch(1);" class="selw6">
								               </select>
					            </td>
					            <th scope="col" class="hcolor"></th>
					            <td scope="col" >
					            </td>
					            <th scope="col" class="hcolor"></th>
					            <td scope="col" >
					            </td>
					          </tr>
                           </thead>
                        </table>
                        <div  class="btn_c">
				       	<ul>
                          <li class="rcv"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('rcv');return false;">접수</a></li>
                          <li class="rcvCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('rcvCncl');return false;">접수취소</a></li>
                          <li class="aprv"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('aprv');return false;">승인</a></li>
                          <li class="aprvCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('aprvCncl');return false;">승인취소</a></li>      
                          <!-- <li class="selReq"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selReq');return false;">선별요청</a></li>
                          <li class="selReqCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selReqCncl');return false;">선별요청취소</a></li>
                          <li><a href="javascript:void(0);" class='myButton' onclick="fn_indexAnlsPgsEvdcHistUDtl();return false">증거분석이력관리</a></li> -->
                          <li><a href="javascript:void(0);" class='myButton' onclick="fn_excelDown();return false">엑셀다운</a></li>
                          <li><a href="javascript:void(0);" class='gyButton' onclick="fn_anlsPsgSearch(1); return false;">조회</a></li>
                        </ul>   
					  </div>
                      
                      </div>
                     </form>
                      
                    <!--------------//검색------------------>
                    
                     <!--------------결과------------------>
                     <div class="r_num">| 결과  <strong id="totalcnt" style="color:#C00"></strong>건</div>                     
                     <!--------------목록---------------------->
                     <div class="t_list" style="OVERFLOW-Y:auto;overflow-x: hidden;width:100%; height:450px;">  
                          <table class="tbl_type" border="1" cellspacing="0" >
                                <caption>분석지원 요청 진행관리 목록</caption>
                                  <colgroup>
                                      <col width="2%">                                      
                                      <col width="7%">
                                      <col width="7%">
                                      <col width="6%">
                                      <col width="8%">
                                      <col width="10%">
                                      <col width="7%">
                                      <col width="7%">
                                      <col width="8%">
                                      <col width="8%">
                                      <col width="10%">
                                      <col width="5%">
                                      <col width="5%">
                                   </colgroup>
                                    <thead>
                                      <tr>
                                         <th scope="col"><input type='checkbox' name='chkAll' id='chkAll' title='' class='check_agree1'/></th>
                                         <th scope="col">요청번호</th>
                                         <th scope="col">작성일자</th>
                                         <th scope="col">부서명</th>
                                         <th scope="col">사건번호</th>
                                         <th scope="col">사건명</th>
                                         <th scope="col">분석요청건수</th>
                                         <th scope="col">요청자</th>
                                         <th scope="col">주임검사<br>(승인일자)</th>
                                         <th scope="col">접수번호</th>
                                         <th scope="col">담당자</th>
                                         <th scope="col">승인</th>                                         
                                         <th scope="col">진행상태</th>
                                      </tr>
                                    </thead>
                                    <tbody id="anlsPgsList">
                                    </tbody>
                             </table>
                     </div>
                      <!--------------//목록---------------------->
                     
                     <!-----------------------페이징----------------------->
                     <div id='page_navi' class="page_wrap"></div>                            
                       <!-----------------------//페이징----------------------->                  
                  </div>
                 
            </div>
    </div>
</div>
</body>
</html>