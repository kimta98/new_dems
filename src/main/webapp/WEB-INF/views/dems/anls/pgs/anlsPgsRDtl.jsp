<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 20.
 * 2. 작성자 : leeji
 * 3. 화면명 : 분석지원요청 진행관리 분석결과 등록
 * 4. 설명 : 분석지원요청 진행관리 분석결과 등록
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<script type="text/javaScript">

var analReqSno = '${param.analReqSno}';
var fileArray = [];
var ftpFileArray = [];
var fileSno = '';
var atchFileId = '';
var path = '';
var nodeObj;
var type = '';
var pgsStat = '';
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	fn_anlsReqInfoSearch();
});

/**
 * 분석지원요청 진행관리 목록페이지이동
 * @param
 * @returns
 */
function fn_moveList(){
	var src = "<c:url value = "/anls/pgs/indexAnlsPgsMList.do"/>";
	parent.$('#'+tabId+' iframe').attr('height', 730);
	parent.$('#'+tabId+' iframe').attr('src', src);
}

/**
* 분석지원요청 진행관리 상세화면 조회
* @param
* @returns anlsReqInfoCallBack
*/
function fn_anlsReqInfoSearch(){

	fn_rsltClose();

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");

	var input = document.createElement('input');
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "analReqSno");
	input.setAttribute("value", analReqSno);
	searchForm.appendChild(input);

	document.body.appendChild(searchForm);

	var callUrl = "<c:url value = "/anls/pgs/queryAnlsPgsInfo.do"/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'infoForm',callbackNm:'anlsReqInfoCallBack'});
}

/**
* 분석지원요청 진행관리 상세화면 조회 콜백함수
* @param {object} data 조회한 결과데이터
* @returns
*/
function anlsReqInfoCallBack(data){

	$('form[name=searchForm]').remove();
	$("#detailTbody").empty();

	$('#rsltAnalReqSno').val('');
	$('#rsltEvdcSno').val('');
	$('#atchFileId').val('');

	var rcvrIdAcpNm = data.anlsReqInfoMap.rcvrIdAcpNm //접수담당자
	var rcvNoAcp = data.anlsReqInfoMap.rcvNoAcp //접수번호
	var rcvDtAcp = data.anlsReqInfoMap.rcvDtAcp //접수일자
	var aprvrIdNm = data.anlsReqInfoMap.aprvrIdNm //승인자
	var prosrAprvDt = data.anlsReqInfoMap.prosrAprvDt //승인일자
	var selReqCrgrNm = data.anlsReqInfoMap.selReqCrgrNm //선별요청담당자명
	var selReqDt = data.anlsReqInfoMap.selReqDt //선별요청일자
	var selCfrmCrgrNm = data.anlsReqInfoMap.selCfrmCrgrNm //선별확인담당자명
	var selCfrmDt = data.anlsReqInfoMap.selCfrmDt //선별확인일자

	//담당자(접수번호,접수일자) 승인자(승인일자) 선별요청담당자(선별요청일자) 선별확인담당자(선별확인일자)
	var rcvrIdAcp = rcvrIdAcpNm != '' ? rcvrIdAcpNm + '(' + rcvNoAcp + ',' + rcvDtAcp + ')' : '';
	var aprvrId = aprvrIdNm != '' ? aprvrIdNm + '(' + prosrAprvDt + ')' : '';
	var selReqCrgr = selReqCrgrNm != '' ? selReqCrgrNm + '(' + selReqDt + ')' : '';
	var selCfrmCrgr = selCfrmCrgrNm != '' ? selCfrmCrgrNm + '(' + selCfrmDt + ')' : '';

	$('span#rcvrIdAcp').text(rcvrIdAcp);
	$('span#aprvrId').text(aprvrId);
	$('span#selReqCrgr').text(selReqCrgr);
	$('span#selCfrmCrgr').text(selCfrmCrgr);

	$("span#analDocFile").text(data.anlsReqInfoMap.analDocFile);
	$("span#analEmailMsgr").text(data.anlsReqInfoMap.analEmailMsgr);
	$("span#analWebHist").text(data.anlsReqInfoMap.analWebHist);
	$("span#analPhotoVideo").text(data.anlsReqInfoMap.analPhotoVideo);
	$("span#analKwrd").text(data.anlsReqInfoMap.analKwrd);


	var pgsStat = data.anlsReqInfoMap.pgsStat;

	//버튼 제어
	var $li = $('div#btn_c').find('li').not(':last');
    $li.each(function(index,item){
    	$(item).hide();
    });

  //버튼 권한별 보이기
    if(session_usergb == 'C01003') { //포렌식수사관

    	//검사승인상태면 접수
    	if(pgsStat == 'C03003'){
    		$li.filter('[class=rcv]').show();
    	}

    	//접수상태면 접수취소
    	if(pgsStat == 'C03005'){
	    	$li.filter('[class=rcvCncl]').show();
    	}

    	//선별요청취소상태면 선별요청
    	/* if(pgsStat == 'C03006'){
	    	$li.filter('[class=selReq]').show();
    	}

    	//선별요청상태면 선별요청취소
    	if(pgsStat == 'C03007'){
	    	$li.filter('[class=selReqCncl]').show();
    	} */


	}

	this.pgsStat = pgsStat;
	var analStatNm = '';

	for(var i = 0; i < data.analysisEvdcDtlList.length; i++){

		/* if((pgsStat == 'C03008' || pgsStat == 'C03007' || pgsStat == 'C03006' || pgsStat == 'C03005') && data.analysisEvdcDtlList[i].rsltCnt > 0){
			button = (data.session_usergb == 'C01003' ? '<p><button class="button100" onclick="fn_insRslt(\'mod\',this);return false;">분석결과 수정</button></p>' : '');
		}else if((pgsStat == 'C03008' || pgsStat == 'C03007' || pgsStat == 'C03006' || pgsStat == 'C03005') && data.analysisEvdcDtlList[i].rsltCnt == 0){
			button = (data.session_usergb == 'C01003' ? '<p><button class="button100" onclick="fn_insRslt(\'reg\',this);return false;">분석결과 등록</button></p>' : '');
		} */
		
		if(data.analysisEvdcDtlList[i].rsltCnt > 0){
			analStatNm = "("+data.analysisEvdcDtlList[i].analStatNm+")";
		}else{
			analStatNm = "(미진행)";
		}

		var tpTag =     '<tr>'+
							'<td rowspan="2">'+(i+1)+
							'<input type="hidden" name="analReqSno" id="analReqSno_'+i+'" value="'+data.analysisEvdcDtlList[i].analReqSno+'"/>'+
							'<input type="hidden" name="evdcSno" id="evdcSno_'+i+'" value="'+data.analysisEvdcDtlList[i].evdcSno+'"/></td>'+
							'<td><a href="javascript:fn_anlsEvdcRsltPop(\''+data.analysisEvdcDtlList[i].analReqSno+'\', \''+data.analysisEvdcDtlList[i].evdcSno+'\', \''+data.analysisEvdcDtlList[i].rcvrIdAcp+'\', \''+data.analysisEvdcDtlList[i].rcvNoAcp+'\');"><p><span id="rcvNoAcp'+i+'">'+data.analysisEvdcDtlList[i].rcvNoAcp+'</span> '+analStatNm+'</p></a></td>'+
							'<td><span id="cfscGoodsDiv_'+i+'">'+data.analysisEvdcDtlList[i].cfscGoodsDivNm+'</span></td>'+
							'<td><span id="mdlNm">'+data.analysisEvdcDtlList[i].mdlNm+'</span></td>'+
							'<td><span id="cfscDt">'+data.analysisEvdcDtlList[i].cfscDt+'</span></td>'+
							'<td><span id="psvCfscrSmitr">'+data.analysisEvdcDtlList[i].psvCfscrSmitr+'</span></td>'+
							'<td><span id="cfscCrgr">'+data.analysisEvdcDtlList[i].cfscCrgr+'</span></td>'+
							'<td><span id="trgtDmgdYn">'+(data.analysisEvdcDtlList[i].trgtDmgdYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td><span id="reqBefDrvYn">'+(data.analysisEvdcDtlList[i].reqBefDrvYn == 'Y' ? 'O' : 'X')+'</span></td>'+
						'</tr>'+
						'<tr>'+
							'<td><span id="cfscDiv_'+i+'">'+data.analysisEvdcDtlList[i].rcvrIdAcpNm+'</span></td>'+
							'<td><span id="cfscDiv_'+i+'">'+data.analysisEvdcDtlList[i].cfscDivNm+'</span></td>'+
							'<td><span id="serlNo">'+data.analysisEvdcDtlList[i].serlNo+'</span></td>'+
							'<td><span id="cfscPlace">'+data.analysisEvdcDtlList[i].cfscPlace+'</span></td>'+
							'<td><span id="realUser">'+data.analysisEvdcDtlList[i].realUser+'</span></td>'+
							'<td><span id="sealYn">'+(data.analysisEvdcDtlList[i].sealYn == 'Y' ? 'O' : 'X')+'</span></td>'+
							'<td><span id="dmgdDtl">'+data.analysisEvdcDtlList[i].dmgdDtl+'</span></td>'+
							'<td><span id="drvDtl">'+data.analysisEvdcDtlList[i].drvDtl+'</span></td>'+
						'</tr>';
		$("#detailTbody").append(tpTag);
	}

	//일반,선별파일리스트
	/*for(var i = 0; i < data.fileList.length; i++){
		var path = (data.fileList[i].filePth+'\\'+data.fileList[i].fileNm).replace(/\\/gi, "//");
		var spanTag = '<p><span>'+data.fileList[i].fileNm+'</span><button class=button60 onclick=javascript:fn_viewFile(\''+encodeURIComponent(path)+'\')>다운로드</button>'+
		              '<input type="hidden" name="path" value="'+path+'"></p>';
		$("div#"+data.fileList[i].atchFileDiv).append(spanTag);
	}*/

	$('#file_list_1').html("");
	$('#file_list_2').html("");

    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileList.length; i++){
		var id = data.atchFileList[i].id;
		var name = data.atchFileList[i].name;
		var path = data.atchFileList[i].path;
		var upload_yn = data.atchFileList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list_1').append(html);
	}

    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileImgList.length; i++){
		var id = data.atchFileImgList[i].id;
		var name = data.atchFileImgList[i].name;
		var path = data.atchFileImgList[i].path;
		var upload_yn = data.atchFileImgList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list_2').append(html);
	}

	//분석기관 넣기
	//$('#analInsttNm').text(data.session_deptnm);

	//gfn_overMaxLength("analRsltCnts",1300);
	
	//select박스 가져오기
	//var codeInfo = [{cdId:'C06',selectId:'analStat' ,type:'1', callbackNm:'fn_codeCallBack'},{cdId:'C13',selectId:'analTool' ,type:'1'}];
	//fn_ajaxCodeList(codeInfo);

}

/**
 * 분석 결과 팝업
 */
function fn_anlsEvdcRsltPop(analReqSno, evdcSno, rcvrIdAcp, rcvNoAcp){
	if(rcvrIdAcp != "" && rcvrIdAcp != session_userid){
		// 담당자가 다를 경우
		fn_showUserPage('분석 대상 담당자가 아닙니다.');
	}else{
		// 담당자이거나 담당자가 현재 없을 경우
		var callUrl = "<c:url value = "/com/PageLink.do"/>?analReqSno="+analReqSno+"&evdcSno="+evdcSno+"&rcvrIdAcp="+rcvrIdAcp+"&rcvNoAcp="+rcvNoAcp

		requestUtil.mdPop({
			popUrl : callUrl+"&link=dems/anls/pgs/evdcRsltPop",
			height: 700,
	        width: 1000,
	        title: '분석결과',
	        divId : 'divEvdcRsltPop'
		});
	}
}

/**
 * 분석지원요청 진행관리 콤보박스 콜백함수
 * @param
 * @returns
 */
/* function fn_codeCallBack(){
	$('#analStat').prepend('<option value="" selected>선택</option>');
	$('#analTool').prepend('<option value="" selected>선택</option>');
} */

/**
* 분석지원요청 진행관리 결과등록화면 상세
* @param {string} type 등록/수정의 타입
* @param {object} obj 선택한 현재 노드
* @returns
*/
/* function fn_insRslt(type,obj){

	var rcvNoAcp = $(obj).parents('tr:eq(0)').find('span[id^=rcvNoAcp]').text();
	var mdlNm = $(obj).parents('tr:eq(0)').find('span[id^=mdlNm]').text();
	var analReqSno = $(obj).parents('tr:eq(0)').find('input[id^=analReqSno]').val();
	var evdcSno = $(obj).parents('tr:eq(0)').find('input[id^=evdcSno]').val();

	$('#rsltInfo').find('span#rcvNoAcp').text(rcvNoAcp);
	$('#rsltInfo').find('span#mdlNm').text(mdlNm);
	$('#rsltInfo').find('input#rsltAnalReqSno').val(analReqSno);
	$('#rsltInfo').find('input#rsltEvdcSno').val(evdcSno);

	if(type == 'reg'){

		$('#rsltInfo').show();
		$('#btnSave').show();
		$('#btnMod').hide();
		$('#btnDel').hide();

		$('#rsltInfo').find('tbody').each(function(index,item){

			var $el = $(item).find('input, select, textarea');

			$el.each(function(index,item){
				$('#'+item.id).val('');
			});
		});


		this.fileArray = [];
		this.ftpFileArray = [];
		this.type = '';

		$('#analStartDt').datepicker("destroy");
		$('#analEndDt').datepicker("destroy");
		gfn_calendarConfig("analStartDt", "analEndDt", "minDate", "");
		gfn_calendarConfig("analEndDt", "analStartDt", "maxDate", "");

	}else{

		$('#rsltInfo').show();
		$('#btnSave').hide();
		$('#btnMod').show();
		$('#btnDel').show();

		$('#rsltInfo').find('tbody').each(function(index,item){

			var $el = $(item).find('input, select, textarea');

			$el.each(function(index,item){
				$('#'+item.id).val('');
			});
		});


		$('#analStartDt').datepicker("destroy");
		$('#analEndDt').datepicker("destroy");
		gfn_calendarConfig("analStartDt", "analEndDt", "minDate", "");
		gfn_calendarConfig("analEndDt", "analStartDt", "maxDate", "");

		this.fileArray = [];
		this.ftpFileArray = [];
		this.type = '';

		//분석결과 조회
		fn_anlsRsltSearch(analReqSno,evdcSno);

	}

	fn_setFrameSize();

} */

/**
* 분석지원요청 진행관리 결과등록
* @param
* @returns fn_saveCallBack
*/
function fn_save(){

	//validate
	if(!fn_formValidate()){
		return;
	}

	var $formDatas = $('#insForm');
	var tpArray =[];

	var formObj = new Object();

	$formDatas.each(function(index,item){

		var $el = $(item).find('input, select, textarea').not('input[type=file]');

		$el.each(function(index,item){
			formObj[item.name] = item.value;
		});

	});

	var data = {fileDatas : tpArray, formDatas : formObj};
	var callUrl = "<c:url value='/anls/pgs/reqAnlsPgsRDtl.do'/>";
	var flag = (ftpFileArray.length > 0 ? 'Y' : 'N');
	requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_saveCallBack',skip:flag});
}

/**
* 분석지원요청 진행관리 결과등록 콜백함수
* @param
* @returns fn_saveCallBack
*/
/* function fn_saveCallBack(result){

	agent.uploadFile({
		obj_id : "file_list_3",
		target_id : result.analReqSno,
		type : "anlsPgs",
		creator_id : session_userid
	});

	fn_anlsReqInfoSearch();
} */

/**
* 분석지원요청 진행관리 분석결과 조회
* @param {string} analReqSno 조회할 분석요청일련번호
* @param {string} evdcSno 조회할 증거일련번호
* @returns fn_anlsRsltSearchCallBack
*/
/* function fn_anlsRsltSearch(analReqSno,evdcSno){

	var searchForm = document.createElement('form');
	searchForm.setAttribute("name","searchForm");
	searchForm.setAttribute("id","searchForm");

	var input = document.createElement('input');
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "analReqSno");
	input.setAttribute("value", analReqSno);
	searchForm.appendChild(input);

	var input = document.createElement('input');
	input.setAttribute("type", "hidden");
	input.setAttribute("name", "evdcSno");
	input.setAttribute("value", evdcSno);
	searchForm.appendChild(input);

	document.body.appendChild(searchForm);

	var callUrl = "<c:url value = "/anls/pgs/goAnlsPsgUDtl.do"/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',setFormNm:'insForm',callbackNm:'fn_anlsRsltSearchCallBack'});

} */

/**
* 분석지원요청 진행관리 분석결과 조회 콜백함수
* @param {object} data 조회한 결과데이터
* @returns
*/
function fn_anlsRsltSearchCallBack(data){
	$('form[name=searchForm]').remove();



	/* $('#analStartDt').datepicker('option','minDate', new Date(gfn_dashDate(data.infoMap.analStartDt,"/")));
	$('#analStartDt').datepicker('setDate', new Date(gfn_dashDate(data.infoMap.analStartDt,"/")));
	$('#analEndDt').datepicker('option','maxDate', new Date(gfn_dashDate(data.infoMap.analEndDt,"/")));
	$('#analEndDt').datepicker('setDate', new Date(gfn_dashDate(data.infoMap.analEndDt,"/"))); */

	//분석결과 첨부파일리스트
	/*for(var i = 0; i < data.fileList.length; i++){
		var path = (data.fileList[i].filePth+'\\'+data.fileList[i].fileNm).replace(/\\/gi, "//");
		var atchFileId = data.fileList[i].atchFileId;
		$('#atchFileId').val(atchFileId);
		var fileSno = data.fileList[i].fileSno;
		var spanTag = '<p><span>'+data.fileList[i].fileNm+'</span><button class=button35 style=width:45px; onclick=javascript:fn_viewFile(\''+encodeURIComponent(path)+'\')>다운로드</button>'+
					  '<button class=button35 onclick=javascript:fn_ftpDelFile(\''+encodeURIComponent(path)+'\',\''+atchFileId+'\',\''+fileSno+'\',this)>삭제</button>'+
		              '<input type="hidden" name="path" value="'+path+'"></p>';
		$("div#rsltFileList").append(spanTag);
	}*/
	$('#file_list_3').html("");
    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileList.length; i++){
		var id = data.atchFileList[i].id;
		var name = data.atchFileList[i].name;
		var path = data.atchFileList[i].path;
		var upload_yn = data.atchFileList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list_3').append(html);
	}

}

/**
* 분석지원요청 진행관리 분석결과 수정
* @param
* @returns fn_modCallBack
*/
function fn_mod(){

	//validate
	if(!fn_formValidate()){
		return;
	}

	var $formDatas = $('#insForm');
	var tpArray =[];

	var formObj = new Object();

	$formDatas.each(function(index,item){

		var $el = $(item).find('input, select, textarea').not('input[type=file]');

		$el.each(function(index,item){
			formObj[item.name] = item.value;
		});

	});

	var data = {fileDatas : tpArray, formDatas : formObj};
	var callUrl = "<c:url value='/anls/pgs/updAnlsPgsUDtl.do'/>";
	var flag = (ftpFileArray.length > 0 ? 'Y' : 'N');
	requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modCallBack',skip:flag});;
}

/**
* 분석지원요청 진행관리 분석결과 수정 콜백함수
* @param
* @returns
*/
function fn_modCallBack(result){

	agent.uploadFile({
		obj_id : "file_list_3",
		target_id : result.analReqSno,
		type : "anlsPgs",
		creator_id : session_userid
	});

	fn_anlsReqInfoSearch();

}

/**
* 분석지원요청 진행관리 분석결과 삭제
* @param
* @returns fn_anlsReqInfoSearch
*/
function fn_del(){

	$('#file_list_3').html("");
	//fn_formValidate
	var $formDatas = $('#insForm');
	var formObj = new Object();

	$formDatas.each(function(index,item){

		var $el = $(item).find('input, select, textarea').not('input[type=file]');

		$el.each(function(index,item){
			formObj[item.name] = item.value;
		});

	});

	var data = {formDatas : formObj};
	var callUrl = "<c:url value='/anls/pgs/delAnlsPgsUDtl.do'/>";
	requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_anlsReqInfoSearch'});
}

/**
* 분석지원요청 진행관리 분석결과 화면닫기
* @param
* @returns
*/
function fn_rsltClose(){
	$('#rsltInfo').hide();
	fn_setFrameSize($('#contents_info').height());
}

/**
* 분석지원요청 진행관리 팝업
* @param {string} 팝업종류별 type
* @returns
*/
function fn_openPop(type){
	var callUrl = "<c:url value = "/com/PageLink.do"/>?type="+type

	requestUtil.mdPop({
		popUrl : callUrl+"&link=dems/anls/req/crgrFindPop",
		height: 700,
        width: 1000,
        title: '담당자조회',
        divId : 'divCrgrFindPop'
	});
}

/**
* 분석지원요청 진행관리 팝업 콜백함수
* @param {object} data 조회한 결과데이터
* @param {string} divId 팝업div아이디
* @returns
*/
function fn_popCallBack(data, divId){

	$("#"+divId).dialog( "close" );
	$("#"+divId).empty();

	$.each(data, function(index, value){
		if($('#'+index).is('span')){
			$('#'+index).text(value);
		}else{
			$('#'+index).val(value);
		}
	});
}

/**
* 분석지원요청 진행관리 첨부파일 다운로드
* @param {path} path 다운로드할 첨부파일 경로
* @returns
*/
function fn_viewFile(path){
	requestUtil.ftpFileAjax({data:{"type": "ftp_downloadFile","path": decodeURIComponent(path)},skip:'Y'});
}

/**
* 분석지원요청 진행관리 첨부파일(첨부한파일경로와 파일명을 넣어준다)
* @param {obj} obj 선택한 노드
* @returns
*/
function fn_fileChk(obj){
	var fileObj = {};
	//경로+파일명
	var fileInfo = obj.value;
	//var fileNm = fileInfo.substring(fileInfo.lastIndexOf('\\')+1, fileInfo.lastIndexOf('.'));
	var fileNm = fileInfo.substring(fileInfo.lastIndexOf('\\')+1);
	var filePath = fileInfo.substring(0, fileInfo.lastIndexOf('\\'));

	//같은경로+파일명 체크
	if(ftpFileArray.length > 0){
		var flag = false;
		ftpFileArray.forEach(function(file){
			if(file == fileInfo) {
				fn_showUserPage('이미 첨부된 파일입니다');
				fn_fileValueClear(obj);
				flag = true;
				return false;
			}
		});
		if(flag) return;

	}

	fileObj.fileNm = fileNm;
	fileObj.filePath = filePath;
	fileObj.fileInfo = fileInfo;

	fileArray.push(fileObj);
	ftpFileArray.push(fileInfo);

	//화면에 표시해주기
	$(obj).parent().append('<p><span>'+fileNm+'</span><button class="button35" onclick="javascript:fn_delFile(this);return false;">삭제</button></p>');

	//file 초기화
	fn_fileValueClear(obj);

}

/**
* 분석지원요청 진행관리 첨부파일(input file을 초기화한다)
* @param {obj} obj 선택한 노드
* @returns
*/
function fn_fileValueClear(obj){

	var agent = navigator.userAgent.toLowerCase();

	if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
		$(obj).replaceWith( $(obj).clone(true) );
	} else {
		$(obj).val("");
	}
}

/**
* 분석지원요청 진행관리 첨부파일 ftp파일업로드
* @param {string} type 서버에 저장할때 등록/수정을 구별하기 위해
* @returns ftp_uploadFileCallback
*/
function ftp_uploadFile(type){

	this.type = type;

	var path = {"path" : "/" + "req" + "/" + analReqSno};
 	//var ftpFileArray = ["C:/Java/jdk1.6.0_45/THIRDPARTYLICENSEREADME.txt","D:\패키지 스크립트.txt","D:\펑션 스크립트.txt","공공급식식재표표준체계구축_WBS_1.2_작업중.xlsx"];
	//var path = {"path": "/2/1"};

	var data = {"type": "ftp_uploadFile",
			    "hash": "sha256",
			    "src" : ftpFileArray,
			    "dst" : path};

	requestUtil.ftpFileAjax({data:data,callbackNm:'ftp_uploadFileCallback',skip:'Y'});
}

/**
* 분석지원요청 진행관리 첨부파일 ftp파일업로드 콜백함수
* @param {object} data ftp업로드한후 받은 데이터 path,hash
* @returns fn_anlsReqInfoSearch
*/
function ftp_uploadFileCallback(data){

	//[{"path":"\\case1\\ev1\\THIRDPARTYLICENSEREADME.txt","hash":"0b9d59a6f41990a62c03f26beb0cc5df992d08d50155a3c6690465dfc4b0b4d2"}]
	var insFileArray = [];

	//ftp에서 받아온 파일리스트와 화면에서 가지고있는 리스트 비교해서 합친다...(type을넣기위해)
	data.result.forEach(function(v, i) {
		fileArray.forEach(function(v1, i1) {
			console.log("data.reuslt path : " + v.path + " hash : " + v.hash);
			var tpObj = {};
			if(v.path.indexOf(v1.fileNm) > 0){
				console.log("fileArray fileNm : " + v1.fileNm + " fileInfo : " + v1.fileInfo+ " type : " + v1.type);
				tpObj.fileHashVal = v.hash;
				//tpObj.fileNm = v.path.substring(v.path.lastIndexOf('\\')+1, v.path.lastIndexOf('.'));
				tpObj.fileNm = v.path.substring(v.path.lastIndexOf('\\')+1);
				tpObj.oriFileNm = v.path.substring(v.path.lastIndexOf('\\')+1);
				tpObj.filePth = v.path.substring(0, v.path.lastIndexOf('\\'));
				insFileArray.push(tpObj);
				console.log("tpObj.fileNm : " + tpObj.fileNm +"tpObj.filePath : " + tpObj.filePth +"tpObj.atchFileDiv : " + tpObj.atchFileDiv);
			}
		});
	});


	//분석요청일련번호
	var formObj = new Object();
	formObj["analReqSno"] = $('#rsltAnalReqSno').val();
	formObj["evdcSno"] = $('#rsltEvdcSno').val();
	formObj["atchFileId"] = $('#atchFileId').val();
	formObj["type"] = type;

	//삭제할 파일리스트 없어도 빈것으로 보내야함
	var delFileArray = [];

	//파일정보 update
	var data = {insFileArray : insFileArray, delFileArray : delFileArray, anlsReqInfo: formObj};
    var callUrl = "<c:url value='/anls/pgs/queryAnlsPgsFile.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_anlsReqInfoSearch'});

}

/**
* 분석지원요청 진행관리 첨부파일 삭제
* @param {object} obj 현재노드
* @returns
*/
function fn_delFile(obj){

	var delFile = $(obj).prev().text();

	//fileArray ftpFileArray에서 찾아서 지우기
	var idx = ftpFileArray.indexOf(delFile);
	ftpFileArray.splice(idx,1);

	var findDelFile = fileArray.find(function(item){
		return item.fileInfo === delFile;
	});
	idx = fileArray.indexOf(findDelFile);
	fileArray.splice(idx,1);
/*
 	ftpFileArray.forEach(function(value,index,file){
		console.log("ftpFileArray file : " + file);
	});

	fileArray.forEach(function(value,index,file){
		$.each(file,function(type,value){
			console.log("type : " + type +   "  fileInfo : " + value.fileInfo+   "  filePath : " + value.filePath+   "  fileNm : " + value.fileNm+   "  type : " + value.type);
		});
	});
	 */
	$(obj).parent('p').remove();

}

/**
* 분석지원요청 진행관리 첨부파일 ftp파일삭제
* @param {string} path 파일경로
* @param {string} atchFileId 첨부파일id
* @param {string} fileSno 파일일련번호
* @param {object} obj 현재노드
* @returns fn_ftpDelFileCallBack
*/
function fn_ftpDelFile(path,atchFileId,fileSno,obj){
	//전역변수에 저장
	this.path = decodeURIComponent(path);
	this.atchFileId = atchFileId;
	this.fileSno = fileSno;
	this.nodeObj = obj;

	//ftp파일을 지우고 테이블을 삭제함
	requestUtil.ftpFileAjax({data:{"type": "ftp_removeFile","src": [decodeURIComponent(path)]},callbackNm:'fn_ftpDelFileCallBack',skip:'Y'});
}

/**
* 분석지원요청 진행관리 첨부파일 ftp파일삭제(한건) 콜백함수
* @param
* @returns fn_delClear
*/
function fn_ftpDelFileCallBack(){

	var formObj = new Object();
	formObj["analReqSno"] = analReqSno;
	formObj["atchFileId"] = atchFileId;
	formObj["fileSno"] = fileSno;
	formObj["evdcSno"] = $('#rsltEvdcSno').val();

	//등록할것이 없어도 빈것으로 보내야함
	var insFileArray = [];
	var delFileArray = [];

	delFileArray.push(path);

	//파일정보 update
	var data = {insFileArray : insFileArray, delFileArray : delFileArray, anlsReqInfo: formObj};
	var callUrl = "<c:url value='/anls/pgs/queryAnlsPgsFile.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,skip:'Y',callbackNm:'fn_delClear'});

}

/**
* 분석지원요청 진행관리 첨부파일 ftp파일삭제 콜백함수의 콜백(파일 초기화)
* @param
* @returns
*/
function fn_delClear(){
	this.fileSno = '';
	this.atchFileId = '';
	this.path = '';
	fn_delFile(nodeObj);
}

/**
* 분석지원요청 진행관리 첨부파일 ftp파일삭제(전부)
* @param
* @returns fn_del
*/
function fn_ftpAllDelFile(){
	fn_del();
}

/**
* 분석지원요청 진행상태 변경요청
* @param {string} reqType 버튼별 type
* @returns
*/
function fn_modPgsStat(reqType){

	var tpArray = [];
	var Objtype = {};

	switch (reqType) {
	case "aprvCncl":
		if(pgsStat == 'C03006'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 과장승인만 가능합니다.');
			return false;
		}
		break;
	case "aprv":
	case "rcvCncl":
		if(pgsStat == 'C03005'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 문서접수만 가능합니다.');
			return false;
		}
		break;
	case "rcv":
		if(pgsStat == 'C03003'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 검사승인만 가능합니다.');
			return false;
		}
		break;
	case "selReq":
		if(pgsStat == 'C03006'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 과장승인만 가능합니다.');
			return false;
		}
		break;
	case "selReqCncl":
		if(pgsStat == 'C03007'){
			tpArray.push(analReqSno);
		}else{
			fn_showUserPage('진행상태가 선별요청만 가능합니다.');
			return false;
		}
		break;
	}

	Objtype.type = reqType;
	var data = {rowDatas : tpArray, type : Objtype};
    var callUrl = "<c:url value='/anls/pgs/queryPgsStat.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_moveList'});

}

/**
 * 분석지원요청 진행관리 폼유효성체크
 * @param
 * @returns
 */
function fn_formValidate(){

	var $formDatas = $('#insForm').find('table.iptTblX');
	var flag = false;
	//*표시 null체크

	$formDatas.each(function(index,item){

		$(item).find('th > span.fontred').each(function(index,item){

				var $el = $(this).parent('th').next().find('input[type=text], select, textarea, div');

				$el.each(function(index,item){
					if($(this).is('div')){
						if($(this).find('span').length < 1){
							fn_showUserPage('파일을 첨부해주세요');
							flag = true;
							return false;
						}
					} else {
						//널체크
						if($(this).val() == '' || $(this).val() == null){
							var title = $(this).attr('title');
							fn_showUserPage(title + '을 입력해주세요');
							$(this).focus();
							flag = true;
							return false;
						}
					}
				});

				if(flag)return false;

		});
		if(flag)return false;

	});

	if(flag)return false;

	return true;

}
</script>
</head>
<body>
<div id="con_wrap1">
	<div class="content">
		<div id="contents_info">
			<div class="sub_ttl">분석 요청 결과 상세</div><!-----타이틀------>
				<div class="sub">
				<form name="infoForm" id="infoForm" method="POST" onsubmit="return false;">
					<div class="t_list">
						<table class="iptTblX">
							<caption>분석결과 상세화면</caption>
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">요청번호</th>
									<td><span id="analReqSno"></span></td>
									<th scope="row">검사(승인일자)</th>
									<td><span id="prosr"></span></td>
								</tr>
								<tr>
									<th scope="row">요청일시</th>
									<td><span id="reqDt"></span></td>
									<th scope="row">사건번호(사건명)</th>
									<td><span id="incdtNo"></span><p>(<span id="incdtNm"></span>)</p></td>
								</tr>
								<tr>
									<th scope="row">요청기관</th>
									<td>
										<span id="reqInsttNm"></span>
									</td>
									<th scope="row">요청부서</th>
									<td>
										<span id="reqDepNm"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">주임군검사</th>
									<td><span id="prosrNm"></span>
									</td>
									<th scope="row">담당자(연락처,HP)</th>
									<td><span id="reqUserNm"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					<div class="sub_ttl">분석 대상 상세정보</div>
					<div class="t_list" style="OVERFLOW-Y:auto; width:100%; height:350px;">
						<table class="iptTblX2">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="5%" />
								<col width="10%" />
								<col width="5%" />
								<col width="10%" />
								<col width="10%" />
								<col width="15%" />
								<col width="15%" />
								<col width="15%" />
								<col width="15%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col" rowspan="2">번호</th>
									<th scope="col">접수번호 (진행상태)</th>
									<th scope="col" rowspan="2">구분</th>
									<th scope="col">모델명</th>
									<th scope="col">압수일시<br>(제출일시)</th>
									<th scope="col">피압수자<br>(제출자)</th>
									<th scope="col">압수자</th>
									<th scope="col">대상물<br>훼손 유무</th>
									<th scope="col">요청 전<br>구동유무</th>
								</tr>
								<tr>
									<th scope="col">담당자</th>
									<th scope="col">시리얼번호</th>
									<th scope="col">압수장소<br>(제출장소)</th>
									<th scope="col">실사용자</th>
									<th scope="col">압수 시<br>봉인유부</th>
									<th scope="col">훼손<br>상세내역</th>
									<th scope="col">구동<br>상세내역</th>
								</tr>
							</thead>
							<tbody id="detailTbody">
							</tbody>
						</table>
					</div>
					<!-- <div id="rsltInfo" style="display:none">
					<div class="sub_ttl">분석결과</div>
					<form name="insForm" id="insForm" method="POST" onsubmit="return false;">
					<input type="hidden" name="analReqSno" id="rsltAnalReqSno"/>
					<input type="hidden" name="evdcSno" id="rsltEvdcSno"/>
					<input type="hidden" name="atchFileId" id="atchFileId">
					<div class="btn_c">
						<ul>
							<li id="btnSave">
								<a href="javascript:void(0);" class='RdButton' onclick="fn_save();return false;">등록</a>
							</li>
							<li id="btnMod">
								<a href="javascript:void(0);" class='RdButton' onclick="fn_mod();return false;">저장</a>
							</li>
							<li id="btnDel">
								<a href="javascript:void(0);" class='RdButton' onclick="fn_ftpAllDelFile();return false;">삭제</a>
							</li>
							<li>
								<a href="javascript:void(0);" class='myButton' onclick="fn_rsltClose();return false;">닫기</a>
							</li>
						</ul>
					</div>
					<div class="t_list">
						<table class="iptTblX">
							<caption>분석결과 등록</caption>
							<colgroup>
								<col width="10%" />
								<col width="40%" />
								<col width="10%" />
								<col width="40%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">분석기관</th>
									<td><span id="analInsttNm"></span></td>
									<th scope="row">접수번호</th>
									<td><span id="rcvNoAcp"></span></td>
								</tr>
								<tr>
									<th scope="row">구분</th>
									<td><span id="mdlNm"></span></td>
									<th scope="row">분석담당자<span class="fontred">*</span></th>
									<td>
										<input class="inpw30" title="분석담당자" type="text" name="analCrgrNm" id="analCrgrNm" readonly/>
										<input type="hidden" name="analCrgrId" id="analCrgrId"/>
										<button class="buttonG40" onclick="javascript:fn_openPop('analCrgr');return false;">검색</button>
									</td>
								</tr>
								<tr>
									<th scope="row">분석시작일<span class="fontred">*</span></th>
									<td><input class="inpw20" title="요청일시" type="text" name="analStartDt" id="analStartDt"/></td>
									<th scope="row">분석종료일<span class="fontred">*</span></th>
									<td><input class="inpw20" title="요청일시" type="text" name="analEndDt" id="analEndDt"/></td>
								</tr>
								<tr>
									<th scope="row">분석도구<span class="fontred">*</span></th>
									<td>
										<select name="analTool" id="analTool" class="selw15" title="분석도구"></select>
									</td>
									<th scope="row">분석상태<span class="fontred">*</span></th>
									<td>
										<select name="analStat" id="analStat" class="selw15" title="분석상태"></select>
									</td>
								</tr>
								<tr>
									<th scope="row">분석내용<span class="fontred">*</span></th>
									<td colspan="3">
										<textarea id="analRsltCnts" name="analRsltCnts" rows=3 cols=30 title="분석내용"></textarea>
										<span class="txt_info" name="analRsltCntsByteChk" id="analRsltCntsByteChk"></span>
									</td>
								</tr>
								<tr>
									<th scope="row">첨부파일</th>
									<td colspan="3">
										<div id="rsltFileList" style="OVERFLOW-Y:auto; width:100%; height:100px;">
		                                    <button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list_3');" >찾아보기</button>
		                                    <ul id="file_list_3" class="file_list_3" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					</form>
					</div> -->
					<div class="t_list">
						<table class="iptTblX">
							<caption>분석대상 상세정보 조회</caption>
							<colgroup>
								<col width="15%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="10%" />
								<col width="45%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">구분</th>
									<th scope="row">문서파일<br>분석</th>
									<th scope="row">메일/메신저<br>분석</th>
									<th scope="row">웹히스토리<br>분석</th>
									<th scope="row">사진/동영상<br>분석</th>
									<th scope="row">키워드</th>
								</tr>
								<tr>
									<td><span>내용</span></td>
									<td><span id="analDocFile"></span></td>
									<td><span id="analEmailMsgr"></span></td>
									<td><span id="analWebHist"></span></td>
									<td><span id="analPhotoVideo"></span></td>
									<td><span id="analKwrd"></span></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="t_list">
						<table class="iptTblX">
							<caption>파일첨부</caption>
							<colgroup>
								<col width="15%" />
								<col width="85%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">첨부파일</th>
									<td>
										<div id="gen" style="OVERFLOW-Y:auto; width:100%; height:50px;">
											<ul id="file_list_1" class="file_list_1" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row">이미지파일(선별파일)</th>
									<td>
										<div id="img" style="OVERFLOW-Y:auto; width:100%; height:50px;">
											<ul id="file_list_2" class="file_list_2" style="margin-left: 18px;"></ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<!-- <div class="t_list">
						<table class="iptTblX">
							<caption>상세화면</caption>
							<colgroup>
								<col width="15%" />
								<col width="35%" />
								<col width="15%" />
								<col width="35%" />
							</colgroup>
							<tbody>
								<tr>
									<th scope="row">담당자(접수번호,접수일자)</th>
									<td><span id="rcvrIdAcp"></span></td>
									<th scope="row">승인자(승인일자)</th>
									<td><span id="aprvrId"></span></td>
								</tr>
								<tr>
									<th scope="row">선별요청</th>
									<td><span id="selReqCrgr"></span></td>
									<th scope="row">선별확인</th>
									<td><span id="selCfrmCrgr"></span></td>
								</tr>
							</tbody>
						</table>
					</div> -->

					<div class="btn_c" id="btn_c">
						<ul>
							<li class="rcv"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('rcv');return false;">접수</a></li>
                          	<li class="rcvCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('rcvCncl');return false;">접수취소</a></li>
                          	<!-- <li class="selReq"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selReq');return false;">선별요청</a></li>
                          	<li class="selReqCncl"><a href="javascript:void(0);" class='myButton' onclick="fn_modPgsStat('selReqCncl');return false;">선별요청취소</a></li> -->
							<li><a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a></li>
						</ul>
					</div>
			</div>
		</div>
	</div>
</div>
<div id="divCrgrFindPop"></div>
<div id="divEvdcRsltPop"></div>
</body>
</html>