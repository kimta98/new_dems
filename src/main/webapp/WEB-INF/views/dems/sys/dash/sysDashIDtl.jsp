<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jmkim
 * 3. 화면명 : 메인 대쉬 보드
 * 4. 설명 : 메인 대쉬 보드
 * </pre>
 */
--%>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>

<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp"%>




<script src="https://d3js.org/d3.v5.min.js" charset="utf-8"></script>

<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/jqplot/jquery.jqplot.js'/>" "></script>
<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/jqplot/jqplot.canvasTextRenderer.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/jqplot/jqplot.canvasAxisLabelRenderer.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/jqplot/jqplot.barRenderer.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/jqplot/jqplot.pieRenderer.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/jqplot/jqplot.categoryAxisRenderer.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/jqplot/jqplot.pointLabels.js'/>"></script>


<link rel="stylesheet" type="text/css" href="<c:url value='/js/frame/thirdparty/jqplot/jquery.jqplot.css'/>" />

<!-- 
<link rel="stylesheet" type="text/css" href="<c:url value='/js/frame/thirdparty/c3/c3.css'/>">
<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/c3/c3.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/chart/chart.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/frame/thirdparty/chart/utils.js'/>"></script>
 -->


    <script>

   // alert();
    

    </script>

<script type="text/javaScript" language="javascript" defer="defer">

var dashMap = new Array();
var dashBoardArr = new Array();
var C19001 = "";
var C19002 = "";
var C19003 = "";
C19001 += '<div class="mbox1" id="C19001">';
C19001 += '<div class="msb11">';
C19001 += '<div class="mbox_ttl1">월별 분석요청 등록 현황</div>';
C19001 += '<div class="mb1_con">';
C19001 += '<div id="myChart"   style="position: relative; height:140px; width:58vw"></div>';
C19001 += '</div>';
C19001 += '</div>';
C19001 += '<div class="msb12">';
if('<%=request.getParameter("session_usergb") %>' == 'C01001' || '<%=request.getParameter("session_usergb") %>' == 'C01002'){
	C19001 += '<div class="mbox_ttl1">신규분석접수현황</div>';
}else{
	C19001 += '<div class="mbox_ttl1">신규분석요청현황</div>';
}
C19001 += '<div class="mb1_con" id="newReq" >';
C19001 += '<ul>';
C19001 += '<li><a href="">';
C19001 += '<i>모바일</i>';
C19001 += '<span class="wd35"><strong>반부패수사1과</strong></span>';
C19001 += '<span class="wd55">군사기밀보호법위반</span></a>';
C19001 += '</li>';
C19001 += '</ul>';
C19001 += '</div>';
C19001 += '</div>';
C19001 += '</div>';

C19002 += '<div class="mbox2" id="C19002">';
C19002 += '<div class="mbox_ttl2">분석관별 분석완료(결과) 현황</div>';
C19002 += '<div class="mb2_con" style="float:left">';
C19002 += '<div id="myChart2"   style="position: relative; height:180px; width:100%"></div>';
C19002 += '</div>';
C19002 += '</div>';

C19003 += '<div class="mbox3" id="C19003">';
C19003 += '<div class="msb31">';
C19003 += '<div class="mbox_ttl3">분석물별 데이터 현황</div>';
C19003 += '<div class="mb3_con" style="float:left">';
C19003 += '<div id="myChart3"   style="position: relative; height:160px; width:100%"></div>';
C19003 += '</div>';
C19003 += '</div>';
C19003 += '<div class="msb31">';
C19003 += '<div class="mbox_ttl3">의뢰부서별 분석건수 현황</div>';
C19003 += '<div class="mb3_con" style="float:left">';
C19003 += '<div id="myChart4"   style="position: relative; height:160px; width:100%"></div>';
C19003 += '</div>';
C19003 += '</div>';
C19003 += '<div class="msb32">';
if('<%=request.getParameter("session_usergb") %>' == 'C01001' || '<%=request.getParameter("session_usergb") %>' == 'C01002'){
	C19003 += '<div class="mbox_ttl3">죄명별 분석접수 현황</div>';
}else{
	C19003 += '<div class="mbox_ttl3">죄명별 분석요청 현황</div>';
}
C19003 += '<div class="mb3_con" style="float:left"><div id="myChart5"   style="position: relative; height:160px; width:100%"></div></div>';
C19003 += '</div>';
C19003 += '</div>';

dashBoardArr.C19001 = C19001;
dashBoardArr.C19002 = C19002;
dashBoardArr.C19003 = C19003;

$(document).ready(function() {
	
	$("#userGb").val(session_usergb);
	var callUrl = "<c:url value='/sys/dash/querySysDashUDtl.do'/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_queryMDtlCallback'});

});

/**
 * @!@ 대시보드관리 조회 콜백
 * @param
 * @returns 
 */
function fn_queryMDtlCallback(data){
	dashMap = data.list;
   	$.each(dashMap, function(idx, row){
   		if(row.useYn == "Y"){
   			$(".m_contents").append(dashBoardArr[row.dashGb]);
   		}
    });
	var callUrl = "<c:url value='/sys/dash/querySysDashIDtl.do'/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'userSelfForm',callbackNm:'fn_draw_Callback'});
}

/**
 * 조회 callback
 * @param 
 * @returns 
 */
function fn_draw_Callback(data) {
	
	//참고 site : https://developer-ljo.tistory.com/16
	
	$.each(dashMap, function(idx, row){
   		if(row.useYn == "Y"){
   			switch(row.dashGb){
	   			case "C19001" :
					//월별 분석 자료 추이 C19001
					fn_drawMonAnal(data);
					
					//신규 분석 요청 현황 C19001
					fn_drawReqInfo(data);
	   				break;
				case "C19002" :
					//분석관별 현황 C19002
					fn_drawAnalInfo(data);
	   				break;
				case "C19003" :
					//분석지원현황 C19003
					fn_drawMediaInfo(data);
					
					//스토리지 사용 현황 C19003
					fn_drawStorageInfo(data);
				
					//장비 지원 현황  C19003
					fn_drawEquipInfo(data);
					break;
	   			default : 
	   				break;
   			}
   		}
    });
	
	
	
	
}


/**
 * 월별 분석 자료 추이
 * @param 
 * @returns 
 */
function fn_drawMonAnal(data) {

	$('#myChart').empty();
	var arryList =[];

	//arryList.push('월별분석 지원건수');
	$.each( data.findIdInfo, function( key, value ) {

			arryList.push(value.cnt);
	});
	
	
	
	var plot1 = $.jqplot('myChart', [arryList],{
		
		grid: {
		    drawGridLines: true,        // wether to draw lines across the grid or not.
		        gridLineColor: '#cccccc',   // CSS color spec of the grid lines.
		        background: 'white',      // CSS color spec for background color of grid.
		        borderColor: '#cfcfcf',     // CSS color spec for border around grid.
		        borderWidth: 2.0,           // pixel width of border around grid.
		        shadow: false,               // draw a shadow for grid.
		        shadowAngle: 45,            // angle of the shadow.  Clockwise from x axis.
		        shadowOffset: 1.5,          // offset from the line of the shadow.
		        shadowWidth: 3,             // width of the stroke for the shadow.
		        shadowDepth: 3
		},
		animate: !$.jqplot.use_excanvas,
		axesStyles: {
	           borderWidth: 0,
	           ticks: {
	               fontSize: '12pt',
	               fontFamily: 'Times New Roman',
	               textColor: 'black'
	           },
	           label: {
	               fontFamily: 'Times New Roman',
	               textColor: 'black'
	           }
	        },
	        
	        axes: {
                xaxis: {
                          ticks: [1,2,3,4,5,6,7,8,9,10,11,12],
                          tickOptions: {
                              formatString: "%'d 월"
                          },
                        },
                 yaxis: {
                	 tickOptions:{
                		 formatString: "%'d"
                	 },
                	 min:0
                         
                 }

            },
            /*
            highlighter: {
                show: true, 
                showLabel: true, 
                tooltipAxes: 'y',
                sizeAdjust: 7.5 , tooltipLocation : 'ne'
            },*/
            series:[
                {color: '#ff7c7c', lineWidth: 1, label:'good line'}
            ]
	});
	
	$(window).resize(function() {
	      plot1.replot( { resetAxes: true } );
	});
}


/**
 * 신규 분석 요청 현황
 * @param 
 * @returns 
 */
function fn_drawReqInfo(data) {
	
	$("#newReq").empty();
	$.each( data.reqList, function( key, value ) {
		
		$("#newReq").append("<ul><li><a href='#'>"
				+"<i style='width:95px;'>"+value.cdNm+"(외 "+(value.cdCnt-1)+")</i>"
				+" <span class='wd35'><strong>"+value.incdtNm+"</strong></span>"
				+" <span class='wd55' style='font-size:13px;'>작성자: "+value.regrId+"("+value.deptNm+")</span></a></li></ul>");
		  
	});
	
	
}


/**
 * 분석관별 현황
 * @param 
 * @returns 
 */
function fn_drawAnalInfo(data) {

	//console.log(data);
	$('#myChart2').empty();
	var arryNameList =[];

	var arryPrgValList =[];
	var arryEndValList =[];

	//arryList.push('월별분석 지원건수');
	$.each( data.analList, function( key, value ) {

		    arryNameList.push(value.analCrgrId);
		    arryPrgValList.push(value.prg);
		    arryEndValList.push(value.end);
		    
	});
	
	 $.jqplot.config.enablePlugins = true;
     var s1 = arryPrgValList;//[2, 6, 7, 10];
     var s2 = arryEndValList;
     var ticks = arryNameList;//['a', 'b', 'c', 'd'];
      
     plot2 = $.jqplot('myChart2', [s1,s2], {
         // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
         
         grid:{background: 'white'},      // CSS color spec for background color of grid.
	        
         stackSeries: true,
         animate: !$.jqplot.use_excanvas,
         seriesDefaults:{
             renderer:$.jqplot.BarRenderer,
             pointLabels: { show: true },
             shadow:false,
         },
         series:[
        	 {color:'#ff6666',
        		 label:'완료'
        		 
        	 },{color:'#668cff',
        		 label:'진행중'
        		 }
        	 ],
         axes: {
             xaxis: {
                 renderer: $.jqplot.CategoryAxisRenderer,
                 ticks: ticks
             }
         },
         legend: {
             show: true,
             label:'진행중',
             location: 'e',
             placement: 'inside'
         },
         highlighter: { show: false }
         
     });
  
     $('#myChart2').bind('jqplotDataClick', 
         function (ev, seriesIndex, pointIndex, data) {
             $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
         }
     );
     
   
    
     $(window).resize(function() {
         plot2.replot( { resetAxes: false } );
   	}); 
		
	

}


/**
 * 분석지원현황
 * @param 
 * @returns 
 */
function fn_drawMediaInfo(data){
	$('#myChart3').empty();
	
	var arryNameList =[];

	var arryValList =[];

	//arryList.push('월별분석 지원건수');
	 $.each( data.mediaList, function( key, value ) {

		    arryNameList =[];
		    arryNameList.push(value.media);
		    arryNameList.push(value.cnt);
		    arryValList.push(arryNameList);
		    
	}); 
	

      var plot3 = $.jqplot('myChart3', [arryValList], {
    	  grid:{
          	background:'white'
          },
    	 gridPadding: {top:0, bottom:38, left:0, right:0},
         seriesDefaults:{
             renderer:$.jqplot.PieRenderer, 
             trendline:{ show:false }, 
             rendererOptions: { padding: 8, showDataLabels: true }
         },
         legend:{
             show:true, 
             placement: 'outside', 
             rendererOptions: {
                 numberRows: 1
             }, 
             location:'s',
             marginTop: '15px'
         }       
     }); 
  
    /*  $('#myChart3').bind('jqplotDataClick', 
         function (ev, seriesIndex, pointIndex, data) {
             $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
         }
     );
     
     */
     $(window).resize(function() {
         plot3.replot( { resetAxes: true } );
   	}); 
	
}


/**
 * 스토리지 사용 현황
 * @param 
 * @returns 
 */
function fn_drawStorageInfo(data){
	console.log(data);
	
	var arryNameList = [];
	var arryLabelList = [
						{"label": "임시등록"}
						, {"label": "승인요청"}
						, {"label": "검사승인"}
						, {"label": "반려"}
						, {"label": "문서접수"}
						, {"label": "과장승인"}
						, {"label": "선별요청"}
						, {"label": "선별확인"}
						];
	
	var arryValList1 =[];
	var arryValList2 =[];
	var arryValList3 =[];
	var arryValList4 =[];
	var arryValList5 =[];
	var arryValList6 =[];
	var arryValList7 =[];
	var arryValList8 =[];
	
	$.each(data.deptList, function(key, value) {
		arryNameList.push(value.deptNm);
		arryValList1.push(value.c03001Cnt);
		arryValList2.push(value.c03002Cnt);
		arryValList3.push(value.c03003Cnt);
		arryValList4.push(value.c03004Cnt);
		arryValList5.push(value.c03005Cnt);
		arryValList6.push(value.c03006Cnt);
		arryValList7.push(value.c03007Cnt);
		arryValList8.push(value.c03008Cnt);
	})
	
	$('#myChart4').empty();
	plot4 = $.jqplot('myChart4', [arryValList1, arryValList2, arryValList3, arryValList4, arryValList5, arryValList6, arryValList7, arryValList8], {
        grid:{
        	background:'white'
        },
		stackSeries: true,
		animate: !$.jqplot.use_excanvas,
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
            pointLabels: { show: true },
            shadow:false,
            shadowAngle: 135,
            rendererOptions: {
                barDirection: 'horizontal',
                highlightMouseDown: true   
            }
        },
        series:arryLabelList,
        legend: {
            show: true,
            label:'상태',
            location: 'e',
            placement: 'inside'
        },
        axes: {
            yaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: arryNameList
            }
        }
    });
	
	 $(window).resize(function() {
         plot4.replot( { resetAxes: true } );
   	}); 
	
}


/**
 * 장비 지원 현황
 * @param 
 * @returns 
 */
function fn_drawEquipInfo(data){
	$('#myChart5').empty();
	var arryValList =[];
	var totalCnt = 0;
	//console.log(data);
	//arryList.push('월별분석 지원건수');
	$.each( data.eqpList, function( key, value ) {
		    totalCnt += value.count;
	}); 
	var total =[];
	total.push(totalCnt);
	total.push("전체 합계");
    arryValList.push(total);
    
	//arryList.push('월별분석 지원건수');
	$.each( data.eqpList, function( key, value ) {
		    var arryNameList =[];
		    arryNameList.push(value.count);
		    arryNameList.push(value.crimeGrpNm);
		    arryValList.push(arryNameList);
	}); 
    
	plot5 = $.jqplot('myChart5', [arryValList], {
        grid:{
        	background:'white'
        },
		captureRightClick: true,
		animate: !$.jqplot.use_excanvas,
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
            shadow:false,
            shadowAngle: 135,
            rendererOptions: {
                barDirection: 'horizontal',
                highlightMouseDown: true   
            },
            pointLabels: {show: true, formatString: '%d'}
        },
        legend: {
            show: false,
            location: 'e',
            placement: 'inside'
        },
        axes: {
            yaxis: {
                renderer: $.jqplot.CategoryAxisRenderer
            }
        }
    });
	
	 $(window).resize(function() {
		 plot5.replot( { resetAxes: true } );
   	}); 
	
}


function test(){
	var analList = new Array();
	analList.push({analCrgrId: "jetco3", prg: 5, end: 0})
	analList.push({analCrgrId: "수사관1번", prg: 5, end: 2})
	analList.push({analCrgrId: "캡틴코리아", prg: 5, end: 0})
	analList.push({analCrgrId: "포렌식수사관", prg: 5, end: 1})
	var imsi = new Map();
	imsi = {"analList":analList};
	
	//console.log(imsi);
	fn_drawAnalInfo(imsi)
}

webSocketWork = new WebSocket(wscProtocol+"://"+serverIp+"/websocketWork/"+session_userip+"/"+session_usergb+"/"+session_userid);
// WebSocket 서버와 접속이 되면 호출되는 함수
webSocketWork.onopen = function(message) {
	// 콘솔 텍스트에 메시지를 출력한다.
	//document.getElementById("messageTextArea").value += "Server connect...\n";
};

// WebSocket 서버와 접속이 끊기면 호출되는 함수
webSocketWork.onclose = function(message) {
	// 콘솔 텍스트에 메시지를 출력한다.
	//document.getElementById("messageTextArea").value += "Server Disconnect...\n";
};

// WebSocket 서버와 통신 중에 에러가 발생하면 요청되는 함수
webSocketWork.onerror = function(message) {
	// 콘솔 텍스트에 메시지를 출력한다.
	//document.getElementById("messageTextArea").value += "error...\n";
};

// WebSocket 서버로 부터 메시지가 오면 호출되는 함수
webSocketWork.onmessage = function(message) {
	// 콘솔 텍스트에 메시지를 출력한다.
	//document.getElementById("messageTextArea").value += "Recieve From Server => " + message.data+ "\n";
	/* console.log(message.data);
	var analList = new Array();
	analList.push({analCrgrId: "jetco3", prg: 5, end: 0})
	analList.push({analCrgrId: "수사관1번", prg: 5, end: 2})
	analList.push({analCrgrId: "캡틴코리아", prg: 5, end: 0})
	analList.push({analCrgrId: "포렌식수사관", prg: 5, end: 1})
	var imsi = new Map();
	imsi = {"analList":analList};
	
	//console.log(imsi);
	fn_drawAnalInfo(imsi) */
	if(isJson(message.data)){
		/* var jsonMessage = JSON.parse(message.data);
		if(session_usergb == 'C01999'){
			toastr.options.onclick = () => { fn_alarmMove(jsonMessage.result.userId) }
			if(jsonMessage.result.type == 'loginBlock'){
				toastr.error(jsonMessage.result.message, "사용자 ID :  "+ jsonMessage.result.userId);
			}else{
				toastr.info(jsonMessage.result.message, "사용자 ID :  "+ jsonMessage.result.userId);
			}
		} */
	}else{
		$("#isChart").val(message.data);
		var callUrl = "<c:url value='/sys/dash/querySysDashLiveIDtl.do'/>";
		requestUtil.search({callUrl:callUrl,srhFormNm:'searchForm',callbackNm:'fn_liveDraw_Callback'});
	}
};

// Send 버튼을 누르면 호출되는 함수
function sendMessage() {
	// 송신 메시지를 작성하는 텍스트 박스 오브젝트를 취득한다.
	//var message = document.getElementById("textMessage");
	
	// 콘솔 텍스트에 메시지를 출력한다.
	//document.getElementById("messageTextArea").value += "Send to Server => " + message.value + "\n";
	
	// WebSocket 서버에 메시지를 송신한다.
	//webSocketWork.send(message.value);
	
	// 송신 메시지를 작성하는 텍스트 박스를 초기화한다.
	//message.value = "";
}

// Disconnect 버튼을 누르면 호출되는 함수
function disconnect() {
	// WebSocket 접속 해제
	webSocketWork.close();
}
	

function fn_liveDraw_Callback(data){
	//console.log(data);
	var isChart = data.isChart;
	switch (isChart) {
		case "myChart":
			//월별 분석 자료 추이 C19001
			fn_drawMonAnal(data);
			break;
		case "myChart2":
			//분석관별 현황 C19002
			fn_drawAnalInfo(data);
			break;
		case "myChart3":
			//분석지원현황 C19003
			fn_drawMediaInfo(data);
			break;
		case "myChart4":
			//스토리지 사용 현황 C19003
			fn_drawStorageInfo(data);
			break;
		case "myChart5":
			//장비 지원 현황  C19003
			fn_drawEquipInfo(data);
			break;
		case "newReq":
			//신규 분석 요청 현황 C19001
			fn_drawReqInfo(data);
			break;
		default:
			break;
	}
	
	$("#isChart").val("");
}
</script>

</head>
<body onLoad="">
 <div id="main_wrap">
        <div class="m_contents">
        	<form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
        		<input type="hidden" name="userGb" id="userGb"/>
        		<input type="hidden" name="isChart" id="isChart"/>
        		<!-- <a href="javascript:void(0);" onclick="test();return false;" class="RdButton">test</a> -->
        	</form>
               <!-- box1   -->
               <!-- <div class="mbox1" id="C19001">
                  <div class="msb11">
                      <div class="mbox_ttl1">월별분석자료추이</div>
                      
                      <div class="mb1_con" style="position: relative; height:120px; width:58vw">
                      
                       <div class="mb1_con">
                      <div id="myChart"   style="position: relative; height:140px; width:58vw"></div>
                      </div>
                  </div>
               
            <div class="msb12">
                      <div class="mbox_ttl1">신규분석요청현황</div>
                      <div class="mb1_con" id="newReq" >
                        <ul>
                          <li><a href="">
                            <i>모바일</i>
                            <span class="wd35"><strong>반부패수사1과</strong></span>
                            <span class="wd55">군사기밀보호법위반</span></a>
                          </li>

                        </ul>
                      </div>
                  </div>
               </div> -->
               
               <!-- box 2  -->
               <!-- <div class="mbox2" id="C19002">
                  <div class="mbox_ttl2">분석관별현황</div>
                  <div class="mb2_con" style="float:left">
                  <div id="myChart2"   style="position: relative; height:180px; width:100%"></div>
                  </div>
               </div> -->
               
               
               <!-- box3   -->
               <!-- <div class="mbox3" id="C19003">
                  <div class="msb31">
                     <div class="mbox_ttl3">분석지원현황</div>
                     <div class="mb3_con" style="float:left">
                     <div id="myChart3"   style="position: relative; height:160px; width:100%"></div>
                     </div>
                  </div>
                  <div class="msb31">
                     <div class="mbox_ttl3">스토리지사용현황</div>
                     <div class="mb3_con" style="float:left">
                     <div id="myChart4"   style="position: relative; height:160px; width:100%"></div>
                     </div>
                  </div>
                  <div class="msb32">
                     <div class="mbox_ttl3">장비지원현황</div>
                     <div class="mb3_con" style="float:left"><div id="myChart5"   style="position: relative; height:160px; width:100%"></div></div>
                  </div>
               </div> -->
            </div>        
 </div>
</body>
</html>
