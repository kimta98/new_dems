<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 중복 죄명 리스트 팝업
 * 4. 설명 : @!@ 중복 죄명 리스트 팝업
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>디지털 증거 관리 시스템</title>

<script type="text/javaScript" language="javascript" defer="defer">

var crimeCd = '${param.CrimeCd}';
var crimeNm = '${param.CrimeNm}';

$(document).ready(function() {
	$("#excelFile").val("");
	fn_queryMList(1);

	fn_crimeCheck();

	<%/* 전체 checkbox 클릭시 */%>
    $('#chkAll').change(function() {
        var chk = $(this).is(':checked');
        if(chk){
               $('input:checkbox[name=chk]').each(function() {
                   $(this).prop("checked", true);
               });
        }else{
               $('input:checkbox[name=chk]').each(function() {
                   $(this).prop("checked", false);
               });
        }
    });

    $('input:checkbox[name=chk]').change(function () {

     	var chklen = $('input[name="chk"]').length;
    	var chkcou = $('input[name="chk"]:checked').length;

        if( chklen == chkcou) {
           	$("input:checkbox[name='chkAll']").prop("checked", true);
        }else{
           	$("input:checkbox[name='chkAll']").prop("checked", false);
        }
    });




});


/**
 * @!@ 중복 죄명 조회
 * @param {json} data
 * @returns
 */
function fn_crimeCheck(){

	var arrayCd = crimeCd.split(",");
	var arrayNm = crimeNm.split(",");

	var tabTdCnt = $("#listTabPop > colgroup").find("col").length;

	$("#listTabPop > tbody").empty();

	if(arrayCd.length == 0){
		var append = "";
		append += "<tr>";

		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";

		append += "</tr>";
		$("#listTabPop > tbody").append(append);
	}else{
		for(var i=0; i<arrayCd.length; i++){
			var append = "";
			append += "<tr>";
			append += "<td name='crimeCd' id='crimeCd"+i+"'>" + arrayCd[i] + "</td>";
			append += "<td name='crimeNm' id='crimeNm"+i+"'>" + arrayNm[i] + "</td>";
			append +='<td style="text-align:center"><input type="checkbox" name="chk" id="chk'+i+'" value="Y" class="check_agree1"></td>';
			append += "</tr>";
	        $("#listTabPop > tbody").append(append);
		}

	}

	$("#totalCountPop").text(arrayCd.length);

}

function fn_modifyCrime(){

	var cnt = 0;
	var cdArray = [];
	var nmArray = [];

	var totalCount = $("#totalCountPop").text();

	for (var i=0; i < totalCount; i++) {

		if($('#chk'+i).is(':checked')) {
			cnt++;
			cdArray.push($("#crimeCd"+i).text());
			nmArray.push($("#crimeNm"+i).text());
		}

	}

	if(cnt < 1){
		fn_showUserPage('선택해주세요');
		return;
	}

	var data = {cdDatas : cdArray, nmDatas : nmArray};
    var callUrl = "<c:url value='/sys/crimecode/querySysCrimeMatchCodeUpdate.do'/>";
    requestUtil.saveData({callUrl:callUrl,data:data,callbackNm:'fn_modifyCrimeCallBack'});

}

function fn_modifyCrimeCallBack(){

	//$("#excelFile").val("");

	var codeInfo = [{cdId:'crimeGrp',selectId:'srcCrimeGrp',type:'4', callbackNm:'fn_ajaxCodeListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	fn_ajaxCodeList(codeInfo);

	fn_dialogClose("sysCrimeCodePop");

}



</script>

</head>
<body>
<div id="con_wrap_pop">
	<div class="contents">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                      <div class="window_popup">
                          <div class="sub_ttl">중복 죄명 업데이트</div>

                          <div class="sub">
                             <!--------------검색------------------>
                             <form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
                             	<input id="userGb" name="userGb"  title="사용자구분"  type="hidden" class="inpw50" />
                             	<input id="srcUseYn" name="srcUseYn"  title="사용여부"  type="hidden" class="inpw50" value="Y"/>
                             <div class="t_head">
                                  <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_queryMList(1);">
                                        <caption>검색</caption>
                                         <colgroup>
                                            <col width="20%">
                                            <col width="30%">
                                            <col width="20%">
                                            <col width="30%">
                                         </colgroup>
                                   <thead>

                                   </thead>
                                </table>
                              </div>
                            <div class="btn_c">
                                  <ul>
                                     	<li><a href="javascript:void(0)" onclick="fn_modifyCrime();return false;" class="myButton">등록</a></li>
                                     	<li><a href="javascript:void(0)" onclick="fn_modifyCrimeCallBack();return false;" class="myButton">취소</a></li>
                               </div>
                               </form>
                            <!--------------결과------------------>
                             <div class="r_num">| 결과  <strong style="color:#C00" id="totalCountPop" style="text-align:left">0</strong>건</div>

                             <!--------------목록---------------------->
                             <div class="t_list">
                                  <table id="listTabPop" class="tbl_type" border="1" cellspacing="0" >
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="20%">
                                              <col width="20%">
                                              <col width="10%">

                                          </colgroup>
                                            <thead>
                                              <tr>
                                                 <th scope="col">죄명코드</th>
                                                 <th scope="col">죄명</th>
                                                 <th scope="col"><input type="checkbox" name="chkAll" id="chkAll" ></th>
                                              </tr>
                                            </thead>
                                            <tbody>
												<tr><td colspan="8">조회 결과가 없습니다.</td></tr>
                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->

                             <!-----------------------페이징----------------------->
                             <div id="page_navi" class="page_wrap"></div>
                               <!-----------------------//페이징----------------------->

                          </div>

                    </div>
               </div>
                 <!---  //contnets  적용 ------>
       </div>
  </div>
</body>
</html>