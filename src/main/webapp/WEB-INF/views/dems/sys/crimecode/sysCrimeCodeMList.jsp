<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 죄명코드 관리 리스트 화면
 * 4. 설명 : @!@ 죄명코드 관리 리스트 화면
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

	var codeInfo = [{cdId:'crimeGrp',selectId:'srcCrimeGrp',type:'4', callbackNm:'fn_ajaxCodeListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	//fn_ajaxCodeList(codeInfo);
	gfn_init({startFnNm:'', param:codeInfo, codeSet:'Y'});

});

/**
 * @!@ 그룹죄명 코드 리스트 조회 콜백
 * @param {json} data
 * @returns
 */
function fn_ajaxCodeListCallback(data){
	 console.log(data);
	$('#srcCrimeGrp').prepend("<option value='' selected>전체</option");
	requestUtil.getSearchForm({targetFormId:"searchForm", callbackNm:"fn_queryMList"});
}

/**
 * @!@ 죄명코드 관리 리스트 조회
 * @param {int} page
 * @returns
 */
function fn_queryMList(page){
	var callUrl = "<c:url value='/sys/crimecode/querySysCrimeCodeMList.do'/>";
	$("#page").val(page);
	requestUtil.searchList({callUrl:callUrl, srhFormNm:'searchForm', callbackNm:'fn_queryMListCallback', page:$("#page").val(), perPageNum:10});

}

/**
 * @!@ 죄명코드 관리 리스트 조회 콜백
 * @param {json} data
 * @returns
 */
function fn_queryMListCallback(data){
	var list = data.list;
	var listCnt = list.length;
	var tabTdCnt = $("#listTab > colgroup").find("col").length;

	$("#listTab > tbody").empty();

	if(listCnt == 0){
		var append = "";
		append += "<tr>";

		append += "<td colspan='"+tabTdCnt+"'>"+nullListMsg+"</td>";

		append += "</tr>";
		$("#listTab > tbody").append(append);
	}else{
		$.each(list,function(idx,row){
			var append = "";
			append += "<tr>";

			append += "<td><a href='javascript:void(0)' onclick='fn_indexSysCrimeCodeUDtl(\""+row.crimeCd+"\");return false;'><u>"+row.crimeCd+"</u></a></td>";
			append += "<td>" + gfn_nullRtnSpace(row.crimeNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.crimeGrp) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.crimeGrpNm) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.relLaw) + "</td>";
			append += "<td>" + gfn_nullRtnSpace(row.useYnNm) + "</td>";

			append += "</tr>";
	        $("#listTab > tbody").append(append);
	 	});
	}

	data.__callFuncName__ ="fn_queryMList";
	data.__naviID__ ="page_navi";
	pageUtil.setPageNavi(data);
	$("#totalCount").text(data.totalCount);

}

/**
 * @!@ 죄명코드 관리 등록 화면 이동
 * @param
 * @returns
 */
function fn_indexSysCrimeCodeRDtl(){
	requestUtil.setSearchForm("searchForm");
	var param = "?"+$("#searchForm").serialize();
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/sys/crimecode/indexSysCrimeCodeRDtl.do"/>'+param);
}

/**
 * @!@ 죄명코드 관리 수정 화면 이동
 * @param {string} crimeCd, crimeGrp
 * @returns
 */
function fn_indexSysCrimeCodeUDtl(crimeCd){
	requestUtil.setSearchForm("searchForm");
	$("#searchForm").append('<input type="hidden" class="" id="crimeCd" name="crimeCd" value="'+crimeCd+'"/>');
	var param = "?"+$("#searchForm").serialize();
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/sys/crimecode/indexSysCrimeCodeUDtl.do"/>'+param);
}

/**
 * @!@ 죄명코드 관리 엑셀 업로드
 * @param {string} crimeCd, crimeGrp
 * @returns
 */

function fn_updExcSysCrimeCodeMList() {
	if($("#excelFile").val() == ""){
		fn_showUserPage("엑셀파일을 선택해주세요.", function() {
		});
		return false;
	}
	requestUtil.updExcMList2({
		fileNm:"excelFile"												// file 태그 id
		, sheetNum:0													// 시트번호
		, strartRowNum:1												// 읽어드릴 행(0부터 시작)
		, startCelNum:0													// 읽어드릴 셀(0부터 시작)
		, celCnt:6														// 총 셀 갯수
		, culmnNmArr:"crimeCd,crimeNm,crimeGrp,crimeGrpNm,useYn,relLaw"	// 셀 컬럼
		, sqlQueryId:"sysCrimeCodeDAO.regSysCrimeCodeRDtl"				// insert 쿼리 id
		, callbackNm: "updExcMListCallback"								// 콜백함수
	});
}

function updExcMListCallback(data){

	$("#excelFile").val("");
	var codeInfo = [{cdId:'crimeGrp',selectId:'srcCrimeGrp',type:'4', callbackNm:'fn_ajaxCodeListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	fn_ajaxCodeList(codeInfo);
}

/**
 * @!@ 사건관리 조회 팝업
 * @param cdId
 * @returns
 */
function fn_sysCrimeCodePop(data) {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"
	var crimeCd = "";
	var crimeNm = "";

	for(var i=0; i<data.crimeCheckNm.length; i++){

		if (i==0) {
			crimeCd += data.crimeCheckNm[i].crimeCd;
			crimeNm += data.crimeCheckNm[i].crimeNm;
		}else {
			crimeCd += ","+data.crimeCheckNm[i].crimeCd;
			crimeNm += ","+data.crimeCheckNm[i].crimeNm;
		}
	}


	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/sys/crimecode/sysCrimeCodePop&CrimeCd="+crimeCd+"&CrimeNm="+crimeNm,
		height: 700,
        width: 600,
        title: '중복 죄명 조회',
        divId : 'sysCrimeCodePop'
	});



}
</script>
</head>
<body>
          <div id="con_wrap">
               <div id="contents_info">
                      <!--- contnets  적용 ------>
                    <div>
                         <div class="loca">
		                    <div class="ttl">죄명코드 관리</div>
		                    <div class="loca_list">Home > 시스템 관리 > 죄명코드 관리</div>
		                  </div>
                          <div class="sub">
                             <!--------------검색------------------>
                             <form name="searchForm" id="searchForm" method="post" onsubmit="return false;">
                             	<input type="hidden" class="" id="page" name="page" value="1"/>
                            <div class="t_head">
                                  <table class="tbl_type_hd" border="1" cellspacing="0" onkeydown="if(gfn_enterChk())fn_queryMList(1);">
                                        <caption>검색</caption>
                                         <colgroup>
                                            <col width="20%">
                                            <col width="30%">
                                            <col width="20%">
                                            <col width="30%">
                                         </colgroup>
                                   <thead>
                                      <tr>
                                         	<th scope="row" class="hcolor">죄명</th>
								           <td>
								               <input type="text" id="srcCrimeNm" name="srcCrimeNm" title="죄명" class="inpw50" maxlength="300"/>
								           </td>
								           <th scope="row" class="hcolor">그룹죄명</th>
								           <td colspan="3">
								               <select id="srcCrimeGrp" name="srcCrimeGrp" title="그룹죄명" onchange="fn_queryMList(1)" class="selw6">
								               </select>
								           </td>
                                      </tr>
                                      <tr>
                                         	<th scope="row" class="hcolor">관련법령</th>
								           <td>
								               <input type="text" id="srcRelLaw" name="srcRelLaw" title="관련법령" class="inpw50" maxlength="200"/>
								           </td>
								           <th scope="row" class="hcolor">사용여부</th>
								           <td colspan="3">
								               <select id="srcUseYn" name="srcUseYn" title="사용여부" onchange="fn_queryMList(1)" class="selw6">
								                   <option value="">전체</option>
								                   <option value="Y" selected>사용</option>
								                   <option value="N">미사용</option>
								               </select>
								           </td>
                                      </tr>
                                   </thead>
                                </table>
                              </div>
                            <div class="btn_c">
                                  <ul>
                                  	 <li><input name="excelFile" id="excelFile" type="file"></li>
                                     <li><a href="javascript:void(0);" onclick="fn_updExcSysCrimeCodeMList();return false;" class="myButton">엑셀업로드</a></li>
                                     <li><a href="<c:url value='/resources/sample/죄명코드엑셀양식.xlsx'/>" download class="myButton">엑셀양식다운로드</a></li>
                                     <li><a href="javascript:void(0);" onclick="fn_indexSysCrimeCodeRDtl();return false;" class="RdButton">등록</a></li>
                                     <li><a href="javascript:void(0);" onclick="fn_queryMList(1);return false;" class="gyButton">조회</a></li>
                                  </ul>
                               </div>
                               </form>
                            <!--------------//검색------------------>

                            <!--------------결과------------------>
                             <div class="r_num">| 결과  <strong style="color:#C00" id="totalCount">0</strong>건</div>

                             <!--------------목록---------------------->
                             <div class="t_list">
                                  <table id="listTab" class="tbl_type" border="1" cellspacing="0" >
                                        <caption>목록</caption>
                                          <colgroup>
                                              <col width="15%">
                                              <col width="15%">
                                              <col width="15%">
                                              <col width="15%">
                                              <col width="25%">
                                              <col width="15%">
                                          </colgroup>
                                            <thead>
                                              <tr>
                                                 <th scope="col">죄명코드</th>
                                                 <th scope="col">죄명</th>
                                                 <th scope="col">그룹죄명코드</th>
                                                 <th scope="col">그룹죄명</th>
                                                 <th scope="col">관련법령</th>
                                                 <th scope="col">사용여부</th>
                                              </tr>
                                            </thead>
                                            <tbody>
												<tr><td colspan="6">조회 결과가 없습니다.</td></tr>
                                          </tbody>
                                     </table>
                             </div>
                              <!--------------//목록---------------------->

                             <!-----------------------페이징----------------------->
                             <div id="page_navi" class="page_wrap"></div>
                               <!-----------------------//페이징----------------------->

                          </div>

                    </div>
            </div>
                 <!---  //contnets  적용 ------>
  </div>

  <div id="fsysCodeRDtlPop"></div>
  <div id="fsysCodeUDtlPop"></div>
  <div id="fsysCodeDtlRDtlPop"></div>
  <div id="fsysCodeDtlUDtlPop"></div>
  <div id="sysCrimeCodePop"></div>

</body>
</html>