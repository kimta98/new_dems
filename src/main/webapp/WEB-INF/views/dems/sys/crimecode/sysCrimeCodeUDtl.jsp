<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 14. 오전 10:11:33
 * 2. 작성자 : jij
 * 3. 화면명 : 죄명코드 관리 수정 화면
 * 4. 설명 : @!@ 죄명코드 관리 수정 화면 
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>


<script type="text/javaScript" language="javascript" defer="defer">
var tabId;

$(document).ready(function() {
	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');
	
	$("#crimeGrp").on("change", function(){
		$("#crimeGrpSelf").val("");
		$("#crimeGrpNm").val("");
		if($(this).val() == "self"){
			$("#trCrimeGrpSelf").show();
		}else{
			$("#crimeGrpNm").val($("option:selected", this).text());
			$("#trCrimeGrpSelf").hide();
		}
	});
	var codeInfo = [{cdId:'crimeGrp',selectId:'crimeGrp',type:'4', callbackNm:'fn_ajaxCodeListCallback', sqlQueryId:'sysCrimeCodeDAO.querySysCrimeGrpCodeMList'}];
	fn_ajaxCodeList(codeInfo);
	
	<%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
	/* <span class="txt_info" name="ctnByteChk" id="ctnByteChk"></span> */
    //gfn_overMaxLengthText("menuNm",50);
    
	//requestUtil.search({callUrl:"<c:url value='/fsys/menu/queryFsysMenuUpper.do'/>", srhFormNm:'insForm', callbackNm:'fn_callback'});
	
});

/**
 * @!@ 그룹죄명 코드 리스트 조회 콜백
 * @param {json} data 
 * @returns 
 */
function fn_ajaxCodeListCallback(data){
	$('#crimeGrp').prepend("<option value=''>선택</option>");
	$('#crimeGrp option:last').after("<option value='self'>직접입력</option>");
	fn_queryMDtl();
}
 
/**
 * @!@ 죄명코드 상세 조회
 * @param
 * @returns 
 */
function fn_queryMDtl(){
	
	var callUrl = "<c:url value='/sys/crimecode/querySysCrimeCodeMDtl.do'/>";
	
	requestUtil.search({callUrl:callUrl, srhFormNm:'insForm', callbackNm:'fn_queryMDtlCallback'});
	
}

/**
 * @!@ 죄명코드 상세 조회 콜백
 * @param
 * @returns 
 */
function fn_queryMDtlCallback(data){
}
 
/**
* @!@ 죄명코드 관리 수정
* @param
* @returns 
*/
function fn_updSysCrimeCodeUDtl(){

	if(!validUtil.checkInputValid({valFormID:'insForm'})){
		return;
	}

	if($("#crimeGrp option:selected").val() == "self"){
		if($("#crimeGrpSelf").val() == ""){
			fn_showUserPage("그룹죄명코드은(는) 필수 입력 항목입니다.", "fn_tagIdFocus", "crimeGrpSelf");
			return false;
		}
		if($("#crimeGrpNm").val() == ""){
			fn_showUserPage("그룹죄명은(는) 필수 입력 항목입니다.", "fn_tagIdFocus", "crimeGrpNm");
			return false;
		}
	}

	fn_showModalPage("저장 하시겠습니까?", function() {
        var callUrl = "<c:url value='/sys/crimecode/updSysCrimeCodeUDtl.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'insForm',callbackNm:'fn_updSysCrimeCodeUDtlCallback'});
	});
	
}

/**
 * @!@ 죄명코드 관리 수정 콜백
 * @param {json} data
 * @returns 
 */
function fn_updSysCrimeCodeUDtlCallback(data){
	fn_indexSysCrimeCodeMList();
}

/**
 * @!@ 죄명코드 관리 리스트 화면 이동
 * @param
 * @returns 
 */
function fn_indexSysCrimeCodeMList(){
	parent.$('#'+tabId+' iframe').attr('src', '<c:url value="/sys/crimecode/indexSysCrimeCodeMList.do"/>');
}

/**
 * @!@ 죄명 조회 팝업
 * @param cdId
 * @returns 
 */
function fn_sysCrimeCodeMListPop() {

	var callUrl = "<c:url value = "/com/PageLink.do"/>"

	requestUtil.mdPop({
		popUrl : callUrl+"?link="+"dems/sys/crimecode/sysCrimeCodeMListPop&flag=match",
		height: 700,
        width: 1000,
        title: '죄명 조회',
        divId : 'sysCrimeCodeMListPop'
	});

}
</script>

</head>

<body>
<div id="con_wrap">
        <div class="content">
           <!----현재위치----->
             
            <div id="contents_info">
                 <div class="sub_ttl">죄명코드 수정</div><!-----타이틀------>
                 
                  <div class="sub">
                     <!------------검색------------------->
                     <form name="insForm" id="insForm" method="post">
                      <input type="hidden" id="crimeGrp" name="crimeGrp" value="000000" />
                      <input type="hidden" id="crimeGrpNm" name="crimeGrpNm" value="수기" />
                      <input type="hidden" id="useYn" name="useYn" value="Y" />
                      <input type="hidden" id="regType" name="regType" value="W" />
                      <div class="t_list">
		                 <table class="iptTblX">
			               <caption>수정</caption>
			               <colgroup>
				             <col width="20%" />
				             <col width="30%" />
				             <col width="20%" />
				             <col width="30%" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">죄명코드<span class="fontred">*</span></th>
				                 <td>
				                 	<span><c:out value="${param.crimeCd}"/></span>
				                 	<input id="crimeCd" name="crimeCd"  title="죄명코드"  type="hidden" value="<c:out value='${param.crimeCd}'/>" class="inpw60" data-requireNm="죄명코드"	data-maxLength="10" />
				                 </td>
				                 <th scope="row">죄명<span class="fontred">*</span></th>
				                 <td>
				                 	<input id="crimeNm" name="crimeNm"  title="죄명"  type="text" class="inpw60" data-requireNm="죄명"	data-maxLength="300"/>
				                 </td>
			                 </tr>
			                 <tr>
				                 <th scope="row">관련법령<span class="fontred">*</span></th>
				                 <td>
				                 	<input id="relLaw" name="relLaw"  title="관련법령"  type="text" class="inpw60" data-requireNm="관련법령"	data-maxLength="200"/>
				                 </td>
				                 <th scope="row">매칭죄명</th>
				                 <td>
				                 	<input id="matchNm" name="matchNm"  title="매칭죄명"  type="text" readonly class="inpw50" />
							 		<input id="matchCd" name="matchCd"  title="매칭죄명코드" value="<c:out value='${param.matchCd}'/>"  type="hidden" class="inpw50" data-maxLength="10"/>
							 		<button class="buttonG40" onclick="fn_sysCrimeCodeMListPop();return false;">검색</button>
				                 </td>
			                 </tr>
			                 <!-- <tr id="trCrimeGrpSelf" style="display: none;">
				                 <th scope="row">그룹죄명코드<span class="fontred">*</span></th>
				                 <td>
							 		<input id="crimeGrpSelf" name="crimeGrpSelf"  title="그룹죄명코드"  type="text" class="inpw20" data-maxLength="6" />
				                 </td>
				                 <th scope="row">그룹죄명<span class="fontred">*</span></th>
				                 <td>
							 		<input id="crimeGrpNm" name="crimeGrpNm"  title="그룹죄명"  type="text" class="inpw20" data-maxLength="100"/>
				                 </td>
			                 </tr> -->
			                 <tr>
				                 <th scope="row">사용여부</th>
				                 <td colspan="3">
				                 	<select id="useYn" name="useYn" class="selw6" title="사용여부">
					                   <option value="Y" selected>사용</option>
					                   <option value="N">미사용</option>
							 		</select>
				                 </td>
			                 </tr>
			                </tbody>
		                 </table>
	                  </div>
					</form>
                    <div class="btn_c">
                      <ul>
                        <li><a href="javascript:void(0);" class="RdButton" onclick="fn_updSysCrimeCodeUDtl();return false;">저장</a></li>
                        <!-- <li><a href="javascript:void(0);" class="myButton">재입력</a></li> -->
                        <li><a href="javascript:void(0);" class="myButton" onclick="fn_indexSysCrimeCodeMList();return false;">목록</a></li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->   
                       
                    
                          
                  </div>
            </div>
        
        </div>
 </div>
<div id="divPrgPopup"></div>
<div id="sysCrimeCodeMListPop"></div>
</body>
</html>