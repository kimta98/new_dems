<%--
/**
 * <pre>
 * 1. 작성일 : 2021. 4. 16.
 * 2. 작성자 : leeji
 * 3. 화면명 : 공지사항 수정
 * 4. 설명 : 공지사항 수정
 * </pre>
 */
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<script type="text/javaScript">
var tabId;
var fileSeq;
$(document).ready(function() {

	tabId = parent.$('li[id*=li_tabs-M][aria-expanded=true]').attr('aria-controls');

    var callUrl = "<c:url value = "/fbbs/ntc/goFBbsNtcUDtl.do"/>";
	requestUtil.search({callUrl:callUrl,srhFormNm:'modForm',callbackNm:'fn_callBack'});

});

/**
* 자유게시판 수정 콜백함수
* @param {object} data 조회한 결과데이터
* @returns
*/
function fn_callBack(data){
	fileSeq = data.fbbsNtcMap.atchFileId;
    <%/* MaxLength 세팅 (textarea인 경우 span id=objName+"ByteChk"가 있으면 text 표시됨)*/%>
    gfn_overMaxLengthText("title",100);
    gfn_overMaxLength("cnts",4000);

    if(data.fbbsNtcMap.fixNoticeYn == 'Y'){
        $("#fixNoticeYn").attr("checked", true);
    }

    //파일첨부리스트 만들기
	for(var i = 0; i < data.atchFileList.length; i++){
		var id = data.atchFileList[i].id;
		var name = data.atchFileList[i].name;
		var path = data.atchFileList[i].path;
		var upload_yn = data.atchFileList[i].upload_yn;
		var html = upload_yn == 'Y' ? agent.gridServerFile(name,path,id,'N') : agent.gridFile(name , '' , 'N');
		$('#file_list').append(html);
	}
}

/**
 * 자유게시판 수정
 * @param form
 * @returns fn_moveList
 */
function fn_modify(){

    gfn_overMaxLength("cnts",4000);

    if( $("#title").val()==''){
    	fn_showUserPage("제목을 입력하세요.");
        $("#title").focus();
        return false;
    }

    if( $("#cnts").val()==''){
    	fn_showUserPage("내용을 입력하세요.");
        editor.focus();
        return false;
    } else {
        //4000바이트 까지만 저장 가능
        if(gfn_checkByte($("#cnts").val()) > 4000){
        	fn_showUserPage("내용이 너무 길어 저장할 수 없습니다.");
            editor.focus();
            return false;
        }
    }

    if(confirm("수정하시겠습니까?")){
        var callUrl = "<c:url value = "/fbbs/ntc/updFBbsNtcUDtl.do"/>";
    	requestUtil.search({callUrl:callUrl,srhFormNm:'modForm',callbackNm:'fn_updateCallback'});
    }
}

/**
 * 공지사항 삭제
 * @param {String} putupSno 페이지게시판순번
 * @returns fn_moveList
 */
function fn_delete(putupSno) {
	debugger;
    $("#putupSno").val(putupSno);
    if(confirm("삭제하시겠습니까?")){
        var callUrl = "<c:url value = "/fbbs/ntc/delFBbsNtcUDtl.do"/>";
    	requestUtil.search({callUrl:callUrl,srhFormNm:'modForm',callbackNm:'fn_moveList'});
    }
}
function fn_updateCallback(){
	agent.uploadFile({
		obj_id : "file_list",
		target_id : fileSeq,
		type : "notify",
		creator_id : session_userid
	});
	fn_moveList();
}
 /**
  * 자유게시판 목록페이지이동
  * @param
  * @returns
  */
function fn_moveList(){
	var src = "<c:url value = "/fbbs/ntc/indexFBbsNtcMList.do"/>";
	parent.$('#'+tabId+' iframe').attr('src', src);
}
</script>
</head>
<body>
<div id="con_wrap">
        <div class="content">
            <div id="contents_info">
                 <div class="sub_ttl">공지사항수정</div><!-----타이틀------>

                  <div class="sub">
                     <!------------검색------------------->
                      <div class="t_list">
                      <form name="modForm" id="modForm" method="post">
		                <input id="putupSno" name="putupSno" type="hidden" value="<c:out value="${param.putupSno}" />"/>
		                 <table class="iptTblX">
			               <caption>공지사항수정</caption>
			               <colgroup>
				             <col width="20%" />
				             <col width="*" />
			               </colgroup>
			               <tbody>
			                 <tr>
				                 <th scope="row">제목</th>
				                 <td><input type="text" class="" maxlength="50" name="title" id="title" style="width:530px;" title="제목 입력" value="<c:out value='${fBbsNtcVO.title }'/>"/></td>
			                 </tr>
			                 <tr>
				                  <th scope="row">등록일</th>
				                  <td><input type="text" id="putupDt" name="putupDt" class="" style="width:120px;" value="<c:out value='${fBbsNtcVO.putupDt }'/>" readonly="true" title="등록일 입력"/></td>
			                 </tr>
			                 <tr>
				                  <th scope="row">작성자</th>
				                  <td><span id="regrNm"></span></td>
			                 </tr>
			                 <tr>
				                 <th scope="row">고정공지</th>
				                 <td><input type="checkbox" id="fixNoticeYn" name="fixNoticeYn" class="" value="Y"></td>
			                 </tr>
			                 <tr>
				                 <th scope="row">내용<span class="fontred">*</span></th>
				                 <td><textarea id="cnts" name="cnts" class="" rows="3" cols="20" style="width:540px;height:150px;" title="내용 입력"><c:out value='${fBbsNtcVO.cntsCvt }' escapeXml="false"/></textarea>
				                 <span class="txt_info" name="cntsByteChk" id="cntsByteChk"></span>
				                 </td>
			                 </tr>
                             <tr id="divFile">
                                <th scope="row">첨부파일</th>
                                <td scope="row" class="t_left1 bdrn">
                                    <button id="atchFile" name="atchFile" type="button" class="btn_sty3" onclick="agent.getFilePath('file_list');" >찾아보기</button>
                                    <ul id="file_list" class="file_list" style="margin-left: 18px;"></ul>
                                </td>
                              </tr>
			                </tbody>
		                 </table>
		                 </form>
	                  </div>

                    <div class="btn_c">
                      <ul>
                      	<% if(request.getParameter("session_userid").equals("Admin")){ %>
                        <li>
                        	<a href="javascript:void(0);" class='RdButton' onclick="fn_modify(); return false;">저장</a>
                        </li>
                        <li>
                        	<a href="javascript:void(0);" class='RdButton' onclick="fn_delete('<c:out value="${param.putupSno}"/>'); return false;">삭제</a>
                        </li>
                        <%} %>
                        <li>
                        	<a href="javascript:void(0);" class='myButton' onclick="fn_moveList(); return false;">목록</a>
                       	</li>
                      </ul>
                    </div>
                    <!-----------//-검색------------------->



                  </div>
            </div>

        </div>
 </div>
</body>
</html>