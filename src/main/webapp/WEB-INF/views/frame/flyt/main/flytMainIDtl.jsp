<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>

<title>디지털 증거 관리 시스템</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />

<!-- CSS -->
<link rel="stylesheet" href="<c:url value="/css/com/jquery-ui.css" />">

<link rel="stylesheet" type="text/css" href="<c:url value="/css/main/dems_common.css" />" />
<!-- CSS -->


<!-- Javascript -->
<script type="text/javascript" src="<c:url value="/js/frame/thirdparty/jquery/jquery-1.9.1.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/js/frame/thirdparty/ui.js" />"></script>
<script src="<c:url value="/js/frame/thirdparty/jquery/jquery-ui.js" />"></script>
<!-- session time out 체크 -->
<script src="<c:url value="/js/frame/timeOutCheck.js" />"></script>
<!-- request 공통 -->
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp"%>


<script type="text/javascript">
var pwdChng = "${pwdChng}"; // 패스워드 변경 여부
var pwdClr = "${pwdClr}"; // 관리자가 비번초기화한후 변경유무 (NOCLEAR:변경X, CLEAR: 변경O)
var session = <%=session.getMaxInactiveInterval()%> ;
//세션타임으로 남은시간 확인
sessionTime = session;

// 초기화
var tabs
$( function() {

	$(window).resize(function() {
		var Cwidth = $(window).width();
		$('#container, #container_02').css({'left':'287px'});
		$('#container, #container_02').css({'width':Cwidth - 300 +'px'});
	});

	tabs = $("#tabs").tabs();

	tabs.on( "click", "span.ui-icon-close", function() {
	var panelId = $( this ).parents( "li" ).remove().attr( "aria-controls" );
	$("#comSearchDiv").find("form[id$="+panelId+"]").remove();
	$( "#" + panelId ).remove();

	tabs.tabs( "refresh" );
	$("#fullPath").text($("li[aria-selected=true]").attr("data"));

});

	tabs.on("click", "span.ui-icon-reload", function() {

		var menuId = $( this ).parent('li').attr('aria-controls');
		var src = $('#'+menuId + ' > p > iframe').attr('src');
		$('#'+menuId + ' > p > iframe').attr('src', src);

		var id = $( this ).parent('li').attr('id');
		$('#'+id + ' > a').trigger('click');

	});

	tabs.on( "keyup", function( event ) {
		if ( event.altKey && event.keyCode === $.ui.keyCode.BACKSPACE ) {
			var panelId = tabs.find( ".ui-tabs-active" ).remove().attr( "aria-controls" );
			$( "#" + panelId ).remove();
			tabs.tabs( "refresh" );
		}
	});

	$("#tabs").tabs({activate:function(event,ui){
		$("#fullPath").text(ui.newTab.attr('data'));
	}});


    //전체 닫기
     $("#close_all").click(function() {
		$("li[id*='tabs-']").each(function() {
			if( this.id =='tabs-main') {
				return true;
			}
			var parentId = $(this).remove().attr("aria-controls");
			$( "#" + parentId ).remove();
		});

		tabs.tabs( "refresh" );
		$("#fullPath").text("메인");
	});

	var url = "<c:url value="/sys/dash/indexSysDashIDtl.do" />";
	$("#tabs-1").append("<p>"+ "<iframe src="+url+" style=border:0;scroll:no;width:100%; onload=calcHeight(this);setFullPath('"+url+"'); />"+ "</p>");
	tabs.tabs("refresh");

	if(pwdChng == "OVER"){
	 	var alertMsg = (pwdClr == 'CLEAR') ? "비밀번호 변경 1개월이 경과하여 비밀번호를 변경해주시기 바랍니다." : "비밀번호가 초기화 되었습니다. 비밀번호를 변경해주세요.";
	 	fn_showUserPage(alertMsg);
	    var callUrl = "<c:url value = "/com/PageLink.do"/>" +'?link=/frame/flyt/login/flytLoginOverPWPDtlPop&divId=ChgPwPopup&pwdClr='+pwdClr;

	    $('#ChgPwPopup').dialog({
			modal: true,
			open: function (){
				$(this).load(callUrl, {limit: 5},  function (responseText, textStatus, req) {
					if (textStatus == "error") {
						$('#ChgPwPopup').html(responseText);
						return;
					}
				});
			},
			height: 400,
			width: 500,
			title: '비밀번호 변경',
			resizable: false,
			close : function(){
				$(this).dialog("close");
				$(this).empty();
			}
	   });
	}
});


/**
 *
 * 상단 탭 열기
 * @author: Kjm
 * @since : 2018-10-10
 * @param {fullName}   상단 페이지 Full 경로
 * @param {url}        호출 URL
 * @param {menuNm}     메뉴명
 * @param {menuId}     메뉴 ID
 *
 *
 */
	function addNaviTab(fullName, url, menuNm, menuId) {

		//console.log( $("#"+menuId));

		// @!@ 재오픈 시키기
		if(menuId == "M000000033") {
			if ($("#tabs-M000000033").length > 0) {
				var removedId = $("#li_tabs-M000000033").remove().attr("aria-controls");
	            $( "#" + removedId ).remove();
			}
 		}else if(menuId == "M000000032") {
			if ($("#tabs-M000000032").length > 0) {
				var removedId = $("#li_tabs-M000000032").remove().attr("aria-controls");
	            $( "#" + removedId ).remove();
			}
 		}else if(menuId == "M000000035") {
			if ($("#tabs-M000000035").length > 0) {
				var removedId = $("#li_tabs-M000000035").remove().attr("aria-controls");
	            $( "#" + removedId ).remove();
			}
 		}else if(menuId == "M000000113") {
			if ($("#tabs-M000000113").length > 0) {
				var removedId = $("#li_tabs-M000000113").remove().attr("aria-controls");
	            $( "#" + removedId ).remove();
			}
 		}else if(menuId == "M000000087") {
			if ($("#tabs-M000000087").length > 0) {
				var removedId = $("#li_tabs-M000000087").remove().attr("aria-controls");
	            $( "#" + removedId ).remove();
			}
 		}else if(menuId == "M000000088") {
			if ($("#tabs-M000000088").length > 0) {
				var removedId = $("#li_tabs-M000000088").remove().attr("aria-controls");
	            $( "#" + removedId ).remove();
			}
 		}


		$("#fullPath").text(fullName);

		//현재 탭이 열려 있는 경우 해당 탭으로 이동
		if ($("#tabs-" + menuId).length > 0) {
			$('#tabs').tabs("option", "active", $("#tabs-" + menuId).index()-1);
			return;
		}

		var maxOpenTabCnt = 10;
		if ($("li[id*='tabs-']").length >= maxOpenTabCnt) {
			fn_showUserPage('탭은 ' + maxOpenTabCnt + ' 개 까지만 열 수 있습니다.');
			return;
		}

		var id = "tabs-" + menuId
		  , li  = "<li id='li_"+id+"' data='"+fullName+"'><a href='#"+id+"'>"+ menuNm+ "</a> <span class='ui-icon ui-icon-close' role='presentation' style='padding-left: 10px; margin-right: 3px;'>Remove Tab</span><span class='ui-icon-reload' role='presentation' style='position: absolute;width: 12px;height: 12px;right: 6px;top: 12px;font-size: 0;'><a href='javascript:void(0)'><img src='/css/dems/images/icons/btn_refrsh.png'/></a></span></li>";

		tabs.find(".ui-tabs-nav").append(li);
		tabs.append("<div id='" + id + "'><p>"+ "<iframe menuId="+ menuId +" src="+url+" style=border:0;width:100%; scrolling=no onload=calcHeight(this);setFullPath('"+url+"');/></iframe>"+ "</p></div>");
		tabs.tabs("refresh");

		$('#tabs').tabs("option", "active", $('#tabs >ul >li').length - 1);

	}

	function calcHeight(arg){
		var the_height = arg.contentWindow.document.body.scrollHeight+20;
		arg.height = the_height > 730 ? the_height : 730 ;
		arg.style.overflow = "hidden";
 	}

	 function setFullPath(url){
		 $("iframe[src='"+url+"']").contents().find('div.loca_list').text($("li[aria-selected=true]").attr("data"));
	 }


	function fn_openMenu(fullName, url, menuNm, menuId) {
		addNaviTab(fullName, fn_getRequestURI() + url, menuNm, menuId);

	}


    function fn_getRequestURI()
    {
        var requestURI = "<%= request.getContextPath()%>";

        return requestURI;
    }


    function fn_userInfoChange(userId)
    {

    	var uHeight = 800;

    	var callUrl = "<c:url value = "/com/PageLink.do"/>"

    	requestUtil.mdPop({
    		popUrl : callUrl+"?link="+"frame/fsys/user/fsysUserUserUpdPDtlPop&userId="+encodeURIComponent(userId),
    		height: 700,
            width: 1000,
            title: '사용자 정보 수정',
            divId : 'divUserPopUp'
    	});


    }

    /*
    */
    function fn_UserDialogCallBack(data)
    {

        $("#userName").text(data.resultMap.userNm+'['+data.resultMap.userGbNm+']');
    }




	 function fn_onTabClose(menuNo){
	 	$('#tabs-'+menuNo).remove();
	  	$('#li_tabs-'+menuNo).remove();
	  	$("#tabs").tabs().tabs( "refresh" );
	 }

	 var divHelpdialog;




	 /* ------------------------------------------ */
	 // 「WebSocketEx」는 프로젝트 명

	// 「websocket」는 호스트 명
	// WebSocket 오브젝트 생성 (자동으로 접속 시작한다. - onopen 함수 호출)
	//var webSocket = new WebSocket("ws://localhost:8080/WebSocketEx/websocket");
	var webSocket = new WebSocket("ws://"+serverIp+"/websocket/"+session_userip+"/"+session_usergb+"/"+session_userid);
	toastr.options = { 
		"closeButton": true
		, "debug": false
		, "newestOnTop": false
		, "progressBar": true
		, "positionClass": "toast-top-right"
		, "preventDuplicates": false
		, "showDuration": "300"
		, "hideDuration": "1000"
		, "timeOut": "20000"
		, "extendedTimeOut": "1000"
		, "showEasing": "swing"
		, "hideEasing": "linear"
		, "showMethod": "fadeIn"
		, "hideMethod": "fadeOut" 
	};
	
	var webSocket = new WebSocket(wscProtocol+"://"+serverIp+"/websocket/"+session_userip+"/"+session_usergb+"/"+session_userid);

	webSocket.onmessage = function(message) {
		// 콘솔 텍스트에 메시지를 출력한다.
		//document.getElementById("messageTextArea").value += "Recieve From Server => " + message.data+ "\n";
		/* console.log(message.data);
		var analList = new Array();
		analList.push({analCrgrId: "jetco3", prg: 5, end: 0})
		analList.push({analCrgrId: "수사관1번", prg: 5, end: 2})
		analList.push({analCrgrId: "캡틴코리아", prg: 5, end: 0})
		analList.push({analCrgrId: "포렌식수사관", prg: 5, end: 1})
		var imsi = new Map();
		imsi = {"analList":analList};
		, "onclick": function() {
			
		}
		//console.log(imsi);
		fn_drawAnalInfo(imsi) */
		if(isJson(message.data)){
			var jsonMessage = JSON.parse(message.data);
			if(session_usergb == 'C01999'){
				//toastr.options.onclick = function() { location.href = "<c:url value='/fsys/user/indexFsysUserUDtl.do?userId="+jsonMessage.result.userId+"' />"; }
				toastr.options.onclick = () => { fn_alarmMove(jsonMessage.result.userId) }
				if(jsonMessage.result.type == 'loginBlock'){
					toastr.error(jsonMessage.result.message, "사용자 ID :  "+ jsonMessage.result.userId);
				}else{
					toastr.info(jsonMessage.result.message, "사용자 ID :  "+ jsonMessage.result.userId);
				}
			}
		}else{
			
		}
	};

	// Disconnect 버튼을 누르면 호출되는 함수
	function disconnect() {
		// WebSocket 접속 해제
		webSocket.close();
	}

	function fn_clickBtn(state, messageData){
		switch(state){
			case 'open':
		        $( 'div.newBoard > div.b' ).slideUp();
		        $( 'div.newBoard > div.c' ).slideDown();
				break;

			case 'close':
		        $( 'div.newBoard > div.b' ).slideDown();
		        $( 'div.newBoard > div.c' ).slideUp();
				break;
		}
		$( '#messageData' ).text(messageData);
	} // fn_clickBtn

	function fn_updEvdcWarnAlamCfrmY(){
		var callUrl = "<c:url value='/fsys/alam/updEvdcWarnAlamCfrmY.do'/>";
        requestUtil.save({callUrl:callUrl,srhFormNm:'updEvdcWarnAlamCfrmYForm',callbackNm:'fn_updEvdcWarnAlamCfrmYCallback'});
	}

	function fn_updEvdcWarnAlamCfrmYCallback(data){
		fn_clickBtn("close", "");
	}

	function fn_getMenuDate(url){
		var obj = $("li [url='"+url+"']");
		var depthFullname = obj.attr("depthFullname");
		var url = obj.attr("url");
		var menuNm = obj.attr("menuNm");
		var menuno = obj.attr("menuno");

		return {depthFullname : depthFullname , url : url , menuNm : menuNm , menuno : menuno};
	}

	function fn_frameMoveUrl(menuNo,url){
		$("[menuId=" + menuNo + "]").attr("src",url);
	}

</script>

</head>
<body oncontextmenu='return false' ondragstart='return false' onselectstart='return false'>
	<link id="contextPathHolder" data-contextPath="${pageContext.request.contextPath}" />
	<!-- sidebar -->
	<div id="left_wrap">
		<nav class="sidebar">
			<div class="header">
				<div class="logo">
					<a href="/flyt/main/indexFLytMainIDtl.do"><img
						src="/images/logo.gif" width="240px" height="45px" /></a>
				</div>
				<div>
					<ul class="gnb_comm"
						style="display: flex; align-content: flex-start; flex-direction: column; flex-wrap: wrap; overflow: auto;">
						<li>
						<img src="/images/login.png" />
						<strong>
							<a id="userName" href="#" style="font-size: 14px; text-decoration: none;" onclick="javascript:fn_userInfoChange('<c:out value="${user.userId}"/>');">
								<c:choose>
									<c:when test="${fn:length(user.userNm) gt 6}">
										<c:out value="${fn:substring(user.userNm, 0, 6)}" />..
									</c:when>
									<c:otherwise>
										<c:out value="${user.userNm}" />
									</c:otherwise>
								</c:choose>
								<span>[${user.userGbNm}] 님</span>
							</a>
						</strong>
						</li>
						<li>
							<img src="/images/logout.png" title="로그아웃" />
							<strong>
								<a href="<c:url value="/flyt/login/procFLytLoginLogoutPDtl.do" />" style="font-size: 14px; text-decoration: none;">로그아웃</a>
							</strong>
						</li>
					</ul>
				</div>
			</div>
			<div class="btn_lock lock">
				<a href="#">메뉴 펼침</a>
			</div>
			<ul class="lnb" id="lnb">
				<c:forEach items="${menuList}" var="resultInfo" varStatus="status">
					<c:if test="${resultInfo.lvl == 2}">
						<li>
							<a href="#">${resultInfo.menuNm}</a>
							<ul>
								<c:forEach items="${menuList}" var="resultVO">
									<c:if test="${resultVO.lvl == 3 and resultVO.upMenuOrdr == resultInfo.upMenuOrdr }">
										<li menuNo="${resultVO.menuNo}" depthFullname="${resultVO.depthFullname}" url="${resultVO.url}" menuNm="${resultVO.menuNm}">
											<a href="javascript:fn_openMenu('${resultVO.depthFullname}','${resultVO.url}','${resultVO.menuNm}','${resultVO.menuNo}')">
												<img src="/images/3dpt_icon.gif"> ${resultVO.menuNm} </img>
											</a>
										</li>
									</c:if>
								</c:forEach>
							</ul>
						</li>
					</c:if>
				</c:forEach>
			</ul>
		</nav>
	</div>

	<div id="container">
		<!-- contents -->
		<article id="contents" class="contents" role="main">
			<div id="tabs">
				<ul>
					<div style="float: left">
						<a id="close_all" href="#" class="btn_close_all">전체닫기</a>
					</div>
					<li id='tabs-main' data='메인'><a href="#tabs-1">Main</a>
						<span class='ui-icon-reload' role='presentation' style='position: absolute; width: 12px; height: 12px; right: 6px; top: 12px; font-size: 0;'>
							<a href='javascript:void(0)'>
								<img src='/css/dems/images/icons/btn_refrsh.png' />
							</a>
						</span>
					</li>
				</ul>
				<div id="tabs-1">
					<form style="display: none;">
						<!-- 송신 메시지를 작성하는 텍스트 박스 -->
						<input id="textMessage" type="text">
						<!-- 메시지 송신을 하는 버튼 -->
						<input onclick="sendMessage()" value="Send" type="button">
						<!-- WebSocket 접속 종료하는 버튼 -->
						<input onclick="disconnect()" value="Disconnect" type="button">
						<!-- 콘솔 메시지의 역할을 하는 로그 텍스트 에리어.(수신 메시지도 표시한다.) -->
						<textarea id="messageTextArea" rows="10" cols="50"></textarea>
					</form>
					<form id="updEvdcWarnAlamCfrmYForm" name="updEvdcWarnAlamCfrmYForm" style="display: none;">
						<input type="hidden" name="vltnDt" id="vltnDt" value="" />
						<input type="hidden" name="vltnSno" id="vltnSno" value="" />
					</form>
					<div class="newBoard">
						<div class="b" style="background-color: green; display: block;"></div>
						<div class="c" style="background-color: #00ffb8; display: none;">
							<span id="messageData"></span><a href="javascript:void(0)" onclick="fn_updEvdcWarnAlamCfrmY();return false;" class="myButton">확인</a>
						</div>
					</div>
				</div>
			</div>
		</article>
		<!-- // contents -->
	</div>
	<!-- // container -->
	</div>

	<!-- 사용자 정보 수정 팝업 -->
	<div id='divUserPopUp'></div>
	<!-- 비밀번호 변경 팝업 -->
	<div id='ChgPwPopup'></div>

	<!-- // wrap -->
	<div id="comSearchDiv" style="display: none;"></div>
</body>
</html>