<%
 /**
  * @Class Name : UserPasswdChange.jsp
  * @Description : 비밀번호 변경여부 확인
  * @Modification Information
  * 
  * @author 김규형
  * @since 2018.11.23
  * @version 1.0
  * @see
  *  
  * Copyright (C) All right reserved.
  */
%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- css 및 js include  -->

<script type="text/javaScript" language="javascript" defer="defer">
window.top.location.href="<c:url value='/flyt/main/indexFLytMainIDtl.do?pwdChng=OVER&pwdClr=' />" + '${loginVO.pwdClr}';


<!--

// -->
</script>
</head>
<body>

</body>
</html>