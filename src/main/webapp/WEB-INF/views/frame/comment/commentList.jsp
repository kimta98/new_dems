<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<head>
<%@ include file="/WEB-INF/views/frame/fcom/demsCommon.jsp" %>

<style type="text/css">
	.commentWrite {width:100%;margin-top:15px;}
	.txtarea {position: relative;padding-right: 75px;}
	.commentTextarea {width: 100%;height: 60px;padding: 5px;font-size: inherit;font-family: inherit;line-height: 20px;box-sizing: border-box;resize: none;}
	.commtentRegistBtn{display: block;position: absolute;top: 0;right: 0;background-color: #2faa40 !important;border-radius: 5px;}
	.commtentRegistBtn a{display: block;width: 40px;height: 40px;padding: 10px 12px;font-size: 1.154em;line-height: 20px;color: #fff;text-align: center;border-radius: 3px;}

	.commentList{border-top: 1px solid #c9c9c9;border-bottom: 1px solid #c9c9c9;}
	.commentList li{overflow: hidden;position: relative;padding: 10px;border-top: 1px solid #e2e2e2;}
	.commentList .id{display: inline-block;font-weight: bold;line-height: 1;vertical-align: top;}
	.commentList .date{display: inline-block;position: relative;margin-left: 10px;padding-left: 11px;line-height: 1;vertical-align: top;}
	.commentList .txt{display: block;margin: 0 40px 0 0;}
	.commentList .del{position: absolute;top: 10px;right: 10px; text-decoration: none;color: #666;}
	.commentList .upd{position: absolute;top: 10px;right: 70px; text-decoration: none;color: #666;}
	.commentList .updCompletion{position: absolute;top: 10px;right: 70px; text-decoration: none;color: #666;}
	
	.pagingWrap{display: block;position: relative;overflow: hidden; margin-top:20px; margin-bottom:80px;}
	.pagingWrap .paging{text-align: center;margin: 0;overflow: hidden;line-height: 1.8em;word-break: keep-all;word-wrap: break-word;}
	.pagingWrap .paging span{margin: 0 1px;text-align: center;color: #fff;font-weight: bold;text-decoration: underline;border: 0 none;line-height: 27px;width: 27px;display: inline-block;width: 25px;border: 1px solid #dfdfdf;}
	.pagingWrap .paging strong{margin: 0 1px;text-align: center;color: #fff; font-weight: bold; text-decoration: underline;line-height: 27px; font-size: 13px;display: inline-block;width: 25px;border: 1px solid #dfdfdf;background-color: #2faa40 !important;overflow: hidden;vertical-align: middle;}
	.pagingWrap .paging a{color: #666666;font-size: 11px;font-weight: normal;text-decoration: none;display: inline-block;width: 25px;line-height: 25px;text-align: center;border: 1px solid #dfdfdf;overflow: hidden;vertical-align: middle;}
</style>
</head>

<script type="text/javascript">
	var session_userid ='<%=request.getParameter("session_userid") %>';

	$(document).ready(function() {
		$(".content").attr("style","overflow-y: scroll;");
		$('html').css("overflow","hidden");
		console.log($("#con_wrap").height());
	});

	function commentRegist(){

		var form = document.commentRegistForm;

		if(form.contents.value == null || form.contents.value == ''){
			alert("댓글을 입력하세요.");
			form.contents.focus();
			return;
		}

		var textLen = $('.commentTextarea').val().length;
		if(textLen > 100) {
			alert("댓글은 100자 이내로 작성하세요.");
			return false;
		}
		if(confirm("댓글을 등록하시겠습니까?")){
			$.ajax({
				type:'POST',
				data : $("form[name=commentRegistForm]").serialize(),
				datatype:'json',
				url : "/bbs/${paramVO.boardIdx}/${paramVO.boardCode}/commentRegist.do",
				success : function(data){
					fnCommentList(1);
				},
				error : function(xhr, status, error){
				}
			});

		}
	}
	// 댓글 수정
	function fnCommentUpdate(idx){
		var selectIdx = document.getElementById('commentIdx' + idx).value;

		if(idx == selectIdx){
			$('#inputupdate'+idx).css('display', 'block');
			$('#updCompletion'+idx).css('display', 'block');
			$('#upd'+idx).css('display', 'none');			
		}
	}
	
	// 수정완료버튼 클릭시
	function fnCommentUpdateCompletion(idx){
		var form = document.commentUpdateForm;
		form.idx.value = idx;
		form.contents.value = document.getElementById('inputupdate'+idx).value;
		
		if(form.contents.value == null || form.contents.value == ''){
			alert("댓글을 한글자 이상 입력하세요.");
			form.contents.focus();
			return;
		}

		var textLen = $('#inputupdate'+idx).val().length;
		if(textLen > 100) {
			alert("댓글은 100자 이내로 작성하세요.");
			return false;
		}
		
		if(confirm("댓글을 수정하시겠습니까?")){
			$.ajax({
				type:'POST',
				data : $("form[name=commentUpdateForm]").serialize(),
				datatype:'json',
				url : "/bbs/${paramVO.boardIdx}/${paramVO.boardCode}/commentUpdate.do",
				success : function(data){
					fnCommentList(1);
				},
				error : function(xhr, status, error){
				}
			});

		}
	}
	// 댓글 삭제
	function fnCommentDelete(idx){
		var form = document.commentDeleteForm;
		form.idx.value = idx;
		if(confirm("댓글을 삭제하시겠습니까?")){
			$.ajax({
				type:'POST',
				data : $("form[name=commentDeleteForm]").serialize(),
				datatype:'json',
				url : "/bbs/${paramVO.boardIdx}/${paramVO.boardCode}/commentDelete.do",
				success : function(data){
					fnCommentList(1);
				},
				error : function(xhr, status, error){
				}
			});

		}
	}

	function fnCommentList(page){
		$("#commentWrap").load("/bbs/${paramVO.boardIdx}/${paramVO.boardCode}/commentList.do?currentPageNo="+page);
	}

</script>
<div class="commentWrap" id="commentWrap">
	<!-- 댓글삭제 -->
	<form name="commentDeleteForm" id="commentDeleteForm" method="post">
		<input type="hidden" name="idx" value="" />
		<input type="hidden" name="boardIdx" value="${paramVO.boardIdx}" />
		<input type="hidden" name="boardCode" value="${paramVO.boardCode}" />
	</form>
	<!-- 댓글수정 -->
	<form name="commentUpdateForm" id="commentUpdateForm" method="post">
		<input type="hidden" name="idx" value="" />
		<input type="hidden" name="boardIdx" value="${paramVO.boardIdx}" />
		<input type="hidden" name="boardCode" value="${paramVO.boardCode}" />
		<input type="hidden" name="contents" value="" />
	</form>
	<!-- 댓글작성 -->
	<form name="commentRegistForm" id="commentRegistForm" method="post">
		<input type="hidden" name="boardIdx" value="${paramVO.boardIdx}" />
		<input type="hidden" name="boardCode" value="${paramVO.boardCode}" />
		<div class="commentWrite">
			<div class="txtarea">
				<textarea name="contents" class="commentTextarea"></textarea>
				<span class="commtentRegistBtn"><a href="#none" onclick="commentRegist();">댓글등록</a></span>
			</div>
		</div>
	</form>

	<!-- //댓글작성 -->
	<p class="commentCount">댓글( <c:out value='${totalCount}' /> )</p>
	<!-- 댓글목록 -->
	<ul class="commentList">
		<c:choose>
			<c:when test="${not empty list}">
				<c:forEach var="commentVO" items="${list}" varStatus="status">
					<li>
						<strong class="id">${commentVO.regNm}</strong>
						<span class="date">
							<fmt:parseDate value="${commentVO.regDt}" var="regData" pattern="yyyy-MM-dd HH:mm:ss"/>
							<fmt:formatDate value="${regData}" pattern="yyyy.MM.dd"/>
						</span>
						<p class="txt">${commentVO.contents}</p>
						<c:if test="${not empty loginVO.userId and loginVO.userId eq commentVO.regId}">
							<input type="hidden" id="commentIdx${commentVO.idx}" value="${commentVO.idx}" />
							<a href="#update" class="upd" id="upd${commentVO.idx}" onclick="fnCommentUpdate('${commentVO.idx}');">수정하기</a>
							<a href="#delete" class="del" onclick="fnCommentDelete('${commentVO.idx}');">삭제하기</a>
							<input type="text" style=" display:none; width: 75%; margin-bottom: 5px; margin-top:13px" value="${commentVO.contents}" id="inputupdate${commentVO.idx}" class="inputupdate"/>
							<a href="#updateCompletion" id="updCompletion${commentVO.idx}" class="updCompletion" style="display: none;" onclick="fnCommentUpdateCompletion('${commentVO.idx}');">수정완료</a>
						</c:if>
					</li>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<li class="ta_c">댓글이 없습니다.</li>
			</c:otherwise>
		</c:choose>
	</ul>
	<!-- //댓글목록 -->
	<div class="pagingWrap">
		<p class="paging">
			<ui:pagination paginationInfo="${paramVO}" type="user" jsFunction="fnCommentList"/>
		</p>
	</div>
</div>
<!-- //댓글 -->