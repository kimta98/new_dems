package frame.fcom.service;

/**
 * @Class Name : CodeVO.java
 * @Description : Code VO class
 * @Modification Information
 *
 * @author 우성택
 * @since 2014.09.03
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

public class CodeVO {
	
	private static final long serialVersionUID = 1L;

	private String cdId;
	private String cdIdNm;
	private String cd;
	private String cdNm;
	public String getCdId() {
		return cdId;
	}
	public void setCdId(String cdId) {
		this.cdId = cdId;
	}
	public String getCdIdNm() {
		return cdIdNm;
	}
	public void setCdIdNm(String cdIdNm) {
		this.cdIdNm = cdIdNm;
	}
	public String getCd() {
		return cd;
	}
	public void setCd(String cd) {
		this.cd = cd;
	}
	public String getCdNm() {
		return cdNm;
	}
	public void setCdNm(String cdNm) {
		this.cdNm = cdNm;
	}
	
	
	

}
