package frame.futil;

public class DemsConst {

	//JQGRID 상수값 그리드 DATA ID
	public static final String Jqrid_rows = "rows";
	
	
	public static final String Messages_UserComMessage   = "_server_user_com_message_";
	public static final String Messages_UserErrMessage   = "_server_user_err_message_";
	public static final String Messages_SysSucMessage    = "_server_sys_suc_message_";

}
