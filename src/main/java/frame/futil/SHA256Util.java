package frame.futil;

import java.security.MessageDigest;
import java.util.Random;

public class SHA256Util {

	public static String getSalt() {
		Random random = new Random();
		byte[] salt = new byte[10];

		random.nextBytes(salt);

		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < salt.length; i++) {
			sb.append(String.format("%02x", salt[i]));
		}

		return sb.toString();
	}

	public static String getEncrypt(String pwd, String key) {

		byte[] salt = key.getBytes();
		String result = "";

		byte[] temp = pwd.getBytes();
		byte[] bytes = new byte[temp.length + salt.length];

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(pwd.getBytes());
			md.update(key.getBytes());

			byte[] b = md.digest();

			StringBuffer sb = new StringBuffer();

			for (int i = 0; i < b.length; i++) {
				sb.append(Integer.toString((b[i] & 0xFF) + 256, 16).substring(1));
			}

			result = sb.toString();

		} catch (Exception e) {

		}

		return result;
	}

	public static void main(String args[]){
		String value = getSalt();
		System.out.println(value);
		System.out.println(SHA256Util.getEncrypt( "tkdwls12!@" , "1da433ffe411cae59a18"));
	}

}
