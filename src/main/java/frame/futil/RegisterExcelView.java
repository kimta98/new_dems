package frame.futil;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.AbstractView;

import dems.site.requested.vo.RequestedVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

public class RegisterExcelView extends AbstractView {
	
	private static final Logger LOGGER  = LoggerFactory.getLogger(RegisterExcelView.class);

	/** The content type for an Excel response */
	private static final String CONTENT_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	
	private static final String[] COLUMN_NAME_ARRAY = {"요청번호", "요청부서", "요청자명", "요청자ID", "등록일", "요청상태", "사건명", "사건번호"};

	public RegisterExcelView() {
		
	}
	
	@Override
	protected boolean generatesDownloadContent() {
		return true;
	}
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		LOGGER.debug("Created Excel Workbook from scratch");

		setContentType(CONTENT_TYPE_XLSX);
		
		buildExcelDocument(model, workbook, request, response);
		
		// Set the filename
		String sFilename = "";
		if(model.get("filename") != null){
			sFilename = (String)model.get("filename");
		}else if(request.getAttribute("filename") != null){
			sFilename = (String)request.getAttribute("filename");
		}else{
			sFilename = getClass().getSimpleName();
		}
		
		response.setContentType(getContentType());

		String header = request.getHeader("User-Agent");
		sFilename = sFilename.replaceAll("\r","").replaceAll("\n","");
		if(header.contains("MSIE") || header.contains("Trident") || header.contains("Chrome")){
			sFilename = URLEncoder.encode(sFilename,"UTF-8").replaceAll("\\+","%20");
			response.setHeader("Content-Disposition","attachment;filename="+sFilename+".xlsx;");
		}else{
			sFilename = new String(sFilename.getBytes("UTF-8"),"ISO-8859-1");
			response.setHeader("Content-Disposition","attachment;filename=\""+sFilename + ".xlsx\"");
		}

		// Flush byte array to servlet output stream.
		ServletOutputStream out = response.getOutputStream();
		out.flush();
		workbook.write(out);
		out.flush();
		
	}
	
	protected void buildExcelDocument(Map model, XSSFWorkbook wb, HttpServletRequest req, HttpServletResponse resp) throws Exception {
		Map<String, Object> dataMap = (Map<String, Object>) model.get("dataMap");
		XSSFCell cell = null;

		/**
		 * keyArray : ['site', 'analysis', 'tral', 'evdc', 'equip']
		 */
		String[] keyArray = (String[]) dataMap.get("itemKeyArray");
		
		String sheetNm = "";
		
		for(String key : keyArray) {
			
			if(key.equals("site")) {
				sheetNm = "현장관리";
			}else if(key.equals("analysis")) {
				sheetNm = "분석관리";
			}else if(key.equals("tral")) {
				sheetNm = "공판관리";
			}else if(key.equals("evdc")) {
				sheetNm = "디지털증거관리";
			}else if(key.equals("equip")) {
				sheetNm = "장비관리";
			}
			
			String[] columnArr = COLUMN_NAME_ARRAY;
			
			List<RequestedVO> dataList = (List<RequestedVO>) dataMap.get(key);
			
			CellStyle cellStyle = wb.createCellStyle(); // 제목셀의 셀스타일
			cellStyle.setWrapText(true); // 줄 바꿈            
			cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index); // 셀 색상
			cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND); // 셀 색상 패턴
			cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 셀 가로 정렬
			cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 셀 세로 정렬
			cellStyle.setDataFormat((short)0x31); // 셀 데이터 형식
			cellStyle.setBorderRight(HSSFCellStyle.BORDER_DOUBLE);
			cellStyle.setBorderLeft(HSSFCellStyle.BORDER_DOUBLE);
			cellStyle.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);
			cellStyle.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);

			// 셀 폰트색상, bold처리
			Font font = wb.createFont();
			font.setColor(HSSFColor.WHITE.index);
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			cellStyle.setFont(font);

			CellStyle cellStyle2 = wb.createCellStyle(); // 데이터셀의 셀스타일
			cellStyle2.setWrapText(true); // 줄 바꿈           
			cellStyle2.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 셀 가로 정렬
			cellStyle2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER); // 셀 세로 정렬
			cellStyle2.setDataFormat((short)0x31); // 셀 데이터 형식
			cellStyle2.setBorderRight(HSSFCellStyle.BORDER_THIN);
			cellStyle2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			cellStyle2.setBorderTop(HSSFCellStyle.BORDER_THIN);
			cellStyle2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			
			XSSFSheet sheet = wb.createSheet(sheetNm);
			sheet.setDefaultColumnWidth(8);
			
			// 컬럼명 삽입
			for(int i=0; i<columnArr.length; i++){
				setText(getCell(sheet, 0, i), columnArr[i]);
				getCell(sheet, 0, i).setCellStyle(cellStyle);
				sheet.autoSizeColumn(i);
				int columnWidth = (sheet.getColumnWidth(i))*5;
				sheet.setColumnWidth(i, columnWidth);

				if(dataList.size() < 1){
					cell = getCell(sheet, 1, i);
					if(i==0){
						setText(cell, "등록된 정보가 없습니다.");
					}
					cell.setCellStyle(cellStyle2);
				}
			}
			
			if(dataList.size() > 0){ // 저장된 데이터가 있을때
				// 리스트 데이터 삽입
				for (int i = 0; i<dataList.size(); i++) {
					RequestedVO requestedVO = dataList.get(i);
					
					XSSFCell cell1 = getCell(sheet, 1+i, 0);
					setText(cell1, requestedVO.getReqSeq());
					cell1.setCellStyle(cellStyle2);
					
					XSSFCell cell2 = getCell(sheet, 1+i, 1);
					setText(cell2, requestedVO.getReqInsttNm());
					cell2.setCellStyle(cellStyle2);
					
					XSSFCell cell3 = getCell(sheet, 1+i, 2);
					setText(cell3, requestedVO.getReqUserNm());
					cell3.setCellStyle(cellStyle2);
					
					XSSFCell cell4 = getCell(sheet, 1+i, 3);
					setText(cell4, requestedVO.getReqUserId());
					cell4.setCellStyle(cellStyle2);
					
					XSSFCell cell5 = getCell(sheet, 1+i, 4);
					setText(cell5, requestedVO.getReqDt());
					cell5.setCellStyle(cellStyle2);
					
					XSSFCell cell6 = getCell(sheet, 1+i, 5);
					setText(cell6, requestedVO.getReqStatus());
					cell6.setCellStyle(cellStyle2);
					
					XSSFCell cell7 = getCell(sheet, 1+i, 6);
					setText(cell7, requestedVO.getIncdtNm());
					cell7.setCellStyle(cellStyle2);
					
					XSSFCell cell8 = getCell(sheet, 1+i, 7);
					setText(cell8, requestedVO.getIncdtNo());
					cell8.setCellStyle(cellStyle2);
				}
			}else{ // 저장된 데이터가 없으면 셀 병합
				// 셀 병합(시작열, 종료열, 시작행, 종료행)
				sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, columnArr.length-1));
			}
			
		}
	}

	protected XSSFCell getCell(XSSFSheet sheet, int row, int col) {
		XSSFRow sheetRow = sheet.getRow(row);
		if (sheetRow == null) {
			sheetRow = sheet.createRow(row);
		}
		XSSFCell cell = sheetRow.getCell((short) col);
		if (cell == null) {
			cell = sheetRow.createCell((short) col);
		}
		return cell;
	}

	protected void setText(XSSFCell cell, String text) {
		cell.setCellType(XSSFCell.CELL_TYPE_STRING);
		cell.setCellValue(text);
	}  
	
}
