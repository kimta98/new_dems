package frame.comment.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.comment.service.CommentService;
import frame.comment.service.CommentVO;
import frame.fcom.service.ComService;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.DemsConst;

@Controller
public class CommentController {

	protected Logger log = LoggerFactory.getLogger(CommentController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "commentService")
	private CommentService commentService;

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;

    @RequestMapping(value="/bbs/{boardIdx}/{boardCode}/commentList.do")
    public String commentList(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") CommentVO commentVO) throws Exception {

		int totalCount = commentService.selectCommentListCount(commentVO);
		if(totalCount > 0){
			List<CommentVO> list = commentService.selectCommentList(commentVO);
		    model.addAttribute("list", list);
		}

		commentVO.setTotalRecordCount(totalCount);

	    model.addAttribute("totalCount", totalCount);

        return "frame/comment/commentList";
    }

    @RequestMapping(value="/bbs/{boardIdx}/{boardCode}/commentRegist.do")
    public ModelAndView commentRegist(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") CommentVO commentVO) throws Exception {

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	commentVO.setRegId(user.getUserId());
		int result = commentService.insertComment(commentVO);
		if(result == 1){
			model.addAttribute(DemsConst.Messages_UserComMessage,"등록되었습니다.");
		} else{
			model.addAttribute(DemsConst.Messages_UserComMessage,"등록중 오류가 발생했습니다.");
		}

        return new ModelAndView(ajaxMainView, model);
    }

    @RequestMapping(value="/bbs/{boardIdx}/{boardCode}/commentUpdate.do")
    public String commentUpdate(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") CommentVO commentVO) throws Exception {
    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	commentVO.setUpdId(user.getUserId());
		int result = commentService.updateComment(commentVO);

        return "frame/comment/commentList";
    }

    @RequestMapping(value="/bbs/{boardIdx}/{boardCode}/commentDelete.do")
    public String commentDelete(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") CommentVO commentVO) throws Exception {

    	CommentVO vo = commentService.selectComment(commentVO);

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	commentVO.setUpdId(user.getUserId());

		int result = commentService.deleteComment(commentVO);

        return "frame/comment/commentList";
    }

}