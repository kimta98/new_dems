
package frame.comment.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;
import frame.comment.service.CommentVO;

@Repository("commentDAO")
public class CommentDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(CommentDAO.class);

    public CommentVO selectComment(CommentVO commentVO) throws Exception {

    	return	(CommentVO) selectOne("commentDAO.selectComment", commentVO);
    }

    public List<CommentVO> selectCommentList(CommentVO commentVO) throws Exception {

    	return (List) selectList("commentDAO.selectCommentList", commentVO);
    }

    public int selectCommentListCount(CommentVO commentVO) throws Exception {

    	return (Integer) selectOne("commentDAO.selectCommentListCount" , commentVO);
    }

    public int insertComment(CommentVO commentVO) throws Exception {
        return (Integer) update("commentDAO.insertComment", commentVO);
    }

    public int updateComment(CommentVO commentVO) throws Exception {
    	return (Integer) update("commentDAO.updateComment", commentVO);
    }

    public int deleteComment(CommentVO commentVO) throws Exception {
    	return (Integer) update("commentDAO.deleteComment", commentVO);
    }

}