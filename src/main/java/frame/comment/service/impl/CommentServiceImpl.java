package frame.comment.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import frame.comment.service.CommentService;
import frame.comment.service.CommentVO;
import frame.fcom.service.ComService;


@Service("commentService")
public class CommentServiceImpl extends EgovAbstractServiceImpl implements CommentService{

	protected Logger log = LoggerFactory.getLogger(CommentServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;

	@Resource(name="commentDAO")
	private CommentDAO commentDAO;

	@Override
	public CommentVO selectComment(CommentVO commentVO) throws Exception {
		return commentDAO.selectComment(commentVO);
	}

	@Override
	public List<CommentVO> selectCommentList(CommentVO commentVO) throws Exception {
		return commentDAO.selectCommentList(commentVO);
	}

	@Override
	public int selectCommentListCount(CommentVO commentVO) throws Exception {
		return commentDAO.selectCommentListCount(commentVO);
	}

	@Override
	public int insertComment(CommentVO commentVO) throws Exception {
		return commentDAO.insertComment(commentVO);
	}

	@Override
	public int updateComment(CommentVO commentVO) throws Exception {
		return commentDAO.updateComment(commentVO);
	}

	@Override
	public int deleteComment(CommentVO commentVO) throws Exception {
		return commentDAO.deleteComment(commentVO);
	}

}