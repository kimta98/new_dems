package frame.comment.service;

import java.util.List;


public interface CommentService {

	CommentVO selectComment(CommentVO commentVO) throws Exception;

	List<CommentVO> selectCommentList(CommentVO commentVO) throws Exception;

    int selectCommentListCount(CommentVO commentVO) throws Exception;

    int insertComment(CommentVO commentVO) throws Exception;

    int updateComment(CommentVO commentVO) throws Exception;

    int deleteComment(CommentVO commentVO) throws Exception;

}