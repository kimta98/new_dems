package frame.fsys.user.service;

import frame.fcom.service.SearchVO;

/**
 * 
 * <pre>
 * 1. 클래스명 : FsysUserVO.java
 * 2. 작성일 : 2021. 4. 14.
 * 3. 작성자 : ilyong
 * 4. 설명 : 
 * </pre>
 */
public class FsysUserVO extends SearchVO {
	
	private static final long serialVersionUID = -2854179418705380886L;


	private String userId;
	private String regStatus;
	private String userGb;
	private String userGbNm;
	private String pwd;
	private String userNm;
	private String telNo;
	private String hpTelNo;
	private String email;
	private String leaveDt;
	private String pwdChgDt;
	private String pwdErrCnt;
	private String pwdFindQues;
	private String pwdFindAsw;
	private String insttCd;
	private String useYn;
	private String regDt;
	private String regrId;
	private String updDt;
	private String updrId;
	private String dispRegDt;
	private String dispPwdChgDt;
	private int rNum;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRegStatus() {
		return regStatus;
	}
	public void setRegStatus(String regStatus) {
		this.regStatus = regStatus;
	}
	public String getUserGb() {
		return userGb;
	}
	public void setUserGb(String userGb) {
		this.userGb = userGb;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
	public String getTelNo() {
		return telNo;
	}
	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}
	public String getHpTelNo() {
		return hpTelNo;
	}
	public void setHpTelNo(String hpTelNo) {
		this.hpTelNo = hpTelNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLeaveDt() {
		return leaveDt;
	}
	public void setLeaveDt(String leaveDt) {
		this.leaveDt = leaveDt;
	}
	public String getPwdChgDt() {
		return pwdChgDt;
	}
	public void setPwdChgDt(String pwdChgDt) {
		this.pwdChgDt = pwdChgDt;
	}
	public String getPwdErrCnt() {
		return pwdErrCnt;
	}
	public void setPwdErrCnt(String pwdErrCnt) {
		this.pwdErrCnt = pwdErrCnt;
	}
	public String getPwdFindQues() {
		return pwdFindQues;
	}
	public void setPwdFindQues(String pwdFindQues) {
		this.pwdFindQues = pwdFindQues;
	}
	public String getPwdFindAsw() {
		return pwdFindAsw;
	}
	public void setPwdFindAsw(String pwdFindAsw) {
		this.pwdFindAsw = pwdFindAsw;
	}
	public String getInsttCd() {
		return insttCd;
	}
	public void setInsttCd(String insttCd) {
		this.insttCd = insttCd;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getRegrId() {
		return regrId;
	}
	public void setRegrId(String regrId) {
		this.regrId = regrId;
	}
	public String getUpdDt() {
		return updDt;
	}
	public void setUpdDt(String updDt) {
		this.updDt = updDt;
	}
	public String getUpdrId() {
		return updrId;
	}
	public void setUpdrId(String updrId) {
		this.updrId = updrId;
	}
	public String getDispRegDt() {
		return dispRegDt;
	}
	public void setDispRegDt(String dispRegDt) {
		this.dispRegDt = dispRegDt;
	}
	public String getDispPwdChgDt() {
		return dispPwdChgDt;
	}
	public void setDispPwdChgDt(String dispPwdChgDt) {
		this.dispPwdChgDt = dispPwdChgDt;
	}
	public int getrNum() {
		return rNum;
	}
	public void setrNum(int rNum) {
		this.rNum = rNum;
	}
	
	/**
	 * @return the userGbNm
	 */
	public String getUserGbNm() {
		return userGbNm;
	}
	/**
	 * @param userGbNm the userGbNm to set
	 */
	public void setUserGbNm(String userGbNm) {
		this.userGbNm = userGbNm;
	}
	
	
	
	
	
	

}
