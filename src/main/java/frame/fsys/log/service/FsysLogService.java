package frame.fsys.log.service;

import java.util.List;
import java.util.Map;

import frame.flyt.main.service.MenuVO;

public interface FsysLogService {

	List selectFsysLogList(Map<String,Object> paramMap) throws Exception;
	
	List selectFsysLogQListDtl(Map map) throws Exception;

}
