package frame.fsys.program.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import frame.flyt.main.service.MenuVO;

/**
 * 
 * <pre>
 * 1. 클래스명 : FsysProgramService.java
 * 2. 작성일 : 2021. 5. 13.
 * 3. 작성자 : jij
 * 4. 설명 : @!@ 프로그램 관리
 * </pre>
 */
public interface FsysProgramService {

	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : delFsysProgramUDtl
	 * 2. 작성일 : 2021. 5. 13. 오후 8:04:03
	 * 3. 작성자 : jij
	 * 4. 설명 : @!@ 프로그램 관리 삭제
	 * </pre>
	 * @param reqParams
	 * @return
	 * @throws Exception
	 */
	String delFsysProgramUDtl(HashMap reqParams) throws Exception;

}
