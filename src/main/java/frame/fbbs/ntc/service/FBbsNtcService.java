package frame.fbbs.ntc.service;

import java.util.List;
import java.util.Map;


public interface FBbsNtcService {

List selectFBbsNtcList(Map<String,Object> paramMap) throws Exception;
	
    void updateFBbsNtcCount(Map<String,Object> paramMap) throws Exception;
    
    Map selectFBbsNtcRDtl(Map<String,Object> paramMap) throws Exception;
    
    Map selectFBbsNtcUser(Map<String,Object> paramMap) throws Exception;
    
    String selectUserGb(Map<String,Object> paramMap) throws Exception;
    
	String insertFBbsNtc(Map<String,Object> paramMap, Map<String,Object> fileMap) throws Exception;
	
    String updateFBbsNtc(Map<String,Object> paramMap, Map<String,Object> fileMap) throws Exception;
    
    void deleteFBbsNtc(Map<String,Object> paramMap) throws Exception;

}