package dems.stts.medcrmtypestate.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import frame.flyt.login.service.FLytLoginVO;



/**
 * 
 * <pre>
 * 1. 클래스명 : SttsMedCrmTypeStateService.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 통계관리
 * </pre>
 */
@SuppressWarnings("rawtypes")
public interface SttsMedCrmTypeStateService {


	/**
	 * 
	 * <pre>
     * 1. 메소드명 : selectSttsMedCrmList
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  통계관리 > 매체별 범죄유형별 접수/처리건수 현황
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	List selectSttsMedCrmList(Map map) throws Exception;
	
	
}

