package dems.stts.medcrmtypestate.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import dems.bbs.free.service.impl.BbsFreeServiceImpl;
import dems.stts.medcrmtypestate.service.SttsMedCrmTypeStateService;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;


/**
 * 
 * <pre>
 * 1. 클래스명 : SttsMedCrmTypeStateImpl.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 통계관리
 * </pre>
 */
@Service("sttsMedCrmTypeStateService")
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class SttsMedCrmTypeStateImpl extends EgovAbstractServiceImpl implements	SttsMedCrmTypeStateService{
	
	protected Logger log = LoggerFactory.getLogger(SttsMedCrmTypeStateImpl.class);
	
	@Resource(name="sttsMedCrmTypeStateDAO")
	private SttsMedCrmTypeStateDAO sttsMedCrmTypeStateDAO;
	
	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="comDAO")
	private ComDAO comDAO;
	
	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
	
	
	/**
    *
    * <pre>
    * 1. 메소드명 : selectSttsMedCrmList
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  통계관리 > 매체별 범죄유형별 접수/처리건수 현황
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	public List selectSttsMedCrmList(Map map) throws Exception{
		return sttsMedCrmTypeStateDAO.selectSttsMedCrmList(map);
	}
}

