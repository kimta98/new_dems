package dems.stts.medcrmtypestate.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("sttsMedCrmTypeStateDAO")
@SuppressWarnings({"rawtypes","unchecked"})
public class SttsMedCrmTypeStateDAO extends EgovComAbstractDAO {

	
   /**
	 *
	 * <pre>
	 * 1. 메소드명 : selectSttsMedCrmList
	 * 2. 작성일 : 2021. 4. 30.
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  통계관리 > 매체별 범죄유형별 접수/처리건수 현황
	 * </pre>
	 * @param map
	 * @param
	 * @param
	 * @return int
	 * @throws Exception
	 */
	public List selectSttsMedCrmList(Map map) throws Exception{
		return selectList("sttsMedCrmTypeStateDAO.selectSttsMedCrmList",map);
	}
	
	
}

