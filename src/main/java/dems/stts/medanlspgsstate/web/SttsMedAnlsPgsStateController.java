package dems.stts.medanlspgsstate.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.site.rcv.service.SiteRcvService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.fexception.MException;
import frame.futil.DemsConst;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;

/**
 * 
 * <pre>
 * 1. 클래스명 : SttsMedAnlsPgsStateController.java
 * 2. 작성일 : 2021. 4. 21.
 * 3. 작성자 : jij
 * 4. 설명 : @!@ 통계	매체별 분석 진행 현황
 * </pre>
 */
@Controller
public class SttsMedAnlsPgsStateController {

	protected Logger log = LoggerFactory.getLogger(SttsMedAnlsPgsStateController.class);
	
	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource MappingJackson2JsonView ajaxMainView;	
	
	 /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "siteRcvService")
	private SiteRcvService siteRcvService;
    
    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;
    
    /*@Resource(name="comController")
    ComController comController;    */

    /**
     * 
     * <pre>
     * 1. 메소드명 : indexSttsMedAnlsPgsStateQList
     * 2. 작성일 : 2021. 4. 20. 오후 2:34:43
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 매체별 분석 진행 현황 조회 화면 이동
     * </pre>
     * @param request
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/stts/medanlspgsstate/indexSttsMedAnlsPgsStateQList.do")
    public String indexSttsMedAnlsPgsStateQList(HttpServletRequest request, ModelMap model, SessionStatus status) throws Exception {

    	return "dems/stts/medanlspgsstate/sttsMedAnlsPgsStateQList";

    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : querySttsMedAnlsPgsStateQList
     * 2. 작성일 : 2021. 4. 21. 오후 12:12:11
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 매체별 분석 진행 현황 조회
     * </pre>
     * @param model
     * @param request
     * @param reqParams
     * @return
     * @throws Exception
     */
    @RequestMapping("/stts/medanlspgsstate/querySttsMedAnlsPgsStateQList.do")
	public ModelAndView querySttsMedAnlsPgsStateQList(ModelMap model,
			HttpServletRequest request, @RequestParam HashMap reqParams) throws Exception {
		 
		reqParams.put("sqlQueryId", "sttsMedAnlsPgsStateDAO.querySttsMedAnlsPgsStateQList");

		List list = comService.selectCommonQueryList(reqParams);

		reqParams.put("list", list);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);

	}
    
//    /**
//     * 
//     * <pre>
//     * 1. 메소드명 : indexSiteRcvRDtl
//     * 2. 작성일 : 2021. 4. 20. 오후 6:21:11
//     * 3. 작성자 : jij
//     * 4. 설명 : @!@ 현장지원 관리 등록 화면 이동 
//     * </pre>
//     * @param request
//     * @param model
//     * @param status
//     * @return
//     * @throws Exception
//     */
//    @RequestMapping(value="/site/rcv/indexSiteRcvRDtl.do")
//    public String indexSiteRcvRDtl(HttpServletRequest request, ModelMap model, SessionStatus status, @RequestParam HashMap<String, String> map) throws Exception {
//
//    	return "dems/site/rcv/siteRcvRDtl";
//    	
//    }
//    
//    
//    /**
//     * 
//     * <pre>
//     * 1. 메소드명 : reqSiteRcvRDtl
//     * 2. 작성일 : 2021. 4. 20. 오후 7:24:24
//     * 3. 작성자 : jij
//     * 4. 설명 : @!@ 현장지원 관리 등록
//     * </pre>
//     * @param request
//     * @param reqParams
//     * @param model
//     * @param status
//     * @return
//     * @throws Exception
//     */
//    @RequestMapping("/site/rcv/reqSiteRcvRDtl.do")
//    public ModelAndView reqSiteRcvRDtl(HttpServletRequest request, @RequestParam HashMap reqParams, ModelMap model,  SessionStatus status) throws Exception {
//
//    	HashMap<String,String> requireParams = new HashMap();
//    	requireParams.put("reqInsttCd", "요청부서");
//    	requireParams.put("prosrNm", "주임군검사");
//    	requireParams.put("incdtNm", "사건번호(사건명)");
//    	requireParams.put("reqUserNm", "담당자");
//    	if((reqParams.get("suppDgtlMchn") == null || reqParams.get("suppDgtlMchn").equals("")) &&
//    		(reqParams.get("suppData") == null || reqParams.get("suppData").equals("")) &&
//    		(reqParams.get("suppServer") == null || reqParams.get("suppServer").equals("")) &&
//    		(reqParams.get("suppEtc") == null || reqParams.get("suppEtc").equals(""))) {
//    		throw new MException(egovMessageSource.getMessageArgs("errors.required", new String[] {"지원구분(압수대상)"}));
//    	}
//    	requireParams.put("beforRead", "사전준비");
//    	requireParams.put("cfscSechPlndDt", "압수수색예정일");
//    	requireParams.put("cfscSechPlace", "압수, 수색 장소");
//    	requireParams.put("reqNum", "요청인원");
//    	requireParams.put("suppReqCnts", "지원요청내용");
//    	validateUtil.check(reqParams, requireParams);
//		  
//    	reqParams.put("sqlQueryId", "siteRcvDAO.reqSiteRcvRDtl");
//    	comService.updateCommonQuery(reqParams);
//
//    	if(reqParams.get("pgsStat").equals("C03002")){
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.site.rcv.003", null));
//    	}else{
//    		model.addAttribute(DemsConst.Messages_SysSucMessage,
//    				egovMessageSource.getMessageArgs("success.common.insert", null));
//    	}
//
//    	return new ModelAndView(ajaxMainView, model);
//
//    }
//    
//    /**
//     * 
//     * <pre>
//     * 1. 메소드명 : indexSiteRcvUDtl
//     * 2. 작성일 : 2021. 4. 21. 오전 10:58:51
//     * 3. 작성자 : jij
//     * 4. 설명 : @!@ 현장지원 관리 수정 화면 이동 
//     * </pre>
//     * @param request
//     * @param model
//     * @param status
//     * @return
//     * @throws Exception
//     */
//    @RequestMapping(value="/site/rcv/indexSiteRcvUDtl.do")
//    public String indexSiteRcvUDtl(HttpServletRequest request, ModelMap model, SessionStatus status) throws Exception {
//    	
//    	return "dems/site/rcv/siteRcvUDtl";
//    	
//    }
//    
//    /**
//     * 
//     * <pre>
//     * 1. 메소드명 : querySiteRcvMDtl
//     * 2. 작성일 : 2021. 4. 21. 오전 11:18:15
//     * 3. 작성자 : jij
//     * 4. 설명 : @!@ 현장지원 관리 상세 조회 
//     * </pre>
//     * @param map
//     * @param request
//     * @param model
//     * @return
//     * @throws Exception
//     */
//    @RequestMapping("/site/rcv/querySiteRcvMDtl.do")
//    public ModelAndView querySiteRcvMDtl(@RequestParam HashMap<String, String> map, HttpServletRequest request,
//    		ModelMap model) throws Exception {
//
//    	map.put("sqlQueryId", "siteRcvDAO.querySiteRcvMDtl");
//    	Map rtnMap = comService.selectCommonQueryMap(map);
//    	model.addAttribute("rtnMap", rtnMap);
//
//    	return new ModelAndView(ajaxMainView, model);
//
//    }
//
//    /**
//     * 
//     * <pre>
//     * 1. 메소드명 : updSiteRcvUDtl
//     * 2. 작성일 : 2021. 4. 21. 오전 10:59:12
//     * 3. 작성자 : jij
//     * 4. 설명 : @!@ 현장지원 관리 수정 
//     * </pre>
//     * @param request
//     * @param reqParams
//     * @param model
//     * @param status
//     * @return
//     * @throws Exception
//     */
//    @RequestMapping("/site/rcv/updSiteRcvUDtl.do")
//    public ModelAndView updSiteRcvUDtl(HttpServletRequest request, @RequestParam HashMap reqParams, ModelMap model,  SessionStatus status) throws Exception {
//
//    	reqParams.put("sqlQueryId", "siteRcvDAO.querySiteRcvPgsStatChk");
//    	String pgsStat = comService.selectCommonQueryString(reqParams);
//    	if(!pgsStat.equals("C03001") && !pgsStat.equals("C03002")){
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.site.rcv.000", new String[]{"저장"}));
//    		return new ModelAndView(ajaxMainView, model);
//    	}
//    	
//    	String suppReqSnoStr = "";
//    	if(!pgsStat.equals("C03001") && reqParams.get("pgsStat").equals("C03002")){
//    		suppReqSnoStr = (String) reqParams.get("suppReqSno");
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.site.rcv.002", new String[]{suppReqSnoStr, "임시저장"}));
//    		return new ModelAndView(ajaxMainView, model);
//    	}
//    	
//    	HashMap<String,String> requireParams = new HashMap();
//    	requireParams.put("reqInsttCd", "요청부서");
//    	requireParams.put("prosrNm", "주임군검사");
//    	requireParams.put("incdtNm", "사건번호(사건명)");
//    	requireParams.put("reqUserNm", "담당자");
//    	if((reqParams.get("suppDgtlMchn") == null || reqParams.get("suppDgtlMchn").equals("")) &&
//        		(reqParams.get("suppData") == null || reqParams.get("suppData").equals("")) &&
//        		(reqParams.get("suppServer") == null || reqParams.get("suppServer").equals("")) &&
//        		(reqParams.get("suppEtc") == null || reqParams.get("suppEtc").equals(""))) {
//        		throw new MException(egovMessageSource.getMessageArgs("errors.required", new String[] {"지원구분(압수대상)"}));
//        	}
//    	requireParams.put("beforRead", "사전준비");
//    	requireParams.put("cfscSechPlndDt", "압수수색예정일");
//    	requireParams.put("cfscSechPlace", "압수, 수색 장소");
//    	requireParams.put("reqNum", "요청인원");
//    	requireParams.put("suppReqCnts", "지원요청내용");
//    	validateUtil.check(reqParams, requireParams);
//    	
//    	reqParams.put("sqlQueryId", "siteRcvDAO.updSiteRcvUDtl");
//    	comService.updateCommonQuery(reqParams);
//
//    	if(reqParams.get("pgsStat").equals("C03002")){
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.site.rcv.003", null));
//    	}else{
//    		model.addAttribute(DemsConst.Messages_SysSucMessage,
//    				egovMessageSource.getMessageArgs("success.common.save", null));
//    	}
//
//    	return new ModelAndView(ajaxMainView, model);
//
//    }
//    
//    /**
//     * 
//     * <pre>
//     * 1. 메소드명 : delSiteRcvUDtl
//     * 2. 작성일 : 2021. 4. 21. 오전 11:17:02
//     * 3. 작성자 : jij
//     * 4. 설명 : @!@ 현장지원 관리 삭제
//     * </pre>
//     * @param map
//     * @param model
//     * @param request
//     * @param status
//     * @return
//     * @throws Exception
//     */
//    @RequestMapping("/site/rcv/delSiteRcvUDtl.do")
//    public ModelAndView delSiteRcvUDtl(@RequestParam HashMap<String, String> map, ModelMap model,
//    		HttpServletRequest request, SessionStatus status) throws Exception {
//
//    	map.put("sqlQueryId", "siteRcvDAO.querySiteRcvPgsStatChk");
//    	String pgsStat = comService.selectCommonQueryString(map);
//    	if(!pgsStat.equals("C03001") && !pgsStat.equals("C03002")){
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.site.rcv.000", new String[]{"삭제"}));
//    		return new ModelAndView(ajaxMainView, model);
//    	}
//
//    	map.put("sqlQueryId", "siteRcvDAO.delSiteRcvUDtl");
//    	comService.updateCommonQuery(map);
//
//    	model.addAttribute(DemsConst.Messages_SysSucMessage,
//    			egovMessageSource.getMessageArgs("success.common.delete", null));
//
//
//    	return new ModelAndView(ajaxMainView, model);
//
//    }
//    
//    /**
//     * 
//     * <pre>
//     * 1. 메소드명 : reqSiteRcvUDtl
//     * 2. 작성일 : 2021. 4. 30. 오후 2:28:30
//     * 3. 작성자 : jij
//     * 4. 설명 : @!@ 승인요청
//     * </pre>
//     * @param request
//     * @param model
//     * @param paramData
//     * @param response
//     * @return
//     * @throws Exception
//     */
//    @RequestMapping("/site/rcv/reqSiteRcvUDtl.do")
//    public ModelAndView reqSiteRcvUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {
//
//    	HashMap resultMap = siteRcvService.reqSiteRcvUDtl(paramData);
//    	
//    	String failList = (String) resultMap.get("failList");
//		String succList = (String) resultMap.get("succList");
//		String failMsg = (String) resultMap.get("failMsg");
//		String succMsg = (String) resultMap.get("succMsg");
//
//    	if((failList != "" && failList.length() > 0) && (succList == "" && succList.length() == 0)) {
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.site.rcv.002", new String[]{failList, failMsg}));
//    	}else if((failList != "" && failList.length() > 0) && (succList != "" && succList.length() > 0)){
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.site.rcv.007", new String[]{succMsg, failList, failMsg}));
//    	}else{
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//    				egovMessageSource.getMessageArgs("dems.site.rcv.001", new String[]{succMsg}));
//    	}
//
//    	return new ModelAndView(ajaxMainView, model);
//
//    }
//    
//    /**
//     * 
//     * <pre>
//     * 1. 메소드명 : reqCnclSiteRcvUDtl
//     * 2. 작성일 : 2021. 4. 30. 오후 12:43:56
//     * 3. 작성자 : jij
//     * 4. 설명 : @!@ 승인요청 취소
//     * </pre>
//     * @param request
//     * @param model
//     * @param paramData
//     * @param response
//     * @return
//     * @throws Exception
//     */
//    @RequestMapping("/site/rcv/reqCnclSiteRcvUDtl.do")
//    public ModelAndView reqCnclSiteRcvUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {
//
//    	HashMap resultMap = siteRcvService.reqCnclSiteRcvUDtl(paramData);
//    	
//    	String failList = (String) resultMap.get("failList");
//		String succList = (String) resultMap.get("succList");
//		String failMsg = (String) resultMap.get("failMsg");
//
//    	if((failList != "" && failList.length() > 0) && (succList == "" && succList.length() == 0)) {
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.site.rcv.002", new String[]{failList, failMsg}));
//    	}else if((failList != "" && failList.length() > 0) && (succList != "" && succList.length() > 0)){
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.site.rcv.007", new String[]{failMsg + "취소", failList, failMsg}));
//    	}else{
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//    				egovMessageSource.getMessageArgs("dems.site.rcv.001", new String[]{failMsg + "취소"}));
//    	}
//
//    	return new ModelAndView(ajaxMainView, model);
//
//    }
    
}	