package dems.stts.medTrtTrmAvgDay.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("sttsMedTrtTrmAvgDayDAO")
public class SttsMedTrtTrmAvgDayDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(SttsMedTrtTrmAvgDayDAO.class);
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectMedTrtTrmAvgDayList
	 * 2. 작성일 : 2021. 5. 27
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 매체별 처리기간 평균일수 목록 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List selectMedTrtTrmAvgDayList(Map map) throws Exception{
		return selectList("sttsMedTrtTrmAvgDayDAO.selectMedTrtTrmAvgDayList", map);
	}
	
}
