package dems.stts.medTrtTrmAvgDay.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import dems.stts.medTrtTrmAvgDay.service.SttsMedTrtTrmAvgDayService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import jdk.nashorn.internal.runtime.Undefined;

/**
 * <pre>
 * 1. 클래스명 : TralReqServiceImpl.java
 * 2. 작성일 : 2021. 4. 28.
 * 3. 작성자 : sjw7240
 * 4. 설명 : 
 * </pre>
 */
@Service("sttsMedTrtTrmAvgDayService")
public class SttsMedTrtTrmAvgDayServiceImpl extends EgovAbstractServiceImpl implements SttsMedTrtTrmAvgDayService{

	protected Logger log = LoggerFactory.getLogger(SttsMedTrtTrmAvgDayServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="comDAO")
	private ComDAO comDAO;

	@Resource(name="sttsMedTrtTrmAvgDayDAO")
	private SttsMedTrtTrmAvgDayDAO sttsMedTrtTrmAvgDayDAO;

	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Override
	public List selectMedTrtTrmAvgDayList(Map<String, Object> map) throws Exception {
		return (List)sttsMedTrtTrmAvgDayDAO.selectMedTrtTrmAvgDayList(map);
	}

	


	

}
