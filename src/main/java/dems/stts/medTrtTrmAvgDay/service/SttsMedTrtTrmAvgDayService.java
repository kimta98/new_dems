package dems.stts.medTrtTrmAvgDay.service;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 1. 클래스명 : SttsMedTrtTrmAvgDayService.java
 * 2. 작성일 : 2021. 5. 26.
 * 3. 작성자 : sjw7240
 * 4. 설명 : 
 * </pre>
 */
public interface SttsMedTrtTrmAvgDayService {
	
	List selectMedTrtTrmAvgDayList(Map<String,Object> map) throws Exception;
	
}
