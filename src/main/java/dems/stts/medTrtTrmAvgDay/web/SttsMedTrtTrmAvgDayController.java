package dems.stts.medTrtTrmAvgDay.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.stts.medTrtTrmAvgDay.service.SttsMedTrtTrmAvgDayService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.fexception.MException;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;

/**
 * <pre>
 * 1. 클래스명 : TralReqController.java
 * 2. 작성일 : 2021. 4. 28.
 * 3. 작성자 : sjw7240
 * 4. 설명 : 공판관리 요청관리 Controller
 * </pre>
 */
@Controller
public class SttsMedTrtTrmAvgDayController {

	protected Logger log = LoggerFactory.getLogger(SttsMedTrtTrmAvgDayController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "sttsMedTrtTrmAvgDayService")
	private SttsMedTrtTrmAvgDayService sttsMedTrtTrmAvgDayService;
    
    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;


    /**
     * 
     * <pre>
     * 1. 메소드명 : indexSttsMedTrtTrmAvgDayQList
     * 2. 작성일 : 2021. 5. 26.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 매체별 처리기간 평균일수 목록 진입
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/stts/medTrtTrmAvgDay/indexSttsMedTrtTrmAvgDayQList.do")
    public String indexSttsMedTrtTrmAvgDayQList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

        return "dems/stts/medTrtTrmAvgDay/sttsMedTrtTrmAvgDayQList";
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : querySttsMedTrtTrmAvgDayQList
     * 2. 작성일 : 2021. 5. 26.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 매체별 처리기간 평균일수 목록 호출
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/stts/medTrtTrmAvgDay/querySttsMedTrtTrmAvgDayQList.do")
    public ModelAndView querySttsMedTrtTrmAvgDayQList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
    	
        List selectMedTrtTrmAvgDayList = sttsMedTrtTrmAvgDayService.selectMedTrtTrmAvgDayList(reqParams);
    	
        reqParams.put("selectMedTrtTrmAvgDayList", selectMedTrtTrmAvgDayList);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);
    	   	
    }

   
  
  
}
