package dems.stts.meddgtlevdcstrgstate.web;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovDateUtil;

import dems.stts.meddgtlevdcstrgstate.service.SttsMedDgtlEvdcStrgStateService;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.fsys.user.service.FsysUserVO;
import frame.futil.DemsConst;
import frame.futil.DemsUtil;
import frame.futil.PageUtil;


/**
 * 
 * <pre>
 * 1. 클래스명 : SttsMedDgtlEvdcStrgStateController.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 통계관리
 * </pre>
 */
@Controller
public class SttsMedDgtlEvdcStrgStateController {

	protected Logger log = LoggerFactory.getLogger(SttsMedDgtlEvdcStrgStateController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;
	
	@Resource(name = "sttsMedDgtlEvdcStrgStateService")
	private SttsMedDgtlEvdcStrgStateService sttsMedDgtlEvdcStrgStateService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;

    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexSttsMedDgtlEvdcStrgStateQList
     * 2. 작성일 : 2021. 4. 16. 오전 10:49:11
     * 3. 작성자 : ilyong
     * 4. 설명 : 통계관리 > 매체별 디지털증거 보존현황 진입
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/stts/meddgtlevdcstrgstate/indexSttsMedDgtlEvdcStrgStateQList.do")
    public String indexSttsMedDgtlEvdcStrgStateQList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {
    	
    	model.addAttribute("message", request.getParameter("message"));
    	if(request.getParameter("msg") != null){
    		model.addAttribute("msg", request.getParameter("msg"));
    	}
    	return "dems/stts/meddgtlevdcstrgstate/sttsMedDgtlEvdcStrgStateQList";
    }


	/**
     * 
     * <pre>
     * 1. 메소드명 : querySttsMedDgtlEvdcStrgStateQList
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 통계관리 > 매체별 디지털증거 보존현황 조회
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/stts/meddgtlevdcstrgstate/querySttsMedDgtlEvdcStrgStateQList.do")
    public ModelAndView querySttsMedDgtlEvdcStrgStateQList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
    	//sessionInsttCd
    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
//    	log.debug("=======queryEvdcAplnMList schPgsStat  split========>>>"+ ((String) reqParams.get("schPgsStat")).split(","));
//    	reqParams.put("sessionUserGb", user.getUserGb());
//    	reqParams.put("sessionInsttCd", user.getInsttCd());
//    	reqParams.put("schPgsStat", ((String) reqParams.get("schPgsStat")).split(","));
//    	
//    	
//    	log.debug("=======queryEvdcAplnMList sessionUserGb========>>>"+user.getUserGb());
//    	log.debug("=======queryEvdcAplnMList sessionInsttCd========>>>"+user.getInsttCd());
//    	
//    	reqParams.put("sqlQueryId", "evdcAplnDAO.queryEvdcAplnMListTotCnt"); 
//    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
//    	reqParams.put("totalCount", totCnt);
//    	
//    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		
		List list = sttsMedDgtlEvdcStrgStateService.selectSttsMedDgtlList(reqParams);
		
//		log.debug("=========queryEvdcTrtMList list.size() ========>>>"+list.size());
//		
//		for(int i=0;i<list.size();i++) {
//			log.debug("=========queryEvdcTrtMList list["+i+"]=========>>>"+list.get(i));
//		}
		
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
   	  
}
