package dems.stts.meddgtlevdcstrgstate.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import frame.flyt.login.service.FLytLoginVO;



/**
 * 
 * <pre>
 * 1. 클래스명 : SttsMedDgtlEvdcStrgStateService.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 통계관리
 * </pre>
 */
@SuppressWarnings("rawtypes")
public interface SttsMedDgtlEvdcStrgStateService {


	/**
	 * 
	 * <pre>
     * 1. 메소드명 : selectSttsMedDgtlList
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  통계관리 > 매체별 디지털증거 보존현황 조회
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	List selectSttsMedDgtlList(Map map) throws Exception;
	
	
}

