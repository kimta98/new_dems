package dems.stts.meddgtlevdcstrgstate.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("sttsMedDgtlEvdcStrgStateDAO")
@SuppressWarnings({"rawtypes","unchecked"})
public class SttsMedDgtlEvdcStrgStateDAO extends EgovComAbstractDAO {

	
   /**
	 *
	 * <pre>
	 * 1. 메소드명 : selectSttsMedDgtlList
	 * 2. 작성일 : 2021. 4. 30.
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  통계관리 > 매체별 디지털증거 보존현황 조회
	 * </pre>
	 * @param map
	 * @param
	 * @param
	 * @return int
	 * @throws Exception
	 */
	public List selectSttsMedDgtlList(Map map) throws Exception{
		return selectList("sttsMedDgtlEvdcStrgStateDAO.selectSttsMedDgtlList",map);
	}
	
	
}

