package dems.stts.meddgtlevdcstrgstate.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import dems.bbs.free.service.impl.BbsFreeServiceImpl;
import dems.stts.meddgtlevdcstrgstate.service.SttsMedDgtlEvdcStrgStateService;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;


/**
 * 
 * <pre>
 * 1. 클래스명 : SttsMedDgtlEvdcStrgStateImpl.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 통계관리
 * </pre>
 */
@Service("sttsMedDgtlEvdcStrgStateService")
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class SttsMedDgtlEvdcStrgStateImpl extends EgovAbstractServiceImpl implements	SttsMedDgtlEvdcStrgStateService{
	
	protected Logger log = LoggerFactory.getLogger(SttsMedDgtlEvdcStrgStateImpl.class);
	
	@Resource(name="sttsMedDgtlEvdcStrgStateDAO")
	private SttsMedDgtlEvdcStrgStateDAO sttsMedDgtlEvdcStrgStateDAO;
	
	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="comDAO")
	private ComDAO comDAO;
	
	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
	
	
	/**
    *
    * <pre>
    * 1. 메소드명 : selectSttsMedDgtlList
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  통계관리 > 매체별 디지털증거 보존현황 조회
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	public List selectSttsMedDgtlList(Map map) throws Exception{
		return sttsMedDgtlEvdcStrgStateDAO.selectSttsMedDgtlList(map);
	}
}

