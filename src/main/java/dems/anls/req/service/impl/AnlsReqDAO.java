/**
 *
 * <pre>
 * 1. 클래스명 : AnlsReqDAO.java
 * 2. 작성일 : 2021. 4. 15.
 * 3. 작성자 : Leeji
 * 4. 설명 : AnlsReqDAO
 * </pre>
 *
 */
package dems.anls.req.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;


@Repository("anlsReqDAO")
public class AnlsReqDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(AnlsReqDAO.class);

    /**
     *
     * <pre>
     * 1. 메소드명 : selectUserGb
     * 2. 작성일 : 2021. 4. 21.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청 요청관리 사용자그룹 조회
     * </pre>
     * @param map
     * @return String
     * @throws Exception
     */
	public String selectAnlsReqUserGb(Map map) throws Exception{
		return (String)selectOne("anlsReqDAO.selectAnlsReqUserGb", map);
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : selectAnlsReqIncdtChk
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 사건 우뮤 체크
    * </pre>
    * @param map
    * @return int
    * @throws Exception
    */
	public int selectAnlsReqIncdtChk(Map map) throws Exception{
		return selectOne("anlsReqDAO.selectAnlsReqIncdtChk", map);
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : selectAnalReqSno
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 분석요청일련번호 조회
    * </pre>
    * @param map
    * @return int
    * @throws Exception
    */
	public String selectAnalReqSno(Map map) throws Exception{
		return selectOne("anlsReqDAO.selectAnalReqSno", map);
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : insertAnalysisSuppReq
     * 2. 작성일 : 2021. 4. 21.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청 요청관리 분석지원요청정보 insert
     * </pre>
     * @param map
     * @return int
     * @throws Exception
     */
    public void insertAnalysisSuppReq(Map<String, Object> paramMap) throws Exception {

    	insert("anlsReqDAO.insertAnalysisSuppReq", paramMap);
    }

    /**
    *
    * <pre>
    * 1. 메소드명 : insertAnalysisEvdcDtl
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 분석대상상세 insert
    * </pre>
    * @param map
    * @return int
    * @throws Exception
    */
   public void insertAnalysisEvdcDtl(Map<String, Object> paramMap) throws Exception {

   	 	insert("anlsReqDAO.insertAnalysisEvdcDtl", paramMap);
   }
   
   /**
   *
   * <pre>
   * 1. 메소드명 : selectAnlsReqListCnt
   * 2. 작성일 : 2021. 4. 22.
   * 3. 작성자 : leeji
   * 4. 설명 :  분석지원요청 요청관리 리스트 카운트
   * </pre>
   * @param map
   * @return int
   * @throws Exception
   */
	public int selectAnlsReqListCnt(Map map) throws Exception{
		return selectOne("anlsReqDAO.selectAnlsReqListCnt", map);
	}
	
 /**
   *
   * <pre>
   * 1. 메소드명 : selectAnlsReqList
   * 2. 작성일 : 2021. 4. 22.
   * 3. 작성자 : leeji
   * 4. 설명 :  분석지원요청 요청관리 리스트 조회
   * </pre>
   * @param map
   * @return List
   * @throws Exception
   */
	public List selectAnlsReqList(Map map) throws Exception{
		return selectList("anlsReqDAO.selectAnlsReqList", map);
	}
	
	/**
     * 
     * <pre>
     * 1. 메소드명 : updateAnlsReqPgsStat
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청관리 진행상태 변경
     * </pre>
     * @param map
     * @return void
     * @throws Exception
     */
    public void updateAnlsReqPgsStat(Map<String, Object> paramMap) throws Exception {
    	update("anlsReqDAO.updateAnlsReqPgsStat", paramMap);
    }
    
    /**
    *
    * <pre>
    * 1. 메소드명 : selectAnalysisSuppReqChk
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 요청지원 유무 체크
    * </pre>
    * @param map
    * @return int
    * @throws Exception
    */
 	public int selectAnalysisSuppReqChk(Map map) throws Exception{
 		return selectOne("anlsReqDAO.selectAnalysisSuppReqChk", map);
 	}
 	
 	/**
    *
    * <pre>
    * 1. 메소드명 : selectReqInsttCd
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 요청기관코드 조회
    * </pre>
    * @param map
    * @return String
    * @throws Exception
    */
 	public String selectReqInsttCd(Map map) throws Exception{
 		return selectOne("anlsReqDAO.selectReqInsttCd", map);
 	}
 	
 	/**
    *
    * <pre>
    * 1. 메소드명 : selectAnlsReqListCnt
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 진행상태 조회
    * </pre>
    * @param map
    * @return String
    * @throws Exception
    */
 	public String selectPgsStat(Map map) throws Exception{
 		return selectOne("anlsReqDAO.selectPgsStat", map);
 	}
 	
 	/**
    *
    * <pre>
    * 1. 메소드명 : deleteAnalysisEvdcDtlList
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 분석대상리스트 삭제
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
 	public void deleteAnalysisEvdcDtlList(Map map) throws Exception{
 		delete("anlsReqDAO.deleteAnalysisEvdcDtlList", map);
 	}
 	
 	/**
    *
    * <pre>
    * 1. 메소드명 : deleteAnlsReqInfo
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 삭제
    * </pre>
    * @param map
    * @return int
    * @throws Exception
    */
 	public void deleteAnlsReqInfo(Map map) throws Exception{
 		delete("anlsReqDAO.deleteAnlsReqInfo", map);
 	}
 	
 	/**
     * 
     * <pre>
     * 1. 메소드명 : updateAnalysisSuppReq
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청 요청관리 변경
     * </pre>
     * @param map
     * @return void
     * @throws Exception
     */
    public void updateAnalysisSuppReq(Map<String, Object> paramMap) throws Exception {
    	update("anlsReqDAO.updateAnalysisSuppReq", paramMap);
    }

}