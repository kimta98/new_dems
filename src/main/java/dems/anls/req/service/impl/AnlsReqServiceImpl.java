package dems.anls.req.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import dems.anls.pgs.service.impl.AnlsPgsDAO;
import dems.anls.req.service.AnlsReqService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovDateUtil;
import egovframework.rte.fdl.string.EgovStringUtil;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import frame.futil.WebsocketWork;


@Service("anlsReqService")
public class AnlsReqServiceImpl extends EgovAbstractServiceImpl implements AnlsReqService{

	protected Logger log = LoggerFactory.getLogger(AnlsReqServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="comDAO")
	private ComDAO comDAO;

	@Resource(name="anlsReqDAO")
	private AnlsReqDAO anlsReqDAO;
	
	@Resource(name="anlsPgsDAO")
	private AnlsPgsDAO anlsPgsDAO;

	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	/**
    *
    * <pre>
    * 1. 메소드명 : insertAnlsReq
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 insert
    * </pre>
    * @param map
    * @return map
    * @throws Exception
    */
	@Override
	public Map<String,Object> insertAnlsReq(Map<String, Object> map) throws Exception {

		Map<String,Object> paramMap = new HashMap<String,Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		validateUtil.check(chkParamMap, requireParams);

		//서버에서
		String userGb = (String) map.get("userGb");
		String regrId = (String) map.get("userId");
		
		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		String type = (String) paramMap.get("type");

		//권한이 일반수사관(C01001) / 포렌식수사관(C01003) 20211203 추가
		if(!"C01001".equals(userGb) && !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000",new String[]{"일반수사관"}));
		}

        //사건이 존재하는지 체크
		paramMap.put("incdtSno"	, paramMap.get("incdtSno"));
		int chkIncdt =  anlsReqDAO.selectAnlsReqIncdtChk(paramMap);
		if(chkIncdt < 1) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.001",new String[]{"해당 사건"}));
		}

		//분석요청일련번호 조회
		paramMap.put("incdtSno"	, paramMap.get("incdtSno"));
		String analReqSno = anlsReqDAO.selectAnalReqSno(paramMap);


		//DEMS_ANALYSIS_SUPP_REQ insert
		paramMap.clear();
		paramMap.put("analReqSno", analReqSno);
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));

		/*진행상태
		C03001:임시등록
		C03002:승인요청
		C03003:검사승인
		C03004:반려
		C03005:문서접수
		C03006:과장승인*/
		if("C01001".equals(userGb)) {
			paramMap.put("pgsStat","C03001");
		}else if("C01003".equals(userGb)) {
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			paramMap.put("pgsStat","C03003");
			paramMap.put("prosrAprvDt", format.format(new Date()));
		}
		paramMap.put("analReqSno", analReqSno);
		paramMap.put("regrId", regrId);
		anlsReqDAO.insertAnalysisSuppReq(paramMap);
		
		WebsocketWork wscw = new WebsocketWork();
		wscw.broadCast("myChart");

		//DEMS_ANALYSIS_EVDC_DTL insert
		ArrayList<Map<String, Object>> paramDetailList = (ArrayList<Map<String, Object>>) map.get("detailList");
		for(int i =0; i < paramDetailList.size(); i++) {
			paramMap.clear();
			paramMap.putAll(paramDetailList.get(i));
			paramMap.put("analReqSno", analReqSno);
			anlsReqDAO.insertAnalysisEvdcDtl(paramMap);
		}
		
		//승인요청만 진행
		if("aprv".equals(type)) {
			paramMap.clear();
			paramMap.put("analReqSno", analReqSno);
			paramMap.put("pgsStat", "C03002");
			paramMap.put("type", type);
			paramMap.put("updrId", regrId);			
			anlsReqDAO.updateAnlsReqPgsStat(paramMap);
		}
		
		Map<String ,Object> returnMap = new HashMap<String,Object>(); 
		returnMap.put("analReqSno", analReqSno);
		
		return returnMap;

	}

	/**
    *
    * <pre>
    * 1. 메소드명 : selectAnlsReqList
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 리스트 조회
    * </pre>
    * @param map
    * @return List
    * @throws Exception
    */
	@Override
	public List selectAnlsReqList(Map<String, Object> map) throws Exception {
		
		return anlsReqDAO.selectAnlsReqList(map);
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : selectAnlsReqListCnt
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 리스트 카운트
    * </pre>
    * @param map
    * @return int
    * @throws Exception
    */
	@Override
	public int selectAnlsReqListCnt(Map<String, Object> map) throws Exception {
		
		return anlsReqDAO.selectAnlsReqListCnt(map);
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : updatePgsStat
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 진행상태 변경(승인요청,승인취소,검사승인,검사승인취소)
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void updatePgsStat(Map<String, Object> map) throws Exception {
		
		
		String type = (String) map.get("type");
		List aprvList = (List) map.get("aprvList");
		String userGb = (String) map.get("userGb");
		String insttCd = (String) map.get("insttCd");
		String userId = (String) map.get("userId");	
		
		Map<String, Object> infoMap = new HashMap<String, Object>();
		
		//요청관리에서 넘어오는 상태 : 승인요청,승인취소,검사승인,검사승인취소,선별확인,선별확인취소만 처리
		if(!("aprv".equals(type) || "aprvCncl".equals(type) || "prosrAprv".equals(type) || "prosrAprvCncl".equals(type) || "selCfrm".equals(type) || "selCfrmCncl".equals(type))) {
			throw new MException(egovMessageSource.getMessage("dems.anls.req.005"));
		}
		
		switch (type) {
		case "aprv"://승인요청
			infoMap.put("type", type);
			infoMap.put("pgsStat", "C03002");
			infoMap.put("updrId", userId);
			break;
		case "aprvCncl": //승인요청취소(임시저장으로변경)
			infoMap.put("type", type);
			infoMap.put("pgsStat", "C03001");
			infoMap.put("updrId", userId);	
			break;
		case "prosrAprv"://검사승인
			infoMap.put("type", type);
			infoMap.put("prosrId", userId);
			infoMap.put("pgsStat", "C03003");
			infoMap.put("updrId", userId);			
			break;
		case "prosrAprvCncl"://검사승인취소(승인요청으로변경)
			infoMap.put("type", type);
			infoMap.put("pgsStat", "C03002");
			infoMap.put("updrId", userId);
			infoMap.put("prosrId", userId);
			break;
		case "selCfrm"://선별확인
			infoMap.put("type", type);
			infoMap.put("pgsStat", "C03008");
			infoMap.put("updrId", userId);
			infoMap.put("selCfrmCrgrId", userId);
			break;
		case "selCfrmCncl"://선별확인취소(선별요청으로변경)
			infoMap.put("type", type);
			infoMap.put("pgsStat", "C03007");
			infoMap.put("updrId", userId);
			break;
		}
		
		
		//권한체크
		if(("aprv".equals(type) || "aprvCncl".equals(type) || "selCfrm".equals(type) || "selCfrmCncl".equals(type)) && (!"C01001".equals(userGb) && !"C01003".equals(userGb))) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"일반수사관"}));
		}
		
		if(("prosrAprv".equals(type) || "prosrAprvCncl".equals(type)) && !"C01002".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"군검사"}));
		}
		
		
		int chkAprv = 0;
		String reqInsttCd = "";
		String pgsStat = "";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();		
		
		for(int i = 0; i < aprvList.size(); i++) {
			
			paramMap.clear();
			paramMap.putAll(infoMap);
			paramMap.put("analReqSno", aprvList.get(i));
			
			//요청자료 유무 체크
			chkAprv = anlsReqDAO.selectAnalysisSuppReqChk(paramMap);
			if(chkAprv < 1) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.002",new String[]{(String) aprvList.get(i)}));
			}
			
			//요청관련 동일부서인지 체크		
			reqInsttCd = anlsReqDAO.selectReqInsttCd(paramMap);
			
			if(!reqInsttCd.equals(insttCd)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.003",new String[]{(String) aprvList.get(i)}));
			}
			
			//진행상태 체크
			pgsStat = anlsReqDAO.selectPgsStat(paramMap);
			
			//승인요청     aprv           -> 임시저장
			//승인요청취소 aprvCanel      -> 승인요청
			//검사승인     prosrAprv      -> 승인요청
			//검사승인취소 prosrAprvCanel -> 검사승인
			//선별확인     selCfrm        -> 선별요청
			//선별확인취소 selCfrmCncl    -> 선별확인
			
			if("aprv".equals(type) && !"C03001".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(i),"임시저장"}));
			}
			
			if(("aprvCanel".equals(type) || "prosrAprv".equals(type) )&& !"C03002".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(i),"승인요청"}));
			}			
			
			if("prosrAprvCanel".equals(type) && !"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(i),"검사승인"}));
			}
			
			if("selCfrm".equals(type) && !"C03007".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(i),"선별요청"}));
			}
			
			if("selCfrmCncl".equals(type) && !"C03008".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(i),"선별확인"}));
			}
			
		}
		
		//진행상태 변경
		for(int i = 0; i < aprvList.size(); i++) {
			
			paramMap.clear();
			paramMap.putAll(infoMap);
			paramMap.put("analReqSno", aprvList.get(i));
			anlsReqDAO.updateAnlsReqPgsStat(paramMap);
			
		}
		
		if(type.equals("prosrAprv") || type.equals("prosrAprvCncl")){//검사승인 //검사승인취소(승인요청으로변경)
			WebsocketWork wscw = new WebsocketWork();
			wscw.broadCast("newReq");
		}
		
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : deleteAnlsReqUDtl
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 분석지원요청 삭제
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void deleteAnlsReqUDtl(Map<String, Object> map) throws Exception {
		
		//동일부서인지 체크	
		String reqInsttCd = anlsReqDAO.selectReqInsttCd(map);
		
		if( !reqInsttCd.equals(map.get("insttCd")) ) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.003",new String[]{(String) map.get("analReqSno")}));
		}
		
		//권한이 일반수사관(C01001)
		if(!"C01001".equals(map.get("userGb")) && !"C01003".equals(map.get("userGb"))) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"일반수사관"}));
		}
		
		//포렌식수사관이 아닐 경우
		if(!"C01003".equals(map.get("userGb"))) {
			//진행상태 체크(임시저장:C03001)
			String pgsStat = anlsReqDAO.selectPgsStat(map);
			if(!"C03001".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000",new String[]{"진행상태가 임시저장"}));
			}
		}else {
			String pgsStat = anlsReqDAO.selectPgsStat(map);
			if(!"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000",new String[]{"진행상태가 검사승인"}));
			}
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		//첨부파일 삭제 
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsReqDAO.selectAnalReqFileId");
		paramMap.put("analReqSno", map.get("analReqSno"));
		String atchFileId = comDAO.selectCommonQueryString(paramMap);
		
		//첨부파일 유무 확인
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsPgsDAO.selectReqFileCnt");
		paramMap.put("atchFileId", atchFileId);
		int fileCnt = comDAO.selectCommonQueryListTotCnt(paramMap);
		
		if(fileCnt > 0) {
			//DEMS_ATCH_FILE_DTL update
			paramMap.clear();
			paramMap.put("loginId",  map.get("userId"));			
			paramMap.put("atchFileId", atchFileId);
			paramMap.put("sqlQueryId", "anlsPgsDAO.deleteAtchFileDtl");
			comDAO.updateCommonQuery(paramMap);		
			//DEMS_ATCH_FILE update
			paramMap.put("sqlQueryId", "anlsPgsDAO.deleteAtchFile");
			comDAO.updateCommonQuery(paramMap);		
		}
		
		//삭제
		anlsReqDAO.deleteAnalysisEvdcDtlList(map);
		anlsReqDAO.deleteAnlsReqInfo(map);

		
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : updateAnlsReq
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : leeji
    * 4. 설명 :   분석지원요청 요청관리 update
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void updateAnlsReq(Map<String, Object> map) throws Exception {
		Map<String,Object> paramMap = new HashMap<String,Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		validateUtil.check(chkParamMap, requireParams);

		//서버에서
		String userGb = (String) map.get("userGb");
		String updrId = (String) map.get("userId");
		
		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		String analReqSno = (String) paramMap.get("analReqSno"); 
		

		//권한이 일반수사관(C01001)
		if(!"C01001".equals(userGb) && !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000",new String[]{"일반수사관"}));
		}

		//동일부서인지 체크	
		paramMap.clear();
		paramMap.put("analReqSno", analReqSno);
		String reqInsttCd = anlsReqDAO.selectReqInsttCd(paramMap);		
		
		if( !reqInsttCd.equals(map.get("insttCd")) ) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.003",new String[]{analReqSno}));
		}		
		
		//포렌식수사관이 아닐 경우
		if(!"C01003".equals(userGb)) {
			//진행상태 체크(임시저장:C03001)
			String pgsStat = anlsReqDAO.selectPgsStat(paramMap);
			if(!"C03001".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000",new String[]{"진행상태가 임시저장"}));
			}
		}else {
			String pgsStat = anlsReqDAO.selectPgsStat(paramMap);
			if(!"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000",new String[]{"진행상태가 검사승인"}));
			}
		}
		

		//DEMS_ANALYSIS_EVDC_DTL delete
		paramMap.clear();
		paramMap.put("analReqSno", analReqSno);
		anlsReqDAO.deleteAnalysisEvdcDtlList(paramMap);
		
		//DEMS_ANALYSIS_EVDC_DTL insert
		ArrayList<Map<String, Object>> paramDetailList = (ArrayList<Map<String, Object>>) map.get("detailList");
		for(int i =0; i < paramDetailList.size(); i++) {
			paramMap.clear();
			paramMap.putAll(paramDetailList.get(i));
			paramMap.put("analReqSno", analReqSno);
			anlsReqDAO.insertAnalysisEvdcDtl(paramMap);
		}
		//DEMS_ANALYSIS_SUPP_REQ update

		/*진행상태
		C03001:임시등록
		C03002:승인요청
		C03003:검사승인
		C03004:반려
		C03005:문서접수
		C03006:과장승인*/

		paramMap.clear();
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		if(!"C01003".equals(userGb)) {
			paramMap.put("pgsStat","C03001");
		}else {
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			paramMap.put("pgsStat","C03003");
			paramMap.put("prosrAprvDt", format.format(new Date()));
		}
		paramMap.put("updrId", updrId);
		anlsReqDAO.updateAnalysisSuppReq(paramMap);

		
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : procAnlsReqFile
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 첨부파일 처리
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void procAnlsReqFile(Map<String, Object> map) throws Exception {
		
		Map<String,Object> paramMap = new HashMap<String, Object>();
		ArrayList<Map<String, Object>> insFileList = (ArrayList<Map<String, Object>>) map.get("insFileList");
		ArrayList<Map<String, Object>> delFileList = (ArrayList<Map<String, Object>>) map.get("delFileList");
		Map<String, Object> anlsReqInfo = (Map<String, Object>) map.get("anlsReqInfo");
		String atchFileSeq = "";
		
		//분석지원요청 파일ID조회
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsReqDAO.selectAnalReqFileId");
		paramMap.put("analReqSno",anlsReqInfo.get("analReqSno"));
		String atchFileId = comDAO.selectCommonQueryString(paramMap);
		
		//등록,수정시
		if(insFileList.size() > 0) {
			
			//등록이거나 //수정인데 파일첨부를 처음하는경우
			if("ins".equals(anlsReqInfo.get("type")) || ("upd".equals(anlsReqInfo.get("type")) && EgovStringUtil.isEmpty(atchFileId))) {
				
				//첨부파일 시퀀스 조회			
				paramMap.clear();
				paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalysisRsltAtchFileSeq");
				atchFileSeq = comDAO.selectCommonQueryString(paramMap);
				
				//첨부파일id를 DEMS_ANALYSIS_SUPP_REQ update		
				paramMap.clear();
				paramMap.put("analReqSno", anlsReqInfo.get("analReqSno"));
				paramMap.put("updrId",  map.get("userId"));
				paramMap.put("atchFileId", atchFileSeq);
				paramMap.put("sqlQueryId", "anlsReqDAO.updateAnalysisSuppReqFileId");
				comDAO.updateCommonQuery(paramMap);
				
				//DEMS_ATCH_FILE insert
				paramMap.put("loginId",  map.get("userId"));			
				anlsPgsDAO.insertAtchFile(paramMap);
				
			} else {
				atchFileSeq = (String) anlsReqInfo.get("atchFileId");
			}

			//DEMS_ATCH_FILE_DTL insert
			for(int i = 0; i < insFileList.size(); i++) {
				paramMap.clear();
				paramMap.put("atchFileId",  atchFileSeq);
				paramMap.put("loginId",  map.get("userId"));
				paramMap.putAll(insFileList.get(i));
				anlsPgsDAO.insertAtchFileDtl(paramMap);	
			}
			
			
		}
		
		//삭제시
		if(delFileList.size() > 0) {
			//DEMS_ATCH_FILE_DTL update
			for(int i = 0; i < delFileList.size(); i++) {
				paramMap.clear();
				paramMap.put("atchFileId", anlsReqInfo.get("atchFileId"));
				paramMap.put("fileSno", anlsReqInfo.get("fileSno"));
				paramMap.put("loginId",  map.get("userId"));
				paramMap.put("sqlQueryId", "anlsPgsDAO.deleteAtchFileSnoDtl");
				comDAO.updateCommonQuery(paramMap);		
			}
			
			//분석지원요청관리 첨부파일 유무(DEMS_ATCH_FILE_DTL)
			paramMap.clear();
			paramMap.put("sqlQueryId", "anlsPgsDAO.selectReqFileCnt");
			paramMap.put("atchFileId", anlsReqInfo.get("atchFileId"));
			int fileCnt = comDAO.selectCommonQueryListTotCnt(paramMap);
			
			if(fileCnt < 1) {
				//DEMS_ATCH_FILE update
				paramMap.clear();
				paramMap.put("loginId",  map.get("userId"));
				paramMap.put("atchFileId", anlsReqInfo.get("atchFileId"));
				paramMap.put("sqlQueryId", "anlsPgsDAO.deleteAtchFile");
				comDAO.updateCommonQuery(paramMap);
				
				//첨부파일id를 DEMS_ANALYSIS_SUPP_REQ update		
				paramMap.clear();
				paramMap.put("analReqSno", anlsReqInfo.get("analReqSno"));
				paramMap.put("atchFileId", null);
				paramMap.put("sqlQueryId", "anlsReqDAO.updateAnalysisSuppReqFileId");
				comDAO.updateCommonQuery(paramMap);	
			}
			
		}
		

	}


}