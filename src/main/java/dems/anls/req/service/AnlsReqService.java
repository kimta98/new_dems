/**
 *
 * <pre>
 * 1. 클래스명 : AnlsReqService.java
 * 2. 작성일 : 2021. 4. 15.
 * 3. 작성자 : Leeji
 * 4. 설명 : 분석지원요청 Service
 * </pre>
 *
 */
package dems.anls.req.service;

import java.util.List;
import java.util.Map;


public interface AnlsReqService {

	Map<String,Object> insertAnlsReq(Map<String,Object> map) throws Exception;
	
	List selectAnlsReqList(Map<String,Object> map) throws Exception;
	
	int selectAnlsReqListCnt(Map<String,Object> map) throws Exception;
	
	void updatePgsStat(Map<String,Object> map) throws Exception;	
	
	void deleteAnlsReqUDtl(Map<String,Object> map) throws Exception;
	
	void updateAnlsReq(Map<String,Object> map) throws Exception;
	
	void procAnlsReqFile(Map<String,Object> map) throws Exception;	

}