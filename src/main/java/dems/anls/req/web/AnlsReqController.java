package dems.anls.req.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.anls.req.service.AnlsReqService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.JetcoUserIpService;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.PageUtil;

/**
 * <pre>
 * 1. 클래스명 : AnlsReqController.java
 * 2. 작성일 : 2021. 4. 20.
 * 3. 작성자 : leeji
 * 4. 설명 : 분석지원요청 요청관리 Controller
 * </pre>
 */
@Controller
public class AnlsReqController {

	protected Logger log = LoggerFactory.getLogger(AnlsReqController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "anlsReqService")
	private AnlsReqService anlsReqService;
    
    @Resource(name = "jetcoUserIpService")
    private JetcoUserIpService jetcoUserIpService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;


    /**
     *
     * <pre>
     * 1. 메소드명 : indexAnlsReqRDtl
     * 2. 작성일 : 2021. 4. 20.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청 요청관리 목록페이지 진입시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return String
     * @throws Exception
     */
    @RequestMapping(value="/anls/req/indexAnlsReqMList.do")
    public String indexAnlsReqMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

        return "dems/anls/req/anlsReqMList";
    }

    /**
     *
     * <pre>
     * 1. 메소드명 : queryAnlsReqMList
     * 2. 작성일 : 2021. 4. 22.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청 요청관리 목록 조회시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return ModelAndView
     * @throws Exception
     */
    @RequestMapping(value="/anls/req/queryAnlsReqMList.do")
    public ModelAndView queryAnlsReqMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	reqParams.put("userGb", user.getUserGb());
    	reqParams.put("regrId", user.getUserId());
    	reqParams.put("insttCd", user.getInsttCd());
    	reqParams.put("searchPgsStat", ((String) reqParams.get("searchPgsStat")).split(","));

    	int totCnt = anlsReqService.selectAnlsReqListCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	PageUtil.calcPage(reqParams);

    	List anlsReqList = anlsReqService.selectAnlsReqList(reqParams);
		reqParams.put("anlsReqList", anlsReqList);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);

    }

    /**
    *
    * <pre>
    * 1. 메소드명 : queryPgsStat
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 진행상태 변경(승인요청,승인취소,검사승인,검사승인취소,선별확인)
    * </pre>
    * @param paramData
    * @param model
    * @param request
    * @return ModelAndView
    * @throws Exception
    */
   @RequestMapping(value="/anls/req/queryPgsStat.do")
   public ModelAndView queryPgsStat(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

	   ObjectMapper mapper = new ObjectMapper();
	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

	   //요청 리스트
	   List tpList = (List) jsonObject.get("rowDatas");

	   //요청 타입(승인요청,승인요청취소,검사승인,검사승인취소,선별확인,선별확인취소)
	   Map<String, Object> infoMap = jsonObject.get("type");
	   String type = (String) infoMap.get("type");

	   Map<String, Object> paramMap = new HashMap<String, Object>();
	   paramMap.put("aprvList", tpList);
	   paramMap.put("userGb", user.getUserGb());
	   paramMap.put("userId", user.getUserId());
	   paramMap.put("insttCd", user.getInsttCd());
	   paramMap.put("type", type);

	   anlsReqService.updatePgsStat(paramMap);

	   return new ModelAndView(ajaxMainView, model);

   }

    /**
     *
     * <pre>
     * 1. 메소드명 : indexAnlsReqRDtl
     * 2. 작성일 : 2021. 4. 20.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청 요청관리 등록페이지 이동
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return String
     * @throws Exception
     */
    @RequestMapping(value="/anls/req/indexAnlsReqRDtl.do")
    public String indexAnlsReqRDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

       FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

 	   model.addAttribute("reqInsttNm", user.getInsttNm());    //요청기관명
 	   model.addAttribute("reqDepNm", user.getDeptNm());      //요청부서명
 	   model.addAttribute("reqInsttCd", user.getInsttCd());  //요청기관코드

        return "dems/anls/req/anlsReqRDtl";
    }

    /**
     *
     * <pre>
     * 1. 메소드명 : reqAnlsReqRDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청 요청관리 등록시 호출
     * </pre>
     * @param paramData
     * @param model
     * @param request
     * @return ModelAndView
     * @throws Exception
     */
    @RequestMapping(value="/anls/req/reqAnlsReqRDtl.do")
    public ModelAndView reqAnlsReqRDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	ObjectMapper mapper = new ObjectMapper();
    	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

    	//분석대상 상세정보
    	ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
    	//분석지원요청정보
    	Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");

    	Map<String, Object> paramMap = new HashMap<String, Object>();
    	paramMap.put("formDataMap", formDataMap);
    	paramMap.put("detailList", tpList);
    	paramMap.put("userGb", user.getUserGb());
    	paramMap.put("userId", user.getUserId());

    	Map<String, Object> returnMap = anlsReqService.insertAnlsReq(paramMap);

    	model.addAttribute("analReqSno", returnMap.get("analReqSno"));

        return new ModelAndView(ajaxMainView, model);

    }

    /**
    *
    * <pre>
    * 1. 메소드명 : queryAnlsReqPop
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 요청관리 팝업관련 호출시
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return ModelAndView
    * @throws Exception
    */
   @RequestMapping(value="/anls/req/queryAnlsReqPop.do")
   public ModelAndView queryAnlsReqPop(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

	    FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	   	reqParams.put("regrId", user.getUserId());
	   	reqParams.put("insttCd", user.getInsttCd());

	   	String type = (String) reqParams.get("type");
	   	String userGb = "";

	   	if(type.equals("prosr")) {
	   		userGb = "C01002";
	   	}else if(type.equals("regUser")) {
	   		userGb = "C01001";
	   		if(user.getUserGb().equals("C01003")) {
	   			userGb = "C01003";
	   		}
	   	}else if(type.equals("analCrgr") || type.equals("analRcvr")) {
	   		userGb = "C01003";
	   	}
	   	

	   	switch (type) {
		case "incdt": //사건
			reqParams.put("sqlQueryId", "anlsReqDAO.selectIncidentListCnt");
			break;
		case "prosr": //검사
		case "regUser": //담당자
		case "analCrgr": //분석담당자
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "anlsReqDAO.selectCrgrListCnt");
			break;
		case "analRcvr":
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "anlsReqDAO.selectCrgrListCnt");
			break;
		}
	   	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
	   	reqParams.put("totalCount", totCnt);

	   	PageUtil.calcPage(reqParams);

	   	switch (type) {
		case "incdt": //사건
			reqParams.put("sqlQueryId", "anlsReqDAO.selectIncidentList");
			break;
		case "prosr": //검사
		case "regUser": //담당자
		case "analCrgr": //분석담당자
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "anlsReqDAO.selectCrgrList");
			break;
		case "analRcvr": //분석담당자
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "anlsReqDAO.selectCrgrList");
			break;	
		}

	   	List popList = comService.selectCommonQueryList(reqParams);
		reqParams.put("popList", popList);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);

   }

   /**
   *
   * <pre>
   * 1. 메소드명 : indexAnlsReqInfo
   * 2. 작성일 : 2021. 4. 20.
   * 3. 작성자 : leeji
   * 4. 설명 :  분석지원요청 요청관리 상세페이지 이동
   * </pre>
   * @param map
   * @param model
   * @param request
   * @return String
   * @throws Exception
   */
  @RequestMapping(value="/anls/req/indexAnlsReqInfo.do")
  public String indexAnlsReqInfo(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

      return "dems/anls/req/anlsReqInfo";
  }

  /**
  *
  * <pre>
  * 1. 메소드명 : queryAnlsReqInfo
  * 2. 작성일 : 2021. 4. 20.
  * 3. 작성자 : leeji
  * 4. 설명 :  분석지원요청 요청관리 상세페이지 진입시 호출
  * </pre>
  * @param map
  * @param model
  * @param request
  * @return ModelAndView
  * @throws Exception
  */
 @RequestMapping(value="/anls/req/queryAnlsReqInfo.do")
 public ModelAndView queryAnlsReqInfo(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

	reqParams.put("sqlQueryId", "anlsReqDAO.selectAnlsReqInfo");
	Map anlsReqInfoMap = comService.selectCommonQueryMap(reqParams);
	reqParams.put("anlsReqInfoMap", anlsReqInfoMap);

	reqParams.put("sqlQueryId", "anlsReqDAO.selectAnalysisEvdcDtlList");
 	List analysisEvdcDtlList = comService.selectCommonQueryList(reqParams);
 	reqParams.put("analysisEvdcDtlList", analysisEvdcDtlList);

 	/*reqParams.put("sqlQueryId", "anlsReqDAO.selectAnlsReqInfoFileList");
 	List fileList = comService.selectCommonQueryList(reqParams);
 	reqParams.put("fileList", fileList);*/

    model.addAttribute("atchFileList",comService.selectFileList((String) anlsReqInfoMap.get("analReqSno") , "anlsReq_File"));
    model.addAttribute("atchFileImgList",comService.selectFileList((String) anlsReqInfoMap.get("analReqSno") , "anlsReq_Img"));

	model.addAllAttributes(reqParams);

	return new ModelAndView(ajaxMainView, model);
 }
 
 
 @RequestMapping(value = "/anls/req/jetcoUserIpInfo.do")
 public ModelAndView jetcoUserIpInfo(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

       FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
       
       
         String selReqCrgrId = (String) reqParams.get("selReqCrgrId");
         //model.addAttribute("analReqSno", returnMap.get("analReqSno"));
         if(selReqCrgrId != null && selReqCrgrId != "") {
             Map map = new HashMap();
             map.put("userId", selReqCrgrId);
             reqParams.put("userIpInfo", jetcoUserIpService.selectUserIpById(map));
         }
         
         model.addAllAttributes(reqParams);     
         
       return new ModelAndView(ajaxMainView, model);

 }

 /**
 *
 * <pre>
 * 1. 메소드명 : delAnlsReqUDtl
 * 2. 작성일 : 2021. 4. 20.
 * 3. 작성자 : leeji
 * 4. 설명 :  분석지원요청 요청관리 상세페이지 삭제시 호출
 * </pre>
 * @param paramData
 * @param model
 * @param request
 * @return ModelAndView
 * @throws Exception
 */
 @RequestMapping(value="/anls/req/delAnlsReqUDtl.do")
 public ModelAndView delAnlsReqUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData) throws Exception {

	Map<String, Object> paramMap = new HashMap<String,Object>();

	ObjectMapper mapper = new ObjectMapper();
 	Map<String, Object> jsonObject = mapper.readValue(paramData, Map.class);

 	paramMap.put("analReqSno", (String) jsonObject.get("analReqSno"));

	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	paramMap.put("userId", user.getUserId());
	paramMap.put("insttCd", user.getInsttCd());
	paramMap.put("userGb", user.getUserGb());

	anlsReqService.deleteAnlsReqUDtl(paramMap);

	return new ModelAndView(ajaxMainView, model);
}

	 /**
	 *
	 * <pre>
	 * 1. 메소드명 : indexAnlsReqInfo
	 * 2. 작성일 : 2021. 4. 20.
	 * 3. 작성자 : leeji
	 * 4. 설명 :  분석지원요청 요청관리 상세페이지 이동
	 * </pre>
	 * @param map
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/anls/req/indexAnlsReqUDtl.do")
	public String indexAnlsReqUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

	    return "dems/anls/req/anlsReqUDtl";
	}


	/**
    *
    * <pre>
    * 1. 메소드명 : updAnlsReqUDtl
    * 2. 작성일 : 2021. 4. 14.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 수정시 호출
    * </pre>
    * @param paramData
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
   @RequestMapping(value="/anls/req/updAnlsReqUDtl.do")
   public ModelAndView updAnlsReqUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

	   	ObjectMapper mapper = new ObjectMapper();
	   	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

	   	//분석대상 상세정보
	   	ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
	   	//분석지원요청정보
	   	Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");

	   	Map<String, Object> paramMap = new HashMap<String, Object>();
	   	paramMap.put("formDataMap", formDataMap);
	   	paramMap.put("detailList", tpList);
	   	paramMap.put("userGb", user.getUserGb());
	   	paramMap.put("insttCd", user.getInsttCd());
	   	paramMap.put("userId", user.getUserId());

    	model.addAttribute("analReqSno", formDataMap.get("analReqSno"));

	   	anlsReqService.updateAnlsReq(paramMap);

	    return new ModelAndView(ajaxMainView, model);

   }

   /**
   *
   * <pre>
   * 1. 메소드명 : queryAnlsReqFile
   * 2. 작성일 : 2021. 4. 22.
   * 3. 작성자 : leeji
   * 4. 설명 :  분석지원요청 요청관리 파일관련 호출시
   * </pre>
   * @param paramData
   * @param model
   * @param request
   * @return
   * @throws Exception
   */
  @RequestMapping(value="/anls/req/queryAnlsReqFile.do")
  public ModelAndView queryAnlsReqFile(HttpServletRequest request, ModelMap model, @RequestBody String paramData) throws Exception {

	  Map<String, Object> paramMap = new HashMap<String, Object>();

	  FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	  paramMap.put("userGb", user.getUserGb());
	  paramMap.put("insttCd", user.getInsttCd());
	  paramMap.put("userId", user.getUserId());

	  ObjectMapper mapper = new ObjectMapper();
	  Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

	  Map<String, Object> anlsReqInfo = (Map<String, Object>) jsonObject.get("anlsReqInfo");
	  paramMap.put("anlsReqInfo", anlsReqInfo);

	  //등록할 첨부파일(일반:gen,선별파일:img)
	  ArrayList<Map<String, Object>> tpList1 = (ArrayList<Map<String, Object>>) jsonObject.get("insFileArray");
	  paramMap.put("insFileList", tpList1);

	  //삭제할 첨부파일
	  ArrayList<Map<String, Object>> tpList2 = (ArrayList<Map<String, Object>>) jsonObject.get("delFileArray");
	  paramMap.put("delFileList", tpList2);

	  anlsReqService.procAnlsReqFile(paramMap);

	  return new ModelAndView(ajaxMainView, model);

  }

}
