/**
 *
 * <pre>
 * 1. 클래스명 : AnlsPgsService.java
 * 2. 작성일 : 2021. 4. 15.
 * 3. 작성자 : Leeji
 * 4. 설명 : 분석지원요청 진행관리 Service
 * </pre>
 *
 */
package dems.anls.pgs.service;

import java.util.HashMap;
import java.util.Map;


public interface AnlsPgsService {

	void updatePgsStat(Map<String,Object> map) throws Exception;
	
	void insertAnlsPgsRDtl(Map<String,Object> map) throws Exception;
	
	void updateAnlsPgsUDtl(Map<String,Object> map) throws Exception;
	
	void deleteAnlsPgsUDtl(Map<String,Object> map) throws Exception;
	
	void procAnlsPgsFile(Map<String,Object> map) throws Exception;

	/**
	 * <pre>
	 * 1. 메소드명 : regAnlsPgsEvdcHistUDtl
	 * 2. 작성일 : 2021. 5. 18. 오후 3:36:22
	 * 3. 작성자 : jij
	 * 4. 설명 : @!@ 
	 * </pre>
	 * @param reqParams
	 * @param fileMap
	 * @return
	 */
	String regAnlsPgsEvdcHistUDtl(HashMap reqParams, HashMap<String, Object> fileMap) throws Exception;	

}