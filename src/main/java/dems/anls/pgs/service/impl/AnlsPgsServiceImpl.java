package dems.anls.pgs.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import dems.anls.pgs.service.AnlsPgsService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import frame.futil.WebsocketWork;


@Service("anlsPgsService")
public class AnlsPgsServiceImpl extends EgovAbstractServiceImpl implements AnlsPgsService{

	protected Logger log = LoggerFactory.getLogger(AnlsPgsServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="comDAO")
	private ComDAO comDAO;
	
	@Resource(name="anlsPgsDAO")
	private AnlsPgsDAO anlsPgsDAO;

	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	/**
    *
    * <pre>
    * 1. 메소드명 : updatePgsStat
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 진행관리 진행상태 변경(수사과장승인,수사과장승인요청취소,접수,접수취소상태,선별요청,선별요청취소)
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void updatePgsStat(Map<String, Object> map) throws Exception {
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		String type = (String) map.get("type");
		List aprvList = (List) map.get("aprvList");
		String userGb = (String) map.get("userGb");
		String insttCd = (String) map.get("insttCd");
		String userId = (String) map.get("userId");
		List<Map<String, Object>> evdcList = (List) map.get("evdcList");

		Map<String, Object> infoMap = new HashMap<String, Object>();
		
		//진행관리에서 넘어오는 상태 : 수사과장승인,수사과장승인요청취소,접수,접수취소상태,선별요청,선별요청취소만 처리
		if(!("aprv".equals(type) || "aprvCncl".equals(type) || "rcv".equals(type) || "rcvCncl".equals(type) || "selReq".equals(type) || "selReqCncl".equals(type))) {
			throw new MException(egovMessageSource.getMessage("dems.anls.req.005"));
		}
		
		switch (type) {
		case "aprv":     //수사과장승인
			infoMap.put("pgsStat", "C03006");
			infoMap.put("aprvrId", userId);
			infoMap.put("updrId", userId);
			break;
		case "aprvCncl": //수사과장승인요청취소(접수으로변경)
			infoMap.put("pgsStat", "C03005");
			infoMap.put("updrId", userId);
			break;
		case "rcv":      //접수
			infoMap.put("pgsStat", "C03005");
			//infoMap.put("rcvNoAcp", rcvNoAcp);	
			infoMap.put("rcvrIdAcp", userId);
			infoMap.put("updrId", userId);
						break;
		case "rcvCncl":  //접수취소(검사승인으로변경)
			infoMap.put("pgsStat", "C03003");
			infoMap.put("aprvrId", userId);
			infoMap.put("updrId", userId);
			break;
		case "selReq":  //션별요청
			infoMap.put("pgsStat", "C03007");
			infoMap.put("selReqCrgrId", userId);
			infoMap.put("updrId", userId);
			break;
		case "selReqCncl":  //션별요청취소(수사과장승인으로변경)
			infoMap.put("pgsStat", "C03006");
			infoMap.put("updrId", userId);
			break;
		}
		
		//권한체크
		if(("aprv".equals(type) || "aprvCncl".equals(type)) && !"C01004".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"수사과장"}));
		}
		
		if(("rcv".equals(type) || "rcvCncl".equals(type) || "selReq".equals(type) || "selReqCncl".equals(type)) && !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식 수사관"}));
		}
		
		
		int chkAprv = 0;
		String pgsStat = "";	
		
		for(int i = 0; i < aprvList.size(); i++) {
			
			paramMap.clear();
			paramMap.putAll(infoMap);
			paramMap.put("analReqSno", aprvList.get(i));
			paramMap.put("sqlQueryId", "anlsReqDAO.selectAnalysisSuppReqChk");
			
			//요청자료 유무 체크
			chkAprv = comDAO.selectCommonQueryListTotCnt(paramMap);
			if(chkAprv < 1) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.002",new String[]{(String) aprvList.get(i)}));
			}
			
			//진행상태 체크
			paramMap.put("sqlQueryId", "anlsReqDAO.selectPgsStat");
			pgsStat = comDAO.selectCommonQueryString(paramMap);
			
			//수사과장승인         aprv      ->접수
			//수사과장승인요청취소 aprvCncl  ->수사과장승인 
			//접수                 rcv       ->검사승인
			//접수취소             rcvCncl   ->접수
			//선별요청             selReq    ->수사과장승인
			//선별요청취소         selReqCncl->선별요청
			
			if(("aprv".equals(type) || "rcvCncl".equals(type) )&& !"C03005".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(i),"문서접수"}));
			}			
			
			if(("aprvCncl".equals(type) || "selReq".equals(type)) && !"C03006".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(i),"과장승인"}));
			}
			
			if("rcv".equals(type) && !"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(i),"검사승인"}));
			}
			
			if("selReqCncl".equals(type) && !"C03007".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(i),"선별요청"}));
			}
			
		}
		
		//DEMS_ANALYSIS_SUPP_REQ 진행상태 변경
		for(int i = 0; i < aprvList.size(); i++) {
			
			
			paramMap.clear();			
			//접수번호 조회
			paramMap.put("sqlQueryId", "anlsPgsDAO.selectRcvNoAcp");
			String rcvNoAcp = comDAO.selectCommonQueryString(paramMap); 	
			paramMap.putAll(infoMap);	
			paramMap.putAll(map);
			paramMap.put("rcvNoAcp", rcvNoAcp);
			paramMap.put("analReqSno", aprvList.get(i));
			paramMap.put("type", type);
			paramMap.put("sqlQueryId", "anlsPgsDAO.updateAnlsReqPgsStat");			
			comDAO.updateCommonQuery(paramMap);
			
			// 승인 대상 요청서가 1개이고 압수물 담당자 배정 목록이 null이 아닐 경우 = 상세페이지에서 승인 버튼을 누를 경우 담당자 ID 가 존재하도록 목록이 오면 결과서 등록
			if(aprvList.size() == 1 && evdcList != null && "aprv".equals(type)) {
				for(Map<String, Object> tempMap : evdcList) {
					for(Map.Entry<String, Object> entry : tempMap.entrySet()) {
						System.out.println("[key]:" + entry.getKey() + ", [value]:" + entry.getValue());
						if(!entry.getValue().equals("")) {
							//빈 값이 아닐 경우 분석 결과서 (분석대기) 기본 등록
							Map<String, Object> rsltMap = new HashMap<String, Object>();
							rsltMap.put("sqlQueryId", "anlsPgsDAO.selectAnalysisRsltInfoCnt");
							rsltMap.put("analReqSno", aprvList.get(0));
							rsltMap.put("evdcSno", entry.getKey());
							rsltMap.put("type", "");
							
							int chkCnt = comDAO.selectCommonQueryListTotCnt(rsltMap);
							if(chkCnt > 0) {
								throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.006",new String[]{(String) rcvNoAcp,"분석결과"}));
							}
							
							
							rsltMap.put("analTool", "C13001");
							rsltMap.put("analStat", "C06001");
							rsltMap.put("analCrgrId", entry.getValue());
							anlsPgsDAO.insertAnalysisRslt(rsltMap);
							
							rsltMap.clear();
							rsltMap.put("type", "assign");
							rsltMap.put("sqlQueryId", "anlsPgsDAO.updateAnalysisEvdcDtlPgsStat");			
							rsltMap.put("analReqSno", aprvList.get(0));
							rsltMap.put("evdcSno", entry.getKey());
							rsltMap.put("rcvrIdAcp", entry.getValue());
							
							comDAO.updateCommonQuery(rsltMap);
							
						}
					}
				}
			}else if("aprvCncl".equals(type)){
				
				
				Map<String, Object> rsltMap = new HashMap<String, Object>();
				rsltMap.put("sqlQueryId", "anlsPgsDAO.selectAnalysisRsltInfoCnt");
				rsltMap.put("analReqSno", aprvList.get(0));
				rsltMap.put("type", "assign");
				
				int chkCnt = comDAO.selectCommonQueryListTotCnt(rsltMap);
				if(chkCnt > 0) {
					throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.006",new String[]{(String) rcvNoAcp,"분석결과"}));
				}
				
				anlsPgsDAO.deleteAnalysisRslt(rsltMap);	
			}
			
		}
		
		if(type.equals("rcv") || type.equals("rcvCncl")){//접수 //접수 취소
			WebsocketWork wscw = new WebsocketWork();
			wscw.broadCast("myChart3");
		}
		
		Map<String, Object> tpMap = new HashMap<String, Object>();
		//DEMS_ANALYSIS_EVDC_DTL 변경
		for(int i = 0; i < aprvList.size(); i++) {			
			
			//DEMS_ANALYSIS_EVDC_DTL 리스트 가져오기			
			paramMap.clear();
			paramMap.put("analReqSno", aprvList.get(i));
			paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalysisEvdcDtlList");
			
			
 			List AnalysisEvdcDtlList = comDAO.selectCommonQueryList(paramMap);  
			
			for(int j = 0; j < AnalysisEvdcDtlList.size(); j++ ) {				

				Map<String, Object> dtlMap = (Map<String, Object>) AnalysisEvdcDtlList.get(j);
				tpMap.clear();	
				//접수상태일때만 접수번호조회
				if(type.equals("rcv")) {
					paramMap.clear();
					paramMap.put("analReqSno", aprvList.get(i));
					paramMap.put("evdcSno", dtlMap.get("evdcSno"));
					paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalysisEvdcDtlRcvNoAcp");
					
					String dtlRcvNoAcp = comDAO.selectCommonQueryString(paramMap);		
					tpMap.put("dtlRcvNoAcp", dtlRcvNoAcp);
				}
					
				if(!(type.equals("selReq") || type.equals("selReqCncl"))) {
					tpMap.putAll(infoMap);	
					tpMap.putAll(map);				
					tpMap.put("analReqSno", aprvList.get(i));
					tpMap.put("evdcSno", dtlMap.get("evdcSno"));
					tpMap.put("type", type);
					tpMap.put("sqlQueryId", "anlsPgsDAO.updateAnalysisEvdcDtlPgsStat");			
					comDAO.updateCommonQuery(tpMap);
				}
			}
		}
		
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : insertAnlsPgsRDtl
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 진행관리 분석결과 등록
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void insertAnlsPgsRDtl(Map<String, Object> map) throws Exception {

		Map<String,Object> paramMap = new HashMap<String,Object>();
		String userGb = (String) map.get("userGb");
		
		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		validateUtil.check(chkParamMap, requireParams);
		
		//권한이 포렌식수사관(C01003)
		if(!"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.001",new String[]{"포렌식수사관"}));
		}
		
		//존재여부
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalysisRsltInfoCnt");
		paramMap.put("analReqSno", map.get("analReqSno"));
		paramMap.put("evdcSno", map.get("evdcSno"));
		
		int chkCnt = comDAO.selectCommonQueryListTotCnt(paramMap);
		if(chkCnt > 0) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.006",new String[]{(String) map.get("rcvNoAcp"),"분석결과"}));
		}
		
		//분석요청 진행상태(선별확인:C03008)
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsReqDAO.selectPgsStat");
		paramMap.put("analReqSno", map.get("analReqSno"));
		String pgsStat = comDAO.selectCommonQueryString(paramMap);
		
		/*if(!"C03008".equals(pgsStat)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) map.get("analReqSno"),"선별확인"}));
		}*/
		
		//분석결과 저장		
		//DEMS_ANALYSIS_RSLT INSERT
		paramMap.clear();
		paramMap.putAll(map);
		paramMap.put("analRsltSno", map.get("analReqSno") + "-" + map.get("evdcSno"));
		anlsPgsDAO.insertAnalysisRslt(paramMap);
		
		paramMap.put("type", "assign");
		paramMap.put("sqlQueryId", "anlsPgsDAO.updateAnalysisEvdcDtlPgsStat");		
		paramMap.put("rcvrIdAcp", paramMap.get("analCrgrId"));
		comDAO.updateCommonQuery(paramMap);
		
		WebsocketWork wscw = new WebsocketWork();
		wscw.broadCast("myChart2");
		
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : updateAnlsPgsUDtl
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 진행관리 분석결과 수정
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void updateAnlsPgsUDtl(Map<String, Object> map) throws Exception {
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
		String userGb = (String) map.get("userGb");
		String userId = (String) map.get("userId");
		String insttCd = (String) map.get("insttCd");
		
		ArrayList<Map<String, Object>> fileList = (ArrayList<Map<String, Object>>) map.get("fileList");
		
		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		validateUtil.check(chkParamMap, requireParams);
		
		//권한이 포렌식수사관(C01003)
		if(!"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.001",new String[]{"포렌식수사관"}));
		}
		
		//분석요청 진행상태(선별확인:C03008)
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsReqDAO.selectPgsStat");
		paramMap.put("analReqSno", map.get("analReqSno"));
		String pgsStat = comDAO.selectCommonQueryString(paramMap);
		
		String [] WHITE_LIST   = {"C03005", "C03006", "C03007", "C03008"};
		/*if(!"C03008".equals(pgsStat)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) map.get("analReqSno"),"선별확인"}));
		}*/
		if(!Arrays.stream(WHITE_LIST).anyMatch(pgsStat::equals)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) map.get("analReqSno"),"문서접수, 과장승인, 선별요청, 선별확인"}));
		}
		
		
		//동일부서 여부
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalInsttcd");
		paramMap.put("analReqSno", map.get("analReqSno"));
		paramMap.put("evdcSno", map.get("evdcSno"));
		String analInsttCd = comDAO.selectCommonQueryString(paramMap);
		
		/*if(!insttCd.equals(analInsttCd)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.003",new String[]{(String) map.get("analReqSno")}));
		}*/
		if(!userGb.equals("C01003") && !insttCd.equals(analInsttCd)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.003",new String[]{(String) map.get("analReqSno")}));
		}

		//DEMS_ANALYSIS_RSLT update
		paramMap.clear();
		paramMap.putAll(map);		
		anlsPgsDAO.updateAnalysisRslt(paramMap);
		
		paramMap.put("type", "assign");
		paramMap.put("sqlQueryId", "anlsPgsDAO.updateAnalysisEvdcDtlPgsStat");		
		paramMap.put("rcvrIdAcp", paramMap.get("analCrgrId"));
		comDAO.updateCommonQuery(paramMap);
		
		WebsocketWork wscw = new WebsocketWork();
		wscw.broadCast("myChart2");
		
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : deleteAnlsPgsUDtl
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 진행관리 분석결과 삭제
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void deleteAnlsPgsUDtl(Map<String, Object> map) throws Exception {
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
		String userGb = (String) map.get("userGb");
		String userId = (String) map.get("userId");
		String insttCd = (String) map.get("insttCd");
		
		ArrayList<Map<String, Object>> fileList = (ArrayList<Map<String, Object>>) map.get("fileList");
		
		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		validateUtil.check(chkParamMap, requireParams);
		
		//권한이 포렌식수사관(C01003)
		if(!"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.001",new String[]{"포렌식수사관"}));
		}
		
		//분석요청 진행상태(선별확인:C03008)
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsReqDAO.selectPgsStat");
		paramMap.put("analReqSno", map.get("analReqSno"));
		String pgsStat = comDAO.selectCommonQueryString(paramMap);
		
		String [] WHITE_LIST   = {"C03005", "C03006", "C03007", "C03008"};
		
		/*if(!"C03008".equals(pgsStat)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) map.get("analReqSno"),"선별확인"}));
		}*/
		if(!Arrays.stream(WHITE_LIST).anyMatch(pgsStat::equals)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) map.get("analReqSno"),"문서접수, 과장승인, 선별요청, 선별확인"}));
		}
		
		
		
		
		//동일부서 여부
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalInsttcd");
		paramMap.put("analReqSno", map.get("analReqSno"));
		paramMap.put("evdcSno", map.get("evdcSno"));
		String analInsttCd = comDAO.selectCommonQueryString(paramMap);
		
		/*if(!insttCd.equals(analInsttCd)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.003",new String[]{(String) map.get("analReqSno")}));
		}*/
		if(!userGb.equals("C01003") && !insttCd.equals(analInsttCd)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.003",new String[]{(String) map.get("analReqSno")}));
		}
		
		//분석지원요청 분석결과 파일ID조회(DEMS_ANALYSIS_RSLT)
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalRsltFileId");
		paramMap.put("analReqSno",map.get("analReqSno"));
		paramMap.put("evdcSno",map.get("evdcSno"));
		String atchFileId = comDAO.selectCommonQueryString(paramMap);	

		//분석지원요청관리 첨부파일 유무(DEMS_ATCH_FILE_DTL)
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsPgsDAO.selectReqFileCnt");
		paramMap.put("atchFileId", atchFileId);
		int fileCnt = comDAO.selectCommonQueryListTotCnt(paramMap);
		
		if(fileCnt > 0) {
			
			//DEMS_ATCH_FILE_DTL update
			paramMap.clear();
			paramMap.put("loginId",  userId);
			paramMap.put("atchFileId",  atchFileId);
			paramMap.put("evdcSno",  map.get("evdcSno"));
			paramMap.put("sqlQueryId", "anlsPgsDAO.deleteAtchFileEvoSnoDtl");
			comDAO.updateCommonQuery(paramMap);	
			
			//DEMS_ATCH_FILE update
			paramMap.put("sqlQueryId", "anlsPgsDAO.deleteAtchFile");
			comDAO.updateCommonQuery(paramMap);	
			
		}
		
		//DEMS_ANALYSIS_RSLT delete
		paramMap.clear();
		paramMap.putAll(map);		
		anlsPgsDAO.deleteAnalysisRslt(paramMap);
		
		WebsocketWork wscw = new WebsocketWork();
		wscw.broadCast("myChart2");

	}

	/**
    *
    * <pre>
    * 1. 메소드명 : procAnlsPgsFile
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 진행관리 첨부파일 처리
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void procAnlsPgsFile(Map<String, Object> map) throws Exception {

		
		Map<String,Object> paramMap = new HashMap<String, Object>();
		ArrayList<Map<String, Object>> insFileList = (ArrayList<Map<String, Object>>) map.get("insFileList");
		ArrayList<Map<String, Object>> delFileList = (ArrayList<Map<String, Object>>) map.get("delFileList");
		Map<String, Object> anlsReqInfo = (Map<String, Object>) map.get("anlsReqInfo");
		String atchFileSeq = "";
		
		//분석지원요청 분석결과 파일ID조회
		paramMap.clear();
		paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalRsltFileId");
		paramMap.put("analReqSno",anlsReqInfo.get("analReqSno"));
		paramMap.put("evdcSno",anlsReqInfo.get("evdcSno"));
		String atchFileId = comDAO.selectCommonQueryString(paramMap);	
				
		//등록,수정시
		if(insFileList.size() > 0) {
			
			//등록이거나 //수정인데 파일첨부를 처음하는경우
			if("ins".equals(anlsReqInfo.get("type")) || ("upd".equals(anlsReqInfo.get("type")) && EgovStringUtil.isEmpty(atchFileId))) {
				
				//첨부파일 시퀀스 조회			
				paramMap.clear();
				paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalysisRsltAtchFileSeq");
				atchFileSeq = comDAO.selectCommonQueryString(paramMap);
				
				//DEMS_ATCH_FILE insert
				paramMap.clear();
				paramMap.put("loginId",  map.get("userId"));	
				paramMap.put("atchFileId", atchFileSeq);
				anlsPgsDAO.insertAtchFile(paramMap);
				
				//DEMS_ANALYSIS_RSLT update
				paramMap.clear();
				paramMap.put("analReqSno", anlsReqInfo.get("analReqSno"));
				paramMap.put("evdcSno",  anlsReqInfo.get("evdcSno"));
				paramMap.put("updrId",  map.get("userId"));
				paramMap.put("atchFileId", atchFileSeq);
				paramMap.put("sqlQueryId", "anlsPgsDAO.updateAnalRsltFileId");
				comDAO.updateCommonQuery(paramMap);
				
				
			} else {
				atchFileSeq = (String) anlsReqInfo.get("atchFileId");
			}

			//DEMS_ATCH_FILE_DTL insert
			for(int i = 0; i < insFileList.size(); i++) {
				paramMap.clear();
				paramMap.put("atchFileId",  atchFileSeq);
				paramMap.put("evdcSno",  anlsReqInfo.get("evdcSno"));				
				paramMap.put("loginId",  map.get("userId"));
				paramMap.putAll(insFileList.get(i));
				anlsPgsDAO.insertAtchFileDtl(paramMap);	
			}
			
		}
		
		//삭제시
		if(delFileList.size() > 0) {
			//DEMS_ATCH_FILE_DTL update
			for(int i = 0; i < delFileList.size(); i++) {
				paramMap.clear();
				paramMap.put("atchFileId", anlsReqInfo.get("atchFileId"));
				paramMap.put("fileSno", anlsReqInfo.get("fileSno"));
				paramMap.put("loginId",  map.get("userId"));
				paramMap.put("sqlQueryId", "anlsPgsDAO.deleteAtchFileSnoDtl");
				comDAO.updateCommonQuery(paramMap);		
			}
			
			//분석지원요청관리 첨부파일 유무(DEMS_ATCH_FILE_DTL)
			paramMap.clear();
			paramMap.put("sqlQueryId", "anlsPgsDAO.selectReqFileCnt");
			paramMap.put("atchFileId", anlsReqInfo.get("atchFileId"));
			int fileCnt = comDAO.selectCommonQueryListTotCnt(paramMap);
			
			//첨부파일을 전부다 지웠을경우
			if(fileCnt < 1) {
				//DEMS_ATCH_FILE update
				paramMap.clear();
				paramMap.put("loginId",  map.get("userId"));
				paramMap.put("atchFileId", anlsReqInfo.get("atchFileId"));
				paramMap.put("sqlQueryId", "anlsPgsDAO.deleteAtchFile");
				comDAO.updateCommonQuery(paramMap);
				
				//DEMS_ANALYSIS_RSLT update
				paramMap.clear();
				paramMap.put("analReqSno", anlsReqInfo.get("analReqSno"));
				paramMap.put("evdcSno",  anlsReqInfo.get("evdcSno"));
				paramMap.put("updrId",  map.get("userId"));
				paramMap.put("atchFileId", null);
				paramMap.put("sqlQueryId", "anlsPgsDAO.updateAnalRsltFileId");
				comDAO.updateCommonQuery(paramMap);
			}
			
		}
	
	}

	/**
	 * <pre>
	 * 1. 메소드명 : regAnlsPgsEvdcHistUDtl
	 * 2. 작성일 : 2021. 5. 18. 오후 3:36:22
	 * 3. 작성자 : jij
	 * 4. 설명 : @!@ 
	 * </pre>
	 * @param reqParams
	 * @param fileMap
	 * @return
	 */
	@Override
	public String regAnlsPgsEvdcHistUDtl(HashMap reqParams, HashMap<String, Object> fileMap) throws Exception {

		HashMap<String,String> requireParams = new HashMap();
		requireParams.put("analReqSno", "분석요청일련번호");
		requireParams.put("evdcSno", "증거일련번호");
		requireParams.put("histRegDt", "등록일자");
		requireParams.put("trtStatCd", "처리상태");
		requireParams.put("trtCnts", "처리내용");
		validateUtil.check(reqParams, requireParams);

		String userGb = (String) reqParams.get("session_usergb");
		String pgsStat = "";
		Map anlsReqInfoMap = new HashMap<>();
		if(userGb.equals("C01003") || userGb.equals("C01004")) {
			reqParams.put("sqlQueryId", "anlsPgsDAO.selectAnlsReqInfo");
			anlsReqInfoMap = comService.selectCommonQueryMap(reqParams);
			pgsStat = (String) anlsReqInfoMap.get("pgsStat");
			if(pgsStat == null || pgsStat.equals("") || pgsStat.equals("C03001") || pgsStat.equals("C03002")){
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.005", null));
			}
		}else{
			throw new MException(egovMessageSource.getMessageArgs("dems.site.rcv.008", null));
		}

		boolean denyListChk = true;
		String[] eList = EgovProperties.getProperty("Globals.fileUpload.Extensions").toString().split("\\.");

		if(fileMap.get("fileList4OriFileNm") != null){
			if (fileMap.get("fileList4OriFileNm") instanceof String) {
				String fileList = (String) fileMap.get("fileList4OriFileNm");
				for(int k=0; k < eList.length ;k++){
					if (fileList.toLowerCase().indexOf("."+eList[k]) > -1){
						denyListChk = false;
					}
				}
			}else {
				List<String> fileList = (List<String>) fileMap.get("fileList4OriFileNm");
				for (int i = 0; i < fileList.size(); i++) {
					for(int k=0; k < eList.length ;k++){
						if (fileList.get(i).toLowerCase().indexOf("."+eList[k]) > -1){
							denyListChk = false;
						}
					}
				}
			}
		}

		String rtnMsg = "";

		if (denyListChk && fileMap.get("fileList4OriFileNm") != null) {
			throw new MException(egovMessageSource.getMessageArgs("common.fileupload.fail002", null));
		}else{
			//첨부파일등록
			String loginId = (String) reqParams.get("session_userid");
			String atchFileId = (String) fileMap.get("atchFileId");

			if(EgovStringUtil.isNull(atchFileId)){
				atchFileId = comService.insertAtchFile("fileList4", "10", loginId, fileMap);
			}else{
				atchFileId = comService.updateAtchFile(atchFileId, "fileList4", "10", loginId, fileMap);
			}

			reqParams.put("atchFileId", atchFileId);

			if(reqParams.get("action").equals("R")) {
				reqParams.put("sqlQueryId", "anlsPgsDAO.regAnlsPgsEvdcHistUDtl");
				rtnMsg = egovMessageSource.getMessageArgs("success.common.insert", null);
			}else if(reqParams.get("action").equals("U")){
				reqParams.put("sqlQueryId", "anlsPgsDAO.updAnlsPgsEvdcHistUDtl");
				rtnMsg = egovMessageSource.getMessageArgs("success.common.save", null);
			}

			comDAO.updateCommonQuery(reqParams);
		}


		return rtnMsg;

	}

}