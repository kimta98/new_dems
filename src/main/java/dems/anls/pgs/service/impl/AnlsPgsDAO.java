/**
 *
 * <pre>
 * 1. 클래스명 : AnlsPgsDAO.java
 * 2. 작성일 : 2021. 4. 15.
 * 3. 작성자 : Leeji
 * 4. 설명 : AnlsPgsDAO
 * </pre>
 *
 */
package dems.anls.pgs.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;


@Repository("anlsPgsDAO")
public class AnlsPgsDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(AnlsPgsDAO.class);

	/**
     *
     * <pre>
     * 1. 메소드명 : insertAnalysisRslt
     * 2. 작성일 : 2021. 4. 21.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석결과 insert
     * </pre>
     * @param map
     * @param
     * @param
     * @return int
     * @throws Exception
     */
    public void insertAnalysisRslt(Map<String, Object> paramMap) throws Exception {

    	insert("anlsPgsDAO.insertAnalysisRslt", paramMap);
    }
    
    /**
    *
    * <pre>
    * 1. 메소드명 : insertAtchFile
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석결과 첨부파일 insert
    * </pre>
    * @param map
    * @param
    * @param
    * @return int
    * @throws Exception
    */
   public void insertAtchFile(Map<String, Object> paramMap) throws Exception {

   	insert("anlsPgsDAO.insertAtchFile", paramMap);
   }
   
   /**
   *
   * <pre>
   * 1. 메소드명 : insertAtchFileDtl
   * 2. 작성일 : 2021. 4. 21.
   * 3. 작성자 : leeji
   * 4. 설명 :  분석결과 첨부파일DTL insert
   * </pre>
   * @param map
   * @param
   * @param
   * @return int
   * @throws Exception
   */
   public void insertAtchFileDtl(Map<String, Object> paramMap) throws Exception {
	   insert("anlsPgsDAO.insertAtchFileDtl", paramMap);
   }
  
   /**
   *
   * <pre>
   * 1. 메소드명 : updateAnalysisRslt
   * 2. 작성일 : 2021. 4. 21.
   * 3. 작성자 : leeji
   * 4. 설명 :  분석결과 update
   * </pre>
   * @param map
   * @param
   * @param
   * @return int
  *  @throws Exception
  */
   public void updateAnalysisRslt(Map<String, Object> paramMap) throws Exception {
	   update("anlsPgsDAO.updateAnalysisRslt", paramMap);
   }
 
   /**
	 *
	 * <pre>
	 * 1. 메소드명 : deleteAnalysisRslt
	 * 2. 작성일 : 2021. 4. 21.
	 * 3. 작성자 : leeji
	 * 4. 설명 :  분석결과 delete
	 * </pre>
	 * @param map
	 * @param
	 * @param
	 * @return int
	 * @throws Exception
   */
   public void deleteAnalysisRslt(Map<String, Object> paramMap) throws Exception {
	   delete("anlsPgsDAO.deleteAnalysisRslt", paramMap);
   }

}