package dems.anls.pgs.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.anls.pgs.service.AnlsPgsService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.JetcoUserIpService;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.fexception.MException;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.DemsConst;
import frame.futil.DemsUtil;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;

/**
 * <pre>
 * 1. 클래스명 : AnlsPgsController.java
 * 2. 작성일 : 2021. 4. 20.
 * 3. 작성자 : 이종인
 * 4. 설명 : 분석지원요청 진행관리 Controller
 * </pre>
 */
@Controller
public class AnlsPgsController {

	protected Logger log = LoggerFactory.getLogger(AnlsPgsController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

    @Resource(name = "jetcoUserIpService")
    private JetcoUserIpService jetcoUserIpService;

	@Resource(name = "anlsPgsService")
	private AnlsPgsService anlsPgsService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;

    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

    /**
     *
     * <pre>
     * 1. 메소드명 : indexAnlsPgsMList
     * 2. 작성일 : 2021. 4. 20.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청 진행관리 목록페이지 진입시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/anls/pgs/indexAnlsPgsMList.do")
    public String indexAnlsPgsMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

        return "dems/anls/pgs/anlsPgsMList";
    }

    /**
     *
     * <pre>
     * 1. 메소드명 : queryAnlsPgsMList
     * 2. 작성일 : 2021. 4. 22.
     * 3. 작성자 : leeji
     * 4. 설명 :  분석지원요청 진행관리 목록 조회시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return ModelAndView
     * @throws Exception
     */
    @RequestMapping(value="/anls/pgs/queryAnlsPgsMList.do")
    public ModelAndView queryAnlsPgsMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	reqParams.put("userGb", user.getUserGb());
    	reqParams.put("regrId", user.getUserId());
    	reqParams.put("insttCd", user.getInsttCd());
    	reqParams.put("searchPgsStat", ((String) reqParams.get("searchPgsStat")).split(","));

    	reqParams.put("sqlQueryId", "anlsReqDAO.selectAnlsReqListCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);

    	reqParams.put("totalCount", totCnt);
    	PageUtil.calcPage(reqParams);

    	reqParams.put("sqlQueryId", "anlsReqDAO.selectAnlsReqList");
    	List anlsPgsList = comService.selectCommonQueryList(reqParams);


		reqParams.put("anlsPgsList", anlsPgsList);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);

    }

    /**
    *
    * <pre>
    * 1. 메소드명 : queryPgsStat
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  분석지원요청 진행관리 진행상태 변경(과장승인,과장승인취소,접수,접수취소,선별요청,선별요청취소)
    * </pre>
    * @param paramData
    * @param model
    * @param request
    * @return ModelAndView
    * @throws Exception
    */
   @RequestMapping(value="/anls/pgs/queryPgsStat.do")
   public ModelAndView queryPgsStat(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

	   ObjectMapper mapper = new ObjectMapper();
	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

	   //요청 리스트
	   List tpList = (List) jsonObject.get("rowDatas");

	   //요청 타입
	   Map<String, Object> infoMap = jsonObject.get("type");
	   String type = (String) infoMap.get("type");
	   
	   //담당자 - 타입이 과장 승인일 경우 
	   List<Map<String, Object>> evdcList = null;
	   if("aprv".equals(type)) {
		   evdcList = (List<Map<String, Object>>) jsonObject.get("evdcDatas");
	   }

	   Map<String, Object> paramMap = new HashMap<String, Object>();
	   paramMap.put("aprvList", tpList);
	   paramMap.put("userGb", user.getUserGb());
	   paramMap.put("userId", user.getUserId());
	   paramMap.put("insttCd", user.getInsttCd());
	   paramMap.put("type", type);
	   paramMap.put("evdcList", evdcList);

	   anlsPgsService.updatePgsStat(paramMap);
       Map selectMap = new HashMap();
       selectMap.put("userId", user.getUserId());
       selectMap.put("userIp", request.getRemoteAddr());
       selectMap.put("useYn", "Y");
       selectMap.put("userKey", request.getSession().getAttribute("UserRsaKey"));
       jetcoUserIpService.selectUserIp(selectMap);

	   return new ModelAndView(ajaxMainView, model);

   }

   /**
   *
   * <pre>
   * 1. 메소드명 : indexAnlsPgsRDtl
   * 2. 작성일 : 2021. 4. 20.
   * 3. 작성자 : leeji
   * 4. 설명 :  분석지원요청 진행관리 분석결과페이지 이동
   * </pre>
   * @param map
   * @param model
   * @param request
   * @return String
   * @throws Exception
   */
  @RequestMapping(value="/anls/pgs/indexAnlsPgsRDtl.do")
  public String indexAnlsPgsRDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

      return "dems/anls/pgs/anlsPgsRDtl";
  }

  /**
  *
  * <pre>
  * 1. 메소드명 : queryAnlsPgsInfo
  * 2. 작성일 : 2021. 4. 20.
  * 3. 작성자 : leeji
  * 4. 설명 :  분석지원요청 분석결과페이지 진입시 호출
  * </pre>
  * @param map
  * @param model
  * @param request
  * @return ModelAndView
  * @throws Exception
  */
 @RequestMapping(value="/anls/pgs/queryAnlsPgsInfo.do")
 public ModelAndView queryAnlsPgsInfo(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

	//분석지원요청 상세정보
	reqParams.put("sqlQueryId", "anlsPgsDAO.selectAnlsReqInfo");
	Map anlsReqInfoMap = comService.selectCommonQueryMap(reqParams);
	reqParams.put("anlsReqInfoMap", anlsReqInfoMap);

	//분석지원요청 분석대상리스트
	reqParams.put("sqlQueryId", "anlsPgsDAO.selectAnalysisEvdcDtlList");
 	List analysisEvdcDtlList = comService.selectCommonQueryList(reqParams);
 	reqParams.put("analysisEvdcDtlList", analysisEvdcDtlList);

	/*reqParams.put("sqlQueryId", "anlsReqDAO.selectAnlsReqInfoFileList");
 	List fileList = comService.selectCommonQueryList(reqParams);
 	reqParams.put("fileList", fileList);*/

    model.addAttribute("atchFileList",comService.selectFileList((String) reqParams.get("analReqSno") , "anlsReq_File"));
    model.addAttribute("atchFileImgList",comService.selectFileList((String) reqParams.get("analReqSno") , "anlsReq_Img"));

 	model.addAllAttributes(reqParams);

	return new ModelAndView(ajaxMainView, model);
 }

  /**
   *
   * <pre>
   * 1. 메소드명 : reqAnlsPgsRDtl
   * 2. 작성일 : 2021. 4. 14.
   * 3. 작성자 : leeji
   * 4. 설명 :  분석지원요청 진행관리 분석결과등록시 호출
   * </pre>
   * @param paramData
   * @param model
   * @param request
   * @return ModelAndView
   * @throws Exception
   */
  @RequestMapping(value="/anls/pgs/reqAnlsPgsRDtl.do")
  public ModelAndView reqAnlsPgsRDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData) throws Exception {

	  FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

	   ObjectMapper mapper = new ObjectMapper();
	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

	   //분석결과 정보
	   Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");

	   Map<String, Object> paramMap = new HashMap<String, Object>();
	   paramMap.put("userGb", user.getUserGb());
	   paramMap.put("userId", user.getUserId());
	   paramMap.put("insttCd", user.getInsttCd());
	   paramMap.putAll(formDataMap);

	   anlsPgsService.insertAnlsPgsRDtl(paramMap);

	   model.addAttribute("analReqSno", paramMap.get("analReqSno"));
	   model.addAttribute("evdcSno", paramMap.get("evdcSno"));

	   return new ModelAndView(ajaxMainView, model);

  }

  /**
  *
  * <pre>
  * 1. 메소드명 : goAnlsPsgUDtl
  * 2. 작성일 : 2021. 4. 14.
  * 3. 작성자 : leeji
  * 4. 설명 :  분석지원요청 진행관리 분석결과수정페이지 진입시 호출
  * </pre>
  * @param map
  * @param model
  * @param request
  * @return ModelAndView
  * @throws Exception
  */
 @RequestMapping(value="/anls/pgs/goAnlsPsgUDtl.do")
 public ModelAndView goAnlsPsgUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

	//분석결과 조회
	reqParams.put("sqlQueryId", "anlsPgsDAO.selectAnalysisRsltInfo");
	Map infoMap = comService.selectCommonQueryMap(reqParams);
	model.addAttribute("infoMap", infoMap);
	/*reqParams.put("sqlQueryId", "anlsPgsDAO.selectAnlsReqRsltFileList");
 	List fileList = comService.selectCommonQueryList(reqParams);
 	model.addAttribute("fileList", fileList);*/
	
	String analReqSno = (String) reqParams.get("analReqSno");
	String evdcSno = (String) reqParams.get("evdcSno");

    model.addAttribute("anlsFileList",comService.selectFileList(analReqSno + "-" + evdcSno , "anlsPgs"));
    model.addAttribute("anlsTotalFileList",comService.selectFileList(analReqSno + "-" + evdcSno , "anlsPgsTotal"));
    model.addAttribute("anlsCheckFileList",comService.selectFileList(analReqSno + "-" + evdcSno , "anlsPgsCheck"));

	return new ModelAndView(ajaxMainView, model);

 }

	 /**
	 *
	 * <pre>
	 * 1. 메소드명 : updAnlsPgsUDtl
	 * 2. 작성일 : 2021. 4. 14.
	 * 3. 작성자 : leeji
	 * 4. 설명 :  분석지원요청 진행관리 분석결과수정시 호출
	 * </pre>
	 * @param paramData
	 * @param model
	 * @param request
	 * @return ModelAndView
	 * @throws Exception
	 */
	@RequestMapping(value="/anls/pgs/updAnlsPgsUDtl.do")
	public ModelAndView updAnlsPgsUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData) throws Exception {

		  FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

		   ObjectMapper mapper = new ObjectMapper();
		   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

		   //첨부파일 정보
		   ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("fileDatas");
		   //분석결과 정보
		   Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");

		   Map<String, Object> paramMap = new HashMap<String, Object>();
		   paramMap.put("fileList", tpList);
		   paramMap.put("userGb", user.getUserGb());
		   paramMap.put("userId", user.getUserId());
		   paramMap.put("insttCd", user.getInsttCd());
		   paramMap.putAll(formDataMap);
		   
		   model.addAttribute("analReqSno", paramMap.get("analReqSno"));
		   model.addAttribute("evdcSno", paramMap.get("evdcSno"));

		   anlsPgsService.updateAnlsPgsUDtl(paramMap);

		   return new ModelAndView(ajaxMainView, model);

	}

	/**
	 *
	 * <pre>
	 * 1. 메소드명 : delAnlsPgsUDtl
	 * 2. 작성일 : 2021. 4. 14.
	 * 3. 작성자 : leeji
	 * 4. 설명 :  분석지원요청 진행관리 분석결과삭제시 호출
	 * </pre>
	 * @param paramData
	 * @param model
	 * @param request
	 * @return ModelAndView
	 * @throws Exception
	 */
	@RequestMapping(value="/anls/pgs/delAnlsPgsUDtl.do")
	public ModelAndView delAnlsPgsUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData) throws Exception {

		  FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

		   ObjectMapper mapper = new ObjectMapper();
		   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

		   //첨부파일 정보
		   ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("fileDatas");
		   //분석결과 정보
		   Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");

		   Map<String, Object> paramMap = new HashMap<String, Object>();
		   paramMap.put("fileList", tpList);
		   paramMap.put("userGb", user.getUserGb());
		   paramMap.put("userId", user.getUserId());
		   paramMap.put("insttCd", user.getInsttCd());
		   paramMap.putAll(formDataMap);

		   anlsPgsService.deleteAnlsPgsUDtl(paramMap);
		   comService.deleteFile((String) formDataMap.get("analReqSno") , "anlsPgs");

		   return new ModelAndView(ajaxMainView, model);

	}

	/**
	   *
	   * <pre>
	   * 1. 메소드명 : queryAnlsPgsFile
	   * 2. 작성일 : 2021. 4. 22.
	   * 3. 작성자 : leeji
	   * 4. 설명 :  분석지원요청 진행관리 파일관련 호출시
	   * </pre>
	   * @param paramData
	   * @param model
	   * @param request
	   * @return ModelAndView
	   * @throws Exception
	   */
	  @RequestMapping(value="/anls/pgs/queryAnlsPgsFile.do")
	  public ModelAndView queryAnlsReqFile(HttpServletRequest request, ModelMap model, @RequestBody String paramData) throws Exception {

		  Map<String, Object> paramMap = new HashMap<String, Object>();

		  FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
		  paramMap.put("userGb", user.getUserGb());
		  paramMap.put("insttCd", user.getInsttCd());
		  paramMap.put("userId", user.getUserId());

		  ObjectMapper mapper = new ObjectMapper();
		  Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

		  Map<String, Object> anlsReqInfo = (Map<String, Object>) jsonObject.get("anlsReqInfo");
		  paramMap.put("anlsReqInfo", anlsReqInfo);

		  ArrayList<Map<String, Object>> tpList1 = (ArrayList<Map<String, Object>>) jsonObject.get("insFileArray");
		  paramMap.put("insFileList", tpList1);

		  //삭제할 첨부파일
		  ArrayList<Map<String, Object>> tpList2 = (ArrayList<Map<String, Object>>) jsonObject.get("delFileArray");
		  paramMap.put("delFileList", tpList2);

		  anlsPgsService.procAnlsPgsFile(paramMap);

		  return new ModelAndView(ajaxMainView, model);

	  }

	  /**
	   *
	   * <pre>
	   * 1. 메소드명 : indexAnlsPgsInfo
	   * 2. 작성일 : 2021. 4. 20.
	   * 3. 작성자 : leeji
	   * 4. 설명 :  분석지원요청 상세페이지 이동
	   * </pre>
	   * @param map
	   * @param model
	   * @param request
	   * @return
	   * @throws Exception
	   */
	  @RequestMapping(value="/anls/pgs/indexAnlsPgsInfo.do")
	  public String indexAnlsReqInfo(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

	      return "dems/anls/pgs/anlsPgsInfo";
	  }

	  /**
	   *
	   * <pre>
	   * 1. 메소드명 : indexAnlsPgsEvdcHistUDtl
	   * 2. 작성일 : 2021. 4. 20.
	   * 3. 작성자 : leeji
	   * 4. 설명 :  @!@ 증거분석이력 상세페이지 이동
	   * </pre>
	   * @param map
	   * @param model
	   * @param request
	   * @return
	   * @throws Exception
	   */
	  @RequestMapping(value="/anls/pgs/indexAnlsPgsEvdcHistUDtl.do")
	  public String indexAnlsPgsEvdcHistUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

	      return "dems/anls/pgs/anlsPgsEvdcHistUDtl";
	  }

	  /**
	   *
	   * <pre>
	   * 1. 메소드명 : queryAnlsPgsEvdcHistMList
	   * 2. 작성일 : 2021. 5. 17. 오후 6:10:31
	   * 3. 작성자 : jij
	   * 4. 설명 : @!@ 증거분석이력 리스트 조회
	   * </pre>
	   * @param request
	   * @param model
	   * @param reqParams
	   * @param response
	   * @return
	   * @throws Exception
	   */
	  @RequestMapping(value="/anls/pgs/queryAnlsPgsEvdcHistMList.do")
	  public ModelAndView queryAnlsPgsEvdcHistMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		  reqParams.put("sqlQueryId", "anlsPgsDAO.queryAnlsPgsEvdcHistMListTotCnt");
		  int totCnt = comService.selectCommonQueryListTotCnt(reqParams);

		  reqParams.put("totalCount", totCnt);
		  PageUtil.calcPage(reqParams);

		  reqParams.put("sqlQueryId", "anlsPgsDAO.queryAnlsPgsEvdcHistMList");
		  List list = comService.selectCommonQueryList(reqParams);


		  reqParams.put("list", list);
		  model.addAllAttributes(reqParams);

		  return new ModelAndView(ajaxMainView, model);

	  }

	  /**
	   *
	   * <pre>
	   * 1. 메소드명 : regAnlsPgsEvdcHistUDtl
	   * 2. 작성일 : 2021. 5. 17. 오후 6:51:16
	   * 3. 작성자 : jij
	   * 4. 설명 : @!@ 증거분석이력 등록
	   * </pre>
	   * @param request
	   * @param reqParams
	   * @param model
	   * @param status
	   * @return
	   * @throws Exception
	   */
	  @RequestMapping("/anls/pgs/regAnlsPgsEvdcHistUDtl.do")
	  public ModelAndView regAnlsPgsEvdcHistUDtl(HttpServletRequest request, @RequestParam HashMap reqParams, ModelMap model,  SessionStatus status) throws Exception {

		  HashMap<String, Object> fileMap = DemsUtil.convertMap(request);

		  String message = anlsPgsService.regAnlsPgsEvdcHistUDtl(reqParams, fileMap);

		  model.addAttribute(DemsConst.Messages_SysSucMessage, message);

		  return new ModelAndView(ajaxMainView, model);

	  }

	  /**
	   *
	   * <pre>
	   * 1. 메소드명 : delAnlsPgsEvdcHistUDtl
	   * 2. 작성일 : 2021. 5. 17. 오후 6:51:16
	   * 3. 작성자 : jij
	   * 4. 설명 : @!@ 증거분석이력 삭제
	   * </pre>
	   * @param request
	   * @param reqParams
	   * @param model
	   * @param status
	   * @return
	   * @throws Exception
	   */
	  @RequestMapping("/anls/pgs/delAnlsPgsEvdcHistUDtl.do")
	  public ModelAndView delAnlsPgsEvdcHistUDtl(HttpServletRequest request, @RequestParam HashMap reqParams, ModelMap model,  SessionStatus status) throws Exception {

		  String userGb = (String) reqParams.get("session_usergb");
		  String pgsStat = "";
		  Map anlsReqInfoMap = new HashMap<>();
		  if(userGb.equals("C01003") || userGb.equals("C01004")) {
			  reqParams.put("sqlQueryId", "anlsPgsDAO.selectAnlsReqInfo");
			  anlsReqInfoMap = comService.selectCommonQueryMap(reqParams);
			  pgsStat = (String) anlsReqInfoMap.get("pgsStat");
			  if(pgsStat == null || pgsStat.equals("") || pgsStat.equals("C03001") || pgsStat.equals("C03002")){
				  throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.005", null));
			  }
		  }else{
			  throw new MException(egovMessageSource.getMessageArgs("dems.site.rcv.008", null));
		  }

			  reqParams.put("sqlQueryId", "anlsPgsDAO.delAnlsPgsEvdcHistUDtl");
		  comService.updateCommonQuery(reqParams);

		  model.addAttribute(DemsConst.Messages_SysSucMessage,
				  egovMessageSource.getMessageArgs("success.common.save", null));

		  return new ModelAndView(ajaxMainView, model);

	  }

}
