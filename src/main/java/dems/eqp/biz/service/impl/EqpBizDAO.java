package dems.eqp.biz.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("eqpBizDAO")
@SuppressWarnings({"rawtypes","unchecked"})
public class EqpBizDAO extends EgovComAbstractDAO {

	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 사업관리 등록 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
//	public void regEqpBizRDtl(HashMap<String, String> map) throws Exception {
//		insert("eqpBizDAO.regEqpBizRDtl", map);
//
//	}
	
	public int regEqpBizRDtl(Map<String, Object> paramMap) throws Exception {

    	return (Integer) insert("eqpBizDAO.regEqpBizRDtl", paramMap);
    }
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 사업관리 수정 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
//	public void updEqpBizUDtl(HashMap<String, String> map) throws Exception {
//		insert("eqpBizDAO.updEqpBizUDtl", map);
//
//	}
	public void updEqpBizUDtl(Map<String, Object> paramMap) throws Exception {
        update("eqpBizDAO.updEqpBizUDtl", paramMap);
    }

	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : delEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:48:53
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 사업관리 삭제 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	public void delEqpBizUDtl(HashMap<String, String> map) throws Exception {
		insert("eqpBizDAO.delEqpBizUDtl", map);

	}
	
}

