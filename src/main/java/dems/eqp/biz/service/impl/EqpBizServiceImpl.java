package dems.eqp.biz.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import dems.bbs.free.service.impl.BbsFreeServiceImpl;
import dems.eqp.biz.service.EqpBizService;
import frame.fcom.service.ComService;

/**
 * 
 * <pre>
 * 1. 클래스명 : EqpBizServiceImpl.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비관리
 * </pre>
 */
@Service("EqpBizService")
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class EqpBizServiceImpl extends EgovAbstractServiceImpl implements	EqpBizService{
	
	protected Logger log = LoggerFactory.getLogger(BbsFreeServiceImpl.class);
	
	@Resource(name="eqpBizDAO")
	private EqpBizDAO eqpBizDAO;
	
	@Resource(name="comService")
	private ComService comService;
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 사업관리 등록 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
//	public void regEqpBizRDtl(HashMap<String, String> map) throws Exception{
//		// TODO Auto-generated method stub
//		
//		//장비관리 등록 처리
//		eqpBizDAO.regEqpBizRDtl(map);
//		
//		//부서별 사용자 등록처리
//		//eqpMgmtDAO.regFsysDeptUserRDtl(map);
//				
//				
////		if( "lvl1".equals(map.get("menuLvl"))) {
////			menuDAO.updateUpAdmMenuOrdr(map);
////		}
//	}
	public String regEqpBizRDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {		
		
		String result = "";

		boolean denyListChk = true;
		String[] eList = EgovProperties.getProperty("Globals.fileUpload.Extensions").toString().split("\\.");

        if(fileMap.get("fileList4OriFileNm") != null){
            if (fileMap.get("fileList4OriFileNm") instanceof String) {
            	String fileList = (String) fileMap.get("fileList4OriFileNm");
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
        	}else {
        		List<String> fileList = (List<String>) fileMap.get("fileList4OriFileNm");
        		for (int i = 0; i < fileList.size(); i++) {
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.get(i).toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
    			}
        	}
        }

	 	if (denyListChk && fileMap.get("fileList4OriFileNm") != null) {
	 		result = "2";
        }else{
	    	//첨부파일등록
			String loginId = (String) paramMap.get("regrId");
	    	String atchFileId = (String) fileMap.get("atchFileId");

	    	if(EgovStringUtil.isNull(atchFileId)){
	    		atchFileId = comService.insertAtchFile("fileList4", "10", loginId, fileMap);
	    	}else{
	    		atchFileId = comService.updateAtchFile(atchFileId, "fileList4", "10", loginId, fileMap);
	    	}
	    	paramMap.put("atchFileId", atchFileId);
	    	//bbsFreeDAO.insertBbsFrees(paramMap);
	    	
	    	eqpBizDAO.regEqpBizRDtl(paramMap);
	    	
	    	result = "1";
        }

        return result;
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 사업관리 수정 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String updEqpBizUDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {	
		// TODO Auto-generated method stub
		log.debug("========updEqpBizUDtl  loginId111111111111==========>>>");
		log.debug("========updEqpBizUDtl  atchFileId22222222222==========>>>");
		
		String result = "";
		String atchFileId = (String) paramMap.get("atchFileId");
		String loginId =  (String) paramMap.get("regrId");
		
		log.debug("========updEqpBizUDtl  loginId==========>>>"+loginId);
		log.debug("========updEqpBizUDtl  atchFileId==========>>>"+atchFileId);
		
		boolean denyListChk = true;
        String[] eList = EgovProperties.getProperty("Globals.fileUpload.Extensions").toString().split("\\.");
		
        if(fileMap.get("fileList4OriFileNm") != null){
            if (fileMap.get("fileList4OriFileNm") instanceof String) {
            	String fileList = (String) fileMap.get("fileList4OriFileNm");
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
        	}else {
        		List<String> fileList = (List<String>) fileMap.get("fileList4OriFileNm");
        		for (int i = 0; i < fileList.size(); i++) {
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.get(i).toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
    			}
        	}
        }
        
        if (denyListChk && fileMap.get("fileList4OriFileNm") != null) {
	 		result = "2";
        }else{
	       	if(EgovStringUtil.isNull(atchFileId)){
	       		atchFileId = comService.insertAtchFile("fileList4", "10", loginId, fileMap);
	       	}else{
	       		atchFileId = comService.updateAtchFile(atchFileId, "fileList4", "10", loginId, fileMap);
	       	}

//	       	paramMap.put("atchFileId", atchFileId);
//	    	bbsFreeDAO.updateBbsFree(paramMap);
	       	
	       	//장비관리 수정 처리
	       	paramMap.put("atchFileId", atchFileId);
			eqpBizDAO.updEqpBizUDtl(paramMap);
	       	
	    	result = "1"; 
        }
        
		
		
		return result; 
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : delEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 사업관리 삭제 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public void delEqpBizUDtl(HashMap<String, String> map) throws Exception{
		// TODO Auto-generated method stub
		
		//장비정보 삭제 처리
		eqpBizDAO.delEqpBizUDtl(map);
				
				
//		if( "lvl1".equals(map.get("menuLvl"))) {
//			menuDAO.updateUpAdmMenuOrdr(map);
//		}
	}
}

