package dems.eqp.biz.web;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.eqp.biz.service.EqpBizService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.fsys.user.service.FsysUserVO;
import frame.futil.DemsConst;
import frame.futil.DemsUtil;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;


/**
 * 
 * <pre>
 * 1. 클래스명 : EqpBizController.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비관리 > 사업관리
 * </pre>
 */
@Controller
public class EqpBizController {

	protected Logger log = LoggerFactory.getLogger(EqpBizController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;
	
	@Resource(name = "EqpBizService")
	private EqpBizService eqpBizService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

    @Resource MappingJackson2JsonView ajaxMainView;

    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexEqpBizMList
     * 2. 작성일 : 2021. 4. 16. 오전 10:49:11
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 사업관리 조회 이동
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/biz/indexEqpBizMList.do")
    public String indexEqpBizMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {
    	
    	model.addAttribute("message", request.getParameter("message"));
    	if(request.getParameter("msg") != null){
    		model.addAttribute("msg", request.getParameter("msg"));
    	}
    	return "dems/eqp/biz/eqpBizMList";
    }


	/**
     * 
     * <pre>
     * 1. 메소드명 : queryEqpBizMList
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 사업관리 조회 처리
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/biz/queryEqpBizMList.do")
    public ModelAndView queryEqpBizMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "eqpBizDAO.queryEqpBizMListTotCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	
    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		reqParams.put("sqlQueryId", "eqpBizDAO.queryEqpBizMList");
		List list = comService.selectCommonQueryList(reqParams);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexEqpBizRDtl
     * 2. 작성일 : 2021. 4. 16. 오전 9:15:21
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 사업관리 등록 화면으로 이동
     * </pre>
     * @param map
     * @param request
     * @param fsysUserVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/biz/indexEqpBizRDtl.do")
    public String indexEqpBizRDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, @ModelAttribute("fsysUserVO") FsysUserVO fsysUserVO, ModelMap model) throws Exception {

    	//공통으로 필요한 데이타.(권한체크등...)
    	// model = comController.common(request, model);

    	//String userId = map.get("userId");

		/*map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperList");
		List sysGrpList = comService.selectCommonQueryList(map);
		model.addAttribute("sysGrpList", sysGrpList);
		
		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperId");
		Map rtnMap = comService.selectCommonQueryMap(map);
		model.addAttribute("topMenuNo",rtnMap.get("topMenuNo"));*/

    	return "dems/eqp/biz/eqpBizRDtl";
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : regEqpBizRDtl
     * 2. 작성일 : 2021. 4. 16. 오전 9:46:21
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 사업관리 등록 처리
     * </pre>
     * @param map
     * @param model
     * @param request
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping("/eqp/biz/regEqpBizRDtl.do")
	public ModelAndView regEqpBizRDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
		
    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	String errMsg = "";

		HashMap<String, String>requireParams = new HashMap<>();
		HashMap<String, String>chkParams = new HashMap<>();
		chkParams.putAll(reqParams);
		requireParams.put("bizNm", "사업명");
		requireParams.put("cntrNo", "계약번호");
		requireParams.put("cntrAmt", "계약금액");
		requireParams.put("cntrForm", "계약형태");
		requireParams.put("bizCnts", "사업내용");
		requireParams.put("bizStartDt", "사업시작일");
		requireParams.put("bizEndDt", "사업종료일");
		requireParams.put("coNm", "업체명");
		requireParams.put("coCrgr", "업체담당자");
        validateUtil.check(chkParams, requireParams);

    	try{
    		
    		
//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuLvl2Cnt");
//    		lvl2Cnt = comService.selectCommonQueryListTotCnt(map);
//    		if(map.get("menuLvl").equals("lvl2") && lvl2Cnt > 98){
//    			System.out.println("고마 느라 마이 느따 아이가");
//    			model.addAttribute(DemsConst.Messages_UserComMessage, egovMessageSource.getMessageArgs("admin.menu.001", null));
//    			return new ModelAndView(ajaxMainView, model);
//    		}
    		
//    		map.put("pwd",DemsUtil.encryptSHA256(map.get("pwd")));
//    		map.put("sqlQueryId", "fsysUserDAO.regFsysUserRDtl");
//    		comService.updateCommonQuery(map);
    		
    		String sRturnResult = "";
    		
    		HashMap<String, Object> fileMap = DemsUtil.convertMap(request);
    		
    		reqParams.put("userId", user.getUserId());;
    		
    		sRturnResult = eqpBizService.regEqpBizRDtl(reqParams,fileMap);
    		
    		if(sRturnResult.equals("2")){
    			model.addAttribute(DemsConst.Messages_UserComMessage,"보안상 업로드할 수 없는 파일이 있어 저장하지 못하였습니다.");			
    		}

            //return new ModelAndView(ajaxMainView, model);
    		
//    		map.put("sqlQueryId", "fsysMenuDAO.updFsysMenuRDtlOrdr");
//    		comService.updateCommonQuery(map);
//    		
//    		if(map.get("menuLvl").equals("lvl2") && lvl2Cnt > 1){
//    			map.put("sqlQueryId", "fsysMenuDAO.updFsysMenuNoReMake");
//    			comService.updateCommonQuery(map);
//    		}
    		
//    		model.addAttribute(DemsConst.Messages_UserComMessage, egovMessageSource.getMessageArgs("success.common.insert", null));
    	}catch (Exception e) {
    		e.printStackTrace();
    		errMsg = URLEncoder.encode(egovMessageSource.getMessage("fail.request.msg"), "UTF-8");
    	}
    	
		return new ModelAndView(ajaxMainView, model);
		
	}
    
    /**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 사업관리 수정 화면 이동
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/biz/indexEqpBizUDtl.do")
	public String indexEqpBizUDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/eqp/biz/eqpBizUDtl";
		
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : queryEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:49:20
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 사업관리 장비상세정보 조회
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("/eqp/biz/queryEqpBizUDtl.do")
	public ModelAndView queryEqpBizUDtl(@RequestParam HashMap<String,String> map, HttpServletRequest request, ModelMap model) throws Exception {
    //public ModelAndView queryEqpBizUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		//공통으로 필요한 데이타.(권한체크등...)
    	//model = comController.common(request, model);


		String errMsg = "";

    	try{
    		
    		
//    		Map<String, Object> paramMap = new HashMap<String, Object>();
//            paramMap.put("putupSno", Integer.parseInt((String) reqParams.get("putupSno")));
//            paramMap.put("boardKind", "C23009");
//
//            Map bbsFreeMap = bbsFreeService.selectBbsFreeRDtl(paramMap);
//            String sTemp = DemsUtil.getHtmlStrCnvr((String) bbsFreeMap.get("cnts"));
//            bbsFreeMap.put("cntsCvt", sTemp);
//
//    	    List<FileVO> atchFileList = comService.selectAtchFileList((String) bbsFreeMap.get("atchFileId"),null);
//    	    bbsFreeService.updateBbsFreeCount(paramMap);
//    	    model.addAttribute("bbsFreeMap", bbsFreeMap);
//    	    model.addAttribute("atchFileList",atchFileList);
//        	
//        	 return new ModelAndView(ajaxMainView, model);
    		log.debug("========사업관리 장비상세정보 bizSno==========================>>>>"+map.get("bizSno"));
    		
    		if(map.get("bizSno") != null && !map.get("bizSno").equals("")) {
    			//log.debug("========사업관리 장비상세정보 atchFileId===========>>>>"+map.get("atchFileId"));
    			
    			//if(map.get("atchFileId") != null && !map.get("atchFileId").equals("")) {
    			List<FileVO> atchFileList = comService.selectAtchFileList((String) map.get("atchFileId"),null);
    			//}
    			
    			
    			map.put("sqlQueryId", "eqpBizDAO.queryEqpBizUDtl");
    			Map resultMap = comService.selectCommonQueryMap(map);
    			
    			model.addAttribute("resultMap", resultMap);
    			model.addAttribute("atchFileList", atchFileList);
    		}

    	}catch (Exception e) {
    		e.printStackTrace();
    		errMsg = URLEncoder.encode(egovMessageSource.getMessage("fail.request.msg"), "UTF-8");
    	}
    	
    	return new ModelAndView(ajaxMainView, model);

    }
    
    /**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 14. 오전 11:20:39
	 * 3. 작성자 : 
	 * 4. 설명 : 장비 지원 관리 > 사업관리 상세정보 수정 처리
	 * </pre>
	 * 
	 * @param map
	 * @param model
	 * @param request
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/eqp/biz/updEqpBizUDtl.do")
	//public ModelAndView updEqpBizUDtl(@RequestParam HashMap<String, String> map, ModelMap model, HttpServletRequest request, SessionStatus status) throws Exception {
	public ModelAndView updEqpBizUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
		
		FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String sRturnResult = "";
		
		HashMap<String, Object> fileMap = DemsUtil.convertMap(request);

		HashMap<String, String>requireParams = new HashMap<>();
		HashMap<String, String>chkParams = new HashMap<>();
		chkParams.putAll(reqParams);
		requireParams.put("bizNm", "사업명");
		requireParams.put("cntrNo", "계약번호");
		requireParams.put("cntrAmt", "계약금액");
		requireParams.put("cntrForm", "계약형태");
		requireParams.put("bizCnts", "사업내용");
		requireParams.put("bizStartDt", "사업시작일");
		requireParams.put("bizEndDt", "사업종료일");
		requireParams.put("coNm", "업체명");
		requireParams.put("coCrgr", "업체담당자");
        validateUtil.check(chkParams, requireParams);
		
		reqParams.put("userId",user.getUserId());
		sRturnResult = eqpBizService.updEqpBizUDtl(reqParams, fileMap);

		model.addAttribute(DemsConst.Messages_SysSucMessage,
				egovMessageSource.getMessageArgs("success.common.update", null));
		
		return new ModelAndView(ajaxMainView, model);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : delEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 14. 오전 11:20:53
	 * 3. 작성자 : jij
	 * 4. 설명 : 장비 지원 관리 > 사업관리 상세정보 삭제 처리
	 * </pre>
	 * 
	 * @param map
	 * @param model
	 * @param request
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/eqp/biz/delEqpBizUDtl.do")
	public ModelAndView delEqpBizUDtl(@RequestParam HashMap<String, String> map, ModelMap model,
			HttpServletRequest request, SessionStatus status) throws Exception {

//		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuLowerCnt");
//
//		int totCnt = comService.selectCommonQueryListTotCnt(map);
//
//		if (totCnt > 0) {
//
//			model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("admin.menu.001", null));
//
//		} else {
//
//			map.put("sqlQueryId", "fsysMenuDAO.delFsysMenuUDtl");
//			comService.updateCommonQuery(map);
//			model.addAttribute(DemsConst.Messages_SysSucMessage,
//					egovMessageSource.getMessageArgs("success.common.delete", null));
//
//		}
		
//		map.put("sqlQueryId", "fsysUserDAO.delFsysUserUDtl");
//		comService.updateCommonQuery(map);
		
		eqpBizService.delEqpBizUDtl(map);
		
		model.addAttribute(DemsConst.Messages_SysSucMessage,
				egovMessageSource.getMessageArgs("success.common.delete", null));

		return new ModelAndView(ajaxMainView, model);

	}
	
	/**
     * 
     * <pre>
     * 1. 메소드명 : queryEqpBizFIndEqupQListPop
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 사업관리 조회 팝업
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception /eqp/mgmt/queryEqpMgmtFIndEqupQListPop.do
     */
    @RequestMapping(value="/eqp/biz/queryEqpBizFIndEqupQListPop.do")
    public ModelAndView queryEqpBizFIndEqupQListPop(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "eqpBizDAO.queryEqpBizMListTotCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	
    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		reqParams.put("sqlQueryId", "eqpBizDAO.queryEqpBizMList");
		List list = comService.selectCommonQueryList(reqParams);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
	

}
