package dems.eqp.lend.web;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovDateUtil;

import dems.eqp.lend.service.EqpLendService;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.fsys.user.service.FsysUserVO;
import frame.futil.DemsConst;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;


/**
 * 
 * <pre>
 * 1. 클래스명 : EqpLendController.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비 지원 관리 > 장비대여관리
 * </pre>
 */
@Controller
public class EqpLendController {

	protected Logger log = LoggerFactory.getLogger(EqpLendController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;
	
	@Resource(name = "eqpLendService")
	private EqpLendService eqpLendService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

    @Resource MappingJackson2JsonView ajaxMainView;

    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexEqpLendMList
     * 2. 작성일 : 2021. 4. 16. 오전 10:49:11
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 장비대여관리 조회 이동
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/lend/indexEqpLendMList.do")
    public String indexEqpLendMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {
    	
    	model.addAttribute("message", request.getParameter("message"));
    	if(request.getParameter("msg") != null){
    		model.addAttribute("msg", request.getParameter("msg"));
    	}
    	return "dems/eqp/lend/eqpLendMList";
    }


	/**
     * 
     * <pre>
     * 1. 메소드명 : queryEqpMntMList
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 장비대여관리 조회 처리
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/lend/queryEqpLendMList.do")
    public ModelAndView queryEqpLendMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "eqpLendDAO.queryEqpLendMListTotCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	
    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		reqParams.put("sqlQueryId", "eqpLendDAO.queryEqpLendMList");
		List list = comService.selectCommonQueryList(reqParams);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
    
    
    /**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 등록 화면 이동
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/lend/indexEqpMntRDtl.do")
	public String indexEqpMntRDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/eqp/lend/eqpMntRDtl";
		
	}
	
    /**
	 * 
	 * <pre>
	 * 1. 메소드명 : queryEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:49:20
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 사업관리 장비상세정보 조회
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("/eqp/lend/queryEqpBizUDtl.do")
	public ModelAndView queryEqpBizUDtl(@RequestParam HashMap<String,String> map, HttpServletRequest request, ModelMap model) throws Exception {
    //public ModelAndView queryEqpBizUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		//공통으로 필요한 데이타.(권한체크등...)
    	//model = comController.common(request, model);


		String errMsg = "";

    	try{
    		
    		
//    		Map<String, Object> paramMap = new HashMap<String, Object>();
//            paramMap.put("putupSno", Integer.parseInt((String) reqParams.get("putupSno")));
//            paramMap.put("boardKind", "C23009");
//
//            Map bbsFreeMap = bbsFreeService.selectBbsFreeRDtl(paramMap);
//            String sTemp = DemsUtil.getHtmlStrCnvr((String) bbsFreeMap.get("cnts"));
//            bbsFreeMap.put("cntsCvt", sTemp);
//
//    	    List<FileVO> atchFileList = comService.selectAtchFileList((String) bbsFreeMap.get("atchFileId"),null);
//    	    bbsFreeService.updateBbsFreeCount(paramMap);
//    	    model.addAttribute("bbsFreeMap", bbsFreeMap);
//    	    model.addAttribute("atchFileList",atchFileList);
//        	
//        	 return new ModelAndView(ajaxMainView, model);
    		
    		if(map.get("bizSno") != null && !map.get("bizSno").equals("")) {
    			log.debug("========사업관리 장비상세정보 atchFileId===========>>>>"+map.get("atchFileId"));
    			List<FileVO> atchFileList = comService.selectAtchFileList((String) map.get("atchFileId"),null);
    			
    			map.put("sqlQueryId", "eqpBizDAO.queryEqpBizUDtl");
    			Map resultMap = comService.selectCommonQueryMap(map);
    			
    			model.addAttribute("resultMap", resultMap);
    			model.addAttribute("atchFileList", atchFileList);
    		}

    	}catch (Exception e) {
    		e.printStackTrace();
    		errMsg = URLEncoder.encode(egovMessageSource.getMessage("fail.request.msg"), "UTF-8");
    	}
    	
    	return new ModelAndView(ajaxMainView, model);

    }
    
    /**
    *
    * <pre>
    * 1. 메소드명 : lendReqEqpLendLendReqRDtl
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여신청
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
   @RequestMapping(value="/eqp/lend/lendReqEqpLendLendReqRDtl.do")
   public ModelAndView lendReqEqpLendLendReqRDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	
	   	ObjectMapper mapper = new ObjectMapper();
	   	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
	   	Map<String, Map<String, String>> jsonString = mapper.readValue(paramData, Map.class);
	
	   	//장비대여 대여장비정보
	   	Map<String, Object> formDataMap1 = (Map<String, Object>) jsonObject.get("formDatas1");
	   	
		Map<String, Object> formDataMap2 = (Map<String, Object>) jsonObject.get("formDatas2");
		
		Map<String, Object> formDataMap3 = (Map<String, Object>) jsonObject.get("formDatas3");
		
		Map<String, Object> formDataMap4 = (Map<String, Object>) jsonObject.get("formDatas4");
	   	
	   	//장비대여 신청정보
	   	ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
	   	
	   	
	   	log.debug("======CON regEqpMntRDtl formDataMap1========>>>"+formDataMap1.size());
	   	log.debug("======CON regEqpMntRDtl formDataMap2========>>>"+formDataMap2.size());
	   	if(formDataMap3 != null && !formDataMap3.isEmpty()) {
	   		log.debug("======CON regEqpMntRDtl formDataMap3========>>>"+formDataMap3.size());
	   	}
	   	
	   	log.debug("======CON regEqpMntRDtl tpList========>>>"+tpList.size());
	   	
	   	Map<String, Object> paramMap = new HashMap<String, Object>();
	   	paramMap.put("formDataMap1", formDataMap1);
	   	paramMap.put("formDataMap2", formDataMap2);
	   	paramMap.put("formDataMap3", formDataMap3);
	   	paramMap.put("formDataMap4", formDataMap4);
	   	paramMap.put("detailList", tpList);
	   	paramMap.put("userGb", user.getUserGb());
	   	paramMap.put("userId", user.getUserId());
	   	paramMap.put("insttCd", user.getInsttCd());
	   	
	   	log.debug("======CON regEqpMntRDtl user.getInsttCd()========>>>"+user.getInsttCd());
	   	
        //필수값 확인
        HashMap<String,String> requireParams = new HashMap();
        /*HashMap<String,String> chkParams = (HashMap<String,String>) jsonString.get("rowDatas");*/
        HashMap<String,String> chkParams = new HashMap();
        Map<String, String> chkDataMap1 = (Map<String, String>) jsonString.get("formDatas1");
	   	
		Map<String, String> chkDataMap2 = (Map<String, String>) jsonString.get("formDatas2");
        chkParams.putAll(chkDataMap1);
        chkParams.putAll(chkDataMap2);
        
        requireParams.put("rentAplnDt", "대여신청일자");
        requireParams.put("rentStartDt", "대여시작일자");
        requireParams.put("rentEndDt", "대여종료일자");
        requireParams.put("rentReqInfo", "대여요청사항");
        validateUtil.check(chkParams, requireParams);
	
	   	eqpLendService.modifyEqpLendRequest(paramMap);

	   	return new ModelAndView(ajaxMainView, model);

   }
   
   /**
   *
   * <pre>
   * 1. 메소드명 : ModifySelEqpLendRequest
   * 2. 작성일 : 2021. 4. 30.
   * 3. 작성자 : ilyong
   * 4. 설명 :  장비 지원 관리 > 장비대여관리 리스트에서 선택한 내역 반납처리
   * </pre>
   * @param map
   * @param model
   * @param request
   * @return
   * @throws Exception
   */
  @RequestMapping(value="/eqp/lend/ModifySelEqpLendRequest.do")
  public ModelAndView ModifySelEqpLendRequest(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	   
	   ObjectMapper mapper = new ObjectMapper();
	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
	
	   //요청 리스트
	   List tpList = (List) jsonObject.get("rowDatas");
	   
	   //요청 타입
	   Map<String, Object> infoMap = jsonObject.get("type");
	   String type = (String) infoMap.get("type");
	
	   Map<String, Object> paramMap = new HashMap<String, Object>();
	   paramMap.put("rtnCfrmList", tpList);
	   paramMap.put("userGb", user.getUserGb());
	   paramMap.put("userId", user.getUserId());
	   paramMap.put("insttCd", user.getInsttCd());
	   paramMap.put("type", type);
	
	   eqpLendService.ModifySelEqpLendRequest(paramMap);
	   
	   return new ModelAndView(ajaxMainView, model);

  }
   
   /**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEqpLendLendReqRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여신청 화면 이동
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/lend/indexEqpLendLendReqRDtl.do")
	public String indexEqpLendLendReqRDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/eqp/lend/eqpLendLendReqRDtl";
		
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEqpLendLendReqUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여신청 수정화면 이동
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/lend/indexEqpLendLendReqUDtl.do")
	public String indexEqpLendLendReqUDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/eqp/lend/eqpLendLendReqUDtl";
		
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEqpLendLendReqVDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여신청 상세화면 이동
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/lend/indexEqpLendLendReqVDtl.do")
	public String indexEqpLendLendReqVDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/eqp/lend/eqpLendLendReqVDtl";
		
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEqpLendAprvUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여신청 수정화면 이동(포렌식 수사관 화면)
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/lend/indexEqpLendAprvUDtl.do")
	public String indexEqpLendAprvUDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/eqp/lend/eqpLendAprvUDtl";
		
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : queryEqpLendReqInfo
	 * 2. 작성일 : 2021. 4. 16. 오전 9:49:20
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여신청 상세정보 조회
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("/eqp/lend/queryEqpLendReqInfo.do")
	public ModelAndView queryEqpLendReqInfo(@RequestParam HashMap<String,String> map, HttpServletRequest request, ModelMap model) throws Exception {

		//공통으로 필요한 데이타.(권한체크등...)
    	//model = comController.common(request, model);


		String errMsg = "";

    	try{
    		
//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperList");
//    		List sysGrpList = comService.selectCommonQueryList(map);
//    		model.addAttribute("sysGrpList", sysGrpList);
//    		
//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperId");
//    		Map rtnMap = comService.selectCommonQueryMap(map);
//    		model.addAttribute("topMenuNo",rtnMap.get("topMenuNo"));
    		
    		log.debug("=====queryEqpMgmtUDtl.do rentAplnSno=======>>>"+map.get("rentAplnSno"));
    		if(map.get("rentAplnSno") != null && !map.get("rentAplnSno").equals("")) {
    			map.put("sqlQueryId", "eqpLendDAO.queryEqpLendReqInfo");
    			Map resultMap = comService.selectCommonQueryMap(map);
    			model.addAttribute("resultMap", resultMap);
    		}

    	}catch (Exception e) {
    		e.printStackTrace();
    		errMsg = URLEncoder.encode(egovMessageSource.getMessage("fail.request.msg"), "UTF-8");
    	}
    	
    	return new ModelAndView(ajaxMainView, model);

    }
	
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryEqpLendDtlList
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 대여장비목록 조회
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/lend/queryEqpLendDtlList.do")
    public ModelAndView queryEqpLendDtlList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "eqpLendDAO.queryEqpLendDtlListTotCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	
    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		reqParams.put("sqlQueryId", "eqpLendDAO.queryEqpLendDtlList");
		List list = comService.selectCommonQueryList(reqParams);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexEqpLendAprvMList
     * 2. 작성일 : 2021. 4. 16. 오전 10:49:11
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 장비대여관리 조회 이동(포렌식 수사관)
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/lend/indexEqpLendAprvMList.do") 
    public String indexEqpLendAprvMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {
    	
    	model.addAttribute("message", request.getParameter("message"));
    	if(request.getParameter("msg") != null){
    		model.addAttribute("msg", request.getParameter("msg"));
    	}
    	return "dems/eqp/lend/eqpLendAprvMList";
    }


	/**
     * 
     * <pre>
     * 1. 메소드명 : queryEqpMntMList
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 장비대여관리 조회 처리(포렌식 수사관)
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/lend/queryEqpLendAprvMList.do")
    public ModelAndView queryEqpLendAprvMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "eqpLendDAO.queryEqpLendMListTotCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	
    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		reqParams.put("sqlQueryId", "eqpLendDAO.queryEqpLendMList");
		List list = comService.selectCommonQueryList(reqParams);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
    
    /**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEqpLendAprvRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여신청 승인화면 이동(포렌식 수사관)
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/lend/indexEqpLendAprvRDtl.do")
	public String indexEqpLendAprvRDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/eqp/lend/eqpLendAprvRDtl";
		
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : lendReqEqpLendAprvRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:49:20
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여신청 승인화면 조회
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("/eqp/lend/lendReqEqpLendAprvRDtl.do")
	public ModelAndView lendReqEqpLendAprvRDtl(@RequestParam HashMap<String,String> map, HttpServletRequest request, ModelMap model) throws Exception {

		//공통으로 필요한 데이타.(권한체크등...)
    	//model = comController.common(request, model);


		String errMsg = "";

    	try{
    		
//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperList");
//    		List sysGrpList = comService.selectCommonQueryList(map);
//    		model.addAttribute("sysGrpList", sysGrpList);
//    		
//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperId");
//    		Map rtnMap = comService.selectCommonQueryMap(map);
//    		model.addAttribute("topMenuNo",rtnMap.get("topMenuNo"));
    		
    		log.debug("=====queryEqpMgmtUDtl.do rentAplnSno=======>>>"+map.get("rentAplnSno"));
    		if(map.get("rentAplnSno") != null && !map.get("rentAplnSno").equals("")) {
    			map.put("sqlQueryId", "eqpLendDAO.queryEqpLendReqInfo"); 
    			Map resultMap = comService.selectCommonQueryMap(map);
    			model.addAttribute("resultMap", resultMap);
    		}

    	}catch (Exception e) {
    		e.printStackTrace();
    		errMsg = URLEncoder.encode(egovMessageSource.getMessage("fail.request.msg"), "UTF-8");
    	}
    	
    	return new ModelAndView(ajaxMainView, model);

    }
    
    /**
    *
    * <pre>
    * 1. 메소드명 : queryAnlsReqPop
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비대여 등록시 사건조회 팝업 호출
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
   @RequestMapping(value="/eqp/lend/queryIncidentPop.do")
   public ModelAndView queryIncidentPop(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

	    FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	   	reqParams.put("regrId", user.getUserId());
	   	reqParams.put("insttCd", user.getInsttCd());
	   	
	   	String type = (String) reqParams.get("type");
	   	String userGb = "";
	   	
	   	if(type.equals("prosr")) {
	   		userGb = "C01002";
	   	}else if(type.equals("regUser")) {
	   		userGb = "C01001";
	   	}else if(type.equals("analCrgr")) {
	   		userGb = "C01003";
	   	}
	   	
	   	reqParams.put("sqlQueryId", "eqpLendDAO.selectIncidentListTotCnt");
	   	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
	   	reqParams.put("totalCount", totCnt);
	   	
	   	PageUtil.calcPage(reqParams);
	   	
	   	reqParams.put("sqlQueryId", "eqpLendDAO.selectIncidentList");
	   	
		List popList = comService.selectCommonQueryList(reqParams);		
		reqParams.put("popList", popList);
		model.addAllAttributes(reqParams);
		
	   return new ModelAndView(ajaxMainView, model);

   }
   
   /**
    * 
    * <pre>
    * 1. 메소드명 : lendTermDupChk
    * 2. 작성일 : 2021. 4. 16. 오전 9:46:51
    * 3. 작성자 : ilyong
    * 4. 설명 : 장비대여 등록시 대여기간 중복 체크
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@RequestMapping("/eqp/lend/lendTermDupChk.do")
	public  ModelAndView lendTermDupChk(@RequestParam HashMap<String,String> map, ModelMap model, HttpServletRequest request,  @RequestParam HashMap reqParams) throws Exception  {
	
		//map.put("sqlQueryId", "eqpLendDAO.lendTermDupChk");

		//List dupList = comService.selectCommonQueryList(map);
		
		List dupList = eqpLendService.selectLendTermDupChk(map);
		
		for(int i=0;i<dupList.size();i++) {
			log.debug("=========lendTermDupChk CON============>>>"+dupList.get(i).toString());
		}
		
		HashMap rtnMap = new HashMap<String,Object>();

		//model.addAttribute(dupList.get(0));
		reqParams.put("dupList", dupList);
		model.addAllAttributes(reqParams);


		return new ModelAndView(ajaxMainView, model);
	}
}
