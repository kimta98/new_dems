package dems.eqp.lend.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("eqpLendDAO")
@SuppressWarnings({"rawtypes","unchecked"})
public class EqpLendDAO extends EgovComAbstractDAO {

	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 유지보수관리 등록 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
//	public void regEqpBizRDtl(HashMap<String, String> map) throws Exception {
//		insert("eqpBizDAO.regEqpBizRDtl", map);
//
//	}
	
	public int regEqpBizRDtl(Map<String, Object> paramMap) throws Exception {

    	return (Integer) insert("eqpLendDAO.regEqpBizRDtl", paramMap);
    }
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 유지보수관리 수정 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
//	public void updEqpBizUDtl(HashMap<String, String> map) throws Exception {
//		insert("eqpBizDAO.updEqpBizUDtl", map);
//
//	}
	public void updEqpBizUDtl(Map<String, Object> paramMap) throws Exception {
        update("eqpLendDAO.updEqpBizUDtl", paramMap);
    }

	/**
    *
    * <pre>
    * 1. 메소드명 : selectAnalReqSno
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : ilyong
    * 4. 설명 :  대여신청일련번호 조회
    * </pre>
    * @param map
    * @param
    * @param
    * @return int
    * @throws Exception
    */
	public String selectRentAplnSno(Map map) throws Exception{
		return selectOne("eqpLendDAO.selectRentAplnSno", map);
	}
	
	/**
    *
    * <pre>
    * 1. 메소드명 : insertEqpLendRequest
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여신청
    * </pre>
    * @param map
    * @param
    * @param
    * @return int
    * @throws Exception
    */
   public void insertEqpLendRequest(Map<String, Object> paramMap) throws Exception {

   	 	insert("eqpLendDAO.insertEqpLendRequest", paramMap);
   }
   
   /**
   *
   * <pre>
   * 1. 메소드명 : insertEqpLendRequest
   * 2. 작성일 : 2021. 4. 30.
   * 3. 작성자 : ilyong
   * 4. 설명 :  장비 지원 관리 > 장비대여관리 장비대여신청 수정
   * </pre>
   * @param map
   * @param
   * @param
   * @return int
   * @throws Exception
   */
  public void modifyEqpLendRequest(Map<String, Object> paramMap) throws Exception {

  	 	insert("eqpLendDAO.modifyEqpLendRequest", paramMap);
  }
  
	  /**
	  *
	  * <pre>
	  * 1. 메소드명 : insertEqpLendRequest
	  * 2. 작성일 : 2021. 4. 30.
	  * 3. 작성자 : ilyong
	  * 4. 설명 :  장비 지원 관리 > 장비대여관리 장비대여신청 승인 또는 반납처리
	  * </pre>
	  * @param map
	  * @param
	  * @param
	  * @return int
	  * @throws Exception
	  */
	 public void modifyEqpLendRequestAprv(Map<String, Object> paramMap) throws Exception {
	
	 	 	insert("eqpLendDAO.modifyEqpLendRequestAprv", paramMap);
	 }
 
	 /**
	 *
	 * <pre>
	 * 1. 메소드명 : insertEqpLendRequest
	 * 2. 작성일 : 2021. 4. 30.
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 장비대여관리 장비대여신청 승인 또는 반납처리
	 * </pre>
	 * @param map
	 * @param
	 * @param
	 * @return int
	 * @throws Exception
	 */
	public void modifyEqpLendRequestDtlAprv(Map<String, Object> paramMap) throws Exception {
	
		 	insert("eqpLendDAO.modifyEqpLendRequestDtlAprv", paramMap);
	}
   
   /**
   *
   * <pre>
   * 1. 메소드명 : insertEqpLendRequestDtl
   * 2. 작성일 : 2021. 4. 30.
   * 3. 작성자 : ilyong
   * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여신청 장비목록 등록
   * </pre>
   * @param map
   * @param
   * @param
   * @return int
   * @throws Exception
   */
  public void insertEqpLendRequestDtl(Map<String, Object> paramMap) throws Exception {

  	 	insert("eqpLendDAO.insertEqpLendRequestDtl", paramMap);
  }
   

   
   /**
   *
   * <pre>
   * 1. 메소드명 : deleteEqpMntRDtl
   * 2. 작성일 : 2021. 4. 21.
   * 3. 작성자 : ilyong
   * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여신청 삭제
   * </pre>
   * @param map
   * @param
   * @param
   * @return int
   * @throws Exception
   */
  public void deleteEqpLendRequest(Map<String, Object> paramMap) throws Exception {

  	 	insert("eqpLendDAO.deleteEqpLendRequest", paramMap);
  }
  
  /**
  *
  * <pre>
  * 1. 메소드명 : deleteEqpMntRDtl
  * 2. 작성일 : 2021. 4. 21.
  * 3. 작성자 : ilyong
  * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여장비목록 삭제
  * </pre>
  * @param map
  * @param
  * @param
  * @return int
  * @throws Exception
  */
 public void deleteEqpLendRequestDtl(Map<String, Object> paramMap) throws Exception {

 	 	insert("eqpLendDAO.deleteEqpLendRequestDtl", paramMap);
 }
	
	/**
    *
    * <pre>
    * 1. 메소드명 : insertAnalysisEvdcDtl
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 유지보수관리 등록/수정
    * </pre>
    * @param map
    * @param
    * @param
    * @return int
    * @throws Exception
    */
   public void mergeEqpMntRDtl(Map<String, Object> paramMap) throws Exception {

   	 	insert("eqpLendDAO.mergeEqpMntRDtl", paramMap);
   }
   
   /**
   *
   * <pre>
   * 1. 메소드명 : insertAnalysisEvdcDtl
   * 2. 작성일 : 2021. 4. 21.
   * 3. 작성자 : ilyong
   * 4. 설명 :  장비 지원 관리 > 유지보수관리 등록/수정
   * </pre>
   * @param map
   * @param
   * @param
   * @return int
   * @throws Exception
   */
   public List selectLendTermDupChk(Map map) throws Exception{
		return selectList("eqpLendDAO.selectLendTermDupChk", map);
   }
   
   /**
   *
   * <pre>
   * 1. 메소드명 : selectLendTermChkCnt
   * 2. 작성일 : 2021. 6. 09.
   * 3. 작성자 : ilyong
   * 4. 설명 :  장비대여 등록시 대여기간 중복 체크
   * </pre>
   * @param map
   * @return int
   * @throws Exception
   */
	public int selectLendTermChkCnt(Map map) throws Exception{
		return selectOne("eqpLendDAO.selectLendTermChkCnt", map);
	}
	
   /**
   *
   * <pre>
   * 1. 메소드명 : selectLendingChkCnt
   * 2. 작성일 : 2021. 6. 09.
   * 3. 작성자 : ilyong
   * 4. 설명 :  장비대여 승인시 대여중인 장비가 있는지 체크
   * </pre>
   * @param map
   * @return int
   * @throws Exception
   */
	public int selectLendingChkCnt(Map map) throws Exception{
		return selectOne("eqpLendDAO.selectLendingChkCnt", map);
	}
}

