package dems.eqp.lend.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import dems.bbs.free.service.impl.BbsFreeServiceImpl;
import dems.eqp.lend.service.EqpLendService;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;


/**
 *
 * <pre>
 * 1. 클래스명 : EqpLendServiceImpl.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비대여관리
 * </pre>
 */
@Service("eqpLendService")
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class EqpLendServiceImpl extends EgovAbstractServiceImpl implements	EqpLendService{

	protected Logger log = LoggerFactory.getLogger(EqpLendServiceImpl.class);

	@Resource(name="eqpLendDAO")
	private EqpLendDAO eqpLendDAO;

	@Resource(name="comService")
	private ComService comService;

	@Resource(name="comDAO")
	private ComDAO comDAO;

	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	/**
	 *
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 등록 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
//	public void regEqpBizRDtl(HashMap<String, String> map) throws Exception{
//		// TODO Auto-generated method stub
//
//		//장비관리 등록 처리
//		eqpBizDAO.regEqpBizRDtl(map);
//
//		//부서별 사용자 등록처리
//		//eqpMgmtDAO.regFsysDeptUserRDtl(map);
//
//
////		if( "lvl1".equals(map.get("menuLvl"))) {
////			menuDAO.updateUpAdmMenuOrdr(map);
////		}
//	}
	public String regEqpBizRDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {

		String result = "";

		boolean denyListChk = true;
		String[] eList = EgovProperties.getProperty("Globals.fileUpload.Extensions").toString().split("\\.");

        if(fileMap.get("fileList4OriFileNm") != null){
            if (fileMap.get("fileList4OriFileNm") instanceof String) {
            	String fileList = (String) fileMap.get("fileList4OriFileNm");
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
        	}else {
        		List<String> fileList = (List<String>) fileMap.get("fileList4OriFileNm");
        		for (int i = 0; i < fileList.size(); i++) {
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.get(i).toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
    			}
        	}
        }

	 	if (denyListChk && fileMap.get("fileList4OriFileNm") != null) {
	 		result = "2";
        }else{
	    	//첨부파일등록
			String loginId = (String) paramMap.get("regrId");
	    	String atchFileId = (String) fileMap.get("atchFileId");

	    	if(EgovStringUtil.isNull(atchFileId)){
	    		atchFileId = comService.insertAtchFile("fileList4", "10", loginId, fileMap);
	    	}else{
	    		atchFileId = comService.updateAtchFile(atchFileId, "fileList4", "10", loginId, fileMap);
	    	}
	    	paramMap.put("atchFileId", atchFileId);
	    	//bbsFreeDAO.insertBbsFrees(paramMap);

	    	eqpLendDAO.regEqpBizRDtl(paramMap);

	    	result = "1";
        }

        return result;
	}

	/**
	 *
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 수정 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String updEqpBizUDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {
		// TODO Auto-generated method stub
		log.debug("========updEqpBizUDtl  loginId111111111111==========>>>");
		log.debug("========updEqpBizUDtl  atchFileId22222222222==========>>>");

		String result = "";
		String atchFileId = (String) paramMap.get("atchFileId");
		String loginId =  (String) paramMap.get("regrId");

		log.debug("========updEqpBizUDtl  loginId==========>>>"+loginId);
		log.debug("========updEqpBizUDtl  atchFileId==========>>>"+atchFileId);

		boolean denyListChk = true;
        String[] eList = EgovProperties.getProperty("Globals.fileUpload.Extensions").toString().split("\\.");

        if(fileMap.get("fileList4OriFileNm") != null){
            if (fileMap.get("fileList4OriFileNm") instanceof String) {
            	String fileList = (String) fileMap.get("fileList4OriFileNm");
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
        	}else {
        		List<String> fileList = (List<String>) fileMap.get("fileList4OriFileNm");
        		for (int i = 0; i < fileList.size(); i++) {
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.get(i).toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
    			}
        	}
        }

        if (denyListChk && fileMap.get("fileList4OriFileNm") != null) {
	 		result = "2";
        }else{
	       	if(EgovStringUtil.isNull(atchFileId)){
	       		atchFileId = comService.insertAtchFile("fileList4", "10", loginId, fileMap);
	       	}else{
	       		atchFileId = comService.updateAtchFile(atchFileId, "fileList4", "10", loginId, fileMap);
	       	}

//	       	paramMap.put("atchFileId", atchFileId);
//	    	bbsFreeDAO.updateBbsFree(paramMap);

	       	//장비관리 수정 처리
	       	paramMap.put("atchFileId", atchFileId);
	       	eqpLendDAO.updEqpBizUDtl(paramMap);

	    	result = "1";
        }



		return result;
	}



	/**
    *
    * <pre>
    * 1. 메소드명 : lendReqEqpLendLendReqRDtl
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여신청
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void modifyEqpLendRequest(Map<String, Object> map) throws Exception {

		Map<String,Object> paramMap = new HashMap<String,Object>();
		Map<String, Object> infoMap = new HashMap<String, Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();

		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		//validateUtil.check(chkParamMap, requireParams);

		String eqpSno = "";
		String modFlag = "";
		String userGb = (String) map.get("userGb");
		String rentUserId = (String) map.get("userId"); //insttCd
		String rentInsttCd = (String) map.get("insttCd");
		String rentAplnDt = "";
		String rentReqInfo = "";
		String rentStartDt = "";
		String rentEndDt = "";
		String rentAplnSno = "";
		String pgsStat = "";
		String rentCfrmDt = "";
		String sessionUserId = "";
		String rtnCfrmrDt = "";
		String saveGb = "";
		String insPgsStat = "";
		String suppReqSno = "";
		String cancelReasons = "";
		int chkTermCnt = 0;
		int chkLendingCnt = 0;

		log.debug("============modifyEqpLendRequest rentInsttCd================>>>"+rentInsttCd);

		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap1"));
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap2"));

		if(map.get("formDataMap3") != null && map.get("formDataMap3") !="") {
			paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap3"));
		}

		if(map.get("formDataMap4") != null && map.get("formDataMap4") !="") {
			paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap4"));
		}


		log.debug("===============modifyEqpLendRequest 장비대여신청정보 =============>>>>>>>");
//		log.debug("===============modifyEqpLendRequest rentAplnDt sessionUserId =============>>>>>>>"+paramMap.get("sessionUserId"));
//		log.debug("===============modifyEqpLendRequest rentAplnDt rentInsttCd =============>>>>>>>"+paramMap.get("rentInsttCd"));
//		log.debug("===============modifyEqpLendRequest rentAplnDt 요청일자 =============>>>>>>>"+paramMap.get("rentAplnDt"));
//		log.debug("===============modifyEqpLendRequest rentStartDt 대여기간 =============>>>>>>>"+paramMap.get("rentStartDt"));

		modFlag = (String)paramMap.get("modFlag");
		rentAplnDt = (String)paramMap.get("rentAplnDt");
		rentReqInfo = (String)paramMap.get("rentReqInfo");
		rentStartDt = (String)paramMap.get("rentStartDt");
		rentEndDt = (String)paramMap.get("rentEndDt");
		pgsStat = (String)paramMap.get("pgsStat");
		rentCfrmDt = (String)paramMap.get("rentCfrmDt");
		sessionUserId = rentUserId;
		rtnCfrmrDt = (String)paramMap.get("rtnCfrmrDt");
		saveGb = (String)paramMap.get("saveGb");
		suppReqSno = (String)paramMap.get("saveGb");
		cancelReasons = (String)paramMap.get("cancelReasons");


		//대여진행상태 (C09001:대여신청,  C09002:대여중,  C09003:반납확인)
//		if(saveGb.equals("req")) {
//			insPgsStat = "C09001";
//		}else if(saveGb.equals("cnc")) {
//			insPgsStat = "";
//		}



		log.debug("===============modifyEqpLendRequest pgsStat =============>>>>>>>"+pgsStat);

		if(!modFlag.equals("insert")){
			rentAplnSno = (String)paramMap.get("rentAplnSno");
			log.debug("===============modifyEqpLendRequest rentAplnSno =============>>>>>>>"+rentAplnSno);
		}

		//DEMS_ANALYSIS_EVDC_DTL insert
		ArrayList<Map<String, Object>> paramDetailList = (ArrayList<Map<String, Object>>) map.get("detailList");

		log.debug("=============== >>>> modFlag  <<<<<< =============>>>>>>>"+modFlag);
		log.debug("=============== >>>> paramDetailList.size()  <<<<<< =============>>>>>>>"+paramDetailList.size());

		//대여신청일련번호 조회
		//rentAplnSno = eqpLendDAO.selectRentAplnSno(paramMap);

		//DEMS_ANALYSIS_SUPP_REQ insert
		paramMap.clear();
		//paramMap.put("rentAplnSno", rentAplnSno);
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap1"));
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap2"));

		//log.debug("=============== >>>> ######## paramMap.toString() ######## <<<<<< =============>>>>>>>"+paramMap.toString());


		//권한체크  (사용자구분 C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)
		if("insert".equals(modFlag) || "modify".equals(modFlag)) {
			boolean userGbChk = false;
			switch (userGb) {
			case "C01001":     //수사관
				userGbChk = true;
				break;
			case "C01003":     //포렌식수사관
				userGbChk = true;
				break;
			default:
				userGbChk = false;
				break;
			}
			log.debug("=============== >>>> ######## userGbChk ######## <<<<<< =============>>>>>>>"+userGbChk);
			if(!userGbChk) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"수사관 또는 포렌식수사관"}));
			}
		}

		//권한체크  (사용자구분 C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)
		if("aprv".equals(modFlag) && !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식수사관"}));
		}


		if(modFlag.equals("insert")) {


			//대여신청일련번호 조회
			rentAplnSno = eqpLendDAO.selectRentAplnSno(paramMap);
			paramMap.clear();
			paramMap.put("rentAplnSno", rentAplnSno);
			paramMap.put("rentUserId"	, rentUserId);
			paramMap.put("rentInsttCd"	, rentInsttCd);
			paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap1"));
			paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap2"));
			log.debug("=============== >>>> ######## paramMap.toString()111111 ######## <<<<<< =============>>>>>>>"+paramMap.toString());

			//###### 장비대여 신청시 장비대여기간이 겹치는지 확인 후 exception 처리 START ######
			if(pgsStat.equals("C09001")) {
				for(int i =0; i < paramDetailList.size(); i++) {
					infoMap.clear();
					infoMap.put("chkRentStartDt", (String)paramMap.get("rentStartDt"));
					infoMap.put("chkRentEndDt", (String)paramMap.get("rentEndDt"));
					infoMap.put("chkEqpSno", paramDetailList.get(i).get("eqpSno"));

					chkTermCnt =  eqpLendDAO.selectLendTermChkCnt(infoMap);
					log.debug("=======chkTermCnt 11111========>>>"+chkTermCnt);
				}

				if(chkTermCnt > 0) {
					throw new MException(egovMessageSource.getMessage("dems.lend.req.001"));
				}
			}
			//###### 장비대여 신청시 장비대여기간이 겹치는지 확인 후 exception 처리 END ######

			//장비대여신청 INSERT
			eqpLendDAO.insertEqpLendRequest(paramMap);

			for(int i =0; i < paramDetailList.size(); i++) {
				log.debug("===============only insertEqpMntRDtl paramDetailList["+i+"] =============>>>>>>>"+paramDetailList.get(i));

					paramMap.clear();
					paramMap.put("rentAplnSno", rentAplnSno);
					paramMap.put("rentInsttCd"	, rentInsttCd);
					paramMap.put("rentUserId"	, rentUserId);
					paramMap.put("rentAplnDt"	, rentAplnDt);
					paramMap.put("rentReqInfo"	, rentReqInfo);
					paramMap.put("rentStartDt"	, rentStartDt);
					paramMap.put("rentEndDt"	, rentEndDt);
					paramMap.put("pgsStat"	, pgsStat);


					paramMap.putAll(paramDetailList.get(i));
					log.debug("=============== >>>> ######## 210610 paramDetailList.get("+i+") eqpSno 222222 ######## <<<<<< =============>>>>>>>"+paramDetailList.get(i).get("eqpSno"));
					log.debug("=============== >>>> ######## paramMap.toString()222222 ######## <<<<<< =============>>>>>>>"+paramMap.toString());

					//대여장비목록 INSERT
					eqpLendDAO.insertEqpLendRequestDtl(paramMap);
			}
		}else if(modFlag.equals("modify")){
			log.debug("=============== >>>> ######## paramMap.toString()222222 ######## <<<<<< =============>>>>>>>"+paramMap.toString());

			if(saveGb.equals("cnc")) {
				pgsStat = "";
			}else if(saveGb.equals("req")) {
				pgsStat = "C09001";
			}



			if(paramDetailList.size() > 0) {
				paramMap.clear();
				paramMap.put("rentAplnSno", rentAplnSno);
				paramMap.put("rentUserId"	, rentUserId);
				paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap1"));
				paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap2"));

				//###### 장비대여 신청시 장비대여기간이 겹치는지 확인 후 exception 처리 START ######
				if(pgsStat.equals("C09001")) {
					for(int i =0; i < paramDetailList.size(); i++) {
						infoMap.clear();
						infoMap.put("chkRentStartDt", (String)paramMap.get("rentStartDt"));
						infoMap.put("chkRentEndDt", (String)paramMap.get("rentEndDt"));
						infoMap.put("chkEqpSno", paramDetailList.get(i).get("eqpSno"));

						chkTermCnt =  eqpLendDAO.selectLendTermChkCnt(infoMap);
						log.debug("=======chkTermCnt 22222========>>>"+chkTermCnt);
					}

					if(chkTermCnt > 0) {
						throw new MException(egovMessageSource.getMessage("dems.lend.req.001"));
					}
				}
				//###### 장비대여 신청시 장비대여기간이 겹치는지 확인 후 exception 처리 END ######

				eqpLendDAO.modifyEqpLendRequest(paramMap);

				eqpLendDAO.deleteEqpLendRequestDtl(paramMap);

				for(int i =0; i < paramDetailList.size(); i++) {
					log.debug("===============modify insertEqpMntRDtl paramDetailList["+i+"] =============>>>>>>>"+paramDetailList.get(i));

						paramMap.clear();
						paramMap.put("rentAplnSno", rentAplnSno);
						paramMap.put("rentInsttCd"	, rentInsttCd);
						paramMap.put("rentUserId"	, rentUserId);
						paramMap.put("rentAplnDt"	, rentAplnDt);
						paramMap.put("rentReqInfo"	, rentReqInfo);
						paramMap.put("rentStartDt"	, rentStartDt);
						paramMap.put("rentEndDt"	, rentEndDt);
						paramMap.put("pgsStat"	, pgsStat);
						paramMap.putAll(paramDetailList.get(i));
						log.debug("=============== >>>> ######## paramMap.toString() modify ######## <<<<<< =============>>>>>>>"+paramMap.toString());

						//대여장비목록 INSERT
						eqpLendDAO.insertEqpLendRequestDtl(paramMap);
				}
			}else {
				log.debug("=============== >>>> ######## delete rentAplnSno ######## <<<<<< =============>>>>>>>"+rentAplnSno);
				paramMap.clear();
				paramMap.put("rentAplnSno", rentAplnSno);

				eqpLendDAO.deleteEqpLendRequestDtl(paramMap);

				eqpLendDAO.deleteEqpLendRequest(paramMap);

			}
		}else if(modFlag.equals("aprv")) {
			log.debug("=======modifyEqpLendRequest rentAplnSno(대여신청일련번호)=========>>>"+rentAplnSno);
			log.debug("=======modifyEqpLendRequest rentCfrmDt(대여확인일자)=========>>>"+rentCfrmDt);
			log.debug("=======modifyEqpLendRequest rentUserId(rentCfrmrId)=========>>>"+rentUserId);
			if(saveGb.equals("lendAprv")) {
				log.debug("=======##### modifyEqpLendRequest 대여승인 #####=========>>>");

				paramMap.clear();
				paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
				paramMap.put("rentCfrmDt", rentCfrmDt);	//대여확인일자
				paramMap.put("rentCfrmrId"	, rentUserId);	//대여확인자id
				paramMap.put("pgsStat"	, "C09002"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
				paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
				paramMap.toString();
				log.debug("=======##### modifyEqpLendRequest 대여승인 paramMap.toString() #####=========>>>"+paramMap.toString());

				//###### 장비대여 승인시 대여중인 장비가 있는지 확인 후 exception 처리 START ######
				for(int i =0; i < paramDetailList.size(); i++) {
					infoMap.clear();
					infoMap.put("chkEqpSno", paramDetailList.get(i).get("eqpSno"));

					chkLendingCnt =  eqpLendDAO.selectLendingChkCnt(infoMap);
					log.debug("=======chkLendingCnt 22222========>>>"+chkLendingCnt);
				}
				//chkLendingCnt = 1;
				if(chkLendingCnt > 0) {
					throw new MException(egovMessageSource.getMessageArgs("dems.lend.req.002", new String[] {rentAplnSno}));
				}
				//###### 장비대여 신청시 장비대여기간이 겹치는지 확인 후 exception 처리 END ######

				eqpLendDAO.modifyEqpLendRequestAprv(paramMap);
				eqpLendDAO.modifyEqpLendRequestDtlAprv(paramMap);

			}else if(saveGb.equals("lendCnc")) {
				log.debug("=======##### modifyEqpLendRequest 대여승인 취소 #####=========>>>");

				paramMap.clear();
				paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
				paramMap.put("rentCfrmDt", "");	//대여확인일자
				paramMap.put("rentCfrmrId"	, "");	//대여확인자id
				paramMap.put("pgsStat"	, "C09001"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
				paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
				paramMap.toString();

				eqpLendDAO.modifyEqpLendRequestAprv(paramMap);
				eqpLendDAO.modifyEqpLendRequestDtlAprv(paramMap);

			}else if(saveGb.equals("rtnCfrm")) {
				log.debug("=======##### modifyEqpLendRequest 반납확인 #####=========>>>");

				paramMap.clear();
				paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
				paramMap.put("rtnCfrmrId"	, rentUserId);	//반납확인자id
				paramMap.put("rtnCfrmrDt", rtnCfrmrDt);	//반납확인일자
				paramMap.put("pgsStat"	, "C09003"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
				paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
				paramMap.toString();

				eqpLendDAO.modifyEqpLendRequestDtlAprv(paramMap);

			}else if(saveGb.equals("rtnCnc")) {
				log.debug("=======##### modifyEqpLendRequest 반납취소 #####=========>>>");
				paramMap.clear();
				paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
				paramMap.put("rtnCfrmrId"	, "");	//반납확인자id
				paramMap.put("rtnCfrmrDt", "");	//반납확인일자
				paramMap.put("pgsStat"	, "C09002"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
				paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
				paramMap.toString();

				eqpLendDAO.modifyEqpLendRequestDtlAprv(paramMap);
			} else if(saveGb.equals("denial")){
				log.debug("=======##### modifyEqpLendRequest 신청반려 #####=========>>>");

				paramMap.clear();
				paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
				paramMap.put("rtnCfrmrId"	, "");
				paramMap.put("rtnCfrmrDt", "");
				paramMap.put("cancelReasons", cancelReasons);
				paramMap.put("pgsStat"	, "C09004"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
				paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
				paramMap.toString();

				eqpLendDAO.modifyEqpLendRequestDtlAprv(paramMap);
			}
		}else if(modFlag.equals("del")) {

			paramMap.clear();
			paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호


			//장비 대여진행상태 조회 (대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
			paramMap.put("sqlQueryId", "eqpLendDAO.selectPgsStat");
			pgsStat = comDAO.selectCommonQueryString(paramMap);

			if("C09002".equals(pgsStat) || "C09003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.lend.req.000",new String[]{rentAplnSno}));
			}

			log.debug("=======##### modifyEqpLendRequest 삭제 rentAplnSno#####=========>>>"+rentAplnSno);
			paramMap.clear();
			paramMap.put("rentAplnSno", rentAplnSno);

			eqpLendDAO.deleteEqpLendRequestDtl(paramMap);

			eqpLendDAO.deleteEqpLendRequest(paramMap);
		}




	}

	/**
    *
    * <pre>
    * 1. 메소드명 : ModifySelEqpLendRequest
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 장비대여관리 리스트에서 선택한 내역 반납처리
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void ModifySelEqpLendRequest(Map<String, Object> map) throws Exception {

		Map<String, Object> paramMap = new HashMap<String, Object>();

		String type = (String) map.get("type");
		List rtnCfrmList = (List) map.get("rtnCfrmList");
		String userGb = (String) map.get("userGb");
		String insttCd = (String) map.get("insttCd");
		String userId = (String) map.get("userId");
		String currentDay = EgovDateUtil.getToday();

		log.debug("=======##### ModifySelEqpLendRequest currentDay #####=========>>>"+currentDay);

		Map<String, Object> infoMap = new HashMap<String, Object>();

//		paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
//		paramMap.put("rtnCfrmrId"	, rentUserId);	//반납확인자id
//		paramMap.put("rtnCfrmrDt", rtnCfrmrDt);	//반납확인일자
//		paramMap.put("pgsStat"	, "C09003"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
//		paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id

		switch (type) {
		case "rtnCfrm":     //반납처리
			infoMap.put("rtnCfrmrId", userId);
			infoMap.put("pgsStat", "C09003");
			infoMap.put("sessionUserId", userId);
			break;
		}

		//권한체크
		if(!"rtnCfrm".equals(type) || !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식 수사관"}));
		}


		int chkAprv = 0;
		String pgsStat = "";

		for(int i = 0; i < rtnCfrmList.size(); i++) {

			paramMap.clear();
			paramMap.putAll(infoMap);
			paramMap.put("rentAplnSno", rtnCfrmList.get(i));

			//장비 대여진행상태 조회 (대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
			paramMap.put("sqlQueryId", "eqpLendDAO.selectPgsStat");
			pgsStat = comDAO.selectCommonQueryString(paramMap);

			if(!"C09002".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) rtnCfrmList.get(i),"대여중"}));
			}

		}

		//DEMS_ANALYSIS_SUPP_REQ 진행상태 변경 rentAplnSno  rtnCfrmrDt
		for(int i = 0; i < rtnCfrmList.size(); i++) {

			paramMap.clear();
			paramMap.putAll(infoMap);
			paramMap.putAll(map);
			paramMap.put("rentAplnSno", rtnCfrmList.get(i));
			paramMap.put("rtnCfrmrDt", currentDay);

			log.debug("=======##### ModifySelEqpLendRequest rtnCfrmList.get["+i+"] #####=========>>>"+rtnCfrmList.get(i));
			log.debug("=======##### ModifySelEqpLendRequest 반납확인 paramMap.toString() #####=========>>>"+paramMap.toString());

			eqpLendDAO.modifyEqpLendRequestDtlAprv(paramMap);


			//comDAO.updateCommonQuery(paramMap);

		}



	}

	/**
    *
    * <pre>
    * 1. 메소드명 : selectLendTermDupChk
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 장비대여관리 리스트에서 선택한 내역 반납처리
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	public List selectLendTermDupChk(Map map) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> paramMap2 = new HashMap<String, Object>();

		List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();

		List test = new ArrayList<Object>();

		log.debug("=====selectLendTermDupChk	chkRentStartDt======>>>"+(String) map.get("chkRentStartDt"));
		log.debug("=====selectLendTermDupChk	chkRentEndDt======>>>"+(String) map.get("chkRentEndDt"));
		log.debug("=====selectLendTermDupChk	chkEqpSno======>>>"+(String) map.get("chkEqpSno"));

		String chkRentStartDt = (String) map.get("chkRentStartDt");
		String chkRentEndDt = (String) map.get("chkRentEndDt");
		String chkEqpSno = (String) map.get("chkEqpSno");
		String ttt = "";
		String rtnMsg = "";

		String[] arrData = chkEqpSno.split("@");

		for(int i = 0; i< arrData.length; i++){
			log.debug("=====selectLendTermDupChk	arrData["+i+"]======>>>"+arrData[i]);
			paramMap.put("chkRentStartDt"	, chkRentStartDt);
			paramMap.put("chkRentEndDt"	, chkRentEndDt);
			paramMap.put("chkEqpSno"	, arrData[i]);

			int chkTermCnt =  eqpLendDAO.selectLendTermChkCnt(paramMap);
			log.debug("=====selectLendTermDupChk	chkTermCnt["+i+"]======>>>"+chkTermCnt);
			if(chkTermCnt == 0) {
				//paramMap2.put("chkEqpSno",arrData[i]+"|Y" );
				rtnMsg += arrData[i]+"|Y,";

				log.debug("=====selectLendTermDupChk111111	chkTermCnt["+i+"]======>>>"+chkTermCnt);
			}else {
				//paramMap2.put("chkEqpSno", arrData[i]+"|N");
				rtnMsg += arrData[i]+"|N,";
				log.debug("=====selectLendTermDupChk222222	chkTermCnt["+i+"]======>>>"+chkTermCnt);
			}
		}
		rtnMsg = rtnMsg.substring(0,rtnMsg.lastIndexOf(","));
		paramMap2.put("chkEqpSno",rtnMsg );

		test.add(paramMap2);

		//사건이 존재하는지 체크
//		paramMap.put("incdtSno"	, paramMap.get("incdtSno"));
//		int chkTermCnt =  eqpLendDAO.selectLendTermChkCnt(paramMap);
//
//		List test= null; //= eqpLendDAO.selectLendTermDupChk(map);

		return test;
	}
}

