package dems.eqp.lend.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import frame.flyt.login.service.FLytLoginVO;



/**
 * 
 * <pre>
 * 1. 클래스명 : EqpLendService.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비 지원 관리 > 장비대여관리
 * </pre>
 */
@SuppressWarnings("rawtypes")
public interface EqpLendService {


	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여관리 등록 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	//void regEqpBizRDtl(HashMap<String, String> map)  throws Exception;
	String regEqpBizRDtl(Map<String,Object> paramMap, Map<String,Object> fileMap) throws Exception;
	
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여관리 상세정보 수정 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	//void updEqpBizUDtl(HashMap<String, String> map)  throws Exception;
	String updEqpBizUDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception;
	
	
	/**
	 * 
	 * <pre>
     * 1. 메소드명 : lendReqEqpLendLendReqRDtl
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여신청
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void modifyEqpLendRequest(Map<String,Object> map) throws Exception;
	
	/**
	 * 
	 * <pre>
     * 1. 메소드명 : ModifySelEqpLendRequest
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  장비 지원 관리 > 장비대여관리 리스트에서 선택한 내역 반납처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void ModifySelEqpLendRequest(Map<String,Object> map) throws Exception;
	
	/**
	 * 
	 * <pre>
     * 1. 메소드명 : selectLendTermDupChk
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  장비 지원 관리 > 장비대여관리 리스트에서 선택한 내역 반납처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	List selectLendTermDupChk(Map map) throws Exception;
}

