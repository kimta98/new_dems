package dems.eqp.mnt.service;

import java.util.HashMap;
import java.util.Map;

import frame.flyt.login.service.FLytLoginVO;



/**
 * 
 * <pre>
 * 1. 클래스명 : EqpMntService.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비 지원 관리 > 유지보수관리
 * </pre>
 */
@SuppressWarnings("rawtypes")
public interface EqpMntService {


	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 등록 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	//void regEqpBizRDtl(HashMap<String, String> map)  throws Exception;
	String regEqpBizRDtl(Map<String,Object> paramMap, Map<String,Object> fileMap) throws Exception;
	
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 상세정보 수정 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	//void updEqpBizUDtl(HashMap<String, String> map)  throws Exception;
	String updEqpBizUDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception;
	
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : modifyEqpMntRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 상세정보 등록/수정
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void modifyEqpMntRDtl(Map<String,Object> map) throws Exception;
}

