package dems.eqp.mnt.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import dems.bbs.free.service.impl.BbsFreeServiceImpl;
import dems.eqp.mnt.service.EqpMntService;
import frame.fcom.service.ComService;
import frame.fexception.MException;
import frame.futil.ValidateUtil;

/**
 * 
 * <pre>
 * 1. 클래스명 : EqpMntServiceImpl.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 유지보수관리
 * </pre>
 */
@Service("EqpMntService")
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class EqpMntServiceImpl extends EgovAbstractServiceImpl implements	EqpMntService{
	
	protected Logger log = LoggerFactory.getLogger(EqpMntServiceImpl.class);
	
	@Resource(name="eqpMntDAO")
	private EqpMntDAO eqpMntDAO;
	
	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 등록 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
//	public void regEqpBizRDtl(HashMap<String, String> map) throws Exception{
//		// TODO Auto-generated method stub
//		
//		//장비관리 등록 처리
//		eqpBizDAO.regEqpBizRDtl(map);
//		
//		//부서별 사용자 등록처리
//		//eqpMgmtDAO.regFsysDeptUserRDtl(map);
//				
//				
////		if( "lvl1".equals(map.get("menuLvl"))) {
////			menuDAO.updateUpAdmMenuOrdr(map);
////		}
//	}
	public String regEqpBizRDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {		
		
		String result = "";

		boolean denyListChk = true;
		String[] eList = EgovProperties.getProperty("Globals.fileUpload.Extensions").toString().split("\\.");

        if(fileMap.get("fileList4OriFileNm") != null){
            if (fileMap.get("fileList4OriFileNm") instanceof String) {
            	String fileList = (String) fileMap.get("fileList4OriFileNm");
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
        	}else {
        		List<String> fileList = (List<String>) fileMap.get("fileList4OriFileNm");
        		for (int i = 0; i < fileList.size(); i++) {
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.get(i).toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
    			}
        	}
        }

	 	if (denyListChk && fileMap.get("fileList4OriFileNm") != null) {
	 		result = "2";
        }else{
	    	//첨부파일등록
			String loginId = (String) paramMap.get("regrId");
	    	String atchFileId = (String) fileMap.get("atchFileId");

	    	if(EgovStringUtil.isNull(atchFileId)){
	    		atchFileId = comService.insertAtchFile("fileList4", "10", loginId, fileMap);
	    	}else{
	    		atchFileId = comService.updateAtchFile(atchFileId, "fileList4", "10", loginId, fileMap);
	    	}
	    	paramMap.put("atchFileId", atchFileId);
	    	//bbsFreeDAO.insertBbsFrees(paramMap);
	    	
	    	eqpMntDAO.regEqpBizRDtl(paramMap);
	    	
	    	result = "1";
        }

        return result;
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 수정 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String updEqpBizUDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {	
		// TODO Auto-generated method stub
		log.debug("========updEqpBizUDtl  loginId111111111111==========>>>");
		log.debug("========updEqpBizUDtl  atchFileId22222222222==========>>>");
		
		String result = "";
		String atchFileId = (String) paramMap.get("atchFileId");
		String loginId =  (String) paramMap.get("regrId");
		
		log.debug("========updEqpBizUDtl  loginId==========>>>"+loginId);
		log.debug("========updEqpBizUDtl  atchFileId==========>>>"+atchFileId);
		
		boolean denyListChk = true;
        String[] eList = EgovProperties.getProperty("Globals.fileUpload.Extensions").toString().split("\\.");
		
        if(fileMap.get("fileList4OriFileNm") != null){
            if (fileMap.get("fileList4OriFileNm") instanceof String) {
            	String fileList = (String) fileMap.get("fileList4OriFileNm");
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
        	}else {
        		List<String> fileList = (List<String>) fileMap.get("fileList4OriFileNm");
        		for (int i = 0; i < fileList.size(); i++) {
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.get(i).toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
    			}
        	}
        }
        
        if (denyListChk && fileMap.get("fileList4OriFileNm") != null) {
	 		result = "2";
        }else{
	       	if(EgovStringUtil.isNull(atchFileId)){
	       		atchFileId = comService.insertAtchFile("fileList4", "10", loginId, fileMap);
	       	}else{
	       		atchFileId = comService.updateAtchFile(atchFileId, "fileList4", "10", loginId, fileMap);
	       	}

//	       	paramMap.put("atchFileId", atchFileId);
//	    	bbsFreeDAO.updateBbsFree(paramMap);
	       	
	       	//장비관리 수정 처리
	       	paramMap.put("atchFileId", atchFileId);
	       	eqpMntDAO.updEqpBizUDtl(paramMap);
	       	
	    	result = "1"; 
        }
        
		
		
		return result; 
	}
	
	
	
	/**
    *
    * <pre>
    * 1. 메소드명 : insertAnlsReq
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 유지보수관리 등록/수정
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void modifyEqpMntRDtl(Map<String, Object> map) throws Exception {

		Map<String,Object> paramMap = new HashMap<String,Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		//validateUtil.check(chkParamMap, requireParams);

		String eqpSno = "";
		String modFlag = "";
		String userGb = (String) map.get("userGb");
		
		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		
		//권한이 일반수사관(C01001)
//		if(!"C01001".equals(userGb)) {
//			throw new MException(egovMessageSource.getMessage("dems.anls.req.000")); modFlag
//		}
		
		log.debug("===============modifyEqpMntRDtl eqpSno before =============>>>>>>>"+paramMap.get("eqpSno"));
		log.debug("===============modifyEqpMntRDtl eqpSno modFlag =============>>>>>>>"+paramMap.get("modFlag"));
		
		eqpSno = (String)paramMap.get("eqpSno");
		modFlag = (String)paramMap.get("modFlag");
		
		log.debug("===============modifyEqpMntRDtl eqpSno after =============>>>>>>>"+paramMap.get("eqpSno"));
		
        //사건이 존재하는지 체크
//		paramMap.clear();
//		paramMap.put("eqpSno"	, map.get("eqpSno"));
		
//		int chkIncdt =  anlsReqDAO.selectAnlsReqIncdtChk(paramMap);
//		if(chkIncdt < 1) {
//			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.001",new String[]{"해당 사건"}));
//		}

		//분석요청일련번호 조회
//		paramMap.clear();
//		paramMap.put("incdtSno"	, map.get("incdtSno"));
//		String analReqSno = anlsReqDAO.selectAnalReqSno(paramMap);


		//DEMS_ANALYSIS_SUPP_REQ insert
//		paramMap.clear();
//		paramMap.put("analReqSno", analReqSno);
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));

		/*진행상태
		C03001:임시등록
		C03002:승인요청
		C03003:검사승인
		C03004:반려
		C03005:문서접수
		C03006:과장승인*/

		//paramMap.put("pgsStat","C03001");
		//paramMap.put("analReqSno", analReqSno);
		//anlsReqDAO.insertAnalysisSuppReq(paramMap);

		//DEMS_ANALYSIS_EVDC_DTL insert
		ArrayList<Map<String, Object>> paramDetailList = (ArrayList<Map<String, Object>>) map.get("detailList");
		
		log.debug("=============== >>>> modFlag  <<<<<< =============>>>>>>>"+modFlag);
		log.debug("=============== >>>> paramDetailList.size()  <<<<<< =============>>>>>>>"+paramDetailList.size());
		
		if(modFlag.equals("insert")) {
			for(int i =0; i < paramDetailList.size(); i++) {
				log.debug("===============only insertEqpMntRDtl paramDetailList["+i+"] =============>>>>>>>"+paramDetailList.get(i));
				
					paramMap.clear();
					paramMap.putAll(paramDetailList.get(i));
					paramMap.put("eqpSno"	, eqpSno);
					eqpMntDAO.insertEqpMntRDtl(paramMap);
					//eqpMntDAO.mergeEqpMntRDtl(paramMap);
			}
		}else{
			eqpMntDAO.deleteEqpMntRDtl(paramMap);
			if(paramDetailList.size() > 0) {
				for(int i =0; i < paramDetailList.size(); i++) {
					log.debug("===============delete & insertEqpMntRDtl paramDetailList["+i+"] =============>>>>>>>"+paramDetailList.get(i));
					
						paramMap.clear();
						paramMap.putAll(paramDetailList.get(i));
						paramMap.put("eqpSno"	, eqpSno);
						eqpMntDAO.insertEqpMntRDtl(paramMap);
						//eqpMntDAO.mergeEqpMntRDtl(paramMap);
				}
			}
		}
		

	}
}

