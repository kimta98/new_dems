package dems.eqp.mnt.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("eqpMntDAO")
@SuppressWarnings({"rawtypes","unchecked"})
public class EqpMntDAO extends EgovComAbstractDAO {

	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 유지보수관리 등록 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
//	public void regEqpBizRDtl(HashMap<String, String> map) throws Exception {
//		insert("eqpBizDAO.regEqpBizRDtl", map);
//
//	}
	
	public int regEqpBizRDtl(Map<String, Object> paramMap) throws Exception {

    	return (Integer) insert("eqpMntDAO.regEqpBizRDtl", paramMap);
    }
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 유지보수관리 수정 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
//	public void updEqpBizUDtl(HashMap<String, String> map) throws Exception {
//		insert("eqpBizDAO.updEqpBizUDtl", map);
//
//	}
	public void updEqpBizUDtl(Map<String, Object> paramMap) throws Exception {
        update("eqpMntDAO.updEqpBizUDtl", paramMap);
    }

	
	
	/**
    *
    * <pre>
    * 1. 메소드명 : insertEqpMntRDtl
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 유지보수관리 등록
    * </pre>
    * @param map
    * @param
    * @param
    * @return int
    * @throws Exception
    */
   public void insertEqpMntRDtl(Map<String, Object> paramMap) throws Exception {

   	 	insert("eqpMntDAO.insertEqpMntRDtl", paramMap);
   }
   
   /**
   *
   * <pre>
   * 1. 메소드명 : deleteEqpMntRDtl
   * 2. 작성일 : 2021. 4. 21.
   * 3. 작성자 : ilyong
   * 4. 설명 :  장비 지원 관리 > 유지보수관리 삭제
   * </pre>
   * @param map
   * @param
   * @param
   * @return int
   * @throws Exception
   */
  public void deleteEqpMntRDtl(Map<String, Object> paramMap) throws Exception {

  	 	insert("eqpMntDAO.deleteEqpMntRDtl", paramMap);
  }
	
	/**
    *
    * <pre>
    * 1. 메소드명 : insertAnalysisEvdcDtl
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 유지보수관리 등록/수정
    * </pre>
    * @param map
    * @param
    * @param
    * @return int
    * @throws Exception
    */
   public void mergeEqpMntRDtl(Map<String, Object> paramMap) throws Exception {

   	 	insert("eqpMntDAO.mergeEqpMntRDtl", paramMap);
   }
	
}

