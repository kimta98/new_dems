package dems.eqp.mnt.web;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.eqp.mnt.service.EqpMntService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;


/**
 * 
 * <pre>
 * 1. 클래스명 : EqpMntController.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비 지원 관리 > 유지보수관리
 * </pre>
 */
@Controller
public class EqpMntController {

	protected Logger log = LoggerFactory.getLogger(EqpMntController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;
	
	@Resource(name = "EqpMntService")
	private EqpMntService eqpMntService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

    @Resource MappingJackson2JsonView ajaxMainView;

    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexEqpMntMList
     * 2. 작성일 : 2021. 4. 16. 오전 10:49:11
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 유지보수관리 조회 이동
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/mnt/indexEqpMntMList.do")
    public String indexEqpMntMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {
    	
    	model.addAttribute("message", request.getParameter("message"));
    	if(request.getParameter("msg") != null){
    		model.addAttribute("msg", request.getParameter("msg"));
    	}
    	return "dems/eqp/mnt/eqpMntMList";
    }


	/**
     * 
     * <pre>
     * 1. 메소드명 : queryEqpMntMList
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 유지보수관리 조회 처리
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/mnt/queryEqpMntMList.do")
    public ModelAndView queryEqpMntMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "eqpMntDAO.queryEqpMntMListTotCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	
    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		reqParams.put("sqlQueryId", "eqpMntDAO.queryEqpMntMList");
		List list = comService.selectCommonQueryList(reqParams);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
    
    
    /**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 등록 화면 이동
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/mnt/indexEqpMntRDtl.do")
	public String indexEqpMntRDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/eqp/mnt/eqpMntRDtl";
		
	}
	
    /**
	 * 
	 * <pre>
	 * 1. 메소드명 : queryEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:49:20
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 사업관리 장비상세정보 조회
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("/eqp/mnt/queryEqpBizUDtl.do")
	public ModelAndView queryEqpBizUDtl(@RequestParam HashMap<String,String> map, HttpServletRequest request, ModelMap model) throws Exception {
    //public ModelAndView queryEqpBizUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		//공통으로 필요한 데이타.(권한체크등...)
    	//model = comController.common(request, model);


		String errMsg = "";

    	try{
    		
    		
//    		Map<String, Object> paramMap = new HashMap<String, Object>();
//            paramMap.put("putupSno", Integer.parseInt((String) reqParams.get("putupSno")));
//            paramMap.put("boardKind", "C23009");
//
//            Map bbsFreeMap = bbsFreeService.selectBbsFreeRDtl(paramMap);
//            String sTemp = DemsUtil.getHtmlStrCnvr((String) bbsFreeMap.get("cnts"));
//            bbsFreeMap.put("cntsCvt", sTemp);
//
//    	    List<FileVO> atchFileList = comService.selectAtchFileList((String) bbsFreeMap.get("atchFileId"),null);
//    	    bbsFreeService.updateBbsFreeCount(paramMap);
//    	    model.addAttribute("bbsFreeMap", bbsFreeMap);
//    	    model.addAttribute("atchFileList",atchFileList);
//        	
//        	 return new ModelAndView(ajaxMainView, model);
    		
    		if(map.get("bizSno") != null && !map.get("bizSno").equals("")) {
    			log.debug("========사업관리 장비상세정보 atchFileId===========>>>>"+map.get("atchFileId"));
    			List<FileVO> atchFileList = comService.selectAtchFileList((String) map.get("atchFileId"),null);
    			
    			map.put("sqlQueryId", "eqpBizDAO.queryEqpBizUDtl");
    			Map resultMap = comService.selectCommonQueryMap(map);
    			
    			model.addAttribute("resultMap", resultMap);
    			model.addAttribute("atchFileList", atchFileList);
    		}

    	}catch (Exception e) {
    		e.printStackTrace();
    		errMsg = URLEncoder.encode(egovMessageSource.getMessage("fail.request.msg"), "UTF-8");
    	}
    	
    	return new ModelAndView(ajaxMainView, model);

    }
    
    /**
    *
    * <pre>
    * 1. 메소드명 : regEqpMntRDtl
    * 2. 작성일 : 2021. 4. 14.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 유지보수 상세정보 등록/수정
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
   @RequestMapping(value="/eqp/mnt/regEqpMntRDtl.do")
   public ModelAndView regEqpMntRDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	
	   	ObjectMapper mapper = new ObjectMapper();
	   	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
    	Map<String, Map<String, String>> jsonObject2 = mapper.readValue(paramData, Map.class);
	
	   	//장비 상세 정보
	   	ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
	   	//장비 유지 보수 상세 정보
	   	Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");
        HashMap<String,String> chkParams = (HashMap<String, String>)jsonObject2.get("formDatas");
	   	
	   	log.debug("======CON regEqpMntRDtl eqpSno========>>>"+formDataMap.get("eqpSno"));
	   	
	   	Map<String, Object> paramMap = new HashMap<String, Object>();
	   	paramMap.put("formDataMap", formDataMap);
	   	paramMap.put("detailList", tpList);
	   	paramMap.put("userGb", user.getUserGb());
	   	paramMap.put("userId", user.getUserId());

		HashMap<String,String> requireParams = new HashMap();
		requireParams.put("dfecCnts", "결함내용");
		requireParams.put("rprCnts", "수리내용");
		requireParams.put("rprStartDt", "수리시작일");
		requireParams.put("rprEndDt", "수리종료일");
		requireParams.put("rprCoNm", "수리업체명");
		requireParams.put("coCrgr", "업체담당자");
        validateUtil.check(chkParams, requireParams);
	
	   	eqpMntService.modifyEqpMntRDtl(paramMap);

	   	return new ModelAndView(ajaxMainView, model);

   }
   
   /**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpMntUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 수정 화면 이동
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/mnt/indexEqpMntUDtl.do")
	public String indexEqpMntUDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/eqp/mnt/eqpMntUDtl";
		
	}
	
	
	

}
