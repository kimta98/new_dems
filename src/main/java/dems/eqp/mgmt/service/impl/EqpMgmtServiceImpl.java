package dems.eqp.mgmt.service.impl;

import java.util.HashMap;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import dems.eqp.mgmt.service.EqpMgmtService;

/**
 * 
 * <pre>
 * 1. 클래스명 : EqpMgmtServiceImpl.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비관리
 * </pre>
 */
@Service("eqpMgmtService")
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class EqpMgmtServiceImpl extends EgovAbstractServiceImpl implements	EqpMgmtService{
	
	@Resource(name="eqpMgmtDAO")
	private EqpMgmtDAO eqpMgmtDAO;
	

	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpMgmtRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비관리 등록 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public void regEqpMgmtRDtl(HashMap<String, String> map) throws Exception{
		// TODO Auto-generated method stub
		
		//장비관리 등록 처리
		eqpMgmtDAO.regEqpMgmtRDtl(map);
		
		//부서별 사용자 등록처리
		//eqpMgmtDAO.regFsysDeptUserRDtl(map);
				
				
//		if( "lvl1".equals(map.get("menuLvl"))) {
//			menuDAO.updateUpAdmMenuOrdr(map);
//		}
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비관리 수정 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public void updEqpMgmtUDtl(HashMap<String, String> map) throws Exception{
		// TODO Auto-generated method stub
		
		//장비관리 수정 처리
		eqpMgmtDAO.updEqpMgmtUDtl(map);
		
		//부서별 사용자 등록처리
		//eqpMgmtDAO.regFsysDeptUserRDtl(map);
				
				
//		if( "lvl1".equals(map.get("menuLvl"))) {
//			menuDAO.updateUpAdmMenuOrdr(map);
//		}
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : delEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비관리 삭제 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public void delEqpMgmtUDtl(HashMap<String, String> map) throws Exception{
		// TODO Auto-generated method stub
		
		//장비정보 삭제 처리
		eqpMgmtDAO.delEqpMgmtUDtl(map);
				
				
//		if( "lvl1".equals(map.get("menuLvl"))) {
//			menuDAO.updateUpAdmMenuOrdr(map);
//		}
	}
}

