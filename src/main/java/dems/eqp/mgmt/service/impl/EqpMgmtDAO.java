package dems.eqp.mgmt.service.impl;

import java.util.HashMap;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("eqpMgmtDAO")
@SuppressWarnings({"rawtypes","unchecked"})
public class EqpMgmtDAO extends EgovComAbstractDAO {


	/**
	 *
	 * <pre>
	 * 1. 메소드명 : regEqpMgmtRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 장비관리 등록 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	public void regEqpMgmtRDtl(HashMap<String, String> map) throws Exception {
		insert("eqpMgmtDAO.regEqpMgmtRDtl", map);

	}

	/**
	 *
	 * <pre>
	 * 1. 메소드명 : updEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 장비관리 수정 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	public void updEqpMgmtUDtl(HashMap<String, String> map) throws Exception {

		insert("eqpMgmtDAO.updEqpMgmtUDtl", map);
	}

	/**
	 *
	 * <pre>
	 * 1. 메소드명 : delEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:48:53
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비관리 삭제 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	public void delEqpMgmtUDtl(HashMap<String, String> map) throws Exception {
		insert("eqpMgmtDAO.delEqpMgmtUDtl", map);

	}

}

