package dems.eqp.mgmt.service;

import java.util.HashMap;
import frame.flyt.login.service.FLytLoginVO;



/**
 * 
 * <pre>
 * 1. 클래스명 : EqpMgmtService.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비관리
 * </pre>
 */
@SuppressWarnings("rawtypes")
public interface EqpMgmtService {


	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpMgmtRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비관리 등록 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void regEqpMgmtRDtl(HashMap<String, String> map)  throws Exception;
	
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비관리 상세정보 수정 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void updEqpMgmtUDtl(HashMap<String, String> map)  throws Exception;
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : delEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비관리 상세정보 삭제 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void delEqpMgmtUDtl(HashMap<String, String> map)  throws Exception;

	
	
}

