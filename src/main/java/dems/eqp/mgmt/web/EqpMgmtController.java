package dems.eqp.mgmt.web;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.eqp.mgmt.service.EqpMgmtService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.flyt.login.service.FLytLoginVO;
import frame.fsys.user.service.FsysUserVO;
import frame.futil.DemsConst;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;


/**
 *
 * <pre>
 * 1. 클래스명 : EqpMgmtController.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 장비관리
 * </pre>
 */
@Controller
public class EqpMgmtController {

	protected Logger log = LoggerFactory.getLogger(EqpMgmtController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "eqpMgmtService")
	private EqpMgmtService eqpMgmtService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

    @Resource MappingJackson2JsonView ajaxMainView;


    /**
     *
     * <pre>
     * 1. 메소드명 : indexEqpMgmtMList
     * 2. 작성일 : 2021. 4. 16. 오전 10:49:11
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비관리 조회 이동
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/mgmt/indexEqpMgmtMList.do")
    public String indexEqpMgmtMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

    	model.addAttribute("message", request.getParameter("message"));
    	if(request.getParameter("msg") != null){
    		model.addAttribute("msg", request.getParameter("msg"));
    	}
    	return "dems/eqp/mgmt/eqpMgmtMList";
    }


	/**
     *
     * <pre>
     * 1. 메소드명 : queryEqpMgmtMList
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 장비관리 조회 처리
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/mgmt/queryEqpMgmtMList.do")
    public ModelAndView queryEqpMgmtMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "eqpMgmtDAO.eqpMgmtMListTotCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);

    	PageUtil.calcPage(reqParams);

//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));

		reqParams.put("sqlQueryId", "eqpMgmtDAO.eqpMgmtMList");
		List eqpMgmtMList = comService.selectCommonQueryList(reqParams);
		reqParams.put("eqpMgmtMList", eqpMgmtMList);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : indexEqpMgmtRDtl
     * 2. 작성일 : 2021. 4. 16. 오전 9:15:21
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 장비관리 등록 화면으로 이동
     * </pre>
     * @param map
     * @param request
     * @param fsysUserVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/eqp/mgmt/indexEqpMgmtRDtl.do")
    public String indexEqpMgmtRDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, @ModelAttribute("fsysUserVO") FsysUserVO fsysUserVO, ModelMap model) throws Exception {

    	//공통으로 필요한 데이타.(권한체크등...)
    	// model = comController.common(request, model);

    	//String userId = map.get("userId");

		/*map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperList");
		List sysGrpList = comService.selectCommonQueryList(map);
		model.addAttribute("sysGrpList", sysGrpList);

		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperId");
		Map rtnMap = comService.selectCommonQueryMap(map);
		model.addAttribute("topMenuNo",rtnMap.get("topMenuNo"));*/

    	return "dems/eqp/mgmt/eqpMgmtRDtl";
    }


    /**
     *
     * <pre>
     * 1. 메소드명 : regEqpMgmtRDtl
     * 2. 작성일 : 2021. 4. 16. 오전 9:46:21
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 지원 관리 > 장비관리 등록 처리
     * </pre>
     * @param map
     * @param model
     * @param request
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping("/eqp/mgmt/regEqpMgmtRDtl.do")
	 public ModelAndView regEqpMgmtRDtl( @RequestParam HashMap<String,String> map, ModelMap model,
			 HttpServletRequest request, SessionStatus status) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String errMsg = "";

        //필수값 확인
        HashMap<String,String> requireParams = new HashMap();
		requireParams.put("eqpBuyDiv", "장비도입구분");
		requireParams.put("eqpNm", "장비명");
		requireParams.put("srNo", "시리얼번호");
		requireParams.put("eqpTyp", "장비유형");
		requireParams.put("purcDt", "도입일자");
		requireParams.put("exprDt", "만료일자");
		requireParams.put("deprPrid", "내용연수");
		requireParams.put("rfidTag", "RFID TAG");
        validateUtil.check(map, requireParams);

    	try{


//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuLvl2Cnt");
//    		lvl2Cnt = comService.selectCommonQueryListTotCnt(map);
//    		if(map.get("menuLvl").equals("lvl2") && lvl2Cnt > 98){
//    			System.out.println("고마 느라 마이 느따 아이가");
//    			model.addAttribute(DemsConst.Messages_UserComMessage, egovMessageSource.getMessageArgs("admin.menu.001", null));
//    			return new ModelAndView(ajaxMainView, model);
//    		}

//    		map.put("pwd",DemsUtil.encryptSHA256(map.get("pwd")));
//    		map.put("sqlQueryId", "fsysUserDAO.regFsysUserRDtl");
//    		comService.updateCommonQuery(map);

    		map.put("regrId",user.getUserId());
    		eqpMgmtService.regEqpMgmtRDtl(map);

//    		map.put("sqlQueryId", "fsysMenuDAO.updFsysMenuRDtlOrdr");
//    		comService.updateCommonQuery(map);
//
//    		if(map.get("menuLvl").equals("lvl2") && lvl2Cnt > 1){
//    			map.put("sqlQueryId", "fsysMenuDAO.updFsysMenuNoReMake");
//    			comService.updateCommonQuery(map);
//    		}

//    		model.addAttribute(DemsConst.Messages_UserComMessage, egovMessageSource.getMessageArgs("success.common.insert", null));
    	}catch (Exception e) {
    		e.printStackTrace();
    		errMsg = URLEncoder.encode(egovMessageSource.getMessage("fail.request.msg"), "UTF-8");
    	}

		return new ModelAndView(ajaxMainView, model);

	}


    /**
	 *
	 * <pre>
	 * 1. 메소드명 : indexEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비관리 수정 화면 이동
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/eqp/mgmt/indexEqpMgmtUDtl.do")
	public String indexEqpMgmtUDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {

		return "dems/eqp/mgmt/eqpMgmtUDtl";

	}

	/**
	 *
	 * <pre>
	 * 1. 메소드명 : queryEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:49:20
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비관리 장비상세정보 조회
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("/eqp/mgmt/queryEqpMgmtUDtl.do")
	public ModelAndView queryEqpMgmtUDtl(@RequestParam HashMap<String,String> map, HttpServletRequest request, ModelMap model) throws Exception {

		//공통으로 필요한 데이타.(권한체크등...)
    	//model = comController.common(request, model);


		String errMsg = "";

    	try{

//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperList");
//    		List sysGrpList = comService.selectCommonQueryList(map);
//    		model.addAttribute("sysGrpList", sysGrpList);
//
//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperId");
//    		Map rtnMap = comService.selectCommonQueryMap(map);
//    		model.addAttribute("topMenuNo",rtnMap.get("topMenuNo"));

    		log.debug("=====queryEqpMgmtUDtl.do eqpSno=======>>>"+map.get("eqpSno"));
    		if(map.get("eqpSno") != null && !map.get("eqpSno").equals("")) {
    			map.put("sqlQueryId", "eqpMgmtDAO.queryEqpMgmtUDtl");
    			Map resultMap = comService.selectCommonQueryMap(map);
    			model.addAttribute("resultMap", resultMap);
    		}

    	}catch (Exception e) {
    		e.printStackTrace();
    		errMsg = URLEncoder.encode(egovMessageSource.getMessage("fail.request.msg"), "UTF-8");
    	}

    	return new ModelAndView(ajaxMainView, model);

    }

    /**
	 *
	 * <pre>
	 * 1. 메소드명 : updFsysMenuUDtl
	 * 2. 작성일 : 2021. 4. 14. 오전 11:20:39
	 * 3. 작성자 :
	 * 4. 설명 : 장비 지원 관리 > 장비관리 장비상세정보 수정 처리
	 * </pre>
	 *
	 * @param map
	 * @param model
	 * @param request
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/eqp/mgmt/updEqpMgmtUDtl.do")
	public ModelAndView updEqpMgmtUDtl(@RequestParam HashMap<String, String> map, ModelMap model,
			HttpServletRequest request, SessionStatus status) throws Exception {

		HashMap<String,String> requireParams = new HashMap();
		requireParams.put("eqpBuyDiv", "장비도입구분");
		requireParams.put("eqpNm", "장비명");
		requireParams.put("eqpTyp", "장비유형");
		requireParams.put("exprDt", "만료일자");
		requireParams.put("eqpCntrNo", "장비계약번호");
		requireParams.put("mdlNm", "모델명");
		requireParams.put("unitAmt", "단가");
		requireParams.put("sumAmt", "합계급액");
		requireParams.put("deprPrid", "내용연수");
		requireParams.put("payDiv", "유상_무상구분");
		requireParams.put("rfidTag", "장비_태그번호");

		validateUtil.check(map,requireParams);

		eqpMgmtService.updEqpMgmtUDtl(map);

		model.addAttribute(DemsConst.Messages_SysSucMessage,
				egovMessageSource.getMessageArgs("success.common.update", null));

		return new ModelAndView(ajaxMainView, model);
	}

	/**
	 *
	 * <pre>
	 * 1. 메소드명 : delEqpMgmtUDtl
	 * 2. 작성일 : 2021. 4. 14. 오전 11:20:53
	 * 3. 작성자 : jij
	 * 4. 설명 : 장비 지원 관리 > 장비관리 장비상세정보 삭제 처리
	 * </pre>
	 *
	 * @param map
	 * @param model
	 * @param request
	 * @param status
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/eqp/mgmt/delEqpMgmtUDtl.do")
	public ModelAndView delEqpMgmtUDtl(@RequestParam HashMap<String, String> map, ModelMap model,
			HttpServletRequest request, SessionStatus status) throws Exception {

//		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuLowerCnt");
//
//		int totCnt = comService.selectCommonQueryListTotCnt(map);
//
//		if (totCnt > 0) {
//
//			model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("admin.menu.001", null));
//
//		} else {
//
//			map.put("sqlQueryId", "fsysMenuDAO.delFsysMenuUDtl");
//			comService.updateCommonQuery(map);
//			model.addAttribute(DemsConst.Messages_SysSucMessage,
//					egovMessageSource.getMessageArgs("success.common.delete", null));
//
//		}

//		map.put("sqlQueryId", "fsysUserDAO.delFsysUserUDtl");
//		comService.updateCommonQuery(map);

		eqpMgmtService.delEqpMgmtUDtl(map);

		model.addAttribute(DemsConst.Messages_SysSucMessage,
				egovMessageSource.getMessageArgs("success.common.delete", null));

		return new ModelAndView(ajaxMainView, model);

	}

	/**
     *
     * <pre>
     * 1. 메소드명 : queryEqpMgmtFIndEqupQListPop
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 장비 조회 팝업
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception /eqp/mgmt/queryEqpMgmtFIndEqupQListPop.do
     */
    @RequestMapping(value="/eqp/mgmt/queryEqpMgmtFIndEqupQListPop.do")
    public ModelAndView queryEqpMgmtFIndEqupQListPop(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "eqpMgmtDAO.eqpMgmtMListTotCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);

    	PageUtil.calcPage(reqParams);

//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));

		reqParams.put("sqlQueryId", "eqpMgmtDAO.eqpMgmtMList");
		List list = comService.selectCommonQueryList(reqParams);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);

    }


}
