package dems.eqp.api.web;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.eqp.biz.service.EqpBizService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.fsys.user.service.FsysUserVO;
import frame.futil.DemsConst;
import frame.futil.DemsUtil;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;

@Controller
public class EqpApiController {

	protected Logger log = LoggerFactory.getLogger(EqpApiController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "EqpBizService")
	private EqpBizService eqpBizService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

    @Resource MappingJackson2JsonView ajaxMainView;


    @RequestMapping(value="/eqp/api/queryEquipTagDataJson.do")
    public ModelAndView queryEquipTagDataJson(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
    	HashMap resultMap = new HashMap<Object,String>();

		reqParams.put("sqlQueryId", "eqpApiDAO.queryEqpInfo");
		Map tempMap = comService.selectCommonQueryMap(reqParams);

		if(tempMap != null ){
			String status = (String)tempMap.get("pgsStat");
			// I 반입승인 , O 반출승인 , M 반입미승인 , G반출미승인 , P 무단반출입
			if(status == null){
				status = "P";
			} else if("C09001".equals(status)){
				status = "P";
			} else if("C09002".equals(status)){
				status = "O";
			} else if("C09003".equals(status)){
				status = "P";
			} else if("C09004".equals(status)){
				status = "P";
			}
			resultMap.put("AssetID", (String)tempMap.get("eqpSno"));
			resultMap.put("RequestNM", (String)tempMap.get("regrId"));
			resultMap.put("OwnerNM", (String)tempMap.get("regrId"));
			resultMap.put("Status", status);
			resultMap.put("TagId", (String)tempMap.get("rfidTag"));

		}else {
			resultMap.put("AssetID", "EMPTY");
			resultMap.put("RequestNM", "EMPTY");
			resultMap.put("OwnerNM", "EMPTY");
			resultMap.put("Status", "P");
			resultMap.put("TagId", reqParams.get("tagId"));
		}


		model.addAllAttributes(resultMap);

		return new ModelAndView(ajaxMainView, model);

    }


}
