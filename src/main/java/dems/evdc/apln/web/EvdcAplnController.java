package dems.evdc.apln.web;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovDateUtil;

import dems.evdc.apln.service.EvdcAplnService;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.fsys.user.service.FsysUserVO;
import frame.futil.DemsConst;
import frame.futil.DemsUtil;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;


/**
 * 
 * <pre>
 * 1. 클래스명 : EvdcAplnController.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 디지털 증거 관리 > 증거 처리 신청
 * </pre>
 */
@Controller
public class EvdcAplnController {

	protected Logger log = LoggerFactory.getLogger(EvdcAplnController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;
	
	@Resource(name = "evdcAplnService")
	private EvdcAplnService evdcAplnService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

    @Resource MappingJackson2JsonView ajaxMainView;

    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexEvdcAplnMList
     * 2. 작성일 : 2021. 4. 16. 오전 10:49:11
     * 3. 작성자 : ilyong
     * 4. 설명 : 디지털 증거 관리 > 증거 처리 신청 조회
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/evdc/apln/indexEvdcAplnMList.do")
    public String indexEvdcAplnMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {
    	
    	model.addAttribute("message", request.getParameter("message"));
    	if(request.getParameter("msg") != null){
    		model.addAttribute("msg", request.getParameter("msg"));
    	}
    	return "dems/evdc/apln/evdcAplnMList";
    }


	/**
     * 
     * <pre>
     * 1. 메소드명 : queryEvdcAplnMList
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 디지털 증거 관리 > 증거 처리 신청 조회 처리
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/evdc/apln/queryEvdcAplnMList.do")
    public ModelAndView queryEvdcAplnMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
    	//sessionInsttCd
    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	
    	reqParams.put("sessionUserGb", user.getUserGb());
    	reqParams.put("sessionInsttCd", user.getInsttCd());
    	reqParams.put("schPgsStat", ((String) reqParams.get("schPgsStat")).split(","));
    	
    	log.debug("=======queryEvdcAplnMList sessionUserGb========>>>"+user.getUserGb());
    	log.debug("=======queryEvdcAplnMList sessionInsttCd========>>>"+user.getInsttCd());
    	
    	reqParams.put("sqlQueryId", "evdcAplnDAO.queryEvdcAplnMListTotCnt"); 
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	
    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		reqParams.put("sqlQueryId", "evdcAplnDAO.queryEvdcAplnMList");
		List list = comService.selectCommonQueryList(reqParams);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
    
    /**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEvdcAplnRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 디지털 증거 관리 > 디지털 증거 폐기/이송 요청 등록화면
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception indexEvdcAplnMList.do
	 */
	@RequestMapping(value="/evdc/apln/indexEvdcAplnRDtl.do")
	public String indexEqpLendLendReqRDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	 	   
 	    model.addAttribute("reqInsttNm", user.getInsttNm());    //요청기관명
 	    model.addAttribute("reqDepNm", user.getDeptNm());      //요청부서명
 	    model.addAttribute("reqInsttCd", user.getInsttCd());  //요청기관코드
		
 	    return "dems/evdc/apln/evdcAplnMListPop";
		
	}
    
	/**
    *
    * <pre>
    * 1. 메소드명 : queryEvdcAplnReqPop
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : ilyong
    * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 요청 등록화면 > 사건조회 팝업 
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return ModelAndView
    * @throws Exception
    */
   @RequestMapping(value="/evdc/apln/queryEvdcAplnReqPop.do")
   public ModelAndView queryEvdcAplnReqPop(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

	    FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	   	reqParams.put("regrId", user.getUserId());
	   	reqParams.put("insttCd", user.getInsttCd());
	   	
	   	String type = (String) reqParams.get("type");
	   	String userGb = "";
	   	
//	   	if(type.equals("prosr")) {
//	   		userGb = "C01002";
//	   	}else if(type.equals("regUser")) {
//	   		userGb = "C01001";
//	   	}else if(type.equals("analCrgr")) {
//	   		userGb = "C01003";
//	   	}
	   	
	   	reqParams.put("sqlQueryId", "evdcAplnDAO.selectEvdcAplnReqPopListTotCnt");
	   	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
	   	reqParams.put("totalCount", totCnt);
	   	
	   	PageUtil.calcPage(reqParams);
	   	
	   	reqParams.put("sqlQueryId", "evdcAplnDAO.selectEvdcAplnReqPopList");
	   	
	   	List popList = comService.selectCommonQueryList(reqParams);		
		reqParams.put("popList", popList);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

   }
   
   /**
   *
   * <pre>
   * 1. 메소드명 : queryEvdcAplnDtlList
   * 2. 작성일 : 2021. 4. 20.
   * 3. 작성자 : ilyong
   * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 요청 등록화면 > 폐기대상 상세정보 조회
   * </pre>
   * @param map
   * @param model
   * @param request
   * @return ModelAndView
   * @throws Exception
   */
  @RequestMapping(value="/evdc/apln/queryEvdcAplnDtlList.do")
  public ModelAndView queryEvdcAplnDtlList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {
  	
// 	reqParams.put("sqlQueryId", "anlsReqDAO.selectAnlsReqInfo");
// 	Map anlsReqInfoMap = comService.selectCommonQueryMap(reqParams);
// 	reqParams.put("anlsReqInfoMap", anlsReqInfoMap);
 	
 	reqParams.put("sqlQueryId", "evdcAplnDAO.selectEvdcAplnDtlList");
  	List evdcAplnDtlList = comService.selectCommonQueryList(reqParams);
  	reqParams.put("evdcAplnDtlList", evdcAplnDtlList);
  	
//  	reqParams.put("sqlQueryId", "anlsReqDAO.selectAnlsReqInfoFileList");
//  	List fileList = comService.selectCommonQueryList(reqParams);
//  	reqParams.put("fileList", fileList);
  	
 	model.addAllAttributes(reqParams); 	
 	 
 	return new ModelAndView(ajaxMainView, model);
  }
   
  
  /**
  *
  * <pre>
  * 1. 메소드명 : modifyEvdcAplnRDtl
  * 2. 작성일 : 2021. 4. 30.
  * 3. 작성자 : ilyong
  * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 요청 등록
  * </pre>
  * @param map
  * @param model
  * @param request
  * @return
  * @throws Exception
  */
 @RequestMapping(value="/evdc/apln/modifyEvdcAplnRDtl.do")
 public ModelAndView modifyEvdcAplnRDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	
	   	ObjectMapper mapper = new ObjectMapper();
	   	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
	   	Map<String, Map<String, String>> jsonString = mapper.readValue(paramData, Map.class);
	
	   	//장비대여 대여장비정보
	   	Map<String, Object> formDataMap1 = (Map<String, Object>) jsonObject.get("formDatas1");
	   	
		Map<String, Object> formDataMap2 = (Map<String, Object>) jsonObject.get("formDatas2");
		
		//장비대여 신청정보
	   	ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
	   	
	   	
	   	log.debug("======CON regEqpMntRDtl formDataMap1========>>>"+formDataMap1.size());
	   	log.debug("======CON regEqpMntRDtl formDataMap2========>>>"+formDataMap2.size());
	   	
	   	
//	   	log.debug("======CON regEqpMntRDtl tpList========>>>"+tpList.size());
	   	
	   	Map<String, Object> paramMap = new HashMap<String, Object>();
	   	paramMap.put("formDataMap1", formDataMap1);
	   	paramMap.put("formDataMap2", formDataMap2);
//	   	paramMap.put("formDataMap3", formDataMap3);
	   	paramMap.put("detailList", tpList);
	   	paramMap.put("userGb", user.getUserGb());
	   	paramMap.put("userId", user.getUserId());

	  //필수값 확인
	   	HashMap<String,String> requireParams = new HashMap();
	   	requireParams.put("reqInsttCd", "요청기관코드");
	   	requireParams.put("reqDt", "요청일자");
	   	requireParams.put("reqUserId", "요청자ID");
	   	requireParams.put("prosrId", "주임군검사ID");
	   	requireParams.put("reqDiv", "요청구분");
	   	
	   	HashMap<String,String> chkParams = new HashMap();
	   	chkParams.putAll((Map<String, String>) jsonString.get("formDatas2"));
	   	chkParams.putAll((Map<String, String>) jsonString.get("formDatas1"));
	   	validateUtil.check(chkParams, requireParams);
	   	
	   	evdcAplnService.modifyEvdcAplnRDtl(paramMap);

	   	return new ModelAndView(ajaxMainView, model);

 }
 
 	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEvdcAplnUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 디지털 증거 관리 > 디지털 증거 폐기/이송 요청 수정화면
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/evdc/apln/indexEvdcAplnUDtl.do")   
	public String indexEvdcAplnUDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/evdc/apln/evdcAplnMListPopUDtl";
		
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEvdcAplnVDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 디지털 증거 관리 > 디지털 증거 폐기/이송 요청 상세화면
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/evdc/apln/indexEvdcAplnVDtl.do")   
	public String indexEvdcAplnVDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/evdc/apln/evdcAplnMListPopVDtl";
		
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : queryEvdcDisuseReqInfo
	 * 2. 작성일 : 2021. 4. 16. 오전 9:49:20
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 디지털 증거 관리 > 디지털 증거 폐기/이송 요청 수정화면 처리
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("/evdc/apln/queryEvdcDisuseReqInfo.do")
	public ModelAndView queryEvdcDisuseReqInfo(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

		//공통으로 필요한 데이타.(권한체크등...)
    	//model = comController.common(request, model);


		String errMsg = "";

    	try{
    		
//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperList");
//    		List sysGrpList = comService.selectCommonQueryList(map);
//    		model.addAttribute("sysGrpList", sysGrpList);
//    		
//    		map.put("sqlQueryId", "fsysMenuDAO.queryFsysMenuUpperId");
//    		Map rtnMap = comService.selectCommonQueryMap(map);
//    		model.addAttribute("topMenuNo",rtnMap.get("topMenuNo"));
    		
    		log.debug("=====queryEqpMgmtUDtl.do disuseReqSno=======>>>"+reqParams.get("disuseReqSno"));
    		
    		reqParams.put("sqlQueryId", "evdcAplnDAO.selectEvdcDisuseReqInfo");
			Map resultMap = comService.selectCommonQueryMap(reqParams);
			reqParams.put("resultMap", resultMap);
			
			model.addAllAttributes(reqParams); 	
    		

    	}catch (Exception e) {
    		e.printStackTrace();
    		errMsg = URLEncoder.encode(egovMessageSource.getMessage("fail.request.msg"), "UTF-8");
    	}
    	
    	return new ModelAndView(ajaxMainView, model);

    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryEvdcAnlsListPop
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 증거목록 조회 팝업 
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception /eqp/mgmt/queryEqpMgmtFIndEqupQListPop.do
     */
    @RequestMapping(value="/evdc/apln/queryEvdcAnlsListPop.do")
    public ModelAndView queryEvdcAnlsListPop(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
    	
    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	
    	reqParams.put("sessionUserGb", user.getUserGb());
    	reqParams.put("sessionInsttCd", user.getInsttCd());
    	
    	log.debug("=======queryEvdcAnlsListPop sessionUserGb========>>>"+user.getUserGb());
    	log.debug("=======queryEvdcAnlsListPop sessionInsttCd========>>>"+user.getInsttCd());
    	
		reqParams.put("sqlQueryId", "evdcAplnDAO.evdcAnlsListPopTotCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	
    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		reqParams.put("sqlQueryId", "evdcAplnDAO.evdcAnlsListPop");
		List list = comService.selectCommonQueryList(reqParams);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
    
    /**
    *
    * <pre>
    * 1. 메소드명 : ModifySelEvdcAnlsList
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 선택 승인요청, 검사승인
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception 
    */
   @RequestMapping(value="/evdc/apln/ModifySelEvdcAnlsList.do")
   public ModelAndView ModifySelEvdcAnlsList(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

 	   FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
 	   
 	   ObjectMapper mapper = new ObjectMapper();
 	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
 	
 	   //요청 리스트
 	   List tpList = (List) jsonObject.get("rowDatas");
 	   
 	   //요청 타입
 	   Map<String, Object> infoMap = jsonObject.get("type");
 	   String type = (String) infoMap.get("type");
 	
 	   Map<String, Object> paramMap = new HashMap<String, Object>();
 	   paramMap.put("tpList", tpList);
 	   paramMap.put("userGb", user.getUserGb());
 	   paramMap.put("userId", user.getUserId());
 	   paramMap.put("insttCd", user.getInsttCd());
 	   paramMap.put("type", type);
 	
 	  evdcAplnService.ModifySelEvdcAnlsList(paramMap);
 	   
 	   return new ModelAndView(ajaxMainView, model);

   }
    
}
