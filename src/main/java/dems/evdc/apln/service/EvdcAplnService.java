package dems.evdc.apln.service;

import java.util.HashMap;
import java.util.Map;

import frame.flyt.login.service.FLytLoginVO;



/**
 * 
 * <pre>
 * 1. 클래스명 : EqpLendService.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 디지털 증거 관리 > 증거 처리 신청
 * </pre>
 */
@SuppressWarnings("rawtypes")
public interface EvdcAplnService {


	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여관리 등록 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	//void regEqpBizRDtl(HashMap<String, String> map)  throws Exception;
	String regEqpBizRDtl(Map<String,Object> paramMap, Map<String,Object> fileMap) throws Exception;
	
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:17:54
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 장비대여관리 상세정보 수정 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	//void updEqpBizUDtl(HashMap<String, String> map)  throws Exception;
	String updEqpBizUDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception;
	
	
	/**
	 * 
	 * <pre>
     * 1. 메소드명 : modifyEvdcAplnRDtl
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 요청 등록
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void modifyEvdcAplnRDtl(Map<String,Object> map) throws Exception;
	
	/**
	 * 
	 * <pre>
     * 1. 메소드명 : ModifySelEqpLendRequest
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  장비 지원 관리 > 장비대여관리 리스트에서 선택한 내역 반납처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void ModifySelEqpLendRequest(Map<String,Object> map) throws Exception;
	
	/**
	 * 
	 * <pre>
     * 1. 메소드명 : ModifySelEvdcAnlsList
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 선택 승인요청, 검사승인
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void ModifySelEvdcAnlsList(Map<String,Object> map) throws Exception;
}

