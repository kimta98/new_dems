package dems.evdc.apln.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import dems.bbs.free.service.impl.BbsFreeServiceImpl;
import dems.evdc.apln.service.EvdcAplnService;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;


/**
 * 
 * <pre>
 * 1. 클래스명 : EvdcAplnServiceImpl.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 디지털 증거 관리 > 증거 처리 신청
 * </pre>
 */
@Service("evdcAplnService")
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class EvdcAplnServiceImpl extends EgovAbstractServiceImpl implements	EvdcAplnService{
	
	protected Logger log = LoggerFactory.getLogger(EvdcAplnServiceImpl.class);
	
	@Resource(name="evdcAplnDAO")
	private EvdcAplnDAO evdcAplnDAO;
	
	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="comDAO")
	private ComDAO comDAO;
	
	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 등록 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String regEqpBizRDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {		
		
		String result = "";

		boolean denyListChk = true;
		String[] eList = EgovProperties.getProperty("Globals.fileUpload.Extensions").toString().split("\\.");

        if(fileMap.get("fileList4OriFileNm") != null){
            if (fileMap.get("fileList4OriFileNm") instanceof String) {
            	String fileList = (String) fileMap.get("fileList4OriFileNm");
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
        	}else {
        		List<String> fileList = (List<String>) fileMap.get("fileList4OriFileNm");
        		for (int i = 0; i < fileList.size(); i++) {
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.get(i).toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
    			}
        	}
        }

	 	if (denyListChk && fileMap.get("fileList4OriFileNm") != null) {
	 		result = "2";
        }else{
	    	//첨부파일등록
			String loginId = (String) paramMap.get("regrId");
	    	String atchFileId = (String) fileMap.get("atchFileId");

	    	if(EgovStringUtil.isNull(atchFileId)){
	    		atchFileId = comService.insertAtchFile("fileList4", "10", loginId, fileMap);
	    	}else{
	    		atchFileId = comService.updateAtchFile(atchFileId, "fileList4", "10", loginId, fileMap);
	    	}
	    	paramMap.put("atchFileId", atchFileId);
	    	//bbsFreeDAO.insertBbsFrees(paramMap);
	    	
	    	evdcAplnDAO.regEqpBizRDtl(paramMap);
	    	
	    	result = "1";
        }

        return result;
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 10:20:13
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 장비 지원 관리 > 유지보수관리 수정 처리
	 * </pre>
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String updEqpBizUDtl(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {	
		// TODO Auto-generated method stub
		log.debug("========updEqpBizUDtl  loginId111111111111==========>>>");
		log.debug("========updEqpBizUDtl  atchFileId22222222222==========>>>");
		
		String result = "";
		String atchFileId = (String) paramMap.get("atchFileId");
		String loginId =  (String) paramMap.get("regrId");
		
		log.debug("========updEqpBizUDtl  loginId==========>>>"+loginId);
		log.debug("========updEqpBizUDtl  atchFileId==========>>>"+atchFileId);
		
		boolean denyListChk = true;
        String[] eList = EgovProperties.getProperty("Globals.fileUpload.Extensions").toString().split("\\.");
		
        if(fileMap.get("fileList4OriFileNm") != null){
            if (fileMap.get("fileList4OriFileNm") instanceof String) {
            	String fileList = (String) fileMap.get("fileList4OriFileNm");
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
        	}else {
        		List<String> fileList = (List<String>) fileMap.get("fileList4OriFileNm");
        		for (int i = 0; i < fileList.size(); i++) {
                	for(int k=0; k < eList.length ;k++){
            	 		if (fileList.get(i).toLowerCase().indexOf("."+eList[k]) > -1){
               		 		denyListChk = false;
            	 		}
            	 	}
    			}
        	}
        }
        
        if (denyListChk && fileMap.get("fileList4OriFileNm") != null) {
	 		result = "2";
        }else{
	       	if(EgovStringUtil.isNull(atchFileId)){
	       		atchFileId = comService.insertAtchFile("fileList4", "10", loginId, fileMap);
	       	}else{
	       		atchFileId = comService.updateAtchFile(atchFileId, "fileList4", "10", loginId, fileMap);
	       	}

//	       	paramMap.put("atchFileId", atchFileId);
//	    	bbsFreeDAO.updateBbsFree(paramMap);
	       	
	       	//장비관리 수정 처리
	       	paramMap.put("atchFileId", atchFileId);
	       	evdcAplnDAO.updEqpBizUDtl(paramMap);
	       	
	    	result = "1"; 
        }
        
		
		
		return result; 
	}
	
	
	
	/**
    *
    * <pre>
    * 1. 메소드명 : modifyEvdcAplnRDtl
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 요청 등록
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void modifyEvdcAplnRDtl(Map<String, Object> map) throws Exception {

		Map<String,Object> paramMap = new HashMap<String,Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		//validateUtil.check(chkParamMap, requireParams);

		String userGb = (String) map.get("userGb"); //userId
		String userId = (String) map.get("userId");
		String currentDay = EgovDateUtil.getToday();
		String modFlag = ""; //
		String saveGb = ""; //
		
		String disuseReqSno = ""; //폐기요청일련번호
		String reqDiv = ""; //요청구분	(폐기요청구분: C07001 폐기, C07002 이송)
		String reqDt = "";	//요청일자
		String reqInsttCd = "";	//요청기관
		String reqUserId = "";	//담당자ID(요청자ID)
		
		String prosrId = "";	//주임군검사ID
		String prosrAprvDt = "";	//주임군검사 승인일자 
		String rcvDt = "";	//접수일시  
		String rcvNo = "";	//접수번호  
		String rcvrId = "";	//접수자ID  
		
		String aprvDt = "";	//승인일자  
		String aprvrId = "";	//승인자ID  
		String pgsStat = "";	//진행상태  
		String useYn = "";	//사용여부  
		String regrId = "";	//등록자ID 
		
		String reqTransInstt = "";	//이송요청기관ID  
		String disuseDt = ""; //폐기일시
		String disuseMthd = ""; //폐기방법 (폐기방법구분: C08001	완전삭제, C08002	이송)
		String transInstt = ""; //이송기관
		 
		
		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap1"));
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap2"));
		
		if(map.get("formDataMap3") != null && map.get("formDataMap3") !="") {
			paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap3"));
		}
		
		//DEMS_EVDC_DISUSE_RSLT list
		ArrayList<Map<String, Object>> paramDetailList = (ArrayList<Map<String, Object>>) map.get("detailList");
		
		modFlag = (String)paramMap.get("modFlag");
		saveGb = (String)paramMap.get("saveGb");
		
		if(!modFlag.equals("insert")) {
			disuseReqSno = (String)paramMap.get("disuseReqSno");
			log.debug("==============modifyEvdcAplnRDtl 	폐기요청일련번호===========>>>>>>" + disuseReqSno);
		}
		
		reqDiv = (String)paramMap.get("reqDiv");
		reqDt = (String)paramMap.get("reqDt");
		reqInsttCd = (String)paramMap.get("reqInsttCd");
		reqUserId = (String)paramMap.get("reqUserId");
		prosrId = (String)paramMap.get("prosrId");
		prosrAprvDt = (String)paramMap.get("prosrAprvDt");
		rcvDt = (String)paramMap.get("rcvDt");
		rcvNo = (String)paramMap.get("rcvNo");
		rcvrId = (String)paramMap.get("rcvrId");
		aprvDt = (String)paramMap.get("aprvDt");
		
		
		aprvrId = (String)paramMap.get("aprvrId");	//승인자ID  
		pgsStat = (String)paramMap.get("pgsStat");	//진행상태  
		useYn = "Y";	//사용여부  
		regrId = userId;	//등록자ID 
		
		reqTransInstt = (String)paramMap.get("reqTransInstt");	//이송요청기관ID  
		disuseDt = (String)paramMap.get("disuseDt"); //폐기일시
		disuseMthd = (String)paramMap.get("disuseMthd"); //폐기방법
		transInstt = (String)paramMap.get("transInstt"); //이송기관
		
		
		log.debug("==============modifyEvdcAplnRDtl 	userGb===========>>>>>>" + userGb);
		log.debug("==============modifyEvdcAplnRDtl 	userId===========>>>>>>" + userId);
		log.debug("==============modifyEvdcAplnRDtl 	modFlag===========>>>>>>" + modFlag);
		log.debug("==============modifyEvdcAplnRDtl 	saveGb===========>>>>>>" + saveGb);
		log.debug("==============modifyEvdcAplnRDtl 	currentDay===========>>>>>>" + currentDay);
		
		log.debug("==============modifyEvdcAplnRDtl 	disuseReqSno===========>>>>>>" + disuseReqSno);
		log.debug("==============modifyEvdcAplnRDtl 	reqDiv===========>>>>>>" + reqDiv);
		log.debug("==============modifyEvdcAplnRDtl 	reqDt===========>>>>>>" + reqDt);
		log.debug("==============modifyEvdcAplnRDtl 	reqInsttCd===========>>>>>>" + reqInsttCd);
		log.debug("==============modifyEvdcAplnRDtl 	reqUserId===========>>>>>>" + reqUserId);
		
		log.debug("==============modifyEvdcAplnRDtl 	prosrId===========>>>>>>" + prosrId);
		log.debug("==============modifyEvdcAplnRDtl 	prosrAprvDt===========>>>>>>" + prosrAprvDt);
		log.debug("==============modifyEvdcAplnRDtl 	rcvDt===========>>>>>>" + rcvDt);
		log.debug("==============modifyEvdcAplnRDtl 	rcvNo===========>>>>>>" + rcvNo);
		log.debug("==============modifyEvdcAplnRDtl 	rcvrId===========>>>>>>" + rcvrId);
		
		log.debug("==============modifyEvdcAplnRDtl 	aprvDt===========>>>>>>" + aprvDt);
		log.debug("==============modifyEvdcAplnRDtl 	aprvrId===========>>>>>>" + aprvrId);
		log.debug("==============modifyEvdcAplnRDtl 	pgsStat===========>>>>>>" + pgsStat);
		log.debug("==============modifyEvdcAplnRDtl 	useYn===========>>>>>>" + useYn);
		log.debug("==============modifyEvdcAplnRDtl 	regrId===========>>>>>>" + regrId);
		
		log.debug("==============modifyEvdcAplnRDtl 	reqTransInstt===========>>>>>>" + reqTransInstt);
		log.debug("==============modifyEvdcAplnRDtl 	disuseDt===========>>>>>>" + disuseDt);
		log.debug("==============modifyEvdcAplnRDtl 	disuseMthd===========>>>>>>" + disuseMthd);
		log.debug("==============modifyEvdcAplnRDtl 	transInstt===========>>>>>>" + transInstt);
		
			
		for(int i =0; i < paramDetailList.size(); i++) {
			log.debug("==============modifyEvdcAplnRDtl 	paramDetailList.get("+i+")===========>>>>>>" + paramDetailList.get(i));
			
		}
		
		if(modFlag.equals("insert")) {
			//폐기요청일련번호 조회
			disuseReqSno = evdcAplnDAO.selectDisuseReqSno(paramMap);
			
			paramMap.clear();
			paramMap.put("disuseReqSno", disuseReqSno);
			paramMap.put("reqDiv"	, reqDiv);
			paramMap.put("reqDt"	, reqDt);
			paramMap.put("reqInsttCd"	, reqInsttCd);
			paramMap.put("reqUserId"	, reqUserId);
			paramMap.put("prosrId"	, prosrId);
			paramMap.put("regrId"	, regrId);
			paramMap.put("reqTransInstt"	, reqTransInstt);
			
			//진행상태코드 C03001 임시등록, C03002	승인요청, C03003 검사승인, C03005 문서접수, C03006 과장승인  
			if(saveGb.equals("imsi")) {
				paramMap.put("pgsStat"	, "C03001"); //임시등록
			}else {
				paramMap.put("pgsStat"	, "C03002"); //승인요청
			}
			
			
			log.debug("=============== >>>> ######## modifyEvdcAplnRDtl(insertEvdcDisuseReq) paramMap.toString() ######## <<<<<< =============>>>>>>>"+paramMap.toString());
			
			//증거폐기요청 INSERT
			evdcAplnDAO.insertEvdcDisuseReq(paramMap);
			
			
			for(int i =0; i < paramDetailList.size(); i++) {
				log.debug("===============only insertEqpMntRDtl paramDetailList["+i+"] =============>>>>>>>"+paramDetailList.get(i));
				
					paramMap.clear();
					paramMap.put("disuseReqSno", disuseReqSno); //reqDiv, reqTransInstt
					paramMap.put("disuseMthd", reqDiv);
					paramMap.put("transInstt", reqTransInstt);
					paramMap.putAll(paramDetailList.get(i));
					log.debug("=============== >>>> ######## modifyEvdcAplnRDtl(insertEvdcDisuseRslt) paramMap.toString() ######## <<<<<< =============>>>>>>>"+paramMap.toString());
					
					//증거폐기 이관 INSERT
					evdcAplnDAO.insertEvdcDisuseRslt(paramMap);
			}
		}else if(modFlag.equals("modify")){
			if(saveGb.equals("del")) {
				paramMap.clear();
				paramMap.put("disuseReqSno", disuseReqSno);
				log.debug("=============== >>>> ######## modFlag(modify) saveGb(del) modifyEvdcAplnRDtl(deleteEvdcDisuseRslt) paramMap.toString() ######## <<<<<< =============>>>>>>>"+paramMap.toString());
				
				//증거폐기 이관 DELETE
				evdcAplnDAO.deleteEvdcDisuseRslt(paramMap);
				
				//증거폐기요청 DELETE
				paramMap.clear();
				paramMap.put("disuseReqSno", disuseReqSno);
				log.debug("=============== >>>> ######## modFlag(modify) saveGb(del) modifyEvdcAplnRDtl(deleteEvdcDisuseReq) paramMap.toString() ######## <<<<<< =============>>>>>>>"+paramMap.toString());
				
				evdcAplnDAO.deleteEvdcDisuseReq(paramMap);
			}else {
				paramMap.clear();
				paramMap.put("disuseReqSno", disuseReqSno);
				paramMap.put("reqDiv"	, reqDiv);
				paramMap.put("reqDt"	, reqDt);
				paramMap.put("reqInsttCd"	, reqInsttCd);
				paramMap.put("reqUserId"	, reqUserId);
				paramMap.put("prosrId"	, prosrId);
				paramMap.put("reqTransInstt"	, reqTransInstt);
				paramMap.put("regrId"	, regrId);
				
				//진행상태코드 C03001 임시등록, C03002	승인요청, C03003 검사승인, C03005 문서접수, C03006 과장승인  
				if(saveGb.equals("imsi")) {
					paramMap.put("pgsStat"	, "C03001"); //임시등록
				}else if(saveGb.equals("req")) {
					paramMap.put("pgsStat"	, "C03002"); //승인요청
				}else if(saveGb.equals("reqCnc")) {
					paramMap.put("pgsStat"	, "C03001"); //승인요청취소
				}else {
					paramMap.put("pgsStat"	, pgsStat); //승인요청취소
				}
				log.debug("=============== >>>> ######## modFlag(modify) modifyEvdcAplnRDtl(insertEvdcDisuseReq) paramMap.toString() ######## <<<<<< =============>>>>>>>"+paramMap.toString());
				
				//디지털 증거 폐기/이송 요청 정보 수정
				evdcAplnDAO.modifyEvdcDisuseReq(paramMap);
				
				paramMap.clear();
				paramMap.put("disuseReqSno", disuseReqSno);
				
				//증거폐기 이관 DELETE
				evdcAplnDAO.deleteEvdcDisuseRslt(paramMap);
				
				for(int i =0; i < paramDetailList.size(); i++) {
					log.debug("===============modFlag(modify) modifyEvdcAplnRDtl paramDetailList["+i+"] =============>>>>>>>"+paramDetailList.get(i));
					
						paramMap.clear();
						paramMap.put("disuseReqSno", disuseReqSno);
						paramMap.put("disuseMthd", reqDiv);
						paramMap.put("transInstt", reqTransInstt);
						paramMap.putAll(paramDetailList.get(i));
						log.debug("=============== >>>> ######## modFlag(modify) modifyEvdcAplnRDtl(insertEvdcDisuseRslt) paramMap.toString() ######## <<<<<< =============>>>>>>>"+paramMap.toString());
						
						//증거폐기 이관 INSERT
						evdcAplnDAO.insertEvdcDisuseRslt(paramMap);
				}
			}
			
		}else if(modFlag.equals("aprv")) {
			if(saveGb.equals("aprv")) { //승인
				paramMap.clear();
				paramMap.put("disuseReqSno", disuseReqSno);
				paramMap.put("prosrId"	, regrId);
				paramMap.put("prosrAprvDt"	, currentDay);
				paramMap.put("pgsStat"	, "C03003");
				paramMap.put("regrId"	, regrId);
				log.debug("=============== >>>> ######## modFlag(aprv) modifyEvdcAplnRDtl(modifyEvdcAnlsListAprv) paramMap.toString() ######## <<<<<< =============>>>>>>>"+paramMap.toString());
				
				//증거폐기요청 검사승인
				evdcAplnDAO.modifyEvdcAnlsListAprv(paramMap);
				
			}else if(saveGb.equals("aprvCnc")) { //승인취소
				paramMap.clear();
				paramMap.put("disuseReqSno", disuseReqSno);
				paramMap.put("prosrId"	, regrId);
				paramMap.put("prosrAprvDt"	, "");
				paramMap.put("pgsStat"	, "C03002");
				paramMap.put("regrId"	, regrId);
				log.debug("=============== >>>> ######## modFlag(aprvCnc) modifyEvdcAplnRDtl(modifyEvdcAnlsListAprv) paramMap.toString() ######## <<<<<< =============>>>>>>>"+paramMap.toString());
				
				//증거폐기요청 검사승인 취소
				evdcAplnDAO.modifyEvdcAnlsListAprv(paramMap);
			}
			
		}
			
//			
//			
//			
//			
//		}else if(modFlag.equals("modify")){
//			log.debug("=============== >>>> ######## paramMap.toString()222222 ######## <<<<<< =============>>>>>>>"+paramMap.toString());
//			
////			UPDATE DEMS_EVDC_DISUSE_REQ
////			SET REQ_DT	= REPLACE(#{reqDt},'-','')
////			  , REQ_DIV = #{reqDiv}
////			  , REQ_USER_ID	= #{reqUserId}
////			  , PROSR_ID	= #{prosrId}
////			  , PGS_STAT = #{pgsStat}
////			  , UPD_DT			= NOW()
////			  , UPDR_ID			= #{userId}
////		  WHERE DISUSE_REQ_SNO	= #{disuseReqSno}
//			paramMap.clear();
//			paramMap.put("userId", userId);
//			paramMap.put("rentUserId"	, rentUserId);
//			paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap1"));
//			paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap2"));
//			
//			//디지털 증거 폐기/이송 요청 정보 수정
//			evdcAplnDAO.modifyEvdcDisuseReq(paramMap);
//			
//			if(reqDiv.equals("C07001")) {  // 폐기요청구분: C07001 폐기, C07002 이송
//				disuseMthd = "C08001"; // 폐기방법구분: C08001	완전삭제, C08002	이송
//				transInstt = "";
//			}else {
//				disuseMthd = "C08002"; 
//			}
//			
//			paramMap.clear();
//			paramMap.put("disuseReqSno", disuseReqSno);
//			paramMap.put("analReqSno", analReqSno);
//			paramMap.put("evdcSno", evdcSno);
//			paramMap.put("disuseMthd", disuseMthd);
//			paramMap.put("transInstt", transInstt);
//			//transInstt
//			
////			modFlag = (String)paramMap.get("modFlag");
////			reqInsttCd = (String)paramMap.get("reqInsttCd");
////			reqDt = (String)paramMap.get("reqDt");
////			incdtSno = (String)paramMap.get("incdtSno");
////			analReqSno = (String)paramMap.get("analReqSno");
////			prosrId = (String)paramMap.get("prosrId");
////			reqUserId = (String)paramMap.get("reqUserId");
////			reqDiv = (String)paramMap.get("reqDiv");
////			transInstt = (String)paramMap.get("transInstt");
////			evdcSno = (String)paramMap.get("transInstt");
//			
//			
//			log.debug("=============== >>>> ######## paramMap.toString()33333 ######## <<<<<< =============>>>>>>>"+paramMap.toString());
//			//디지털 증거 폐기 이관 정보 수정
//			evdcAplnDAO.modifyEvdcDisuseRslt(paramMap);
//			
//		}else if(modFlag.equals("aprv")) {
//			log.debug("=======modifyEqpLendRequest rentAplnSno(대여신청일련번호)=========>>>"+rentAplnSno);
////			log.debug("=======modifyEqpLendRequest rentCfrmDt(대여확인일자)=========>>>"+rentCfrmDt);
////			log.debug("=======modifyEqpLendRequest rentUserId(rentCfrmrId)=========>>>"+rentUserId);
////			if(pgsStat.equals("lendAprv")) {
////				log.debug("=======##### modifyEqpLendRequest 대여승인 #####=========>>>");
////				
////				paramMap.clear();
////				paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
////				paramMap.put("rentCfrmDt", rentCfrmDt);	//대여확인일자
////				paramMap.put("rentCfrmrId"	, rentUserId);	//대여확인자id
////				paramMap.put("pgsStat"	, "C09002"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)  
////				paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
////				paramMap.toString();
////				log.debug("=======##### modifyEqpLendRequest 대여승인 paramMap.toString() #####=========>>>"+paramMap.toString());
////				
////				evdcAplnDAO.modifyEqpLendRequestAprv(paramMap);
////				evdcAplnDAO.modifyEqpLendRequestDtlAprv(paramMap);
////				
////			}else if(pgsStat.equals("lendCnc")) {
////				log.debug("=======##### modifyEqpLendRequest 대여승인 취소 #####=========>>>");
////				
////				paramMap.clear();
////				paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
////				paramMap.put("rentCfrmDt", "");	//대여확인일자
////				paramMap.put("rentCfrmrId"	, "");	//대여확인자id
////				paramMap.put("pgsStat"	, "C09001"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
////				paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
////				paramMap.toString();
////				
////				evdcAplnDAO.modifyEqpLendRequestAprv(paramMap);
////				evdcAplnDAO.modifyEqpLendRequestDtlAprv(paramMap);
////				
////			}else if(pgsStat.equals("rtnCfrm")) {
////				log.debug("=======##### modifyEqpLendRequest 반납확인 #####=========>>>");
////				
////				paramMap.clear();
////				paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
////				paramMap.put("rtnCfrmrId"	, rentUserId);	//반납확인자id
////				paramMap.put("rtnCfrmrDt", rtnCfrmrDt);	//반납확인일자
////				paramMap.put("pgsStat"	, "C09003"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
////				paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
////				paramMap.toString();
////				
////				evdcAplnDAO.modifyEqpLendRequestDtlAprv(paramMap);
////				
////			}else if(pgsStat.equals("rtnCnc")) {
////				log.debug("=======##### modifyEqpLendRequest 반납취소 #####=========>>>");
////				paramMap.clear();
////				paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
////				paramMap.put("rtnCfrmrId"	, "");	//반납확인자id
////				paramMap.put("rtnCfrmrDt", "");	//반납확인일자
////				paramMap.put("pgsStat"	, "C09002"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
////				paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
////				paramMap.toString();
////				
////				evdcAplnDAO.modifyEqpLendRequestDtlAprv(paramMap);
////			}
////}
		

		

	}
	
	/**
    *
    * <pre>
    * 1. 메소드명 : ModifySelEqpLendRequest
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 장비대여관리 리스트에서 선택한 내역 반납처리
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void ModifySelEqpLendRequest(Map<String, Object> map) throws Exception {

		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		String type = (String) map.get("type");
		List rtnCfrmList = (List) map.get("rtnCfrmList");
		String userGb = (String) map.get("userGb");
		String insttCd = (String) map.get("insttCd");
		String userId = (String) map.get("userId");
		String currentDay = EgovDateUtil.getToday();
		
		log.debug("=======##### ModifySelEqpLendRequest currentDay #####=========>>>"+currentDay);
		
		Map<String, Object> infoMap = new HashMap<String, Object>();
		
//		paramMap.put("rentAplnSno", rentAplnSno);	//대여신청일련번호
//		paramMap.put("rtnCfrmrId"	, rentUserId);	//반납확인자id
//		paramMap.put("rtnCfrmrDt", rtnCfrmrDt);	//반납확인일자
//		paramMap.put("pgsStat"	, "C09003"); //(대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
//		paramMap.put("sessionUserId", sessionUserId);	//로그인사용자id
		
		switch (type) {
		case "rtnCfrm":     //반납처리
			infoMap.put("rtnCfrmrId", userId);
			infoMap.put("pgsStat", "C09003");
			infoMap.put("sessionUserId", userId);
			break;
		}
		
		//권한체크
		if(!"rtnCfrm".equals(type) || !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식 수사관"}));
		}
		
		
		int chkAprv = 0;
		String pgsStat = "";	
		
		for(int i = 0; i < rtnCfrmList.size(); i++) {
			
			paramMap.clear();
			paramMap.putAll(infoMap);
			paramMap.put("rentAplnSno", rtnCfrmList.get(i));
			
			//장비 대여진행상태 조회 (대여진행상태 : C09001 대여신청, C09002 대여중, C09003	반납확인)
			paramMap.put("sqlQueryId", "evdcAplnDAO.selectPgsStat");
			pgsStat = comDAO.selectCommonQueryString(paramMap); 	
			
			if(!"C09002".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) rtnCfrmList.get(i),"대여중"}));
			}			
			
		}
		
		//DEMS_ANALYSIS_SUPP_REQ 진행상태 변경 rentAplnSno  rtnCfrmrDt
		for(int i = 0; i < rtnCfrmList.size(); i++) {
			
			paramMap.clear();			
			paramMap.putAll(infoMap);	
			paramMap.putAll(map);
			paramMap.put("rentAplnSno", rtnCfrmList.get(i));
			paramMap.put("rtnCfrmrDt", currentDay);
			
			log.debug("=======##### ModifySelEqpLendRequest rtnCfrmList.get["+i+"] #####=========>>>"+rtnCfrmList.get(i));
			log.debug("=======##### ModifySelEqpLendRequest 반납확인 paramMap.toString() #####=========>>>"+paramMap.toString());
			
			evdcAplnDAO.modifyEqpLendRequestDtlAprv(paramMap);
			
			
			//comDAO.updateCommonQuery(paramMap);
	
		}
		
				

	}
	
	/**
    *
    * <pre>
    * 1. 메소드명 : ModifySelEvdcAnlsList
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 선택 승인요청, 검사승인
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void ModifySelEvdcAnlsList(Map<String, Object> map) throws Exception {

		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		String type = (String) map.get("type");
		List tpList = (List) map.get("tpList");
		String userGb = (String) map.get("userGb");
		String insttCd = (String) map.get("insttCd");
		String prosrId = (String) map.get("prosrId");
		String userId = (String) map.get("userId");
		String currentDay = EgovDateUtil.getToday();
		
		log.debug("=======##### ModifySelEvdcAnlsList currentDay #####=========>>>"+currentDay);
		
		Map<String, Object> infoMap = new HashMap<String, Object>();
		
		
		switch (type) {
		case "pscAprv":     //검사승인
			infoMap.put("prosrId", userId);
			infoMap.put("prosrAprvDt", currentDay);
			infoMap.put("pgsStat", "C03003");
			infoMap.put("regrId", userId);
			break;
		case "rtnCfrm":     //승인요청
			infoMap.put("pgsStat", "C03002");   //승인요청
			infoMap.put("regrId", userId);
			break;	
			
		}
		
		//권한체크  (사용자구분 C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)    
		if("pscAprv".equals(type) && !"C01002".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"군검사"}));
		}
		
		if("rtnCfrm".equals(type) && !"C01001".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"수사관"}));
		}
		
		
		int chkAprv = 0;
		String pgsStat = "";	
		
		for(int i = 0; i < tpList.size(); i++) {
			
			paramMap.clear();
			paramMap.putAll(infoMap);
			paramMap.put("disuseReqSno", tpList.get(i));
			
			//증거신청목록 진행상태 조회 (진행상태코드 C03001:임시등록, C03002:승인요청, C03003:검사승인, C03004:반려, C03005:문서접수, C03006:과장승인)
			paramMap.put("sqlQueryId", "evdcAplnDAO.selectEvdcPgsStat");
			pgsStat = comDAO.selectCommonQueryString(paramMap); 	
			
			if(type.equals("pscAprv")) {  //검사승인
				if(!"C03002".equals(pgsStat)) {
					throw new MException(egovMessageSource.getMessageArgs("dems.evdc.req.000",new String[]{(String) tpList.get(i),"승인요청"}));
				}	
			}else if(type.equals("rtnCfrm")) { //승인요청
				if(!"C03001".equals(pgsStat)) {
					throw new MException(egovMessageSource.getMessageArgs("dems.evdc.req.000",new String[]{(String) tpList.get(i),"임시등록"}));
				}
			}
					
			
		}
		
		//DEMS_EVDC_DISUSE_REQ 진행상태 변경 
		for(int i = 0; i < tpList.size(); i++) {
			
			paramMap.clear();			
			paramMap.putAll(infoMap);	
			//paramMap.putAll(map);
			paramMap.put("disuseReqSno", tpList.get(i));
			
			
			log.debug("=======##### ModifySelEvdcAnlsList tpList.get["+i+"] #####=========>>>"+tpList.get(i));
			log.debug("=======##### ModifySelEvdcAnlsList 반납확인 paramMap.toString() #####=========>>>"+paramMap.toString());
			
			evdcAplnDAO.modifyEvdcAnlsListAprv(paramMap); //ModifySelEvdcAnlsList
			
		}
		
				

	}	
}

