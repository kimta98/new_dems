package dems.evdc.apln.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("evdcAplnDAO")
@SuppressWarnings({"rawtypes","unchecked"})
public class EvdcAplnDAO extends EgovComAbstractDAO {

	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : regEqpBizRDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  디지털 증거 관리 > 증거 처리 신청
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	public int regEqpBizRDtl(Map<String, Object> paramMap) throws Exception {

    	return (Integer) insert("evdcAplnDAO.regEqpBizRDtl", paramMap);
    }
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updEqpBizUDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:58:51
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 유지보수관리 수정 처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
//	public void updEqpBizUDtl(HashMap<String, String> map) throws Exception {
//		insert("eqpBizDAO.updEqpBizUDtl", map);
//
//	}
	public void updEqpBizUDtl(Map<String, Object> paramMap) throws Exception {
        update("evdcAplnDAO.updEqpBizUDtl", paramMap);
    }

		/**
	    *
	    * <pre>
	    * 1. 메소드명 : selectAnalReqSno
	    * 2. 작성일 : 2021. 4. 21.
	    * 3. 작성자 : ilyong
	    * 4. 설명 :  대여신청일련번호 조회
	    * </pre>
	    * @param map
	    * @param
	    * @param
	    * @return int
	    * @throws Exception
	    */
		public String selectDisuseReqSno(Map map) throws Exception{
			return selectOne("evdcAplnDAO.selectDisuseReqSno", map);
		}
	
		/**
	    *
	    * <pre>
	    * 1. 메소드명 : insertEvdcDisuseRslt
	    * 2. 작성일 : 2021. 4. 30.
	    * 3. 작성자 : ilyong
	    * 4. 설명 :  디지털 증거 관리 > 증거폐기 이관 INSERT
	    * </pre>
	    * @param map
	    * @param
	    * @param
	    * @return int
	    * @throws Exception insertEvdcDisuseReq
	    */
	   public void insertEvdcDisuseRslt(Map<String, Object> paramMap) throws Exception {
	
	   	 	insert("evdcAplnDAO.insertEvdcDisuseRslt", paramMap);
	   }
	   
	   
	   /**
	   *
	   * <pre>
	   * 1. 메소드명 : deleteEvdcDisuseRslt
	   * 2. 작성일 : 2021. 4. 21.
	   * 3. 작성자 : ilyong
	   * 4. 설명 :  디지털 증거 관리 > 증거폐기 이관 DELETE
	   * </pre>
	   * @param map
	   * @param
	   * @param
	   * @return int
	   * @throws Exception
	   */
	  public void deleteEvdcDisuseRslt(Map<String, Object> paramMap) throws Exception {

	  	 	update("evdcAplnDAO.deleteEvdcDisuseRslt", paramMap);
	  }
	  
	  
	  /**
	   *
	   * <pre>
	   * 1. 메소드명 : deleteEvdcDisuseReq
	   * 2. 작성일 : 2021. 4. 21.
	   * 3. 작성자 : ilyong
	   * 4. 설명 :  디지털 증거 관리 > 증거폐기요청 DELETE
	   * </pre>
	   * @param map
	   * @param
	   * @param
	   * @return int
	   * @throws Exception
	   */
	  public void deleteEvdcDisuseReq(Map<String, Object> paramMap) throws Exception {

	  	 	update("evdcAplnDAO.deleteEvdcDisuseReq", paramMap);
	  }
	   
	   /**
	   *
	   * <pre>
	   * 1. 메소드명 : insertEqpLendRequest
	   * 2. 작성일 : 2021. 4. 30.
	   * 3. 작성자 : ilyong
	   * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 요청 등록
	   * </pre>
	   * @param map
	   * @param
	   * @param
	   * @return int
	   * @throws Exception insertEvdcDisuseReq
	   */
	  public void insertEvdcDisuseReq(Map<String, Object> paramMap) throws Exception {
	
	  	 	insert("evdcAplnDAO.insertEvdcDisuseReq", paramMap);
	  }
   
	   /**
	   *
	   * <pre>
	   * 1. 메소드명 : modifyEvdcDisuseReq
	   * 2. 작성일 : 2021. 4. 30.
	   * 3. 작성자 : ilyong
	   * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 요청 정보 수정
	   * </pre>
	   * @param map
	   * @param
	   * @param
	   * @return int
	   * @throws Exception      
	   */
	  public void modifyEvdcDisuseReq(Map<String, Object> paramMap) throws Exception {
	
	  	 	insert("evdcAplnDAO.modifyEvdcDisuseReq", paramMap);
	  }
  
	  /**
	  *
	  * <pre>
	  * 1. 메소드명 : modifyEvdcDisuseReq
	  * 2. 작성일 : 2021. 4. 30.
	  * 3. 작성자 : ilyong
	  * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기 이관 정보 수정
	  * </pre>
	  * @param map
	  * @param
	  * @param
	  * @return int
	  * @throws Exception      
	  */
	 public void modifyEvdcDisuseRslt(Map<String, Object> paramMap) throws Exception {
	
	 	 	insert("evdcAplnDAO.modifyEvdcDisuseRslt", paramMap);
	 }
  
	  /**
	  *
	  * <pre>
	  * 1. 메소드명 : insertEqpLendRequest
	  * 2. 작성일 : 2021. 4. 30.
	  * 3. 작성자 : ilyong
	  * 4. 설명 :  장비 지원 관리 > 장비대여관리 장비대여신청 승인 또는 반납처리
	  * </pre>
	  * @param map
	  * @param
	  * @param
	  * @return int
	  * @throws Exception
	  */
	 public void modifyEqpLendRequestAprv(Map<String, Object> paramMap) throws Exception {
	
	 	 	insert("evdcAplnDAO.modifyEqpLendRequestAprv", paramMap);
	 }
 
	 /**
	 *
	 * <pre>
	 * 1. 메소드명 : insertEqpLendRequest
	 * 2. 작성일 : 2021. 4. 30.
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  장비 지원 관리 > 장비대여관리 장비대여신청 승인 또는 반납처리
	 * </pre>
	 * @param map
	 * @param
	 * @param
	 * @return int
	 * @throws Exception
	 */
	public void modifyEqpLendRequestDtlAprv(Map<String, Object> paramMap) throws Exception {
	
		 	insert("evdcAplnDAO.modifyEqpLendRequestDtlAprv", paramMap);
	}
   
	   /**
	   *
	   * <pre>
	   * 1. 메소드명 : insertEqpLendRequestDtl
	   * 2. 작성일 : 2021. 4. 30.
	   * 3. 작성자 : ilyong
	   * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여신청 장비목록 등록
	   * </pre>
	   * @param map
	   * @param
	   * @param
	   * @return int
	   * @throws Exception
	   */
	  public void insertEqpLendRequestDtl(Map<String, Object> paramMap) throws Exception {
	
	  	 	insert("evdcAplnDAO.insertEqpLendRequestDtl", paramMap);
	  }
   

   
   /**
   *
   * <pre>
   * 1. 메소드명 : deleteEqpMntRDtl
   * 2. 작성일 : 2021. 4. 21.
   * 3. 작성자 : ilyong
   * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여신청 삭제
   * </pre>
   * @param map
   * @param
   * @param
   * @return int
   * @throws Exception
   */
  public void deleteEqpLendRequest(Map<String, Object> paramMap) throws Exception {

  	 	insert("evdcAplnDAO.deleteEqpLendRequest", paramMap);
  }
  
  /**
  *
  * <pre>
  * 1. 메소드명 : deleteEqpMntRDtl
  * 2. 작성일 : 2021. 4. 21.
  * 3. 작성자 : ilyong
  * 4. 설명 :  장비 지원 관리 > 장비대여관리 대여장비목록 삭제
  * </pre>
  * @param map
  * @param
  * @param
  * @return int
  * @throws Exception
  */
 public void deleteEqpLendRequestDtl(Map<String, Object> paramMap) throws Exception {

 	 	insert("evdcAplnDAO.deleteEqpLendRequestDtl", paramMap);
 }
	
	/**
    *
    * <pre>
    * 1. 메소드명 : insertAnalysisEvdcDtl
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : ilyong
    * 4. 설명 :  장비 지원 관리 > 유지보수관리 등록/수정
    * </pre>
    * @param map
    * @param
    * @param
    * @return int
    * @throws Exception
    */
   public void mergeEqpMntRDtl(Map<String, Object> paramMap) throws Exception {

   	 	insert("evdcAplnDAO.mergeEqpMntRDtl", paramMap);
   }
   
   /**
	 *
	 * <pre>
	 * 1. 메소드명 : insertEqpLendRequest
	 * 2. 작성일 : 2021. 4. 30.
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 선택 승인요청, 검사승인
	 * </pre>
	 * @param map
	 * @param
	 * @param
	 * @return int
	 * @throws Exception
	 */
	public void modifyEvdcAnlsListAprv(Map<String, Object> paramMap) throws Exception {
	
		 	insert("evdcAplnDAO.modifyEvdcAnlsListAprv", paramMap);
	}
	
}

