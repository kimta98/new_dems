package dems.evdc.trt.service;

import java.util.HashMap;
import java.util.Map;

import frame.flyt.login.service.FLytLoginVO;



/**
 * 
 * <pre>
 * 1. 클래스명 : EvdcTrtService.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 디지털 증거 관리 > 증거 처리 관리
 * </pre>
 */
@SuppressWarnings("rawtypes")
public interface EvdcTrtService {


	/**
	 * 
	 * <pre>
     * 1. 메소드명 : ModifySelEvdcTrtsList
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  디지털 증거 관리 > 증거처리관리 선택 접수, 승인
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void modifySelEvdcTrtsList(Map<String,Object> map) throws Exception;
	
	/**
	 * 
	 * <pre>
     * 1. 메소드명 : modifyEvdcTrtUDtl
     * 2. 작성일 : 2021. 4. 30.
     * 3. 작성자 : ilyong
     * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 접수, 승인처리
	 * </pre>
	 * @param map
	 * @throws Exception
	 */
	void modifyEvdcTrtUDtl(Map<String,Object> map) throws Exception;
}

