package dems.evdc.trt.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import dems.bbs.free.service.impl.BbsFreeServiceImpl;
import dems.evdc.trt.service.EvdcTrtService;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;


/**
 * 
 * <pre>
 * 1. 클래스명 : EvdcTrtServiceImpl.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 디지털 증거 관리 > 증거 처리 신청
 * </pre>
 */
@Service("evdcTrtService")
@SuppressWarnings({"rawtypes","unchecked","unused"})
public class EvdcTrtServiceImpl extends EgovAbstractServiceImpl implements	EvdcTrtService{
	
	protected Logger log = LoggerFactory.getLogger(EvdcTrtServiceImpl.class);
	
	@Resource(name="evdcTrtDAO")
	private EvdcTrtDAO evdcTrtDAO;
	
	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="comDAO")
	private ComDAO comDAO;
	
	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
	
	
	/**
    *
    * <pre>
    * 1. 메소드명 : ModifySelEvdcTrtsList
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  디지털 증거 관리 > 증거처리관리 선택 접수, 승인
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void modifySelEvdcTrtsList(Map<String, Object> map) throws Exception {

		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		String type = (String) map.get("type");
		List tpList = (List) map.get("tpList");
		String userGb = (String) map.get("userGb");
		String insttCd = (String) map.get("insttCd");
		String prosrId = (String) map.get("prosrId");
		String userId = (String) map.get("userId");
		String currentDay = EgovDateUtil.getToday();
		String rcvNo = "";
		
		log.debug("=======##### ModifySelEvdcAnlsList currentDay #####=========>>>"+currentDay);
		
		Map<String, Object> infoMap = new HashMap<String, Object>();
		
		/*
		rcvDt		접수일시
		rcvNo		접수번호
		rcvrId		접수자ID
		updDt		수정일자
		updrId		수정자ID
		pgsStat
		*/
		
		
		
		switch (type) {
		case "frsRcv":     //접수
			infoMap.put("saveGb", "frsRcv");
			infoMap.put("rcvDt", currentDay);
			infoMap.put("rcvrId", userId);
			infoMap.put("pgsStat", "C03005"); 
			infoMap.put("regrId", userId);
			break;
		case "frsAprv":     //승인
			infoMap.put("saveGb", "frsAprv");
			infoMap.put("aprvDt", currentDay);
			infoMap.put("aprvrId", userId);
			infoMap.put("pgsStat", "C03006");   
			infoMap.put("regrId", userId);
			break;	
			
		}
		
		//권한체크  (사용자구분 C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)    
		if("frsRcv".equals(type) && !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식수사관"}));
		}
		
		if("frsAprv".equals(type) && !"C01004".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식수사과장"}));
		}
		
		
		int chkAprv = 0;
		String pgsStat = "";	
		
		for(int i = 0; i < tpList.size(); i++) {
			//폐기요청일련번호 조회
			//rcvNo = evdcAplnDAO.selectDisuseReqSno(paramMap);
			
			/*
			rcvDt		접수일시
			rcvNo		접수번호
			rcvrId		접수자ID
			updDt		수정일자
			updrId		수정자ID
			*/
			
			//DEMS_ANALYSIS_EVDC_DTL 리스트 가져오기			
//			paramMap.clear();
//			paramMap.put("analReqSno", aprvList.get(i));
//			paramMap.put("sqlQueryId", "anlsPgsDAO.selectAnalysisEvdcDtlList");
//			
//			
// 			List AnalysisEvdcDtlList = comDAO.selectCommonQueryList(paramMap);  
			
			
			paramMap.clear();
			paramMap.putAll(infoMap);
			paramMap.put("disuseReqSno", tpList.get(i));
			
			//증거신청목록 진행상태 조회 (진행상태코드 C03001:임시등록, C03002:승인요청, C03003:검사승인, C03004:반려, C03005:문서접수, C03006:과장승인)
			paramMap.put("sqlQueryId", "evdcAplnDAO.selectEvdcPgsStat");
			pgsStat = comDAO.selectCommonQueryString(paramMap); 	
			
			if(type.equals("frsRcv")) {  //접수
				if(!"C03003".equals(pgsStat)) {
					throw new MException(egovMessageSource.getMessageArgs("dems.evdc.req.000",new String[]{(String) tpList.get(i),"검사승인"}));
				}	
			}else if(type.equals("frsAprv")) { //승인
				if(!"C03005".equals(pgsStat)) {
					throw new MException(egovMessageSource.getMessageArgs("dems.evdc.req.000",new String[]{(String) tpList.get(i),"접수"}));
				}
			}
					
			
		}
		
		//DEMS_EVDC_DISUSE_REQ 진행상태 변경 
		for(int i = 0; i < tpList.size(); i++) {
			//접수번호 조회
			rcvNo = evdcTrtDAO.selectDisuseRcvNo(paramMap);
			
			paramMap.clear();	
			paramMap.put("rcvNo",rcvNo);
			paramMap.putAll(infoMap);	
			//paramMap.putAll(map);
			paramMap.put("disuseReqSno", tpList.get(i));
			
			log.debug("=======##### ModifySelEvdcAnlsList tpList.get["+i+"] #####=========>>>"+tpList.get(i));
			log.debug("=======##### ModifySelEvdcAnlsList 반납확인 paramMap.toString() #####=========>>>"+paramMap.toString());
			
			//증거처리관리 선택 접수, 승인
			evdcTrtDAO.modifyEvdcTrtListAprv(paramMap); //ModifySelEvdcAnlsList
			
			
			if(type.equals("frsRcv")) { //접수
				//증거폐기요청 정보(요청구분, 이송요청기관) 조회			
				paramMap.clear();
				paramMap.put("disuseReqSno", tpList.get(i));
				paramMap.put("sqlQueryId", "evdcTrtDAO.selectEvdcDisuseReqList");

				List EvdcDisuseReqList = comDAO.selectCommonQueryList(paramMap);
				Map<String, Object> dtlMap = (Map<String, Object>) EvdcDisuseReqList.get(0);
				
				paramMap.clear();
				paramMap.put("disuseReqSno", tpList.get(i));
				paramMap.put("disuseDt", currentDay);
				paramMap.put("disuseMthd", dtlMap.get("reqDiv"));
				paramMap.put("transInstt", dtlMap.get("reqTransInstt"));
				
				log.debug("=======##### ModifySelEvdcTrtsList DEMS_EVDC_DISUSE_RSLT 접수일 경우 paramMap.toString() #####=========>>>"+paramMap.toString());
				
				//증거폐기 이관 정보 수정
				evdcTrtDAO.modifyEvdcTrtRslt(paramMap); 
			}
			
			
		}
		
				

	}
	
	/**
    *
    * <pre>
    * 1. 메소드명 : modifyEvdcTrtUDtl
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 접수, 승인처리
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void modifyEvdcTrtUDtl(Map<String, Object> map) throws Exception {

		Map<String,Object> paramMap = new HashMap<String,Object>();
		
		Map<String, Object> infoMap = new HashMap<String, Object>();
		Map<String,Object> infoMap2 = new HashMap<String,Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		//validateUtil.check(chkParamMap, requireParams);

		String userGb = (String) map.get("userGb"); //userId
		String userId = (String) map.get("userId");
		String currentDay = EgovDateUtil.getToday();
		String modFlag = ""; //
		String saveGb = ""; //
		
		String disuseReqSno = ""; //폐기요청일련번호
		String disuseDt = ""; //폐기일/이송일
		String disuseMthd = ""; //폐기방법
		String transInstt = ""; //이송기관
		String rcvrId = "";	//접수자ID
		String rcvDt = "";	//접수일시
		String rcvNo = "";	//접수번호  
		String regrId = "";	//등록자ID 
		String aprvrId = "";	//승인자ID     
		String aprvDt = "";	//승인일자   
		String useYn = "";	//사용여부
		String pgsStat = ""; //진행상태
		
		
		
		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap1"));
		//paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap2"));
		
//		if(map.get("formDataMap3") != null && map.get("formDataMap3") !="") {
//			paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap3"));
//		}
		
		//DEMS_EVDC_DISUSE_RSLT list
		//ArrayList<Map<String, Object>> paramDetailList = (ArrayList<Map<String, Object>>) map.get("detailList");
		
		modFlag = (String)paramMap.get("modFlag");
		saveGb = (String)paramMap.get("saveGb");
		disuseReqSno = (String)paramMap.get("disuseReqSno");
		
		disuseDt = (String)paramMap.get("disuseDt");
		disuseMthd = (String)paramMap.get("disuseMthd");
		transInstt = (String)paramMap.get("transInstt");
		
		rcvrId = (String)paramMap.get("rcvrId");
		rcvDt = EgovDateUtil.getToday();
		regrId = userId;
		aprvrId = userId;
		aprvDt = EgovDateUtil.getToday();
		
		
		log.debug("==============modifyEvdcTrtUDtl 	userGb===========>>>>>>" + userGb);
		log.debug("==============modifyEvdcTrtUDtl 	userId===========>>>>>>" + userId);
		log.debug("==============modifyEvdcTrtUDtl 	modFlag===========>>>>>>" + modFlag);
		log.debug("==============modifyEvdcTrtUDtl 	saveGb===========>>>>>>" + saveGb);
		log.debug("==============modifyEvdcTrtUDtl 	currentDay===========>>>>>>" + currentDay);
		
		log.debug("==============modifyEvdcTrtUDtl 	disuseReqSno===========>>>>>>" + disuseReqSno);
		log.debug("==============modifyEvdcTrtUDtl 	disuseDt===========>>>>>>" + disuseDt);
		log.debug("==============modifyEvdcTrtUDtl 	disuseMthd===========>>>>>>" + disuseMthd);
		log.debug("==============modifyEvdcTrtUDtl 	transInstt===========>>>>>>" + transInstt);
		log.debug("==============modifyEvdcTrtUDtl 	rcvrId===========>>>>>>" + rcvrId);
		
		log.debug("==============modifyEvdcTrtUDtl 	rcvDt===========>>>>>>" + rcvDt);
		log.debug("==============modifyEvdcTrtUDtl 	rcvNo===========>>>>>>" + rcvNo);
		log.debug("==============modifyEvdcTrtUDtl 	regrId===========>>>>>>" + regrId);
		log.debug("==============modifyEvdcTrtUDtl 	aprvrId===========>>>>>>" + aprvrId);
		log.debug("==============modifyEvdcTrtUDtl 	aprvDt===========>>>>>>" + aprvDt);
		
		log.debug("==============modifyEvdcTrtUDtl 	useYn===========>>>>>>" + useYn);
		
		log.debug("==============modifyEvdcTrtUDtl 	1111111111111111111111111111111111111111===========>>>>>>" + useYn);
		switch (saveGb) {
			case "frsRcv":     //접수
				//접수번호 조회
				rcvNo = evdcTrtDAO.selectDisuseRcvNo(paramMap);
				
				infoMap.put("saveGb", saveGb);
				infoMap.put("rcvDt", currentDay);
				infoMap.put("rcvrId", userId);
				infoMap.put("pgsStat", "C03005");
				infoMap.put("rcvNo", rcvNo); 
				infoMap.put("disuseDt", disuseDt);
				infoMap.put("disuseMthd", disuseMthd);
				infoMap.put("transInstt", transInstt);
				infoMap.put("regrId", userId);
				infoMap.put("disuseReqSno", disuseReqSno);
			break;
			case "frsRcvCnc":     //접수취소
				
				infoMap.put("saveGb", saveGb);
				infoMap.put("pgsStat", "C03003");
				infoMap.put("regrId", userId);
				infoMap.put("disuseReqSno", disuseReqSno);
				break;	
			case "frsAprv":     //승인
				infoMap.put("saveGb", saveGb);
				infoMap.put("pgsStat", "C03006");  
				infoMap.put("aprvrId", userId);
				infoMap.put("aprvDt", currentDay);
				infoMap.put("regrId", userId);
				infoMap.put("disuseReqSno", disuseReqSno);
			break;	
			case "frsAprvCnc":     //승인취소
				infoMap.put("saveGb", saveGb);
				infoMap.put("pgsStat", "C03005");   
				infoMap.put("regrId", userId);
				infoMap.put("disuseReqSno", disuseReqSno);
			break;		
		}	
		log.debug("==============modifyEvdcTrtUDtl 	22222222222222222222222222222222222222222222===========>>>>>>" + useYn);	
		//권한체크  (사용자구분 C01001:수사관, C01002:군검사, C01003:포렌식수사관, C01004:수사과장, C01999:관리자)    
		if(("frsRcv".equals(saveGb) && !"C01003".equals(userGb)) || ("frsRcvCnc".equals(saveGb) && !"C01003".equals(userGb))) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식수사관"}));
		}
		
		if(("frsAprv".equals(saveGb) && !"C01004".equals(userGb)) || ("frsAprvCnc".equals(saveGb) && !"C01004".equals(userGb))) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식수사과장"}));
		}
		log.debug("==============modifyEvdcTrtUDtl 	33333333333333333333333333333333333333333===========>>>>>>" + useYn);
		
		infoMap2.clear();
		infoMap2.put("disuseReqSno", disuseReqSno);
		
		//증거신청목록 진행상태 조회 (진행상태코드 C03001:임시등록, C03002:승인요청, C03003:검사승인, C03004:반려, C03005:문서접수, C03006:과장승인)
		infoMap2.put("sqlQueryId", "evdcAplnDAO.selectEvdcPgsStat");
		pgsStat = comDAO.selectCommonQueryString(infoMap2); 	
		
		if(saveGb.equals("frsRcv")) {  //접수
			if(!"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.evdc.req.000",new String[]{disuseReqSno,"검사승인"}));
			}	
		}else if(saveGb.equals("frsRcvCnc")) { //접수취소
			if(!"C03005".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.evdc.req.000",new String[]{disuseReqSno,"접수"}));
			}
		}else if(saveGb.equals("frsAprv")) { //승인
			if(!"C03005".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.evdc.req.000",new String[]{disuseReqSno,"접수"}));
			}
		}else if(saveGb.equals("frsAprv")) { //승인취소
			if(!"C03006".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.evdc.req.000",new String[]{disuseReqSno,"승인"}));
			}
		}
		
		
		infoMap2.clear();
		infoMap2.putAll(infoMap);
		log.debug("=======##### modifyEvdcTrtUDtl 접수, 접수취소, 승인, 승인취소 paramMap.toString() #####=========>>>"+infoMap2.toString());
		
		//증거처리관리 접수, 접수취소, 승인, 승인취소 처리
		evdcTrtDAO.modifyEvdcTrtListAprv(infoMap2); //ModifySelEvdcAnlsList
		
		if(saveGb.equals("frsRcv")) { //접수
			//증거폐기요청 정보(요청구분, 이송요청기관) 조회			
			infoMap2.clear();
			infoMap2.putAll(infoMap);
			
			log.debug("=======##### modifyEvdcTrtUDtl DEMS_EVDC_DISUSE_RSLT 접수일 경우 paramMap.toString() #####=========>>>"+infoMap2.toString());
			
			//증거폐기 이관 정보 수정
			evdcTrtDAO.modifyEvdcTrtRslt(paramMap); 
		}else if(saveGb.equals("frsRcvCnc")) { //접수 취소
			//증거폐기요청 정보(요청구분, 이송요청기관) 조회			
			infoMap2.clear();
			infoMap2.put("disuseReqSno", disuseReqSno);
			infoMap2.put("sqlQueryId", "evdcTrtDAO.selectEvdcDisuseReqList");

			List EvdcDisuseReqList = comDAO.selectCommonQueryList(infoMap2);
			Map<String, Object> dtlMap = (Map<String, Object>) EvdcDisuseReqList.get(0);
			
			infoMap2.clear();
			infoMap2.put("disuseReqSno", disuseReqSno);
			infoMap2.put("disuse_dt", "");
			infoMap2.put("disuseMthd", dtlMap.get("reqDiv"));
			infoMap2.put("transInstt", dtlMap.get("reqTransInstt"));
			
			log.debug("=======##### modifyEvdcTrtUDtl DEMS_EVDC_DISUSE_RSLT 접수취소일 경우 paramMap.toString() #####=========>>>"+infoMap2.toString());
			
			//증거폐기 이관 정보 수정
			evdcTrtDAO.modifyEvdcTrtRslt(infoMap2); 
		}
		
	}	
}

