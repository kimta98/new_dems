package dems.evdc.trt.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("evdcTrtDAO")
@SuppressWarnings({"rawtypes","unchecked"})
public class EvdcTrtDAO extends EgovComAbstractDAO {

	
   /**
	 *
	 * <pre>
	 * 1. 메소드명 : modifyEvdcTrtListAprv
	 * 2. 작성일 : 2021. 4. 30.
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  디지털 증거 관리 > 증거처리관리 선택 접수, 승인
	 * </pre>
	 * @param map
	 * @param
	 * @param
	 * @return int
	 * @throws Exception
	 */
	public void modifyEvdcTrtListAprv(Map<String, Object> paramMap) throws Exception {
	
		 	update("evdcTrtDAO.modifyEvdcTrtListAprv", paramMap);
	}
	
	/**
    *
    * <pre>
    * 1. 메소드명 : selectDisuseRcvNo
    * 2. 작성일 : 2021. 4. 21.
    * 3. 작성자 : ilyong
    * 4. 설명 :  증거폐기요청 정보(요청구분, 이송요청기관) 조회
    * </pre>
    * @param map
    * @param
    * @param
    * @return int
    * @throws Exception
    */
	public String selectDisuseRcvNo(Map map) throws Exception{
		return selectOne("evdcTrtDAO.selectDisuseRcvNo", map);
	}
	
	/**
	 *
	 * <pre>
	 * 1. 메소드명 : modifyEvdcTrtRslt
	 * 2. 작성일 : 2021. 4. 30.
	 * 3. 작성자 : ilyong
	 * 4. 설명 :  디지털 증거 관리 > 증거폐기 이관 정보 수정
	 * </pre>
	 * @param map
	 * @param
	 * @param
	 * @return int
	 * @throws Exception
	 */
	public void modifyEvdcTrtRslt(Map<String, Object> paramMap) throws Exception {
	
		 	update("evdcTrtDAO.modifyEvdcTrtRslt", paramMap);
	}
}

