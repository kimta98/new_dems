package dems.evdc.trt.web;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovDateUtil;

import dems.evdc.trt.service.EvdcTrtService;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.fsys.user.service.FsysUserVO;
import frame.futil.DemsConst;
import frame.futil.DemsUtil;
import frame.futil.PageUtil;


/**
 * 
 * <pre>
 * 1. 클래스명 : EvdcTrtController.java
 * 2. 작성일 : 2021. 4. 16.
 * 3. 작성자 : ilyong
 * 4. 설명 : 디지털 증거 관리 > 증거 처리 관리
 * </pre>
 */
@Controller
public class EvdcTrtController {

	protected Logger log = LoggerFactory.getLogger(EvdcTrtController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;
	
	@Resource(name = "evdcTrtService")
	private EvdcTrtService evdcTrtService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;

    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexEvdcAplnMList
     * 2. 작성일 : 2021. 4. 16. 오전 10:49:11
     * 3. 작성자 : ilyong
     * 4. 설명 : 디지털 증거 관리 > 증거 처리 관리 조회
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/evdc/trt/indexEvdcTrtMList.do")
    public String indexEvdcTrtMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {
    	
    	model.addAttribute("message", request.getParameter("message"));
    	if(request.getParameter("msg") != null){
    		model.addAttribute("msg", request.getParameter("msg"));
    	}
    	return "dems/evdc/trt/evdcTrtMList";
    }


	/**
     * 
     * <pre>
     * 1. 메소드명 : queryEvdcTrtMList
     * 2. 작성일 : 2021. 4. 14. 오전 10:49:36
     * 3. 작성자 : ilyong
     * 4. 설명 : 디지털 증거 관리 > 증거 처리 관리 조회 처리
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/evdc/trt/queryEvdcTrtMList.do")
    public ModelAndView queryEvdcTrtMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
    	//sessionInsttCd
    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	log.debug("=======queryEvdcAplnMList schPgsStat  split========>>>"+ ((String) reqParams.get("schPgsStat")).split(","));
    	reqParams.put("sessionUserGb", user.getUserGb());
    	reqParams.put("sessionInsttCd", user.getInsttCd());
    	reqParams.put("schPgsStat", ((String) reqParams.get("schPgsStat")).split(","));
    	
    	
    	log.debug("=======queryEvdcAplnMList sessionUserGb========>>>"+user.getUserGb());
    	log.debug("=======queryEvdcAplnMList sessionInsttCd========>>>"+user.getInsttCd());
    	
    	reqParams.put("sqlQueryId", "evdcAplnDAO.queryEvdcAplnMListTotCnt"); 
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	
    	PageUtil.calcPage(reqParams);
    	
//    	log.debug("=======reqParams searchNm========>>>"+reqParams.get("searchNm"));
//    	log.debug("=======reqParams searchInstt========>>>"+reqParams.get("searchInstt"));
    	
		reqParams.put("sqlQueryId", "evdcAplnDAO.queryEvdcAplnMList");
		List list = comService.selectCommonQueryList(reqParams);
		
//		log.debug("=========queryEvdcTrtMList list.size() ========>>>"+list.size());
//		
//		for(int i=0;i<list.size();i++) {
//			log.debug("=========queryEvdcTrtMList list["+i+"]=========>>>"+list.get(i));
//		}
		
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

    }
    
  
    /**
    *
    * <pre>
    * 1. 메소드명 : ModifySelEvdcTrtsList
    * 2. 작성일 : 2021. 4. 30.
    * 3. 작성자 : ilyong
    * 4. 설명 :  디지털 증거 관리 > 증거처리관리 선택 접수, 승인
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception 
    */
   @RequestMapping(value="/evdc/trt/modifySelEvdcTrtsList.do")
   public ModelAndView modifySelEvdcTrtsList(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

 	   FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
 	   
 	   ObjectMapper mapper = new ObjectMapper();
 	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
 	
 	   //요청 리스트
 	   List tpList = (List) jsonObject.get("rowDatas");
 	   
 	   //요청 타입
 	   Map<String, Object> infoMap = jsonObject.get("type");
 	   String type = (String) infoMap.get("type");
 	
 	   Map<String, Object> paramMap = new HashMap<String, Object>();
 	   paramMap.put("tpList", tpList);
 	   paramMap.put("userGb", user.getUserGb());
 	   paramMap.put("userId", user.getUserId());
 	   paramMap.put("insttCd", user.getInsttCd());
 	   paramMap.put("type", type);
 	
 	  evdcTrtService.modifySelEvdcTrtsList(paramMap);
 	   
 	   return new ModelAndView(ajaxMainView, model);

   }
   
   /**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexEvdcTrtVDtl
	 * 2. 작성일 : 2021. 4. 16. 오전 9:48:15
	 * 3. 작성자 : ilyong
	 * 4. 설명 : 디지털 증거 관리 > 디지털 증거 폐기/이송 접수화면(포레식수사관)
	 * </pre>
	 * @param map
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/evdc/trt/indexEvdcTrtVDtl.do")   
	public String indexEvdcTrtVDtl(@RequestParam HashMap<String,String> map,HttpServletRequest request, ModelMap model) throws Exception {
	
		return "dems/evdc/trt/evdcTrtMListPopVDtl";
		
	}
	
	/**
	  *
	  * <pre>
	  * 1. 메소드명 : modifyEvdcTrtUDtl
	  * 2. 작성일 : 2021. 4. 30.
	  * 3. 작성자 : ilyong
	  * 4. 설명 :  디지털 증거 관리 > 디지털 증거 폐기/이송 접수, 승인처리
	  * </pre>
	  * @param map
	  * @param model
	  * @param request
	  * @return
	  * @throws Exception
	  */
	 @RequestMapping(value="/evdc/trt/modifyEvdcTrtUDtl.do")
	 public ModelAndView modifyEvdcTrtUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

		   	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		   	ObjectMapper mapper = new ObjectMapper();
		   	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
		
		   	//장비대여 대여장비정보
		   	Map<String, Object> formDataMap1 = (Map<String, Object>) jsonObject.get("formDatas1");
		   	
			//Map<String, Object> formDataMap2 = (Map<String, Object>) jsonObject.get("formDatas2");
			
			//장비대여 신청정보
		   	//ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
		   	
		   	
		   	log.debug("======CON regEqpMntRDtl formDataMap1========>>>"+formDataMap1.size());
		   	//log.debug("======CON regEqpMntRDtl formDataMap2========>>>"+formDataMap2.size());
		   	
		   	
//		   	log.debug("======CON regEqpMntRDtl tpList========>>>"+tpList.size());
		   	
		   	Map<String, Object> paramMap = new HashMap<String, Object>();
		   	paramMap.put("formDataMap1", formDataMap1);
		   	//paramMap.put("formDataMap2", formDataMap2);
//		   	paramMap.put("formDataMap3", formDataMap3);
		   	//paramMap.put("detailList", tpList);
		   	paramMap.put("userGb", user.getUserGb());
		   	paramMap.put("userId", user.getUserId());
		
		   	evdcTrtService.modifyEvdcTrtUDtl(paramMap);

		   	return new ModelAndView(ajaxMainView, model);

	 }	  
}
