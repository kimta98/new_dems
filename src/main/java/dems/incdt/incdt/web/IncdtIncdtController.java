package dems.incdt.incdt.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.fexception.MException;
import frame.futil.DemsConst;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;

/**
 * 
 * <pre>
 * 1. 클래스명 : IncdtIncdtController.java
 * 2. 작성일 : 2021. 4. 21.
 * 3. 작성자 : jij
 * 4. 설명 : @!@ 사건관리 담당 
 * </pre>
 */
@Controller
public class IncdtIncdtController {

	protected Logger log = LoggerFactory.getLogger(IncdtIncdtController.class);
	
	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource MappingJackson2JsonView ajaxMainView;	
	
	 /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;
    
    /*@Resource(name="comController")
    ComController comController;    */

    /**
     * 
     * <pre>
     * 1. 메소드명 : indexIncdtIncdtMList
     * 2. 작성일 : 2021. 4. 20. 오후 2:34:43
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 사건 관리 리스트 조회 화면 이동
     * </pre>
     * @param request
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/incdt/incdt/indexIncdtIncdtMList.do")
    public String indexIncdtIncdtMList(HttpServletRequest request, ModelMap model, SessionStatus status) throws Exception {

    	return "dems/incdt/incdt/incdtIncdtMList";
    	
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryIncdtIncdtMList
     * 2. 작성일 : 2021. 4. 21. 오후 12:12:11
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 사건 관리 리스트 조회
     * </pre>
     * @param model
     * @param request
     * @param reqParams
     * @return
     * @throws Exception
     */
    @RequestMapping("/incdt/incdt/queryIncdtIncdtMList.do")
	public ModelAndView queryIncdtIncdtMList(ModelMap model,
			HttpServletRequest request, @RequestParam HashMap reqParams) throws Exception {

		reqParams.put("sqlQueryId", "incdtIncdtDAO.queryIncdtIncdtMListTotCnt");
		int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
 		reqParams.put("totalCount", totCnt);
 		
 		PageUtil.calcPage(reqParams);
		 
		reqParams.put("sqlQueryId", "incdtIncdtDAO.queryIncdtIncdtMList");

		List list = comService.selectCommonQueryList(reqParams);

		reqParams.put("list", list);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);

	}
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexIncdtIncdtRDtl
     * 2. 작성일 : 2021. 4. 20. 오후 6:21:11
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 사건 관리 등록 화면 이동 
     * </pre>
     * @param request
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/incdt/incdt/indexIncdtIncdtRDtl.do")
    public String indexIncdtIncdtRDtl(HttpServletRequest request, ModelMap model, SessionStatus status, @RequestParam HashMap<String, String> map) throws Exception {

    	return "dems/incdt/incdt/incdtIncdtRDtl";
    	
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryIncdtRegUserMList
     * 2. 작성일 : 2021. 4. 22. 오후 2:39:55
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 담당자 리스트 조회
     * </pre>
     * @param model
     * @param request
     * @param reqParams
     * @return
     * @throws Exception
     */
    @RequestMapping("/incdt/incdt/queryIncdtRegUserMList.do")
	public ModelAndView queryIncdtRegUserMList(ModelMap model,
			HttpServletRequest request, @RequestParam HashMap reqParams) throws Exception {

		reqParams.put("sqlQueryId", "incdtIncdtDAO.queryIncdtRegUserMListTotCnt");
		int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
 		reqParams.put("totalCount", totCnt);
 		
 		PageUtil.calcPage(reqParams);
		 
		reqParams.put("sqlQueryId", "incdtIncdtDAO.queryIncdtRegUserMList");

		List list = comService.selectCommonQueryList(reqParams);

		reqParams.put("list", list);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);

	}
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : regIncdtIncdtRDtl
     * 2. 작성일 : 2021. 4. 20. 오후 7:24:24
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 사건 관리 등록
     * </pre>
     * @param request
     * @param reqParams
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping("/incdt/incdt/regIncdtIncdtRDtl.do")
    public ModelAndView regIncdtIncdtRDtl(HttpServletRequest request, @RequestParam HashMap reqParams, ModelMap model,  SessionStatus status) throws Exception {

    	String insttCd = (String) reqParams.get("session_insttcd");
    	String regInsttCd = (String) reqParams.get("regInsttCd");
    	String session_usergb = (String) reqParams.get("session_usergb");
    	
    	if(!session_usergb.equals("C01001") && !session_usergb.equals("C01003")) {
    		throw new MException(egovMessageSource.getMessageArgs("dems.site.rcv.008", null));
    	}
    	
    	if(!insttCd.equals(regInsttCd)) {
    		throw new MException(egovMessageSource.getMessageArgs("dems.site.rcv.008", null));
    	}
    	
    	HashMap<String,String> requireParams = new HashMap();
    	requireParams.put("incdtNo", "사건번호");
    	requireParams.put("incdtNm", "사건명");
    	requireParams.put("crimeCd", "죄명");
    	requireParams.put("regInsttCd", "담당부서");
    	requireParams.put("regUserId", "담당자");
    	requireParams.put("aprvrId", "주임검사(승인자)");
    	validateUtil.check(reqParams, requireParams);
    	
    	reqParams.put("sqlQueryId", "incdtIncdtDAO.regIncdtIncdtRDtl");
    	comService.updateCommonQuery(reqParams);

    	model.addAttribute(DemsConst.Messages_SysSucMessage,
    			egovMessageSource.getMessageArgs("success.common.insert", null));

    	return new ModelAndView(ajaxMainView, model);

    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexIncdtIncdtUDtl
     * 2. 작성일 : 2021. 4. 21. 오전 10:58:51
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 사건 관리 수정 화면 이동 
     * </pre>
     * @param request
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/incdt/incdt/indexIncdtIncdtUDtl.do")
    public String indexIncdtIncdtUDtl(HttpServletRequest request, ModelMap model, SessionStatus status) throws Exception {

    	return "dems/incdt/incdt/incdtIncdtUDtl";
    	
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryIncdtIncdtMDtl
     * 2. 작성일 : 2021. 4. 21. 오전 11:18:15
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 사건 상세 조회 
     * </pre>
     * @param map
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/incdt/incdt/queryIncdtIncdtMDtl.do")
    public ModelAndView queryIncdtIncdtMDtl(@RequestParam HashMap<String, String> map, HttpServletRequest request,
    		ModelMap model) throws Exception {

    	map.put("sqlQueryId", "incdtIncdtDAO.queryIncdtIncdtMDtl");
    	Map rtnMap = comService.selectCommonQueryMap(map);
    	model.addAttribute("rtnMap", rtnMap);

    	return new ModelAndView(ajaxMainView, model);

    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryIncdtUsingChkCnt
     * 2. 작성일 : 2021. 6. 8. 오후 2:48:47
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 사용중인 사건 건수 조회
     * </pre>
     * @param map
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/incdt/incdt/queryIncdtUsingChkCnt.do")
    public ModelAndView queryIncdtUsingChkCnt(@RequestParam HashMap<String, String> map, HttpServletRequest request,
    		ModelMap model) throws Exception {

    	map.put("sqlQueryId", "incdtIncdtDAO.queryIncdtUsingChkCnt");
    	int chkCnt =  comService.selectCommonQueryListTotCnt(map);
    	
    	model.addAttribute("chkCnt", chkCnt);

    	return new ModelAndView(ajaxMainView, model);

    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : updIncdtIncdtUDtl
     * 2. 작성일 : 2021. 4. 21. 오전 10:59:12
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 사건 관리 수정 
     * </pre>
     * @param request
     * @param reqParams
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping("/incdt/incdt/updIncdtIncdtUDtl.do")
    public ModelAndView updIncdtIncdtUDtl(HttpServletRequest request, @RequestParam HashMap reqParams, ModelMap model,  SessionStatus status) throws Exception {

    	String insttCd = (String) reqParams.get("session_insttcd");
    	String regInsttCd = (String) reqParams.get("regInsttCd");
    	String session_usergb = (String) reqParams.get("session_usergb");
    	
    	if(!session_usergb.equals("C01001") && !session_usergb.equals("C01003")) {
    		throw new MException(egovMessageSource.getMessageArgs("dems.site.rcv.008", null));
    	}
    	
    	if(!insttCd.equals(regInsttCd)) {
    		throw new MException(egovMessageSource.getMessageArgs("dems.site.rcv.008", null));
    	}
    	
    	HashMap<String,String> requireParams = new HashMap();
    	requireParams.put("incdtNo", "사건번호");
    	requireParams.put("incdtNm", "사건명");
    	requireParams.put("crimeCd", "죄명");
    	requireParams.put("regInsttCd", "담당부서");
    	requireParams.put("regUserId", "담당자");
    	requireParams.put("aprvrId", "주임검사(승인자)");
    	validateUtil.check(reqParams, requireParams);
    	
    	reqParams.put("sqlQueryId", "incdtIncdtDAO.updIncdtIncdtUDtl");
    	comService.updateCommonQuery(reqParams);

    	model.addAttribute(DemsConst.Messages_SysSucMessage,
    			egovMessageSource.getMessageArgs("success.common.save", null));

    	return new ModelAndView(ajaxMainView, model);

    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : delIncdtIncdtUDtl
     * 2. 작성일 : 2021. 4. 21. 오전 11:17:02
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 사건 관리 삭제
     * </pre>
     * @param map
     * @param model
     * @param request
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping("/incdt/incdt/delIncdtIncdtUDtl.do")
    public ModelAndView delIncdtIncdtUDtl(@RequestParam HashMap<String, String> map, ModelMap model,
    		HttpServletRequest request, SessionStatus status) throws Exception {

    	String insttCd = (String) map.get("session_insttcd");
    	String regInsttCd = (String) map.get("regInsttCd");
    	String session_usergb = (String) map.get("session_usergb");
    	
    	if(!session_usergb.equals("C01001") && !session_usergb.equals("C01003")) {
    		throw new MException(egovMessageSource.getMessageArgs("dems.site.rcv.008", null));
    	}
    	
    	if(!insttCd.equals(regInsttCd)) {
    		throw new MException(egovMessageSource.getMessageArgs("dems.site.rcv.008", null));
    	}
    	
//    	map.put("sqlQueryId", "incdtIncdtDAO.queryIncdtUsingChkCnt");
//    	int chkCnt =  comService.selectCommonQueryListTotCnt(map);
//    	
//    	if(chkCnt > 0) {
//    		model.addAttribute(DemsConst.Messages_UserComMessage,
//					egovMessageSource.getMessageArgs("dems.incdt.incdt.000", null));
//    		return new ModelAndView(ajaxMainView, model);
//    	}
    	
    	map.put("sqlQueryId", "incdtIncdtDAO.delIncdtIncdtUDtl");
    	comService.updateCommonQuery(map);

    	model.addAttribute(DemsConst.Messages_SysSucMessage,
    			egovMessageSource.getMessageArgs("success.common.delete", null));


    	return new ModelAndView(ajaxMainView, model);

    }

}	