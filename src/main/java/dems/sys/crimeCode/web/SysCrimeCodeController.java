package dems.sys.crimeCode.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.DemsConst;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;

/**
 *
 * <pre>
 * 1. 클래스명 : SysCrimeCodeController.java
 * 2. 작성일 : 2021. 4. 20.
 * 3. 작성자 : jij
 * 4. 설명 : @!@ 죄명 코드 관리 담당
 * </pre>
 */
@Controller
public class SysCrimeCodeController {

	protected Logger log = LoggerFactory.getLogger(SysCrimeCodeController.class);

	@Resource(name="comService")
	private ComService comService;

	@Resource(name="egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource MappingJackson2JsonView ajaxMainView;

	 /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;


    /**
     *
     * <pre>
     * 1. 메소드명 : indexSysCrimeCodeMList
     * 2. 작성일 : 2021. 4. 20. 오후 2:34:43
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 죄명코드 관리 리스트 조회 화면 이동
     * </pre>
     * @param request
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/sys/crimecode/indexSysCrimeCodeMList.do")
    public String indexSysCrimeCodeMList(HttpServletRequest request, ModelMap model, SessionStatus status) throws Exception {

    	return "dems/sys/crimecode/sysCrimeCodeMList";

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : querySysCrimeCodeMList
     * 2. 작성일 : 2021. 4. 21. 오후 12:12:11
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 죄명코드 관리 리스트 조회
     * </pre>
     * @param model
     * @param request
     * @param reqParams
     * @return
     * @throws Exception
     */
    @RequestMapping("/sys/crimecode/querySysCrimeCodeMList.do")
	public ModelAndView querySysCrimeCodeMList(ModelMap model,
			HttpServletRequest request, @RequestParam HashMap reqParams) throws Exception {

		reqParams.put("sqlQueryId", "sysCrimeCodeDAO.querySysCrimeCodeMListTotCnt");
		int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
 		reqParams.put("totalCount", totCnt);

 		PageUtil.calcPage(reqParams);

		reqParams.put("sqlQueryId", "sysCrimeCodeDAO.querySysCrimeCodeMList");

		List list = comService.selectCommonQueryList(reqParams);
		System.out.println("@@@@@@@@@@@@@@@@list = " + list);
		reqParams.put("list", list);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);

	}


    /**
     *
     * <pre>
     * 1. 메소드명 : indexSysCrimeCodeRDtl
     * 2. 작성일 : 2021. 4. 20. 오후 6:21:11
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 죄명코드 관리 등록 화면 이동
     * </pre>
     * @param request
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/sys/crimecode/indexSysCrimeCodeRDtl.do")
    public String indexSysCrimeCodeRDtl(HttpServletRequest request, ModelMap model, SessionStatus status) throws Exception {

    	return "dems/sys/crimecode/sysCrimeCodeRDtl";

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : regSysCrimeCodeRDtl
     * 2. 작성일 : 2021. 4. 20. 오후 7:24:24
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 죄명코드 관리 등록
     * </pre>
     * @param request
     * @param reqParams
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping("/sys/crimecode/regSysCrimeCodeRDtl.do")
    public ModelAndView regSysCrimeCodeRDtl(HttpServletRequest request, @RequestParam HashMap reqParams, ModelMap model,  SessionStatus status) throws Exception {

    	HashMap<String,String> requireParams = new HashMap();
    	requireParams.put("crimeCd", "죄명코드");
    	requireParams.put("crimeNm", "죄명");
    	requireParams.put("crimeGrp", "그룹 죄코드");
    	requireParams.put("crimeGrpNm", "그룹 죄명");
    	requireParams.put("relLaw", "관련법령");
    	validateUtil.check(reqParams, requireParams);

		/*
		 * if(reqParams.get("crimeGrp").equals("self")) { reqParams.put("crimeGrp",
		 * reqParams.get("crimeGrpSelf")); reqParams.put("sqlQueryId",
		 * "sysCrimeCodeDAO.querySysCrimeGrpCodeMDtl"); Map rtnGrpMap =
		 * comService.selectCommonQueryMap(reqParams); if(rtnGrpMap != null) {
		 * model.addAttribute(DemsConst.Messages_UserErrMessage,
		 * egovMessageSource.getMessageArgs("dems.sys.crimeCd.001", new String[]
		 * {"그룹죄명코드"}));
		 *
		 * return new ModelAndView(ajaxMainView, model); } }
		 */

    	reqParams.put("sqlQueryId", "sysCrimeCodeDAO.querySysCrimeCodeMDtl");
    	Map rtnMap = comService.selectCommonQueryMap(reqParams);
    	if(rtnMap != null) {
    		model.addAttribute(DemsConst.Messages_UserErrMessage,
					egovMessageSource.getMessageArgs("common.isExist.msg", null));

        	return new ModelAndView(ajaxMainView, model);
    	}


    	reqParams.put("sqlQueryId", "sysCrimeCodeDAO.regSysCrimeCodeRDtl");
    	comService.updateCommonQuery(reqParams);

    	model.addAttribute(DemsConst.Messages_SysSucMessage,
    			egovMessageSource.getMessageArgs("success.common.insert", null));

    	return new ModelAndView(ajaxMainView, model);

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : indexSysCrimeCodeUDtl
     * 2. 작성일 : 2021. 4. 21. 오전 10:58:51
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 죄명코드 관리 수정 화면 이동
     * </pre>
     * @param request
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/sys/crimecode/indexSysCrimeCodeUDtl.do")
    public String indexSysCrimeCodeUDtl(HttpServletRequest request, ModelMap model, SessionStatus status) throws Exception {

    	return "dems/sys/crimecode/sysCrimeCodeUDtl";

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : querySysCrimeCodeMDtl
     * 2. 작성일 : 2021. 4. 21. 오전 11:18:15
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 죄명코드 상세 조회
     * </pre>
     * @param map
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/sys/crimecode/querySysCrimeCodeMDtl.do")
    public ModelAndView querySysCrimeCodeMDtl(@RequestParam HashMap<String, String> map, HttpServletRequest request,
    		ModelMap model) throws Exception {

    	map.put("sqlQueryId", "sysCrimeCodeDAO.querySysCrimeCodeMDtl");
    	Map rtnMap = comService.selectCommonQueryMap(map);
    	model.addAttribute("rtnMap", rtnMap);

    	return new ModelAndView(ajaxMainView, model);

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : updSysCrimeCodeUDtl
     * 2. 작성일 : 2021. 4. 21. 오전 10:59:12
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 죄명코드 관리 수정
     * </pre>
     * @param request
     * @param reqParams
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping("/sys/crimecode/updSysCrimeCodeUDtl.do")
    public ModelAndView updSysCrimeCodeUDtl(HttpServletRequest request, @RequestParam HashMap reqParams, ModelMap model,  SessionStatus status) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	if(reqParams.get("crimeGrp").equals("self")) {
    		reqParams.put("crimeGrp", reqParams.get("crimeGrpSelf"));
    		reqParams.put("sqlQueryId", "sysCrimeCodeDAO.querySysCrimeGrpCodeMDtl");
    		Map rtnGrpMap = comService.selectCommonQueryMap(reqParams);
    		if(rtnGrpMap != null) {
    			model.addAttribute(DemsConst.Messages_UserComMessage,
    					egovMessageSource.getMessageArgs("dems.sys.crimeCd.001", new String[] {"그룹죄명코드"}));

    			return new ModelAndView(ajaxMainView, model);
    		}
    	}

    	reqParams.put("updrId", user.getUserId());
    	reqParams.put("sqlQueryId", "sysCrimeCodeDAO.updSysCrimeCodeUDtl");
    	comService.updateCommonQuery(reqParams);

    	model.addAttribute(DemsConst.Messages_SysSucMessage,
    			egovMessageSource.getMessageArgs("success.common.update", null));

    	return new ModelAndView(ajaxMainView, model);

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : delSysCrimeCodeUDtl
     * 2. 작성일 : 2021. 4. 21. 오전 11:17:02
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 죄명코드 관리 삭제
     * </pre>
     * @param map
     * @param model
     * @param request
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping("/sys/crimecode/delSysCrimeCodeUDtl.do")
    public ModelAndView delSysCrimeCodeUDtl(@RequestParam HashMap<String, String> map, ModelMap model,
    		HttpServletRequest request, SessionStatus status) throws Exception {

    	map.put("sqlQueryId", "sysCrimeCodeDAO.delSysCrimeCodeUDtl");
    	comService.updateCommonQuery(map);

    	model.addAttribute(DemsConst.Messages_SysSucMessage,
    			egovMessageSource.getMessageArgs("success.common.delete", null));


    	return new ModelAndView(ajaxMainView, model);

    }

    /**
    *
    * <pre>
    * 1. 메소드명 : queryPgsStat
    * 2. 작성일 : 2021. 4. 22.
    * 3. 작성자 : leeji
    * 4. 설명 :  죄명코드 관리 -> 중복죄명조회 팝업 관련죄명 업데이트
    * </pre>
    * @param paramData
    * @param model
    * @param request
    * @return ModelAndView
    * @throws Exception
    */
   @RequestMapping("/sys/crimecode/querySysCrimeMatchCodeUpdate.do")
   public ModelAndView querySysCrimeMatchCodeUpdate(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

	   ObjectMapper mapper = new ObjectMapper();
	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

	   List cdList = (List) jsonObject.get("cdDatas");
	   List nmList = (List) jsonObject.get("nmDatas");

	   Map<String, Object> paramMap = new HashMap<String, Object>();

	   paramMap.put("userId", user.getUserId());
	   paramMap.put("sqlQueryId", "updSysCrimeMatchCodeUDtl");

	   Map<String, Object> sMap = new HashMap<String, Object>();
	   sMap.put("sqlQueryId", "sysCrimeCodeDAO.querySysCrimeCodeNm");

	   for(int i = 0; i < cdList.size(); i++) {

		    sMap.put("crimeNm", nmList.get(i));
		    String matchCd = comService.selectCommonQueryString(sMap);

		    paramMap.put("crimeCd", cdList.get(i));
		    paramMap.put("matchCd", matchCd);

		    comService.updateCommonQuery(paramMap);
	   }

	   return new ModelAndView(ajaxMainView, model);

   }

}