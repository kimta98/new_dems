package dems.sys.dash.service;

public interface SysDashService {

	/**
	 * <pre>
	 * 1. 메소드명 : regSysDashUDtl
	 * 2. 작성일 : 2021. 5. 21. 오전 11:53:18
	 * 3. 작성자 : jij
	 * 4. 설명 : @!@ 대시보드관리 저장
	 * </pre>
	 * @param paramData
	 */
	void regSysDashUDtl(String paramData) throws Exception;

}