package dems.bbs.free.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import dems.bbs.free.service.BbsFreeService;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import frame.fcom.service.ComService;


@Service("bbsFreeService")
public class BbsFreeServiceImpl extends EgovAbstractServiceImpl implements BbsFreeService{

	protected Logger log = LoggerFactory.getLogger(BbsFreeServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;

	@Resource(name="bbsFreeDAO")
	private BbsFreeDAO bbsFreeDAO;

	/**
     *
     * <pre>
     * 1. 메소드명 : selectBbsFree
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 리스트 조회
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public List selectBbsFreeList(Map<String, Object> paramMap) throws Exception {

		return (List) bbsFreeDAO.selectBbsFreeList(paramMap);
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : updateBbsFreeCount
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 조회수 update
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public void updateBbsFreeCount(Map<String, Object> paramMap) throws Exception {

		bbsFreeDAO.updateBbsFreeCount(paramMap);

	}

	/**
     *
     * <pre>
     * 1. 메소드명 : selectBbsFreeUser
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 사용자 조회
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public Map selectBbsFreeUser(Map<String, Object> paramMap) throws Exception {

		return bbsFreeDAO.selectBbsFreeUser(paramMap);
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : selectUserGb
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 사용자그룹 조회
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public String selectUserGb(Map<String, Object> paramMap) throws Exception {

		String userId = (String) paramMap.get("userId");
		return bbsFreeDAO.selectUserGb(userId);
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : insertBbsFree
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 insert
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public String insertBbsFree(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {
    	bbsFreeDAO.insertBbsFrees(paramMap);
        return "";
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : updateBbsFree
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 update
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public String updateBbsFree(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {

    	bbsFreeDAO.updateBbsFree(paramMap);
    	return "";
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : deleteBbsFree
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 delete
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public void deleteBbsFree(Map<String, Object> paramMap) throws Exception {

		bbsFreeDAO.deleteBbsFree(paramMap);

	}

	/**
     *
     * <pre>
     * 1. 메소드명 : selectBbsFreeRDtl
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 상세 조회
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public Map selectBbsFreeRDtl(Map<String, Object> paramMap) throws Exception {

		return bbsFreeDAO.selectBbsFreeRDtl(paramMap);
	}


}