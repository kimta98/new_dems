/**
 * 
 * <pre>
 * 1. 클래스명 : BbsFreeDAO.java
 * 2. 작성일 : 2021. 4. 15.
 * 3. 작성자 : Leeji
 * 4. 설명 : BbsFreeDAO
 * </pre>
 * 
 */
package dems.bbs.free.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;


@Repository("bbsFreeDAO")
public class BbsFreeDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(BbsFreeDAO.class);

	/**
     * 
     * <pre>
     * 1. 메소드명 : selectBbsFreeList
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 리스트 조회
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return Map
     * @throws Exception
     */
    public List selectBbsFreeList(Map<String, Object> paramMap) throws Exception {

    	return (List) selectList("bbsFreeDAO.selectBbsFreeList", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : updateBbsFreeCount
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 조회수 update 
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return void
     * @throws Exception
     */
    public void updateBbsFreeCount(Map<String, Object> paramMap) throws Exception {
    	update("bbsFreeDAO.updateBbsFreeCount", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : selectBbsFreeUser
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 사용자 조회
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return map
     * @throws Exception
     */
    public Map selectBbsFreeUser(Map<String, Object> paramMap) throws Exception {

    	return (Map) selectByPk("bbsFreeDAO.selectBbsFreeUser", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : selectUserGb
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 사용자그룹 조회
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return String
     * @throws Exception
     */
	public String selectUserGb(String userId) throws Exception{
		return (String)selectOne("bbsFreeDAO.selectUserGb", userId);
	}

	/**
     * 
     * <pre>
     * 1. 메소드명 : insertBbsFree
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 insert
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return int
     * @throws Exception
     */
    public int insertBbsFrees(Map<String, Object> paramMap) throws Exception {

    	return (Integer) insert("bbsFreeDAO.insertBbsFree", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : updateBbsFree
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 update
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return void
     * @throws Exception
     */
    public void updateBbsFree(Map<String, Object> paramMap) throws Exception {
        update("bbsFreeDAO.updateBbsFree", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : deleteBbsFree
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 delete
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return void
     * @throws Exception
     */
    public void deleteBbsFree(Map<String, Object> paramMap) throws Exception {
        update("bbsFreeDAO.deleteBbsFree", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : selectBbsFreeRDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 상세 조회
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return Map
     * @throws Exception
     */
    public Map selectBbsFreeRDtl(Map<String, Object> paramMap) throws Exception {

    	return (Map) selectOne("bbsFreeDAO.selectBbsFreeRDtl", paramMap);
    }

}