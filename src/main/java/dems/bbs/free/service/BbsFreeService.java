/**
 * 
 * <pre>
 * 1. 클래스명 : BbsFreeService.java
 * 2. 작성일 : 2021. 4. 15.
 * 3. 작성자 : Leeji
 * 4. 설명 : 자유게시판 Service
 * </pre>
 * 
 */
package dems.bbs.free.service;

import java.util.List;
import java.util.Map;


public interface BbsFreeService {

	List selectBbsFreeList(Map<String,Object> paramMap) throws Exception;
	
    void updateBbsFreeCount(Map<String,Object> paramMap) throws Exception;
    
    Map selectBbsFreeRDtl(Map<String,Object> paramMap) throws Exception;
    
    Map selectBbsFreeUser(Map<String,Object> paramMap) throws Exception;
    
    String selectUserGb(Map<String,Object> paramMap) throws Exception;
    
	String insertBbsFree(Map<String,Object> paramMap, Map<String,Object> fileMap) throws Exception;
	
    String updateBbsFree(Map<String,Object> paramMap, Map<String,Object> fileMap) throws Exception;
    
    void deleteBbsFree(Map<String,Object> paramMap) throws Exception;

}