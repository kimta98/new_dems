/**
 * <pre>
 * 1. 클래스명 : BbsFreeController
 * 2. 작성일 : 2021. 04. 14.
 * 3. 작성자 : leeji
 * 4. 설명 : 자유게시판 Controller
 * </pre>
 *
 */
package dems.bbs.free.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.bbs.free.service.BbsFreeService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovDateUtil;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.DemsConst;
import frame.futil.DemsUtil;
import frame.futil.PageUtil;

@Controller
public class BbsFreeController {

	protected Logger log = LoggerFactory.getLogger(BbsFreeController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "bbsFreeService")
	private BbsFreeService bbsFreeService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;


    /**
     *
     * <pre>
     * 1. 메소드명 : indexBbsFreeMList
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 목록페이지 진입시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/free/indexBbsFreeMList.do")
    public String indexBbsFreeMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

        return "dems/bbs/free/bbsFreeMList";
    }


    /**
     *
     * <pre>
     * 1. 메소드명 : queryBbsFreeMList
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 목록 조회시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/free/queryBbsFreeMList.do")
    public ModelAndView queryBbsFreeMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	reqParams.put("userGb", user.getUserGb());
    	reqParams.put("regrId", user.getUserId());

		reqParams.put("sqlQueryId", "bbsFreeDAO.selectBbsFreeListCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);

    	PageUtil.calcPage(reqParams);

		reqParams.put("sqlQueryId", "bbsFreeDAO.selectBbsFreeList");
		List bbsFreeList = comService.selectCommonQueryList(reqParams);
		reqParams.put("bbsFreeList", bbsFreeList);
		model.addAllAttributes(reqParams);
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@ reqParams : " + reqParams);
		return new ModelAndView(ajaxMainView, model);


    }

    /**
     *
     * <pre>
     * 1. 메소드명 : indexBbsFreeRDtl
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 등록페이지 진입시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/free/indexBbsFreeRDtl.do")
    public String indexBbsFreeRDtl(HttpServletRequest request, @RequestParam HashMap<String,String> map, ModelMap model, SessionStatus status) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	model.addAttribute("regNm",user.getUserNm());

    	return "dems/bbs/free/bbsFreeRDtl";
    }

    /**
     *
     * <pre>
     * 1. 메소드명 : regBbsFreeRDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 등록시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/free/regBbsFreeRDtl.do")
    public ModelAndView regBbsFreeRDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "fBbsNtcDAO.selectFileSeq");
    	int fileSeq = comService.selectCommonQueryListTotCnt(reqParams);

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	reqParams.put("atchFileId", fileSeq);
    	reqParams.put("regrId", user.getUserId());
    	reqParams.put("boardKind", "C23009");

    	String sRturnResult = "";

    	String putupDt = EgovDateUtil.getCurrentDateAsString();
    	reqParams.put("putupDt", putupDt.replaceAll("-", ""));

    	HashMap<String, Object> fileMap = DemsUtil.convertMap(request);

		sRturnResult = bbsFreeService.insertBbsFree(reqParams,fileMap);

		model.addAttribute("fileSeq",fileSeq);
		if(sRturnResult.equals("2")){
			model.addAttribute(DemsConst.Messages_UserComMessage,"보안상 업로드할 수 없는 파일이 있어 저장하지 못하였습니다.");
		}

        return new ModelAndView(ajaxMainView, model);


    }

    /**
     *
     * <pre>
     * 1. 메소드명 : delBbsFreeUDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 삭제시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/free/delBbsFreeUDtl.do")

    public ModelAndView delBbsFreeUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	reqParams.put("regrId", user.getUserId());
    	reqParams.put("boardKind", "C23009");

		bbsFreeService.deleteBbsFree(reqParams);

        return new ModelAndView(ajaxMainView, model);


    }

    /**
     *
     * <pre>
     * 1. 메소드명 : indexBbsFreeUDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 수정페이지 진입시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/free/indexBbsFreeUDtl.do")

    public String indexBbsFreeUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
    	
        return "dems/bbs/free/bbsFreeUDtl";

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : goBbsFreeUDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 수정페이지 조회시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/free/goBbsFreeUDtl.do")

    public ModelAndView goBbsFreeUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("putupSno", Integer.parseInt((String) reqParams.get("putupSno")));
        paramMap.put("boardKind", "C23009");

        Map bbsFreeMap = bbsFreeService.selectBbsFreeRDtl(paramMap);
        String sTemp = DemsUtil.getHtmlStrCnvr((String) bbsFreeMap.get("cnts"));
        bbsFreeMap.put("cntsCvt", sTemp);

	    List<FileVO> atchFileList = comService.selectFileList((String) bbsFreeMap.get("atchFileId"),"");
	    bbsFreeService.updateBbsFreeCount(paramMap);
	    model.addAttribute("bbsFreeMap", bbsFreeMap);
	    model.addAttribute("atchFileList",atchFileList);
    	 return new ModelAndView(ajaxMainView, model);

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : updBbsFreeUDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 수정시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/free/updBbsFreeUDtl.do")

    public ModelAndView updBbsFreeUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	reqParams.put("regrId", user.getUserId());
    	reqParams.put("boardKind", "C23009");

    	String sRturnResult = "";
    	String putupDt = EgovDateUtil.getCurrentDateAsString();
    	reqParams.put("putupDt", putupDt.replaceAll("-", ""));

    	HashMap<String, Object> fileMap = DemsUtil.convertMap(request);
		sRturnResult = bbsFreeService.updateBbsFree(reqParams, fileMap);

		if(sRturnResult.equals("2")){
			model.addAttribute(DemsConst.Messages_SysSucMessage,"보안상 업로드할 수 없는 파일이 있어 저장하지 못하였습니다.");
		}

        return new ModelAndView(ajaxMainView, model);

    }



}