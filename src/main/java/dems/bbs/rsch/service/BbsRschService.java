/**
 * 
 * <pre>
 * 1. 클래스명 : BbsRschService.java
 * 2. 작성일 : 2021. 4. 15.
 * 3. 작성자 : Leeji
 * 4. 설명 : 연구자료관리 Service
 * </pre>
 * 
 */
package dems.bbs.rsch.service;

import java.util.List;
import java.util.Map;


public interface BbsRschService {

	List selectBbsRschList(Map<String,Object> paramMap) throws Exception;
	
    void updateBbsRschCount(Map<String,Object> paramMap) throws Exception;
    
    Map selectBbsRschRDtl(Map<String,Object> paramMap) throws Exception;
    
    Map selectBbsRschUser(Map<String,Object> paramMap) throws Exception;
    
    String selectUserGb(Map<String,Object> paramMap) throws Exception;
    
	String insertBbsRsch(Map<String,Object> paramMap, Map<String,Object> fileMap) throws Exception;
	
    String updateBbsRsch(Map<String,Object> paramMap, Map<String,Object> fileMap) throws Exception;
    
    void deleteBbsRsch(Map<String,Object> paramMap) throws Exception;

}