package dems.bbs.rsch.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import dems.bbs.rsch.service.BbsRschService;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.string.EgovStringUtil;
import frame.fcom.service.ComService;


@Service("bbsRschService")
public class BbsRschServiceImpl extends EgovAbstractServiceImpl implements BbsRschService{

	protected Logger log = LoggerFactory.getLogger(BbsRschServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;

	@Resource(name="bbsRschDAO")
	private BbsRschDAO bbsRschDAO;

	/**
     *
     * <pre>
     * 1. 메소드명 : selectBbsRsch
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 리스트 조회
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public List selectBbsRschList(Map<String, Object> paramMap) throws Exception {

		return (List) bbsRschDAO.selectBbsRschList(paramMap);
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : updateBbsRschCount
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 조회수 update
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public void updateBbsRschCount(Map<String, Object> paramMap) throws Exception {

		bbsRschDAO.updateBbsRschCount(paramMap);

	}

	/**
     *
     * <pre>
     * 1. 메소드명 : selectBbsRschUser
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 사용자 조회
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public Map selectBbsRschUser(Map<String, Object> paramMap) throws Exception {

		return bbsRschDAO.selectBbsRschUser(paramMap);
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : selectUserGb
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 사용자그룹 조회
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public String selectUserGb(Map<String, Object> paramMap) throws Exception {

		String userId = (String) paramMap.get("userId");
		return bbsRschDAO.selectUserGb(userId);
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : insertBbsRsch
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 insert
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public String insertBbsRsch(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {
    	bbsRschDAO.insertBbsRschs(paramMap);

        return "";
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : updateBbsRsch
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 update
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public String updateBbsRsch(Map<String, Object> paramMap, Map<String,Object> fileMap) throws Exception {
    	bbsRschDAO.updateBbsRsch(paramMap);

    	return "";
	}

	/**
     *
     * <pre>
     * 1. 메소드명 : deleteBbsRsch
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 delete
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public void deleteBbsRsch(Map<String, Object> paramMap) throws Exception {

		bbsRschDAO.deleteBbsRsch(paramMap);

	}

	/**
     *
     * <pre>
     * 1. 메소드명 : selectBbsRschRDtl
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  자유게시판 상세 조회
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
	@Override
	public Map selectBbsRschRDtl(Map<String, Object> paramMap) throws Exception {

		return bbsRschDAO.selectBbsRschRDtl(paramMap);
	}


}