/**
 * 
 * <pre>
 * 1. 클래스명 : BbsRschDAO.java
 * 2. 작성일 : 2021. 4. 15.
 * 3. 작성자 : Leeji
 * 4. 설명 : BbsRschDAO
 * </pre>
 * 
 */
package dems.bbs.rsch.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;


@Repository("bbsRschDAO")
public class BbsRschDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(BbsRschDAO.class);

	/**
     * 
     * <pre>
     * 1. 메소드명 : selectBbsRschList
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구자료관리 리스트 조회
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return Map
     * @throws Exception
     */
    public List selectBbsRschList(Map<String, Object> paramMap) throws Exception {

    	return (List) selectList("bbsRschDAO.selectBbsRschList", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : updateBbsRschCount
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구자료관리 조회수 update 
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return void
     * @throws Exception
     */
    public void updateBbsRschCount(Map<String, Object> paramMap) throws Exception {
    	update("bbsRschDAO.updateBbsRschCount", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : selectBbsRschUser
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구자료관리 사용자 조회
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return map
     * @throws Exception
     */
    public Map selectBbsRschUser(Map<String, Object> paramMap) throws Exception {

    	return (Map) selectByPk("bbsRschDAO.selectBbsRschUser", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : selectUserGb
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구자료관리 사용자그룹 조회
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return String
     * @throws Exception
     */
	public String selectUserGb(String userId) throws Exception{
		return (String)selectOne("bbsRschDAO.selectUserGb", userId);
	}

	/**
     * 
     * <pre>
     * 1. 메소드명 : insertBbsRsch
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구자료관리 insert
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return int
     * @throws Exception
     */
    public int insertBbsRschs(Map<String, Object> paramMap) throws Exception {

    	return (Integer) insert("bbsRschDAO.insertBbsRsch", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : updateBbsRsch
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구자료관리 update
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return void
     * @throws Exception
     */
    public void updateBbsRsch(Map<String, Object> paramMap) throws Exception {
        update("bbsRschDAO.updateBbsRsch", paramMap);
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : deleteBbsRsch
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구자료관리 delete
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return void
     * @throws Exception
     */
    public void deleteBbsRsch(Map<String, Object> paramMap) throws Exception {
        update("bbsRschDAO.deleteBbsRsch", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : selectBbsRschRDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구자료관리 상세 조회
     * </pre>
     * @param map
     * @param 
     * @param 
     * @return Map
     * @throws Exception
     */
    public Map selectBbsRschRDtl(Map<String, Object> paramMap) throws Exception {

    	return (Map) selectOne("bbsRschDAO.selectBbsRschRDtl", paramMap);
    }

}