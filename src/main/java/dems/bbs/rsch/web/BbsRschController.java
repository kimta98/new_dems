/**
 * <pre>
 * 1. 클래스명 : BbsRschController
 * 2. 작성일 : 2021. 04. 14.
 * 3. 작성자 : leeji
 * 4. 설명 : 연구관리자료 Controller
 * </pre>
 *
 */
package dems.bbs.rsch.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.bbs.rsch.service.BbsRschService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovDateUtil;
import frame.fcom.service.ComService;
import frame.fcom.service.FileVO;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.DemsConst;
import frame.futil.DemsUtil;
import frame.futil.PageUtil;

@Controller
public class BbsRschController {

	protected Logger log = LoggerFactory.getLogger(BbsRschController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "bbsRschService")
	private BbsRschService bbsRschService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;


    /**
     *
     * <pre>
     * 1. 메소드명 : indexBbsRschMList
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구관리자료 목록페이지 진입시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/rsch/indexBbsRschMList.do")
    public String indexBbsRschMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

        return "dems/bbs/rsch/bbsRschMList";
    }


    /**
     *
     * <pre>
     * 1. 메소드명 : queryBbsRschMList
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구관리자료 목록 조회시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/rsch/queryBbsRschMList.do")
    public ModelAndView queryBbsRschMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	reqParams.put("userGb", user.getUserGb());
    	reqParams.put("regrId", user.getUserId());

		reqParams.put("sqlQueryId", "bbsRschDAO.selectBbsRschListCnt");
    	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
    	reqParams.put("totalCount", totCnt);

    	PageUtil.calcPage(reqParams);

		reqParams.put("sqlQueryId", "bbsRschDAO.selectBbsRschList");
		List bbsRschList = comService.selectCommonQueryList(reqParams);
		reqParams.put("bbsRschList", bbsRschList);
		model.addAllAttributes(reqParams);

		return new ModelAndView(ajaxMainView, model);


    }

    /**
     *
     * <pre>
     * 1. 메소드명 : indexBbsRschRDtl
     * 2. 작성일 : 2021. 4. 15.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구관리자료 등록페이지 진입시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/rsch/indexBbsRschRDtl.do")
    public String indexBbsRschRDtl(HttpServletRequest request, @RequestParam HashMap<String,String> map, ModelMap model, SessionStatus status) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	model.addAttribute("regNm",user.getUserNm());

    	return "dems/bbs/rsch/bbsRschRDtl";
    }

    /**
     *
     * <pre>
     * 1. 메소드명 : regBbsRschRDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구관리자료 등록시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/rsch/regBbsRschRDtl.do")
    public ModelAndView regBbsRschRDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

		reqParams.put("sqlQueryId", "fBbsNtcDAO.selectFileSeq");
    	int fileSeq = comService.selectCommonQueryListTotCnt(reqParams);

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	reqParams.put("atchFileId", fileSeq);
    	reqParams.put("regrId", user.getUserId());
    	reqParams.put("boardKind", "C23008");

    	String sRturnResult = "";

    	String putupDt = EgovDateUtil.getCurrentDateAsString();
    	reqParams.put("putupDt", putupDt.replaceAll("-", ""));

    	HashMap<String, Object> fileMap = DemsUtil.convertMap(request);

		sRturnResult = bbsRschService.insertBbsRsch(reqParams,fileMap);


		model.addAttribute("fileSeq",fileSeq);
		if(sRturnResult.equals("2")){
			model.addAttribute(DemsConst.Messages_SysSucMessage,"보안상 업로드할 수 없는 파일이 있어 저장하지 못하였습니다.");
		}

        return new ModelAndView(ajaxMainView, model);


    }

    /**
     *
     * <pre>
     * 1. 메소드명 : delBbsRschUDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구관리자료 삭제시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/rsch/delBbsRschUDtl.do")

    public ModelAndView delBbsRschUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	reqParams.put("regrId", user.getUserId());
    	reqParams.put("boardKind", "C23008");

		bbsRschService.deleteBbsRsch(reqParams);

        return new ModelAndView(ajaxMainView, model);


    }

    /**
     *
     * <pre>
     * 1. 메소드명 : indexBbsRschUDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구관리자료 수정페이지 진입시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/rsch/indexBbsRschUDtl.do")

    public String indexBbsRschUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

        return "dems/bbs/rsch/bbsRschUDtl";

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : goBbsRschUDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구관리자료 수정페이지 조회시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/rsch/goBbsRschUDtl.do")

    public ModelAndView goBbsRschUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("putupSno", Integer.parseInt((String) reqParams.get("putupSno")));
        paramMap.put("boardKind", "C23008");

        Map bbsRschMap = bbsRschService.selectBbsRschRDtl(paramMap);
        String sTemp = DemsUtil.getHtmlStrCnvr((String) bbsRschMap.get("cnts"));
        bbsRschMap.put("cntsCvt", sTemp);

	    List<FileVO> atchFileList = comService.selectFileList((String) bbsRschMap.get("atchFileId"),"");
	    bbsRschService.updateBbsRschCount(paramMap);
	    model.addAttribute("bbsRschMap", bbsRschMap);
	    model.addAttribute("atchFileList",atchFileList);

    	 return new ModelAndView(ajaxMainView, model);

    }

    /**
     *
     * <pre>
     * 1. 메소드명 : updBbsRschUDtl
     * 2. 작성일 : 2021. 4. 14.
     * 3. 작성자 : leeji
     * 4. 설명 :  연구관리자료 수정시 호출
     * </pre>
     * @param map
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/bbs/rsch/updBbsRschUDtl.do")

    public ModelAndView updBbsRschUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	reqParams.put("regrId", user.getUserId());
    	reqParams.put("boardKind", "C23008");

    	String sRturnResult = "";
    	String putupDt = EgovDateUtil.getCurrentDateAsString();
    	reqParams.put("putupDt", putupDt.replaceAll("-", ""));

    	HashMap<String, Object> fileMap = DemsUtil.convertMap(request);
		sRturnResult = bbsRschService.updateBbsRsch(reqParams, fileMap);

		if(sRturnResult.equals("2")){
			model.addAttribute(DemsConst.Messages_SysSucMessage,"보안상 업로드할 수 없는 파일이 있어 저장하지 못하였습니다.");
		}

        return new ModelAndView(ajaxMainView, model);

    }



}