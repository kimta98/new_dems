package dems.tral.pgs.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.tral.pgs.service.TralPgsService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.PageUtil;

/**
 * <pre>
 * 1. 클래스명 : TralPgsController.java
 * 2. 작성일 : 2021. 4. 28.
 * 3. 작성자 : sjw7240
 * 4. 설명 : 공판관리 요청관리 Controller
 * </pre>
 */
@Controller
public class TralPgsController {

	protected Logger log = LoggerFactory.getLogger(TralPgsController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "tralPgsService")
	private TralPgsService tralPgsService;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;


    /**
     * 
     * <pre>
     * 1. 메소드명 : indexTralPgsMList
     * 2. 작성일 : 2021. 4. 28.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판관리_요청관리 목록_진입
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/tral/pgs/indexTralPgsMList.do")
    public String indexTralPgsMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

        return "dems/tral/pgs/tralPgsMList";
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryTralPgsMList
     * 2. 작성일 : 2021. 4. 28.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판관리_요청관리 목록_조회
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/tral/pgs/queryTralPgsMList.do")
    public ModelAndView queryTralPgsMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
    	
       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	reqParams.put("userGb", user.getUserGb());
    	reqParams.put("srcRegrId", user.getUserId());
    	reqParams.put("srcInsttCd", user.getInsttCd());
    	reqParams.put("tral", "pgs");
        reqParams.put("srcPgsStat", ((String) reqParams.get("srcPgsStat")).split(","));
    	
    	int totCnt = tralPgsService.selectTralPgsListCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	PageUtil.calcPage(reqParams);

    	List tralPgsList = tralPgsService.selectTralPgsList(reqParams);		
		reqParams.put("tralPgsList", tralPgsList);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);
    	   	
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexTralPgsRDtl
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청관리 등록 호출
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/tral/pgs/indexTralPgsRDtl.do")
    public String indexTralPgsRDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

       FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
 	   
 	   model.addAttribute("reqInsttNm", user.getInsttNm());    //요청기관명
 	   model.addAttribute("reqDepNm", user.getDeptNm());      //요청부서명
 	   model.addAttribute("reqInsttCd", user.getInsttCd());  //요청기관코드
 	   
        return "dems/tral/pgs/tralPgsRDtl";
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : tstReqTralPgsRDtl
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청관리 증언 요청(등록)
     * </pre>
     * @param request
     * @param model
     * @param paramData
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/tral/pgs/tstReqTralPgsRDtl.do")
    public ModelAndView tstReqTralPgsRDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	ObjectMapper mapper = new ObjectMapper();
    	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

    	//증인 상세정보
    	ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
    	//증인 정보
    	Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");

    	Map<String, Object> paramMap = new HashMap<String, Object>();
    	paramMap.put("formDataMap", formDataMap);
    	paramMap.put("detailList", tpList);
    	paramMap.put("userGb", user.getUserGb());
    	paramMap.put("userId", user.getUserId());

    	tralPgsService.insertTralPgs(paramMap);

        return new ModelAndView(ajaxMainView, model);

    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryTralPgsPop
     * 2. 작성일 : 2021. 5. 4.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청 팝업 호출
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
   @RequestMapping(value="/tral/pgs/queryTralPgsPop.do")
   public ModelAndView queryTralPgsPop(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

	    FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	   	reqParams.put("regrId", user.getUserId());
	   	reqParams.put("insttCd", user.getInsttCd());
	   	
	   	String type = (String) reqParams.get("type");
	   	String userGb = "";
	   	
	   	if(type.equals("prosr")) {
	   		userGb = "C01002";
	   	}else if(type.equals("regUser")) {
	   		userGb = "C01001";
	   	}else if(type.equals("tralCrgr")) {
	   		userGb = "C01003";
	   	}else if(type.equals("incdt")) {
	   		userGb = user.getUserGb();
	   	}
	   	
	   	switch (type) {
		case "incdt": //사건			
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "tralPgsDAO.selectIncidentListCnt");
			break;
		case "prosr": //검사
		case "regUser": //담당자
		case "tralCrgr": //증언담당자
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "tralPgsDAO.selectCrgrListCnt");
			break;
        case "tstf":
            reqParams.put("sqlQueryId", "tralPgsDAO.selectTstfListCnt");
            break;

		}
	   	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
	   	reqParams.put("totalCount", totCnt);
	   	
	   	PageUtil.calcPage(reqParams);
	   	
	   	switch (type) {
		case "incdt": //사건			
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "tralPgsDAO.selectIncidentList");
			break;
		case "prosr": //검사
		case "regUser": //담당자
		case "tralCrgr": //증언담당자
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "tralPgsDAO.selectCrgrList");
			break;
        case "tstf":
            reqParams.put("sqlQueryId", "tralPgsDAO.selectTstfList");
            break;

		}
	   	
	   	List popList = comService.selectCommonQueryList(reqParams);		
		reqParams.put("popList", popList);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

   }
   
   /**
    * 
    * <pre>
    * 1. 메소드명 : reqTralPgsRDtlSno
    * 2. 작성일 : 2021. 5. 6. 
    * 3. 작성자 : sjw7240
    * 4. 설명 : 증언요청 최신번호 가져오기
    * </pre>
    * @param request
    * @param model
    * @param paramData
    * @param response
    * @return
    * @throws Exception
    */
   @RequestMapping(value="/tral/pgs/reqTralPgsRDtlSno.do")
   public ModelAndView reqTralPgsRDtlSno(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

   	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

   	ObjectMapper mapper = new ObjectMapper();
   	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

   	//증인 상세정보
   	ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
   	//증인 정보
   	Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");

   	Map<String, Object> paramMap = new HashMap<String, Object>();
   	paramMap.put("formDataMap", formDataMap);
   	paramMap.put("detailList", tpList);
   	paramMap.put("userGb", user.getUserGb());
   	paramMap.put("userId", user.getUserId());

   	tralPgsService.insertTralPgs(paramMap);

       return new ModelAndView(ajaxMainView, model);

   }

   /**
    * 
    * <pre>
    * 1. 메소드명 : indexTralPgsUDtl
    * 2. 작성일 : 2021. 5. 6.
    * 3. 작성자 : sjw7240
    * 4. 설명 : 공판지원요청 상세 진입
    * </pre>
    * @param request
    * @param model
    * @param reqParams
    * @return
    * @throws Exception
    */
  @RequestMapping(value="/tral/pgs/indexTralPgsUDtl.do")
  public String indexTralPgsUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {
    	 
    	return "dems/tral/req/tralReqUDtl";
  }

  /**
   * 
   * <pre>
   * 1. 메소드명 : queryTralPgsInfo
   * 2. 작성일 : 2021. 5. 6.
   * 3. 작성자 : sjw7240
   * 4. 설명 : 공판지원요청 상세 호출
   * </pre>
   * @param request
   * @param model
   * @param reqParams
   * @return
   * @throws Exception
   */
  @RequestMapping(value="/tral/pgs/queryTralPgsInfo.do")
  public ModelAndView queryTralPgsInfo(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

	 	reqParams.put("sqlQueryId", "tralPgsDAO.selectTralPgsInfo");
	 	Map tralPgsInfoMap = comService.selectCommonQueryMap(reqParams);
	 	reqParams.put("tralPgsInfoMap", tralPgsInfoMap);
	 	
	 	reqParams.put("sqlQueryId", "tralPgsDAO.selectTralPgsInfoChk");
	 	Map tralPgsInfoChkMap = comService.selectCommonQueryMap(reqParams);
	 	reqParams.put("tralPgsInfoChkMap", tralPgsInfoChkMap);
  	
 	model.addAllAttributes(reqParams); 	
 	 
 	return new ModelAndView(ajaxMainView, model);
  }


  /**
   * 
   * <pre>
   * 1. 메소드명 : updTralPgsUDtl
   * 2. 작성일 : 2021. 5. 7. 
   * 3. 작성자 : sjw7240
   * 4. 설명 : 공판지원요청 수정
   * </pre>
   * @param request
   * @param model
   * @param paramData
   * @param response
   * @return
   * @throws Exception
   */
  @RequestMapping(value="/tral/pgs/updTralPgsUDtl.do")
  public ModelAndView updTralPgsUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

  	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

  	ObjectMapper mapper = new ObjectMapper();
  	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

  	//증인 상세정보
  	ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
  	//증인 정보
  	Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");

  	Map<String, Object> paramMap = new HashMap<String, Object>();
  	paramMap.put("formDataMap", formDataMap);
  	paramMap.put("detailList", tpList);
  	paramMap.put("userGb", user.getUserGb());
  	paramMap.put("userId", user.getUserId());
  	paramMap.put("insttCd", user.getInsttCd());

  	tralPgsService.updTralPgsUDtl(paramMap);

     return new ModelAndView(ajaxMainView, model);

  }

  /**
   * 
   * <pre>
   * 1. 메소드명 : delTralPgsUDtl
   * 2. 작성일 : 2021. 5. 10. 
   * 3. 작성자 : sjw7240
   * 4. 설명 : 공판지원요청 삭제(승인취소)
   * </pre>
   * @param request
   * @param model
   * @param paramData
   * @param response
   * @return
   * @throws Exception
   */
  @RequestMapping(value="/tral/pgs/delTralPgsUDtl.do")
  public ModelAndView delTralPgsUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	
	   ObjectMapper mapper = new ObjectMapper();
	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
	
	   //요청 리스트
	   List tpList = (List) jsonObject.get("rowDatas");
	   
	   //요청 타입(승인요청,승인요청취소,검사승인,검사승인취소,선별확인,선별확인취소)
	   Map<String, Object> infoMap = jsonObject.get("type");
	   String type = (String)tpList.get(tpList.size()-2);
       String tralPgsSno = "";
       String updrId = (String)tpList.get(tpList.size()-1);
       String incdtSno = "";
       String pgsStat = (String)infoMap.get("pgsStat");
       
       for(int i = 0; i < tpList.size()-2; i+=2) {
		   tralPgsSno = (String)tpList.get(i);
		   incdtSno = (String)tpList.get(i+1);
			
		   Map<String, Object> paramMap = new HashMap<String, Object>();
		   paramMap.clear();
		   paramMap.put("aprvList", tpList);
		   paramMap.put("userGb", user.getUserGb());
		   paramMap.put("userId", user.getUserId());
		   paramMap.put("insttCd", user.getInsttCd());
		   paramMap.put("type", type);
		   paramMap.put("tralPgsSno", tralPgsSno);
		   paramMap.put("incdtSno", incdtSno);
		
/*		   tralPgsService.queryPgsStat(paramMap);*/
		   
		   //삭제작업
		   paramMap.clear();
	       paramMap.put("tralPgsSno", tralPgsSno);
		   paramMap.put("type", type);
		   paramMap.put("regrId", updrId);
		   if(pgsStat.equals("C03002")) {
			   paramMap.put("pgsStat", "C03001");
		   } else if(pgsStat.equals("C03003")) {
			   paramMap.put("pgsStat", "C03002");
		   } else if(pgsStat.equals("C03005")) {
                paramMap.put("pgsStat", "C03003");
           } else if(pgsStat.equals("C03006")){
                paramMap.put("pgsStat", "C03005");
           }

	       tralPgsService.delTralPgsUDtl(paramMap);
       }
	   
	   return new ModelAndView(ajaxMainView, model);
  }

  /**
   * 
   * <pre>
   * 1. 메소드명 : queryPgsStat
   * 2. 작성일 : 2021. 5. 11.
   * 3. 작성자 : sjw7240
   * 4. 설명 : 
   * </pre>
   * @param request
   * @param model
   * @param paramData
   * @param response
   * @return
   * @throws Exception
   */
  @RequestMapping(value="/tral/pgs/queryPgsStat.do")
  public ModelAndView queryPgsStat(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	
	   ObjectMapper mapper = new ObjectMapper();
	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
	
	   //요청 리스트
	   List tpList = (List) jsonObject.get("rowDatas");
	   
	   //요청 타입(승인요청,승인요청취소,검사승인,검사승인취소,선별확인,선별확인취소)
	   Map<String, Object> infoMap = jsonObject.get("type");
	   String type = (String) infoMap.get("type");
	
	   Map<String, Object> paramMap = new HashMap<String, Object>();
	   paramMap.put("aprvList", tpList);
	   paramMap.put("userGb", user.getUserGb());
	   paramMap.put("userId", user.getUserId());
	   paramMap.put("insttCd", user.getInsttCd());
	   paramMap.put("type", type);
	
	   tralPgsService.queryPgsStat(paramMap);
	   
	   return new ModelAndView(ajaxMainView, model);
}
  
  
}
