package dems.tral.pgs.service;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 1. 클래스명 : TralPgsService.java
 * 2. 작성일 : 2021. 4. 28.
 * 3. 작성자 : sjw7240
 * 4. 설명 : 
 * </pre>
 */
public interface TralPgsService {
	
	List selectTralPgsList(Map<String,Object> map) throws Exception;
	
	List indexTralPgsUDtl(Map<String,Object> map) throws Exception;
	
	int selectTralPgsListCnt(Map<String,Object> map) throws Exception;
	
	void queryPgsStat(Map<String,Object> map) throws Exception;	
	
	void insertTralPgs(Map<String,Object> map) throws Exception;

	void updTralPgsUDtl(Map<String,Object> map) throws Exception;
	
	void delTralPgsUDtl(Map<String,Object>map) throws Exception;
}
