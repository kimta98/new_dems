package dems.tral.pgs.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("tralPgsDAO")
public class TralPgsDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(TralPgsDAO.class);

	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralPgsList
	 * 2. 작성일 : 2021. 4. 28. 
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 리스트 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List selectTralPgsList(Map map) throws Exception{
		return selectList("tralPgsDAO.selectTralPgsList", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexTralPgsUDtl
	 * 2. 작성일 : 2021. 5. 6.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 상세 조회(진입)
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List indexTralPgsUDtl(Map map) throws Exception{
		return selectList("tralPgsDAO.indexTralPgsUDtl", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralPgsListCnt
	 * 2. 작성일 : 2021. 4. 28. 
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 리스트 카운트
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int selectTralPgsListCnt(Map map) throws Exception{
		return selectOne("tralPgsDAO.selectTralPgsListCnt", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : insertTestifySuppPgs
	 * 2. 작성일 : 2021. 5. 3.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판지원요청 insert
	 * </pre>
	 * @param paramMap
	 * @throws Exception
	 */
    public void insertTestifySuppPgs(Map<String, Object> paramMap) throws Exception {

    	insert("tralPgsDAO.insertTestifySuppPgs", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : insertTestifyEvdcDtl
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청상세 insert
     * </pre>
     * @param paramMap
     * @throws Exception
     */
    public void insertTestifyEvdcDtl(Map<String, Object> paramMap) throws Exception {

    	 	insert("tralPgsDAO.insertTestifyEvdcDtl", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : selectTralPgsIncdtChk
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판 관리 사건 우뮤 체크
     * </pre>
     * @param map
     * @return
     * @throws Exception
     */
	public int selectTralPgsIncdtChk(Map map) throws Exception{
		return selectOne("tralPgsDAO.selectTralPgsIncdtChk", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralPgsSno
	 * 2. 작성일 : 2021. 5. 3.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판요청 일련번호 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public String selectTralPgsSno(Map map) throws Exception{
		return selectOne("tralPgsDAO.selectTralPgsSno", map);
	}

	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectRcvNo
	 * 2. 작성일 : 2021. 5. 3.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판요청 접수번호 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public String selectRcvNo(Map map) throws Exception{
		return selectOne("tralPgsDAO.selectRcvNo", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralPgsUserGb
	 * 2. 작성일 : 2021. 5. 3
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판요청 사용자그룹 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public String selectTralPgsUserGb(Map map) throws Exception{
		return (String)selectOne("tralPgsDAO.selectTralPgsUserGb", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectPgsInsttCd
	 * 2. 작성일 : 2021. 5. 4. 
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 요청기관코드 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
 	public String selectPgsInsttCd(Map map) throws Exception{
 		return selectOne("tralPgsDAO.selectPgsInsttCd", map);
 	}
 	
 	/**
 	 * 
 	 * <pre>
 	 * 1. 메소드명 : selectPgsStat
 	 * 2. 작성일 : 2021. 5. 4. 
 	 * 3. 작성자 : sjw7240
 	 * 4. 설명 : 공판요청지원 진행상태 조회
 	 * </pre>
 	 * @param map
 	 * @return
 	 * @throws Exception
 	 */
 	public String selectPgsStat(Map map) throws Exception{
 		return selectOne("tralPgsDAO.selectPgsStat", map);
 	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updateTestifySuppPgs
	 * 2. 작성일 : 2021. 5. 3.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판요청 수정
	 * </pre>
	 * @param paramMap
	 * @throws Exception
	 */
    public void updTralPgsUDtl(Map<String, Object> paramMap) throws Exception {
    	update("tralPgsDAO.updTralPgsUDtl", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : updateTralPgsPgsStat
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청 진행상태 변경
     * </pre>
     * @param paramMap
     * @throws Exception
     */
    public void updateTralPgsPgsStat(Map<String, Object> paramMap) throws Exception {
    	update("tralPgsDAO.updateTralPgsPgsStat", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : delTralPgsUDtl
     * 2. 작성일 : 2021. 5. 3. 
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청 삭제
     * </pre>
     * @param map
     * @throws Exception
     */
 	public void delTralPgsUDtl(Map<String, Object> paramMap) throws Exception{
 		update("tralPgsDAO.delTralPgsUDtl", paramMap);
 	}
}
