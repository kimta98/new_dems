package dems.tral.pgs.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import dems.tral.pgs.service.TralPgsService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import jdk.nashorn.internal.runtime.Undefined;

/**
 * <pre>
 * 1. 클래스명 : TralPgsServiceImpl.java
 * 2. 작성일 : 2021. 4. 28.
 * 3. 작성자 : sjw7240
 * 4. 설명 : 
 * </pre>
 */
@Service("tralPgsService")
public class TralPgsServiceImpl extends EgovAbstractServiceImpl implements TralPgsService{

	protected Logger log = LoggerFactory.getLogger(TralPgsServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="comDAO")
	private ComDAO comDAO;

	@Resource(name="tralPgsDAO")
	private TralPgsDAO tralPgsDAO;

	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralPgsList
	 * 2. 작성일 : 2021. 4. 28. 오후 12:52:32
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 리스트 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectTralPgsList(Map<String, Object> map) throws Exception {
		
		return tralPgsDAO.selectTralPgsList(map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralPgsListCnt
	 * 2. 작성일 : 2021. 4. 28. 오후 12:54:18
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 리스트 카운트
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@Override
	public int selectTralPgsListCnt(Map<String, Object> map) throws Exception {
		
		return tralPgsDAO.selectTralPgsListCnt(map);
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : insertTralPgs
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판요청 등록
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void insertTralPgs(Map<String, Object> map) throws Exception {

		Map<String,Object> paramMap = new HashMap<String,Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		validateUtil.check(chkParamMap, requireParams);

		//서버에서
		String userGb = (String) map.get("userGb");
		String regrId = (String) map.get("userId");
		
		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		String type = (String) paramMap.get("type");

		//권한이 일반수사관(C01001)
		if(!"C01001".equals(userGb)) {
			if(userGb.equals("C01999")) {
				
			}
			else {
				throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.000",new String[]{"일반수사관"}));
			}
			
		} 

        //사건이 존재하는지 체크
		paramMap.put("incdtSno"	, paramMap.get("incdtSno"));
		int chkIncdt =  tralPgsDAO.selectTralPgsIncdtChk(paramMap);
		if(chkIncdt < 1) {
			throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.001",new String[]{"해당 사건"}));
		}

		//공판요청일련번호 조회
		paramMap.put("incdtSno"	, paramMap.get("incdtSno"));
		String tstfReqSno = tralPgsDAO.selectTralPgsSno(paramMap);


		paramMap.clear();
		paramMap.put("tstfReqSno", tstfReqSno);
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));

		/*진행상태
		C03001:임시등록
		C03002:승인요청
		C03003:검사승인
		C03004:반려
		C03005:문서접수
		C03006:과장승인*/

		paramMap.put("pgsStat","C03001");
		paramMap.put("tstfReqSno", tstfReqSno);
		paramMap.put("regrId", regrId);
		tralPgsDAO.insertTestifySuppPgs(paramMap);

		ArrayList<Map<String, Object>> paramDetailList = (ArrayList<Map<String, Object>>) map.get("detailList");
		for(int i =0; i < paramDetailList.size(); i++) {
			paramMap.clear();
			paramMap.putAll(paramDetailList.get(i));
			paramMap.put("tstfReqSno", tstfReqSno);
			tralPgsDAO.insertTestifyEvdcDtl(paramMap);
		}
		
		//승인요청만 진행
		if("req".equals(type)) {
			paramMap.clear();
			paramMap.put("tstfReqSno", tstfReqSno);
			paramMap.put("pgsStat", "C03002");
			paramMap.put("type", type);
			paramMap.put("updrId", regrId);			
			tralPgsDAO.updateTralPgsPgsStat(paramMap);
		}
	}
	
	/**
    *
    * <pre>
    * 1. 메소드명 : updateTralPgs
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판요청 수정
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void updTralPgsUDtl(Map<String, Object> map) throws Exception {
		Map<String,Object> paramMap = new HashMap<String,Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		validateUtil.check(chkParamMap, requireParams);

		//서버에서
		String userGb = (String) map.get("userGb");
		String updrId = (String) map.get("userId");
		String regrId = (String)map.get("regrId");
		String type = (String)map.get("type");
		
		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		String tstfReqSno = (String) paramMap.get("tstfReqSno"); 
		

		//권한이 일반수사관(C01001)
		if(!"C01001".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.000",new String[]{"일반수사관"}));
		}

		//동일부서인지 체크	
		paramMap.clear();
		paramMap.put("tstfReqSno", tstfReqSno);
		String reqInsttCd = tralPgsDAO.selectPgsInsttCd(paramMap);
		if(!reqInsttCd.equals(map.get("insttCd"))) {
			throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.003",new String[]{tstfReqSno}));
		}		
		
		//진행상태 체크(임시저장:C03001)
		String pgsStat = tralPgsDAO.selectPgsStat(paramMap);
		if(!"C03001".equals(pgsStat)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.000",new String[]{"진행상태가 임시저장"}));
		}

		/*진행상태
		C03001:임시등록
		C03002:승인요청
		C03003:검사승인
		C03004:반려
		C03005:문서접수
		C03006:과장승인*/

		paramMap.clear();
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		paramMap.put("pgsStat","C03001");
		paramMap.put("updrId", updrId);
		tralPgsDAO.updTralPgsUDtl(paramMap);
		
		//승인요청만 진행
		if("req".equals(type)) {
			paramMap.clear();
			paramMap.put("tstfReqSno", tstfReqSno);
			paramMap.put("pgsStat", "C03002");
			paramMap.put("type", type);
			paramMap.put("updrId", regrId);			
			tralPgsDAO.updateTralPgsPgsStat(paramMap);
		}
		
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : delTralPgsUDtl
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판요청 삭제(승인 취소)
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void delTralPgsUDtl(Map<String, Object> map) throws Exception {
		Map<String,Object> paramMap = new HashMap<String,Object>();

		String type = (String)map.get("type");
		String tstfReqSno = (String)map.get("tstfReqSno");
		String regrId = (String)map.get("regrId");
		String pgsStat = (String)map.get("pgsStat");
		
		paramMap.clear();
		paramMap.put("tstfReqSno", tstfReqSno);
		paramMap.put("regrId", regrId);
		paramMap.put("useYn", "N");
		paramMap.put("type", type);

		//승인취소
		if(type.equals("aprvCncl")){
			paramMap.put("aprvDt", null);
			paramMap.put("aprvrId", null);
			paramMap.put("pgsStat", pgsStat);
			tralPgsDAO.delTralPgsUDtl(paramMap);
			
		//검사승인취소
		} else if(type.equals("prosrAprvCncl")) {
			paramMap.put("prosrId", null);
			paramMap.put("prosrAprvDt", null);
			paramMap.put("pgsStat", pgsStat);
			tralPgsDAO.delTralPgsUDtl(paramMap);
			
			//검사승인요청취소
			//요청자ID는 notnull 이므로 조심
		} else if(type.equals("reqCncl")) {
			paramMap.put("reqDt", null);
			paramMap.put("pgsStat", pgsStat);
			tralPgsDAO.delTralPgsUDtl(paramMap);

			//접수취소
		} else if(type.equals("rcvCncl")) {
			paramMap.put("rcvDt", null);
			paramMap.put("rcvrId", null);
			paramMap.put("pgsStat", pgsStat);
			tralPgsDAO.delTralPgsUDtl(paramMap);

		}
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : indexTralPgsUDtl
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판요청 상세 진입(조회)
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public List indexTralPgsUDtl(Map<String, Object> map) throws Exception {
		
		return tralPgsDAO.indexTralPgsUDtl(map);
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : queryPgsStat
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판 관리 진행상태 변경(승인요청,승인취소,검사승인,검사승인취소)
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void queryPgsStat(Map<String, Object> map) throws Exception {
		
		List aprvList = (List) map.get("aprvList");
		String userGb = (String) map.get("userGb");
		String insttCd = (String) map.get("insttCd");
		String userId = (String) map.get("userId");
		String incdtSno = "";
		
		
		Map<String, Object> infoMap = new HashMap<String, Object>();
		String type = (String)aprvList.get(aprvList.size()-2);
		
		if((map.get("incdtSno") != null) && !"tstf".equals(type)) {
			incdtSno = (String)map.get("incdtSno");
		} else if((map.get("insttCd") != null) && !"tstf".equals(type)) {
			incdtSno = (String)map.get("insttCd");
		}
		
		//요청관리에서 넘어오는 상태 : 승인요청,승인취소,검사승인,검사승인취소만 처리
		if(!( "req".equals(type) || "reqCncl".equals(type) || "prosrAprv".equals(type) || "prosrAprvCncl".equals(type) || "rcv".equals(type) || "rcvCncl".equals(type) || "aprv".equals(type) || "aprvCncl".equals(type) || "tstf".equals(type))) {
			throw new MException(egovMessageSource.getMessage("dems.anls.req.005"));
		} 
		
		switch (type) {
		case "req"://승인요청
			infoMap.put("type", type);
			infoMap.put("pgsStat", "C03002");
			infoMap.put("updrId", userId);
			break;
		case "reqCncl": //승인요청취소(임시저장으로변경)
			infoMap.put("type", type);
			infoMap.put("pgsStat", "C03001");
			infoMap.put("updrId", userId);	
			break;
		case "prosrAprv"://검사승인
			infoMap.put("type", type);
			infoMap.put("prosrId", userId);
			infoMap.put("pgsStat", "C03003");
			infoMap.put("updrId", userId);			
			break;
		case "prosrAprvCncl"://검사승인취소(승인요청으로변경)
			infoMap.put("type", type);
			infoMap.put("pgsStat", "C03002");
			infoMap.put("updrId", userId);
			infoMap.put("prosrId", userId);
			break;
		case "rcv":
			infoMap.put("type", type);
			infoMap.put("rcvrId", userId);
			infoMap.put("pgsStat", "C03005");
			infoMap.put("updrId", userId);	
			break;
		case "rcvCncl":
			infoMap.put("type", type);
			infoMap.put("rcvrId", userId);
			infoMap.put("pgsStat", "C03003");
			infoMap.put("updrId", userId);	
			break;
		case "aprv":
			infoMap.put("type", type);
			infoMap.put("aprvrId", userId);
			infoMap.put("pgsStat", "C03006");
			infoMap.put("updrId", userId);	
			break;
		case "aprvCncl":
			infoMap.put("type", type);
			infoMap.put("aprvrId", userId);
			infoMap.put("pgsStat", "C03005");
			infoMap.put("updrId", userId);	
			break;
		case "tstf":
			infoMap.put("type", type);
			infoMap.put("tstfrId", null);
			infoMap.put("updrId", userId);	
			infoMap.put("pgsStat", "C03006");
			break;
		}
		
		//권한체크
		if(("req".equals(type) || "reqCncl".equals(type)) && !"C01001".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"일반수사관"}));
		}
		
		if(("prosrAprv".equals(type) || "prosrAprvCncl".equals(type)) && !"C01002".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"군검사"}));
		}
		
		if(("rcv".equals(type) || "rcvCncl".equals(type)) && !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식수사관"}));
		}
		
		if(("aprv".equals(type) || "aprvCncl".equals(type)) && !"C01004".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식수사과장"}));
		}


		
		
		int chkAprv = 0;
		String reqInsttCd = "";
		String pgsStat = "";
		String rcvNo = "";
		Map<String, Object> paramMap = new HashMap<String, Object>();	
		
		//요청자료 유무 체크
		for(int i = 0; i < aprvList.size()-2; i += 2) {
			paramMap.clear();
			paramMap.putAll(infoMap);
			paramMap.put("incdtSno", aprvList.get(i+1));
			
			
			chkAprv = tralPgsDAO.selectTralPgsIncdtChk(paramMap);
			if(chkAprv < 1 && !"tstf".equals(type)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.002",new String[]{(String) aprvList.get(1)}));
			}

				paramMap.clear();
				paramMap.putAll(infoMap);
				paramMap.put("tstfReqSno", aprvList.get(i));
				
				if(tralPgsDAO.selectPgsInsttCd(paramMap) == null) {
					reqInsttCd = "";
				} else {
					reqInsttCd = tralPgsDAO.selectPgsInsttCd(paramMap);
				}
			if(!"tstf".equals(type)) {
				
				if(!reqInsttCd.equals(insttCd) && !(userGb.equals("C01003") || userGb.equals("C01004"))) {
					throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.003",new String[]{(String) aprvList.get(0)}));
				}
				
				//진행상태 체크
				pgsStat = tralPgsDAO.selectPgsStat(paramMap);
			}
			
			//승인요청     req           -> 임시저장
			//승인요청취소 reqCanel      -> 승인요청
			//검사승인     prosrAprv      -> 승인요청
			//검사승인취소 prosrAprvCanel -> 검사승인
			//선별확인     selCfrm        -> 선별요청
			//선별확인취소 selCfrmCncl    -> 선별확인
			
			if("req".equals(type) && !"C03001".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(0),"임시저장"}));
			}
			
			if(("reqCncl".equals(type) || "prosrAprv".equals(type) )&& !"C03002".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(0),"승인요청"}));
			}			
			
			if("prosrAprvCncl".equals(type) && !"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(0),"검사승인"}));
			}
			
			if("rcv".equals(type) && !"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(0),"접수"}));
			}		
			
			if("rcvCncl".equals(type) && !"C03005".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(0),"접수취소"}));
			}
			
			if("aprv".equals(type) && !"C03005".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(0),"승인"}));
			}			
			
			if("aprvCncl".equals(type) && !"C03006".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) aprvList.get(0),"승인취소"}));
			}

			
		}		

		//진행상태 변경
		if("tstf".equals(type)) {
			paramMap.clear();
			paramMap.put("tstfReqSno", aprvList.get(0));
			paramMap.put("incdtSno", aprvList.get(1));
			paramMap.put("tstfDt", aprvList.get(2));
			paramMap.put("tstfPlace", aprvList.get(3));
			paramMap.put("tstfrId", aprvList.get(4));
			paramMap.put("realTstfCnts", aprvList.get(5));
			paramMap.put("type", aprvList.get(6));
			paramMap.put("updrId", aprvList.get(7));
			paramMap.put("pgsStat", "C03006");
			tralPgsDAO.updateTralPgsPgsStat(paramMap);
			
		} else {
			for(int i = 0; i < aprvList.size()-2; i++) {
				
				paramMap.clear();
				paramMap.putAll(infoMap);
				paramMap.put("tstfReqSno", aprvList.get(i));
				rcvNo = tralPgsDAO.selectRcvNo(paramMap);
				paramMap.put("rcvNo", Integer.parseInt(rcvNo));
				
				tralPgsDAO.updateTralPgsPgsStat(paramMap);
				
			}
		}
		
	}

	

}
