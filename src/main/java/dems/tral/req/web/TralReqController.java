package dems.tral.req.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.tral.req.service.TralReqService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.fexception.MException;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.DemsConst;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;

/**
 * <pre>
 * 1. 클래스명 : TralReqController.java
 * 2. 작성일 : 2021. 4. 28.
 * 3. 작성자 : sjw7240
 * 4. 설명 : 공판관리 요청관리 Controller
 * </pre>
 */
@Controller
public class TralReqController {

	protected Logger log = LoggerFactory.getLogger(TralReqController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "tralReqService")
	private TralReqService tralReqService;
    
    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;


    /**
     * 
     * <pre>
     * 1. 메소드명 : indexTralReqMList
     * 2. 작성일 : 2021. 4. 28.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판관리_요청관리 목록_진입
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/tral/req/indexTralReqMList.do")
    public String indexTralReqMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

    	String pgs = (String)map.get("pgs");
    	String url = "";
    	url = "dems/tral/req/tralReqMList";
    	
    	if("pgs".equals(pgs)) {
    		url = "dems/tral/pgs/tralPgsMList";
    	}
        return url;
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryTralReqMList
     * 2. 작성일 : 2021. 4. 28.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판관리_요청관리 목록_조회
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/tral/req/queryTralReqMList.do")
    public ModelAndView queryTralReqMList(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {
    	
       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	reqParams.put("userGb", user.getUserGb());
    	reqParams.put("regrId", user.getUserId());
    	reqParams.put("insttCd", user.getInsttCd());
    	reqParams.put("tral", "req");
        reqParams.put("srcPgsStat", ((String) reqParams.get("srcPgsStat")).split(","));
    	
    	int totCnt = tralReqService.selectTralReqListCnt(reqParams);
    	reqParams.put("totalCount", totCnt);
    	PageUtil.calcPage(reqParams);

    	List tralReqList = tralReqService.selectTralReqList(reqParams);		
		reqParams.put("tralReqList", tralReqList);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);
    	   	
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexTralReqRDtl
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청관리 등록 호출
     * </pre>
     * @param request
     * @param model
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/tral/req/indexTralReqRDtl.do")
    public String indexTralReqRDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap<String,String> map) throws Exception {

       FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
 	   
 	   model.addAttribute("reqInsttNm", user.getInsttNm());    //요청기관명
 	   model.addAttribute("reqDepNm", user.getDeptNm());      //요청부서명
 	   model.addAttribute("reqInsttCd", user.getInsttCd());  //요청기관코드
 	   
        return "dems/tral/req/tralReqRDtl";
    }

    /**
     * 
     * <pre>
     * 1. 메소드명 : tstReqTralReqRDtl
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청관리 증언 요청(등록)
     * </pre>
     * @param request
     * @param model
     * @param paramData
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/tral/req/tstReqTralReqRDtl.do")
    public ModelAndView tstReqTralReqRDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

    	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	ObjectMapper mapper = new ObjectMapper();
    	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
    	Map<String, Map<String, String>> jsonObject2 = mapper.readValue(paramData, Map.class);

    	//증인 정보
    	Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");
        HashMap<String,String> chkParams = (HashMap<String, String>)jsonObject2.get("formDatas");

    	Map<String, Object> paramMap = new HashMap<String, Object>();
    	paramMap.put("formDataMap", formDataMap);
    	paramMap.put("userGb", user.getUserGb());
    	paramMap.put("userId", user.getUserId());

    	//체크박스 체크
        int reqSuppCnt = 0;
        
        String supp[] = {"suppDisk", "suppServer", "suppMobile", "suppData", "suppCallDtl", "suppCallDtl", "suppEtc"};
        
        for(int i = 0; i < supp.length; i++) {
        	if((String)formDataMap.get(supp[i]) != null && ((String)formDataMap.get(supp[i])).equals("Y")) {
        		reqSuppCnt++;
        	}
        }

        if (reqSuppCnt <= 0) {
        	throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.004", null));
        } 

        //필수값 확인
        HashMap<String,String> requireParams = new HashMap();
        requireParams.put("reqDt", "요청일자");
        requireParams.put("reqInsttCd", "요청기관코드");
        requireParams.put("reqUserId", "요청자ID");
        requireParams.put("incdtSno", "사건일련번호");
        requireParams.put("prosrId", "주임군검사ID");
        requireParams.put("relIncdt", "관련사건");
        requireParams.put("tstfReqCnts", "증언요청내용");
        validateUtil.check(chkParams, requireParams);
        
    	tralReqService.insertTralReq(paramMap);
        
        reqSuppCnt = 0;


        return new ModelAndView(ajaxMainView, model);

    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : queryTralReqPop
     * 2. 작성일 : 2021. 5. 4.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청 팝업 호출
     * </pre>
     * @param request
     * @param model
     * @param reqParams
     * @param response
     * @return
     * @throws Exception
     */
   @RequestMapping(value="/tral/req/queryTralReqPop.do")
   public ModelAndView queryTralReqPop(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams, HttpServletResponse response) throws Exception {

	    FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	   	reqParams.put("regrId", user.getUserId());
	   	reqParams.put("insttCd", user.getInsttCd());
	   	
	   	String type = (String) reqParams.get("type");
	   	String userGb = "";
	   	
	   	if(type.equals("prosr")) {
	   		userGb = "C01002";
	   	}else if(type.equals("regUser")) {
	   		userGb = "C01001";
	   		if(user.getUserGb().equals("C01003")) {
	   			userGb = "C01003";
	   		}
	   	}else if(type.equals("tralCrgr")) {
	   		userGb = "C01003";
	   	}else if(type.equals("incdt")) {
	   		userGb = user.getUserGb();
	   	}
	   	
	   	switch (type) {
		case "incdt": //사건			
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "tralReqDAO.selectIncidentListCnt");
			break;
		case "prosr": //검사
		case "regUser": //담당자
		case "tralCrgr": //증언담당자
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "tralReqDAO.selectCrgrListCnt");
			break;
        case "tstf":
            reqParams.put("sqlQueryId", "tralReqDAO.selectTstfListCnt");
            break;

		}
	   	int totCnt = comService.selectCommonQueryListTotCnt(reqParams);
	   	reqParams.put("totalCount", totCnt);
	   	
	   	PageUtil.calcPage(reqParams);
	   	
	   	switch (type) {
		case "incdt": //사건			
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "tralReqDAO.selectIncidentList");
			break;
		case "prosr": //검사
		case "regUser": //담당자
		case "tralCrgr": //증언담당자
			reqParams.put("userGb",userGb);
			reqParams.put("sqlQueryId", "tralReqDAO.selectCrgrList");
			break;
        case "tstf":
            reqParams.put("sqlQueryId", "tralReqDAO.selectTstfList");
            break;

		}
	   	
	   	List popList = comService.selectCommonQueryList(reqParams);		
		reqParams.put("popList", popList);
		model.addAllAttributes(reqParams);
		
		return new ModelAndView(ajaxMainView, model);

   }
   
   /**
    * 
    * <pre>
    * 1. 메소드명 : reqTralReqRDtlSno
    * 2. 작성일 : 2021. 5. 6. 
    * 3. 작성자 : sjw7240
    * 4. 설명 : 증언요청 최신번호 가져오기
    * </pre>
    * @param request
    * @param model
    * @param paramData
    * @param response
    * @return
    * @throws Exception
    */
   @RequestMapping(value="/tral/req/reqTralReqRDtlSno.do")
   public ModelAndView reqTralReqRDtlSno(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

   	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

   	ObjectMapper mapper = new ObjectMapper();
   	Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);

   	//증인 상세정보
   	ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
   	//증인 정보
   	Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");

   	Map<String, Object> paramMap = new HashMap<String, Object>();
   	paramMap.put("formDataMap", formDataMap);
   	paramMap.put("detailList", tpList);
   	paramMap.put("userGb", user.getUserGb());
   	paramMap.put("userId", user.getUserId());

   	tralReqService.insertTralReq(paramMap);

       return new ModelAndView(ajaxMainView, model);

   }

   /**
    * 
    * <pre>
    * 1. 메소드명 : indexTralReqUDtl
    * 2. 작성일 : 2021. 5. 6.
    * 3. 작성자 : sjw7240
    * 4. 설명 : 공판지원요청 상세 진입
    * </pre>
    * @param request
    * @param model
    * @param reqParams
    * @return
    * @throws Exception
    */
  @RequestMapping(value="/tral/req/indexTralReqUDtl.do")
  public String indexTralReqUDtl(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {
    	 
    	return "dems/tral/req/tralReqUDtl";
  }

  /**
   * 
   * <pre>
   * 1. 메소드명 : queryTralReqInfo
   * 2. 작성일 : 2021. 5. 6.
   * 3. 작성자 : sjw7240
   * 4. 설명 : 공판지원요청 상세 호출
   * </pre>
   * @param request
   * @param model
   * @param reqParams
   * @return
   * @throws Exception
   */
  @RequestMapping(value="/tral/req/queryTralReqInfo.do")
  public ModelAndView queryTralReqInfo(HttpServletRequest request, ModelMap model, @RequestParam HashMap reqParams) throws Exception {

	 	reqParams.put("sqlQueryId", "tralReqDAO.selectTralReqInfo");
	 	Map tralReqInfoMap = comService.selectCommonQueryMap(reqParams);
	 	reqParams.put("tralReqInfoMap", tralReqInfoMap);
	 	
	 	reqParams.put("sqlQueryId", "tralReqDAO.selectTralReqInfoChk");
	 	Map tralReqInfoChkMap = comService.selectCommonQueryMap(reqParams);
	 	reqParams.put("tralReqInfoChkMap", tralReqInfoChkMap);
  	
 	model.addAllAttributes(reqParams); 	
 	 
 	return new ModelAndView(ajaxMainView, model);
  }


  /**
   * 
   * <pre>
   * 1. 메소드명 : updTralReqUDtl
   * 2. 작성일 : 2021. 5. 7. 
   * 3. 작성자 : sjw7240
   * 4. 설명 : 공판지원요청 수정
   * </pre>
   * @param request
   * @param model
   * @param paramData
   * @param response
   * @return
   * @throws Exception
   */
    @RequestMapping(value="/tral/req/updTralReqUDtl.do")
    public ModelAndView updTralReqUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

        FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
        Map<String, Map<String, String>> jsonObject2 = mapper.readValue(paramData, Map.class);

        //증인 상세정보
        ArrayList<Map<String, Object>> tpList = (ArrayList<Map<String, Object>>) jsonObject.get("rowDatas");
        //증인 정보
        Map<String, Object> formDataMap = (Map<String, Object>) jsonObject.get("formDatas");
        HashMap<String,String> chkParams = (HashMap<String, String>)jsonObject2.get("formDatas");

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("formDataMap", formDataMap);
        paramMap.put("detailList", tpList);
        paramMap.put("userGb", user.getUserGb());
        paramMap.put("userId", user.getUserId());
        paramMap.put("insttCd", user.getInsttCd());

        //체크박스 체크
        int reqSuppCnt = 0;
        
        String supp[] = {"suppDisk", "suppServer", "suppMobile", "suppData", "suppCallDtl", "suppCallDtl", "suppEtc"};
        
        for(int i = 0; i < supp.length; i++) {
            if((String)formDataMap.get(supp[i]) != null && ((String)formDataMap.get(supp[i])).equals("Y")) {
                reqSuppCnt++;
            }
        }

        if(reqSuppCnt <= 0) {
        	throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.004", null));
        }
        
        //필수값 확인
        HashMap<String,String> requireParams = new HashMap();
        requireParams.put("reqDt", "요청일자");
        requireParams.put("reqInsttCd", "요청기관코드");
        requireParams.put("reqUserId", "요청자ID");
        requireParams.put("incdtSno", "사건일련번호");
        requireParams.put("prosrId", "주임군검사ID");
        requireParams.put("relIncdt", "관련사건");
        requireParams.put("tstfReqCnts", "증언요청내용");
        validateUtil.check(chkParams, requireParams);

        tralReqService.updTralReqUDtl(paramMap);

        return new ModelAndView(ajaxMainView, model);

    }

  /**
   * 
   * <pre>
   * 1. 메소드명 : delTralReqUDtl
   * 2. 작성일 : 2021. 5. 10. 
   * 3. 작성자 : sjw7240
   * 4. 설명 : 공판지원요청 삭제(승인취소)
   * </pre>
   * @param request
   * @param model
   * @param paramData
   * @param response
   * @return
   * @throws Exception
   */
  @RequestMapping(value="/tral/req/delTralReqUDtl.do")
  public ModelAndView delTralReqUDtl(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

	   FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
	
	   ObjectMapper mapper = new ObjectMapper();
	   Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
	   Map<String, Map<String, String>> jsonString = mapper.readValue(paramData, Map.class);
	    
	    //타입 가져오기
	    HashMap<String,String> typeMap =  (HashMap<String, String>)jsonString.get("type");
	    String type = typeMap.get("type");

	    //요청 리스트
	    List tpList = (List) jsonObject.get("rowDatas");    
	   
       Map<String, Object> paramMap = new HashMap<String, Object>();
       
       paramMap.clear();
       paramMap.put("updrId", user.getUserId());
       paramMap.put("userGb", user.getUserGb());
       paramMap.put("type", type);
       paramMap.put("userId", user.getUserId());
       paramMap.put("insttCd", user.getInsttCd());
       paramMap.put("tpList", tpList);
       switch(type) {
	       case "prosrAprvCncl":
			   paramMap.put("pgsStat", "C03002");
	    	   break;
	       case "reqCncl":
			   paramMap.put("pgsStat", "C03001");
	    	   break;
	       case "rcvCncl":
	           paramMap.put("pgsStat", "C03003");
	    	   break;
	       case "AprvCncl":
	           paramMap.put("pgsStat", "C03005");
	    	   break;
       }
       tralReqService.delTralReqUDtl(paramMap);
	   return new ModelAndView(ajaxMainView, model);
  }

  /**
   * 
   * <pre>
   * 1. 메소드명 : queryPgsStat
   * 2. 작성일 : 2021. 5. 11.
   * 3. 작성자 : sjw7240
   * 4. 설명 : 
   * </pre>
   * @param request
   * @param model
   * @param paramData
   * @param response
   * @return
   * @throws Exception
   */
  @RequestMapping(value="/tral/req/queryPgsStat.do")
  public ModelAndView queryPgsStat(HttpServletRequest request, ModelMap model, @RequestBody String paramData, HttpServletResponse response) throws Exception {

    FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    ObjectMapper mapper = new ObjectMapper();
    Map<String, Map<String, Object>> jsonObject = mapper.readValue(paramData, Map.class);
    Map<String, Map<String, String>> jsonString = mapper.readValue(paramData, Map.class);
    
    //타입 가져오기
    HashMap<String,String> typeMap =  (HashMap<String, String>)jsonString.get("type");
    String type = typeMap.get("type");

    //요청 리스트
    List tpList = (List) jsonObject.get("rowDatas");    

    Map<String, Object> paramMap = new HashMap<String, Object>();
    //필수값 확인
    if("tstf".equals(type)) {
        HashMap<String,String> requireParams = new HashMap();
        HashMap<String,String> chkParams = new HashMap();
        chkParams.putAll(typeMap);
        requireParams.put("tstfrId", "증언자ID");
        requireParams.put("tstfDt", "증언일시");
        requireParams.put("tstfPlace", "증언장소");
        requireParams.put("realTstfCnts", "실증언내용");
        validateUtil.check(chkParams, requireParams);
        
        paramMap.putAll(typeMap);
    }

    //요청 타입(승인요청,승인요청취소,검사승인,검사승인취소,선별확인,선별확인취소)

    paramMap.put("tpList", tpList);
    paramMap.put("userGb", user.getUserGb());
    paramMap.put("type", type);
    paramMap.put("userId", user.getUserId());
    paramMap.put("insttCd", user.getInsttCd());

    tralReqService.queryPgsStat(paramMap);
    
    return new ModelAndView(ajaxMainView, model);
}
  
  
}
