package dems.tral.req.service;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 1. 클래스명 : TralReqService.java
 * 2. 작성일 : 2021. 4. 28.
 * 3. 작성자 : sjw7240
 * 4. 설명 : 
 * </pre>
 */
public interface TralReqService {
	
	List selectTralReqList(Map<String,Object> map) throws Exception;
	
	List indexTralReqUDtl(Map<String,Object> map) throws Exception;
	
	int selectTralReqListCnt(Map<String,Object> map) throws Exception;
	
	void queryPgsStat(Map<String,Object> map) throws Exception;	
	
	void insertTralReq(Map<String,Object> map) throws Exception;

	void updTralReqUDtl(Map<String,Object> map) throws Exception;
	
	void delTralReqUDtl(Map<String,Object>map) throws Exception;
}
