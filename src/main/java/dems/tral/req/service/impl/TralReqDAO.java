package dems.tral.req.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("tralReqDAO")
public class TralReqDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(TralReqDAO.class);

	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralReqList
	 * 2. 작성일 : 2021. 4. 28. 
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 리스트 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List selectTralReqList(Map map) throws Exception{
		return selectList("tralReqDAO.selectTralReqList", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : indexTralReqUDtl
	 * 2. 작성일 : 2021. 5. 6.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 상세 조회(진입)
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List indexTralReqUDtl(Map map) throws Exception{
		return selectList("tralReqDAO.indexTralReqUDtl", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralReqListCnt
	 * 2. 작성일 : 2021. 4. 28. 
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 리스트 카운트
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int selectTralReqListCnt(Map map) throws Exception{
		return selectOne("tralReqDAO.selectTralReqListCnt", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : tstReqTralReqRDtl
	 * 2. 작성일 : 2021. 5. 3.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판지원요청 insert
	 * </pre>
	 * @param paramMap
	 * @throws Exception
	 */
    public void tstReqTralReqRDtl(Map<String, Object> paramMap) throws Exception {

    	insert("tralReqDAO.tstReqTralReqRDtl", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : insertTestifyEvdcDtl
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청상세 insert
     * </pre>
     * @param paramMap
     * @throws Exception
     */
    public void insertTestifyEvdcDtl(Map<String, Object> paramMap) throws Exception {

    	 	insert("tralReqDAO.insertTestifyEvdcDtl", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : selectTralReqIncdtChk
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판 관리 사건 우뮤 체크
     * </pre>
     * @param map
     * @return
     * @throws Exception
     */
	public int selectTralReqIncdtChk(Map map) throws Exception{
		return selectOne("tralReqDAO.selectTralReqIncdtChk", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralReqSno
	 * 2. 작성일 : 2021. 5. 3.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판요청 일련번호 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public String selectTralReqSno(Map map) throws Exception{
		return selectOne("tralReqDAO.selectTralReqSno", map);
	}

	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectRcvNo
	 * 2. 작성일 : 2021. 5. 3.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판요청 접수번호 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public String selectRcvNo(Map map) throws Exception{
		return selectOne("tralReqDAO.selectRcvNo", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralReqUserGb
	 * 2. 작성일 : 2021. 5. 3
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판요청 사용자그룹 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public String selectTralReqUserGb(Map map) throws Exception{
		return (String)selectOne("tralReqDAO.selectTralReqUserGb", map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectReqInsttCd
	 * 2. 작성일 : 2021. 5. 4. 
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 요청기관코드 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
 	public String selectReqInsttCd(Map map) throws Exception{
 		return selectOne("tralReqDAO.selectReqInsttCd", map);
 	}
 	
 	/**
 	 * 
 	 * <pre>
 	 * 1. 메소드명 : selectPgsStat
 	 * 2. 작성일 : 2021. 5. 4. 
 	 * 3. 작성자 : sjw7240
 	 * 4. 설명 : 공판요청지원 진행상태 조회
 	 * </pre>
 	 * @param map
 	 * @return
 	 * @throws Exception
 	 */
 	public String selectPgsStat(Map map) throws Exception{
 		return selectOne("tralReqDAO.selectPgsStat", map);
 	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : updateTestifySuppReq
	 * 2. 작성일 : 2021. 5. 3.
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판요청 수정
	 * </pre>
	 * @param paramMap
	 * @throws Exception
	 */
    public void updTralReqUDtl(Map<String, Object> paramMap) throws Exception {
    	update("tralReqDAO.updTralReqUDtl", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : updateTralReqPgsStat
     * 2. 작성일 : 2021. 5. 3.
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청 진행상태 변경
     * </pre>
     * @param paramMap
     * @throws Exception
     */
    public void updateTralReqPgsStat(Map<String, Object> paramMap) throws Exception {
    	update("tralReqDAO.updateTralReqPgsStat", paramMap);
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : delTralReqUDtl
     * 2. 작성일 : 2021. 5. 3. 
     * 3. 작성자 : sjw7240
     * 4. 설명 : 공판요청 삭제
     * </pre>
     * @param map
     * @throws Exception
     */
 	public void delTralReqUDtl(Map<String, Object> paramMap) throws Exception{
 		update("tralReqDAO.delTralReqUDtl", paramMap);
 	}
}
