package dems.tral.req.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import dems.tral.req.service.TralReqService;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import frame.fcom.service.ComService;
import frame.fcom.service.impl.ComDAO;
import frame.fexception.MException;
import frame.futil.ValidateUtil;
import jdk.nashorn.internal.runtime.Undefined;

/**
 * <pre>
 * 1. 클래스명 : TralReqServiceImpl.java
 * 2. 작성일 : 2021. 4. 28.
 * 3. 작성자 : sjw7240
 * 4. 설명 : 
 * </pre>
 */
@Service("tralReqService")
public class TralReqServiceImpl extends EgovAbstractServiceImpl implements TralReqService{

	protected Logger log = LoggerFactory.getLogger(TralReqServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="comDAO")
	private ComDAO comDAO;

	@Resource(name="tralReqDAO")
	private TralReqDAO tralReqDAO;

	@Resource(name = "validateUtil")
	private ValidateUtil validateUtil;

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralReqList
	 * 2. 작성일 : 2021. 4. 28. 오후 12:52:32
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 리스트 조회
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectTralReqList(Map<String, Object> map) throws Exception {
		
		return tralReqDAO.selectTralReqList(map);
	}
	
	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : selectTralReqListCnt
	 * 2. 작성일 : 2021. 4. 28. 오후 12:54:18
	 * 3. 작성자 : sjw7240
	 * 4. 설명 : 공판관리요청관리 리스트 카운트
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@Override
	public int selectTralReqListCnt(Map<String, Object> map) throws Exception {
		
		return tralReqDAO.selectTralReqListCnt(map);
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : insertTralReq
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판요청 등록
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void insertTralReq(Map<String, Object> map) throws Exception {

		Map<String,Object> paramMap = new HashMap<String,Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		validateUtil.check(chkParamMap, requireParams);

		//서버에서
		String userGb = (String) map.get("userGb");
		String regrId = (String) map.get("userId");
		
		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		String type = (String) paramMap.get("type");

		//권한이 일반수사관(C01001)
		if(!"C01001".equals(userGb)) {
			if(userGb.equals("C01999") || userGb.equals("C01003")) {
				
			}
			else {
				throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.000",new String[]{"일반수사관"}));
			}
			
		} 

        //사건이 존재하는지 체크
		paramMap.put("incdtSno"	, paramMap.get("incdtSno"));
		int chkIncdt =  tralReqDAO.selectTralReqIncdtChk(paramMap);
		if(chkIncdt < 1) {
			throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.001",new String[]{"해당 사건"}));
		}

		//공판요청일련번호 조회
		paramMap.put("incdtSno"	, paramMap.get("incdtSno"));
		String tstfReqSno = tralReqDAO.selectTralReqSno(paramMap);


		paramMap.clear();
		paramMap.put("tstfReqSno", tstfReqSno);
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));

		/*진행상태
		C03001:임시등록
		C03002:승인요청
		C03003:검사승인
		C03004:반려
		C03005:문서접수
		C03006:과장승인*/
		if(userGb.equals("C01003")) {
			paramMap.put("pgsStat","C03003");
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			paramMap.put("pgsStat","C03003");
			paramMap.put("prosrAprvDt", format.format(new Date()));
			paramMap.put("reqDt", format.format(new Date()));
		}else {
			paramMap.put("pgsStat","C03001");
		}
		paramMap.put("tstfReqSno", tstfReqSno);
		paramMap.put("regrId", regrId);
		tralReqDAO.tstReqTralReqRDtl(paramMap);
		
		//승인요청만 진행
		if("req".equals(type)) {
			paramMap.clear();
			paramMap.put("tstfReqSno", tstfReqSno);
			paramMap.put("pgsStat", "C03002");
			paramMap.put("type", type);
			paramMap.put("updrId", regrId);			
			tralReqDAO.updateTralReqPgsStat(paramMap);
		}
	}
	
	/**
    *
    * <pre>
    * 1. 메소드명 : updateTralReq
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판요청 수정
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void updTralReqUDtl(Map<String, Object> map) throws Exception {
		Map<String,Object> paramMap = new HashMap<String,Object>();

		//폼필수값체크
		HashMap<String,String> requireParams = new HashMap();
		HashMap<String,String> chkParamMap = new HashMap();
		//requireParams.put("cdIdNm", "코드명||12"); // 메시지 명 , maxlength
		validateUtil.check(chkParamMap, requireParams);

		//서버에서
		String userGb = (String) map.get("userGb");
		String updrId = (String) map.get("userId");
		String regrId = (String)map.get("regrId");
		String type = (String)map.get("type");
		
		//화면에서
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		String tstfReqSno = (String) paramMap.get("tstfReqSno"); 
		

		//권한이 일반수사관(C01001)
		if(!"C01001".equals(userGb) && !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.000",new String[]{"일반수사관"}));
		}

		//동일부서인지 체크	
		paramMap.clear();
		paramMap.put("tstfReqSno", tstfReqSno);
		String reqInsttCd = tralReqDAO.selectReqInsttCd(paramMap);
		if(!reqInsttCd.equals(map.get("insttCd"))) {
			throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.003",new String[]{tstfReqSno}));
		}		
		
		//진행상태 체크(임시저장:C03001)
		String pgsStat = tralReqDAO.selectPgsStat(paramMap);
		if(userGb.equals("C01003")) {
			if(!"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.000",new String[]{"진행상태가 검사승인"}));
			}
		}else {
			if(!"C03001".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.tral.req.000",new String[]{"진행상태가 임시저장"}));
			}
		}

		/*진행상태
		C03001:임시등록
		C03002:승인요청
		C03003:검사승인
		C03004:반려
		C03005:문서접수
		C03006:과장승인*/

		paramMap.clear();
		paramMap.putAll((Map<? extends String, ? extends Object>) map.get("formDataMap"));
		if(userGb.equals("C01003")) {
			paramMap.put("pgsStat","C03003");
			paramMap.put("updrId", regrId);
		}else {			
			paramMap.put("pgsStat","C03001");
		}
		paramMap.put("updrId", updrId);
		tralReqDAO.updTralReqUDtl(paramMap);
		
		//승인요청만 진행
		if("req".equals(type)) {
			paramMap.clear();
			paramMap.put("tstfReqSno", tstfReqSno);
			paramMap.put("pgsStat", "C03002");
			paramMap.put("type", type);
			paramMap.put("updrId", regrId);			
			tralReqDAO.updateTralReqPgsStat(paramMap);
		}
		
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : delTralReqUDtl
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판요청 삭제(승인 취소)
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public void delTralReqUDtl(Map<String, Object> map) throws Exception {
		Map<String,Object> paramMap = new HashMap<String,Object>();

		String type = (String)map.get("type");
		String regrId = (String)map.get("regrId");
		String pgsStat = (String)map.get("pgsStat");
		String updrId = (String) map.get("userId");
		List tstfReqSnoList = (List) map.get("tpList");
		
		for(int i =0; i < tstfReqSnoList.size(); i++) {
			paramMap.clear();
			paramMap.put("tstfReqSno", tstfReqSnoList.get(i));
			paramMap.put("updrId", updrId);
			paramMap.put("type", type);
			//승인취소
			if(type.equals("aprvCncl")){
				paramMap.put("aprvrId", null);
				paramMap.put("pgsStat", "C03005");
				tralReqDAO.delTralReqUDtl(paramMap);
				
			//검사승인취소
			} else if(type.equals("prosrAprvCncl")) {
				paramMap.put("pgsStat", "C03002");
				tralReqDAO.delTralReqUDtl(paramMap);
				
				//검사승인요청취소
				//요청자ID는 notnull 이므로 조심
			} else if(type.equals("reqCncl")) {
				paramMap.put("pgsStat", "C03001");
				tralReqDAO.delTralReqUDtl(paramMap);

				//접수취소
			} else if(type.equals("rcvCncl")) {
				paramMap.put("rcvrId", null);
				paramMap.put("pgsStat", "C03003");
				tralReqDAO.delTralReqUDtl(paramMap);

			}
		}

	}

	/**
    *
    * <pre>
    * 1. 메소드명 : indexTralReqUDtl
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판요청 상세 진입(조회)
    * </pre>
    * @param map
    * @param model
    * @param request
    * @return
    * @throws Exception
    */
	@Override
	public List indexTralReqUDtl(Map<String, Object> map) throws Exception {
		
		return tralReqDAO.indexTralReqUDtl(map);
	}

	/**
    *
    * <pre>
    * 1. 메소드명 : queryPgsStat
    * 2. 작성일 : 2021. 5. 3.
    * 3. 작성자 : sjw7240
    * 4. 설명 :  공판 관리 진행상태 변경(승인요청,승인취소,검사승인,검사승인취소)
    * </pre>
    * @param map
    * @return
    * @throws Exception
    */
	@Override
	public void queryPgsStat(Map<String, Object> map) throws Exception {
		
		String userGb = (String) map.get("userGb");
		String insttCd = (String) map.get("insttCd");
		String userId = (String) map.get("userId");
		String type = (String)map.get("type");
		List tstfReqSnoList = (List) map.get("tpList");
		String incdtSno = "";
		
		//요청관리에서 넘어오는 상태 : 승인요청,승인취소,검사승인,검사승인취소만 처리
		if(!( "req".equals(type) || "reqCncl".equals(type) || "prosrAprv".equals(type) || "prosrAprvCncl".equals(type) || "rcv".equals(type) || "rcvCncl".equals(type) || "aprv".equals(type) || "aprvCncl".equals(type) || "tstf".equals(type))) {
			throw new MException(egovMessageSource.getMessage("dems.anls.req.005"));
		} 
		
		Map<String, Object> queryInfoMap = new HashMap<String, Object>();
		switch (type) {
		case "req"://승인요청
			queryInfoMap.put("type", type);
			queryInfoMap.put("pgsStat", "C03002");
			queryInfoMap.put("updrId", userId);
			break;
		case "reqCncl": //승인요청취소(임시저장으로변경)
			queryInfoMap.put("type", type);
			queryInfoMap.put("pgsStat", "C03001");
			queryInfoMap.put("updrId", userId);	
			break;
		case "prosrAprv"://검사승인
			queryInfoMap.put("type", type);
			queryInfoMap.put("prosrId", userId);
			queryInfoMap.put("pgsStat", "C03003");
			queryInfoMap.put("updrId", userId);			
			break;
		case "prosrAprvCncl"://검사승인취소(승인요청으로변경)
			queryInfoMap.put("type", type);
			queryInfoMap.put("pgsStat", "C03002");
			queryInfoMap.put("updrId", userId);
			queryInfoMap.put("prosrId", userId);
			break;
		case "rcv":
			queryInfoMap.put("type", type);
			queryInfoMap.put("rcvrId", userId);
			queryInfoMap.put("pgsStat", "C03005");
			queryInfoMap.put("updrId", userId);	
			break;
		case "rcvCncl":
			queryInfoMap.put("type", type);
			queryInfoMap.put("rcvrId", userId);
			queryInfoMap.put("pgsStat", "C03003");
			queryInfoMap.put("updrId", userId);	
			break;
		case "aprv":
			queryInfoMap.put("type", type);
			queryInfoMap.put("aprvrId", userId);
			queryInfoMap.put("pgsStat", "C03006");
			queryInfoMap.put("updrId", userId);	
			break;
		case "aprvCncl":
			queryInfoMap.put("type", type);
			queryInfoMap.put("aprvrId", userId);
			queryInfoMap.put("pgsStat", "C03005");
			queryInfoMap.put("updrId", userId);	
			break;
		case "tstf":
			queryInfoMap.put("type", type);
			queryInfoMap.put("tstfrId", userId);
			queryInfoMap.put("updrId", userId);	
			queryInfoMap.put("pgsStat", "C03006");
			queryInfoMap.put("tstfPlace", (String) map.get("tstfPlace"));
			queryInfoMap.put("realTstfCnts", (String) map.get("realTstfCnts"));
			break;
		}
		
		//권한체크
		if(("req".equals(type) || "reqCncl".equals(type)) && !"C01001".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"일반수사관"}));
		}
		
		if(("prosrAprv".equals(type) || "prosrAprvCncl".equals(type)) && !"C01002".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"군검사"}));
		}
		
		if(("rcv".equals(type) || "rcvCncl".equals(type)) && !"C01003".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식수사관"}));
		}
		
		if(("aprv".equals(type) || "aprvCncl".equals(type)) && !"C01004".equals(userGb)) {
			throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.000", new String[]{"포렌식수사과장"}));
		}

		String reqInsttCd = "";
		String pgsStat = "";
		String rcvNo = "";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		for(int i = 0; i < tstfReqSnoList.size(); i++) {
			
			paramMap.clear();
			paramMap.putAll(queryInfoMap);
			paramMap.put("tstfReqSno", tstfReqSnoList.get(i));
			
/*			if(tralReqDAO.selectReqInsttCd(paramMap) == null) {
				reqInsttCd = "";
			} else {
				reqInsttCd = tralReqDAO.selectReqInsttCd(paramMap);
			}*/
			reqInsttCd = tralReqDAO.selectReqInsttCd(paramMap);
			if(!"tstf".equals(type)) {
			
				//진행상태 체크
				pgsStat = tralReqDAO.selectPgsStat(paramMap);
			}
			//승인요청     req           -> 임시저장
			//승인요청취소 reqCanel      -> 승인요청
			//검사승인     prosrAprv      -> 승인요청
			//검사승인취소 prosrAprvCanel -> 검사승인
			//선별확인     selCfrm        -> 선별요청
			//선별확인취소 selCfrmCncl    -> 선별확인
			
			if("req".equals(type) && !"C03001".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String) (String)tstfReqSnoList.get(i),"임시저장"}));
			}
			
			if(("reqCncl".equals(type) || "prosrAprv".equals(type) )&& !"C03002".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String)tstfReqSnoList.get(i),"승인요청"}));
			}			
			
			if("prosrAprvCncl".equals(type) && !"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String)tstfReqSnoList.get(i),"검사승인"}));
			}
			
			if("rcv".equals(type) && !"C03003".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String)tstfReqSnoList.get(i),"접수"}));
			}		
			
			if("rcvCncl".equals(type) && !"C03005".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String)tstfReqSnoList.get(i),"접수취소"}));
			}
			
			if("aprv".equals(type) && !"C03005".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String)tstfReqSnoList.get(i),"승인"}));
			}			
			
			if("aprvCncl".equals(type) && !"C03006".equals(pgsStat)) {
				throw new MException(egovMessageSource.getMessageArgs("dems.anls.req.004",new String[]{(String)tstfReqSnoList.get(i),"승인취소"}));
			}
			
		}
	
	//진행상태 변경
		
	for(int i =0 ; i < tstfReqSnoList.size(); i++) {
		if("tstf".equals(type)) {
			paramMap.clear();
			paramMap.putAll(queryInfoMap);
			paramMap.put("tstfReqSno", tstfReqSnoList.get(i));
			paramMap.put("pgsStat", "C03005");
			tralReqDAO.updateTralReqPgsStat(paramMap);
			
		} else {
				
				paramMap.clear();
				paramMap.putAll(queryInfoMap);
				paramMap.put("tstfReqSno", tstfReqSnoList.get(i));
				
				if("rcv".equals(type)) {
					rcvNo = tralReqDAO.selectRcvNo(paramMap);
					paramMap.put("rcvNo", Integer.parseInt(rcvNo));
				}
				
				tralReqDAO.updateTralReqPgsStat(paramMap);
				
		}
	}
		
	}

	

}
