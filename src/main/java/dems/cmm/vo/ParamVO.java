package dems.cmm.vo;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

public class ParamVO extends PaginationInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private int parentPageNo = 1;

	private int currentPageNo = 1;

	private int recordCountPerPage = 10;

	private int pageSize = 10;

	public int getParentPageNo() {
		return parentPageNo;
	}

	public void setParentPageNo(int parentPageNo) {
		this.parentPageNo = parentPageNo;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}

	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public int getFirstRecordIndex() {

		return super.getFirstRecordIndex() + 1;
	}

	public int getSeqNo() {

		return getTotalRecordCount() - ((getCurrentPageNo() - 1) * getRecordCountPerPage());
	}

	public int getSeqNoAsc() {

		return (getCurrentPageNo() - 1) * getRecordCountPerPage() + 1;
	}

}
