package dems.site.pgs.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import frame.fcom.service.ComService;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.DemsConst;
import frame.futil.DemsUtil;
import frame.futil.PageUtil;
import frame.futil.ValidateUtil;

/**
 * 
 * <pre>
 * 1. 클래스명 : SitePgsController.java
 * 2. 작성일 : 2021. 4. 30.
 * 3. 작성자 : jij
 * 4. 설명 : @!@ 현장지원 접수관리 담당
 * </pre>
 */
@Controller
public class SitePgsController {

	protected Logger log = LoggerFactory.getLogger(SitePgsController.class);
	
	@Resource(name="comService")
	private ComService comService;
	
	@Resource(name="egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource MappingJackson2JsonView ajaxMainView;	
	
	 /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "validateUtil")
	private ValidateUtil validateUtil;
    
    /*@Resource(name="comController")
    ComController comController;    */

    /**
     * 
     * <pre>
     * 1. 메소드명 : indexSitePgsMList
     * 2. 작성일 : 2021. 4. 30. 오후 4:03:21
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 현장지원 접수관리 조회 화면 이동
     * </pre>
     * @param request
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/site/pgs/indexSitePgsMList.do")
    public String indexSitePgsMList(HttpServletRequest request, ModelMap model, SessionStatus status) throws Exception {

    	return "dems/site/pgs/sitePgsMList";
    	
    }
    
    /**
     * 
     * <pre>
     * 1. 메소드명 : indexSitePgsMDtl
     * 2. 작성일 : 2021. 4. 20. 오후 6:21:11
     * 3. 작성자 : jij
     * 4. 설명 : @!@ 현장지원 관리 상세 화면 이동 
     * </pre>
     * @param request
     * @param model
     * @param status
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/site/pgs/indexSitePgsMDtl.do")
    public String indexSitePgsMDtl(HttpServletRequest request, ModelMap model, SessionStatus status, @RequestParam HashMap<String, String> map) throws Exception {

    	return "dems/site/pgs/sitePgsMDtl";

    }
    
    @RequestMapping(value="/site/pgs/querySiteRcvMListExcel.do")
	public ModelAndView querySiteRcvMList(ModelMap model,
			HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap reqParams) throws Exception {

    	ModelAndView mav = new ModelAndView("excelView");
        Map<String, Object> dataMap = new HashMap<String, Object>();

        reqParams.put("sqlQueryId", "siteRcvDAO.querySiteRcvMList");
        reqParams.put("pageStart", 0);
        reqParams.put("perPageNum", 999999999);
		List list = comService.selectCommonQueryList(reqParams);
        String filename = "현장지원";
        String[] columnArr = {"요청번호", "등록일자", "부서명", "사건번호", "사건명", "지원예정일", "담당자", "주임검사(승인일자)", "진행상태"};
        String[] columnVarArr = {"suppReqSno", "regDt", "reqInsttNm", "incdtNo", "incdtNm", "cfscSechPlndDt", "reqUserNm", "prosrNm", "pgsStat"};
                
        dataMap.put("columnArr", columnArr);
        dataMap.put("columnVarArr", columnVarArr);
        dataMap.put("sheetNm", "게시물 목록");    
        dataMap.put("list", list);
        
        mav.addObject("dataMap", dataMap);
        mav.addObject("filename", filename);

		return mav;

	}
    
}	