package dems.site.calendar.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.site.calendar.service.CalendarService;
import dems.site.calendar.vo.ScheduleVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.DemsConst;

@Controller
public class ScheduleController {

	protected Logger log = LoggerFactory.getLogger(CalendarController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "calendarService")
	private CalendarService calendarService;

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;

    @RequestMapping(value="/schedule/scheduleRegistPop.do")
    public String scheduleRegistPop(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") ScheduleVO paramVO) throws Exception {



        return "dems/site/calendar/scheduleRegistPop";
    }

    @RequestMapping(value="/schedule/scheduleRegistProc.do")
    public ModelAndView scheduleRegistProc(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") ScheduleVO scheduleVO) throws Exception {

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
       	scheduleVO.setRegId(user.getUserId());
       	scheduleVO.setRegNm(user.getUserNm());
       	scheduleVO.setInsttCd(user.getInsttCd());
		int result = calendarService.insertSchedule(scheduleVO);
		if(result == 1){
			model.addAttribute(DemsConst.Messages_UserComMessage,"등록되었습니다.");
		} else{
			model.addAttribute(DemsConst.Messages_UserComMessage,"등록중 오류가 발생했습니다.");
		}

        return new ModelAndView(ajaxMainView, model);
    }

    @RequestMapping(value="/schedule/scheduleUpdatePop.do")
    public String scheduleUpdatePop(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") ScheduleVO paramVO) throws Exception {

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

    	ScheduleVO scheduleVO = calendarService.selectSchedule(paramVO);
		model.addAttribute("scheduleVO", scheduleVO);

		if(user.getUserId().equals(scheduleVO.getRegId())){
			return "dems/site/calendar/scheduleUpdatePop";
		}




        return "dems/site/calendar/scheduleViewPop";

    }

    @RequestMapping(value="/schedule/scheduleUpdateProc.do")
    public ModelAndView scheduleUpdateProc(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") ScheduleVO paramVO) throws Exception {

    	String resultMsg = "";

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
       	paramVO.setRegId(user.getUserId());

    	ScheduleVO tempVO = calendarService.selectSchedule(paramVO);
		if(tempVO == null || EgovStringUtil.isEmpty(tempVO.getRegId())){
			resultMsg = "존재하지 않는 일정입니다.";
		} else if(!tempVO.getRegId().equals(user.getUserId())){
			resultMsg = "접근 권한이 없습니다.";
		}

		if(EgovStringUtil.isEmpty(resultMsg)){
			int result = calendarService.updateSchedule(paramVO);
			if(result == 1){
				model.addAttribute(DemsConst.Messages_UserComMessage,"수정되었습니다.");
			} else{
				model.addAttribute(DemsConst.Messages_UserComMessage,"수정중 오류가 발생했습니다.");
			}
		}

        return new ModelAndView(ajaxMainView, model);
    }

    @RequestMapping(value="/schedule/scheduleDeleteProc.do")
    public ModelAndView scheduleDeleteProc(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") ScheduleVO paramVO) throws Exception {

    	String resultMsg = "";

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
       	paramVO.setRegId(user.getUserId());

    	ScheduleVO tempVO = calendarService.selectSchedule(paramVO);
		if(tempVO == null || EgovStringUtil.isEmpty(tempVO.getRegId())){
			resultMsg = "존재하지 않는 일정입니다.";
		} else if(!tempVO.getRegId().equals(user.getUserId())){
			resultMsg = "접근 권한이 없습니다.";
		}

		if(EgovStringUtil.isEmpty(resultMsg)){
			int result = calendarService.deleteSchedule(paramVO);
			if(result == 1){
				model.addAttribute(DemsConst.Messages_UserComMessage,"삭제되었습니다.");
			} else{
				model.addAttribute(DemsConst.Messages_UserComMessage,"삭제중 오류가 발생했습니다.");
			}
			model.addAttribute(DemsConst.Messages_UserComMessage,"삭제되었습니다.");
		}


        return new ModelAndView(ajaxMainView, model);
    }

    @RequestMapping(value="/schedule/scheduleViewPop.do")
    public String scheduleViewPop(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") ScheduleVO paramVO) throws Exception {


    	ScheduleVO scheduleVO = calendarService.selectSchedule(paramVO);
		model.addAttribute("scheduleVO", scheduleVO);

        return "dems/site/calendar/scheduleViewPop";
    }
}