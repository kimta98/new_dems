package dems.site.calendar.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.site.calendar.service.CalendarService;
import dems.site.calendar.vo.CalendarVO;
import dems.site.calendar.vo.ScheduleVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.flyt.login.service.FLytLoginVO;

@Controller
public class CalendarController {

	protected Logger log = LoggerFactory.getLogger(CalendarController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "calendarService")
	private CalendarService calendarService;

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;

    @RequestMapping(value="/calendar/calendarView.do")
    public String calendarView(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") CalendarVO paramVO) throws Exception {

		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter fmtDay = DateTimeFormat.forPattern("yyyy-MM-dd");
		DateTimeFormatter fmtMonth = DateTimeFormat.forPattern("yyyy-MM");
		// 오늘 날짜
		String today = now.toString(fmtDay);
		model.addAttribute("today", today);

		String searchMonth = paramVO.getSearchMonth();
		String searchYmd = paramVO.getSearchYmd();
		// 검색일자
		if (EgovStringUtil.isEmpty(searchYmd) && EgovStringUtil.isEmpty(searchMonth)) {
			searchYmd = today;
			searchMonth = now.toString(fmtMonth);
		} else if (EgovStringUtil.isNotEmpty(searchYmd)) {
			searchMonth = searchYmd.substring(0, 7);
		} else if (EgovStringUtil.isNotEmpty(searchMonth) && today.startsWith(searchMonth)) {
			searchYmd = today;
		}

		if (EgovStringUtil.isEmpty(searchYmd)) {
			searchYmd = today;
		}

		paramVO.setSearchYmd(searchYmd);
		paramVO.setSearchMonth(searchMonth);

		// 검색달
		LocalDateTime currentDate = LocalDateTime.parse(searchMonth, fmtMonth);
		model.addAttribute("currentMonth", searchMonth);

		model.addAttribute("weekOfMonth", EgovDateUtil.getWeekOfMonth(currentDate));
		model.addAttribute("lastDay", EgovDateUtil.getDayOfMonthMaxDay(currentDate));

		// 이전달
		model.addAttribute("prevMonth", currentDate.minusMonths(1).toString(fmtMonth));
		// 다음달
		model.addAttribute("nextMonth", currentDate.plusMonths(1).toString(fmtMonth));


       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();

		ScheduleVO scheduleVO = new ScheduleVO();
		scheduleVO.setScheduleYmd(searchMonth);
		scheduleVO.setInsttCd(user.getInsttCd());
		scheduleVO.setRegId(user.getUserId());

		String searchGroup = paramVO.getSearchGroup();

		if(EgovStringUtil.isEmpty(searchGroup)){
			paramVO.setSearchGroup("All");
		} else if("Team".equals(searchGroup)){
		} else if("My".equals(searchGroup)){
		}

		scheduleVO.setAccessCd(paramVO.getSearchGroup());

		HashMap<String,List<ScheduleVO>> scheduleMap = new HashMap<String,List<ScheduleVO>>();

		List<ScheduleVO> scheduleList = calendarService.selectScheduleList(scheduleVO);
		for(ScheduleVO tempVO : scheduleList){

			String ymd = tempVO.getScheduleYmd();
			List<ScheduleVO> list = scheduleMap.containsKey(ymd) ?
					(List<ScheduleVO>)scheduleMap.get(ymd) : new ArrayList<ScheduleVO>();

			list.add(tempVO);

			if(!scheduleMap.containsKey(ymd)){
				scheduleMap.put(ymd, list);
			}
		}

		model.addAttribute("scheduleMap", scheduleMap);

        return "dems/site/calendar/calendarView";
    }

}