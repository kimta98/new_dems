package dems.site.calendar.service.impl;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import dems.site.calendar.vo.ScheduleVO;
import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("calendarDAO")
public class CalendarDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(CalendarDAO.class);

    public List<ScheduleVO> selectScheduleList(ScheduleVO scheduleVO) throws Exception {
    	return (List) selectList("calendarDAO.selectScheduleList", scheduleVO);
    }

    public ScheduleVO selectSchedule(ScheduleVO scheduleVO) throws Exception {
    	return (ScheduleVO) selectOne("calendarDAO.selectSchedule", scheduleVO);
    }

    public int updateSchedule(ScheduleVO scheduleVO) throws Exception {
    	return update("calendarDAO.updateSchedule", scheduleVO);
    }


    public int deleteSchedule(ScheduleVO scheduleVO) throws Exception {
    	return update("calendarDAO.deleteSchedule", scheduleVO);
    }

    public int insertSchedule(ScheduleVO scheduleVO) throws Exception {
    	return insert("calendarDAO.insertSchedule", scheduleVO);
    }
}
