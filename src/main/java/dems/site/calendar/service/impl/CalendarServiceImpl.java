package dems.site.calendar.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import dems.site.calendar.service.CalendarService;
import dems.site.calendar.vo.CalendarVO;
import dems.site.calendar.vo.ScheduleVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import frame.fcom.service.ComService;


@Service("calendarService")
public class CalendarServiceImpl extends EgovAbstractServiceImpl implements CalendarService{

	protected Logger log = LoggerFactory.getLogger(CalendarServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;

	@Resource(name="calendarDAO")
	private CalendarDAO calendarDAO;

	@Override
	public List<ScheduleVO> selectScheduleList(ScheduleVO scheduleVO) throws Exception {
		return calendarDAO.selectScheduleList(scheduleVO);
	}

	@Override
	public ScheduleVO selectSchedule(ScheduleVO scheduleVO) throws Exception {
		return calendarDAO.selectSchedule(scheduleVO);
	}

	@Override
	public int updateSchedule(ScheduleVO scheduleVO) throws Exception {
		return calendarDAO.updateSchedule(scheduleVO);
	}

	@Override
	public int deleteSchedule(ScheduleVO scheduleVO) throws Exception {
		return calendarDAO.deleteSchedule(scheduleVO);
	}

	@Override
	public int insertSchedule(ScheduleVO scheduleVO) throws Exception {
		return calendarDAO.insertSchedule(scheduleVO);
	}



}