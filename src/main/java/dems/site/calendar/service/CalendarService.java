package dems.site.calendar.service;

import java.util.List;

import dems.site.calendar.vo.ScheduleVO;


public interface CalendarService {

	List<ScheduleVO> selectScheduleList(ScheduleVO scheduleVO) throws Exception;

	ScheduleVO selectSchedule(ScheduleVO scheduleVO) throws Exception;

	int updateSchedule(ScheduleVO scheduleVO) throws Exception;

	int deleteSchedule(ScheduleVO scheduleVO) throws Exception;

	int insertSchedule(ScheduleVO scheduleVO) throws Exception;

}