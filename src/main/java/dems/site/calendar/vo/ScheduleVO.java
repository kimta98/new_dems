package dems.site.calendar.vo;

public class ScheduleVO {

	private static final long serialVersionUID = 1L;

	private int scheduleIdx;

	private String scheduleType;

	private String scheduleSubject;

	private String scheduleContent;

	private String scheduleUrl;

	private String scheduleYmd;

	private String accessCd;

	private String insttCd;

	private String delYn;

	private String regDt;

	private String regId;

	private String regNm;

	public int getScheduleIdx() {
		return scheduleIdx;
	}

	public void setScheduleIdx(int scheduleIdx) {
		this.scheduleIdx = scheduleIdx;
	}

	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public String getScheduleSubject() {
		return scheduleSubject;
	}

	public void setScheduleSubject(String scheduleSubject) {
		this.scheduleSubject = scheduleSubject;
	}

	public String getScheduleContent() {
		return scheduleContent;
	}

	public void setScheduleContent(String scheduleContent) {
		this.scheduleContent = scheduleContent;
	}

	public String getScheduleUrl() {
		return scheduleUrl;
	}

	public void setScheduleUrl(String scheduleUrl) {
		this.scheduleUrl = scheduleUrl;
	}

	public String getScheduleYmd() {
		return scheduleYmd;
	}

	public void setScheduleYmd(String scheduleYmd) {
		this.scheduleYmd = scheduleYmd;
	}

	public String getAccessCd() {
		return accessCd;
	}

	public void setAccessCd(String accessCd) {
		this.accessCd = accessCd;
	}

	public String getInsttCd() {
		return insttCd;
	}

	public void setInsttCd(String insttCd) {
		this.insttCd = insttCd;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public String getRegDt() {
		return regDt;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public String getRegNm() {
		return regNm;
	}

	public void setRegNm(String regNm) {
		this.regNm = regNm;
	}

}
