package dems.site.calendar.vo;

import java.util.Date;

import dems.cmm.vo.ParamVO;

public class CalendarVO extends ParamVO {

	private static final long serialVersionUID = 1L;

	private String searchMonth;

	private String searchYmd;

	private String searchGroup;

	public String getSearchMonth() {
		return searchMonth;
	}

	public void setSearchMonth(String searchMonth) {
		this.searchMonth = searchMonth;
	}

	public String getSearchYmd() {
		return searchYmd;
	}

	public void setSearchYmd(String searchYmd) {
		this.searchYmd = searchYmd;
	}

	public String getSearchGroup() {
		return searchGroup;
	}

	public void setSearchGroup(String searchGroup) {
		this.searchGroup = searchGroup;
	}

}
