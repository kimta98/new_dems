/**
 *
 * <pre>
 * 1. 클래스명 : AnlsReqService.java
 * 2. 작성일 : 2021. 4. 15.
 * 3. 작성자 : Leeji
 * 4. 설명 : 분석지원요청 Service
 * </pre>
 *
 */
package dems.site.rcv.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * <pre>
 * 1. 클래스명 : SiteRcvService.java
 * 2. 작성일 : 2021. 4. 29.
 * 3. 작성자 : jij
 * 4. 설명 : @!@ 현장지원 관리 담당
 * </pre>
 */
public interface SiteRcvService {

	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : reqSiteRcvUDtl
	 * 2. 작성일 : 2021. 4. 30. 오후 2:14:56
	 * 3. 작성자 : jij
	 * 4. 설명 : @!@ 현장지원 요청
	 * </pre>
	 * @param map
	 * @return
	 * @throws Exception
	 */
	HashMap reqSiteRcvUDtl(String paramData) throws Exception;

	/**
	 * 
	 * <pre>
	 * 1. 메소드명 : reqCnclSiteRcvUDtl
	 * 2. 작성일 : 2021. 4. 30. 오후 2:15:08
	 * 3. 작성자 : jij
	 * 4. 설명 : @!@ 현장지원 요청취소
	 * </pre>
	 * @param paramData
	 * @return
	 * @throws Exception
	 */
	HashMap reqCnclSiteRcvUDtl(String paramData) throws Exception;

}