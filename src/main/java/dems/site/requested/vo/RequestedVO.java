package dems.site.requested.vo;

import java.util.Date;

import dems.cmm.vo.ParamVO;

public class RequestedVO extends ParamVO {

	private static final long serialVersionUID = 1L;

	private String reqSeq;

	private String reqBorderNm;

	private String reqInsttNm;

	private String reqInsttCd; //

	private String incdtSno; //

	private String incdtNm; //

	private String incdtNo; //

	private String reqUserNm;

	private String reqUserId; //

	private String reqDt;

	private String reqStatus;

	/** search */
	private String searchIncdtNo;

	private String searchIncdtNm;

	private String searchBorder;

	private String searchRegNm;

	private String startDtFormat;

	private String endDtFormat;

	public String getReqSeq() {
		return reqSeq;
	}

	public void setReqSeq(String reqSeq) {
		this.reqSeq = reqSeq;
	}

	public String getReqBorderNm() {
		return reqBorderNm;
	}

	public void setReqBorderNm(String reqBorderNm) {
		this.reqBorderNm = reqBorderNm;
	}

	public String getReqInsttNm() {
		return reqInsttNm;
	}

	public void setReqInsttNm(String reqInsttNm) {
		this.reqInsttNm = reqInsttNm;
	}

	public String getReqInsttCd() {
		return reqInsttCd;
	}

	public void setReqInsttCd(String reqInsttCd) {
		this.reqInsttCd = reqInsttCd;
	}

	public String getIncdtSno() {
		return incdtSno;
	}

	public void setIncdtSno(String incdtSno) {
		this.incdtSno = incdtSno;
	}

	public String getIncdtNm() {
		return incdtNm;
	}

	public void setIncdtNm(String incdtNm) {
		this.incdtNm = incdtNm;
	}

	public String getIncdtNo() {
		return incdtNo;
	}

	public void setIncdtNo(String incdtNo) {
		this.incdtNo = incdtNo;
	}

	public String getReqUserNm() {
		return reqUserNm;
	}

	public void setReqUserNm(String reqUserNm) {
		this.reqUserNm = reqUserNm;
	}

	public String getReqUserId() {
		return reqUserId;
	}

	public void setReqUserId(String reqUserId) {
		this.reqUserId = reqUserId;
	}

	public String getReqDt() {
		return reqDt;
	}

	public void setReqDt(String reqDt) {
		this.reqDt = reqDt;
	}

	public String getReqStatus() {
		return reqStatus;
	}

	public void setReqStatus(String reqStatus) {
		this.reqStatus = reqStatus;
	}

	public String getSearchIncdtNo() {
		return searchIncdtNo;
	}

	public void setSearchIncdtNo(String searchIncdtNo) {
		this.searchIncdtNo = searchIncdtNo;
	}

	public String getSearchIncdtNm() {
		return searchIncdtNm;
	}

	public void setSearchIncdtNm(String searchIncdtNm) {
		this.searchIncdtNm = searchIncdtNm;
	}

	public String getSearchBorder() {
		return searchBorder;
	}

	public void setSearchBorder(String searchBorder) {
		this.searchBorder = searchBorder;
	}

	public String getSearchRegNm() {
		return searchRegNm;
	}

	public void setSearchRegNm(String searchRegNm) {
		this.searchRegNm = searchRegNm;
	}

	public String getStartDtFormat() {
		return startDtFormat;
	}

	public void setStartDtFormat(String startDtFormat) {
		this.startDtFormat = startDtFormat;
	}

	public String getEndDtFormat() {
		return endDtFormat;
	}

	public void setEndDtFormat(String endDtFormat) {
		this.endDtFormat = endDtFormat;
	}

}
