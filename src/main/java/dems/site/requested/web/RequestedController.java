package dems.site.requested.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.site.requested.service.RequestedService;
import dems.site.requested.vo.RequestedVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.com.cmm.util.RegisterExcelUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.flyt.login.service.FLytLoginVO;

@Controller
public class RequestedController {

	protected Logger log = LoggerFactory.getLogger(RequestedController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "requestedService")
	private RequestedService requestedService;

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;

    @RequestMapping(value="/requested/requestedList.do")
    public String scheduleRegistPop(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "paramVO") RequestedVO requestedVO) throws Exception {
    	// C01001 C01002 C01003 C01004

       	FLytLoginVO user = (FLytLoginVO)EgovUserDetailsHelper.getAuthenticatedUser();
       	String userGb = user.getUserGb();

       	HashMap<String,Object> reqParams = new HashMap<String,Object>();
       	if("C01001".equals(userGb)){
    		reqParams.put("rcvPgsStat", "C03001,C03002,C03003,C03005,C03006".split(","));
    		reqParams.put("anlsPgsStat", "C03001,C03002,C03003,C03005,C03006,C03007,C03008".split(","));
    		reqParams.put("tralPgsStat", "C03001,C03002,C03003,C03005,C03006".split(","));
    		reqParams.put("eqpPgsStat", "".split(","));
    		reqParams.put("evdcPgsStat", "C03001,C03002,C03003,C03005,C03006".split(","));
       	} else if("C01002".equals(userGb)){
    		reqParams.put("rcvPgsStat", "C03002,C03003,C03005,C03006".split(","));
    		reqParams.put("anlsPgsStat", "C03002,C03003,C03005,C03006,C03007,C03008".split(","));
    		reqParams.put("tralPgsStat", "C03002,C03003,C03005,C03006".split(","));
    		reqParams.put("eqpPgsStat", "".split(","));
    		reqParams.put("evdcPgsStat", "C03002,C03003,C03005,C03006".split(","));
       	} else if("C01003".equals(userGb)){
    		reqParams.put("rcvPgsStat", "C03003,C03005,C03006".split(","));
    		reqParams.put("anlsPgsStat", "C03003,C03005,C03006,C03007,C03008".split(","));
    		reqParams.put("tralPgsStat", "C03003,C03005,C03006".split(","));
    		reqParams.put("eqpPgsStat", "".split(","));
    		reqParams.put("evdcPgsStat", "C03003,C03005,C03006".split(","));
       	} else if("C01004".equals(userGb)){
    		reqParams.put("rcvPgsStat", "C03005,C03006".split(","));
    		reqParams.put("anlsPgsStat", "C03005,C03006,C03007,C03008".split(","));
    		reqParams.put("tralPgsStat", "C03005,C03006".split(","));
    		reqParams.put("eqpPgsStat", "".split(","));
    		reqParams.put("evdcPgsStat", "C03005,C03006".split(","));
       	}
       	reqParams.put("searchIncdtNo", requestedVO.getSearchIncdtNo());
       	reqParams.put("searchIncdtNm", requestedVO.getSearchIncdtNm());
       	reqParams.put("searchBorder", requestedVO.getSearchBorder());
       	reqParams.put("searchRegNm", requestedVO.getSearchRegNm());

       	reqParams.put("session_usergb", user.getUserGb());
       	reqParams.put("session_insttcd", user.getInsttCd());

       	reqParams.put("firstRecordIndex", requestedVO.getFirstRecordIndex());
       	reqParams.put("lastRecordIndex", requestedVO.getLastRecordIndex());
       	reqParams.put("startDtFormat", requestedVO.getStartDtFormat());
       	reqParams.put("endDtFormat", requestedVO.getEndDtFormat());

		int totalCount = requestedService.selectRequestedCount(reqParams);
		requestedVO.setTotalRecordCount(totalCount);

		List<RequestedVO> list = requestedService.selectRequestedList(reqParams);
		model.addAttribute("requestedList", list);
		model.addAttribute("totalCount", totalCount);
        return "dems/site/requested/requestedList";
    }
    
    @RequestMapping(value = "/requested/register.do")
    public String registerView(ModelMap model, HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap reqParams) throws Exception {
    	
    	//response.setHeader("Set-Cookie", "fileDownload=true; path=/");
    	//ModelAndView mav = RegisterExcelUtil.createRegisterExcel(reqParams, requestedService.selectRequestedList(reqParams));
    	
    	return "dems/site/register/register";
    }
    
    @RequestMapping(value = "/requested/registerExcel.do")
    public ModelAndView queryRegisterExcel(ModelMap model, HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap reqParams) throws Exception {
    	
    	String checkStr = (String) reqParams.get("checkList");
    	String[] checkArray = checkStr.split(",");
    	
    	reqParams.put("rcvPgsStat", "C03003,C03005,C03006".split(","));
		reqParams.put("anlsPgsStat", "C03003,C03005,C03006,C03007,C03008".split(","));
		reqParams.put("tralPgsStat", "C03003,C03005,C03006".split(","));
		reqParams.put("eqpPgsStat", "".split(","));
		reqParams.put("evdcPgsStat", "C03003,C03005,C03006".split(","));
		
		int totalCount = requestedService.selectRequestedCount(reqParams);
		
		reqParams.put("firstRecordIndex", "1");
       	reqParams.put("lastRecordIndex", totalCount);
       	reqParams.put("startDtFormat", reqParams.get("startDt"));
       	reqParams.put("endDtFormat", reqParams.get("endDt"));
       	
		Map<String, Object> totalMap = new HashMap<String, Object>();
		
		/** excel 생성 시 map key 값 array */ 
		totalMap.put("itemKeyArray", checkArray);
		
    	for(String checkItem : checkArray) {
    		List<RequestedVO> list = null;
    		switch(checkItem) {
    		case "site" :
    			reqParams.put("searchBorder", "지원요청");
    			list = requestedService.selectRequestedList(reqParams);
    			break;
    		case "analysis" :
    			reqParams.put("searchBorder", "분석요청");
    			list = requestedService.selectRequestedList(reqParams);
    			break;
    		case "tral" :
    			reqParams.put("searchBorder", "공판요청");
    			list = requestedService.selectRequestedList(reqParams);
    			break;
    		case "evdc" :
    			reqParams.put("searchBorder", "증거관리");
    			list = requestedService.selectRequestedList(reqParams);
    			break;
    		case "equip" :
    			reqParams.put("searchBorder", "장비대여");
    			list = requestedService.selectRequestedList(reqParams);
    			break;
    		}
    		totalMap.put(checkItem, list);
    	}
    	response.setHeader("Set-Cookie", "fileDownload=true; path=/");
    	ModelAndView mav = new ModelAndView("registerExcelView");

        mav.addObject("dataMap", totalMap);
        mav.addObject("filename", "대장관리_" + new SimpleDateFormat("yyyyMMdd").format(new Date()));
    	
    	return mav;
    }

}