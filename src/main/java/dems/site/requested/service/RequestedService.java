package dems.site.requested.service;

import java.util.List;
import java.util.Map;

import dems.site.calendar.vo.ScheduleVO;
import dems.site.requested.vo.RequestedVO;


public interface RequestedService {

	List<RequestedVO> selectRequestedList(Map map) throws Exception;

	int selectRequestedCount(Map map) throws Exception;


}