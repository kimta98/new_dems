package dems.site.requested.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import dems.site.calendar.service.CalendarService;
import dems.site.calendar.vo.CalendarVO;
import dems.site.calendar.vo.ScheduleVO;
import dems.site.requested.service.RequestedService;
import dems.site.requested.vo.RequestedVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import frame.fcom.service.ComService;


@Service("requestedService")
public class RequestedServiceImpl extends EgovAbstractServiceImpl implements RequestedService{

	protected Logger log = LoggerFactory.getLogger(RequestedServiceImpl.class);

	@Resource(name="comService")
	private ComService comService;

	@Resource(name="requestedDAO")
	private RequestedDAO requestedDAO;

	@Override
	public List<RequestedVO> selectRequestedList(Map map) throws Exception {
		return requestedDAO.selectRequestedList(map);
	}

	@Override
	public int selectRequestedCount(Map map) throws Exception {
		return requestedDAO.selectRequestedCount(map);
	}



}