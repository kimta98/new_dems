package dems.site.requested.service.impl;


import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import dems.site.requested.vo.RequestedVO;
import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("requestedDAO")
public class RequestedDAO extends EgovComAbstractDAO {

	protected Logger log = LoggerFactory.getLogger(RequestedDAO.class);

    public List<RequestedVO> selectRequestedList(Map map) throws Exception {
    	return (List) selectList("requestedDAO.selectRequestedList", map);
    }
    public Integer selectRequestedCount(Map map) throws Exception {
    	return (Integer) selectOne("requestedDAO.selectRequestedCount", map);
    }
}
