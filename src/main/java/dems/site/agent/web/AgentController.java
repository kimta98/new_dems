package dems.site.agent.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import dems.site.calendar.service.CalendarService;
import dems.site.calendar.vo.ScheduleVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;
import frame.flyt.login.service.FLytLoginVO;
import frame.futil.DemsConst;

@Controller
public class AgentController {

	protected Logger log = LoggerFactory.getLogger(AgentController.class);

	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;


    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource MappingJackson2JsonView ajaxMainView;

    @RequestMapping(value="/agent/api/selectUserInfo.do")
    public ModelAndView scheduleRegistProc(HttpServletRequest request, ModelMap model, @ModelAttribute(value = "userKey") String userKey) throws Exception {
    	HashMap resultMap = new HashMap<Object,Object>();

    	HashMap<String,String> reqParams = new HashMap<String,String>();
    	reqParams.put("userKey", userKey);
    	reqParams.put("sqlQueryId", "agentDAO.selectUserInfoByUserKey");
    	List anlsPgsList = comService.selectCommonQueryList(reqParams);
		resultMap.put("list", anlsPgsList);

		model.addAllAttributes(resultMap);

        return new ModelAndView(ajaxMainView, model);
    }

}