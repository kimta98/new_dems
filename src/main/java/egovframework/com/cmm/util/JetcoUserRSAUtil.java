package egovframework.com.cmm.util;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Base64;

public class JetcoUserRSAUtil {
	
	public static final int KEY_SIZE = 1024;
	
	public static String createRSAPubKey() throws Exception{
		SecureRandom secureRandom = new SecureRandom();
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(KEY_SIZE, secureRandom);
		KeyPair keyPair = keyPairGenerator.genKeyPair();
		
		PublicKey pubKey = keyPair.getPublic();
		
		return Base64.getEncoder().encodeToString(pubKey.getEncoded());
	}

}
