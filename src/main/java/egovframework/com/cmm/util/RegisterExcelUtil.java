package egovframework.com.cmm.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import dems.site.requested.service.RequestedService;
import dems.site.requested.vo.RequestedVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.rte.fdl.property.EgovPropertyService;
import frame.fcom.service.ComService;

public class RegisterExcelUtil {
	
	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;

	@Resource(name = "comService")
	private ComService comService;

	@Resource(name = "requestedService")
	private RequestedService requestedService;

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

	public ModelAndView createRegisterExcel(HashMap reqParams, Map<String, Object> totalMap) {
		ModelAndView mav = new ModelAndView("RegisterExcelView");

        mav.addObject("dataMap", totalMap);
        mav.addObject("filename", "대장관리_" + new SimpleDateFormat("yyyyMMdd").format(new Date()));
		
		return mav;
	}
	
}
