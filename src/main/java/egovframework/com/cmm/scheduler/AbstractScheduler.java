package egovframework.com.cmm.scheduler;

import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

public abstract class AbstractScheduler {
	
	private ThreadPoolTaskScheduler scheduler;
	
	/**
	 * 스케줄 시작
	 */
	public void startScheduler() {
		scheduler = new ThreadPoolTaskScheduler();
		scheduler.initialize();
		scheduler.schedule(getRunnable(), getTrigger());
	}
	
	/**
	 * 스케줄 로직
	 * @return
	 */
	private Runnable getRunnable() {
		return new Runnable() {
			@Override
			public void run() {
				runner();
			}
		};
	}
	
	public abstract void runner();
	
	/**
	 * 스케줄 주기
	 * @return
	 */
	public abstract Trigger getTrigger();
	
	/**
	 * 스케줄 종료
	 */
	public void stopScheduler() {
		if(scheduler != null) {
			scheduler.shutdown();
		}
	}

}