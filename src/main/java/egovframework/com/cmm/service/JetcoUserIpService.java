package egovframework.com.cmm.service;

import java.util.Map;

public interface JetcoUserIpService {
    
    @SuppressWarnings("rawtypes")
    void selectUserIp(Map map) throws Exception;
    
    @SuppressWarnings("rawtypes")
    Map selectUserIpById(Map map) throws Exception;
    
    @SuppressWarnings("rawtypes")
    void insertUserIp(Map map) throws Exception;
    
    @SuppressWarnings("rawtypes")
    void updateUserIp(Map map) throws Exception;

}
