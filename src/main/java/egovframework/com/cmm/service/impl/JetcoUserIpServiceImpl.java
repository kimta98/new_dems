package egovframework.com.cmm.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.service.JetcoUserIpService;

@Service("jetcoUserIpService")
public class JetcoUserIpServiceImpl implements JetcoUserIpService{

    protected Logger log = LoggerFactory.getLogger(JetcoUserIpServiceImpl.class);
    
    @Resource(name="jetcoUserIpDAO")
    private JetcoUserIpDAO jetcoUserIpDAO;
    
    
    @SuppressWarnings("rawtypes")
    @Override
    public void selectUserIp(Map map) throws Exception {
        List userIpList = jetcoUserIpDAO.selectUserIp(map);
        jetcoUserIpDAO.updateEmptyUserId(map);
        if(userIpList != null && userIpList.size() > 0) {
            this.updateUserIp(map);
        }else {
            this.insertUserIp(map);
        }
    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public Map selectUserIpById(Map map) throws Exception {
        return jetcoUserIpDAO.selectUserIpById(map);
    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public void insertUserIp(Map map) throws Exception {
        jetcoUserIpDAO.insertUserIp(map);
    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public void updateUserIp(Map map) throws Exception {
        jetcoUserIpDAO.updateUserIp(map);
    }
    
}
