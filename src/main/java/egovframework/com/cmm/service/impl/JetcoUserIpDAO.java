package egovframework.com.cmm.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;


@Repository("jetcoUserIpDAO")
public class JetcoUserIpDAO extends EgovComAbstractDAO{
    
    protected Logger log = LoggerFactory.getLogger(JetcoUserIpDAO.class);
    
    public List selectUserIp(Map map) throws Exception {
        return selectList("jetcoUserIpDAO.selectUserIp", map);
    }
    
    public Map selectUserIpById(Map map) throws Exception {
        return selectOne("jetcoUserIpDAO.selectUserIpById", map);
    }
    
    public void insertUserIp(Map map) throws Exception {
        insert("jetcoUserIpDAO.insertUserIp", map);
    }
    
    public void updateUserIp(Map map) throws Exception {
        update("jetcoUserIpDAO.updateUserIp", map);
    }
    
    public void updateEmptyUserId(Map map) throws Exception {
        update("jetcoUserIpDAO.updateEmptyUserId", map);
    }
    
}
