package egovframework.com.cmm;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import egovframework.rte.fdl.cryptography.EgovEnvCryptoService;
import egovframework.rte.fdl.cryptography.impl.EgovEnvCryptoServiceImpl;

public class EgovEnvCryptoUserTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(EgovEnvCryptoUserTest.class);

	public static void main(String[] args) {

		String[] arrCryptoString = { "userId", // 데이터베이스 접속 계정 설정
				"userPassword", // 데이터베이스 접속 패드워드 설정
				"url", // 데이터베이스 접속 주소 설정
				"databaseDriver" // 데이터베이스 드라이버
		};

		ArrayList<String[]> arrCryptoList = new ArrayList<String[]>();

		arrCryptoList.add(new String[]{"Globals.mysql.Url","jdbc:log4jdbc:mysql://192.168.51.69:3306/dems?useSSL=false&useUnicode=true&characterEncoding=utf8"});
		arrCryptoList.add(new String[]{"Globals.mysql.UserName","root"});
		arrCryptoList.add(new String[]{"Globals.mysql.Password","P@ssw0rd"});

		arrCryptoList.add(new String[]{"Globals.oracle.Url","jdbc:oracle:thin:@127.0.0.1:1521:orcl"});

		arrCryptoList.add(new String[]{"Globals.altibase.Url","jdbc:Altibase://127.0.0.1:20300/mydb?encoding=UTF-8"});
		arrCryptoList.add(new String[]{"Globals.tibero.Url","jdbc:tibero:thin:@127.0.0.1:8629:tibero"});
		arrCryptoList.add(new String[]{"Globals.cubrid.Url","jdbc:cubrid:127.0.0.1:30000:com:::?charset=utf-8"});
		arrCryptoList.add(new String[]{"Globals.maria.Url","jdbc:mariadb://127.0.0.1:3336/com"});
		arrCryptoList.add(new String[]{"Globals.postgres.Url","jdbc:postgresql://127.0.0.1:5432/comdb"});

		arrCryptoList.add(new String[]{"Globals.oracle.UserName","com"});
		arrCryptoList.add(new String[]{"Globals.oracle.Password","setThePassword"});


		LOGGER.info("------------------------------------------------------");
		ApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] { "classpath:/egovframework/spring/com/context-crypto.key" });
		EgovEnvCryptoService cryptoService = context.getBean(EgovEnvCryptoServiceImpl.class);
		LOGGER.info("------------------------------------------------------");

		String label = "";
		try {
			for (int i = 0; i < arrCryptoList.size(); i++) {
				String array[] = arrCryptoList.get(i);

				label = array[0];

				LOGGER.info(label + " 원본(orignal):" + array[1]);
				LOGGER.info(label + " 인코딩(encrypted):" + cryptoService.encrypt(array[1]));
				LOGGER.info("------------------------------------------------------");
			}
		} catch (IllegalArgumentException e) {
			LOGGER.error("[" + e.getClass() + "] IllegalArgumentException : " + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("[" + e.getClass() + "] Exception : " + e.getMessage());
		}

	}

}